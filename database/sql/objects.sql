-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Авг 29 2017 г., 08:29
-- Версия сервера: 5.7.8-rc
-- Версия PHP: 5.6.30-12~ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `rk_dev`
--

--
-- Дамп данных таблицы `objects`
--

INSERT INTO `objects` (`id`, `owner_id`, `object_type`, `object_name`, `object_country`, `object_city`, `object_address`, `object_ip`, `created_at`, `updated_at`, `allow_d`, `age`, `kods`, `show`, `grades`, `entrance_ip`, `sync_path`, `sync_enable`, `object_regnr`, `vat`) VALUES
(2, 6, '1', 'Rīgas 74. vidusskola', 'LV', 'RĪGA', 'INDUĻA IELA 4, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1084', '213.101.216.89', '2013-09-02 13:21:26', '2017-04-12 05:01:15', 1, '', 338, 1, '1A,1B,2A,2B,3A,3B,4A,4B', '', '', 1, '3713900758', 0),
(4, 22, '1', 'Rīgas 72. vidusskola', 'LV', 'RĪGA', 'IKŠĶILES IELA 6, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1057', '213.101.216.88', '2015-08-24 09:22:40', '2017-04-12 05:01:15', 0, '', 444, 1, '', '', '', 0, '3613900670', 0),
(5, 23, '1', 'Rīgas 93. vidusskola', 'LV', 'RĪGA', 'SESKU IELA 72, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1082', '213.101.216.92', '2015-08-24 09:24:20', '2017-04-12 05:01:15', 0, '', 331, 1, '', '', '', 0, '3613900669', 0),
(6, 25, '1', 'Rīgas Rīnūžu vidusskola', 'LV', 'RĪGA', 'AUGUSTA DOMBROVSKA IELA 88, ZIEMEĻU RAJONS, RĪGA, LV-1015', '213.101.216.102', '2015-08-26 15:37:07', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900818', 0),
(7, 26, '1', 'Rīgas 88. vidusskola', 'LV', 'RĪGA', 'ILŪKSTES IELA 30, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1082', '213.101.216.84', '2015-08-26 19:59:51', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3613900672', 0),
(8, 27, '1', 'Rīgas 9. vakara (maiņu) vidusskola', 'LV', 'RĪGA', 'LAKTAS IELA 2/4, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1013', '213.101.216.96', '2015-08-26 20:46:43', '2017-06-15 05:01:14', 0, '', 1, 1, '', '', '', 0, '3714900782', 0),
(9, 28, '1', 'Rīgas 31. vidusskola', 'LV', 'RĪGA', 'SKUJU IELA 11, ZIEMEĻU RAJONS, RĪGA, LV-1015', '213.101.216.98', '2015-08-27 07:55:57', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900826', 0),
(10, 29, '1', 'Rīgas 34. vidusskola', 'LV', 'RĪGA', 'KANDAVAS IELA 4, KURZEMES RAJONS, RĪGA, LV-1083', '213.101.216.86', '2015-08-27 08:29:55', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900765', 0),
(11, 31, '1', 'Rīgas 21. vidusskola', 'LV', 'RĪGA', 'TOMSONA IELA 35, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1013', '213.101.216.108', '2015-08-27 08:31:49', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900750', 0),
(12, 32, '1', 'Rīgas Itas Kozakēvičas Poļu vidusskola', 'LV', 'RĪGA', 'NĪCGALES IELA 15, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1035', '213.101.216.90', '2015-08-27 09:04:13', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3713900789', 0),
(13, 33, '1', 'Rīgas Kristīgā vidusskola', 'LV', 'RĪGA', 'AUGUSTA DEGLAVA IELA 3, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1009', '213.101.216.104', '2015-08-27 09:05:53', '2017-04-12 05:01:18', 0, '', 1, 1, '', '', '', 0, '3613900676', 0),
(14, 34, '1', 'Rīgas Lietuviešu vidusskola', 'LV', 'RĪGA', 'PRŪŠU IELA 42A, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1057', '213.101.216.91', '2015-08-27 09:06:20', '2017-04-12 05:01:17', 0, '', 1, 1, '', '', '', 0, '3613901263', 0),
(15, 35, '1', 'Mežciema pamatskola', 'LV', 'RĪGA', 'HIPOKRĀTA IELA 31, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1079', '213.101.216.94', '2015-08-27 09:07:57', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3712900760', 0),
(16, 36, '1', 'Rīgas Centra daiļamatniecības pamatskola', 'LV', 'RĪGA', 'ASPAZIJAS BULVĀRIS 34, CENTRA RAJONS, RĪGA, LV-1050', '213.101.216.110', '2015-08-27 09:08:43', '2017-04-12 05:01:17', 0, '', 1, 1, '', '', '', 0, '3412900786', 0),
(17, 37, '1', 'Rīgas Purvciema vidusskola', 'LV', 'RĪGA', 'DETLAVA BRANTKALNA IELA 5, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1082', '213.101.216.100', '2015-08-27 09:09:08', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900749', 0),
(18, 38, '1', 'Rīgas 34. vidusskola Vircavas 7', 'LV', 'RĪGA', 'KANDAVAS IELA 4, KURZEMES RAJONS, RĪGA, LV-1083', '213.101.216.106', '2015-08-27 09:12:22', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900765', 0),
(20, 40, '1', 'Rīgas 6. vidusskola', 'LV', 'RĪGA', 'MATĪSA IELA 37/39, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1011', '213.101.216.240', '2015-10-08 09:44:10', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900684', 0),
(21, 41, '1', 'Rīgas sākumskola \"Valodiņa\"', 'LV', 'RĪGA', 'BLAUMAŅA IELA 26, CENTRA RAJONS, RĪGA, LV-1011', '10.10.10.10', '2015-10-08 11:21:22', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3420900801', 0),
(22, 42, '1', 'Rīgas 60. vidusskola', 'LV', 'RĪGA', 'VAIDAVAS IELA 6, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1084', '213.101.216.234', '2015-10-08 14:43:18', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3713900743', 0),
(23, 43, '1', 'Rīgas Klasiskā ģimnāzija', 'LV', 'RĪGA', 'PURVCIEMA IELA 38, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1035', '213.101.216.236', '2015-10-08 17:00:52', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3716900736', 0),
(24, 44, '1', 'Rīgas Valsts 1. ģimnāzija', 'LV', 'RĪGA', 'RAIŅA BULVĀRIS 8, CENTRA RAJONS, RĪGA, LV-1050', '213.101.216.244', '2015-10-12 11:31:38', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3419900793', 0),
(25, 45, '1', 'Rīgas Natālijas Draudziņas vidusskola', 'LV', 'RĪGA', 'TĒRBATAS IELA 69, CENTRA RAJONS, RĪGA, LV-1001', '213.101.216.228', '2015-10-12 13:43:59', '2017-06-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3413902866', 0),
(26, 46, '1', 'Rīgas Centra humanitārā vidusskola', 'LV', 'RĪGA', 'KRIŠJĀŅA BARONA IELA 116, CENTRA RAJONS, RĪGA, LV-1012', '213.101.216.242', '2015-10-13 08:59:34', '2017-06-15 05:01:15', 0, '', 1, 1, '', '', '', 0, '3413900790', 0),
(27, 47, '1', 'Rīgas Valsts 3. ģimnāzija', 'LV', 'RĪGA', 'GRĒCINIEKU IELA 10, CENTRA RAJONS, RĪGA, LV-1050', '213.101.216.212', '2015-10-13 13:09:33', '2017-04-12 05:01:21', 0, '', 1, 1, '', '', '', 0, '3419900794', 0),
(28, 48, '1', 'Rīgas 86. vidusskola', 'LV', 'RĪGA', 'ILŪKSTES IELA 10, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1082', '213.101.216.216', '2015-10-14 07:06:31', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900683', 0),
(30, 50, '1', 'Rīgas 64. vidusskola', 'LV', 'RĪGA', 'ŪNIJAS IELA 93, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1084', '213.101.217.150', '2015-10-14 08:29:07', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900756', 0),
(31, 51, '1', 'Rīgas 22. vidusskola', 'LV', 'RĪGA', 'BRUŅINIEKU IELA 10, CENTRA RAJONS, RĪGA, LV-1001', '213.101.216.232', '2015-10-14 10:46:00', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3413900799', 0),
(32, 52, '1', 'Rīgas Natālijas Draudziņas vidusskola Tērbatas iela 69', 'LV', 'RĪGA', 'TĒRBATAS IELA 69, CENTRA RAJONS, RĪGA, LV-1001', '213.101.217.8', '2015-10-19 15:25:27', '2017-06-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3413902866', 0),
(34, 54, '1', 'Rīgas Valsts 2. ģimnāzija', 'LV', 'RĪGA', 'KRIŠJĀŅA VALDEMĀRA IELA 1, ZIEMEĻU RAJONS, RĪGA, LV-1013', '213.101.217.4', '2015-10-20 06:57:14', '2017-04-12 05:01:21', 0, '', 1, 1, '', '', '', 0, '3919902869', 0),
(36, 56, '1', 'Rīgas Sergeja Žoltoka vidusskola', 'LV', 'RĪGA', 'VIETALVAS IELA 15, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1035', '213.101.217.6', '2015-10-20 09:29:55', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3613900665', 0),
(37, 57, '1', 'Š. Dubnova Rīgas Ebreju vidusskola', 'LV', 'RĪGA', 'MIERA IELA 62, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1013', '213.101.217.10', '2015-10-20 11:10:00', '2017-04-12 05:01:17', 0, '', 1, 1, '', '', '', 0, '3713900754', 0),
(38, 58, '1', 'Rīgas Ukraiņu vidusskola', 'LV', 'RĪGA', 'VISVALŽA IELA 4, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1050', '213.101.217.2', '2015-10-20 15:11:53', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3613900788', 0),
(39, 59, '1', 'Rīgas 15. vidusskola', 'LV', 'RĪGA', 'VISVALŽA IELA 9, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1050', '213.101.217.12', '2015-10-20 15:56:32', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900677', 0),
(40, 60, '1', 'Rīgas 51. vidusskola', 'LV', 'RĪGA', 'MASKAVAS IELA 262, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1063', '213.101.217.14', '2015-10-21 07:51:24', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900685', 0),
(41, 61, '1', 'Rīgas 92. vidusskola', 'LV', 'RĪGA', 'ULBROKAS IELA 3, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1021', '213.101.216.243', '2015-10-21 08:49:39', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900679', 0),
(42, 62, '1', 'Rīgas Pļavnieku pamatskola', 'LV', 'RĪGA', 'JĀŅA GRESTES IELA 14A, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1021', '213.101.216.246', '2015-10-21 09:41:54', '2017-04-12 05:01:21', 0, '', 1, 1, '', '', '', 0, '3612902865', 0),
(43, 63, '1', 'Rīgas 75. vidusskola', 'LV', 'RĪGA', 'OGRES IELA 9, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1019', '213.101.216.248', '2015-10-21 15:12:12', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900682', 0),
(44, 64, '1', 'Rīgas 65. vidusskola', 'LV', 'RĪGA', 'PRŪŠU IELA 32, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1057', '213.101.216.250', '2015-10-21 16:14:58', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900664', 0),
(45, 65, '1', 'Rīgas Ķengaraga vidusskola', 'LV', 'RĪGA', 'MASKAVAS IELA 273, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1063', '213.101.216.251', '2015-10-22 07:24:42', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900680', 0),
(46, 66, '1', 'Rīgas pilsētas Pļavnieku ģimnāzija', 'LV', 'RĪGA', 'ANDREJA SAHAROVA IELA 35, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1082', '213.101.216.252', '2015-10-22 09:12:35', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3616900675', 0),
(47, 67, '1', 'Rīgas Valsts vācu ģimnāzija', 'LV', 'RĪGA', 'ĀGENSKALNA IELA 21, KURZEMES RAJONS, RĪGA, LV-1048', '213.101.217.73', '2015-10-22 14:01:06', '2017-04-12 05:01:21', 0, '', 1, 1, '', '', '', 0, '3519900642', 0),
(48, 68, '1', 'Rīgas Anniņmuižas vidusskola', 'LV', 'RĪGA', 'KLEISTU IELA 14, KURZEMES RAJONS, RĪGA, LV-1067', '213.101.217.71', '2015-10-23 07:16:38', '2017-04-12 05:01:17', 0, '', 1, 1, '', '', '', 0, '3513900767', 0),
(49, 69, '1', 'Iļģuciema vidusskola', 'LV', 'RĪGA', 'DZIRCIEMA IELA 109, KURZEMES RAJONS, RĪGA, LV-1055', '213.101.216.254', '2015-10-23 10:02:50', '2016-09-02 03:15:07', 0, '', 1, 1, '', '', '', 0, '3513900770', 1),
(50, 70, '1', 'Rīgas Daugavgrīvas vidusskola', 'LV', 'RĪGA', 'PARĀDES IELA 5C, KURZEMES RAJONS, RĪGA, LV-1016', '213.101.217.60', '2015-10-26 10:32:08', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3513900766', 0),
(51, 71, '1', 'Rīgas Imantas vidusskola', 'LV', 'RĪGA', 'KURZEMES PROSPEKTS 158, KURZEMES RAJONS, RĪGA, LV-1029', '213.101.217.54', '2015-10-27 08:01:05', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900773', 0),
(52, 72, '1', 'Rīgas 46. vidusskola', 'LV', 'RĪGA', 'SKUJU IELA 28, ZIEMEĻU RAJONS, RĪGA, LV-1015', '213.101.217.52', '2015-10-27 14:33:45', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900827', 0),
(53, 73, '1', 'Rīgas 66. speciālā vidusskola', 'LV', 'RĪGA', 'KATRĪNAS IELA 4, ZIEMEĻU RAJONS, RĪGA, LV-1045', '213.101.217.56', '2015-10-28 09:28:28', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3925900830', 0),
(54, 74, '1', 'Rīgas Ostvalda vidusskola', 'LV', 'RĪGA', 'DAMMES IELA 20, KURZEMES RAJONS, RĪGA, LV-1067', '213.101.217.58', '2015-10-28 17:13:46', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900775', 0),
(55, 75, '1', 'Rīgas 19. vidusskola', 'LV', 'RĪGA', 'MIGLAS IELA 9, KURZEMES RAJONS, RĪGA, LV-1016', '213.101.217.50', '2015-10-29 07:29:25', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900771', 0),
(56, 76, '1', 'Rīgas 33. vidusskola', 'LV', 'RĪGA', 'STŪRMAŅU IELA 23, KURZEMES RAJONS, RĪGA, LV-1016', '213.101.217.48', '2015-10-29 08:16:19', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3513900836', 0),
(57, 77, '1', 'Rīgas 41. vidusskola', 'LV', 'RĪGA', 'SLOKAS IELA 49A, KURZEMES RAJONS, RĪGA, LV-1007', '213.101.217.47', '2015-10-29 09:25:08', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900768', 0),
(58, 78, '1', 'Rīgas 54. vidusskola', 'LV', 'RĪGA', 'BALTĀ IELA 22A, KURZEMES RAJONS, RĪGA, LV-1055', '213.101.217.62', '2015-10-29 11:50:54', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900764', 0),
(59, 79, '1', 'Rīgas 69. vidusskola', 'LV', 'RĪGA', 'IMANTAS IELA 11A, KURZEMES RAJONS, RĪGA, LV-1067', '213.101.217.64', '2015-10-29 13:12:53', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900774', 0),
(60, 80, '1', 'Rīgas 96. vidusskola', 'LV', 'RĪGA', 'AURU IELA 6A, KURZEMES RAJONS, RĪGA, LV-1069', '213.101.217.67', '2015-10-29 14:27:14', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3513900776', 0),
(61, 81, '1', 'Rīgas 71. vidusskola', 'LV', 'RĪGA', 'IĻĢUCIEMA IELA 6, KURZEMES RAJONS, RĪGA, LV-1055', '213.101.217.69', '2015-10-30 08:13:10', '2017-05-28 05:01:16', 0, '', 1, 1, '', '', '', 0, '3513900772', 0),
(62, 82, '1', 'Rīgas 71. vidusskola Grīvas 26', 'LV', 'RĪGA', 'IĻĢUCIEMA IELA 6, KURZEMES RAJONS, RĪGA, LV-1055', '10.10.10.10', '2015-10-30 08:37:17', '2017-05-28 05:01:16', 0, '', 1, 1, '', '', '', 0, '3513900772', 0),
(63, 83, '1', 'J.G.Herdera Rīgas Grīziņkalna vidusskola', 'LV', 'RĪGA', 'LAUKU IELA 9, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1009', '213.101.217.76', '2015-10-30 10:59:55', '2017-04-12 05:01:23', 0, '', 1, 1, '', '', '', 0, '3613903047', 0),
(64, 84, '1', 'Rīgas 25. vidusskola', 'LV', 'RĪGA', 'RUŠONU IELA 6, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1057', '213.101.217.78', '2015-11-02 08:58:29', '2017-04-12 05:01:14', 1, '', 1, 1, '', '', '', 0, '3613900678', 0),
(65, 85, '1', 'Andreja Pumpura Rīgas 11. pamatskola', 'LV', 'RĪGA', 'MASKAVAS IELA 197, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1019', '213.101.217.81', '2015-11-02 09:43:34', '2017-04-12 05:01:17', 0, '', 1, 1, '', '', '', 0, '3612900689', 0),
(66, 86, '1', 'O. Kalpaka Rīgas Tautas daiļamatu pamatskola', 'LV', 'RĪGA', 'SKRINDU IELA 1, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1003', '213.101.217.83', '2015-11-02 10:22:14', '2017-04-12 05:01:17', 0, '', 1, 1, '', '', '', 0, '3612900688', 0),
(67, 87, '1', 'Jankas Kupalas Rīgas Baltkrievu pamatskola', 'LV', 'RĪGA', 'ILŪKSTES IELA 10, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1082', '10.10.10.10', '2015-11-03 10:34:09', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3612900249', 0),
(68, 88, '1', 'Rīgas Austrumu vidusskola', 'LV', 'RĪGA', 'VIĻĀNU IELA 13, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1003', '213.100.161.71', '2015-11-03 11:48:58', '2017-04-12 05:01:24', 0, '', 1, 1, '', '', '', 0, '3613903082', 0),
(69, 89, '1', 'Rīgas Valda Avotiņa pamatskola - attīstības centrs', 'LV', 'RĪGA', 'SALASPILS IELA 14, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1057', '10.10.10.10', '2015-11-03 12:56:01', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3624900694', 0),
(70, 90, '1', 'Rīgas Ezerkrastu pamatskola', 'LV', 'RĪGA', 'MALIENAS IELA 89, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1064', '213.101.217.91', '2015-11-03 15:08:56', '2017-04-12 05:01:22', 0, '', 1, 1, '', '', '', 0, '3712902971', 0),
(71, 91, '1', 'Rīgas Franču licejs', 'LV', 'RĪGA', 'MĒNESS IELA 8, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1013', '213.101.217.103', '2015-11-04 08:21:05', '2016-09-01 03:15:07', 0, '', 1, 1, '', '', '', 0, '3713900740', 1),
(72, 92, '1', 'Rīgas Hanzas vidusskola', 'LV', 'RĪGA', 'GROSTONAS IELA 5, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1013', '213.101.217.93', '2015-11-04 11:39:21', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900741', 0),
(73, 93, '1', 'Rīgas Teikas vidusskola', 'LV', 'RĪGA', 'AIZKRAUKLES IELA 14, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1006', '213.101.217.95', '2015-11-06 07:54:55', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3713900746', 0),
(74, 94, '1', 'Rīgas 45. vidusskola', 'LV', 'RĪGA', 'ROPAŽU IELA 34, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1039', '213.101.217.97', '2015-11-06 11:29:35', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900742', 0),
(75, 95, '1', 'Rīgas 49. vidusskola', 'LV', 'RĪGA', 'KRIŠJĀŅA VALDEMĀRA IELA 65, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1010', '213.101.217.99', '2015-11-06 14:43:05', '2016-09-02 03:15:07', 0, '', 1, 1, '', '', '', 0, '3713900751', 1),
(76, 96, '1', 'Rīgas 63. vidusskola', 'LV', 'RĪGA', 'BALTEZERA IELA 6, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1024', '213.101.217.101', '2015-11-06 15:27:06', '2017-06-14 05:01:14', 0, '', 1, 1, '', '', '', 0, '3713900755', 1),
(77, 97, '1', 'Rīgas 63. vidusskola Pāles 9', 'LV', 'RĪGA', 'BALTEZERA IELA 6, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1024', '213.101.217.118', '2015-11-06 16:07:47', '2017-06-14 05:01:14', 0, '', 1, 1, '', '', '', 0, '3713900755', 1),
(78, 98, '1', 'Rīgas 80. vidusskola', 'LV', 'RĪGA', 'ANDROMEDAS GATVE 11, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1084', '213.101.217.116', '2015-11-09 07:47:36', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900753', 0),
(79, 99, '1', 'Rīgas 84. vidusskola', 'LV', 'RĪGA', 'LIELVĀRDES IELA 141, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1082', '213.101.217.114', '2015-11-10 08:22:13', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900735', 0),
(80, 100, '1', 'Rīgas 6. vidusskola Matīsa 37/39', 'LV', 'RĪGA', 'MATĪSA IELA 37/39, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1011', '10.10.10.10', '2015-11-10 11:06:06', '2017-04-12 05:01:14', 0, '', 1, 1, '', '', '', 0, '3613900684', 0),
(81, 101, '1', 'Ziemeļvalstu ģimnāzija', 'LV', 'RĪGA', 'ANNIŅMUIŽAS IELA 11, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1029', '213.101.217.112', '2015-11-11 09:14:23', '2017-06-09 05:01:14', 0, '', 1, 1, '', '', '', 0, '3816900700', 0),
(83, 103, '1', 'Friča Brīvzemnieka pamatskola', 'LV', 'RĪGA', 'ZEĻĻU IELA 4, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1002', '213.101.217.110', '2015-11-16 14:09:39', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3812900716', 0),
(84, 104, '1', 'Rīgas Kultūru vidusskola', 'LV', 'RĪGA', 'GANĪBU DAMBIS 7, ZIEMEĻU RAJONS, RĪGA, LV-1045', '213.101.217.105', '2015-11-16 14:52:58', '2017-04-12 05:01:18', 0, '', 1, 1, '', '', '', 0, '3913901617', 0),
(85, 105, '1', 'Rīgas Avotu pamatskola', 'LV', 'RĪGA', 'AVOTU IELA 44, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1009', '213.101.217.100', '2015-11-17 13:30:14', '2017-04-12 05:01:22', 0, '', 1, 1, '', '', '', 0, '3612902961', 0),
(86, 106, '1', 'Rīgas 28. vidusskola', 'LV', 'RĪGA', 'SLIEŽU IELA 23, ZIEMEĻU RAJONS, RĪGA, LV-1005', '213.101.217.129', '2015-11-19 07:27:37', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900823', 0),
(87, 107, '1', 'Rīgas 37. vidusskola', 'LV', 'RĪGA', 'ČIEKURKALNA 1. LĪNIJA 53, ZIEMEĻU RAJONS, RĪGA, LV-1026', '213.101.216.224', '2015-11-19 08:00:50', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900822', 0),
(88, 108, '1', 'Rīgas 7. pamatskola', 'LV', 'RĪGA', 'JAUNCIEMA 4. ŠĶĒRSLĪNIJA 4, ZIEMEĻU RAJONS, RĪGA, LV-1023', '213.101.217.132', '2015-11-19 08:44:57', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3912900813', 0),
(89, 109, '1', 'Rīgas 85. vidusskola', 'LV', 'RĪGA', 'NĪCGALES IELA 22, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1035', '213.101.217.134', '2015-11-19 09:25:16', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900738', 0),
(90, 110, '1', 'Rīgas 89. vidusskola', 'LV', 'RĪGA', 'HIPOKRĀTA IELA 27, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1079', '213.101.217.136', '2015-11-19 09:56:17', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3713900752', 0),
(91, 111, '1', 'Āgenskalna Valsts ģimnāzija', 'LV', 'RĪGA', 'LAVĪZES IELA 2A, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1002', '213.101.217.138', '2015-11-19 10:25:19', '2017-04-12 05:01:15', 1, '', 1, 1, '', '', '', 0, '3819900703', 0),
(92, 112, '1', 'Rīgas Igauņu pamatskola', 'LV', 'RĪGA', 'ATGĀZENES IELA 26, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1004', '213.101.217.140', '2015-11-19 10:54:43', '2017-04-12 05:01:21', 0, '', 1, 1, '', '', '', 0, '3812902864', 0),
(93, 113, '1', 'Rīgas Zolitūdes ģimnāzija', 'LV', 'RĪGA', 'RUSES IELA 22, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1029', '213.101.217.142', '2015-11-19 13:45:47', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3816900724', 0),
(94, 114, '1', 'Rīgas 47. vidusskola', 'LV', 'RĪGA', 'SKAISTKALNES IELA 7, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1004', '213.101.217.146', '2015-11-19 15:41:01', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3813900728', 0),
(95, 115, '1', 'Rīgas 47. vidusskola Ilmājas 14', 'LV', 'RĪGA', 'SKAISTKALNES IELA 7, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1004', '10.10.10.10', '2015-11-20 07:35:26', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3813900728', 0),
(96, 116, '1', 'Rīgas 53. vidusskola', 'LV', 'RĪGA', 'MELNSILA IELA 6, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1046', '213.101.217.148', '2015-11-20 09:06:39', '2017-06-09 05:01:14', 0, '', 1, 1, '', '', '', 0, '3813900725', 0),
(97, 117, '1', 'aa', 'LV', 'RĪGA', 'ŠAMPĒTERA 98, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1046', '10.10.10.10', '2015-11-30 08:14:27', '2016-09-01 07:18:59', 0, '', 0, 1, '', '', '', 0, '', 0),
(98, 118, '1', 'Rīgas 61. vidusskola', 'LV', 'RĪGA', 'PĀRSLAS IELA 14, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1002', '213.101.217.179', '2015-11-30 14:46:32', '2016-10-05 03:00:06', 0, '', 1, 1, '', '', '', 0, '3813900726', 0),
(99, 119, '1', 'Rīgas 94. vidusskola', 'LV', 'RĪGA', 'OZOLCIEMA IELA 26, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1058', '213.101.217.183', '2015-11-30 15:56:59', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3813900723', 0),
(100, 120, '1', 'Rīgas Pārdaugavas pamatskola', 'LV', 'RĪGA', 'KARTUPEĻU IELA 2, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1058', '213.101.217.185', '2015-12-01 14:37:51', '2017-04-12 05:01:15', 0, '', 1, 1, '', '', '', 0, '3812900704', 0),
(101, 121, '1', 'Puškina licejs', 'LV', 'RĪGA', 'SARKANDAUGAVAS IELA 22, ZIEMEĻU RAJONS, RĪGA, LV-1005', '213.101.217.187', '2015-12-02 09:53:19', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900821', 0),
(102, 122, '1', 'Rīgas 10. vidusskola', 'LV', 'RĪGA', 'LENČU IELA 1, ZIEMEĻU RAJONS, RĪGA, LV-1010', '213.101.217.189', '2015-12-02 10:44:27', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900824', 0),
(103, 123, '1', 'Rīgas 13. vidusskola', 'LV', 'RĪGA', 'PULKVEŽA BRIEŽA IELA 25, ZIEMEĻU RAJONS, RĪGA, LV-1010', '213.101.217.195', '2015-12-02 14:25:37', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900817', 0),
(104, 124, '1', 'Rīgas 29. vidusskola', 'LV', 'RĪGA', 'LĒDURGAS IELA 26, ZIEMEĻU RAJONS, RĪGA, LV-1034', '213.101.217.94', '2015-12-02 15:10:39', '2017-04-12 05:01:16', 0, '', 1, 1, '', '', '', 0, '3913900828', 0),
(105, 125, '1', 'Rīgas 95. vidusskola', 'LV', 'RĪGA', 'BRUKNAS IELA 5, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1058', '213.101.217.74', '2015-12-02 15:32:18', '2017-06-09 05:01:14', 0, '', 1, 1, '', '', '', 0, '3813900727', 0),
(109, 132, '1', 'Rīgas 40. vidusskola', 'LV', 'RĪGA', 'TĒRBATAS IELA 15/17, CENTRA RAJONS, RĪGA, LV-1011', '213.101.217.152', '2015-12-16 11:31:08', '2017-06-15 05:01:14', 0, '', 1, 0, '', '', '', 0, '3413900797', 0),
(110, 133, '1', 'Rīgas 24. pamatskola', 'LV', 'Rīga', 'Aleksandra Čaka iela 139', '10.10.10.10', '2015-12-16 14:29:11', '2016-01-14 10:38:52', 0, '', 1, 0, '', '', '', 0, '3412900785', 0),
(113, 136, '1', 'Rīgas vakara ģimnāzija', 'LV', 'RĪGA', 'MAZĀ MATĪSA IELA 3, LATGALES PRIEKŠPILSĒTA, RĪGA, LV-1009', '213.101.130.17', '2015-12-17 16:17:34', '2017-06-12 05:01:13', 0, '', 1, 0, '', '', '', 0, '3614900691', 0),
(116, 147, '1', 'Rīgas Juglas vidusskola', 'LV', 'RĪGA', 'KVĒLES IELA 64, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1064', '213.101.217.89', '2016-01-03 22:29:48', '2017-04-12 05:01:17', 0, '', 1, 0, '', '', '', 0, '3713900757', 0),
(117, 148, '1', 'Rīgas Jāņa Poruka vidusskola', 'LV', 'RĪGA', 'LĪBEKAS IELA 27, ZIEMEĻU RAJONS, RĪGA, LV-1014', '213.101.217.191', '2016-01-03 22:36:27', '2017-05-28 05:01:16', 0, '', 1, 0, '', '', '', 0, '3913900820', 0),
(118, 149, '1', 'Rīgas Raiņa 8. vakara (maiņu) vidusskola', 'LV', 'RĪGA', 'KRIŠJĀŅA BARONA IELA 71, CENTRA RAJONS, RĪGA, LV-1001', '10.10.10.10', '2016-01-03 22:39:35', '2017-04-12 05:01:16', 0, '', 1, 0, '', '', '', 0, '3414900800', 0),
(119, 230, '1', 'Angļu gimnāzija', 'LV', 'RĪGA', 'ZVĀRDES IELA 1, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1004', '213.101.217.130', '2016-01-11 08:41:00', '2017-04-12 05:01:15', 0, '', 1, 0, '', '', '', 0, '3816900699', 0),
(120, 231, '1', 'Rīgas 40. vidusskola Akas 10', 'LV', 'RĪGA', 'TĒRBATAS IELA 15/17, CENTRA RAJONS, RĪGA, LV-1011', '213.101.217.175', '2016-01-11 08:44:51', '2017-06-15 05:01:14', 0, '', 1, 0, '', '', '', 0, '3413900797', 0),
(121, 232, '1', 'Rīgas 95. vidusskola 2 korp', 'LV', 'RĪGA', 'BRUKNAS IELA 5, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1058', '213.101.217.87', '2016-01-11 09:49:03', '2017-06-09 05:01:14', 0, '', 1, 0, '', '', '', 0, '3813900727', 0),
(122, 265, '1', 'Pamatskola \"Rīdze\"', 'LV', 'RĪGA', 'KRIŠJĀŅA VALDEMĀRA IELA 2, CENTRA RAJONS, RĪGA, LV-1010', '212.93.110.156', '2016-02-05 05:36:59', '2016-09-02 03:15:10', 0, '', 1, 0, '', '', '', 0, '3412900804', 1),
(123, 479, '1', 'Rīgas 18. vakara maiņu skola', 'LV', 'RĪGA', 'SĪMAŅA IELA 14, ZIEMEĻU RAJONS, RĪGA, LV-1005', '213.101.217.193', '2016-02-15 09:34:51', '2017-04-12 05:01:16', 0, '', 1, 0, '', '', '', 0, '3914900832', 0),
(124, 497, '1', 'Rīgas 14. vakara (maiņu) vidusskolas', 'LV', 'RĪGA', 'MARGRIETAS IELA 4, ZEMGALES PRIEKŠPILSĒTA, RĪGA, LV-1046', '10.10.10.10', '2016-02-29 23:38:44', '2017-06-09 05:01:14', 0, '', 1, 0, '', '', '', 0, '3814900734', 0),
(125, 658, '1', 'Rīgas 49. vidusskola 2. korpuss', 'LV', 'RĪGA', 'KRIŠJĀŅA VALDEMĀRA IELA 65, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1010', '213.101.217.85', '2016-08-17 18:22:00', '2016-09-02 03:15:07', 0, '', 1, 0, '', '', '', 0, '3713900751', 1),
(126, 721, '1', 'Rīgas Jāņa Poruka vidusskola Gaujas 23 ', 'LV', 'RĪGA', 'LĪBEKAS IELA 27, ZIEMEĻU RAJONS, RĪGA, LV-1014', '213.101.217.108', '2016-08-31 12:15:45', '2017-05-28 05:01:16', 0, '', 1, 0, '', '', '', 0, '3913900820', 0),
(127, 899, '1', 'Rīgas Valsts 2. ģimnāzija (Kafeinica)', 'LV', 'RĪGA', 'KRIŠJĀŅA VALDEMĀRA IELA 1, ZIEMEĻU RAJONS, RĪGA, LV-1013', '213.101.130.2 ', '2016-09-12 07:55:45', '2017-04-12 05:01:21', 0, '', 1, 0, '', '', '', 0, '3919902869', 0),
(128, 1015, '1', 'Rīgas Teikas vidusskola (Pagrabs)	', 'LV', 'RĪGA', 'AIZKRAUKLES IELA 14, VIDZEMES PRIEKŠPILSĒTA, RĪGA, LV-1006', '213.101.217.144', '2016-10-14 09:21:15', '2017-04-12 05:01:14', 0, '', 1, 0, '', '', '', 0, '3713900746', 0),
(129, 1074, '1', 'Rīgas Valsts 1. ģimnāzija kafejnīca', 'LV', 'RĪGA', 'RAIŅA BULVĀRIS 8, CENTRA RAJONS, RĪGA, LV-1050', '213.100.163.55', '2016-12-07 15:12:50', '2017-04-12 05:01:16', 0, '', 1, 0, '', '', '', 0, '3419900793', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
