<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('settings')->delete();

        DB::table('settings')->insert(array('banned_ips' => '', 'import_path' => ''));

        DB::table('roles')->delete();

        DB::table('roles')->insert(array('id' => 1, 'name' => 'Administrators', 'permissions' => '', 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));
        DB::table('roles')->insert(array('id' => 2, 'name' => 'Objekts', 'permissions' => '', 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));
        DB::table('roles')->insert(array('id' => 3, 'name' => 'Tirgotajs', 'permissions' => '', 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));
        DB::table('roles')->insert(array('id' => 4, 'name' => 'Gramatvedis', 'permissions' => '', 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));
        DB::table('roles')->insert(array('id' => 5, 'name' => 'RK', 'permissions' => '', 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));
    }

}