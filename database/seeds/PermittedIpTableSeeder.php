<?php

use Illuminate\Database\Seeder;

class PermittedIpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permitted_ip')->insert(['ip' => '159.148.6.37']);
        DB::table('permitted_ip')->insert(['ip' => '90.142.249.152']);
        DB::table('permitted_ip')->insert(['ip' => '78.59.21.124']);
        DB::table('permitted_ip')->insert(['ip' => '78.59.213.175']);
        for ($i = 0; $i <= 254; $i++) {
            DB::table('permitted_ip')->insert(['ip' => '194.9.212.'.$i]);
        }
    }
}
