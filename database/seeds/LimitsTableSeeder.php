<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LimitsTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('eticket_limit_types')->delete();

        DB::table('eticket_limit_types')->insert(array('name' => 'Dienas','days' => '1'));
        DB::table('eticket_limit_types')->insert(array('name' => 'Nedēļas','days' => '7'));
        DB::table('eticket_limit_types')->insert(array('name' => 'Mēneša','days' => '30'));
    }

}