<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('eticket_statuses')->delete();

        DB::table('eticket_statuses')->insert(array('name' => 'Aktīvs','id' => '1'));
        DB::table('eticket_statuses')->insert(array('name' => 'Bloķēts','id' => '2'));
        DB::table('eticket_statuses')->insert(array('name' => 'Neapstiprināts','id' => '3'));
        DB::table('eticket_statuses')->insert(array('name' => 'Gaida uz apstiprināšanu','id' => '4'));
    }

}