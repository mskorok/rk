<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('accounts')->insert(array('id' => 1,'account_status' => '1', 'account_name' => 'future bank', 'account_type' => 0, 'balance' => 0, 'owner_id' => 0, 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));
        DB::table('accounts')->insert(array('id' => 2,'account_status' => '1', 'account_name' => 'free meals', 'account_type' => 0, 'balance' => 0, 'owner_id' => 0, 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));

        DB::table('owners')->insert(array('id' => 1,'owner_type' => '0', 'owner_name' => 'future bank owner', 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));
        DB::table('owners')->insert(array('id' => 2,'owner_type' => '0', 'owner_name' => 'free meals owner', 'created_at' => '0000-00-00', 'updated_at' => '0000-00-00'));
    }

}