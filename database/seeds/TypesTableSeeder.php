<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypesTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('tickets_types')->delete();

        DB::table('tickets_types')->insert(array('name' => 'Nav atrasts e-talons','id' => '1'));
        DB::table('tickets_types')->insert(array('name' => 'E-talons maina īpašnieku','id' => '2'));
        DB::table('tickets_types')->insert(array('name' => 'Naudas pārskaitījums','id' => '3'));
        DB::table('tickets_types')->insert(array('name' => 'Naudas atgriešana','id' => '4'));
        DB::table('tickets_types')->insert(array('name' => 'Profila datu apstiprinājums','id' => '5'));
    }

}