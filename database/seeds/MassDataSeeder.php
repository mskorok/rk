<?php

//use Illuminate\Database\Eloquent\Model;
//use Faker\Factory as Faker;
//use Illuminate\Database\Seeder;
//
//class MassDataSeeder extends Seeder {
//
//    public function run() {
//        Model::unguard();
//
//        $faker = Faker::create();
//
//        $all_account_ids = Account::lists('id');
//        $all_mpos_ids = Mpos::lists('id');
//        $all_object_ids = Objekts::lists('id');
//        $all_eticket_nrs = Eticket::lists('nr');
//        $all_user_ids = User::lists('id');
//        $all_owner_ids = Owner::lists('id');
//
//        foreach(range(1, 10000) as $index)
//        {
//            $pass = $faker->password . "A1";
//            User::create([
//                'password' => $pass,
//                'password_confirmation' => $pass,
//                'username' => $faker->firstName,
//                'surname' => $faker->lastName,
//                'email' => $faker->email,
//                'phone' => $faker->randomNumber(8, true),
//                'info' => $faker->word . " | " . $faker->word . " | " . $faker->word,
//                'pazime' => "0",
//                'confirmed' => "1",
//                'terms' => "1",
//                'pk' => $faker->randomNumber(6, true) . "-" . $faker->randomNumber(5, true)
//            ]);
//
//            Owner::create([
//                'owner_name' => $faker->name,
//                'owner_type' => "1"
//            ]);
//
//            OwnerAccess::create([
//                'user_id' => $faker->randomElement($all_user_ids),
//                'owner_id' => $faker->randomElement($all_owner_ids),
//                'access_status' => "1",
//                'access_level' => "1"
//            ]);
//
//            Account::create([
//                'owner_id' => $faker->randomElement($all_owner_ids),
//                'account_type' => "1",
//                'account_status' => "1",
//                'balance' => 0
//            ]);
//
//            AccountAccess::create([
//                'user_id' => $faker->randomElement($all_user_ids),
//                'account_id' => $faker->randomElement($all_account_ids),
//                'access_status' => "1",
//                'access_level' => "1"
//            ]);
//        }
//
//        foreach(range(1, 100000) as $index)
//        {
//            $e = new Eticket();
//            $e->nr   = $faker->randomNumber(5,true).$faker->randomNumber(5,true);
//            $e->account_id = $faker->randomElement($all_account_ids);
//            $e->eticket_status = "1";
//            $e->pk = $faker->randomNumber(6,true)."-".$faker->randomNumber(5,true);
//            $e->name  = $faker->name;
//            $e->grade   = $faker->randomNumber(1)."z";
//            if ( ! $e->save()) dd($e->getErrors());
//        }
//
//        foreach(range(1, 500000) as $index)
//        {
//            $dat = $faker->dateTimeThisYear;
//
//            Transaction::create([
//                'operation_id' => "1",
//                'mpos_id' => $faker->randomElement($all_mpos_ids),
//                'amount' => "10",
//                'from_account_id' => $faker->randomElement($all_account_ids),
//                'to_account_id' => $faker->randomElement($all_account_ids),
//                'from_eticket_nr' => $faker->randomElement($all_eticket_nrs),
//                'object_id' => $faker->randomElement($all_object_ids),
//                't_status' => "P",
//                'confirmed' => "2",
//                'created_at' => $dat,
//                'updated_at' => $dat
//            ]);
//        }
//    }
//
//}