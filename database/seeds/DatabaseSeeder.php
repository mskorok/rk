<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return voi
     */
	public function run()
	{
		Model::unguard();

		$this->call('SettingsTableSeeder');

		$this->command->info('Settings table seeded!');

		$this->call('LimitsTableSeeder');

		$this->command->info('Limits table seeded!');

		$this->call('StatusesTableSeeder');

		$this->command->info('Statuses table seeded!');

		$this->call('TypesTableSeeder');

		$this->command->info('Types table seeded!');

		$this->call('AccountsTableSeeder');

		$this->command->info('Accounts table seeded!');
	}

}