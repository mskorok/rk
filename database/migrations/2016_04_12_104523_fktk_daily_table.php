<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FktkDailyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($t)
		{
			$t->string('konts',50)->default("");
			$t->integer('rk_confirmed_user_id')->default(0);
			$t->timestamp('rk_confirmed')->default("0000-00-00");
		});

		Schema::create('money_daily_balance', function(Blueprint $t)
		{
			$t->increments('id');
            $t->date('datums')->default("0000-00-00");
            $t->integer('start_balance')->unsigned()->default('0');
            $t->integer('end_balance')->unsigned()->default('0');
            $t->integer('incoming')->unsigned()->default('0');
            $t->integer('outgoing')->default('0');
            $t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($t)
		{
			$t->dropColumn('konts');
			$t->dropColumn('rk_confirmed_user_id');
			$t->dropColumn('rk_confirmed');
		});

        Schema::drop('money_daily_balance');
	}

}
