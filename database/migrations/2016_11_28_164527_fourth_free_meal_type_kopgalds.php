<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class FourthFreeMealTypeKopgalds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('etickets', function($t)
        {
            $t->date('kopgalds_from')->default("0000-00-00");
            $t->date('kopgalds_until')->default("0000-00-00");
        });

        Schema::table('etickets_base', function($t)
        {
            $t->date('kopgalds_from')->default("0000-00-00");
            $t->date('kopgalds_until')->default("0000-00-00");
        });

        Schema::table('daily_log_zz', function($t)
        {
            $t->date('kopgalds_from')->default("0000-00-00");
            $t->date('kopgalds_until')->default("0000-00-00");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('etickets', function($t)
        {
            $t->dropColumn('kopgalds_from');
            $t->dropColumn('kopgalds_until');
        });

        Schema::table('etickets_base', function($t)
        {
            $t->dropColumn('kopgalds_from');
            $t->dropColumn('kopgalds_until');
        });

        Schema::table('daily_log_zz', function($t)
        {
            $t->dropColumn('kopgalds_from');
            $t->dropColumn('kopgalds_until');
        });
    }
}
