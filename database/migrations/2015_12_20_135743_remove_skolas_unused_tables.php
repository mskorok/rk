<?php

use Illuminate\Database\Migrations\Migration;

class RemoveSkolasUnusedTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('dinner_reports');
		Schema::drop('dinner_schedule');
		Schema::drop('object_dotations');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
