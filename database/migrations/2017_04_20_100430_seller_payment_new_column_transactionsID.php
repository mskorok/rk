<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SellerPaymentNewColumnTransactionsID extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_payout_ids', function(Blueprint $t)
        {
            $t->increments('id');
            $t->integer('trans_id')->unique();
            $t->integer('payout_trans_id');
            $t->integer('seller_id');
            $t->timestamps('created_at');
            
        });
        
        Schema::table('seller_payments', function($table) {
            $table->integer('complete')->default(0);
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seller_payout_ids');
        
        Schema::table('seller_payments', function($table) {
            $table->dropColumn('complete');
        });

    }
}
