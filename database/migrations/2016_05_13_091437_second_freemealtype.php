<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SecondFreemealtype extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets', function($t)
		{
			$t->date('second_meals_from')->default("0000-00-00");
			$t->date('second_meals_until')->default("0000-00-00");
		});

		Schema::table('etickets_base', function($t)
		{
			$t->date('second_meals_from')->default("0000-00-00");
			$t->date('second_meals_until')->default("0000-00-00");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets', function($t)
		{
			$t->dropColumn('second_meals_from');
			$t->dropColumn('second_meals_until');
		});

        Schema::table('etickets_base', function($t)
        {
            $t->dropColumn('second_meals_from');
            $t->dropColumn('second_meals_until');
        });
	}

}
