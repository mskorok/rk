<?php

use Illuminate\Database\Migrations\Migration;

class CreateEticketLimitTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Creates user operations log table
		Schema::create('eticket_limit_types', function($t)
		{
		    $t->increments('id');
		    $t->string('name',100);
		    $t->integer('days');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eticket_limit_types');
	}

}