<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DinnerReportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dinner_reports', function(Blueprint $t)
		{
			$t->increments('id');
			$t->integer('object_id')->unsigned()->index();
			$t->integer('dotation_id')->unsigned()->index();
			$t->date('diena');
			$t->string('grade',3)->default('');
			$t->integer('amount')->unsigned()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dinner_reports');
	}

}