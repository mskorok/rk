<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Creates user operations log table
		Schema::create('log_user', function($t)
		{
		    $t->increments('id');
		    $t->integer('user_id');
		    $t->string('op',100);
		    $t->string('value',255);
		    $t->string('ip',100);
		    $t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log_user');
	}

}