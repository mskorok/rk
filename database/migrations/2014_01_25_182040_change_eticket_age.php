<?php

use Illuminate\Database\Migrations\Migration;

class ChangeEticketAge extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets', function($t)
		{
			$t->dropColumn('age');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets', function($t)
		{
		    $t->smallInteger('age')->unsigned();
		});
	}

}