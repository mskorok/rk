<?php

use Illuminate\Database\Migrations\Migration;

class EticketThumbnail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets', function($t)
		{
		    $t->integer('thumb_id')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets', function($t)
		{
		    $t->dropColumn('thumb_id');
		});
	}

}