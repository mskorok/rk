<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('username');
            $table->string('surname');
            $table->string('pk');
            $table->string('email')->nullable();
            $table->integer('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('doc_type')->nullable();
            $table->string('doc_number')->nullable();
            $table->date('document_date')->nullable();
            $table->boolean('has_status')->default(false);
            $table->string('social_status')->nullable();
            $table->float('income_month')->nullable();
            $table->float('income_year')->nullable();
            $table->float('income_amount')->nullable();
            $table->float('outgoing_month')->nullable();
            $table->float('outgoing_year')->nullable();
            $table->float('outgoing_amount')->nullable();
            $table->boolean('is_manager')->default(true);
            $table->string('manager_status')->nullable();
            $table->boolean('is_owner')->default(true);
            $table->string('owner_status')->nullable();
            $table->boolean('is_accept_terms')->default(false);
            $table->string('doc_copy')->nullable();
            $table->string('doc_copy_path')->nullable();
            $table->string('doc_copy_image_id')->nullable();
            $table->string('avatar')->nullable();
            $table->string('avatar_path')->nullable();
            $table->string('avatar_image_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile');
    }
}
