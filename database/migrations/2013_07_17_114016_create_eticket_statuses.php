<?php

use Illuminate\Database\Migrations\Migration;

class CreateEticketStatuses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eticket_statuses', function($t)
		{
		    $t->increments('id');
		    $t->string('name',100);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eticket_statuses');
	}

}