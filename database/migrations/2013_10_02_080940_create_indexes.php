<?php

use Illuminate\Database\Migrations\Migration;

class CreateIndexes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($t)
		{
		    $t->index('email');
		    $t->index('confirmed');
		    $t->index('deleted_at');
		});

		Schema::table('transactions', function($t)
		{
		    $t->index('object_id');
		    $t->index('mpos_id');
		    $t->index('authorisator_id');
		});

		Schema::table('tickets', function($t)
		{
		    $t->index('user_id');
		    $t->index('recipient_id');
		});

		Schema::table('sellers_access', function($t)
		{
		    $t->index('seller_id');
		    $t->index('user_id');
		});

		Schema::table('owner_access', function($t)
		{
		    $t->index('user_id');
		    $t->index('owner_id');
		    $t->index('access_status');
		    $t->index('access_level');
		    $t->index('deleted_at');
		});

		Schema::table('object_dotations', function($t)
		{
		    $t->index('object_id');
		});

		Schema::table('mpos', function($t)
		{
		    $t->index('object_id');
		    $t->index('owner_id');
		});

		Schema::table('log_user', function($t)
		{
		    $t->index('user_id');
		});

		Schema::table('etickets_sync', function($t)
		{
		    $t->index('authorisator_id');
		});

		Schema::table('etickets', function($t)
		{
		    $t->index('group_id');
		    $t->index('eticket_status');
		});

		Schema::table('eticket_objects', function($t)
		{
		    $t->index('eticket_id');
		});

		Schema::table('accounts', function($t)
		{
		    $t->index('owner_id');
		    $t->index('account_type');
		    $t->index('account_status');
		});

		Schema::table('account_access', function($t)
		{
		    $t->index('user_id');
		    $t->index('access_level');
		    $t->index('access_status');
		    $t->index('deleted_at');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($t)
		{
		    $t->dropIndex('users_email_index');
		    $t->dropIndex('users_confirmed_index');
		    $t->dropIndex('users_deleted_at_index');
		});

		Schema::table('transactions', function($t)
		{
		    $t->dropIndex('transactions_object_id_index');
		    $t->dropIndex('transactions_mpos_id_index');
		    $t->dropIndex('transactions_authorisator_id_index');
		});

		Schema::table('tickets', function($t)
		{
		    $t->dropIndex('tickets_user_id_index');
		    $t->dropIndex('tickets_recipient_id_index');
		});

		Schema::table('sellers_access', function($t)
		{
		    $t->dropIndex('sellers_access_seller_id_index');
		    $t->dropIndex('sellers_access_user_id_index');
		});

		Schema::table('owner_access', function($t)
		{
		    $t->dropIndex('owner_access_user_id_index');
		    $t->dropIndex('owner_access_owner_id_index');
		    $t->dropIndex('owner_access_access_status_index');
		    $t->dropIndex('owner_access_access_level_index');
		    $t->dropIndex('owner_access_deleted_at_index');
		});

		Schema::table('object_dotations', function($t)
		{
		    $t->dropIndex('object_dotations_object_id_index');
		});

		Schema::table('mpos', function($t)
		{
		    $t->dropIndex('mpos_object_id_index');
		    $t->dropIndex('mpos_owner_id_index');
		});

		Schema::table('log_user', function($t)
		{
		    $t->dropIndex('log_user_user_id_index');
		});

		Schema::table('etickets_sync', function($t)
		{
		    $t->dropIndex('etickets_sync_authorisator_id_index');
		});

		Schema::table('etickets', function($t)
		{
		    $t->dropIndex('etickets_group_id_index');
		    $t->dropIndex('etickets_eticket_status_index');
		});

		Schema::table('eticket_objects', function($t)
		{
		    $t->dropIndex('eticket_objects_eticket_id_index');
		});

		Schema::table('accounts', function($t)
		{
		    $t->dropIndex('accounts_owner_id_index');
		    $t->dropIndex('accounts_account_type_index');
		    $t->dropIndex('accounts_account_status_index');
		});

		Schema::table('account_access', function($t)
		{
		    $t->dropIndex('account_access_user_id_index');
		    $t->dropIndex('account_access_access_status_index');
		    $t->dropIndex('account_access_access_level_index');
		    $t->dropIndex('account_access_deleted_at_index');
		});
	}

}