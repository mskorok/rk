<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class ExpandDbLogFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('daily_log_zz', function($t)
        {
            $t->string('name',100)->default("");
            $t->string('grade',4)->default("");
            $t->integer('school_id')->default(0);
            $t->integer('age')->default(0);
            $t->date('free_meals_from')->default("0000-00-00");
            $t->date('free_meals_until')->default("0000-00-00");
            $t->date('second_meals_from')->default("0000-00-00");
            $t->date('second_meals_until')->default("0000-00-00");
            $t->date('third_meals_from')->default("0000-00-00");
            $t->date('third_meals_until')->default("0000-00-00");
            $t->boolean("riga")->default(false);
            $t->boolean("alive")->default(false);
        });

        Schema::table('daily_log_rk', function($t)
        {
            $t->string('nr',12)->default("");
            $t->date('expires')->default("0000-00-00");
            $t->boolean("blacklisted")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_log_zz', function($t)
        {
            $t->dropColumn('name');
            $t->dropColumn('grade');
            $t->dropColumn('school_id');
            $t->dropColumn('age');
            $t->dropColumn('free_meals_from');
            $t->dropColumn('free_meals_until');
            $t->dropColumn('second_meals_from');
            $t->dropColumn('second_meals_until');
            $t->dropColumn('third_meals_from');
            $t->dropColumn('third_meals_until');
            $t->dropColumn('riga');
            $t->dropColumn('alive');
        });

        Schema::table('daily_log_rk', function($t)
        {
            $t->dropColumn('nr');
            $t->dropColumn('expires');
            $t->dropColumn('blacklisted');
        });
    }
}
