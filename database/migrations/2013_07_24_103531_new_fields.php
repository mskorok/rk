<?php

use Illuminate\Database\Migrations\Migration;

class NewFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('authorisators', function($t)
		{
		    $t->string('secret_key',255)->default('SECRETKEY');
		    $t->bigInteger('sync_session')->default(0);
		});

		Schema::table('etickets', function($t)
		{
		    $t->integer('sync_state')->default(0);
		});

		Schema::table('mpos', function($t)
		{
		    $t->string('secret_key',255)->default('SECRETKEY');
		    $t->string('reboot_flag',1)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('authorisators', function($t)
		{
		    $t->dropColumn('secret_key','sync_session');
		});

		Schema::table('etickets', function($t)
		{
		    $t->dropColumn('sync_state');
		});

		Schema::table('mpos', function($t)
		{
		    $t->dropColumn('secret_key','reboot_flag');
		});
	}

}