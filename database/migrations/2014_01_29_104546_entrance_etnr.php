<?php

use Illuminate\Database\Migrations\Migration;

class EntranceEtnr extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('entrance_log', function($t)
		{
		    $t->string('eticket_nr',10)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('entrance_log', function($t)
		{
		    $t->dropColumn('eticket_nr');
		});
	}

}