<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Autorizatoriemdec15 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('authorisators', function($t)
		{
			$t->timestamp('last_sync')->default("2000-01-01 00:00:00");
			$t->timestamp('last_sync_full')->default("2000-01-01 00:00:00");
		});

		Schema::table('dotations', function($t)
		{
			$t->date('from_date')->after("last_used")->default("0000-00-00");
			$t->integer('avail')->default(0);
			$t->date('import_date')->default("0000-00-00");
		});

        Schema::table('object_dotation_prices', function($t)
        {
            $t->string('dotation_type',1)->default("O"); // "O" - once a day; "D" - discount; "B" - bonus points;
            $t->string('ex_code',10)->nullable();
            $t->integer('importer_id')->nullable();
        });

        Schema::table('transactions', function($t)
        {
            $t->integer('dotation_id')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('authorisators', function($t)
		{
			$t->dropColumn('last_sync');
			$t->dropColumn('last_sync_full');
		});

        Schema::table('dotations', function($t)
        {
            $t->dropColumn('from_date');
            $t->dropColumn('avail');
            $t->dropColumn('import_date');
        });

        Schema::table('object_dotation_prices', function($t)
        {
            $t->dropColumn('dotation_type');
            $t->dropColumn('ex_code');
            $t->dropColumn('importer_id');
        });

        Schema::table('transactions', function($t)
        {
            $t->dropColumn('dotation_id');
        });
	}

}
