<?php

use Illuminate\Database\Migrations\Migration;

class CreateSyncTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eticket_objects', function($t)
		{
		    $t->increments('id');
		    $t->integer('eticket_id');
		    $t->integer('object_id');
		});

		Schema::create('etickets_sync', function($t)
		{
		    $t->increments('record_id');
		    $t->integer('eticket_id')->unsigend()->index();
		    $t->integer('authorisator_id');
		    $t->string('record_state',1)->default("n");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eticket_objects');
		Schema::drop('etcikets_sync');
	}

}