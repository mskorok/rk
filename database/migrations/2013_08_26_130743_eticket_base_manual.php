<?php

use Illuminate\Database\Migrations\Migration;

class EticketBaseManual extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets_base', function($t)
		{
		    $t->smallInteger('custom')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets_base', function($t)
		{
		    $t->dropColumn('custom');
		});
	}

}