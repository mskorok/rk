<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('i')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->unsignedInteger('child_photo')->nullable();
            $table->unsignedInteger('school_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('class')->nullable();
            $table->unsignedInteger('card_1')->default(0);
            $table->unsignedInteger('card_2')->default(0);
            $table->unsignedInteger('card_3')->default(0);
            $table->unsignedInteger('card_4')->default(0);
            $table->unsignedInteger('card_5')->default(0);
            $table->unsignedInteger('card_6')->default(0);
            $table->unsignedInteger('card_7')->default(0);
            $table->unsignedInteger('card_8')->default(0);
            $table->unsignedInteger('card_9')->default(0);
            $table->unsignedInteger('card_10')->default(0);
            $table->unsignedInteger('pk_1')->default(0);
            $table->unsignedInteger('pk_2')->default(0);
            $table->unsignedInteger('pk_3')->default(0);
            $table->unsignedInteger('pk_4')->default(0);
            $table->unsignedInteger('pk_5')->default(0);
            $table->unsignedInteger('pk_6')->default(0);
            $table->unsignedInteger('pk_7')->default(0);
            $table->unsignedInteger('pk_8')->default(0);
            $table->unsignedInteger('pk_9')->default(0);
            $table->unsignedInteger('pk_10')->default(0);
            $table->unsignedInteger('pk_11')->default(0);
            $table->enum('limits', ['day', 'week', 'month'])->default('day');
            $table->float('amount')->default(0);
            $table->float('top_up_sum')->default(0);
            $table->string('payment_code')->nullable();
            $table->unsignedInteger('payment_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cards');
    }
}
