<?php

use Illuminate\Database\Migrations\Migration;

class ObjectTurnicetParams extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('objects', function($t)
		{
		    $t->string('entrance_ip',50)->unsigend()->default('');
		    $t->string('sync_path',255)->default('');
		    $t->smallInteger('sync_enable')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('objects', function($t)
		{
		    $t->dropColumn('entrance_ip');
		    $t->dropColumn('sync_path');
		    $t->dropColumn('sync_enable');
		});
	}

}