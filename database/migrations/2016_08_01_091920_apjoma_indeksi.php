<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApjomaIndeksi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets', function($t)
		{
			$t->index('pk');
			$t->index('account_id');
		});

		Schema::table('objects_ogroups', function($t)
		{
			$t->index('ogroup_id');
		});

        Schema::table('transactions', function($t)
        {
            $t->index('from_account_id');
            $t->index('from_eticket_nr');
            $t->index('t_status');
            $t->index('freemeals_type');
        });

        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets', function($t)
		{
			$t->dropIndex('etickets_pk_index');
			$t->dropIndex('etickets_account_id_index');
		});

        Schema::table('objects_ogroups', function($t)
        {
            $t->dropIndex('objects_ogroups_ogroup_id_index');
        });

        Schema::table('transactions', function($t)
        {
            $t->dropIndex('transactions_from_account_id_index');
            $t->dropIndex('transactions_from_eticket_nr_index');
            $t->dropIndex('transactions_t_status_index');
            $t->dropIndex('transactions_freemeals_type_index');
        });
	}

}
