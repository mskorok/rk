<?php

use Illuminate\Database\Migrations\Migration;

class AddUserLoginData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($t)
		{
		    $t->timestamp('last_login')->default('0000-00-00 00:00:00');
		    $t->integer('login_count')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($t)
		{
		    $t->dropColumn('last_login','login_count');
		});
	}

}