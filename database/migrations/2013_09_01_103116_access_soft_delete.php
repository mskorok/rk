<?php

use Illuminate\Database\Migrations\Migration;

class AccessSoftDelete extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('owner_access', function($t)
		{
		    $t->softDeletes();
		});

		Schema::table('account_access', function($t)
		{
		    $t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('owner_access', function($t)
		{
		    $t->dropColumn('deleted_at');
		});

		Schema::table('account_access', function($t)
		{
		    $t->dropColumn('deleted_at');
		});
	}

}