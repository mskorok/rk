<?php

use Illuminate\Database\Migrations\Migration;

class CreateEticketsBase extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('etickets_base', function($t)
		{
		    $t->increments('id');
		    $t->string('pk',12)->unique();
		    $t->string('etickets',255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('etickets_base');
	}

}