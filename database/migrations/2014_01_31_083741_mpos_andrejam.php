<?php

use Illuminate\Database\Migrations\Migration;

class MposAndrejam extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mpos', function($t)
		{
		    $t->string('mpos_serial',100)->default('NEWMPOS')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mpos', function($t)
		{
		    $t->dropColumn('mpos_serial');
		});
	}

}