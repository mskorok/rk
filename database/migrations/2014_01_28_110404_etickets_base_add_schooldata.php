<?php

use Illuminate\Database\Migrations\Migration;

class EticketsBaseAddSchooldata extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets_base', function($t)
		{
		    $t->string('name',100)->default('');
		    $t->string('surname',50)->default('');
		    $t->string('grade',3)->default('');
		    $t->integer('object_id')->unsigned()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets_base', function($t)
		{
		    $t->dropColumn('name');
		    $t->dropColumn('surname');
		    $t->dropColumn('grade');
		    $t->dropColumn('object_id');
		});
	}

}