<?php

use Illuminate\Database\Migrations\Migration;

class ObjektaKods extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('objects', function($t)
		{
		    $t->integer('kods')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('objects', function($t)
		{
		    $t->dropColumn('kods');
		});
	}

}