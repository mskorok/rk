<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BrivpusdienuDatiem extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets_base', function($t)
		{
			$t->integer('brivpusdienas')->unsigned()->default(0);
			$t->integer('school_id')->unsigned()->default(0);
			$t->date('free_meals_from')->default("0000-00-00");
			$t->date('free_meals_until')->default("0000-00-00");
		});

        Schema::table('etickets', function($t)
        {
            $t->date('free_meals_from')->default("0000-00-00");
            $t->date('free_meals_until')->default("0000-00-00");
        });

        Schema::create('schools', function(Blueprint $t)
        {
            $t->integer('id')->unsigned()->default(0);
            $t->primary('id');
            $t->string('heading',255)->default('');
            $t->string('reg_nr',20)->default('');
            $t->date('changed');
            $t->integer('status')->default('1')->index();
            $t->float('equal_price')->default(0);
            $t->timestamps();
        });

        Schema::create('schools_prices', function(Blueprint $t)
        {
            $t->increments('id');
            $t->integer('school_id')->unsigned()->default('0')->index();
            $t->integer('object_id')->unsigned()->default('0')->index();
            $t->integer('grade')->unsigned()->default(0);
            $t->decimal('price')->default(0);
            $t->boolean('vat')->default(false);
            $t->string('regnr',20)->default('');
            $t->timestamps();
        });

        Schema::table('manual_payments', function($t)
        {
            $t->string('payment_code',20)->default("");
        });

        Schema::table('users', function($t)
        {
            $t->string('pk',12)->default("");
        });

        Schema::table('objects', function($t)
        {
            $t->string('object_regnr',20)->default('');
            $t->integer('vat')->default(0);
        });

        Schema::create('sold_free_meals', function(Blueprint $t)
        {
            $t->integer('object_id')->default(0)->index();
            $t->integer('mpos_id')->default(0)->index();
            $t->integer('seller_id')->default(0)->index();
            $t->string('pk',12)->default("");
            $t->string('eticket',10)->default("");
            $t->integer('amount')->unsigned()->default(0);
            $t->integer('from_account_id')->default(0)->index();
            $t->integer('to_account_id')->default(0)->index();
            $t->integer('transaction_id')->default(0);
            $t->timestamps();
        });

        Schema::table('dotations', function(Blueprint $t)
        {
            $t->string('pk',12)->default("");
            $t->integer('dotation_id')->default(0);
            $t->integer('transaction_id')->default(0);
            $t->date('last_used')->default("0000-00-00");
            $t->date('until_date')->default("0000-00-00");
        });

        Schema::create('object_dotation_prices', function(Blueprint $t)
        {
            $t->increments('id');
            $t->string('heading',255)->default("");
            $t->integer('object_id')->default(0)->index();
            $t->integer('seller_account_id')->default(0)->index();
            $t->integer('price')->default(0);
            $t->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets_base', function($t)
		{
            $t->dropColumn('brivpusdienas');
		});

        Schema::drop('schools');
        Schema::drop('schools_prices');
        Schema::drop('sold_free_meals');
        Schema::drop('object_dotation_prices');

        Schema::table('manual_payments', function($t)
        {
            $t->dropColumn('payment_code');
        });

        Schema::table('users', function($t)
        {
            $t->dropColumn('pk');
        });

        Schema::table('objects', function($t)
        {
            $t->dropColumn('object_regnr');
            $t->dropColumn('vat');
        });

        Schema::table('dotations', function($t)
        {
            $t->dropColumn('pk');
            $t->dropColumn('dotation_id');
            $t->dropColumn('transaction_id');
            $t->dropColumn('last_used');
            $t->dropColumn('until_date');

            $t->increments('id');
        });

        Schema::table('etickets', function($t)
        {
            $t->dropColumn('free_meals_from');
            $t->dropColumn('free_meals_until');
        });
	}

}
