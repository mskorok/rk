<?php

use Illuminate\Database\Migrations\Migration;

class AddObjectShow extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('objects', function($t)
		{
		    $t->smallInteger('show')->unsigned()->default('0')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('objects', function($t)
		{
		    $t->dropColumn('show');
		});
	}

}