<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingsNewFieldsYearlyRenew extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('settings', function($t)
		{
			$t->string('yearly_text',255)->default("");
			$t->date('yearly_date')->default("0000-00-00");
		});

		Schema::table('users', function($t)
		{
			$t->string('yearly_status',1)->default("N");
			$t->date('yearly_updated')->default("0000-00-00");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('settings', function($t)
		{
			$t->dropColumn('yearly_text');
			$t->dropColumn('yearly_date');
		});

        Schema::table('users', function($t)
        {
            $t->dropColumn('yearly_status');
            $t->dropColumn('yearly_updated');
        });
	}

}
