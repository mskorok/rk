<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThirdDotationType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets', function($t)
		{
			$t->date('third_meals_from')->default("0000-00-00");
			$t->date('third_meals_until')->default("0000-00-00");
		});

		Schema::table('etickets_base', function($t)
		{
			$t->date('third_meals_from')->default("0000-00-00");
			$t->date('third_meals_until')->default("0000-00-00");
		});

		Schema::table('transactions', function($t)
		{
			$t->smallInteger('freemeals_type')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets', function($t)
		{
			$t->dropColumn('third_meals_from');
			$t->dropColumn('third_meals_until');
		});

		Schema::table('etickets_base', function($t)
		{
			$t->dropColumn('third_meals_from');
			$t->dropColumn('third_meals_until');
		});

        Schema::table('transactions', function($t)
        {
            $t->dropColumn('freemeals_type');
        });
	}

}
