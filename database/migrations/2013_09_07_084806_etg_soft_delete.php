<?php

use Illuminate\Database\Migrations\Migration;

class EtgSoftDelete extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('eticket_groups', function($t)
		{
		    $t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('eticket_groups', function($t)
		{
		    $t->dropColumn('deleted_at');
		});
	}

}