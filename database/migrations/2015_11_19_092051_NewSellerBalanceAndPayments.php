<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewSellerBalanceAndPayments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('seller_daily_balance', function($t)
		{
			$t->text('mpos_account_amounts')->default('');
		});

		Schema::create('seller_payments', function(Blueprint $t)
		{
			$t->increments('id');
			$t->integer('seller_id')->unsigned()->default('0')->index();
			$t->integer('transaction_id')->unsigned()->default('0');
			$t->integer('amount')->default('0');
			$t->string('period_code',20)->default('');
			$t->integer('status')->default('0')->index();
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('seller_daily_balance', function($t)
		{
			$t->dropColumn('mpos_account_amounts');
		});

        Schema::drop('seller_payments');
	}

}
