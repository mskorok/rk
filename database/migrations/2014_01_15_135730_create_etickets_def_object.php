<?php

use Illuminate\Database\Migrations\Migration;

class CreateEticketsDefObject extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('eticket_objects', function($t)
		{
		    $t->smallInteger('pirma')->unsigned()->default('0')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('eticket_objects', function($t)
		{
		    $t->dropColumn('pirma');
		});
	}

}