<?php

use Illuminate\Database\Migrations\Migration;

class CreateEticketGroups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eticket_groups', function($t)
		{
		    $t->increments('id');
		    $t->string('group_name',255);
		    $t->integer('limit_type');
		    $t->integer('limit');
		    $t->integer('balance');
		    $t->integer('account_id');
		    $t->timestamps();
		});

		Schema::table('etickets', function($t)
		{
		    $t->integer('group_id')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eticket_groups');

		Schema::table('etickets', function($t)
		{
		    $t->dropColumn('group_id');
		});
	}

}