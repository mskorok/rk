<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trans_id');
            $table->unsignedInteger('amount');
            $table->unsignedInteger('currency');
            $table->string('client_ip_addr');
            $table->text('description');
            $table->string('language');
            $table->string('dms_ok');
            $table->string('result');
            $table->string('result_code');
            $table->string('result_3dsecure');
            $table->string('card_number');
            $table->string('t_date');
            $table->text('response');
            $table->unsignedInteger('reversal_amount');
            $table->unsignedInteger('makeDMS_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction');
    }
}
