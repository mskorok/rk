<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Eticketsbaseauthoradminlog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('log_admin', function(Blueprint $t)
		{
			$t->increments('id');
			$t->integer('user_id')->unsigned()->index();
			$t->string('op',50)->default('');
			$t->string('value',255)->default('');
			$t->string('ip',50)->default('');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log_admin');
	}

}
