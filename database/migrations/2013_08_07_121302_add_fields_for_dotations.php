<?php

use Illuminate\Database\Migrations\Migration;

class AddFieldsForDotations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets', function($t)
		{
		    $t->smallInteger('age')->default(0);
		});

		Schema::table('objects', function($t)
		{
		    $t->smallInteger('allow_d')->default(0);
		    $t->string('age')->default('');
		});

		Schema::create('dotations', function($t)
		{
		    $t->increments('id');
		});

		Schema::create('object_dotations', function($t)
		{
		    $t->increments('id');
		    $t->integer('object_id');
		    $t->integer('dotation_id');
		    $t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets', function($t)
		{
		    $t->dropColumn('age');
		});

		Schema::table('objects', function($t)
		{
		    $t->dropColumn('allow_d','age');
		});

		Schema::drop('dotations');
		Schema::drop('object_dotations');
	}

}