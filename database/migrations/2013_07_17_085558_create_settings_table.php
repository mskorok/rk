<?php

use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Creates sellers table
		Schema::create('settings', function($t)
		{
		    $t->increments('id');
		    $t->string('lang',2)->default('lv');
		    $t->text('banned_ips');
		    $t->integer('wrong_password')->default(5);
		    $t->string('import_path');
		    $t->boolean('lvl')->default(true);
		    $t->string('confirm_type',20)->default('base_table');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}