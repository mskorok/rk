<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SellerDailyBalance extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('seller_daily_balance', function(Blueprint $t)
        {
            $t->increments('id');
            $t->integer('seller_id')->unsigned()->default(0)->index();
            $t->date('datums');
            $t->integer('balance')->unsigned()->default('0');
            $t->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('seller_daily_balance');
	}

}
