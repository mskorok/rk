<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionAdRegnr extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function($t)
		{
			$t->string('seller_reg_nr',12)->default("");
		});

		Schema::table('users', function($t)
		{
			$t->string('dokuments',10)->default("");
		});

        Schema::table('settings', function($t)
        {
            $t->string('dokuments_text',255)->default("");
        });

		Schema::table('seller_daily_balance', function($t)
		{
			$t->integer('paid_out')->default(0);
			$t->integer('free_out')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function($t)
		{
			$t->dropColumn('seller_reg_nr');
		});

        Schema::table('users', function($t)
        {
            $t->dropColumn('dokuments');
        });

        Schema::table('settings', function($t)
        {
            $t->dropColumn('dokuments_text');
        });

        Schema::table('seller_daily_balance', function($t)
        {
            $t->dropColumn('paid_out');
            $t->dropColumn('free_out');
        });
	}

}
