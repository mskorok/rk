<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEntranceData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entrance_log', function(Blueprint $t)
		{
			$t->increments('id');
			$t->integer('object_id')->unsigned();
			$t->string('pk',12)->default('');
			$t->string('virziens',1)->default('0');
			$t->integer('turnicet_id')->unsigned();
			$t->date('datums');
			$t->timestamp('laiks');
			$t->integer('valid_id')->unsigend();
			$t->string('status',50)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entrance_log');
	}

}