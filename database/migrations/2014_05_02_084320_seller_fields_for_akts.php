<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SellerFieldsForAkts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('sellers', function($t)
        {
            $t->string('contract_nr',50)->default('')->nullable();
            $t->string('contract_person',100)->default('')->nullable();
        });

        Schema::create('akti', function(Blueprint $t)
        {
            $t->increments('id');
            $t->integer('seller_id')->unsigned()->default('0')->index();
            $t->date('datums_no');
            $t->date('datums_lidz');
            $t->integer('akts_nr')->unsigned()->default('0');
            $t->string('akts_nrt',50)->default('');
            $t->integer('user_id')->unsigned()->default('0')->index();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sellers', function($t)
        {
            $t->dropColumn('contract_nr');
            $t->dropColumn('contract_person');
        });

        Schema::drop('akti');
    }

}
