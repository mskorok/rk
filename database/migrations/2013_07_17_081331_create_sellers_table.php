<?php

use Illuminate\Database\Migrations\Migration;

class CreateSellersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// rename columnt in object_access
		Schema::table('object_access', function($table)
		{
		    $table->renameColumn('owner_id', 'user_id');
		});

		// Creates sellers table
		Schema::create('sellers', function($t)
		{
		    $t->increments('id');
		    $t->integer('owner_id');
		    $t->string('seller_name');
		    $t->timestamps();
		});

		// Creates sellers access table
		Schema::create('sellers_access', function($t)
		{
		    $t->increments('id');
		    $t->integer('seller_id');
		    $t->integer('user_id');
		    $t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sellers');
		Schema::drop('sellers_access');
	}

}