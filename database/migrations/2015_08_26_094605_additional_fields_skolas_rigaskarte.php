<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalFieldsSkolasRigaskarte extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('settings', function($t)
		{
			$t->string('payment_form_message',255)->default('');
			$t->string('payment_form_details',255)->default('');
		});

		Schema::create('manual_payments', function(Blueprint $t)
		{
			$t->increments('id');
			$t->integer('user_id')->unsigned()->default('0')->index();
			$t->integer('eticket_id')->unsigned()->default('0')->index();
			$t->integer('user_amount')->default('0');
            $t->integer('amount')->unsigned()->default('0');
			$t->integer('confirmed')->default('0')->index();
			$t->datetime('confirmed_date');
			$t->integer('accountant_id')->unsigned()->default('0')->index();
            $t->integer('transaction_id')->unsigned()->default('0');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('settings', function($t)
		{
			$t->dropColumn('payment_form_message');
			$t->dropColumn('payment_form_details');
		});

		Schema::drop('manual_payments');
	}

}
