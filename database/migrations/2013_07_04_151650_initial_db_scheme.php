<?php

use Illuminate\Database\Migrations\Migration;

class InitialDbScheme extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	       Schema::create('users', function($table)
	    {
	        $table->increments('id');
	        $table->string('user_type',1)->default('');
	        $table->string('user_status',1)->default('');
	        $table->string('password')->default('');
	        $table->string('hash_type',20)->default('');
	        $table->string('username')->default('');
	        $table->string('surname')->default('');
	        $table->string('email')->unique();
	        $table->string('phone',255)->default('');
			$table->string('confirmation_code')->default('');
			$table->boolean('confirmed')->default(false);
	        $table->timestamps();
	    });

		Schema::create('owners', function($table)
        {
        	$table->increments('id');
        	$table->string('owner_type',1)->default('');
        	$table->string('owner_name')->default('');
        	$table->string('reg_nr',50)->default('');
        	$table->timestamps();
        });

	Schema::create('accounts', function($table)
        {
        	$table->increments('id');
        	$table->integer('owner_id');
        	$table->string('account_type',1);
        	$table->string('account_status',1);
        	$table->string('account_name')->default('');
        	$table->integer('balance')->default(0);
        	$table->timestamps();
        });

	Schema::create('etickets', function($table)
        {
        	$table->increments('id');
        	$table->string('nr',50)->unique();
        	$table->integer('account_id');
        	$table->string('eticket_status',1);
        	$table->string('pin',4);
        	$table->integer('balance');
        	$table->integer('limit_type');
        	$table->integer('limit');
        	$table->integer('turnover');
        	$table->timestamps();
        });

	Schema::create('authorisators', function($table)
        {
        	$table->increments('id');
        	$table->integer('object_id');
        	$table->string('authorisator_name');
        	$table->timestamps();
        });

		Schema::create('tickets', function($table)
        {
        	$table->increments('id');
        	$table->integer('user_id');
        	$table->integer('recipient_id');
        	$table->integer('owner_id');
        	$table->integer('mpos_id');
        	$table->integer('object_id');
        	$table->string('ticket_status',1);
        	$table->string('ticket_type',10);
                $table->dateTime('completed_when');
                $table->string('content');
        	$table->string('eticket_nr',10);
        	$table->timestamps();
        });

		Schema::create('account_access', function($table)
        {
        	$table->increments('id');
        	$table->integer('user_id');
        	$table->integer('account_id');
        	$table->string('access_status',1);
        	$table->string('access_level',1);
        	$table->dateTime('starts_when')->nullable();
        	$table->dateTime('ends_when')->nullable();
        	$table->text('comments')->default('');
        	$table->timestamps();
        });

		Schema::create('owner_access', function($table)
        {
        	$table->increments('id');
        	$table->integer('user_id');
        	$table->integer('owner_id');
        	$table->string('access_status',1);
        	$table->string('access_level',1);
        	$table->text('comments')->default('');
        	$table->timestamps();
        });

		Schema::create('objects', function($table)
        {
        	$table->increments('id');
        	$table->integer('owner_id')->default(0);
        	$table->string('object_type',2)->default('');
        	$table->string('object_name');
        	$table->string('object_country',2)->default('lv');
        	$table->string('object_city',100);
        	$table->string('object_address');
        	$table->string('object_ip',50);
        	$table->timestamps();
        });

		Schema::create('object_access', function($table)
        {
        	$table->increments('id');
        	$table->integer('object_id');
        	$table->integer('owner_id');
        	$table->timestamps();
        });

		Schema::create('operations', function($table)
        {
        	$table->increments('id');
        	$table->string('operation_name');
        	$table->string('operation_type',2);
        	$table->timestamps();
        });

	       Schema::create('mpos', function($table)
	    {
	    	$table->increments('id');
	    	$table->integer('object_id');
	    	$table->integer('account_id');
	    	$table->integer('operation_id')->default(0);
	    	$table->integer('owner_id');
	    	$table->string('mpos_type',2)->default('');
	    	$table->string('mpos_status',1)->default('');
	    	$table->string('mpos_name');
	    	$table->dateTime('last_online')->nullable();
	    	$table->string('firmware_version',100)->default('');
	    	$table->string('mpos_owner_type',1)->default('');
	    	$table->timestamps();
	    });

		Schema::create('transactions', function($table)
        {
        	$table->increments('id');
        	$table->integer('authorisator_id');
        	$table->string('local_id',100);
        	$table->integer('mpos_id');
        	$table->integer('operation_id');
        	$table->integer('amount')->default(0);
        	$table->integer('from_account_id');
        	$table->integer('to_account_id');
        	$table->string('from_eticket_nr',50);
        	$table->string('to_eticket_nr',50);
        	$table->integer('object_id');
        	$table->text('comments');
        	$table->string('t_status',1);
        	$table->string('confirmed',1);
        	$table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('owners');
		Schema::drop('accounts');
		Schema::drop('etickets');
		Schema::drop('authorisators');
		Schema::drop('tickets');
		Schema::drop('account_access');
		Schema::drop('owner_access');
		Schema::drop('objects');
		Schema::drop('object_access');
		Schema::drop('operations');
		Schema::drop('mpos');
		Schema::drop('transactions');
	}

}