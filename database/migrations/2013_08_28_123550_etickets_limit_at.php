<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class EticketsLimitAt extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets', function($t)
		{
		    $t->date('limit_at')->nullable();
			$t->string('grade',3)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets', function($t)
		{
		    $t->dropColumn('limit_at');
            $t->dropColumn('grade');
		});
	}

}