<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class ZzRkTmpTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_log_zz', function(Blueprint $t)
        {
            $t->increments('id');
            $t->date('datums')->default("0000-00-00");
            $t->string('pk',12)->default('');
            $t->text('data');
            $t->timestamps();
        });

        Schema::create('daily_log_rk', function(Blueprint $t)
        {
            $t->increments('id');
            $t->date('datums')->default("0000-00-00");
            $t->string('pk',12)->default('');
            $t->text('data');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_log_zz');
        Schema::drop('daily_log_rk');
    }
}
