<?php

use Illuminate\Database\Migrations\Migration;

class AddFieldsToEticket extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etickets', function($t)
		{
		    $t->string('pk',12)->default('');
		    $t->string('name',255)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etickets', function($t)
		{
		    $t->dropColumn('pk','name');
		});
	}

}