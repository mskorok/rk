<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class ObjectDotationHistoryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object_dotation_history', function(Blueprint $t)
        {
            $t->increments('id');
            $t->date('datums')->default("0000-00-00");
            $t->integer('object_id')->unsiqned()->default(0);
            $t->integer('skaits')->unsiqned()->default(0);
            $t->integer('regen')->unsiqned()->default(0);
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('object_dotation_history');
    }
}
