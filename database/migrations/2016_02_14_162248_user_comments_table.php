<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_comments', function(Blueprint $t)
		{
			$t->increments('id');
			$t->string('pk',12)->default('')->unique();
			$t->integer('last_editor_id')->default('0');
			$t->text('data');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_comments');
	}

}
