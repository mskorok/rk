<?php

use Illuminate\Database\Migrations\Migration;

class CreateEticketOpers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eticket_opers', function($t)
		{
		    $t->increments('id');
		    $t->integer('eticket_id');
		    $t->integer('operation_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eticket_opers');
	}

}