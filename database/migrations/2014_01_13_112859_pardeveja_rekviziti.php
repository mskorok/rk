<?php

use Illuminate\Database\Migrations\Migration;

class PardevejaRekviziti extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sellers', function($t)
		{
		    $t->string('jur_adrese',255)->default('');
		    $t->string('talrunis',50)->default('');
		    $t->string('banka',100)->default('');
		    $t->string('bankas_kods',10)->default('');
		    $t->string('konts',50)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sellers', function($t)
		{
		    $t->dropColumn('jur_adrese');
		    $t->dropColumn('talrunis');
		    $t->dropColumn('banka');
		    $t->dropColumn('bankas_kods');
		    $t->dropColumn('konts');
		});
	}

}