<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('sort')->default(1);
            $table->boolean('active')->default(true);
            $table->string('path');
            $table->string('path_sm')->nullable();
            $table->string('path_xs')->nullable();
            $table->dateTime('exposition_time_to')->nullable();
            $table->dateTime('exposition_time_from')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('top_slider');
    }
}
