<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', ['image', 'flash', 'html', 'ajax_html', 'xml'])->default('image');
            $table->unsignedInteger('sort')->default(1);
            $table->boolean('active')->default(true);
            $table->enum('size', ['1140 х 250', '555 х 160', '360 х 160', '262 х 160']);
            $table->string('path')->nullable();
            $table->string('path_sm')->nullable();
            $table->string('path_xs')->nullable();
            $table->text('code')->nullable();
            $table->dateTime('exposition_time_to')->nullable();
            $table->dateTime('exposition_time_from')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banners');
    }
}
