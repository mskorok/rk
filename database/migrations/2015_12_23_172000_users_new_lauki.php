<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersNewLauki extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($t)
		{
			$t->string('address',255)->default("");
			$t->string('info',255)->default("");
			$t->integer('pazime')->default(0);
			$t->string('pazime_text',255)->default("");
			$t->text('citi')->default("");
		});

        Schema::drop('eticket_groups');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($t)
		{
			$t->dropColumn('address');
			$t->dropColumn('info');
			$t->dropColumn('pazime');
			$t->dropColumn('pazime_text');
			$t->dropColumn('citi');
		});
	}

}
