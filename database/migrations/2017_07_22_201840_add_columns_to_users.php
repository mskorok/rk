<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('bank_confirmed')->default(false);
            $table->boolean('profile_filled')->default(false);
            $table->boolean('has_card')->default(false);
            $table->boolean('card_not_finished')->default(false);
            $table->boolean('refill_account_not_created')->default(false);
            $table->string('doc_copy')->nullable();
            $table->string('document_path')->nullable();
            $table->unsignedInteger('document_image_id')->nullable();
            $table->string('avatar')->nullable();
            $table->string('avatar_path')->nullable();
            $table->unsignedInteger('avatar_image_id')->nullable();
            $table->string('api_token', 60)->nullable();
            $table->string('delete_reason')->nullable();
//            $table->unsignedInteger('doc_type_id')->nullable();
//            $table->unsignedInteger('citizenship_id')->nullable();
//            $table->foreign('doc_type_id')->references('id')->on('doc_type')->onDelete('cascade');
//            $table->foreign('citizenship_id')->references('id')->on('citizenship')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('bank_confirmed');
            $table->dropColumn('profile_filled');
            $table->dropColumn('has_card');
            $table->dropColumn('card_not_finished');
            $table->dropColumn('refill_account_not_created');
            $table->dropColumn('doc_copy');
            $table->dropColumn('document_path');
            $table->dropColumn('document_image_id');
            $table->dropColumn('avatar');
            $table->dropColumn('avatar_path');
            $table->dropColumn('avatar_image_id');
            $table->dropColumn('api_token');
            $table->dropColumn('delete_reason');
//            $table->dropForeign('doc_type_id');
//            $table->dropForeign('citizenship_id');
//            $table->dropColumn('doc_type_id');
//            $table->dropColumn('citizenship_id');
        });
    }
}
