<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SellerBalanceAddFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('seller_daily_balance', function($t)
		{
			$t->integer('balance_paid')->default(0);
			$t->integer('balance_free')->default(0);
			$t->integer('daily_paid')->default(0);
			$t->integer('daily_free')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('seller_daily_balance', function($t)
		{
			$t->dropColumn('balance_paid');
			$t->dropColumn('balance_free');
            $t->dropColumn('daily_paid');
            $t->dropColumn('daily_free');
		});
	}

}
