<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AndrejamKludam extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function($t)
		{
			$t->string('err_code',20)->default("");
			$t->string('err_text',1024)->default("");
			$t->date('sent_to_api')->default("0000-00-00");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function($t)
		{
			$t->dropColumn('err_code');
			$t->dropColumn('err_text');
			$t->dropColumn('sent_to_api');
		});
	}

}
