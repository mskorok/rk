<?php

use Illuminate\Database\Migrations\Migration;

class CreateTicketTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets_types', function($t)
		{
		    $t->increments('id');
		    $t->string('name',100);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets_types');
	}

}