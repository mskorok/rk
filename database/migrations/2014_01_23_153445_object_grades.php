<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ObjectGrades extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('objects', function($t)
		{
		    $t->string('grades',255)->unsigend()->default('');
		});

		Schema::create('dinner_schedule', function(Blueprint $t)
		{
			$t->increments('id');
			$t->integer('object_id')->unsigned();
			$t->integer('dotation_id')->unsigned();
			$t->smallInteger('day')->unsigned();
			$t->string('grade',3)->default('');
			$t->string('time_from',5)->default('00:00');
			$t->string('time_until',5)->default('00:00');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('objects', function($t)
		{
		    $t->dropColumn('grades');
		});

		Schema::drop('dinner_schedule');
	}

}