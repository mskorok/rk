<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Dotations::class, function (Faker\Generator $faker) {
    return [
        'pk' => $faker->randomNumber(6, true).'-'.$faker->randomNumber(5, true),
        'dotation_id' => 1,
        'last_used' => '0000-00-00',
        'from_date' => '2016-01-01',
        'until_date' => '2016-05-01',
        'import_date' => '2016-01-01',
    ];
});
