<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\ObjectDotationPrices::class, function (Faker\Generator $faker) {
    return [
        'heading' => $faker->lastName,
        'object_id' => 1,
        'price' => 10,
        'seller_account_id' => 1,
        'dotation_type' => 'O',
    ];
});
