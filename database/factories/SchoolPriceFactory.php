<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\SchoolPrices::class, function (Faker\Generator $faker) {
    return [
        'school_id' => 1,
        'object_id' => 1,
        'grade' => 1,
        'price' => '1.00',
        'vat' => 1,
        'regnr' => $faker->randomNumber(5, true).$faker->randomNumber(6, true),
    ];
});
