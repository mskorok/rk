<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Transaction::class, function (Faker\Generator $faker) {
    return [
        'authorisator_id' => 1,
        'local_id' => 1,
        'mpos_id' => 1,
        'operation_id' => 1,
        'amount' => rand(1,100),
        'from_account_id' => 1,
        'to_account_id' => 1,
        'from_eticket_nr' => 1,
        'to_eticket_nr' => 1,
        'object_id' => 1,
        'comments' => '',
        't_status' => 'P',
        'confirmed' => 2,
        'parent_id' => 0,
        'dotation_id' => 0,
        'seller_reg_nr' => '',
        'freemeals_type' => 0,
        'err_code' => '',
        'err_text' => '',
        'sent_to_api' => '0000-00-00',
    ];
});
