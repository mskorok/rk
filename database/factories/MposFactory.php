<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Mpos::class, function (Faker\Generator $faker) {
    return [
        'object_id' => 1,
        'account_id' => 1,
        'operation_id' => 1,
        'owner_id' => 1,
        'mpos_type' => 1,
        'mpos_status' => '',
        'mpos_name' => $faker->firstName,
        'last_online' => '0000-00-00 00:00:00',
        'firmware_version' => '',
        'mpos_owner_type' => '',
        'secret_key' => 'abc',
        'reboot_flag' => 0,
        'mpos_serial' => $faker->randomNumber(8, true),
    ];
});
