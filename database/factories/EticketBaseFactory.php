<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\EticketsBase::class, function (Faker\Generator $faker) {
    return [
        'pk' => $faker->randomNumber(6, true).'-'.$faker->randomNumber(5, true),
        'etickets' => "",
        'custom' => 0,
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        //'grade' => $faker->numberBetween(1,12).$faker->randomLetter,
        'object_id' => 0,
        'brivpusdienas' => 0,
        'school_id' => 1,
        'free_meals_from' => '0000-00-00',
        'free_meals_until' => '0000-00-00',
        'second_meals_from' => '0000-00-00',
        'second_meals_until' => '0000-00-00',
        'third_meals_from' => '0000-00-00',
        'third_meals_until' => '0000-00-00',
        'kopgalds_from' => '0000-00-00',
        'kopgalds_until' => '0000-00-00',
    ];
});
