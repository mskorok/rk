<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\School::class, function (Faker\Generator $faker) {
    return [
        'heading' => $faker->firstName,
        'reg_nr' => 1,
        'changed' => '2016-01-01',
        'status' => '1',
        'equal_price' => '0.00',
    ];
});
