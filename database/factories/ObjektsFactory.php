<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Objekts::class, function (Faker\Generator $faker) {
    return [
        'owner_id' => 1,
        'object_type' => 1,
        'object_name' => $faker->firstName,
        'object_country' => 'LV',
        'object_city' => 'Riga',
        'object_address' => $faker->address,
        'object_ip' => $faker->ipv4,
        'object_regnr' => $faker->randomNumber(5, true).$faker->randomNumber(6, true),
        'kods' => '1',
        'show' => '1',
        'vat' => '1',
    ];
});
