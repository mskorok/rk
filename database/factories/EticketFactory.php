<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Eticket::class, function (Faker\Generator $faker) {
    return [
        'nr' => "1".$faker->randomNumber(4, true).$faker->randomNumber(5, true),
        'account_id' => 1,
        'eticket_status' => 1,
        'pin' => '0000',
        'balance' => 1,
        'limit_type' => 0,
        'limit' => 0,
        'turnover' => 0,
        'sync_state' => 0,
        'group_id' => 0,
        'thumb_id' => 0,
        'grade' => $faker->numberBetween(1,12).$faker->randomLetter,
        'pk' => $faker->randomNumber(6, true).'-'.$faker->randomNumber(5, true),
        'name' => "Vards Uzvards".$faker->randomNumber(2, true),
        'limit_at' => '0000-00-00',
        'free_meals_from' => '0000-00-00',
        'free_meals_until' => '0000-00-00',
        'second_meals_from' => '0000-00-00',
        'second_meals_until' => '0000-00-00',
        'third_meals_from' => '0000-00-00',
        'third_meals_until' => '0000-00-00',
        'kopgalds_from' => '0000-00-00',
        'kopgalds_until' => '0000-00-00',
    ];
});
