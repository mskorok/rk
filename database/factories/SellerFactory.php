<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Seller::class, function (Faker\Generator $faker) {
    return [
        'owner_id' => 1,
        'seller_name' => $faker->firstName,
        'jur_adrese' => $faker->address,
        'talrunis' => $faker->phoneNumber,
        'banka' => 'A/S SWEDBANKA',
        'bankas_kods' => 'HABALV22',
        'konts' => 'LV47HABA05510255'.$faker->randomNumber(5, true),
        'reg_nr' => $faker->randomNumber(5, true).$faker->randomNumber(6, true),
        'contract_nr' => '',
        'contract_person' => '',
    ];
});
