<?php

namespace App\Providers;

use App\Models\Owner;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Owner::created(function ($owner) {
            if ($owner->owner_type == 1) {
                $typeExists = Owner::typeExists();
                if (!$typeExists) {
                    $owners =  Owner::U()->get();
                    $owners->addToIndex();
                }

                $owner->addToIndex();
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Illuminate\Contracts\Auth\Registrar',
            'App\Services\Registrar'
        );
    }
}
