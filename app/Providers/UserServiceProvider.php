<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.18.7
 * Time: 13:07
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('UserService', 'App\Services\UserService');
    }
}
