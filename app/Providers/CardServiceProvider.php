<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.31.7
 * Time: 21:02
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CardServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('CardService', 'App\Services\CardService');
    }
}
