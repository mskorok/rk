<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.18.7
 * Time: 09:10
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DecryptServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Decrypt', 'App\Services\Decrypt');
    }
}
