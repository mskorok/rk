<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Eticket;
use App\Models\EticketsObjekti;
use App\Models\Mpos;
use App\Models\Objekts;
use App\Models\OwnerAccess;
use App\Models\Ticket;
use App\Models\Transaction;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{

    protected $result1;
    protected $result2;

    /**
     * Initializer.
     *
     * @access   public
     * @throws \Exception
     */
    public function __construct()
    {
        $u = Auth::user();

        if (isset($u)) {
            $roleids = Cache::remember('roleids_'.$u->id, 30, function () use ($u) {
                return $u->currentRoleIds();
            });

            $ownerships = Cache::remember('ownerships_'.$u->id, 30, function () use ($u) {
                return $u->grantedOwnerships();
            });

            View::share('roleids', $roleids);
            View::share('granted', $ownerships);
            View::share('u', $u);
        }

        if ((isset($u)) && (App::environment() !== 'testing')) {
            if (! in_array(2, $roleids, false)) { // objektam nav bilances
                $account = Cache::remember('user_balance_'.$u->id, 30, function () use ($u) {
                    try {
                        $owner = OwnerAccess::ao()->u($u->id)->first()->owner->id;
                        $account = AccountAccess::ao()->u($u->id)->first()->account;
                    } catch (\Exception $e) {
                        return '';
                    }

                    if (isset($account->owner_id, $owner)) {
                        if ($account->owner_id == $owner) {
                            return $account;
                        } else {
                            return '';
                        }
                    } else {
                        return '';
                    }
                });

                View::share('account', $account);
            }

            $active_etickets = $this->getUsersActiveEtickets(1);

            View::share('active_etickets', $active_etickets);

            // seller balances if such account
            if (in_array(3, $roleids, false)) {
                // seller balance
                // sum of mpos balances
                $owner_id = OwnerAccess::seller()->u(Auth::user()->id)->value('owner_id');
                if ($owner_id > 0) {
                    $total = 0;
                    // get MPOS devices and their accounts
                    $mpos = Mpos::with('account')->own($owner_id)->get();
                    $mpos_account_ids = Mpos::own($owner_id)->lists("account_id")->toArray();
                    foreach ($mpos as $device) {
                        if ($device->account != null) {
                            $total = $total + $device->account->balance;
                        }
                    }

                    // get free meal balance and count
                    $free_meal_count = Transaction::whereIn("to_account_id", $mpos_account_ids)->nizMFree()->count();
//                    $free_meal_total = Transaction::whereIn("to_account_id", $mpos_account_ids)
//                        ->NizMFree()->sum("amount");

                    $total = sprintf("%.2f", $total/100);
                    //$free_meal_total = sprintf("%.2f",$free_meal_total/100);

                    View::share('seller_balance', $total);
                    View::share('seller_free_meal_count', $free_meal_count);
                    //View::share('seller_free_meal_balance', $free_meal_total);
                } else {
                    throw new \Exception('no owner');
                }
            }

            // object eticket count if such account
            if (in_array(2, $roleids, false)) {
                // seller balance
                // sum of mpos balances
                $owner_id = OwnerAccess::objekts()->u(Auth::user()->id)->value('owner_id');
                if ($owner_id > 0) {
                    // get object
                    $object_id = Objekts::where('owner_id', $owner_id)->value('id');
                    if ($object_id > 0) {
                        $count=0;
                        // get eticket count
                        $count = EticketsObjekti::obj($object_id)->count();

                        View::share('object_eticket_count', $count);
                    } else {
                        View::share('object_eticket_count', 0);
                    }
                } else {
                    View::share('object_eticket_count', 0);
                }
            }

            // new notifications
            $new_notifs = $this->get_user_new_message_count();
            View::share('new_notifs', $new_notifs);
        }

        $this->result1 = '';
        $this->result2 = '';
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (! is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    protected function getUsersActiveEtickets($count = 0)
    {
        $user_id = Auth::user()->id;

        if ($count == 1) {
            $active_etickets = Cache::rememberForever('active_etickets_'.$user_id, function () use ($user_id, $count) {
                // get all owners for user
                $all_owners = array();
                $owners = OwnerAccess::with('owner')->ao()->u($user_id)->get();
                foreach ($owners as $o) {
                    array_push($all_owners, $o->owner->id);
                }

                // get granted ownerships for user
                /** @var array $granted */
                $granted = Auth::user()->grantedOwnerships();
                foreach ($granted as $o) {
                    array_push($all_owners, $o->owner->id);
                }

                // get all eticket accounts (type=2) for owners
                $all_accounts = Account::whereIn("owner_id", $all_owners)
                    ->et()->lists("id")->all();

                if (count($all_accounts) == 0) {
                    $all_accounts = ['0'];
                }

                return Eticket::a()->whereIn('account_id', $all_accounts)->count();
            });
        } else {
            $active_etickets = Cache::rememberForever(
                'all_active_etickets_'.$user_id,
                function () use ($user_id, $count) {
                // get all owners for user
                    $all_owners = array();
                    $owners = OwnerAccess::with('owner')->ao()->u($user_id)->get();
                    foreach ($owners as $o) {
                        array_push($all_owners, $o->owner->id);
                    }

                // get granted ownerships for user
                    $granted = Auth::user()->grantedOwnerships();
                    foreach ($granted as $o) {
                        array_push($all_owners, $o->owner->id);
                    }

                // get all eticket accounts (type=2) for owners
                    $all_accounts = Account::whereIn("owner_id", $all_owners)->et()->lists("id")->all();

                    if (count($all_accounts)==0) {
                        $all_accounts = ['0'];
                    }

                    return Eticket::a()->whereIn('account_id', $all_accounts)->get();
                }
            );
        }

        return $active_etickets;
    }

    protected function get_user_object_owners()
    {
        $user_id = Auth::user()->id;

        $all_object_owners = array();
        $owners = OwnerAccess::objekts()->u($user_id)->get();
        foreach ($owners as $o) {
            array_push($all_object_owners, $o->owner_id);
        }

        return $all_object_owners;
    }

    protected function get_owner_objects($owners)
    {
        $all_objects = array();
        foreach ($owners as $o) {
            $objects = Objekts::where('owner_id', $o)->orderBy("object_name", "ASC")->get();
            foreach ($objects as $a) {
                array_push($all_objects, $a->id);
            }
        }

        return $all_objects;
    }

    protected function get_all_objects()
    {
        $all_objects = Cache::remember('all_object_ids', 30, function () {
            $all_objects = array();
            $objects = Objekts::orderBy("object_name", "ASC")->get();
            foreach ($objects as $a) {
                array_push($all_objects, $a->id);
            }

            return $all_objects;
        });

        return $all_objects;
    }

    protected function get_all_users()
    {
        $all_users = Cache::remember('all_user_ids', 30, function () {
            $all_accounts = array();
            $sellers = Account::u()->get();
            foreach ($sellers as $a) {
                array_push($all_accounts, $a->id);
            }

            return $all_accounts;
        });

        return $all_users;
    }

    protected function get_all_sellers()
    {
        $all_accounts = Cache::remember('all_seller_ids', 30, function () {
            $all_accounts = array();
            $sellers = Account::sell()->get();
            foreach ($sellers as $a) {
                array_push($all_accounts, $a->id);
            }

            return $all_accounts;
        });

        return $all_accounts;
    }

    protected function admin_user()
    {
        $admin_user = 0;

        $roleids = Cache::remember('roleids_'.Auth::user()->id, 30, function () {
            $u = Auth::user();

            return $u->currentRoleIds();
        });

        if (in_array(1, $roleids, false)) {
            $admin_user = 1;
        }

        return $admin_user;
    }

    protected function accountant_user()
    {
        $accountant_user = 0;

        $roleids = Cache::remember('roleids_'.Auth::user()->id, 30, function () {
            $u = Auth::user();

            return $u->currentRoleIds();
        });

        if (in_array(4, $roleids, false)) {
            $accountant_user = 1;
        }

        return $accountant_user;
    }

    protected function get_all_mpos()
    {
        $all_mpos = Cache::remember('all_mpos', 30, function () {
            return Mpos::lists('mpos_name', 'id');
        });

        return $all_mpos;
    }

    protected function get_seller_mpos_accounts($seller_owner_id)
    {
        $mpos_accounts = Cache::remember(
            'seller_mpos_accounts_'.$seller_owner_id,
            30,
            function () use ($seller_owner_id) {
                return Mpos::own($seller_owner_id)->get();
            }
        );

        return $mpos_accounts;
    }

    protected function get_seller_accounts($mpos_accounts,$account_id)
    {
        $seller_accounts = [];
        if ($account_id > 0) {
            $seller_accounts[0] = $account_id;
        }

        foreach ($mpos_accounts as $value) {
            if ($value->account_id > 0) {
                array_push($seller_accounts, $value->account_id);
            }
        }

        return $seller_accounts;
    }

    protected function get_seller_objects($seller_owner_id)
    {
        $seller_objects = Cache::remember(
            'seller_objects_'.$seller_owner_id,
            30,
            function () use ($seller_owner_id) {
                $seller_object_ids = Mpos::own($seller_owner_id)->distinct()->lists('object_id')->toArray();
                $seller_objects = [];
                if (count($seller_object_ids) > 0) {
                    $seller_objects = Objekts::whereIn('id', $seller_object_ids)->select('id', 'object_name')->get();
                }
                return $seller_objects;
            }
        );

        return $seller_objects;
    }

    protected function get_user_new_message_count()
    {
        $user_id = Auth::user()->id;
        $message_count = Cache::remember('message_count_'.$user_id, 10, function () use ($user_id) {
            $last_login = Cache::get('last_login_'.$user_id, date("Y-m-d H:i:s"));

            $sk = Ticket::where(function ($query) use ($user_id) {
                $query->where('user_id', '=', $user_id)->orWhere('recipient_id', '=', $user_id);
            })->where(function ($query) use ($last_login) {
                    $query->where('completed_when', '>=', $last_login)
                        ->orWhere('created_at', '>=', $last_login)
                        ->orWhere('updated_at', '>=', $last_login);
            })->count();

            return $sk;
        });

        return $message_count;
    }

    protected function create_eticket_api_calls($pk, $all_rk_tickets = 0)
    {
        Session::put('all_rk_tickets', $all_rk_tickets);

        $RCX = new RollingCurlX(2);

        //request 1 -  zz dats
        $xml = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"><wsa:Action>http://tempuri.org/IMonitoringService/GetPersonData</wsa:Action></soap:Header>
            <soap:Body>
              <tem:GetPersonData>
                 <!--Optional:-->
                 <tem:request>
                    <zzd:Header>
                       <zzd1:From></zzd1:From>
                    </zzd:Header>
                    <zzd:Person>
                       <zzd:PersonId>'.$pk.'</zzd:PersonId>
                    </zzd:Person>
                 </tem:request>
              </tem:GetPersonData>
            </soap:Body>
            </soap:Envelope>';

        $headers = array(
            "POST HTTP/1.1",
            "Accept-Encoding: gzip,deflate",
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/GetPersonData"',
            "Content-length: ".strlen($xml),
            "Host: ".Config::get('services.rs.zzurl2'),
            "Connection: Keep-Alive",
            "User-Agent: skolas.rigaskarte.lv",
        );

        $url = Config::get('services.rs.zzurl');
        $options = [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSLCERT => storage_path()."/zz_cert.pem",
            CURLOPT_SSLCERTPASSWD => Config::get('services.rs.certkey'),
        ];

        $RCX->addRequest($url, $xml, [$this, 'callback_functn'], [], $options, $headers);

        //request 2 - RK
        $xml2 = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
          <soap:Body>
            <GetCardsByPersonId xmlns="http://www.rigaskarte.lv/dpa/addon">
              <personCode>'.$pk.'</personCode>
            </GetCardsByPersonId>
          </soap:Body>
        </soap:Envelope>';

        $headers2 = array(
            "POST  HTTP/1.1",
            "Host: 91.231.68.12",
            "Content-type: text/xml; charset=\"utf-8\"",
            "SOAPAction: \"http://www.rigaskarte.lv/dpa/addon/GetCardsByPersonId\"",
            "Content-length: ".strlen($xml2)
        );

        $url2 = Config::get('services.rs.rkurl');
        $options2 = [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => Config::get('services.rs.rk'),
        ];

        $RCX->addRequest($url2, $xml2, [$this, 'callback_functn'], [], $options2, $headers2);

        $RCX->setTimeout(90000); //in milliseconds
        $RCX->execute();

        return ["r1" => $this->result1, "r2" => $this->result2];
    }

    // callback
    public function callback_functn($response, $url, $request_info, $user_data, $time)
    {
        if ($url == Config::get('services.rs.rkurl')) {
            if ($request_info["http_code"] != 200) {
                $this->result1 = 'error';
            } else {
                // return all valid rk tickets
                $all_rk_tickets = Session::get('all_rk_tickets', 0);

                // LOG data to file
                $log_file = Session::get("custom_log_file", date("Y-m")."_rk_log.txt");
                File::append(storage_path().'/'.$log_file, date("Y-m-d H:i:s")." - RK get etickets\n".$response."\n");

                // process data
                $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $response);
                $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
                $xml_string =str_replace('<soap:', '<', $xml_string);
                $res1 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);

                if (! isset($res1["Body"]["GetCardsByPersonIdResponse"]["GetCardsByPersonIdResult"]["LsrCardInfo"])) {
                    $this->result1 = "error1";
                } else {
                    $result1 = $res1["Body"]["GetCardsByPersonIdResponse"]["GetCardsByPersonIdResult"]["LsrCardInfo"];

                    if ($all_rk_tickets == 0) {
                        if (isset($result1[0]["ExpirationDate"])) {
                            $result1 = $result1[0];
                        }

                        $result = (strtotime(substr($result1["ExpirationDate"], 0, 10)) > time())
                            ? $result1["PersonCode"]
                            : 0;

                        $this->result1 = $result;
                    } else {
                        $ets = [];

                        // single
                        if (isset($result1["ExpirationDate"])) {
                            if (strtotime(substr($result1["ExpirationDate"], 0, 10)) > time()) {
                                $ets[] = $result1["SerialNumber"];
                            }
                        } else {
                            // multiple
                            //
                            foreach ($result1 as $et) {
                                if (strtotime(substr($et["ExpirationDate"], 0, 10)) > time()) {
                                    $ets[] = $et["SerialNumber"];
                                }
                            }
                        }

                        $this->result1 = $ets;
                    }
                }
            }
        } else {
            if ($request_info["http_code"] != 200) {
                $this->result2 = 'error';
            } else {
                // LOG data to file
                $log_file = Session::get("custom_log_file", date("Y-m")."_zz_log.txt");
                File::append(storage_path().'/'.$log_file, date("Y-m-d H:i:s")." - getchilddata\n".$response."\n");

                // process data
                $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $response);
                $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
                $xml_string =str_replace('<s:', '<', $xml_string);
                $xml_string =str_replace('<a:', '<', $xml_string);
                $xml_string =str_replace('<b:', '<', $xml_string);
                $res2 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);

                if (! isset($res2["Body"]["GetPersonDataResponse"]["GetPersonDataResult"])) {
                    //
                } else {
                    $result2 = $res2["Body"]["GetPersonDataResponse"]["GetPersonDataResult"];

                    $this->result2 = "error1";

                    $result = [
                        "pk" => $result2["Person"]["PersonId"],
                        "name" => $result2["Person"]["PersonName"].' '.$result2["Person"]["PersonSurname"],
                        "School" => $result2["School"],
                        "FreeMeals" => $result2["FreeMeals"],
                        "UnusedMeals" => $result2["UnusedMeals"],
                        "RdMeals" => $result2["RdMeals"]
                    ];

                    $this->result2 = $result;
                }
            }
        }
    }

    protected function numeric_entify_utf8 ($utf8_string)
    {
        $out = "";
        $ns = strlen($utf8_string);
        for ($nn = 0; $nn < $ns; $nn++) {
            $ch = $utf8_string [$nn];
            $ii = ord($ch);

            if ($ii < 128) {
                $out .= $ch;
            } elseif ($ii >>5 == 6) {
                $b1 = ($ii & 31);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b2 = ($ii & 63);

                $ii = ($b1 * 64) + $b2;

                $ent = sprintf("&#%d;", $ii);
                $out .= $ent;
            } elseif ($ii >>4 == 14) {
                $b1 = ($ii & 31);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b2 = ($ii & 63);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b3 = ($ii & 63);

                $ii = ((($b1 * 64) + $b2) * 64) + $b3;

                $ent = sprintf("&#%d;", $ii);
                $out .= $ent;
            } elseif ($ii >>3 == 30) {
                $b1 = ($ii & 31);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b2 = ($ii & 63);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b3 = ($ii & 63);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b4 = ($ii & 63);

                $ii = ((((($b1 * 64) + $b2) * 64) + $b3) * 64) + $b4;

                $ent = sprintf("&#%d;", $ii);
                $out .= $ent;
            }
        }
        return $out;
    }

    protected function translit($str)
    {
        $s = $str;
        $s = preg_replace('|\&[^;]+;|U', '', $s);
        $s = strip_tags($s);

        $s = str_replace("a", "a", $s);
        $s = str_replace("c", "c", $s);
        $s = str_replace("e", "e", $s);
        $s = str_replace("g", "g", $s);
        $s = str_replace("i", "i", $s);
        $s = str_replace("k", "k", $s);
        $s = str_replace("l", "l", $s);
        $s = str_replace("n", "n", $s);
        $s = str_replace("�", "s", $s);
        $s = str_replace("u", "u", $s);
        $s = str_replace("�", "z", $s);

        $s = $this->numeric_entify_utf8($s);

        // lat
        $s = str_replace("&#257;","a",$s);
        $s = str_replace("&#269;","c",$s);
        $s = str_replace("&#275;","e",$s);
        $s = str_replace("&#291;","g",$s);
        $s = str_replace("&#299;","i",$s);
        $s = str_replace("&#311;","k",$s);
        $s = str_replace("&#316;","l",$s);
        $s = str_replace("&#326;","n",$s);
        $s = str_replace("&#353;","s",$s);
        $s = str_replace("&#363;","u",$s);
        $s = str_replace("&#382;","z",$s);
        // lat CAPITAL
        $s = str_replace("&#256;","a",$s);
        $s = str_replace("&#268;","c",$s);
        $s = str_replace("&#274;","e",$s);
        $s = str_replace("&#290;","g",$s);
        $s = str_replace("&#298;","i",$s);
        $s = str_replace("&#310;","k",$s);
        $s = str_replace("&#315;","l",$s);
        $s = str_replace("&#325;","n",$s);
        $s = str_replace("&#352;","s",$s);
        $s = str_replace("&#362;","u",$s);
        $s = str_replace("&#381;","z",$s);
        // rus
        $s = str_replace("&#1081;","i",$s);
        $s = str_replace("&#1094;","ch",$s);
        $s = str_replace("&#1091;","u",$s);
        $s = str_replace("&#1082;","k",$s);
        $s = str_replace("&#1077;","e",$s);
        $s = str_replace("&#1085;","n",$s);
        $s = str_replace("&#1075;","g",$s);
        $s = str_replace("&#1096;","sh",$s);
        $s = str_replace("&#1097;","sh",$s);
        $s = str_replace("&#1079;","z",$s);
        $s = str_replace("&#1093;","h",$s);
        $s = str_replace("&#1098;","",$s);
        $s = str_replace("&#1092;","f",$s);
        $s = str_replace("&#1099;","i",$s);
        $s = str_replace("&#1074;","v",$s);
        $s = str_replace("&#1072;","a",$s);
        $s = str_replace("&#1087;","p",$s);
        $s = str_replace("&#1088;","r",$s);
        $s = str_replace("&#1086;","o",$s);
        $s = str_replace("&#1083;","l",$s);
        $s = str_replace("&#1076;","d",$s);
        $s = str_replace("&#1078;","zh",$s);
        $s = str_replace("&#1101;","e",$s);
        $s = str_replace("&#1103;","ja",$s);
        $s = str_replace("&#1095;","ch",$s);
        $s = str_replace("&#1089;","s",$s);
        $s = str_replace("&#1084;","m",$s);
        $s = str_replace("&#1080;","i",$s);
        $s = str_replace("&#1090;","t",$s);
        $s = str_replace("&#1100;","",$s);
        $s = str_replace("&#1073;","b",$s);
        $s = str_replace("&#1102;","ju",$s);
        $s = str_replace("&#1105;","jo",$s);
        // rus CAPITAL
        $s = str_replace("&#1049;","i",$s);
        $s = str_replace("&#1062;","ch",$s);
        $s = str_replace("&#1059;","u",$s);
        $s = str_replace("&#1050;","k",$s);
        $s = str_replace("&#1045;","e",$s);
        $s = str_replace("&#1053;","n",$s);
        $s = str_replace("&#1043;","g",$s);
        $s = str_replace("&#1064;","sh",$s);
        $s = str_replace("&#1065;","sh",$s);
        $s = str_replace("&#1047;","z",$s);
        $s = str_replace("&#1061;","h",$s);
        $s = str_replace("&#1066;","",$s);
        $s = str_replace("&#1060;","f",$s);
        $s = str_replace("&#1067;","i",$s);
        $s = str_replace("&#1042;","v",$s);
        $s = str_replace("&#1040;","a",$s);
        $s = str_replace("&#1055;","p",$s);
        $s = str_replace("&#1056;","r",$s);
        $s = str_replace("&#1054;","o",$s);
        $s = str_replace("&#1051;","l",$s);
        $s = str_replace("&#1044;","d",$s);
        $s = str_replace("&#1046;","zh",$s);
        $s = str_replace("&#1069;","e",$s);
        $s = str_replace("&#1071;","ja",$s);
        $s = str_replace("&#1063;","ch",$s);
        $s = str_replace("&#1057;","s",$s);
        $s = str_replace("&#1052;","m",$s);
        $s = str_replace("&#1048;","i",$s);
        $s = str_replace("&#1058;","t",$s);
        $s = str_replace("&#1068;","",$s);
        $s = str_replace("&#1041;","b",$s);
        $s = str_replace("&#1070;","ju",$s);
        $s = str_replace("&#1025;","jo",$s);
        // swe
        $s = str_replace("&#5767168;","o",$s);
        $s = str_replace("&#16384;","a",$s);
        $s = str_replace("&#20480;","a",$s);
        // swe CAPITAL
        $s = str_replace("&#1408;","o",$s);
        $s = str_replace("&#320;","a",$s);
        // nor
        $s = str_replace("&#24576;","ae",$s);
        // nor CAPITAL
        $s = str_replace("&#384;","ae",$s);

        $s = strtolower($s);
        $s = preg_replace('|[^a-z0-9_\ \,\.\-]|', '', $s);
        $s = chop(trim($s));
        //$s = str_replace(' ', '_', $s);
        //$s = str_replace('-', '_', $s);
        //$s = str_replace(',', '_', $s);
        //$s = str_replace('.', '_', $s);
        while (strpos($s, '__') !== false) {
            $s = str_replace('__', '_', $s);
        }
        if (substr($s, -1) == '_') {
            $s = substr($s, 0, -1);
        }
        return($s);
    }

    protected function add_pk_to_monitoring_list($pk)
    {
        $xml = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
   <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"><wsa:Action>http://tempuri.org/IMonitoringService/UpdateMonitoringList</wsa:Action></soap:Header>
   <soap:Body>
      <tem:UpdateMonitoringList>
         <!--Optional:-->
         <tem:request>
            <zzd:Header>
               <zzd1:From>'.date("Y-m-d").'</zzd1:From>
            </zzd:Header>
            <zzd:Operations>
               <!--Zero or more repetitions:-->
               <zzd:MonitoringOperation>
                  <zzd:PersonId>'.$pk.'</zzd:PersonId>
                  <zzd:Operation>add</zzd:Operation>
               </zzd:MonitoringOperation>
            </zzd:Operations>
         </tem:request>
      </tem:UpdateMonitoringList>
   </soap:Body>
</soap:Envelope>';

        $headers = array(
            "POST HTTP/1.1",
            "Accept-Encoding: gzip,deflate",
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/UpdateMonitoringList"',
            "Content-length: ".strlen($xml),
            "Host: ".Config::get('services.rs.zzurl2'),
            "Connection: Keep-Alive",
            "User-Agent: skolas.rigaskarte.lv",
        );

        $url =  Config::get('services.rs.zzurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLCERT , storage_path()."/zz_cert.pem");
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD , Config::get('services.rs.certkey'));

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);

        // LOG data to file
        $log_file = Session::get("custom_log_file",date("Y-m")."_zz_log.txt");
        File::append(storage_path().'/'.$log_file, date("Y-m-d H:i:s")." - addtomonitoring ($http_code) \n".$response."\n");

        if ($http_code != 200)
            return 'error';

        // process data
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $response);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string =str_replace('<s:', '<', $xml_string);
        $xml_string =str_replace('<a:', '<', $xml_string);
        $xml_string =str_replace('<b:', '<', $xml_string);

        $res = json_decode(json_encode(simplexml_load_string($xml_string)), 1);

        // need OK
        $result = isset ($res["Body"]["UpdateMonitoringListResponse"]["UpdateMonitoringListResult"]["Results"]["MonitoringOperationResult"]["OperationAnswer"]) ?
            substr($res["Body"]["UpdateMonitoringListResponse"]["UpdateMonitoringListResult"]["Results"]["MonitoringOperationResult"]["OperationAnswer"],0,2) : '';

        // log and return if other response
        if ($result != "OK") {
            Log::error("error, add to monitoring list ($pk) - ".implode(",", $res["Body"]["UpdateMonitoringListResponse"]["UpdateMonitoringListResult"]["Results"]["MonitoringOperationResult"]));

            if (isset($res["Body"]["UpdateMonitoringListResponse"]["UpdateMonitoringListResult"]["Results"]["MonitoringOperationResult"]["AnswerDescription"]))
            {
                $answer_description = trim($res["Body"]["UpdateMonitoringListResponse"]["UpdateMonitoringListResult"]["Results"]["MonitoringOperationResult"]["AnswerDescription"]);
                if (strpos($answer_description, 'jau ir ieraksts ar personas kodu') !== false)
                {
                    return "OK";
                }
            }

            return 'error1';
        }

        return "OK";
    }

    protected function get_monitoring_list_pks()
    {
        $xml = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
	<soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"><wsa:Action>http://tempuri.org/IMonitoringService/GetMonitoringList</wsa:Action></soap:Header>
   <soap:Body>
      <tem:GetMonitoringList/>
   </soap:Body>
</soap:Envelope>';

        $headers = array(
            "POST HTTP/1.1",
            "Accept-Encoding: gzip,deflate",
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/GetMonitoringList"',
            "Content-length: ".strlen($xml),
            "Host: ".Config::get('services.rs.zzurl2'),
            "Connection: Keep-Alive",
            "User-Agent: skolas.rigaskarte.lv",
        );

        $url = Config::get('services.rs.zzurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLCERT , storage_path()."/zz_cert.pem");
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD , Config::get('services.rs.certkey'));

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);

        // LOG data to file
        $log_file = Session::get("custom_log_file",date("Y-m")."_zz_log.txt");
        File::append(storage_path().'/'.$log_file, date("Y-m-d H:i:s")." - getmonitoringlist ($http_code) \n".$response."\n");

        if ($http_code != 200) {
            return 'error';
        }

        // process data
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $response);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string =str_replace('<s:', '<', $xml_string);
        $xml_string =str_replace('<a:', '<', $xml_string);
        $xml_string =str_replace('<b:', '<', $xml_string);

        $res = json_decode(json_encode(simplexml_load_string($xml_string)), 1);

        return isset ($res["Body"]["GetMonitoringListResponse"]["GetMonitoringListResult"]["string"]) ?
            $res["Body"]["GetMonitoringListResponse"]["GetMonitoringListResult"]["string"] : [];

    }

}