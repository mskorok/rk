<?php
namespace App\Http\Controllers\etickets;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\Eticket;
use App\Models\Mpos;
use App\Models\Ticket;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class MessagesController extends BaseController {

    protected $links = 'messages';

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        $data = Ticket::with('type')->U(Auth::user()->id)->orderBy('id','desc')->get();

        // Title
        $title = Lang::get('etickets/'.$this->links.'/title.management');

        // remove badge
        Cache::forever('last_login_'.Auth::user()->id, (string)date("Y-m-d H:i:s"));

        // update user last login
        $user = Auth::user();
        $user->last_login = date("Y-m-d H:i:s");
        $user->save();

        // Show the page
        return View::make('etickets/'.$this->links.'/index', compact('data','title'));
    }

    public function getMconfirm($objekts)
    {
        $amount = intval($objekts->content)/100;

        // Title
        $title = 'Apstiprināt naudas atgriešanu - '.$objekts->eticket_nr.', '.$amount.' EUR';

        // Show the page
        return View::make('etickets/'.$this->links.'/confirm', compact('objekts', 'title'));
    }

    public function getMcancel($objekts)
    {
        $amount = intval($objekts->content)/100;

        // Title
        $title = Lang::get('etickets/'.$this->links.'/title.obj_cancel').' - naudas atgriešana - '.$objekts->eticket_nr.', '.$amount.' EUR';

        // Show the page
        return View::make('etickets/'.$this->links.'/cancel', compact('objekts', 'title'));
    }

    public function postMcancel($objekts)
    {
        if (($objekts->recipient_id!=Auth::user()->id) AND ($objekts->user_id!=Auth::user()->id))
        {
            return Redirect::to('etickets')->with('error', 'Nekorekta piekļuve!');
        }

        if ($objekts->ticket_status!=0)
        {
            return Redirect::to('etickets/messages')->with('error', 'Nekorekta piekļuve!');
        }

        $id = $objekts->id;
        $objekts = Ticket::find($id);

        // get MPOS account
        $account_id = Mpos::find($objekts->mpos_id)->account_id;
        $account = Account::find($account_id);
        if ($account->account_type!=6)
        {
            return Redirect::to('etickets/'.$this->links)->with('error', 'Kļūda atlasot kontu!');
        }

        try {
            DB::transaction(function() use($objekts,$account)
            {
                // create reverse TMP transaction
                $t = New Transaction;
                $t->amount = $objekts->content;
                $t->operation_id = 1;
                $t->from_account_id = 999997; // tmp account
                $t->to_account_id = $account->id;
                $t->comments = Auth::user()->id;
                $t->t_status = 'P';
                $t->confirmed = 1;

                if (!$t->save()) throw New Exception();

                // increase MPOS balance
                $account->balance = $account->balance + $objekts->content;

                if (!$account->save()) throw New Exception();
            });
        }
        catch(Exception $e) {
            return Redirect::to('etickets/'.$this->links)
                ->withInput()
                ->with('error','Kļūda saglabājot datus!');
        }

        $input = $objekts;

        $objekts->ticket_status = 2;
        $objekts->completed_when = date("Y-m-d H:i:s");

        if ($objekts->save())
        {
            return Redirect::to('etickets/'.$this->links)->with('success', Lang::get('etickets/'.$this->links.'/messages.cancel.success'));
        }
        else
        {
            return Redirect::to('etickets/'.$this->links)->with('error', Lang::get('etickets/'.$this->links.'/messages.cancel.error'));
        }
    }

    public function postMconfirm($objekts)
    {
        if (($objekts->recipient_id!=Auth::user()->id) AND ($objekts->user_id!=Auth::user()->id))
        {
            return Redirect::to('etickets')->with('error', 'Nekorekta piekļuve!');
        }

        if ($objekts->ticket_status!=0)
        {
            return Redirect::to('etickets/messages')->with('error', 'Nekorekta piekļuve!');
        }

        $id = $objekts->id;
        $objekts = Ticket::find($id);

        // get e-ticket account
        $account = Account::find($objekts->object_id);
        if ($account->account_type!=2)
        {
            return Redirect::to('etickets/'.$this->links)->with('error', 'Kļūda atlasot kontu!');
        }

        // get e-ticket
        $et = Eticket::where('account_id',$account->id)->first();
        if (!isset($et->id))
        {
            return Redirect::to('etickets/'.$this->links)->with('error', 'Kļūda atlasot e-talonu!');
        }

        // get MPOS
        $mpos = Mpos::find($objekts->mpos_id);
        if (!isset($mpos->id))
        {
            return Redirect::to('etickets/'.$this->links)->with('error', 'Kļūda atlasot MPOS!');
        }

        try {
            DB::transaction(function() use($objekts,$account,$et,$mpos)
            {
                // create reverse TMP transaction
                $t = New Transaction;
                $t->amount = $objekts->content;
                $t->operation_id = 1;
                $t->from_account_id = 999997; // tmp account
                $t->to_account_id = $account->id;
                $t->to_eticket_nr = $et->nr;
                $t->mpos_id = $objekts->mpos_id;
                $t->object_id = $mpos->object_id;
                $t->t_status = 'P';
                $t->confirmed = 1;
                $t->comments = Auth::user()->id;

                if (!$t->save()) throw New Exception();

                // increase account balance
                $account->balance = $account->balance + $objekts->content;

                if (!$account->save()) throw New Exception();

                // increase e-ticket balance field
                $et->balance = $et->balance + $objekts->content;

                if (!$et->save()) throw New Exception();
            });
        }
        catch(Exception $e) {
            return Redirect::to('etickets/'.$this->links)
                ->withInput()
                ->with('error','Kļūda saglabājot datus!');
        }

        $objekts->ticket_status = 1;
        $objekts->completed_when = date("Y-m-d H:i:s");

        if ($objekts->save())
        {
            return Redirect::to('etickets/'.$this->links)->with('success', Lang::get('etickets/'.$this->links.'/messages.confirm.success'));
        }
        else
        {
            return Redirect::to('etickets/'.$this->links)->with('error', Lang::get('etickets/'.$this->links.'/messages.confirm.error'));
        }
    }
}