<?php

namespace App\Http\Controllers\etickets;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\OwnerAccess;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class DeleteController extends BaseController {

    protected $links = 'delete';

    /**
     * Show the form for deleting profile
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = 'Dzēst profilu';

        // balance
        $owner = OwnerAccess::Ao()->U(Auth::user()->id)->first()->owner->id;
        $account = AccountAccess::Ao()->U(Auth::user()->id)->first()->account;
        if ($account->owner_id==$owner)
        {
            $balance = sprintf("%.2f",$account->balance/100);
        }
        else throw new \Exception('no account');

        // Show the page
        return View::make('etickets/'.$this->links.'/create_edit', compact('title', 'mode','balance'));
    }

    /**
     * Delete profile
     *
     * @return Response
     */
    public function postIndex()
    {

        $password = Input::get( 'password' );

        $owner_id = OwnerAccess::Ao()->U(Auth::user()->id)->first()->owner->id;

        $credentials = array(
            'email' => Auth::user()->email,
            'password' => $password
        );
        if (!Auth::validate($credentials))
        {
            return Redirect::to('etickets/'.$this->links)->withInput()->with('error', 'Nepareiza parole!');
        }

        // balance
        $account = AccountAccess::Ao()->U(Auth::user()->id)->first()->account;
        if ($account->owner_id!=$owner_id)
        {
            return Redirect::to('etickets/'.$this->links)->withInput()->with('error', 'Kļūda atlasot kontu!');
        }

        $u = User::find(Auth::user()->id);
        $user_id = $u->id;

        if ($u->delete())
        {
            //find owner
            $oa = OwnerAccess::Ao()->U($user_id)->first();
            if (isset($oa->id))
            {
                // account disable
                $acc = Account::O($oa->owner_id);
                if (isset($acc->id))
                {
                    $acc->account_status=0;
                    $acc->save();
                }
            }

            // oener access delete
            OwnerAccess::Sub()->where('user_id',$user_id)->delete();
            OwnerAccess::Sub()->where('owner_id',$user_id)->delete();
            
            Auth::logout();

            return Redirect::to('/user/login')->with('success', 'Lietotājs dzēsts!');
        }
        else {
            return Redirect::to('etickets/'.$this->links)->withInput()->with('error', 'Kļūda dzēšot lietotāju!');
        }
    }

}