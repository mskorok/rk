<?php
namespace App\Http\Controllers\etickets;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Eticket;
use App\Models\ManualPayment;
use App\Models\OwnerAccess;
use App\Models\Settings;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Barryvdh\DomPDF\PDF;

class TransferController extends BaseController
{
    protected $links = 'mtransfer';

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\View | RedirectResponse
     */
    /*
    public function getIndex()
    {
        $redirect = config('auth.views.web.not_in_terms');
        // check for confirmed user
        if ((int)\Auth::user()->terms !== 1) {
            return redirect()->to($redirect);
//            return Redirect::to('/'); // todo REDIRECT
        }

        // Title
        $title = 'Papildināt kontu';

        // Mode
        $mode = 'create';

        // get all owners for user
        $all_owners = array();
        $owners = OwnerAccess::with('owner')->ao()->u(\Auth::user()->id)->get();
        foreach ($owners as $o) {
            array_push($all_owners, $o->owner->id);
        }

        // get granted ownerships for user
        $granted = \Auth::user()->grantedOwnerships();
        foreach ($granted as $o) {
            array_push($all_owners, $o->owner_id);
        }

        // get all eticket accounts (type=2) for owners
        $all_accounts = array();
        $accounts = Account::whereIn('owner_id', $all_owners)->et()->get();
        foreach ($accounts as $a) {
            array_push($all_accounts, $a->id);
        }

        if (count($all_accounts) == 0) {
            $all_accounts = array('0');
        }

        $all_etickets = Eticket::whereIn('account_id', $all_accounts)
            ->where('group_id', 0)
            ->get();

        $info = str_replace("\\n", "<br />", Settings::first()->payment_form_message);

        // Show the page
        return View::make(
            'etickets/' . $this->links . '/create_edit',
            compact('title', 'mode', 'all_etickets', 'info')
        );
    }
     * 
     */

    /**
     * Store a newly created resource in storage.
     *
     * @return Redirect |View |RedirectResponse
     */
    /*
    public function postCreate()
    {
        $redirect = config('auth.views.web.not_in_terms');
        // check for confirmed user
        if ((int)\Auth::user()->terms !== 1) {
            return redirect()->to($redirect);
//            return Redirect::to('/'); // todo REDIRECT
        }

        $amount = Input::get('summa');
        $eticket_nr = Input::get('eticket');
        $eticket = Eticket::findOrFail($eticket_nr);
        $user = Auth::user();

        $owner = OwnerAccess::ao()->u($user->id)->first()->owner;
        $account = AccountAccess::ao()->u($user->id)->first()->account;

        if ($account->owner_id != $owner->id) {
            return Redirect::to('etickets/mtransfer')
                ->withInput()
                ->withErrors('Cant find account');
        }
        
        // check for three unconfimed payments for this eticket
        $waiting = ManualPayment::where("user_id", $user->id)
            ->where("confirmed", 0)
            ->where("eticket_id", $eticket->id)
            ->count();

        if ($waiting >= 3) {
            return Redirect::back()
                ->with('error', 'Šim e-talonam jau ir izveidoti ' . $waiting . ' neapstiprināti maksājumi!');
        }

        $amount = (int) round($amount);
        $amount *= 100;
        
        if ($amount <= 0) {
            return Redirect::back()->with('error', 'Nekorekta summa!');
        }
        // check for monthly limit in single payment
        if ($amount > 14000) {
            return Redirect::back()
                ->with('error', 'Maksājuma summa nedrīkst pārsniegt mēneša limitu - 140.00 EUR!');
        }

        // payment identification
        $payment_code = strtolower(str_random(10));

        $t = new ManualPayment();

        $t->user_amount = $amount;
        $t->user_id = Auth::user()->id;
        $t->eticket_id = $eticket->id;
        $t->payment_code = $payment_code;
        $t->confirmed = 0;

        if (!$t->save()) {
            // Redirect on failure
            return Redirect::to('etickets/mtransfer')
                ->withInput()
                ->withErrors($t->getErrors());
        }

        Cache::forget('user_balance_' . $user->id);

        // get settings data
        $rekviziti = str_replace("\\n", "<br />", Settings::first()->payment_form_details);

        return Redirect::to('etickets/mtransfer')
            ->with('success', 'Konta papildināšanas maksājuma uzdevums izveidots veiksmīgi.<br />
            <br />
            Maksājuma veikšanai izmantojamie rekvizīti:
            <br />
            ' . $rekviziti . '
            <br />
            <br />
            Veicot maksājumu tā detaļās norādiet unikālo identifikatoru: <b>' . $payment_code . '</b>
            <br />
            <br />
            Maksājuma uzdevumu PDF formātā varat lejuplādēt - <a href="/etickets/mtransfer/pdf/' . $t->id . '/">ŠEIT</a>');
    }
     * 
     */

    public function getPdf($id)
    {
        $manual_payment = ManualPayment::findOrFail($id);

        if ($manual_payment->user_id != Auth::user()->id) {
            return Redirect::to('etickets');
        }

        // get settings data
        $rekviziti = str_replace("\\n", "<br />", Settings::first()->payment_form_details);

        $html = '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
                <head profile="http://gmpg.org/xfn/11">
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><style tyle="text/css">body { font-family: DejaVu Sans, sans-serif; }</style></head><body>' .
            View::make('etickets/mtransfer/pdf', compact('manual_payment', 'rekviziti'))
            . '</body></html>';

        return \PDF::loadHTML($html)->download('maksajuma_uzdevums.pdf');
    }
}
