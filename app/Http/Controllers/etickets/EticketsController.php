<?php

namespace App\Http\Controllers\etickets;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Dotations;
use App\Models\Eticket;
use App\Models\EticketLimits;
use App\Models\EticketsBase;
use App\Models\EticketsObjekti;
use App\Models\GroupObjekts;
use App\Models\ObjectDotationPrices;
use App\Models\Objekts;
use App\Models\OwnerAccess;
use App\Models\School;
use App\Models\SchoolPrices;
use App\Models\Transaction;
use App\Models\Upload;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Vinkla\Hashids\Facades\Hashids;

class EticketsController extends BaseController
{

    protected $links = 'etickets';

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Eticket Model
     * @var Eticket
     */
    protected $etickets;

    public function __construct(Eticket $etickets)
    {
        parent::__construct();
        $this->etickets = $etickets;
    }

    /**
     * Display a listing of resources
     *
     * @return Response | \Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('etickets/' . $this->links . '/title.management');

        // Grab all
        $data = $this->etickets;

        // Show the page
        return View::make('etickets/' . $this->links . '/index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response | \Illuminate\Contracts\View\View | RedirectResponse
     */
    public function getCreate()
    {
        $redirect = config('auth.views.web.not_in_terms');
        // check for confirmed user
        if ((int) \Auth::user()->terms !== 1) {
            return redirect()->to($redirect);
//            return Redirect::to('/'); // todo REDIRECT
        }

        $limiti = EticketLimits::get();
        $skolas = School::where("reg_nr", '!=', '')
                        ->orderBy("heading", "ASC")
                        ->has('objekts')
                        ->lists("heading", "id")->toArray();

        $thumb_name = '';

        // Title
        $title = Lang::get('etickets/' . $this->links . '/title.create_a_new_obj');

        // Mode
        $mode = 'create';

        // Show the page
        return View::make(
                        'etickets/' . $this->links . '/create_edit', compact('title', 'mode', 'limiti', 'thumb_name', 'skolas')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response |RedirectResponse
     */
    /*
      public function postCreate()
      {

      // check for confirmed user
      if ((int) \Auth::user()->terms !== 1) {
      return redirect()->to($redirect);
      //            return Redirect::to('/'); // todo REDIRECT
      }

      $nr = trim(Input::get('nr'));
      $pk = trim(Input::get('pk'));
      $name = trim(strip_tags(Input::get('name')));
      $pin = trim(strip_tags(Input::get('pin')));
      $grade = trim(strip_tags(Input::get('grade')));
      $limit_type = (int) Input::get('limit_type');
      $skola = (int) Input::get('skola');

      if (strlen($nr) != 10) {
      return Redirect::back()
      ->withInput()
      ->with('error', 'Nekorekts e-talona numurs!');
      }

      if (strlen($pk)!=11 && (strlen($pk)!=12 && $pk[6]!='-')) {
      return Redirect::back()
      ->withInput()
      ->with('error', 'Nekorekts personas kods!');
      } else {
      if (strlen($pk) == 11) {
      $pk = substr($pk, 0, 6).'-'.substr($pk, 6, 5);
      }
      }

      if (! preg_match('/^[0-9]{1,2}[\.]?[a-z]{0,1}$/', $grade)) {
      return Redirect::back()
      ->withInput()
      ->with('error', 'Ievadīta nekorekta klase!');
      } else {
      $grade = strtolower(str_replace(".", "", $grade));
      }

      // get object
      try {
      $owner_id = OwnerAccess::Ao()->U(Auth::user()->id)->first()->owner->id;
      $object_id = Objekts::where("object_regnr", School::findOrFail($skola)->reg_nr)->first()->id;
      } catch (\Exception $e) {
      Log::error("create e-ticket - get object|owner error - " . json_encode($e->getMessage()));

      return Redirect::back()
      ->withInput()
      ->with('error', 'Pakalpojums nav pieejams šajā mācību iestādē!');
      }

      if ($object_id <= 0) {
      return Redirect::back()->withInput()->with('error', 'Pakalpojums nav pieejams šajā mācību iestādē!');
      }

      // make translit name for comparison
      $name_translit = $this->translit($name);

      // check from pre-fetched free meal persons table
      $base = EticketsBase::pk($nr, $pk)
      ->where('custom', '<=', 1)
      ->where('brivpusdienas', 1)
      ->first();

      $already_exists = Eticket::a()->nr($nr)->first();
      if (isset($already_exists->id)) {
      // check for auto created free meal e-ticket
      if (! isset($already_exists->account->owner_id)) {
      return Redirect::back()->withInput()->with('error', 'Šāds e-talons jau ir reģistrēts sistēmā!');
      }

      if ($already_exists->account->owner_id !== 2) {
      return Redirect::back()->withInput()->with('error', 'Šāds e-talons jau ir reģistrēts sistēmā!');
      }
      }

      // check if e-ticket is created and inactive
      $oldet = Eticket::nr($nr)->where('eticket_status', 0)->first();
      if ($oldet && $base === null) {
      try {
      // check for old account
      $acc = Account::find($oldet->account_id);
      if ($acc) {
      $acc->owner_id = $owner_id;
      $acc->account_status = 1;
      if (!$acc->save()) {
      throw new \Exception("Could not update an account", 1);
      }
      } else {
      // create account
      $acc = new Account;
      $acc->account_type = 2;
      $acc->account_status = 1;
      $acc->owner_id = $owner_id;
      if (!$acc->save()) {
      throw new \Exception("Could not create an account", 1);
      }
      }

      $oldet->eticket_status = 1;
      $oldet->pin = $pin;
      $oldet->name = $name;

      if (!$oldet->save()) {
      throw new \Exception("Could not update an eticket", 1);
      }

      $oldet->DetachObjekti();
      $oldet->saveObjekti([$object_id], 1); //1=create new default

      $eticket_id = $oldet->id;
      } catch (\Exception $e) {
      Log::error("create e-ticket - DB restore error - " . json_encode($e->getMessage()));

      return Redirect::back()
      ->withInput()
      ->with('error', 'Kļūda saglabājot datus!');
      }
      }

      // take from free meals table, without API call
      if ($base !== null && isset($base->id)) {
      // compare data
      $base_fullname = $base->name.' '.$base->surname;
      $base_name_translit = $this->translit($base_fullname);
      if (levenshtein($base_name_translit, $name_translit) > 1) {
      return Redirect::back()->withInput()->with('error', 'Kļūdains personas vārds vai uzvārds!');
      }

      $name = $base_fullname;

      if (levenshtein($grade, $base->grade) > 1) {
      return Redirect::back()->withInput()->with('error', 'Kļūdaini norādīta klase!');
      }

      $grade = $base->grade;

      if ($skola !== $base->school_id) {
      return Redirect::back()->withInput()->with('error', 'Kļūdaini norādīta skola!');
      }

      // add to monitoringlist
      $monitoring = $this->add_pk_to_monitoring_list($pk);
      if ($monitoring !== 'OK') {
      return Redirect::back()->withInput()->with('error', 'Kļūda apstrādājot datus!');
      }

      // everything ok - save
      $mas = ["DateFrom" => $base->free_meals_from, "DateTill" => $base->free_meals_until];
      $mas2 = ["DateFrom" => $base->second_meals_from, "DateTill" => $base->second_meals_until];
      $mas3 = ["DateFrom" => $base->third_meals_from, "DateTill" => $base->third_meals_until];
      $mas4 = ["DateFrom" => $base->kopgalds_from, "DateTill" => $base->kopgalds_until];
      $eticket_id = $this->save_new_eticket(
      $nr,
      $pk,
      $name,
      $pin,
      $grade,
      $limit_type,
      $owner_id,
      $object_id,
      $skola,
      $mas,
      $mas2,
      $mas3,
      $mas4
      );

      if (!is_int($eticket_id)) {
      return Redirect::back()->withInput()->with('error', $eticket_id);
      }
      } else {
      // make API calls
      $results = $this->create_eticket_api_calls($pk);

      if ($results['r1'] === 'error' || $results['r2'] === 'error') {
      return Redirect::back()->withInput()->with('error', 'Kļūda parbaudot datus, lūdzu mēģiniet vēlreiz!');
      }

      if ($results['r1'] === 'error1' || $results['r2'] === 'error1') {
      return Redirect::back()->withInput()->with('error', 'Kļūda, šādi dati nav atrasti!');
      }

      if ($results['r1'] === 0) {
      return Redirect::back()->withInput()->with('error', 'E-talons ar šādu numuru nav atrasts!');
      }

      if ($results['r1'] !== $results['r2']['pk']) {
      return Redirect::back()->withInput()
      ->with('error', 'E-talons ar šādu numuru nav atrasts šai personai!');
      }

      if ($results['r1'] !== $results['r2']['pk']) {
      return Redirect::back()->withInput()
      ->with('error', 'E-talons ar šādu numuru nav atrasts šai personai!');
      }

      if (!isset($results['r2']['School']['SchoolId'])) {
      return Redirect::back()->withInput()
      ->with('error', "Šāda persona nav atrasta norādītajā mācību iestādē!");
      }

      if ($results['r2']['School']['SchoolId'] != $skola) {
      return Redirect::back()->withInput()
      ->with('error', "Šāda persona nav atrasta norādītajā mācību iestādē!");
      }

      if ((int) $grade !== (int) $results['r2']['School']['SchoolClass']) {
      return Redirect::back()->withInput()->with('error', 'Šāda persona nav atrasta norādītajā klasē!');
      }

      if (levenshtein($name_translit, $this->translit($results['r2']['name'])) > 1) {
      return Redirect::back()->withInput()->with('error', 'Kļūdains personas vārds vai uzvārds!');
      }

      // add to monitoringlist
      $monitoring = $this->add_pk_to_monitoring_list($pk);
      if ($monitoring !== 'OK') {
      return Redirect::back()->withInput()->with('error', 'Kļūda apstrādājot datus!');
      }

      $mas = isset($results["r2"]["FreeMeals"]["FreeMealPeriod"])
      ? App::make('App\Console\Commands\BaseCommand')
      ->find_current_periods($results["r2"]["FreeMeals"]["FreeMealPeriod"])
      : ['0000-00-00','0000-00-00'];
      $mas2 = isset($results["r2"]["UnusedMeals"]["UnusedMeal"])
      ? App::make('App\Console\Commands\BaseCommand')
      ->find_current_periods($results["r2"]["UnusedMeals"]["UnusedMeal"])
      : ['0000-00-00','0000-00-00'];
      $mas3 = isset($results["r2"]["RdMeals"]["RdMeal"])
      ? App::make('App\Console\Commands\BaseCommand')
      ->find_current_periods($results["r2"]["RdMeals"]["RdMeal"])
      : ['0000-00-00','0000-00-00'];
      $mas4 = isset($results["r2"]["TableRdMeals"]["TableRdMeal"])
      ? App::make('App\Console\Commands\BaseCommand')
      ->find_current_periods($results["r2"]["TableRdMeals"]["TableRdMeal"])
      : ['0000-00-00','0000-00-00'];

      // everything ok - save
      $eticket_id = $this->save_new_eticket(
      $nr,
      $pk,
      $results["r2"]["name"],
      $pin,
      $results["r2"]["School"]["SchoolClass"],
      $limit_type,
      $owner_id,
      $object_id,
      $skola,
      $mas,
      $mas2,
      $mas3,
      $mas4
      );

      if (! is_int($eticket_id)) {
      return Redirect::back()->withInput()->with('error', $eticket_id);
      }
      }

      // thumbnail upload
      if (Input::file()) {
      try {
      $fails = array('image' => Input::file('file'));
      $rules = array('image' => 'required|mimes:jpeg,png,gif|max:10000');
      $validator = Validator::make($fails, $rules);
      if ($validator->fails()) {
      throw new \Exception(
      "Kļūda apstrādājot fotogrāfiju! Atļauti JPG , PNG un GIF attēli ar izmēru līdz 10MB!"
      );
      }

      if (! Input::file('file')->isValid()) {
      throw new \Exception(
      "Kļūda apstrādājot fotogrāfiju! Atļauti JPG , PNG un GIF attēli ar izmēru līdz 10MB!"
      );
      }

      $image = Input::file('file');
      $filename  = $nr . '.' . $image->getClientOriginalExtension();

      $path = public_path('avatars/' . $filename);

      Image::make($image->getRealPath())->resize(100, 100)->save($path);

      $upl = new \App\Models\Image();
      $upl->filename = $filename;
      $upl->path = 'avatars/';
      $upl->extension = $image->getClientOriginalExtension();
      $upl->user_id = Auth::user()->id;
      $upl->parent_id = 0;
      $upl->save();

      // add to e-ticket
      $updateo = Eticket::find($eticket_id);
      $updateo->thumb_id = $upl->id;
      $updateo->save();
      } catch (\Exception $exception) {
      // Something went wrong. Log it.
      Log::error($exception);
      // Return error
      return Redirect::to('etickets/'.$this->links.'/' . Hashids::encode($eticket_id) . '/edit')
      ->withInput()
      ->with(
      'error',
      'Kļūda apstrādājot fotogrāfiju! Atļauti JPG , PNG un GIF attēli ar izmēru līdz 10MB!'
      );
      }
      }

      // operations
      DB::table('eticket_opers')->insert(
      array('eticket_id' => $eticket_id, 'operation_id' => 1)
      );

      // update user active e-tickets cache
      Cache::forget('active_etickets_'.Auth::user()->id);
      Cache::forget('all_active_etickets_'.Auth::user()->id);
      Cache::forget('user_accounts_et_'.Auth::user()->id);
      Cache::forget('user_accounts_etg_'.Auth::user()->id);

      return Redirect::to('etickets/'.$this->links.'/' . Hashids::encode($eticket_id) . '/edit')
      ->with('success', Lang::get('etickets/'.$this->links.'/messages.create.success'));
      }
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param $objekts
     * @return Response
     */
    public function getEdit($objekts = '')
    {
        if (isset($objekts->id) AND ( $objekts->eticket_status < 3)) {
            $limiti = EticketLimits::get();

            // talonam pieejamie objekti
            $obj = $objekts->activeObjekti();

            if (count($obj) < 1)
                return Redirect::back()->with('error', 'Kļūda! E-talons nav piesaistits nevienam objektam, lūdzu sazinieties ar sistēmas administratoru!');

            $objconng = GroupObjekts::whereIn('object_id', $obj)->lists('ogroup_id')->toArray();
            if (count($objconng) > 0) {
                $objconn = GroupObjekts::whereIn('ogroup_id', $objconng)->whereNotIn('object_id', $obj)->lists('object_id')->toArray();
            } else
                $objconn = array();
            $obj = array_merge($obj, $objconn);

            $objekti = Objekts::whereIn('id', $obj)->get();

            // thumbnail
            $thumb_name = '';
            try {
                if ($objekts->thumb_id > 0) {
                    $upl = \App\Models\Image::where('parent_id', 0)->where('id', $objekts->thumb_id)->first();
                    $thumb_name = $upl->path . $upl->filename;
                }
            } catch (\Exception $exception) {
                Log::error($exception);
            }

            // Title
            $title = Lang::get('etickets/' . $this->links . '/title.obj_edit');
            // mode
            $mode = 'edit';

            return View::make('etickets/' . $this->links . '/create_edit', compact('objekts', 'title', 'mode', 'limiti', 'objekti', 'thumb_name'));
        } else
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postEdit($objekts)
    {
        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            $updateo = Eticket::find($objekts->id);

            $pin = substr(trim(Input::get('pin')), 0, 4);
            if (strlen($pin) < 4)
                $pin = str_pad($pin, 4, "0", STR_PAD_LEFT);

            // update
            $updateo->pin = $pin;
            $updateo->limit_type = Input::get('limit_type');
            $updateo->limit = Input::get('limit') * 100;

            // thumbnail upload
            if (Input::file()) {
                try {
                    $fails = array('image' => Input::file('file'));
                    $rules = array('image' => 'required|mimes:jpeg,png,gif|max:10000');
                    $validator = Validator::make($fails, $rules);
                    if ($validator->fails()) {
                        throw new \Exception("Kļūda apstrādājot fotogrāfiju! Atļauti JPG , PNG un GIF attēli ar izmēru līdz 10MB!");
                    }

                    if (!Input::file('file')->isValid()) {
                        throw new \Exception("Kļūda apstrādājot fotogrāfiju! Atļauti JPG , PNG un GIF attēli ar izmēru līdz 10MB!");
                    }

                    $image = Input::file('file');
                    $filename = $updateo->nr . '.' . $image->getClientOriginalExtension();

                    $path = public_path('avatars/' . $filename);

                    Image::make($image->getRealPath())->resize(100, 100)->save($path);

                    $upl = new \App\Models\Image();
                    $upl->filename = $filename;
                    $upl->path = 'avatars/';
                    $upl->extension = $image->getClientOriginalExtension();
                    $upl->user_id = Auth::user()->id;
                    $upl->parent_id = 0;
                    $upl->save();

                    $updateo->thumb_id = $upl->id;
                } catch (\Exception $exception) {
                    // Something went wrong. Log it.
                    Log::error($exception);
                    // Return error
                    return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/edit')
                                    ->withInput()
                                    ->with('error', 'Kļūda apstrādājot fotogrāfiju! Atļauti JPG , PNG un GIF attēli ar izmēru līdz 10MB!');
                }
            }

            if ($updateo->save()) {
                Cache::forget("all_active_etickets_" . Auth::user()->id);
                Cache::forget("active_etickets_" . Auth::user()->id);

                return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/edit')->with('success', Lang::get('etickets/' . $this->links . '/messages.edit.success'));
            } else {
                // Redirect on failure
                return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/edit')
                                ->withInput()
                                ->withErrors($updateo->getErrors());
            }
        } else {
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Disable resource page.
     *
     * @param $objekts
     * @return Response
     */
    public function getDisable($objekts)
    {
        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            // Title
            $title = Lang::get('etickets/' . $this->links . '/title.obj_disable') . ' - ' . $objekts->nr;

            // Show the page
            return View::make('etickets/' . $this->links . '/disable', compact('objekts', 'title'));
        } else {
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Disable resource
     *
     * @param $objekts
     * @return Response
     */
    public function postDisable($objekts)
    {

        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            $id = $objekts->id;

            $objekts = Eticket::find($id);

            $objekts->eticket_status = 2;

            if ($objekts->save()) {
                // update user active e-tickets cache
                Cache::forget('active_etickets_' . Auth::user()->id);
                Cache::forget("all_active_etickets_" . Auth::user()->id);

                return Redirect::to('etickets/' . $this->links)->with('success', Lang::get('etickets/' . $this->links . '/messages.disable.success'));
            } else
                return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.disable.error'));
        } else
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
    }

    /**
     * Enable resource page.
     *
     * @param $objekts
     * @return Response | \Illuminate\Contracts\View\View
     */
    public function getEnable($objekts)
    {
        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            // Title
            $title = Lang::get('etickets/' . $this->links . '/title.obj_enable') . ' - ' . $objekts->nr;

            // Show the page
            return View::make('etickets/' . $this->links . '/enable', compact('objekts', 'title'));
        } else {
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Enable resource
     *
     * @param $objekts
     * @return Response
     */
    public function postEnable($objekts)
    {

        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            $id = $objekts->id;

            $objekts = Eticket::find($id);

            $objekts->eticket_status = 1;

            if ($objekts->save()) {
                // update user active e-tickets cache
                Cache::forget('active_etickets_' . Auth::user()->id);
                Cache::forget("all_active_etickets_" . Auth::user()->id);

                return Redirect::to('etickets/' . $this->links)->with('success', Lang::get('etickets/' . $this->links . '/messages.enable.success'));
            } else {
                return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.enable.error'));
            }
        } else {
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Transfer money page.
     *
     * @param $objekts
     * @return Response | \Illuminate\Contracts\View\View
     */
    public function getPut($objekts)
    {
        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            // users balance
            $user_id = Auth::user()->id;

            $owner = OwnerAccess::Ao()->U($user_id)->first()->owner;
            $account = AccountAccess::Ao()->U($user_id)->first()->account;

            if ($account->owner_id != $owner->id) {
                return Redirect::to('etickets/' . $this->links)->withInput()->with('error', 'Cant find account');
            }

            $user_balance = $account->balance / 100;

            // e-ticket balance
            $et_acc = Account::find($objekts->account_id);
            $group_balance = $et_acc->balance / 100;

            // Title
            $title = Lang::get('etickets/' . $this->links . '/title.obj_put') . ' - ' . $objekts->nr;

            // Show the page
            return View::make(
                            'etickets/' . $this->links . '/put', compact('objekts', 'title', 'user_balance', 'group_balance')
            );
        } else {
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Transfer money to e-ticket
     *
     * @param $objekts
     * @return Response
     */
    public function postPut($objekts)
    {
        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            $sum = intval(Input::get('sum') * 100);
            if (round($sum) == 0) {
                return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/put')
                                ->withInput()->with('error', 'Jānorāda summa!');
            }

            $sign = ($sum > 0) ? '+' : '-';
            $sum = abs($sum);

            $user_id = Auth::user()->id;

            $owner = OwnerAccess::ao()->u($user_id)->first()->owner;
            $account = AccountAccess::ao()->u($user_id)->first()->account;

            if ($account->owner_id != $owner->id) {
                return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/put')
                                ->withInput()->with('error', 'Cant find account');
            }

            if ($sign == '+') {
                if ($account->balance < $sum) {
                    return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/put')
                                    ->withInput()->with('error', 'Nepietiek līdzekļu!');
                }
            }

            $id = $objekts->id;

            $eticket = Eticket::find($id);

            // eticket account
            $et_acc = Account::find($eticket->account_id);

            if ($sign == '-') {
                if ($et_acc->balance < $sum) {
                    return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/put')
                                    ->withInput()->with('error', 'Nepietiek līdzekļu!');
                }
            }

            try {
                DB::transaction(function() use($eticket, $account, $sum, $et_acc, $sign) {
                    if ($sign == '+') {
                        $eticket->increment('balance', $sum);
                        $account->decrement('balance', $sum);
                        $et_acc->increment('balance', $sum);
                    } else {
                        $eticket->decrement('balance', $sum);
                        $account->increment('balance', $sum);
                        $et_acc->decrement('balance', $sum);
                    }

                    $eticket->save();
                    $account->save();
                    $et_acc->save();

                    // create transaction
                    $t = New Transaction;

                    $t->amount = $sum;
                    $t->operation_id = 1;
                    if ($sign == '+') {
                        $t->from_account_id = $account->id;     // Owner account
                        $t->to_account_id = $et_acc->id;      // E-ticket account
                        $t->to_eticket_nr = $eticket->nr;
                    } else {
                        $t->from_account_id = $et_acc->id;
                        $t->to_account_id = $account->id;
                        $t->from_eticket_nr = $eticket->nr;
                    }
                    $t->confirmed = 1;
                    $t->save();
                });
            } catch (\Exception $e) {
                return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/put')
                                ->with('error', Lang::get('etickets/' . $this->links . '/messages.put.error'));
            }

            // update user balance cache
            Cache::forget('user_balance_' . $user_id);


            return Redirect::to('etickets/' . $this->links . '/' . Hashids::encode($objekts->id) . '/put')
                            ->with('success', Lang::get('etickets/' . $this->links . '/messages.put.success'));
        } else {
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Delete resource page.
     *
     * @return Response
     */
    public function getDel($objekts)
    {
        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            // Title
            $title = Lang::get('etickets/' . $this->links . '/title.obj_delete') . ' - ' . $objekts->nr;

            // Show the page
            return View::make('etickets/' . $this->links . '/delete', compact('objekts', 'title'));
        } else {
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Delete resource
     *
     * @return Response
     */
    public function postDel($objekts)
    {
        if (( $objekts->id ) AND ( $objekts->eticket_status < 3)) {
            // get all owners for user
            $all_owners = array();
            $owners = OwnerAccess::with('owner')->Ao()->U(Auth::user()->id)->get();
            foreach ($owners as $o) {
                array_push($all_owners, $o->owner->id);
            }

            // get granted ownerships for user
            /** @var array $granted */
            $granted = Auth::user()->grantedOwnerships();
            foreach ($granted as $o) {
                array_push($all_owners, $o->owner->id);
            }

            // get e-ticket account owner
            $acc = Account::Et()->find($objekts->account_id);
            $acc_owner = $acc->owner_id;

            // check if active user is owner, and operation is permitted
            if (in_array($acc_owner, $all_owners)) {
                // check balance
                if ($objekts->balance > 0 || $acc->balance > 0)
                    return Redirect::to('etickets/' . $this->links . '/')->with('error', 'Kļūda dzēšot e-talonu!');

                // check if free meal ticket
                $free_meal = 0;
                $base = EticketsBase::where("pk", $objekts->pk)->first();
                if (isset($base->id)) {
                    $base_etickets = explode(",", $base->etickets);
                    if (in_array($objekts->nr, $base_etickets))
                        $free_meal = 1;
                }

                try {
                    DB::transaction(function() use($objekts, $acc, $free_meal) {
                        if ($free_meal == 1) {
                            $acc->owner_id = 2;
                            $acc->save();

                            $objekts->pin = '0000';
                            $objekts->limit_type = 0;
                            $objekts->limit = 0;
                            $objekts->save();
                        } else {
                            // delete e-ticket
                            $objekts->eticket_status = 0;
                            $objekts->save();

                            // delete account owner and deactivate
                            $acc->owner_id = 0;
                            $acc->account_status = 0;
                            $acc->save();

                            // delete objects
                            $objekts->DetachObjekti();

                            // delete e-ticket operations
                            DB::table('eticket_opers')->where('eticket_id', $objekts->id)->delete();
                        }
                    });
                } catch (\Exception $e) {
                    return Redirect::to('etickets/' . $this->links . '/')->with('error', 'Kļūda dzēšot e-talonu!');
                }
            } else {
                return Redirect::to('etickets/' . $this->links . '/')->with('error', 'Neizdevās dzēst e-talonu, nav tiesību!');
            }

            // update user active e-tickets cache
            Cache::forget('active_etickets_' . Auth::user()->id);
            Cache::forget("all_active_etickets_" . Auth::user()->id);

            return Redirect::to('etickets/' . $this->links . '/')->with('success', 'E-talons veiksmīgi dzēsts.');
        } else {
            return Redirect::to('etickets/' . $this->links)->with('error', Lang::get('etickets/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Show a list of all the e-tickets formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        // get all owners for user
        $all_owners = array();
        $owners = OwnerAccess::with('owner')->Ao()->U(Auth::user()->id)->get();
        foreach ($owners as $o) {
            array_push($all_owners, $o->owner->id);
        }

        // get granted ownerships for user
        /** @var array $granted */
        $granted = Auth::user()->grantedOwnerships();
        foreach ($granted as $o) {
            array_push($all_owners, $o->owner_id);
        }

        // get all eticket accounts (type=2) for owners
        $all_accounts = Account::whereIn("owner_id", $all_owners)->Et()->lists("id")->all();

        if (count($all_accounts) == 0)
            $all_accounts = ['0'];

        $data = Eticket::leftjoin('eticket_statuses', 'etickets.eticket_status', '=', 'eticket_statuses.id')->
                leftjoin('eticket_limit_types', 'etickets.limit_type', '=', 'eticket_limit_types.id')
                ->leftJoin('uploads', function($join) {
                    $join->on('etickets.thumb_id', '=', 'uploads.id');
                    $join->on(DB::raw('uploads.parent_id=0 '), DB::raw(''), DB::raw(''));
                })
                ->select(array('etickets.id', 'etickets.thumb_id', 'uploads.filename', 'uploads.path', 'nr', 'eticket_statuses.name', 'etickets.eticket_status', 'pin', 'balance', 'eticket_limit_types.name as tname', 'limit', 'etickets.created_at', 'etickets.name as etname'))->
                whereIn('account_id', $all_accounts)
                ->where('group_id', 0);

        return Datatables::of($data)
                        ->edit_column('tname', '{!! $tname !!}&nbsp;')
                        ->edit_column('nr', '{!! $nr !!}<br /> {!! $etname !!} ')
                        ->edit_column('limit', '{!! sprintf("%.2f",$limit/100) !!} EUR')
                        ->edit_column('balance', '{!! sprintf("%.2f",$balance/100) !!} EUR')
                        ->edit_column('path', '@if ($path!=\'\')
                        <img width="100" src="/{!! $path.$filename !!}" />
                    @else
                        <img width="100" src="/assets/img/no_avatar.jpg" />
                    @endif')
                        ->add_column('actions', '
            @if($eticket_status<\'3\')
            <div style="width:180px;"><a href="{{{ URL::to(\'etickets/' . $this->links . '/\' . Hashids::encode($id) . \'/edit\' ) }}}" class="iframe btn btn-xs btn-primary">{{{ Lang::get(\'button.edit\') }}}</a>
            @endif
            @if($eticket_status==\'1\')
                <a href="{{{ URL::to(\'etickets/' . $this->links . '/\' . Hashids::encode($id) . \'/disable\' ) }}}" class="btn btn-xs btn-primary">{{{ Lang::get(\'button.disable\') }}}</a>
            @elseif ($eticket_status==\'2\')
                <a href="{{{ URL::to(\'etickets/' . $this->links . '/\' . Hashids::encode($id) . \'/enable\' ) }}}" class="btn btn-xs btn-primary">{{{ Lang::get(\'button.enable\') }}}</a>
            @endif
            @if($eticket_status<\'3\')
            <a href="{{{ URL::to(\'etickets/' . $this->links . '/\' . Hashids::encode($id) . \'/put\' ) }}}" class="btn btn-xs btn-primary">{{{ Lang::get(\'button.put\') }}}</a></div>
            @endif')
                        ->add_column('del', '
            @if($eticket_status<\'3\')
            <a href="{{{ URL::to(\'etickets/' . $this->links . '/\' . Hashids::encode($id) . \'/del\' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            @endif')
                        ->remove_column('id')
                        ->remove_column('eticket_status')
                        ->remove_column('etname')
                        ->remove_column('thumb_id')
                        ->remove_column('filename')
                        ->make();
    }

    public function getWait()
    {
        $title = 'E-talons izveidots veiksmīgi';

        return View::make('etickets/' . $this->links . '/wait', compact('title'));
    }

    public function getFetchDefObj($q)
    {
        // get all user accounts
        $object_id = EticketsBase::where('etickets', 'LIKE', '%' . $q . '%')->where('custom', '<=', 1)->lists('object_id')->toArray();
        if (isset($object_id[0])) {
            $object = Objekts::select(['id', 'object_name'])->where('id', $object_id[0])->first();
        } else
            $object = [];

        return response()->json($object);
    }

    public function save_new_eticket($nr, $pk, $name, $pin, $grade, $limit_type, $owner_id, $object_id, $skola, $mas = [], $mas2 = [], $mas3 = [], $mas4 = [])
    {
        DB::beginTransaction();

        if (strlen($nr) == 8)
            $nr = "00" . $nr;

        try {
            // check for already auto created free meal e-ticket
            $et = Eticket::A()->Nr($nr)->first();
            if (isset($et->id)) {
                $acc = Account::findOrFail($et->account_id);
                $acc->owner_id = $owner_id;
                $acc->account_type = 2;
                $acc->account_status = 1;
                $acc->save();

                $et->pin = $pin;
                $et->limit_type = $limit_type;
                $et->school_id = $skola;
                $et->grade = strtolower($grade);
                if (!$et->save()) {
                    Log::error("error, changing free eticket - " . $et->getErrors());
                    throw new \Exception("error, changing free eticket", 1);
                }
                
                EticketsObjekti::where("eticket_id", $et->id)->delete();
            } else {

                // create account
                $acc = New Account;
                $acc->account_type = 2;
                $acc->account_status = 1;
                $acc->owner_id = $owner_id;
                $acc->save();

                // create new e-ticket
                $et = New Eticket;
                $et->nr = $nr;
                $et->pk = $pk;
                $et->name = $name;
                $et->pin = $pin;
                $et->school_id = $skola;
                $et->grade = strtolower($grade);
                $et->limit_type = $limit_type;
                $et->eticket_status = 1;
                $et->balance = 0;
                $et->turnover = 0;
                $et->account_id = $acc->id;

                if (count($mas) >= 2 && isset($mas["DateFrom"]) && isset($mas["DateTill"])) {
                    $et->free_meals_from = substr($mas["DateFrom"], 0, 10);
                    $et->free_meals_until = substr($mas["DateTill"], 0, 10);
                } else if (count($mas) == 2) {
                    $et->free_meals_from = substr($mas[0], 0, 10);
                    $et->free_meals_until = substr($mas[1], 0, 10);
                }

                if (count($mas2) >= 2 && isset($mas2["DateFrom"]) && isset($mas2["DateTill"])) {
                    $et->second_meals_from = substr($mas2["DateFrom"], 0, 10);
                    $et->second_meals_until = substr($mas2["DateTill"], 0, 10);
                } else if (count($mas2) == 2) {
                    $et->second_meals_from = substr($mas2[0], 0, 10);
                    $et->second_meals_until = substr($mas2[1], 0, 10);
                }

                if (count($mas3) >= 2 && isset($mas3["DateFrom"]) && isset($mas3["DateTill"])) {
                    $et->third_meals_from = substr($mas3["DateFrom"], 0, 10);
                    $et->third_meals_until = substr($mas3["DateTill"], 0, 10);
                } else if (count($mas3) == 2) {
                    $et->third_meals_from = substr($mas3[0], 0, 10);
                    $et->third_meals_until = substr($mas3[1], 0, 10);
                }

                if (count($mas4) >= 2 && isset($mas4["DateFrom"]) && isset($mas4["DateTill"])) {
                    $et->kopgalds_from = substr($mas4["DateFrom"], 0, 10);
                    $et->kopgalds_until = substr($mas4["DateTill"], 0, 10);
                } else if (count($mas4) == 2) {
                    $et->kopgalds_from = substr($mas4[0], 0, 10);
                    $et->kopgalds_until = substr($mas4[1], 0, 10);
                }

                if (!$et->save()) {
                    Log::error("error, creating eticket - " . $et->getErrors());
                    throw new \Exception("error, creating eticket", 1);
                }

                // operations
                DB::table('eticket_opers')->insert(
                        array('eticket_id' => $et->id, 'operation_id' => 1)
                );

                // free meal dotations
                // first, calculate which period to use if available both
                $joined_date_from = '';
                $joined_date_until = '';

                $mas3 = array_values($mas3);
                $mas2 = array_values($mas2);
                $mas = array_values($mas);

                // use one of valid intervals by priority
                if (isset($mas3[0]) && isset($mas3[1])) {
                    if (strtotime($mas3[0]) <= time() && strtotime($mas3[1]) > time()) {
                        $joined_date_from = substr($mas3[0], 0, 10);
                        $joined_date_until = substr($mas3[1], 0, 10);
                    }
                }

                if (isset($mas2[0]) && isset($mas2[1])) {
                    if (strtotime($mas2[0]) <= time() && strtotime($mas2[1]) > time()) {
                        $joined_date_from = substr($mas2[0], 0, 10);
                        $joined_date_until = substr($mas2[1], 0, 10);
                    }
                }

                if (isset($mas[0]) && isset($mas[1])) {
                    if (strtotime($mas[0]) <= time() && strtotime($mas[1]) > time()) {
                        $joined_date_from = substr($mas[0], 0, 10);
                        $joined_date_until = substr($mas[1], 0, 10);
                    }
                }

                // if there is a period to use
                if (trim($joined_date_from) != '' && trim($joined_date_until) != '') {
                    // check for persons existing free meal data
                    $dot_exists = Dotations::where("pk", $pk)->first();
                    if (isset($dot_exists->id)) {
                        if ($dot_exists->until_date != $joined_date_until) {
                            $dot_exists->until_date = $joined_date_until;
                            $dot_exists->last_used = substr(new Carbon('yesterday'), 0, 10);
                            if (!$dot_exists->save()) {
                                Log::error("error, updating person dotation ($pk) - " . $dot_exists->getErrors());
                                throw new Exception("error, updating person dotation", 1);
                            }
                        }
                    } else {
                        if (strtotime($joined_date_from) <= time() && trim($joined_date_from) != '') {
                            // calculate available dotation ids based on school and grade
                            try {
                                $regnr = School::findOrFail($skola)->reg_nr;
                                $dot_object_id = Objekts::where("object_regnr", $regnr)->first()->id;
                                $grade_price = (SchoolPrices::where("school_id", $skola)->whereGrade(intval($grade))->first()->price) * 100;
                                $dotation_id = ObjectDotationPrices::where("object_id", $dot_object_id)->wherePrice($grade_price)->first()->id;
                            } catch (\Exception $e) {
                                Log::error("error finding dotation for child ($pk) - " . $e->getMessage());
                                throw new \Exception("error finding dotation for child", 1);
                            }

                            if ($dotation_id > 0) {
                                $dot = New Dotations();
                                $dot->pk = $pk;
                                $dot->dotation_id = $dotation_id;
                                $dot->from_date = $joined_date_from;
                                $dot->until_date = $joined_date_until;
                                $dot->import_date = date("Y-m-d");
                                $dot->last_used = "2015-01-01";
                                if (!$dot->save()) {
                                    Log::error("error, creating person dotation ($pk) ($joined_date_from) - " . $dot->getErrors());
                                    throw new \Exception("error, creating person dotation", 1);
                                }
                            }
                        }
                    }
                }
            }
            
            $eto = New EticketsObjekti;
            $eto->eticket_id = $et->id;
            $eto->object_id = $object_id;
            $eto->pirma = 1;
            $eto->save();

            // check for grouped objects
            $ogroup = GroupObjekts::where("object_id", $object_id)->first();
            $ogroup_id = isset($ogroup->ogroup_id) ? $ogroup->ogroup_id : 0;

            if ($ogroup_id > 0) {
                $other_objects = GroupObjekts::where("object_id", '!=', $object_id)
                    ->where("ogroup_id", $ogroup_id)
                    ->lists("object_id")->toArray();

                if (count($other_objects) > 0) {
                    $objectToAdd = [];
                    foreach ($other_objects as $other) {
                        $objectToAdd[] = $other;
                    }
                    $et->saveObjekti($objectToAdd, 1, 0); //1=create new default
                }
            }
            
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::error("create e-ticket - DB save error - " . $e->getMessage() . " | " . $e->getLine());
            DB::rollback();

            return 'Kļūda saglabājot datus - ' . $e->getMessage();
        }

        DB::commit();

        return $et->id;
    }

}
