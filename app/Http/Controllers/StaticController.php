<?php
namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class StaticController extends BaseController
{

    public function getTerms()
    {
        $title = 'Pašapkalpošanās portāla un Skolēna E kartes lietošanas noteikumi norēķiniem ar Tirgotājiem.';

        return View::make('static/terms', compact('title'));
    }

    public function getContact()
    {
        $title = 'Palīdzība';

        return View::make('static/contact', compact('title'));
    }

    public function postContact()
    {
        $epasts = (isset(Auth::user()->email)) ? Auth::user()->email : trim(Input::get( 'epasts' ));
        $talrunis = (isset(Auth::user()->phone)) ? Auth::user()->phone : trim(Input::get( 'talrunis' ));
        $message = trim(Input::get( 'message' ));
        $recaptcha_response_field = Input::get('g-recaptcha-response');

        $input  = Input::all();
        $input['epasts'] = $epasts;
        $input['talrunis'] = $talrunis;

        if ($recaptcha_response_field=='')
            return Redirect::back()
                ->withInput()
                ->with('error','Nav aizpildīts drošības kods!');

        if ($message=='')
            return Redirect::back()
                ->withInput()
                ->with('error','Jāaizpilda ziņas teksts!');

        if ($epasts=='' AND $talrunis=='')
        {
            return Redirect::back()
                ->withInput()
                ->with('error','Jānorāda e-pasts vai tālruņa numurs!');
        }

        $rules = array(
            'epasts' 	=> 'required_without:talrunis',
            'talrunis' 	=> 'required_without:epasts|between:8,99',
            'message' 	=> 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {

            // send email to admin
            $data = array(
                'epasts' => $epasts,
                'talrunis' => $talrunis,
                'mess' => $message,
                'ip' => $_SERVER['REMOTE_ADDR']
            );

            try {
                Mail::send('mail.contact', $data, function($message) {
                    $message->to('olegs@telematika.lv', 'skolas.rigaskarte.lv Administrator')->cc(Config::get('mail.rsemail2'))->cc(Config::get('mail.rsemail1'))->subject('skolas.rigaskarte.lv - jauna ziņa no formas');
                });
            }
            catch (Exception $e)
            {
                return Redirect::back()
                    ->withInput()
                    ->with('error','Neizdevās nosūtīt ziņu!');
            }

            return Redirect::back()
                ->with('success','Ziņa nosūtīta!');

        } else {
            return Redirect::back()
                ->withInput()
                ->withErrors($v->messages());
        }
    }

    public function getCinfo()
    {
        $title = 'Kontaktinformācija';

        return View::make('static/cinfo', compact('title'));
    }

    public function aprikojuma_instrukcija()
    {
        if (Auth::check())
        {
            $u = Auth::user();
            $roleids = $u->currentRoleIds();

            // tikai tirgotajiem
            if (array_search(3, $roleids)!== false)
            {
                $file= storage_path(). "/aprikojuma_instrukcija.pdf";
                $headers = array(
                    'Content-Type: application/pdf',
                );
                return response()->download($file, 'aprikojuma_instrukcija.pdf', $headers);
            }
        }
    }

    public function aprikojuma_instrukcijaru()
    {
        if (Auth::check())
        {
            $u = Auth::user();
            $roleids = $u->currentRoleIds();

            // tikai tirgotajiem
            if (array_search(3, $roleids)!== false)
            {
                $file= storage_path(). "/aprikojuma_instrukcija_ru.pdf";
                $headers = array(
                    'Content-Type: application/pdf',
                );
                return response()->download($file, 'aprikojuma_instrukcija_ru.pdf', $headers);
            }
        }
    }

    public function kludu_pazinojumi()
    {
        if (Auth::check())
        {
            $u = Auth::user();
            $roleids = $u->currentRoleIds();

            // tikai tirgotajiem
            if (array_search(3, $roleids)!== false)
            {
                $file= storage_path(). "/kludu_pazinojumi.pdf";
                $headers = array(
                    'Content-Type: application/pdf',
                );
                return response()->download($file, 'kludu_pazinojumi.pdf', $headers);
            }
        }
    }

    public function isa_instrukcija()
    {
        if (Auth::check())
        {
            $u = Auth::user();
            $roleids = $u->currentRoleIds();

            // tikai tirgotajiem
            if (array_search(3, $roleids)!== false)
            {
                $file= storage_path(). "/isa_instrukcija.pdf";
                $headers = array(
                    'Content-Type: application/pdf',
                );
                return response()->download($file, 'isa_instrukcija.pdf', $headers);
            }
        }
    }
}