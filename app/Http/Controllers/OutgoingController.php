<?php
namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Mpos;
use App\Models\OwnerAccess;
use App\Models\Seller;
use App\Models\SellerPayments;
use App\Models\Settings;
use App\Models\Transaction;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class OutgoingController extends BaseController {

    protected $links = 'bank';

    /**
     * Show the form for making bank transfer
     *
     * @return Response
     */
    public function getBank()
    {
        // Title
        $title = 'Sagatavot pārskaitījumu uz savu bankas kontu';

        // Show the page
        return View::make('outgoing/'.$this->links.'/create', compact('title'));
    }

    public function getHistory()
    {
        // Title
        $title = 'Izmaksu vēsture';

        $owner_id = OwnerAccess::Seller()->U(Auth::user()->id)->value('owner_id');
        if ($owner_id > 0)
        {
            $seller_acc = Account::O($owner_id)->Sell()->first();
            $seller_free_meals_acc = Account::O($owner_id)->SellerFreeMeals()->first();
        }
        else throw new \Exception("No owner found");

        $seller_free_meals_acc_id = isset($seller_free_meals_acc->id) ? $seller_free_meals_acc->id : 0;

        $data = Transaction::Ok()
            ->where(function($query) {
                $query->where('to_account_id',  999999);
                $query->orWhere('to_account_id', 999996);
            })
            ->where(function($query) use ($seller_acc, $seller_free_meals_acc_id) {
                $query->where('from_account_id', $seller_acc->id);

                if ($seller_free_meals_acc_id > 0)
                    $query->orWhere('from_account_id', $seller_free_meals_acc_id);
            })
            ->orderBy('id','desc')
            ->get();

        // Show the page
        return View::make('outgoing/history', compact('title','data'));
    }

    public function getPdf($id)
    {
        $owner_id = OwnerAccess::Seller()->U(Auth::user()->id)->value('owner_id');
        if ($owner_id>0)
        {
            $seller_acc = Account::O($owner_id)->Sell()->first();
            $seller_free_meals_acc = Account::O($owner_id)->SellerFreeMeals()->first();
        }

        $data = Transaction::with('from_account')->where('id',$id)->Ok()->firstOrFail();

        if ($data->from_account_id != $seller_acc->id && $data->from_account_id != $seller_free_meals_acc->id)
        {
            return Redirect::to('outgoing/history')->with('error', 'Nav atrasts!');
        }

        return response()->download(public_path('assets/pdfs').'/'.$data->comments.'.pdf', $data->id.'.pdf');
    }

    public function getFreePdf($id)
    {
        $owner_id = OwnerAccess::Seller()->U(Auth::user()->id)->value('owner_id');
        if ($owner_id>0)
        {
            $seller_acc = Account::O($owner_id)->Sell()->first();
            $seller_free_meals_acc = Account::O($owner_id)->SellerFreeMeals()->first();
        }

        $data = Transaction::with('from_account')->where('id',$id)->Ok()->firstOrFail();

        if ($data->from_account_id != $seller_acc->id && $data->from_account_id != $seller_free_meals_acc->id)
        {
            return Redirect::to('outgoing/history')->with('error', 'Nav atrasts!');
        }

        // generate free meals PDF from transactions
        $data = Transaction::where('id',$id)->Ok()->firstOrFail();
        $seller = Seller::with("owner")->where('owner_id',$owner_id)->first();
        $settings = Settings::firstOrFail();
        $periods_arr = explode("-",$data->comments);
        $periods = $periods_arr[0].'-'.$periods_arr[1];
        $payment_id = SellerPayments::where("transaction_id",$data->id)->first()->id;
        $mpos_account_ids = Mpos::Own($owner_id)->lists("account_id")->toArray();
        $count = Transaction::Ok()
            ->where('from_account_id', '=', 2)
            ->whereIn('to_account_id', $mpos_account_ids)
            ->where("created_at","LIKE",$periods.'%')
            ->count();

        $html = '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
                    <head profile="http://gmpg.org/xfn/11">
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><style tyle="text/css">body { font-family: DejaVu Sans, sans-serif; }</style></head><body>'.
                    View::make('accountant/freereceipt', compact('data','seller','settings','periods','payment_id','count'))
                    .'</body></html>';
        $outputName = $periods."-FREE.pdf";

        return \PDF::loadHTML($html)->download($outputName);
    }
}