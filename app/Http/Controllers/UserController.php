<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Owner;
use App\Models\OwnerAccess;
use App\Models\Settings;
use App\Models\User;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class UserController extends BaseController
{

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    /**
     * Stores new user
     *
     */
    public function postIndex()
    {
        \Debugbar::disable();

        // username, surname, pk from received data
        if (!Session::has('register_encoded')) {
            return Redirect::to(config('registration.login_path'))
                ->with('error', "Trūkst reģistrācijas datu!");
        }

        $encoded_data = Session::get('register_encoded');
        //$encoded_data = 'MadRotJpb2AR1kKhcYqGhmELQ9qOMlCNA9/9SCSdVyiXlJAN+rZwtpD1//XvKNh690fmmGmKbKMPHTJjuj8WY0pjnTCL47XRBSMIWdRZKiybx3FrCIV+vimVPJKwaTknxQtX2o7JDBxST6lyxNHUlSKw1CGpBvkNK1o9M8Nieec=';

        try {
            $data = json_decode($this->decrypt($encoded_data));
            $timestamp = strtotime(str_replace("T", " ", $data->Timestamp));
        } catch (\Exception $e) {
            Log::error("register - bad data=".$e->getMessage());
            return Redirect::to(config('registration.login_path'))
                ->with("error", "Saņemti kļūdaini reģistrācijas dati!");
        }

        $server_timestamp = (time() + (10*60));

        if ($timestamp > $server_timestamp || (strlen($timestamp) < 6)) {
            return Redirect::to(config('registration.login_path'))
                ->with("error", "Saņemtie dati nav korekti, pārāk ilgs pārsūtīšanas laiks! Lūdzu mēģiniet vēlreiz.");
        }
        $errorMessages = new \Illuminate\Support\MessageBag;

        // check for existing user from emaks
        $emaksuser = 0;
        if (Auth::check()) {
            if (Auth::user()->terms == 2) {
                $emaksuser = 1;
                $this->user = Auth::user();
            }
        }

        // terms
        if (Input::get('terms')!=1) {
            return Redirect::back()
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', 'Jāapstiprina noteikumi!');
        }

        // existing PK
        $exists = User::where("pk", $data->PersonCode)->count();
        if ($exists > 0 && $emaksuser == 0) {
            return Redirect::back()
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', 'Lietotājs ar šādu personas kodu jau ir reģistrēts sistēmā!');
        }

        $this->user->username = $data->FirstName;
        $this->user->surname = $data->LastName;
        $this->user->pk = $data->PersonCode;

        $this->user->email = Input::get('email');
        $this->user->phone = Input::get('phone');
        $this->user->address = trim(strip_tags(Input::get('address')));
        // if terms accepted, set to 3 = waiting for RK confirmation
        $this->user->terms = ((int) Input::get('terms') === 1) ? 3 : 0;

        // info bloks
        $info1 = trim(strip_tags(Input::get('info1')));
        $info = trim(strip_tags(Input::get('info')));
        $info2 = trim(strip_tags(Input::get('info2')));

        $this->user->info = $info1.' | '.$info.' | '.$info2;
        $this->user->pazime = (int) Input::get('pazime');
        $this->user->pazime_text = trim(strip_tags(Input::get('pazime_text')));

        // prognozes bloks
        $pilsoniba = trim(strip_tags(Input::get('pilsoniba')));
        $maks1 = trim(strip_tags(Input::get('maks1')));
        $maks2 = trim(strip_tags(Input::get('maks2')));
        $maks3 = trim(strip_tags(Input::get('maks3')));
        $maks4 = trim(strip_tags(Input::get('maks4')));
        $maks5 = trim(strip_tags(Input::get('maks5')));
        $maks6 = trim(strip_tags(Input::get('maks6')));
        $izcelsme1 = trim(strip_tags(Input::get('izcelsme1')));
        $izcelsme2 = trim(strip_tags(Input::get('izcelsme2')));
        $darijumi1 = trim(strip_tags(Input::get('darijumi1')));
        $darijumi2 = trim(strip_tags(Input::get('darijumi2')));
        $ipasnieks1 = trim(strip_tags(Input::get('ipasnieks1')));
        $ipasnieks2 = trim(strip_tags(Input::get('ipasnieks2')));

        if ($izcelsme1 === 'Cita uz kartes saņemto līdzeklu izcelsme' &&  $izcelsme2 === '') {
            $errorMessages->add('izcelsme', 'Šis lauks ir obligāts.');
        }

        if ($darijumi1 === 'Nē' &&  $darijumi2 === '') {
            $errorMessages->add('darijumi', 'Šis lauks ir obligāts.');
        }

        if ($ipasnieks1 === 'Nē' &&  $ipasnieks2 === '') {
            $errorMessages->add('ipasnieks', 'Šis lauks ir obligāts.');
        }

        if (strtotime($info2) < strtotime("+90 days")) {
            $errorMessages->add(
                'info',
                'Dokumentam ir jābūt derīgam vismaz 3 mēnešus pirms dokumenta derīguma termiņa beigām.'
            );
        }

        $password = Input::get('password');
        $passwordConfirmation = Input::get('password_confirmation');

        // pazime
        if ((int) $this->user->pazime !== 1 &&  (int) $this->user->pazime !== 2) {
            $errorMessages->add('pazime', 'Šis lauks ir obligāts.');
        }

        if ((int) $this->user->pazime === 1 &&  $this->user->pazime_text === '') {
            $errorMessages->add('pazime_text', 'Šis lauks ir obligāts.');
        }

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $this->user->password = bcrypt($password);
                // The password confirmation will be removed from model
                // before saving. This field will be used in Ardent's
                // auto validation.
                $this->user->password_confirmation = bcrypt($passwordConfirmation);
            } else {
                // Redirect to the new user page
                return Redirect::back()
                    ->withInput(Input::except('password', 'password_confirmation'))
                    ->with('error', Lang::get('admin/users/messages.password_does_not_match'));
            }
        } else {
            unset($this->user->password, $this->user->password_confirmation);
        }

        $this->user->confirmation_code = str_random(20);

        // citi JSON lauks
        $this->user->citi = json_encode([
            "pilsoniba" => $pilsoniba,
            "maks1" => $maks1,
            "maks2" => $maks2,
            "maks3" => $maks3,
            "maks4" => $maks4,
            "maks5" => $maks5,
            "maks6" => $maks6,
            "izcelsme1" => $izcelsme1,
            "izcelsme2" => $izcelsme2,
            "darijumi1" => $darijumi1,
            "darijumi2" => $darijumi2,
            "ipasnieks1" => $ipasnieks1,
            "ipasnieks2" => $ipasnieks2,
        ]);

        // custom validator
        $custom = array(
            'required' => 'Šis lauks ir obligāts.',
            'email' => 'Jāievada korekta e-pasts adrese.',
            'phone' => 'Jāievada korekts tālruņa numurs bez valsts koda.',
            'size' => 'Nekorekts lauka garums (8 cipari).',
            'integer' => 'Var ievadīt tikai ciparus.',
            'unique' => 'Šāda vērtība jau ir reģistrēta sistēmā.',
        );

        if ($emaksuser === 1) {
            $validator = Validator::make(
                array(
                    'username' => $this->user->username,
                    'surname' => $this->user->surname,
                    'email' => $this->user->email,
                    'password' => $password,
                    'password_confirmation' => $passwordConfirmation,
                    'phone' => $this->user->phone,
                    'pk' => $this->user->pk,
                    'address' => $this->user->address,
                    'info' => $info,
                    'info2' => $info2,
                    'pazime' => $this->user->pazime,
                    'pilsoniba' => $pilsoniba,
                    'maks1' => $maks1,
                    'maks2' => $maks2,
                    'maks3' => $maks3,
                    'maks4' => $maks4,
                    'maks5' => $maks5,
                    'maks6' => $maks6,
                ),
                array(
                    'username' => 'required',
                    'surname' => 'required',
                    'email' => 'required|email',
                    'password' => 'required|regex:/(?=.*\d)(?=.*[a-zA-Z]).{8,99}/',
                    'password_confirmation' => 'required|regex:/(?=.*\d)(?=.*[a-zA-Z]).{8,99}/',
                    'phone' => 'required',
                    'pk' => 'required|regex:/(\d){6}-(\d){5}/',
                    'address' => 'required',
                    'info' => 'required',
                    'info2' => 'required',
                    'pazime' => 'required',
                    'pilsoniba' => 'required',
                    'maks1' => 'required|integer',
                    'maks2' => 'required|integer',
                    'maks3' => 'required|integer',
                    'maks4' => 'required|integer',
                    'maks5' => 'required|integer',
                    'maks6' => 'required|integer',
                ),
                $custom
            );
        } else {
            $validator = Validator::make(
                array(
                    'username' => $this->user->username,
                    'surname' => $this->user->surname,
                    'email' => $this->user->email,
                    'password' => $password,
                    'password_confirmation' => $passwordConfirmation,
                    'phone' => $this->user->phone,
                    'pk' => $this->user->pk,
                    'address' => $this->user->address,
                    'info' => $info,
                    'info2' => $info2,
                    'pazime' => $this->user->pazime,
                    'pilsoniba' => $pilsoniba,
                    'maks1' => $maks1,
                    'maks2' => $maks2,
                    'maks3' => $maks3,
                    'maks4' => $maks4,
                    'maks5' => $maks5,
                    'maks6' => $maks6,
                ),
                array(
                    'username' => 'required',
                    'surname' => 'required',
                    'email' => 'required|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
                    'password' => 'required|regex:/(?=.*\d)(?=.*[a-zA-Z]).{8,99}/',
                    'password_confirmation' => 'required|regex:/(?=.*\d)(?=.*[a-zA-Z]).{8,99}/',
                    'phone' => 'required',
                    'pk' => 'required|regex:/(\d){6}-(\d){5}/',
                    'address' => 'required',
                    'info' => 'required',
                    'info2' => 'required',
                    'pazime' => 'required',
                    'pilsoniba' => 'required',
                    'maks1' => 'required|integer',
                    'maks2' => 'required|integer',
                    'maks3' => 'required|integer',
                    'maks4' => 'required|integer',
                    'maks5' => 'required|integer',
                    'maks6' => 'required|integer',
                ),
                $custom
            );

            // DB rules
            $rules = array(
                'username' => 'required',
                'surname' => 'required',
                'email' => 'required|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
                'password' => 'required|confirmed|regex:/(?=.*\d)(?=.*[a-zA-Z]).{8,99}/',
                'password_confirmation' => 'regex:/(?=.*\d)(?=.*[a-zA-Z]).{8,99}/',
                'phone' => 'required',
                'address' => 'required',
                'info' => 'required',
                'pazime' => 'required',
                'pazime_text' => '',
                'citi' => '',
            );
        }

        if ($validator->fails()) {
            $errorMessages->merge($validator->errors()->toArray());
            return Redirect::back()
                ->withInput(Input::except('password'))
                ->withErrors($errorMessages);
        }

        // Save if valid. Password field will be hashed before save
        if ($emaksuser == 1) {
            if (! $this->user->save()) {
                return Redirect::back()
                    ->withInput(Input::except('password'))
                    ->with("error", "Kļūda atjaunojot informāciju!");
            }
        } else {
            $this->user->save($rules);
        }

        if ($this->user->id) {
            try {
                // create owner
                $own = new Owner;
                $own->owner_type=1;
                $own->owner_name = $this->user->username.' '.$this->user->surname;
                $own->save();

                if ($own->id) {
                    // create owner access
                    $oacc = new OwnerAccess;
                    $oacc->user_id = $this->user->id;
                    $oacc->owner_id = $own->id;
                    $oacc->access_status = 1;
                    $oacc->access_level = 1;
                    $oacc->save();

                    if ($oacc->id) {
                        // create account
                        $acc = new Account;
                        $acc->owner_id = $own->id;
                        $acc->account_type=1;
                        $acc->account_status=1;
                        $acc->save();

                        if ($acc->id) {
                            // create account access
                            $access = new AccountAccess;
                            $access->user_id = $this->user->id;
                            $access->account_id = $acc->id;
                            $access->access_status=1;
                            $access->access_level=1;
                            $access->save();

                            if (!$access->id) {
                                $own->delete();
                                $oacc->delete();
                                $acc->delete();
                                throw new \Exception($access->getErrors().'1');
                            }
                        } else {
                            $oacc->delete();
                            $own->delete();
                            throw new \Exception($acc->getErrors().'2');
                        }
                    } else {
                        $own->delete();
                        throw new \Exception($oacc->getErrors().'3');
                    }
                } else {
                    throw new \Exception($own->getErrors().'4');
                }
            } catch (\Exception $e) {
                $this->user->delete();

                return Redirect::back()
                    ->withInput(Input::except('password'))
                    ->with('error', "1".$e->getMessage());
            }

            Cache::forget('all_user_ids');
            Session::forget('register_encoded');

            //send verification mail to user
            //---------------------------------------------------------

            $user = $this->user;
            $data = ['user' => $user];
            Mail::send('auth.confirm', $data, function ($message) use ($user) {
                /** @var $message Message */
                $message->subject("Skolas.rigaskarte.lv reģistrācija");
                $message->to($user->email);
            });

            // Redirect with success message
            return Redirect::to(config('registration.login_path'))
                ->with('notice', Lang::get('user/user.user_account_created'));
        } else {
            // Get validation errors (see Ardent package)
            $error = $this->user->errors()->all();

            // custom mesidzu filtresana, ja neradam konkretu lauku
            //$error = array_unique($error);

            return Redirect::back()
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }

    /**
     * Displays the form for user creation
     *
     */

    public function getRegister()
    {
        if (! Session::has('register_encoded')) {
            return Redirect::to(config('registration.login_path'))
                ->with('error', trans('errors.register_encoded_error'));
        }

        $encoded_data = Session::get('register_encoded');
        //$encoded_data = 'MadRotJpb2AR1kKhcYqGhmELQ9qOMlCNA9/9SCSdVyiXlJAN+rZwtpD1//XvKNh690fmmGmKbKMPHTJjuj8WY0pjnTCL47XRBSMIWdRZKiybx3FrCIV+vimVPJKwaTknxQtX2o7JDBxST6lyxNHUlSKw1CGpBvkNK1o9M8Nieec=';

        try {
            $data = json_decode($this->decrypt($encoded_data));
            $timestamp = strtotime(str_replace('T', ' ', $data->Timestamp));
        } catch (\Exception $e) {
            Log::error("register - bad data=".$e->getMessage());
            return Redirect::to(config('registration.login_path'))
                ->with('error', trans('errors.bad_data_error'));
        }

        $server_timestamp = (time() + (10*60));

        if ($timestamp > $server_timestamp || (strlen($timestamp) < 6)) {
            return Redirect::to(config('registration.login_path'))
                ->with('error', trans('errors.overtime_error'));
        }
        return View::make('site/user/create', compact('data'));
    }

    public function postRegister(Request $request)
    {
        $content = rawurldecode($request->getContent());

        if (strlen($content) < 50) {
            return Redirect::to(config('registration.login_path'))->with('error', trans('errors.empty_data_error'));
        }

        $encoded_data = substr($content, 5);

        Log::info("register - data= ".$encoded_data);

        try {
            $data = json_decode($this->decrypt($encoded_data));
            $timestamp = strtotime(str_replace("T", " ", $data->Timestamp));
        } catch (\Exception $e) {
            Log::error("register - bad data=".$e->getMessage());
            return Redirect::to(config('registration.login_path'))
                ->with('error', trans('errors.bad_data_error'));
        }

        $server_timestamp = (time() + (10*60));

        Log::info("register - decoded data=".json_encode($data));
        Log::info("register - timestamp sanemts= ".$timestamp." , timestamp servera+10min= ".$server_timestamp);

        if ($timestamp > $server_timestamp || (strlen($timestamp) < 6)) {
            return Redirect::to(config('registration.login_path'))
                ->with('error', trans('errors.overtime_error'));
        }

        Session::put('register_encoded', $encoded_data);

        return Redirect::to(config('registration.profile_path'));
    }

    public function getForceterms()
    {
        $user = Auth::user();

        // Show the page
        return View::make('site/user/forceterms', compact('user'));
    }

    // registration data decryption
    protected function strippadding($string)
    {
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if (preg_match("/$slastc{".$slast."}/", $string)) {
            return substr($string, 0, strlen($string)-$slast);
        } else {
            return false;
        }
    }

    protected function decrypt($string = "")
    {
        $key = base64_decode(Config::get('services.rs.key'));
        $iv = base64_decode(Config::get('services.rs.iv'));

        $string = base64_decode($string);

        return $this->strippadding(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $string, MCRYPT_MODE_CBC, $iv));
    }

    public function getYearly()
    {
        $yearly_text = trim(Settings::get()->first()->yearly_text);

        // Show the page
        return View::make('site/user/yearly', compact('yearly_text'));
    }

    public function getYearlyNo()
    {
        $user = Auth::user();
        $user->yearly_status = 'N';
        $user->yearly_updated = date("Y-m-d");
        if (! $user->save()) {
            return Redirect::back()->with("error", "Kļūda saglabājot datus!");
        }

        return Redirect::to("/")->with("success", "Informācija veiksmīgi saglabāta!");
    }

    public function postYearly()
    {
        $izcelsme1 = trim(strip_tags(Input::get('izcelsme1')));
        $izcelsme2 = trim(strip_tags(Input::get('izcelsme2')));
        $darijumi1 = trim(strip_tags(Input::get('darijumi1')));
        $darijumi2 = trim(strip_tags(Input::get('darijumi2')));
        $ipasnieks1 = trim(strip_tags(Input::get('ipasnieks1')));
        $ipasnieks2 = trim(strip_tags(Input::get('ipasnieks2')));

        $errorMessages = new \Illuminate\Support\MessageBag;

        if ($izcelsme1 === 'Cita uz kartes saņemto līdzeklu izcelsme' &&  $izcelsme2 === '') {
            $errorMessages->add('izcelsme', 'Šis lauks ir obligāts.');
        }

        if ($darijumi1 === 'Nē' &&  $darijumi2 === '') {
            $errorMessages->add('darijumi', 'Šis lauks ir obligāts.');
        }

        if ($ipasnieks1 === 'Nē' &&  $ipasnieks2 === '') {
            $errorMessages->add('ipasnieks', 'Šis lauks ir obligāts.');
        }

        // custom validator
        $custom = array(
            'required' => 'Šis lauks ir obligāts.',
        );

        $validator = Validator::make(
            array(
                'izcelsme1' => $izcelsme1,
                'darijumi1' => $darijumi1,
                'ipasnieks1' => $ipasnieks1,
            ),
            array(
                'izcelsme1' => 'required',
                'darijumi1' => 'required',
                'ipasnieks1' => 'required',
            ),
            $custom
        );

        if ($validator->fails()) {
            $errorMessages->merge($validator->errors()->toArray());
            return Redirect::back()
                ->withInput()
                ->withErrors($errorMessages);
        }

        try {
            $user = Auth::user();
            $user->yearly_status = 'Y';
            $user->yearly_updated = date("Y-m-d");

            $citi = json_decode($user->citi);
            $citi->izcelsme1 = $izcelsme1;
            $citi->izcelsme2 = $izcelsme2;
            $citi->darijumi1 = $darijumi1;
            $citi->darijumi2 = $darijumi2;
            $citi->ipasnieks1 = $ipasnieks1;
            $citi->ipasnieks2 = $ipasnieks2;
            $user->citi = json_encode($citi);

            $user->save();

            // send mail to RK
            Mail::send(
                'mail.report',
                ["info" => "Lietotājs ".$user->username." ".$user->surname." ir veicis ikgadējo informācijas atjaunošanu."],
                function ($message) use ($user) {
                    /** @var $message Message */
                    $message->to(Config::get('mail.rsemail1'))
                        ->subject("Lietotājs ".$user->username." ".$user->surname." ir veicis ikgadējo informācijas atjaunošanu.");
                });
        } catch (\Exception $e) {
            return Redirect::back()->with("error", "Kļūda saglabājot datus!");
        }

        return Redirect::to("/")->with("success", "Informācija veiksmīgi saglabāta!");
    }

    public function getEditform()
    {
        // check for user status
        $u = Auth::user();

        if (! isset($u->terms)) {
            return Redirect::to("/")->with('error', 'Nav atrasts!');
        }

        if ($u->terms != 0 && $u->terms != 3) {
            return Redirect::to("/")->with('error', 'Nav atrasts!');
        }

        // Show the page
        $title = 'Labot reģistrācijas anketu';
        $newuser = $u;
        $citi = json_decode($newuser->citi);
        $info = explode("|", $newuser->info);

        return View::make('site/user/edit', compact('title', 'newuser', 'citi', 'info'));
    }

    public function postEditform()
    {
        // check for user status
        $u = Auth::user();
        $this->user = $u;
        if ($u->terms != 0 && $u->terms != 3) {
            return Redirect::to("/")->with('error', 'Nav atrasts!');
        }

        $errorMessages = new \Illuminate\Support\MessageBag;

        $this->user->address = trim(strip_tags(Input::get('address')));

        // info bloks
        $info1 = trim(strip_tags(Input::get('info1')));
        $info = trim(strip_tags(Input::get('info')));
        $info2 = trim(strip_tags(Input::get('info2')));

        $this->user->info = $info1.' | '.$info.' | '.$info2;
        $this->user->pazime = (int) Input::get('pazime');
        $this->user->pazime_text = trim(strip_tags(Input::get('pazime_text')));

        // prognozes bloks
        $pilsoniba = trim(strip_tags(Input::get('pilsoniba')));
        $maks1 = trim(strip_tags(Input::get('maks1')));
        $maks2 = trim(strip_tags(Input::get('maks2')));
        $maks3 = trim(strip_tags(Input::get('maks3')));
        $maks4 = trim(strip_tags(Input::get('maks4')));
        $maks5 = trim(strip_tags(Input::get('maks5')));
        $maks6 = trim(strip_tags(Input::get('maks6')));
        $izcelsme1 = trim(strip_tags(Input::get('izcelsme1')));
        $izcelsme2 = trim(strip_tags(Input::get('izcelsme2')));
        $darijumi1 = trim(strip_tags(Input::get('darijumi1')));
        $darijumi2 = trim(strip_tags(Input::get('darijumi2')));
        $ipasnieks1 = trim(strip_tags(Input::get('ipasnieks1')));
        $ipasnieks2 = trim(strip_tags(Input::get('ipasnieks2')));

        if ($izcelsme1 === 'Cita uz kartes saņemto līdzeklu izcelsme' &&  $izcelsme2 === '') {
            $errorMessages->add('izcelsme', 'Šis lauks ir obligāts.');
        }

        if ($darijumi1 ==+ 'Nē' &&  $darijumi2 === '') {
            $errorMessages->add('darijumi', 'Šis lauks ir obligāts.');
        }

        if ($ipasnieks1 === 'Nē' &&  $ipasnieks2 === '') {
            $errorMessages->add('ipasnieks', 'Šis lauks ir obligāts.');
        }

        if (strtotime($info2) < strtotime("+90 days")) {
            $errorMessages->add(
                'info',
                'Dokumentam ir jābūt derīgam vismaz 3 mēnešus pirms dokumenta derīguma termiņa beigām.'
            );
        }

        // pazime
        if ((int) $this->user->pazime !== 1 &&  (int) $this->user->pazime !== 2) {
            $errorMessages->add('pazime', 'Šis lauks ir obligāts.');
        }

        if ((int) $this->user->pazime === 1 &&  $this->user->pazime_text === '') {
            $errorMessages->add('pazime_text', 'Šis lauks ir obligāts.');
        }

        // citi JSON lauks
        $this->user->citi = json_encode([
            "pilsoniba" => $pilsoniba,
            "maks1" => $maks1,
            "maks2" => $maks2,
            "maks3" => $maks3,
            "maks4" => $maks4,
            "maks5" => $maks5,
            "maks6" => $maks6,
            "izcelsme1" => $izcelsme1,
            "izcelsme2" => $izcelsme2,
            "darijumi1" => $darijumi1,
            "darijumi2" => $darijumi2,
            "ipasnieks1" => $ipasnieks1,
            "ipasnieks2" => $ipasnieks2,
        ]);

        // custom validator
        $custom = array(
            'required' => 'Šis lauks ir obligāts.',
            'size' => 'Nekorekts lauka garums (8 cipari).',
            'integer' => 'Var ievadīt tikai ciparus.',
            'unique' => 'Šāda vērtība jau ir reģistrēta sistēmā.',
        );

        $validator = Validator::make(
            array(
                'address' => $this->user->address,
                'info' => $info,
                'info2' => $info2,
                'pazime' => $this->user->pazime,
                'pilsoniba' => $pilsoniba,
                'maks1' => $maks1,
                'maks2' => $maks2,
                'maks3' => $maks3,
                'maks4' => $maks4,
                'maks5' => $maks5,
                'maks6' => $maks6,
            ),
            array(
                'address' => 'required',
                'info' => 'required',
                'info2' => 'required',
                'pazime' => 'required',
                'pilsoniba' => 'required',
                'maks1' => 'required',
                'maks2' => 'required',
                'maks3' => 'required',
                'maks4' => 'required',
                'maks5' => 'required',
                'maks6' => 'required',
            ),
            $custom
        );

        // DB rules
        $rules = array(
            'address' => 'required',
            'info' => 'required',
            'pazime' => 'required',
            'pazime_text' => '',
            'citi' => '',
        );

        if ($validator->fails() || $errorMessages->count() > 0) {
            $errorMessages->merge($validator->errors()->toArray());
            return Redirect::back()
                ->withInput(Input::except('password'))
                ->withErrors($errorMessages);
        }

        try {
            $this->user->save();
        } catch (\Exception $e) {
            return Redirect::back()
                ->withInput(Input::except('password'))
                ->with('error', 'Kļūda saglabājot datus!');
        }

        return Redirect::to('user/editform')
            ->with('success', 'Dati veiksmīgi saglabāti.');
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getConfirm($code)
    {
        $code = trim(strip_tags($code));
        if (strlen($code) == 20) {
            // send mail to RK
            $user = User::where("confirmation_code", $code)->first();
            if (isset($user->id)) {
                if ($user->confirmed != 1) {
                    $user->confirmed = 1;
                    $user->save();

                    Mail::send('mail.rknewuser', ['user' => $user], function ($message) {
                        $message->to(Config::get('mail.rsemail1'))
                            ->bcc(Config::get('mail.rsemail2'))->subject('Skolas.rigaskarte.lv - jauns lietotājs.');
                    });
                }
            }

            return Redirect::to('auth/login')->with('notice', Lang::get('confide/confide.alerts.confirmation'));
        } else {
            return Redirect::to('auth/login')->with('error', Lang::get('confide/confide.alerts.wrong_confirmation'));
        }
    }
}
