<?php
namespace App\Http\Controllers\rk;

use App\Http\Controllers\BaseController;
use App\Models\Ticket;
use App\Models\User;
use App\Models\UserComments;
use Bllim\Datatables\Datatables;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class RkController extends BaseController
{

    protected $links = 'rk';

    public function getIndex()
    {
        return View::make($this->links.'/index');
    }

    public function getCancel($objekts)
    {
        // Title
        $title = 'Atcelt lietotāja reģistrācijas pieprasījumu - '.$objekts->username.' '.$objekts->surname.' ?';

        $comm = UserComments::where("pk", $objekts->pk)->first();
        $comment = (isset($comm->data)) ? trim($comm->data) : '';

        if ($objekts->pk == '' || $objekts->info == '') {
            return Redirect::back()->with('error', 'Nekorekts lietotājs.');
        }

        // Show the page
        return View::make($this->links.'/cancel', compact('title', 'objekts', 'comment'));
    }

    public function postCancel($objekts)
    {
        if ($objekts->pk == '' || $objekts->info == '') {
            return Redirect::back()->with('error', 'Nekorekts lietotājs.');
        }

        $comment = trim(Input::get("comment"));

        try {
            DB::transaction(function () use ($objekts, $comment) {
                $comm = UserComments::where("pk", $objekts->pk)->first();
                if (isset($comm->id)) {
                    echo $comm->data = $comment;
                    $comm->last_editor_id = Auth::user()->id;
                    $comm->save();
                } else {
                    $comm = new UserComments();
                    $comm->pk = $objekts->pk;
                    $comm->data = $comment;
                    $comm->last_editor_id = Auth::user()->id;
                    $comm->save();
                }

                $objekts->terms = 0; // cancelled
                $objekts->save();

                // create ticket
                $t = new Ticket;
                $t->user_id = $objekts->id;
                $t->ticket_type = 5;
                $t->ticket_status = 1;
                $t->eticket_nr = 0;
                $t->content = 'Jūsu lietotāja profila pieprasījums ir noraidīts!';
                if (!$t->save()) {
                    throw new \RuntimeException("Neizdevās izveidot paziņojumu!!", 5);
                }
            });

            //send email
            Mail::send('mail.rkcancel', [], function ($message) use ($objekts) {
                $message->to($objekts->email, $objekts->username.' '.$objekts->surname)
                    ->subject('Skolas.rigaskarte.lv - neveiksmīga profila datu pārbaude.');
            });
        } catch (\RuntimeException $e) {
            return Redirect::to($this->links.'/index/'.$objekts->id.'/cancel')
                ->with('error', $e->getMessage());
        }

        return Redirect::to($this->links.'/index/')
            ->with('success', 'Lietotāja reģistrācijas pieprasījums atcelts.');
    }

    public function getConfirm($newuser)
    {
        try {
            // Title
            $title = 'Apstiprināt lietotāju - '.$newuser->username.' '.$newuser->surname;

            $citi = json_decode($newuser->citi);
            $info = explode("|", $newuser->info);
            $comm = UserComments::where("pk", $newuser->pk)->first();
            $comment = (isset($comm->data)) ? trim($comm->data) : '';

            if ($newuser->pk == '' || $newuser->info == '') {
                return Redirect::back()->with('error', 'Nekorekts lietotājs.');
            }
        } catch (\RuntimeException $exception) {
            dd($exception->getMessage());
        }

        // Show the page
        return View::make($this->links.'/confirm', compact('title', 'newuser', 'citi', 'info', 'comment'));
    }

    public function postConfirm($objekts)
    {
        if ($objekts->pk == '' || $objekts->info == '') {
            return Redirect::back()->with('error', 'Nekorekts lietotājs.');
        }

        $comment = trim(Input::get("comment"));

        try {
            DB::transaction(function () use ($objekts, $comment) {
                $comm = UserComments::where("pk", $objekts->pk)->first();
                if (isset($comm->id)) {
                    echo $comm->data = $comment;
                    $comm->last_editor_id = Auth::user()->id;
                    if (!$comm->save()) {
                        dd($comm->getErrors());
                    }
                } else {
                    $comm = new UserComments();
                    $comm->pk = $objekts->pk;
                    $comm->data = $comment;
                    $comm->last_editor_id = Auth::user()->id;
                    if (! $comm->save()) {
                        dd($comm->getErrors());
                    }
                }

                $objekts->terms = 1; // confirmed
                $objekts->save();

                // create ticket
                $t = new Ticket;
                $t->user_id = $objekts->id;
                $t->ticket_type = 5;
                $t->ticket_status = 1;
                $t->eticket_nr = 0;
                $t->content = 'Jūsu lietotāja profils sistēmā ir apstiprināts, varat sākt to izmantot pilnībā!';
                $t->save();
            });

            //send email
            Mail::send('mail.rksuccess', ["user" => $objekts], function ($message) use ($objekts) {
                $message->to($objekts->email, $objekts->username.' '.$objekts->surname)
                    ->subject('Skolas.rigaskarte.lv - veiksmīga profila datu pārbaude.');
            });
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }

        return Redirect::to($this->links.'/index/')->with('success', 'Lietotājs veiksmīgi apstiprināts.');
    }

    public function dldoc($doc)
    {
        $user = User::find((int) $doc);

        $file = $user->dokuments;
        $path = public_path().$file;
        if (file_exists($path)) {
            return response()->download($path);
        }
        $path = storage_path().'/documents/'.$file;
        if (file_exists($path)) {
            return response()->download($path);
        }

        return redirect()->back()->withErrors(['not_found' => 'File not found']);
    }

    public function getDeact($objekts)
    {
        // Title
        $title = 'Atcelt lietotāja reģistrācijas apstiprinājumu - '.$objekts->username.' '.$objekts->surname.' ?';

        // Show the page
        return View::make($this->links.'/deactivate', compact('title', 'objekts'));
    }

    public function postDeact($objekts)
    {
        $objekts->terms = 3; // set to unconfirmed
        if (!$objekts->save()) {
            return Redirect::to($this->links.'/index/'.$objekts->id.'/deactivate')
                ->withInput()
                ->withErrors($objekts->getErrors());
        }

        return Redirect::to($this->links.'/index/')
            ->with('success', 'Lietotāja reģistrācijas pieprasījuma statuss ir mainits uz neapstiprināts.');
    }

    /**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $users = User::leftJoin('users_comments AS c', 'c.pk', '=', 'users.pk')
            ->leftJoin('users AS u2', 'c.last_editor_id', '=', 'u2.id')
            ->select([
                'users.id',
                'users.username',
                'users.surname',
                'users.pk',
                'users.created_at',
                'users.terms',
                'users.dokuments',
                'users.info',
                'u2.username as u21',
                'u2.surname as u22',
                'c.created_at as created_at2'
            ]);

        return Datatables::of($users)
            ->edit_column('username', '{{{ $username.\' \'.$surname }}}')
            ->edit_column('terms', '
            @if ($terms == 3 && trim($dokuments)==\'\')
            Nepabeigta reģistrācija
            @elseif ($terms == 3 && trim($dokuments)!=\'\')
            Neapstiprināts
            @elseif ($terms == 1)
            Apstiprināts ({{{ $u21.\' \'.$u22 }}}, {{{ $created_at2 }}})
            @elseif ($terms == 2)
            Importēts no e-maks.lv
            @elseif ($terms == 0)
            Atcelts
            @else
            Cits statuss - {!! $terms !!}
            @endif')

            ->add_column('actions', '@if ($terms == 1)
                                <a href="{{{ URL::to(\'rk/index/\' . $id . \'/deactivate\' ) }}}" class="btn btn-sm btn-danger">Deaktivizēt</a>
                            @elseif ($terms == 0 && $pk != \'\' && $info != \'\')
                                <a href="{{{ URL::to(\'rk/index/\' . $id . \'/confirm\' ) }}}" class="btn btn-sm btn-success">{{{ Lang::get(\'button.confirm\') }}}</a>
                            @elseif ($terms == 3)
                                <a href="{{{ URL::to(\'rk/index/\' . $id . \'/confirm\' ) }}}" class="btn btn-sm btn-success">{{{ Lang::get(\'button.confirm\') }}}</a>
                                <a href="{{{ URL::to(\'rk/index/\' . $id . \'/cancel\' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get(\'button.cancel\') }}}</a>
                            @endif')

            ->remove_column('id')
            ->remove_column('surname')
            ->remove_column('dokuments')
            ->remove_column('info')
            ->remove_column('u21')
            ->remove_column('u22')
            ->remove_column('created_at2')

            ->make();
    }
}
