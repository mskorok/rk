<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Eticket;
use App\Models\ManualPayment;
use App\Models\Settings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class PaymentsController extends BaseController {

    protected $links = 'payments';

    public function getIndex()
    {
        $data = ManualPayment::with("eticket")
            ->where("user_id",Auth::user()->id)
            ->latest()
            ->get();

        return View::make($this->links.'/index', compact('data'));
    }

    public function getDelete($id)
    {
        $manual_payment = ManualPayment::findOrFail($id);

        if ($manual_payment->user_id != Auth::user()->id)
        {
            return Redirect::to('etickets');
        }

        $eticket_nr = Eticket::where('id',$manual_payment->eticket_id)->first()->nr;

        $data = $manual_payment;
        return View::make($this->links.'/delete', compact('data', 'eticket_nr'));
    }

    public function postDelete($id)
    {
        $manual_payment = ManualPayment::findOrFail($id);

        if ($manual_payment->user_id != Auth::user()->id || $manual_payment->confirmed != 0)
        {
            return Redirect::to('etickets');
        }

        if ($manual_payment->delete())
        {
            return Redirect::to('reports/payments')->with('success', 'Maksājums dzēsts.');
        }
        else {
            return Redirect::back()->with('error', 'Neizdevās dzēst!');
        }
    }

    public function getPdf($id)
    {
        $manual_payment = ManualPayment::findOrFail($id);

        if ($manual_payment->user_id != Auth::user()->id)
        {
            return Redirect::to('etickets');
        }

        // get settings data
        $rekviziti = str_replace("\\n","<br />",Settings::first()->payment_form_details);

        $html = '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
                <head profile="http://gmpg.org/xfn/11">
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><style tyle="text/css">body { font-family: DejaVu Sans, sans-serif; }</style></head><body>'.
            View::make('etickets/mtransfer/pdf', compact('manual_payment','rekviziti'))
            .'</body></html>';

        return \PDF::loadHTML($html)->download('maksajuma_uzdevums.pdf');
    }

}