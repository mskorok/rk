<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Mpos;
use App\Models\Seller;
use App\Models\Transaction;
use DateTime;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class GrFreeRepController extends BaseController {

    protected $links = 'grfreemeals';

    public function index()
    {
        // get selected search fields
        $no = trim(Input::get('no'));
        $lidz = trim(Input::get('lidz'));

        $data = [];
        $totals["count"] = 0;
        $totals["sum"] = 0;

        if (Input::get('seller_id'))
        {
            $seller_id = intval(Input::get('seller_id'));

            if ($seller_id>0 && $no!='' && $lidz!='')
            {
                // check dates
                if ($no>=$lidz)
                {
                    Session::flash('error', 'Nekorekts datumu intervāls!');
                }
                else {

                    $datetime = new DateTime($lidz);
                    $datetime->modify('+1 day');
                    $lidz2 = $datetime->format('Y-m-d');

                    // seller
                    $seller = Seller::find($seller_id);
                    if ( ! $seller->id)
                        return Redirect::back()
                            ->withInput()
                            ->with('error','Nav atrasts tirgotājs!');

                    $seller_owner_id = $seller->owner_id;

                    $seller_mposes_ids = Mpos::where("owner_id",$seller_owner_id)
                        ->where("mpos_type",1)
                        ->lists("account_id")->toArray();

                    $data = Transaction::with("mpos","object")
                        ->where('from_account_id', '=', 2)
                        ->whereIn('to_account_id', $seller_mposes_ids)
                        ->where('created_at', '>=', $no)
                        ->where('created_at', '<=', $lidz2)
                        ->orderBy("id","ASC")
                        ->paginate(50);

                    // calculate totals
                    $totals["count"] = $data->getTotal();
                    $totals["sum"] = Transaction::where('from_account_id', '=', 2)
                        ->whereIn('to_account_id', $seller_mposes_ids)
                        ->where('created_at', '>=', $no)
                        ->where('created_at', '<=', $lidz2)
                        ->sum("amount");
                }
            }
            else return Redirect::back()
                    ->withInput()
                    ->with('error','Nav aizpildīti visi lauki!');
        }
        else $seller_id = 0;

        $all_sellers = Seller::get()->lists("seller_name","id")->toArray();
        $all_sellers[0] = " ---Izvēlieties tirgotāju--- ";
        asort($all_sellers);

        // Title
        $title = 'Brīvpusdienu pārskats';

        // Show the page
        $formview = View::make('reports.partials.datesubmit',compact('no','lidz'));
        return View::make('reports/'.$this->links.'/index', compact('title','all_sellers','seller_id','no','lidz','data','totals'))
            ->with('dateintervalssubmit',$formview);
    }

}