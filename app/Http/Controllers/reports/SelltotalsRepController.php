<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\Mpos;
use App\Models\Seller;
use App\Models\Transaction;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class SelltotalsRepController extends BaseController {

    protected $links = 'selltotals';

    public function index()
    {
        // get selected search fields
        $seller = intval(Input::get('seller'));
        $no = trim(Input::get('no'));
        $lidz = trim(Input::get('lidz'));

        if (Input::get('seller'))
        {
            if ($seller>0 && $no!='' && $lidz!='')
            {
                if ($no=='')
                {
                    $no2 = date( 'Y-m-d', strtotime('2000-01-01'));
                }
                else $no2 = date( 'Y-m-d', strtotime($no));

                if ($lidz=='')
                {
                    $lidz2 = date( 'Y-m-d', strtotime("tomorrow"));
                }
                else {
                    $lidz2 = new DateTime($lidz);
                    $lidz2->setTime(23,59,59);
                    $lidz2 = $lidz2->format('Y-m-d H:i:s');
                }

                // seller
                $seller = Seller::find($seller);
                if (!$seller->id)
                {
                    die('Nav atrasts tirgotājs!');
                }
                $seller_owner_id = $seller->owner_id;
                $seller_id = $seller->id;

                // get MPOS devices and their accounts
                $mpos_accounts = Mpos::with('account')->Own($seller_owner_id)->lists('account_id')->toArray();

                $mpos = Mpos::Own($seller_owner_id)->lists('mpos_name','id')->toArray();

                // get seller account
                $seller_account_id = Account::O($seller->owner_id)->sell()->first()->id;



                // get outgoing transaction data
                $d = Transaction::select(DB::raw('mpos_id, SUM(amount) as total, DATE(created_at) as datums'))->Ok();
                $d->whereIn('from_account_id',$mpos_accounts);
                $d->where('to_account_id', $seller_account_id);

                $d->where('created_at','>=' ,$no2);
                $d->where('created_at','<=' ,$lidz2);

                $d->groupBy('datums');
                $d->groupBy('mpos_id');
                $d->orderBy('datums','ASC');
                $outgoing = $d->get()->toArray();

                foreach ($outgoing as $key => $single)
                {
                    $k = $single["datums"].'|'.$single["mpos_id"];
                    $outgoing[$k] = $single["total"];
                    unset($outgoing[$key]);
                }



                // get returned transaction data
                $d = Transaction::select(DB::raw('mpos_id, SUM(amount) as total, DATE(created_at) as datums'))->Ok();
                $d->whereIn('from_account_id',$mpos_accounts);
                $d->where('to_account_id', 999997);

                $d->where('created_at','>=' ,$no2);
                $d->where('created_at','<=' ,$lidz2);

                $d->groupBy('datums');
                $d->groupBy('mpos_id');
                $d->orderBy('datums','ASC');
                $returned = $d->get()->toArray();

                foreach ($returned as $key => $single)
                {
                    $k = $single["datums"].'|'.$single["mpos_id"];
                    $returned[$k] = $single["total"];
                    unset($returned[$key]);
                }




                // get incoming transaction data
                $d = Transaction::select(DB::raw('count(*) as sk, mpos_id, SUM(amount) as total, DATE(created_at) as datums'))->Ok();
                $d->whereIn('to_account_id',$mpos_accounts);
                $d->where('from_account_id','!=',2);

                $d->where('created_at','>=' ,$no2);
                $d->where('created_at','<=' ,$lidz2);

                $d->groupBy('datums');
                $d->groupBy('mpos_id');
                $d->orderBy('datums','ASC');
                $data = $d->get();

                foreach ($data as &$d)
                {
                    $k = $d->datums.'|'.$d->mpos_id;
                    $d->outgoing = (isset($outgoing[$k])) ? $outgoing[$k] : 0;
                    $d->returned = (isset($returned[$k])) ? $returned[$k] : 0;
                    $d->daytotal = $d->total-$d->returned-$d->outgoing;
                }

            }
            else {
                Session::flash('error', 'Nav aizpildīti visi lauki!');
            }
        }

        if (!isset($data)) $data=array();
        if (!isset($mpos)) $mpos=array();
        if (!isset($seller_id)) $seller_id = 0;

        $all_sellers = Seller::all();

        // Title
        $title = 'Tirgotāja kopējais pārskats';

        // Show the page
        $formview = View::make('reports.partials.datesubmit',compact('no','lidz'));
        return View::make('reports/'.$this->links.'/index', compact('title','all_sellers','data','mpos','seller_id'))
            ->with('dateintervalssubmit',$formview);
    }

}