<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\Akts;
use App\Models\Seller;
use App\Models\SellerBalances;
use App\Models\Settings;
use App\Models\Transaction;
use Barryvdh\DomPDF\PDF;
use DateTime;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AktsRepController extends BaseController {

    protected $links = 'akts';

    public function index()
    {
        // get selected search fields
        $seller = intval(Input::get('seller'));
        $no = trim(Input::get('no'));
        $lidz = trim(Input::get('lidz'));

        if (Input::get('seller'))
        {
            if ($seller>0 && $no!='' && $lidz!='')
            {
                // check dates
                if (($no>=$lidz) OR ($lidz>date('Y-m-d',strtotime("-1 days"))) )
                {
                    Session::flash('error', 'Nekorekts datumu intervāls, beigu datums nevar but jaunāks par vakardienu!');
                }
                else {

                    // seller
                    $seller = Seller::find($seller);
                    if (!$seller->id)
                    {
                        die('Nav atrasts tirgotājs!');
                    }
                    $seller_id = $seller->id;
                    $seller_owner_id = $seller->owner_id;


                    // check for existing PDF
                    // if not generate and assign nr
                    $new=0;

                    $existing = Akts::select('id','akts_nr','akts_nrt')
                        ->where('seller_id',$seller_id)
                        ->where('datums_no',$no)
                        ->where('datums_lidz',$lidz)
                        ->first();
                    if (isset($existing->id))
                    {
                        $outputName = md5($existing->akts_nr.$existing->akts_nrt);

                        return response()->download(public_path('assets/aktspdfs').'/'.$outputName.'.pdf', 'akts_'.$outputName.'.pdf');
                    }
                    else { // assign new nr

                        $noa = explode("-",$no);
                        $nrt = '/'.$noa[1].'/'.$noa[0].'/'.substr($seller->seller_name,0,40);

                        $last = Akts::where('seller_id',$seller_id)
                            ->where('akts_nrt',$nrt)
                            ->orderBy('akts_nr','DESC')
                            ->first();

                        if (!isset($last->akts_nr))
                        {
                            $akts_nr = 1;
                        }
                        else $akts_nr = $last->akts_nr+1;
                    }

                    // start balance
                    $previous_day_date = new DateTime($no);
                    $previous_day = $previous_day_date->format('Y-m-d');

                    $start_balance = intval(SellerBalances::where('datums',$previous_day)
                        ->where('seller_id',$seller)
                        ->value('balance_paid'));

                    // end balance
                    $end_balance = intval(SellerBalances::where('datums',$lidz)
                        ->where('seller_id',$seller)
                        ->value('balance_paid'));

                    // get daily balances for this period
                    $daily_balances_sum = intval(SellerBalances::where('datums','>=',$no)
                        ->where("datums",'<',$lidz)
                        ->sum("daily_paid"));

                    // settings
                    $settings = Settings::firstOrFail();

                    // get seller account
                    $seller_account_id = Account::O($seller->owner_id)->sell()->first()->id;

                    if ($no=='')
                    {
                        $no2 = date( 'Y-m-d', strtotime('2000-01-01'));
                    }
                    else $no2 = date( 'Y-m-d', strtotime($no));

                    if ($lidz=='')
                    {
                        $lidz2 = date( 'Y-m-d', strtotime("tomorrow"));
                    }
                    else {
                        $lidz2 = new DateTime($lidz);
                        $lidz2->setTime(23,59,59);
                        $lidz2 = $lidz2->format('Y-m-d H:i:s');
                    }

                    // get outgoing payment sum
                    $outgoing_paid = Transaction::where('from_account_id',$seller_account_id)
                        ->where('to_account_id',999999)
                        ->where('created_at','>=' ,$no2)
                        ->where('created_at','<=' ,$lidz2)
                        ->Ok()
                        ->sum('amount');

                    // get outgoing commision sum
                    $outgoing_comision = 0;

                    // get incoming payment sum
                    $d = SellerBalances::where("seller_id",$seller->id);
                    $d->where('datums','>=' ,$no2);
                    $d->where('datums','<=' ,$lidz2);
                    $data = $d->sum("daily_paid");

                    $total_in = $data;

                    $months = array(
                        '1' => 'janvārī',
                        '2' => 'februārī',
                        '3' => 'martā',
                        '4' => 'aprīlī',
                        '5' => 'maijā',
                        '6' => 'jūnijā',
                        '7' => 'jūlijā',
                        '8' => 'augustā',
                        '9' => 'septembrī',
                        '10' => 'oktobrī',
                        '11' => 'novembrī',
                        '12' => 'decembrī'
                    );

                    // generate PDF

                    $html = '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
                        <head profile="http://gmpg.org/xfn/11">
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><style tyle="text/css">body { font-family: DejaVu Sans, sans-serif; font-size:12px; }</style></head><body>'.
                        View::make('reports/'.$this->links.'/report', compact('no','lidz','seller','settings','start_balance','end_balance','total_in','outgoing_paid','outgoing_comision','months','akts_nr','nrt'))
                        .'</body></html>';

                    define('PDFS_DIR', public_path('assets/aktspdfs'));

                    if (!is_dir(PDFS_DIR)){
                        mkdir(PDFS_DIR, 0755, true);
                    }

                    $outputName = md5($akts_nr.$nrt);

                    $pdfPath = PDFS_DIR.'/'.$outputName.'.pdf';
                    File::put($pdfPath, \PDF::loadHTML($html)->output());

                    // insert new row in akts table
                    $akts = New Akts();
                    $akts->seller_id = $seller_id;
                    $akts->datums_no = $no;
                    $akts->datums_lidz = $lidz;
                    $akts->akts_nr = $akts_nr;
                    $akts->akts_nrt = $nrt;
                    $akts->user_id = Auth::user()->id;
                    $akts->save();

                    return response()->download(public_path('assets/aktspdfs').'/'.$outputName.'.pdf', 'akts_'.$outputName.'.pdf');

                }
            }
            else {
                Session::flash('error', 'Nav aizpildīti visi lauki!');
            }
        }

        $all_sellers = Seller::all();

        // Title
        $title = 'Izveidot salīdzināšanas aktu';

        // Show the page
        $formview = View::make('reports.partials.datesubmit',compact('no','lidz'));
        return View::make('reports/'.$this->links.'/index', compact('title','all_sellers'))
            ->with('dateintervalssubmit',$formview);
    }

}