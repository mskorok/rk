<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Eticket;
use App\Models\EticketsObjekti;
use App\Models\Mpos;
use App\Models\Objekts;
use App\Models\Owner;
use App\Models\Seller;
use App\Models\Transaction;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class ObjRepController extends BaseController {

    protected $links = 'objekts';

    public function getObjekts()
    {

        $admin_user = $this->admin_user();

        if ($admin_user==0)
        {
            // get all object owners for user
            $all_object_owners = $this->get_user_object_owners();

            // get all objects
            $all_objects = $this->get_owner_objects($all_object_owners);
        }
        else {
            // get all objects
            $all_objects = $this->get_all_objects();
        }

        // get selected account
        $account = intval(Input::get('account'));

        if ($account>0)
        {
            $account_id = $account;

            // check if privileged
            if (!in_array($account_id, $all_objects))
            {
                $user = Auth::user();
                return View::make('site/user/index', compact('user'))->with('error','Nav piekļuves tiesību!');
            }

            // get selected search fields
            $no = trim(Input::get('no'));
            $lidz = trim(Input::get('lidz'));

            // get object data
            $object = Objekts::find($account_id);
            $username = $object->object_name;

            // get data for selected account
            $d = Transaction::with(array('from_account','to_account'));
            $d->where('object_id',$account_id)->Ok();

            if (($no!='') || ($lidz!=''))
            {
                if ($no=='')
                {
                    $no2 = date( 'Y-m-d', strtotime('2000-01-01'));
                }
                else $no2 = date( 'Y-m-d', strtotime($no));

                if ($lidz=='')
                {
                    $lidz2 = date( 'Y-m-d', strtotime("tomorrow"));
                }
                else {
                    $lidz2 = new DateTime($lidz);
                    $lidz2->setTime(23,59,59);
                    $lidz2 = $lidz2->format('Y-m-d H:i:s');
                }

                $d->where('created_at','>=' ,$no2);
                $d->where('created_at','<=' ,$lidz2);
            }

            $data_totals = $d->get();

            $data = $d->paginate(50);

            // calculate totals
            $totals["in"] = 0;
            $totals["out"] = 0;
            foreach ($data_totals as $key => $single)
            {
                if ($single->from_account_id==999997)
                {
                    $totals["out"]+=$single->amount;
                }
                else $totals["in"]+=$single->amount;
            }

            // Title
            $title = 'Vēsture - objekta vēsture - '.$username;
        }
        else {
            $account_id = 0;

            $data = array();

            $totals = 0;

            // Title
            $title = 'Vēsture - objekta vēsture';
        }

        if (count($all_objects)>0)
        {
            if ($admin_user==0)
            {
                $all_acc = Objekts::whereIn('id',$all_objects)->get();
            }
            else {
                $all_acc = Cache::remember('all_objects', 30, function()
                {
                    $all_acc = Objekts::all();

                    return $all_acc;
                });
            }
        }
        else $all_acc = array();

        $all_mpos = $this->get_all_mpos();

        // Show the page
        $formview = View::make('reports.partials.datesubmit',compact('no','lidz'));
        return View::make('reports/'.$this->links.'/index', compact('title','data','account_id','username','all_acc','no','lidz','totals','all_mpos'))
            ->with('dateintervalssubmit',$formview);
    }

    public function getObjEtickets()
    {

        $admin_user = $this->admin_user();

        if ($admin_user==0)
        {
            // get all object owners for user
            $all_object_owners = $this->get_user_object_owners();

            // get all objects
            $all_objects = $this->get_owner_objects($all_object_owners);
        }
        else {
            // get all objects
            $all_objects = $this->get_all_objects();
        }

        // get selected account
        $account = intval(Input::get('account'));

        if ($account>0)
        {
            $account_id = $account;

            Session::put('report_object', $account_id);

            // check if privileged
            if (!in_array($account_id, $all_objects))
            {
                $user = Auth::user();
                return View::make('site/user/index', compact('user'))->with('error','Nav piekļuves tiesību!');
            }

            // get selected search fields
            $eticket_data = trim(Input::get('eticket'));
            $eticket_nr = substr(trim(Input::get('eticket')),0,strpos(trim(Input::get('eticket')),","));
            $pin = trim(Input::get('pin'));
            $blocked = Input::get('blocked');

            $eticket = Eticket::where('nr',$eticket_nr)->value('id');

            // get object data
            $object = Objekts::find($account_id);
            $username = $object->object_name;

            // get data
            $etickets = EticketsObjekti::Obj($object->id)
                ->where(function($query) use ($eticket)
                {
                    if ($eticket>0) {
                        $query->where('eticket_id', $eticket);
                    }
                })
                ->lists('eticket_id')->toArray();

            if (count($etickets)==0) $etickets = array('0');

            // eticket data
            $d = Eticket::whereIn('id',$etickets)
                ->where(function($query) use ($pin,$blocked)
                {
                    if ($pin=='yes') {
                        $query->where('pin' ,'!=','0000');
                    }
                    else if ($pin=='no') {
                        $query->where('pin', '0000');
                    }

                    if ($blocked=='yes') {
                        $query->where('eticket_status' ,2);
                    }
                    else if ($blocked=='no') {
                        $query->where('eticket_status', '!=',2);
                    }
                });

            $data_totals = $d->get();

            $data = $d->paginate(50);

            // calculate totals
            $totals = count($data_totals);

            // Title
            $title = 'Objekta e-talonu atskaite - '.$username;
        }
        else {
            $account_id = 0;
            Session::forget('report_object');

            $data = array();
            $pin='';
            $blocked='';

            $totals = 0;
            $eticket_data = '';

            // Title
            $title = 'Objekta e-talonu atskaite';
        }

        if (count($all_objects)>0)
        {
            if ($admin_user==0)
            {
                $all_acc = Objekts::whereIn('id',$all_objects)->get();
            }
            else {
                $all_acc = Cache::remember('all_objects', 30, function()
                {
                    $all_acc = Objekts::all();

                    return $all_acc;
                });
            }
        }
        else $all_acc = array();

        // Show the page
        return View::make('reports/'.$this->links.'/etickets', compact('title','data','account_id','username','all_acc','eticket_data','pin','blocked','totals'));
    }

    public function getObjSeller()
    {

        $admin_user = $this->admin_user();

        if ($admin_user==0)
        {
            // get all object owners for user
            $all_object_owners = $this->get_user_object_owners();

            // get all objects
            $all_objects = $this->get_owner_objects($all_object_owners);
        }
        else {
            // get all objects
            $all_objects = $this->get_all_objects();
        }

        $seller_name = 'visi';

        // get selected account
        $account = intval(Input::get('account'));

        if ($account>0)
        {
            $account_id = $account;

            // check if privileged
            if (!in_array($account_id, $all_objects))
            {
                $user = Auth::user();
                return View::make('site/user/index', compact('user'))->with('error','Nav piekļuves tiesību!');
            }

            // get selected search fields
            $no = trim(Input::get('no'));
            $lidz = trim(Input::get('lidz'));
            $seller = intval(trim(Input::get('seller')));

            // get object data
            $object = Objekts::find($account_id);
            $username = $object->object_name;

            // get seller data for this object
            $all_sellers = array();
            $all_sell_owners = Mpos::Obj($account_id)->select('owner_id')->distinct()->get()->toArray();
            foreach ($all_sell_owners as $o)
            {
                $owner_id = $o["owner_id"];
                $sell = Cache::rememberForever('seller_byowner_'.$owner_id, function() use ($owner_id)
                {
                    return Seller::where("owner_id",$owner_id)->get()->first();
                });

                if ($sell!=null)
                {
                    array_push($all_sellers, $sell->toArray());
                }
            }

            // get data for selected account
            if (($no!='') || ($lidz!=''))
            {
                if ($no=='')
                {
                    $no2 = date( 'Y-m-d', strtotime('2000-01-01'));
                }
                else $no2 = date( 'Y-m-d', strtotime($no));

                if ($lidz=='')
                {
                    $lidz2 = date( 'Y-m-d', strtotime("tomorrow"));
                }
                else {
                    $lidz2 = new DateTime($lidz);
                    $lidz2->setTime(23,59,59);
                    $lidz2 = $lidz2->format('Y-m-d H:i:s');
                }
            }

            if ($seller>0)
            {
                $sell = Seller::findOrFail($seller);

                $seller_name = $sell->seller_name;
                $own = $sell->owner_id;
                $all_mposes = MPos::Own($own)->lists('account_id')->toArray();

                if (array_search(0,$all_mposes)!==false)
                    unset($all_mposes[array_search(0,$all_mposes)]);
            }
            else {
                $own = array();
                foreach ($all_sellers as $s) {
                    array_push($own,$s["owner_id"]);
                }

                if (count($own)>0)
                {
                    $all_mposes = MPos::whereIn('owner_id',$own)->lists('account_id')->toArray();

                    unset($all_mposes[array_search(0,$all_mposes)]);
                }
                else $all_mposes = array();

            }

            if (count($all_mposes)>0)
            {
                $d = Transaction::where('object_id',$account_id);
                $d->whereIn('to_account_id',$all_mposes);
                $d->where('from_account_id','!=',2);
                $d->select(DB::raw('sum(amount) as summa, mpos_id'));
                $d->groupBy('mpos_id');

                if (($no!='') || ($lidz!=''))
                {
                    $d->where('created_at','>=' ,$no2);
                    $d->where('created_at','<=' ,$lidz2);
                }

                $data = $d->get();

                $seller_data = array();
                $totals["in"] = 0;

                if ($seller>0)
                {
                    foreach ($data as $d)
                    {
                        $totals["in"]+=$d->summa;
                    }
                    $seller_data[0] = array(
                        'name'  => $seller_name,
                        'summa' => $totals["in"]
                    );
                }
                else {
                    $all_mposes2 = MPos::whereIn('owner_id',$own)->lists('owner_id','id')->toArray();

                    foreach ($data as $d)
                    {
                        if (!isset($seller_data[$all_mposes2[$d->mpos_id]])) // create
                        {
                            $seller_data[$all_mposes2[$d->mpos_id]] = array(
                                'name'  => Owner::where('id',$all_mposes2[$d->mpos_id])->first()->owner_name,
                                'summa' => $d->summa
                            );
                        }
                        else { // sum
                            $seller_data[$all_mposes2[$d->mpos_id]]["summa"]+= $d->summa;
                        }

                        $totals["in"]+=$d->summa;
                    }
                }

                $data = $seller_data;
            }
            else { // no MPOS devices
                $data = array();
                $totals["in"] = 0;
            }

            // Title
            $title = 'Objekta tirgotāju atskaite, objekts - '.$username.', tirgotājs - '.$seller_name;
        }
        else {
            $account_id = 0;
            $seller = 0;

            $data = array();
            $all_sellers = array();

            $totals = 0;

            // Title
            $title = 'Objekta tirgotāju atskaite';
        }

        if (count($all_objects)>0)
        {
            if ($admin_user==0)
            {
                $all_acc = Objekts::whereIn('id',$all_objects)->get();
            }
            else {
                $all_acc = Cache::remember('all_objects', 30, function()
                {
                    $all_acc = Objekts::all();

                    return $all_acc;
                });
            }
        }
        else $all_acc = array();

        // Show the page
        $formview = View::make('reports.partials.datesubmit',compact('no','lidz'));
        return View::make('reports/objekts/seller', compact('title','data','account_id','username','all_acc','no','lidz','totals','all_sellers','seller','seller_name'))
            ->with('dateintervalssubmit',$formview);
    }

}