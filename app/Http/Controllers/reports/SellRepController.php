<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\OwnerAccess;
use App\Models\Transaction;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;

class SellRepController extends BaseController {

    protected $links = 'seller';

	public function getSeller()
	{
        list($admin_user, $all_accounts) = $this->get_all_accounts();

        // get selected account
        $account = intval(Input::get('account'));
        $mpos_id = intval(Input::get('mpos'));
        $object_id = intval(Input::get('object'));
        $veids = intval(Input::get('veids'));
        $grupet = intval(Input::get('grupet'));
        $no = trim(Input::get('no'));
        $lidz = trim(Input::get('lidz'));

        $freemeals[0] = 0;
        $freemeals[1] = 0;
        $freemeals[2] = 0;
        $freemeals[3] = 0;
        $freemeals[4] = 0;
        $freemeals[41] = 0;
        $freemeals[42] = 0;

        // nothing selected
        if ($account <= 0)
        {
            $account_id = 0;
            $veids = 0;
            $grupet = 0;

            $data = array();

            $totals = 0;

            // Title
            $title = 'Vēsture - tirgotāja vēsture';
        }
        else {
            $account_id = $account;

            // check if privileged
            if (!in_array($account_id, $all_accounts))
            {
                $user = Auth::user();
                return View::make('site/user/index', compact('user'))->with('error','Nav piekļuves tiesību!');
            }

            // get seller data
            $seller = Account::with('Owner')->find($account_id);
            $username = $seller->owner->owner_name;

            list($mpos_accounts, $seller_accounts, $mpos_accounts2, $seller_free_meals_account, $seller_objects, $no, $lidz, $data_totals, $data) =
                $this->calculate_data($seller, $account_id, $veids, $no, $lidz, $object_id, $mpos_id, $grupet);

            if ($lidz == 'error')
                return Redirect::back()->withInput()->with("error","Nekorekts datuma formāts!");

            // calculate totals
            $totals = $this->calculate_totals($data_totals, $seller_accounts, $mpos_accounts2, $account_id, $seller_free_meals_account);

            // calculate free meal type totals
            foreach ($data_totals as $single)
            {
                if ($single->from_account_id==2)
                {
                    if (intval($single->freemeals_type) == 4)
                    {
                        $freemeals[4]++;

                        $grade = 0;
                        if (isset($single->frome->grade))
                        {
                            $grade = intval($single->frome->grade);
                        }

                        if ($grade > 0)
                        {
                            if ($grade <= 4)
                            {
                                $freemeals[41]++;
                            }
                            else $freemeals[42]++;
                        }
                    }
                    elseif (intval($single->freemeals_type) == 3)
                        $freemeals[3]++;
                    elseif (intval($single->freemeals_type) == 2)
                        $freemeals[2]++;
                    elseif (intval($single->freemeals_type) == 1)
                        $freemeals[1]++;
                    elseif (intval($single->freemeals_type) == 0)
                        $freemeals[0]++;
                }
            }

            // Title
            $title = 'Vēsture - tirgotāja vēsture - '.$username;
        }

        $all_acc = $this->get_all_acc($all_accounts, $admin_user);

        $all_mpos = $this->get_all_mpos();

        // Show the page
        $formview = View::make('reports.partials.datesubmit',compact('no','lidz'));
        return View::make('reports/'.$this->links.'/index', compact('title','data','account_id','username','all_acc','no','lidz','totals',
            'mpos_accounts','mpos_id','seller_objects','object_id','all_mpos','admin_user','seller_free_meals_account','veids','grupet','freemeals'))
            ->with('dateintervalssubmit',$formview);
	}

    /**
     * @return array
     */
    private function get_all_accounts()
    {
        $user_id = Auth::user()->id;

        $admin_user = $this->admin_user();
        $accountant_user = $this->accountant_user();

        if ($admin_user || $accountant_user) {
            // get all sellers
            $all_accounts = $this->get_all_sellers();
            return array($admin_user, $all_accounts);
        } else {
            // get all owners for user
            $all_seller_owners = array();
            $owners = OwnerAccess::with('owner')->Seller()->U($user_id)->get();
            foreach ($owners as $o) {
                array_push($all_seller_owners, $o->owner->id);
            }

            // get all seller accounts (type=3) for owners
            $all_accounts = array();
            foreach ($all_seller_owners as $o) {
                $accounts = Account::O($o)->Sell()->get();
                foreach ($accounts as $a) {
                    array_push($all_accounts, $a->id);
                }
            }
            return array($admin_user, $all_accounts);
        }
    }

    /**
     * @param $totals
     * @param $data_totals
     * @param $seller_accounts
     * @param $mpos_accounts2
     * @param $account_id
     * @param $seller_free_meals_account
     * @return mixed
     */
    private function calculate_totals(
        $data_totals,
        $seller_accounts,
        $mpos_accounts2,
        $account_id,
        $seller_free_meals_account
    ) {
        $totals["in"] = 0;
        $totals["out"] = 0;
        $totals["free"] = 0;
        foreach ($data_totals as $key => $single) {
            if (in_array($single->to_account_id, $seller_accounts) && in_array($single->from_account_id,
                    $mpos_accounts2)
            ) {
                // outgoing payment
                $totals["out"] = $totals["out"] + $single->amount;
            } else {
                if ($single->from_account_id == 2) {
                    // free meals
                    $totals["free"]++;
                } else {
                    if ($single->from_account_id == $account_id) {
                        $totals["out"] = $totals["out"] + $single->amount;
                    } else {
                        if ($single->to_account_id == $account_id) {
                            $totals["in"] = $totals["in"] + $single->amount;
                        } else {
                            if (in_array($single->from_account_id, $seller_accounts)) {
                                if ($single->to_account_id != $seller_free_meals_account->id) {
                                    $totals["out"] = $totals["out"] + $single->amount;
                                }
                            } else {
                                if (in_array($single->to_account_id, $seller_accounts)) {
                                    $totals["in"] = $totals["in"] + $single->amount;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $totals;
    }

    /**
     * @param $all_accounts
     * @param $admin_user
     * @return array|mixed
     */
    private function get_all_acc($all_accounts, $admin_user)
    {
        if (count($all_accounts) > 0) {
            if ($admin_user == 0) {
                $all_acc = Account::with('Owner')->whereIn('id', $all_accounts)->get();
                return $all_acc;
            } else {
                $all_acc = Cache::remember('all_sellers', 30, function () {
                    $all_acc = Account::with('Owner')->Sell()->get();

                    return $all_acc;
                });
                return $all_acc;
            }
        } else $all_acc = array();
        {
            return $all_acc;
        }
    }

    /**
     * @param $seller
     * @param $account_id
     * @param $veids
     * @param $no
     * @param $lidz
     * @param $object_id
     * @param $mpos_id
     * @param $grupet
     * @return array
     */
    private function calculate_data($seller, $account_id, $veids, $no, $lidz, $object_id, $mpos_id, $grupet)
    {
        // get seller accounts
        $mpos_accounts = $this->get_seller_mpos_accounts($seller->owner->id);
        $seller_accounts = $this->get_seller_accounts($mpos_accounts, $account_id);
        $mpos_accounts2 = array();

        foreach ($mpos_accounts as $value)
        {
            if ($value->account_id > 0)
                array_push($mpos_accounts2, $value->account_id);
        }

        $seller_free_meals_account = Account::SellerFreeMeals()->O($seller->owner->id)->firstOrFail();

        // get seller objects
        $seller_objects = $this->get_seller_objects($seller->owner->id);

        // get data for selected account
        $d = Transaction::with(array('from_account', 'to_account', 'fromEt'));
        $d->Ok();
        $d->where(function ($query) use ($mpos_accounts2) {
            $query->whereIn('from_account_id', $mpos_accounts2)
                ->orwhereIn('to_account_id', $mpos_accounts2);
        });

        if ($veids == 1) {
            $d->where("from_account_id", '!=', 2);
            $d->where("to_account_id", '!=', $seller_free_meals_account->id);
        }
        else {
            if ($veids == 2)
            {
                $d->where(function ($query) use ($seller_free_meals_account) {
                    $query->where('from_account_id', 2)
                        ->orwhere('to_account_id', $seller_free_meals_account->id);
                });
            }
        }

        if (($no != '') || ($lidz != ''))
        {
            if ($no == '')
            {
                $no2 = date('Y-m-d', strtotime('2000-01-01'));
            } else {
                $no2 = date('Y-m-d', strtotime($no));
            }

            if ($lidz == '')
            {
                $lidz2 = date('Y-m-d', strtotime("tomorrow"));
            } else {
                try {
                    $lidz2 = new DateTime($lidz);
                    $lidz2->setTime(23, 59, 59);
                    $lidz2 = $lidz2->format('Y-m-d H:i:s');
                } catch (\Exception $e) {
                    $lidz2 = 'error';
                }
            }

            $d->where('created_at', '>=', $no2);
            $d->where('created_at', '<=', $lidz2);
        }
        $no = isset($no2) ? $no2 : '';
        $lidz = isset($lidz2) ? $lidz2 : '';

        if ($object_id > 0) {
            $d->where('object_id', '=', $object_id);
        }

        if ($mpos_id > 0) {
            $d->where('mpos_id', '=', $mpos_id);
        }

        if ($grupet == 1) {
            $d->orderBy("object_id", "ASC");
        }

        $d->orderBy("created_at", "ASC");

        $data_totals = $d->get();

        $data = ($grupet == 0) ? $d->paginate(100) : $d->get();

        return array(
            $mpos_accounts,
            $seller_accounts,
            $mpos_accounts2,
            $seller_free_meals_account,
            $seller_objects,
            $no,
            $lidz,
            $data_totals,
            $data
        );
    }

    public function getXls($data)
    {
        try {
            $data = base64_decode($data);
            $split = explode("|",$data);
            $no = $split[0];
            $lidz = $split[1];
            $account_id = $split[2];
            $object_id = $split[3];
            $mpos_id = $split[4];
            $grupet = 1;
            $veids = $split[6];
        } catch (\Exception $e) {
            dd("Nekorekti parametri!");
        }

        list($admin_user, $all_accounts) = $this->get_all_accounts();
        $all_mpos = $this->get_all_mpos();

        // get seller data
        $seller = Account::with('Owner')->find($account_id);

        list($mpos_accounts, $seller_accounts, $mpos_accounts2, $seller_free_meals_account, $seller_objects, $no, $lidz, $data_totals, $data) =
            $this->calculate_data($seller, $account_id, $veids, $no, $lidz, $object_id, $mpos_id, $grupet);

        if ($lidz == 'error')
            return Redirect::back()->withInput()->with("error","Nekorekts datuma formāts!");

        // calculate totals
        $totals = $this->calculate_totals($data_totals, $seller_accounts, $mpos_accounts2, $account_id, $seller_free_meals_account);

        $data_print = [];
        $sk = 0;
        if (count($data) > 0)
        {
            foreach ($data as $single)
            {
                if ($grupet == 1)
                {
                    if ( ! isset($last_object_id))
                    {
                        $last_object_id =  $single->object_id;
                        $subtotal["in"] = 0;
                        $subtotal["out"] = 0;
                        $subtotal["free"] = 0;
                    }
                    elseif ($last_object_id != $single->object_id)
                    {
                        array_push($data_print[$sk],
                        [
                            "Ienākošais",
                            sprintf("%.2f", $subtotal["in"]/100)
                        ]);
                        array_push($data_print[$sk],
                        [
                            "Izejošais",
                            sprintf("%.2f", $subtotal["out"]/100)
                        ]);
                        if ($subtotal["free"] > 0)
                            array_push($data_print[$sk],
                            [
                                "Izsniegtas brīvpusdienas",
                                $subtotal["free"]
                            ]);

                        $sk++;

                        $last_object_id =  $single->object_id;
                        $subtotal["in"] = 0;
                        $subtotal["out"] = 0;
                        $subtotal["free"] = 0;
                    }

                    if ($single->object_id > 0 && $single->from_account_id > 2)
                        $subtotal["in"]+= $single->amount;
                    if ($single->object_id == 0 && $single->to_account_id != $seller_free_meals_account->id)
                        $subtotal["out"]+= $single->amount;
                    if ($single->from_account_id == 2)
                        $subtotal["free"]++;
                }

                $row = [];

                if ($single->from_account_id==0)
                    $row[0] = 'Bankas pārskaitījums';
                elseif ($single->from_account_id==999997)
                    $row[0] = 'Naudas atgriešanas atcelšana';
                elseif ($single->from_account_id==2) {
                    $row[0] = 'E-talons ';
                    if ($admin_user) { $row[0].=$single->from_eticket_nr; } else { $row[0].= "*******".substr($single->from_eticket_nr,-3,3); }
                    $row[0].='brīvpusdienas';
                }
                elseif ($single->from_account->account_type==6)
                    $row[0] = 'Pārskaitījums no MPOS ('.$all_mpos[$single->from_account->owner_id].')';
                elseif ($single->from_account->account_type==2) {
                    $row[0] = 'E-talons ';
                    if ($admin_user) { $row[0].=$single->from_eticket_nr; } else { $row[0].= "*******".substr($single->from_eticket_nr,-3,3); }
                }
                elseif ($single->from_account->account_type==5)
                    $row[0] = 'E-talonu grupa';
                elseif ($single->from_account->account_type==3)
                    $row[0] = 'Tirgotāja konts: '.$single->from_account->owner->owner_name;
                else $row[0] = $single->from_account_id;

                if ($admin_user == 1 && $single->comments == 'Manual Serial')
                    $row[0].= ' (manual)';
                

                if ($single->to_account_id==0)
                    $row[1] = 'Nav definēts konts';
                elseif ($single->to_account!==NULL)
                {
                    if ($single->to_account->account_type==3)
                        $row[1] = 'Tirgotāja konts: '.$single->to_account->owner->owner_name;
                    elseif ($single->to_account->account_type==6)
                        $row[1] = 'MPOS ('.$all_mpos[$single->to_account->owner_id].')';
                }
                else {
                    if ($single->to_account_id == 999999)
                        $row[1] = 'Naudas izmaksa';
                    elseif ($single->to_account_id == 999998)
                        $row[1] = 'Naudas izmaksas komisija';
                    elseif ($single->to_account_id == 999997)
                        $row[1] = 'Naudas atgriešana';
                    else
                        $row[1] = $single->to_account_id;
                }

                if ($single->from_account_id==2 || $single->to_account_id == $seller_free_meals_account->id)
                    $row[2] = 'Brīvpusdienas';
                else
                    $row[2] = sprintf("%.2f",$single->amount/100);

                $row[3] = $single->created_at;

                $data_print[$sk][] = $row;
            }

            if ($grupet == 1)
            {
                array_push($data_print[$sk],
                    [
                        "Ienākošais",
                        sprintf("%.2f", $subtotal["in"]/100)
                    ]);
                array_push($data_print[$sk],
                    [
                        "Izejošais",
                        sprintf("%.2f", $subtotal["out"]/100)
                    ]);
                if ($subtotal["free"] > 0)
                    array_push($data_print[$sk],
                        [
                            "Izsniegtas brīvpusdienas",
                            $subtotal["free"]
                        ]);
            }
        }

        $data = ($grupet == 0) ? $data_print[0] : $data_print;

        // render and push XLS
        Excel::create('Tirgotāja atskaite '.$no.' - '.$lidz, function($excel) use ($data, $totals, $no, $lidz, $grupet)
        {
            $excel->setTitle('Tirgotāja atskaite '.$no.' - '.$lidz);
            $excel->setCreator('skolas.rigaskarte.lv')->setCompany('skolas.rigaskarte.lv');
            $excel->setDescription('Tirgotāja atskaite '.$no.' - '.$lidz);

            if ($grupet == 0)
            {
                $excel->sheet('Atskaite', function($sheet) use($data, $totals, $no, $lidz)
                {
                    $sheet->setOrientation('landscape');
                    $sheet->getPageSetup()->setFitToPage(false);
                    $sheet->getDefaultRowDimension()->setRowHeight(-1);
                    $sheet->setWidth(array(
                        'A'     =>  35,
                        'B'     =>  35,
                        'C'     =>  15,
                        'D'     =>  20,
                    ));
                    $sheet->setHeight(0, 20);

                    $sheet->fromArray($data, null, 'A1', true, false);

                    $sheet->prependRow(['No', 'Uz', 'Summa EUR', 'Datums, laiks']);

                    $sheet->freezeFirstRow();

                    $sheet->appendRow([' ']);
                    $sheet->appendRow(['Kopā par periodu:']);
                    $sheet->appendRow(['Ienākošais:', sprintf("%.2f",$totals["in"]/100)]);
                    $sheet->appendRow(['Izejošais:', sprintf("%.2f",$totals["out"]/100)]);
                    if ($totals["free"] > 0)
                        $sheet->appendRow(['Izsniegtas brīvpusdienas:', $totals["free"]]);

                    $sheet->cells('A1:D1', function($cells) {
                        $cells->setFontWeight('bold')->setBackground('#CCCCCC');
                    });

                    $sheet->setColumnFormat(array(
                        'C2:C3999' => '0.00'
                    ));

                    $lastrow = count($data) + 3;
                    $lastrow2 = $lastrow + 4;

                    $sheet->cells("A$lastrow:D$lastrow2", function($cells) {
                        $cells->setFontWeight('bold');
                    });
                });
            }
            else {
                foreach ($data as $key => $lapa)
                {
                    $excel->sheet('Lapa'.$key, function($sheet) use($lapa, $totals, $no, $lidz)
                    {
                        $sheet->setOrientation('landscape');
                        $sheet->getPageSetup()->setFitToPage(false);
                        $sheet->getDefaultRowDimension()->setRowHeight(-1);
                        $sheet->setWidth(array(
                            'A'     =>  35,
                            'B'     =>  35,
                            'C'     =>  15,
                            'D'     =>  20,
                        ));
                        $sheet->setHeight(0, 20);

                        $sheet->fromArray($lapa, null, 'A1', true, false);

                        $sheet->prependRow(['No', 'Uz', 'Summa EUR', 'Datums, laiks']);

                        $sheet->freezeFirstRow();

                        $sheet->appendRow([' ']);
                        $sheet->appendRow(['Kopā par periodu:']);
                        $sheet->appendRow(['Ienākošais:', sprintf("%.2f",$totals["in"]/100)]);
                        $sheet->appendRow(['Izejošais:', sprintf("%.2f",$totals["out"]/100)]);
                        if ($totals["free"] > 0)
                            $sheet->appendRow(['Izsniegtas brīvpusdienas:', $totals["free"]]);

                        $sheet->cells('A1:D1', function($cells) {
                            $cells->setFontWeight('bold')->setBackground('#CCCCCC');
                        });

                        $sheet->setColumnFormat(array(
                            'C2:C3999' => '0.00'
                        ));

                        $lastrow = count($lapa) + 3;
                        $lastrow2 = $lastrow + 4;

                        $sheet->cells("A$lastrow:D$lastrow2", function($cells) {
                            $cells->setFontWeight('bold');
                        });
                    });
                }
            }

        })->download('xlsx');
    }

}