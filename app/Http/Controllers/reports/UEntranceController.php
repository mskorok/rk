<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Entrance;
use App\Models\Eticket;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class UEntranceController extends BaseController
{

    protected $links = 'uentrance';

    public function getEntrance()
    {
        $all_et = $this->getUsersActiveEtickets();

        $all_et_ids = array();
        foreach ($all_et as $et) {
            array_push($all_et_ids, $et->id);
        }

        // get selected account
        $eticket_id = (int) Input::get('et');

        if ($eticket_id>0) {
            // check if privileged
            if (!in_array($eticket_id, $all_et_ids, false)) {
                $user = Auth::user();
                return View::make('site/user/index', compact('user'))
                    ->with('error', 'Nav piekļuves tiesību!');
            }

            // get selected search fields
            $datums = trim(Input::get('datums'));

            // get object data
            $eticket = Eticket::find($eticket_id);
            $username = $eticket->nr;

            // get thumbnail
            $thumb = Eticket::leftJoin('images', 'etickets.thumb_id', '=', 'images.id')
                ->leftjoin('images AS u2', 'images.id', '=', 'u2.parent_id')
                ->select(array('u2.path'))
                ->where('etickets.id', $eticket_id)
                ->get();

            if ($thumb[0]->path!='') {
                $thumbnail = '<img width="100" src="/'.str_replace("public/", "", $thumb[0]->path).'" />';
            } else {
                $thumbnail = '<img width="100" src="/assets/img/no_avatar.jpg" />';
            }

            // get data for selected account
            $d = Entrance::with('Eticket')->where('pk', $eticket->pk);

            if ($datums!='') {
                $d->where('datums', '=', $datums);
            }

            $data_totals = $d->get();

            $data = $d->paginate(50);

            $totals = $d->count();

            // Title
            $title = 'Caurlaižu atskaite - '.$username;
        } else {
            $eticket_id = 0;
            $thumbnail = '';

            $data = array();

            // Title
            $title = 'Caurlaižu atskaite';
        }

        // Show the page
        $formview = View::make('reports.partials.datsubmit', compact('datums'));
        return View::make(
            'reports/'.$this->links.'/index',
            compact('title', 'data', 'eticket_id', 'username', 'all_et', 'eticket', 'datums', 'totals', 'thumbnail')
        )->with('datesubmit', $formview);
    }
}