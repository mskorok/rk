<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Entrance;
use App\Models\Objekts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class EntranceController extends BaseController {

    protected $links = 'entrance';

    public function getEntrance()
    {
        $admin_user = $this->admin_user();

        if ($admin_user==0)
        {
            // get all object owners for user
            $all_object_owners = $this->get_user_object_owners();

            // get all available objects
            $all_objects = $this->get_owner_objects($all_object_owners);
        }
        else {
            // get all objects
            $all_objects = $this->get_all_objects();
        }

        // get selected account
        $account = intval(Input::get('account'));

        if ($account>0)
        {
            $account_id = $account;

            if ($admin_user==0)
            {
                // check if privileged
                if (!in_array($account_id, $all_objects))
                {
                    $user = Auth::user();
                    return View::make('site/user/index', compact('user'))->with('error','Nav piekļuves tiesību!');
                }
            }

            // get selected search fields
            $datums = trim(Input::get('datums'));

            // get object data
            $object = Objekts::find($account_id);
            $username = $object->object_name;

            // get data for selected account
            $d = Entrance::where('object_id',$account);

            if ($datums!='')
            {
                $d->where('datums','=' ,$datums);
            }
                
            $data_totals = $d->get();

            $data = $d->paginate(50);

            $totals = $d->count();

            // Title
            $title = 'Objekta lietotāju pārvietošanās atskaite - '.$username;
        }
        else {
            $account_id = 0;

            $data = array();
            $all_et = 0;

            $totals = 0;

            // Title
            $title = 'Objekta lietotāju pārvietošanās atskaite';
        }

        if (count($all_objects)>0)
        {
           $all_acc = Objekts::whereIn('id',$all_objects)->get();
        }
        else $all_acc = array();

        // Show the page
        $formview = View::make('reports.partials.datsubmit',compact('datums'));
        return View::make('reports/'.$this->links.'/index', compact('title','data','account_id','username','all_acc','eticket','datums','totals','eticket_data'))
            ->with('datesubmit',$formview);
    }

}