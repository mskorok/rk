<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\MoneyBalances;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;

class MoneyRepController extends BaseController
{
    protected $links = 'money';

    public function index()
    {
        // get selected search fields
        $no = trim(Input::get('no'));
        $lidz = trim(Input::get('lidz'));

        if ($no!='' && $lidz!='')
        {
            $no2 = date( 'Y-m-d', strtotime($no));
            $lidz2 = date( 'Y-m-d', strtotime($lidz));

            // get data
            $d = MoneyBalances::where("datums",'>=',$no2)
                ->where("datums",'<=',$lidz2);
            $d->orderBy('datums','ASC');
            $data = $d->get();

            $totals = [
                "first" => 0,
                "last" => 0,
                "in" => 0,
                "out" => 0,
            ];

            $sk = 0;
            foreach ($data as $d)
            {
                if ($sk == 0) $totals["first"] = $d->start_balance;
                $totals["last"] = $d->end_balance;
                $totals["in"]+= $d->incoming;
                $totals["out"]+= $d->outgoing;
                $sk ++;
            }

        }
        else {
            $data = [];
            $no2 = '';
            $lidz2 = '';
            //Session::flash('error', 'Nav aizpildīti visi lauki!');
        }

        // Title
        $title = 'Atskaite par elektronisko naudu sistēmā';

        // Show the page
        $formview = View::make('reports.partials.datesubmit',compact('no','lidz'));
        return View::make('reports/'.$this->links.'/index', compact('title','data', 'totals', 'no2', 'lidz2'))
            ->with('dateintervalssubmit',$formview);
    }

    public function xls($no, $lidz)
    {
        if ($no!='' && $lidz!='')
        {
            $no2 = date( 'Y-m-d', strtotime($no));
            $lidz2 = date( 'Y-m-d', strtotime($lidz));

            // get data
            $d = MoneyBalances::select('datums', 'start_balance', 'incoming', 'outgoing', 'end_balance')->where("datums",'>=',$no2)
                ->where("datums",'<=',$lidz2);
            $d->orderBy('datums','ASC');
            $data = $d->get();

            $totals = [
                "first" => 0,
                "last" => 0,
                "in" => 0,
                "out" => 0,
            ];

            $sk = 0;
            foreach ($data as $d)
            {
                if ($sk == 0) $totals["first"] = $d->start_balance;
                $totals["last"] = $d->end_balance;
                $totals["in"]+= $d->incoming;
                $totals["out"]+= $d->outgoing;
                $sk ++;
            }

            // render and push XLS
            Excel::create('Atskaite par elektronisko naudu sistēmā '.$no.' - '.$lidz, function($excel) use ($data, $totals, $no, $lidz)
            {
                $excel->setTitle('Atskaite par elektronisko naudu sistēmā '.$no.' - '.$lidz);
                $excel->setCreator('skolas.rigaskarte.lv')->setCompany('skolas.rigaskarte.lv');
                $excel->setDescription('Atskaite par elektronisko naudu sistēmā '.$no.' - '.$lidz);

                $excel->sheet('Atskaite', function($sheet) use($data, $totals, $no, $lidz)
                {
                    $sheet->setOrientation('landscape');
                    $sheet->getPageSetup()->setFitToPage(false);
                    $sheet->getDefaultRowDimension()->setRowHeight(-1);
                    $sheet->setWidth(array(
                        'A'     =>  11,
                        'B'     =>  35,
                        'C'     =>  16,
                        'D'     =>  16,
                        'E'     =>  34,
                    ));
                    $sheet->setHeight(0, 20);

                    $sheet->fromArray($data, null, 'A1', true, false);

                    $sheet->prependRow(['Datums', 'Apgrozībā esoša E-nauda dienas sākumā', 'Emitēta E-nauda', 'Iztērēta E-nauda', 'Apgrozībā esoša E-nauda dienas beigās']);

                    $sheet->freezeFirstRow();

                    $sheet->appendRow(['KOPĀ', $totals["first"], $totals["in"], $totals["out"], $totals["last"]]);

                    $sheet->cells('A1:Z1', function($cells) {
                        $cells->setFontWeight('bold')->setBackground('#CCCCCC');
                    });

                    $sheet->setColumnFormat(array(
                        'B2:E3999' => '0.00'
                    ));

                    $lastrow = count($data) + 2;

                    $sheet->cells("A$lastrow:Z$lastrow", function($cells) {
                        $cells->setFontWeight('bold');
                    });
                });

            })->download('xlsx');
        }
    }

}