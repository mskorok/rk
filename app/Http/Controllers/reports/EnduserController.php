<?php
namespace App\Http\Controllers\reports;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\Eticket;
use App\Models\Owner;
use App\Models\OwnerAccess;
use App\Models\Transaction;
use DateTime;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class EnduserController extends BaseController
{

    protected $links = 'account';
    protected $links2 = 'eticket';

    public function getAccount()
    {
        $user_id = Auth::user()->id;

        $admin_user = $this->admin_user();

        if ($admin_user == 0) {
            // get all owners for user
            $all_owners = array();
            $owners = OwnerAccess::with('owner')->ao()->u($user_id)->get();
            foreach ($owners as $o) {
                array_push($all_owners, $o->owner->id);
            }

            // get granted ownerships for user
            $granted = Auth::user()->grantedOwnerships(2);
            foreach ($granted as $o) {
                array_push($all_owners, $o->owner->id);
            }

            // get all user accounts (type=1) for owners
            $all_accounts = Account::whereIn("owner_id", $all_owners)->u()->lists("id")->all();
        } else {
            // get all users
            $all_accounts = $this->get_all_users();
        }

        // get selected account
        $account = Input::get('account');

        if ($admin_user==0) {
            $account = intval($account);
            if ($account > 0) {
                $account_id = $account;
            } else {
                $account_id = reset($all_accounts);
            }
        } else {
            if (trim($account) != '') {
                $owner = Owner::u()->where('owner_name', trim($account))->first();
                if (! isset($owner->id)) {
                    return Redirect::back()
                        ->withInput()
                        ->with('error', 'Šāds lietotājs nav atrasts');
                }
                $account_id = Account::O($owner->id)->value('id');
            } else {
                $account_id = reset($all_accounts);
            }
        }

        // check if privileged
        if (!in_array($account_id, $all_accounts, false)) {
            $user = Auth::user();
            return View::make('site/user/index', compact('user'))->with('error', 'Nav piekļuves tiesību!');
        }

        // account owner data
        $account_owner_id = Account::find($account_id)->owner_id;
        if ($account_owner_id > 0) {
            $username = Owner::find($account_owner_id)->owner_name;
        } else {
            $user = Auth::user();
            return View::make('site/user/index', compact('user'))->with('error', 'Nav atrasti īpašnieka dati!');
        }

        // get selected search fields
        $no = trim(Input::get('no'));
        $lidz = trim(Input::get('lidz'));

        // get data for selected account
        $d = Transaction::with(
            ['from_account', 'from_account.owner', 'to_account', 'to_account.owner', 'fromEt', 'toEt']
        );
        $d->Ok();

        if (($no != '') || ($lidz != '')) {
            if ($no == '') {
                $no2 = date('Y-m-d', strtotime('2000-01-01'));
            } else {
                $no2 = date('Y-m-d', strtotime($no));
            }

            if ($lidz == '') {
                $lidz2 = date('Y-m-d', strtotime("tomorrow"));
            } else {
                $lidz2 = new DateTime($lidz);
                $lidz2->setTime(23, 59, 59);
                $lidz2 = $lidz2->format('Y-m-d H:i:s');
            }

            $d->where('created_at', '>=', $no2);
            $d->where('created_at', '<=', $lidz2);
        }

        $d->where(function ($query) use ($account_id) {
            $query->where('from_account_id', $account_id)
            ->orWhere('to_account_id', $account_id);
        });

        $d->orderBy('id', 'desc');

        $data_totals = $d->get();

        $data = $d->paginate(50);
       
        // calculate totals
        $totals["in"] = 0;
        $totals["out"] = 0;
        foreach ($data_totals as $key => $single) {
            if ($single->from_account_id==$account_id) {
                $totals["out"] = $totals["out"] + $single->amount;
            } elseif ($single->to_account_id == $account_id) {
                $totals["in"] = $totals["in"] + $single->amount;
            }
        }

        if ($admin_user == 0) {
            $all_acc = Account::with('Owner')->whereIn('id', $all_accounts)->get();
        } else {
            $all_acc = array();
        }

        // Title
        $title = 'Konta vēsture - '.$username;

        // Show the page
        $formview = View::make('reports.partials.datesubmit', compact('no', 'lidz'));
        return View::make(
            'reports/'.$this->links.'/index',
            compact('title', 'data', 'account_id', 'username', 'all_acc', 'no', 'lidz', 'totals', 'admin_user')
        )->with('dateintervalssubmit', $formview);
    }

    public function getEticket()
    {
        $user = Auth::user();

        $all_et = $this->get_users_active_etickets();

        // get selected account
        $et = intval(Input::get('et'));

        if ($et > 0) {
            $eticket_id = $et;

            $eticket = Eticket::find($eticket_id);
            if (! $eticket_id) {
                return View::make('site/user/index', compact('user'))->with('error', 'E-talons nav atrasts!');
            }

            // check if privileged
            if (! in_array($eticket->account_id, array_unique($all_et->pluck("account_id")->all()), false)) {
                return View::make('site/user/index', compact('user'))->with('error', 'Nav piekļuves tiesību!');
            }

            // get selected search fields
            $no = trim(Input::get('no'));
            $lidz = trim(Input::get('lidz'));

            $account_id = $eticket->account_id;

            // get data for selected account
            $d = Transaction::with(
                ['from_account', 'from_account.owner', 'to_account', 'to_account.owner', 'fromEt', 'toEt']
            );
            $d->Ok();

            if (($no!='') || ($lidz!='')) {
                if ($no=='') {
                    $no2 = date('Y-m-d', strtotime('2000-01-01'));
                } else {
                    $no2 = date('Y-m-d', strtotime($no));
                }

                if ($lidz=='') {
                    $lidz2 = date('Y-m-d', strtotime("tomorrow"));
                } else {
                    $lidz2 = new DateTime($lidz);
                    $lidz2->setTime(23, 59, 59);
                    $lidz2 = $lidz2->format('Y-m-d H:i:s');
                }

                $d->where('created_at', '>=', $no2);
                $d->where('created_at', '<=', $lidz2);
            }

            $d->where(function ($query) use ($account_id) {
                $query->where('from_account_id', $account_id)
                ->orWhere('to_account_id', $account_id);
            });
            $d->orderBy('id', 'desc');

            $data_totals = $d->get();

            $data = $d->paginate(50);

            // calculate totals
            $totals["in"] = 0;
            $totals["out"] = 0;
            foreach ($data_totals as $key => $single) {
                if ($single->from_account_id == $account_id) {
                    $totals["out"] = $totals["out"] + $single->amount;
                } elseif ($single->to_account_id == $account_id) {
                    $totals["in"] = $totals["in"] + $single->amount;
                }
            }

            // Title
            $title = 'E-talons: '.$eticket->nr. ' ('.$eticket->name.') vēsture';
        } else {
            $eticket_id = 0;

            $data = array();

            $totals = 0;

            // Title
            $title = 'E-talona vēsture';
        }

        if ($all_et->count() == 0) {
            $all_et = [];
        }

        $all_mpos = $this->get_all_mpos();

        // Show the page
        $formview = View::make('reports.partials.datesubmit', compact('no', 'lidz'));
        return View::make(
            'reports/'.$this->links2.'/index',
            compact('title', 'data', 'eticket_id', 'all_et', 'no', 'lidz', 'totals', 'all_mpos')
        )->with('dateintervalssubmit', $formview);
    }

    /**
     * Get JSON data
     *
     * @param $q
     * @return Response
     */
    public function getFetch($q)
    {
        $obj = Session::get('report_object', 0);

        if ($obj==0) {
            $roleids = Auth::user()->currentRoleIds();

            if (array_search(1, $roleids)=== false) {
                exit();
            }

            $r = Eticket::where('nr', 'LIKE', '%' . $q . '%')
                ->select('nr', 'name')->take(10)->get()->toArray();

            $return = array();
            foreach ($r as $rr) {
                array_push($return, $rr["nr"].', '.$rr["name"]);
            }
        } else {
            // get all e-tickets for this object
            $r = DB::select("SELECT nr, name FROM etickets WHERE nr LIKE ? 
                AND id IN (
                    SELECT eticket_id FROM eticket_objects WHERE object_id = ?
                ) LIMIT 0,10", array('%'.$q.'%',$obj));

            $return = array();
            foreach ($r as $rr) {
                array_push($return, $rr->nr.', '.$rr->name);
            }
        }

        return response()->json($return);
    }

    /**
     * Get JSON data
     *
     * @param $q
     * @return Response
     */
    public function getFetch2($q)
    {
        $typeExists = Owner::typeExists();
        if (! $typeExists) {
            $owners =  Owner::u()->get();
            $owners->addToIndex();
        }

        // get all user accounts
        if (app()->environment() === 'testing') {
            $users = Owner::where('owner_name', 'LIKE', '%' . $q . '%')->U()->lists('owner_name')->toArray();
        } else {
            $users = Owner::searchByQuery(['query_string' => ['query' => '*'. $q . '*']], null, ['owner_name'], 10)
                ->pluck("owner_name")->toArray();
        }

        return response()->json($users);
    }

}
