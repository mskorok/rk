<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\PortalResetPasswords;
//use Illuminate\Foundation\Auth\ResetsPasswords;
//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Password;
//use Illuminate\View\View;
//use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Mail\Mailer;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use PortalResetPasswords;


    /**
     * @var string
     */
    protected $loginView;

    /**
     * @var string
     */
    protected $registerView;

    /**
     * @var string
     */
    protected $redirectPath;

    /**
     * @var string
     */
    protected $loginPath;

    /**
     * @var string
     */
    protected $resetView;

    /**
     * @var string
     */
    protected $redirectAfterLogout;

    /**
     * @var string
     */
    protected $redirectTo;

    /**
     * @var string|\Symfony\Component\Translation\TranslatorInterface
     */
    protected $subject;

    /**
     * The mailer instance.
     *
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    protected $mailer;

    /**
     * Create a new password controller instance.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->loginView = config('auth.views.web.login_view');
        $this->registerView = config('auth.views.web.register_view');
        $this->redirectPath = config('auth.views.web.redirect_to');
        $this->redirectTo = config('auth.views.web.redirect_to');
        $this->redirectAfterLogout = config('auth.views.web.login_path');
        $this->loginPath = config('auth.views.web.login_path');
        $this->resetView = config('auth.views.web.reset_view');
        $this->subject = trans('passwords.reset_subject');
        $this->mailer = $mailer;
        $this->middleware('guest');
    }

//    public function resetAjax(Request $request)
//    {
//        if ($this->isAjax()) {
//            $this->validateSendResetLinkEmail($request);
//
//            $broker = $this->getBroker();
//
//            $response = Password::broker($broker)->sendResetLink(
//                $this->getSendResetLinkEmailCredentials($request),
//                $this->resetEmailBuilder()
//            );
//
//            switch ($response) {
//                case Password::RESET_LINK_SENT:
//                    return response()->json(['success' => $response]);
//                case Password::INVALID_USER:
//                default:
//                    return response()->json(['error' => $response]);
//            }
//        } else {
//            return redirect()->route('landing');
//        }
//    }
//
//
//    /**
//     * Reset the given user's password.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return \Illuminate\Http\Response | Response
//     */
//    public function reset(Request $request)
//    {
//        $this->validate(
//            $request,
//            $this->getResetValidationRules(),
//            $this->getResetValidationMessages(),
//            $this->getResetValidationCustomAttributes()
//        );
//
//        $credentials = $this->getResetCredentials($request);
//
//        $broker = $this->getBroker();
//
//        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
//            $this->resetPassword($user, $password);
//        });
//
//        switch ($response) {
//            case Password::PASSWORD_RESET:
//                $this->sendPasswordChangeNotification($credentials);
//                return $this->getResetSuccessResponse($response);
//            default:
//                return $this->getResetFailureResponse($request, $response);
//        }
//    }
//
//    /**
//     * Display the form to request a password reset link.
//     *
//     * @return \Illuminate\Http\Response | View
//     */
//    public function showLinkRequestForm()
//    {
//        if (\Auth::check()) {
//            return redirect()->route('landing');
//        }
//        if (property_exists($this, 'linkRequestView')) {
//            return view($this->linkRequestView);
//        }
//
//        if (view()->exists('auth.passwords.email')) {
//            return view('auth.passwords.email');
//        }
//
//        return view('auth.password');
//    }
//
//    /**
//     * Display the password reset view for the given token.
//     *
//     * If no token is present, display the link request form.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  string|null  $token
//     * @return \Illuminate\Http\Response | View
//     */
//    public function showResetForm(Request $request, $token = null)
//    {
//        if (\Auth::check()) {
//            return redirect()->route('landing');
//        }
//        if (is_null($token)) {
//            return $this->getEmail();
//        }
//
//        $email = $request->input('email');
//
//        if (property_exists($this, 'resetView')) {
//            return view($this->resetView)->with(compact('token', 'email'));
//        }
//
//        if (view()->exists('auth.passwords.reset')) {
//            return view('auth.passwords.reset')->with(compact('token', 'email'));
//        }
//
//        return view('auth.reset')->with(compact('token', 'email'));
//    }
//
//    protected function isAjax()
//    {
//        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])
//            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
//    }
//
//    private function sendPasswordChangeNotification($credentials)
//    {
//        $view = 'auth.passwords.notify';
//        if (isset($credentials['email'])) {
//            $email = $credentials['email'];
//            $this->mailer->send($view, compact('email'), function ($m) use ($email) {
//                $m->to($email);
//                $m->subject($this->getEmailSubject());
//            });
//        }
//    }
}
