<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Mail\Message;

class PortalAuthController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    use AuthenticatesAndRegistersUsers;

    /**
     * @var string
     */
    protected $loginView;

    /**
     * @var string
     */
    protected $registerView;

    /**
     * @var string
     */
    protected $redirectPath;

    /**
     * @var string
     */
    protected $loginPath;

    /**
     * @var string
     */
    protected $redirectAfterLogout;

    /**
     * @var string
     */
    protected $resetView;

    /**
     * @var string
     */
    protected $redirectTo;

    /**
     * The mailer instance.
     *
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    protected $mailer;

    /**
     * @var string|\Symfony\Component\Translation\TranslatorInterface
     */
    protected $subject;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     * @param Mailer $mailer
     * @throws \Exception
     */
    public function __construct(Guard $auth, Registrar $registrar, Mailer $mailer)
    {
        parent::__construct();

        $this->loginView = config('auth.views.web.login_view');
        $this->registerView = config('auth.views.web.register_view');
        $this->redirectPath = config('auth.views.web.redirect_to');
        $this->redirectTo = config('auth.views.web.redirect_to');
        $this->redirectAfterLogout = config('auth.views.web.login_path');
        $this->loginPath = config('auth.views.web.login_path');
        $this->resetView = config('auth.views.web.reset_view');
        $this->subject = trans('passwords.reset_subject');
        $this->mailer = $mailer;
        $this->auth = $auth;
        $this->registrar = $registrar;
        $this->middleware(
            'guest',
            [
                'except' => [
                    'getLogout'
                ]
            ]
        );
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $this->validateLogin($request);
        } catch (\Exception $e) {
            return redirect()->route('landing');
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }



    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response | RedirectResponse | View
     * @throws \Illuminate\Foundation\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        try {
            if ($validator->fails()) {
                $this->throwValidationException(
                    $request,
                    $validator
                );
            }
        } catch (\Exception $exception) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $user = $this->create($request->all());

        $this->sendConfirmRegistrationLink($user);

        return redirect()->route('success', ['email' => $user->email]);
    }

    /**
     * @param Request $request
     * @param $token
     * @return RedirectResponse | View
     */
    public function registerConfirm(Request $request, $token)
    {
        if (\Auth::check()) {
            return redirect()->route('landing');
        }
        $email = $request->get('email');
        $user = User::where('email', $email)->first();
        if (!$user) {
            return redirect()->route('logout');// TODO
        }
        if ($token === $user->confirmation_code && empty($user->confirmed)) {
            $user->confirmed = true;
            $user->api_token = str_random(60);
            $user->save();
            \Auth::guard($this->getGuard())->login($user);
            return redirect()->route('thanks');
        } else {
            return redirect()->to($this->redirectPath());
        }
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        $redirect = config('auth.views.web.redirect_to');
        return url($redirect);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'email' => 'required|string|email|confirmed|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User | Model
     */
    protected function create(array $data)
    {
        return User::create([
//            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => str_random(20)
        ]);
    }

    /**
     * @param User $user
     */
    protected function sendConfirmRegistrationLink(User $user)
    {
        $email = $user->email;
        $view = 'auth.passwords.confirm';
        if ($email) {
            $this->mailer->send($view, compact('email', 'user'), function ($m) use ($email) {
                /** @var $m Message */
                $m->to($email);
                $m->subject($this->getEmailSubject());
            });
        }
    }

    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return array_add($request->only($this->loginUsername(), 'password'), 'confirmed', 1);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response | RedirectResponse
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::guard($this->getGuard())->user());
        }

        return redirect()->to($this->redirectPath());
    }
}
