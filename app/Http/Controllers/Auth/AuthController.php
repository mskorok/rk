<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AuthController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    use AuthenticatesAndRegistersUsers;

    protected $redirectPath;// = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        parent::__construct();

        $this->auth = $auth;
        $this->registrar = $registrar;
        $this->redirectPath = config('auth.views.web.redirect_to');
        $this->middleware(
            'guest',
            [
                'except' => [
                    'getLogout','getUser','postUser','getForceterms','postForceterms','saveprofile','getDashboard'
                ]
            ]
        );
    }

    protected function getCredentials(Request $request)
    {
        return array_add($request->only($this->loginUsername(), 'password'), 'confirmed', 1);
    }

    /**
     *
     * Profile edit page
     *
     * @return mixed
     */
    public function getUser()
    {
        $user = Auth::user();

        return View::make('site.user.index', compact('user'));
    }

    public function postUser(UpdateUserRequest $request)
    {
        if (!Auth::check()) {
            Redirect::to('auth\login');
        }

        $credentials = ['email' => Auth::user()->email, 'password' => Input::get("cpassword")];
        if (!Auth::validate($credentials)) {
            return Redirect::back()
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', 'Nepareiza pašreizējā parole!');
        }

        $password = Input::get('password');
        $passwordConfirmation = Input::get('password_confirmation');

        if ($password != $passwordConfirmation) {
            return Redirect::back()
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', 'Nesakrīt ievadītās paroles!');
        }

        $user = Auth::user();
        $user->username = Input::get("username");
        $user->surname = Input::get("surname");
        $user->email = Input::get('email');
        $user->phone = Input::get("phone");

        if (trim(strip_tags(Input::get("password")))!='') {
            $user->password = bcrypt(Input::get("password"));
        }

        if (!$user->save()) {
            return Redirect::back()
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', 'Kļuda saglabājot datus!');
        }

        return Redirect::back()
            ->with('success', Lang::get('user/user.user_account_updated'));
    }

    public function getDashboard()
    {
        return View::make('auth/dashboard');
    }
}
