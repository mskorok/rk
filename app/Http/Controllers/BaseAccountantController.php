<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Eticket;
use App\Models\OwnerAccess;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class BaseAccountantController extends BaseController {

    protected function confirm_payment($payment,$summa,$back_link)
    {
        // transaction data
        $user = User::findOrFail($payment->user_id);
        $email = $user->email;
        $eticket = Eticket::findOrFail($payment->eticket_id);

        // user account
        $owner = OwnerAccess::Ao()->U($user->id)->first()->owner;
        $account = AccountAccess::Ao()->U($user->id)->first()->account;

        // eticket account
        $et_acc = Account::find($eticket->account_id);

        if ($account->owner_id!=$owner->id)
        {
            return Redirect::to($back_link)
                ->withInput()
                ->withErrors('Cant find account');
        }

        // complete manual payment
        $payment->confirmed  = 1;
        $payment->confirmed_date  = date("Y-m-d H:i:s");
        $payment->accountant_id  = Auth::user()->id;
        $payment->amount  = intval($summa*100);

        if ($payment->save())
        {
            // transactions
            try
            {
                DB::transaction(function() use($eticket,$account,$et_acc,$payment,$email)
                {
                    $sum = $payment->amount;

                    // transaction to user profile
                    $t = New Transaction;

                    $t->amount = $sum;
                    $t->operation_id = 1;
                    $t->from_account_id = 0;
                    $t->to_account_id = $account->id;
                    $t->confirmed = 1;
                    $t->save();

                    // transaction from profile to eticket
                    $eticket->increment('balance', $sum);
                    $et_acc->increment('balance', $sum);

                    $eticket->save();
                    $account->save();
                    $et_acc->save();

                    // create transaction
                    $t = New Transaction;

                    $t->amount              = $sum;
                    $t->operation_id        = 1;
                    $t->from_account_id = $account->id;     // Owner account
                    $t->to_account_id   = $et_acc->id;      // E-ticket group account
                    $t->to_eticket_nr   = $eticket->nr;
                    $t->confirmed           = 1;
                    $t->save();

                    $payment->transaction_id = $t->id;
                    $payment->save();

                    // create ticket
                    $t = New Ticket;
                    $t->user_id = $payment->user_id;
                    $t->ticket_type = 3; // naudas parskaitijums
                    $t->ticket_status = 1;
                    $t->eticket_nr = $eticket->nr;

                    if ($payment->user_amount!=$payment->amount)
                    {
                        $t->content = 'Pārskaitījums nr. '.$payment->id.' ir saņemts un apstiprināts, bet tam ir mainīta summa no '.($payment->user_amount/100).' EUR uz <b>'.($payment->amount/100).' EUR</b>.';
                    }
                    else {
                        $t->content = 'Pārskaitījums nr. '.$payment->id.' par summu '.($payment->amount/100).' EUR ir saņemts un apstiprināts.';
                    }

                    $t->save();

                    // send email
                    if (App::environment() == 'production')
                    {
                        $data["mess"] = $t->content;
                        Mail::send('mail.payment', $data, function($message) use ($email) {
                            $message->to($email)->subject('skolas.rigaskarte.lv - apstiprināts pārskaitījums');
                        });
                    }

                });
            }
            catch (\Exception $e)
            {
                $payment->confirmed  = 0;
                $payment->confirmed_date  = '';
                $payment->accountant_id  = '';
                $payment->amount  = 0;
                $payment->save();

                Log::error('Error, manual transfer confirmation - '.$e->getMessage());

                return Redirect::to($back_link."/".$payment->id)->with('error', 'Neizdevās saglabāt datu bāzē!');
            }

            Cache::forget('user_balance_'.$user->id);

            return Redirect::to($back_link)->with('success', 'Maksājums apstiprināts.');
        }
        else {
            return Redirect::to($back_link)->with('error', 'Neizdevās apstiprināt!');
        }
    }

}