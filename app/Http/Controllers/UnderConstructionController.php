<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;

class UnderConstructionController extends BaseController
{

    protected $links = 'under-construction';

    /**
     * Show the form
     * @return View
     */
    public function getIndex()
    {
        $locale = \Lang::getLocale();
        if ($locale === 'ru') {
            return view('under-construction.index-ru');
        } elseif ($locale === 'lv') {
            return view('under-construction.index-lv');
        }
        // Title
        $title_1 = 'Sakara ar sistēmas veiktajiem uzlabojumiem un pārēju uz jauno portāla versiju lūdzam atgriezties';
        $title_2 = '01-09-2017';
        // Show the page
        return view($this->links.'/index', compact('title_1', 'title_2'));
    }
}
