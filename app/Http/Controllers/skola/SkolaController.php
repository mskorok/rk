<?php
namespace App\Http\Controllers\skola;

use App\Http\Controllers\BaseController;
use App\Models\Account;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\EticketsObjekti;
use App\Models\OwnerAccess;
use App\Models\Ticket;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class SkolaController extends BaseController {

    protected $links = 'skola';

    public function getIndex()
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        $data = Eticket::where('eticket_status',5)->whereHas('objekti', function($q) use ($all_objects)
        {
            $q->whereIn('object_id', $all_objects);
        })->get();

        return View::make($this->links.'/index', compact('data'));
    }

    public function getCancel($objekts)
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        // get eticket object
        $primary_object_id = intval($objekts->pirmais_objekts()->id);
        if ($primary_object_id<=0)
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts objekts!');
        else if (!in_array($primary_object_id,$all_objects))
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts!');

        // Title
        $title = 'Atcelt e-talona pieprasījumu - '.$objekts->nr. ' ?';

        // Show the page
        return View::make($this->links.'/cancel', compact('title','objekts'));
    }

    public function postCancel($objekts)
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        // get eticket object
        $primary_object_id = intval($objekts->pirmais_objekts()->id);
        if ($primary_object_id<=0)
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts objekts!');
        else if (!in_array($primary_object_id,$all_objects))
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts!');

        try {
            DB::transaction(function() use($objekts)
            {
                $eticket_user_id = OwnerAccess::Ao()->O(Account::find($objekts->account_id)->owner_id)->value('user_id');

                if (!Account::find($objekts->account_id)->delete()) throw new Exception("Neizdevās dzēst kontu!",4);
                if (!EticketsObjekti::Et($objekts->id)->delete()) throw new Exception("Neizdevās dzēst objektu!",3);
                if (!DB::table('eticket_opers')->where('eticket_id',$objekts->id)->delete()) throw new Exception("Neizdevās dzēst operāciju!",2);

                // create ticket
                $t = New Ticket;
                $t->user_id = $eticket_user_id;
                $t->ticket_type = 5;
                $t->ticket_status = 1;
                $t->eticket_nr = $objekts->nr;
                $t->content = 'E-talona '.$objekts->nr.' pieprasījums ir noraidīts!';
                if (!$t->save()) throw new Exception("Neizdevās izveidot paziņojumu!!",5);

                if (!$objekts->delete()) throw new Exception("Neizdevās dzēst objektu!",1);
            });
        }
        catch (Exception $e)
        {
           return Redirect::to($this->links.'/index/'.$objekts->id.'/cancel')
               ->with('error',$e->getMessage()); 
        }

        return Redirect::to($this->links.'/index/')
            ->with('success','E-talona pieprasījums atcelts.');
    }

    public function getConfirm($objekts)
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        // get eticket object
        $primary_object_id = intval($objekts->pirmais_objekts()->id);
        if ($primary_object_id<=0)
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts objekts!');
        else if (!in_array($primary_object_id,$all_objects))
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts!');

        // Title
        $title = 'Apstiprināt e-talonu - '.$objekts->nr;

        // Show the page
        return View::make($this->links.'/confirm', compact('title','objekts'));
    }

    public function postConfirm($objekts)
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        // get eticket object
        $primary_object_id = intval($objekts->pirmais_objekts()->id);
        if ($primary_object_id<=0)
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts objekts!');
        else if (!in_array($primary_object_id,$all_objects))
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts!');
        
    	$name = Input::get( 'name' );
    	$pk = Input::get( 'pk' );
    	$grade = Input::get( 'grade' );

    	$objekts->name = $name;
    	$objekts->pk = $pk;
    	$objekts->grade = $grade;
    	$objekts->eticket_status = 6; // rk confirmation
    	if (!$objekts->save())
    	{
    		return Redirect::to($this->links.'/index/'.$objekts->id.'/confirm')
    		    ->withInput()
    		    ->withErrors($objekts->getErrors());
    	}

    	return Redirect::to($this->links.'/index/')
    	    ->with('success','E-talons veiksmīgi apstiprināts.');
    }

    public function getIndexb()
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        $data = EticketsBase::where('custom',2)->whereIn('object_id', $all_objects)->get();

        return View::make($this->links.'/indexb', compact('data'));
    }

    public function getCancelb($objekts)
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        // get eticket object
        $primary_object_id = intval($objekts->object_id);
        if ($primary_object_id<=0)
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts!');
        else if (!in_array($primary_object_id,$all_objects))
            Redirect::to($this->links.'/index/')->with('error','Nav atrasts!');

        // Title
        $title = 'Atcelt e-talona bāzes ieraksta pieprasījumu - '.$objekts->etickets. ' ?';

        // Show the page
        return View::make($this->links.'/cancelb', compact('title','objekts'));
    }

    public function getConfirmb($objekts)
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        // get eticket object
        $primary_object_id = intval($objekts->object_id);
        if ($primary_object_id<=0)
            Redirect::to($this->links.'/bindex/')->with('error','Nav atrasts!');
        else if (!in_array($primary_object_id,$all_objects))
            Redirect::to($this->links.'/bindex/')->with('error','Nav atrasts!');

        // Title
        $title = 'Apstiprināt e-talona bāzes ierakstu - '.$objekts->etickets;

        // Show the page
        return View::make($this->links.'/confirmb', compact('title','objekts'));
    }

    public function postCancelb($objekts)
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        // get eticket object
        $primary_object_id = intval($objekts->object_id);
        if ($primary_object_id<=0)
            Redirect::to($this->links.'/bindex/')->with('error','Nav atrasts!');
        else if (!in_array($primary_object_id,$all_objects))
            Redirect::to($this->links.'/bindex/')->with('error','Nav atrasts!');

        try {
            DB::transaction(function() use($objekts)
            {
                // create ticket
                $t = New Ticket;
                $t->user_id = 0; // to aadmin
                $t->ticket_type = 5;
                $t->ticket_status = 1;
                $t->eticket_nr = $objekts->etickets;
                $t->content = 'E-talona bāzes ('.$objekts->pk.') pieprasījums ir noraidīts!';
                if (!$t->save()) throw new Exception("Neizdevās izveidot paziņojumu!!",5);

                if (!$objekts->delete()) throw new Exception("Neizdevās dzēst pieprasījumu!",1);
            });
        }
        catch (Exception $e)
        {
           return Redirect::to($this->links.'/index/'.$objekts->id.'/cancelb')
               ->with('error',$e->getMessage()); 
        }

        return Redirect::to($this->links.'/bindex/')
            ->with('success','E-talona bāzes pieprasījums atcelts.');
    }

    public function postConfirmb($objekts)
    {
        // get all object owners for user
        $all_object_owners = $this->get_user_object_owners();

        // get all objects
        $all_objects = $this->get_owner_objects($all_object_owners);

        // get eticket object
        $primary_object_id = intval($objekts->object_id);
        if ($primary_object_id<=0)
            Redirect::to($this->links.'/bindex/')->with('error','Nav atrasts!');
        else if (!in_array($primary_object_id,$all_objects))
            Redirect::to($this->links.'/bindex/')->with('error','Nav atrasts!');
        
        $name = Input::get( 'name' );
        $surname = Input::get( 'surname' );
        $pk = Input::get( 'pk' );
        $etickets = Input::get( 'etickets' );
        $grade = Input::get( 'grade' );

        $objekts->name = $name;
        $objekts->surname = $surname;
        $objekts->pk = $pk;
        $objekts->grade = $grade;
        $objekts->custom = 3; // rk base confirmation
        if (!$objekts->save())
        {
            return Redirect::to($this->links.'/index/'.$objekts->id.'/confirmb')
                ->withInput()
                ->withErrors($objekts->getErrors());
        }

        return Redirect::to($this->links.'/bindex/')
            ->with('success','E-talona pieprasījums veiksmīgi apstiprināts.');
    }

}