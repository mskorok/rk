<?php
namespace App\Http\Controllers;

use App\Models\Settings;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class DokumentsController extends BaseController {

    protected $links = 'dokuments';

    /**
     * Show the form
     *
     * @return Response
     */
    public function getIndex()
    {
        // check for user status
        $u = Auth::user();
        if ($u->terms != 0 && $u->terms != 3)
            return Redirect::to("/")->with('error', 'Nav atrasts!');

        // Title
        $title = 'Pievienot personu apliecinošu dokumentu';
        $action = '/dokuments';
        $dokuments_text = Settings::first()->dokuments_text;

        // Show the page
        return View::make($this->links.'/index', compact('title','action','dokuments_text'));
    }

    public function postIndex()
    {
        // check for user status
        $u = Auth::user();
        if ($u->terms != 0 && $u->terms != 3)
            return Redirect::to("/")->with('error', 'Nav atrasts!');

        $input = array('file' => Input::file('file'));
        $rules['file'] = 'mimes:jpg,png,gif,jpeg,pdf|required|max:2000';

        $validator = Validator::make($input, $rules);

        // Check to see if validation fails or passes
        if ($validator->fails())
            return Redirect::back()->with('error', 'Nekorekts dokuments!');

        $file            = Input::file('file');
        $destinationPath = storage_path().'/documents/';
        $extension = $file->getClientOriginalExtension();
        $filename        = $u->id.'.'.$extension;
        $uploadSuccess   = $file->move($destinationPath, $filename);

        if ($uploadSuccess)
        {
            $user = User::findOrFail($u->id);
            $user->dokuments = $filename;
            if ( ! $user->save())
            {
                Log::error("dokuments upload, cant save row - ".json_encode($user->getErrors()));
                return Redirect::back()->with('error', 'Kļūda saglabājot datus!');
            }
        }
        else return Redirect::back()->with('error', 'Kļūda saglabājot faila datus!');

        // send email to user
        if (App::environment() == 'production')
        {
            $objekts = $u;
            Mail::send('mail.dokuments', ["user" => $objekts], function($message) use ($objekts) {
                $message->to($objekts->email, $objekts->username.' '.$objekts->surname)->bcc(Config::get('mail.rsemail1'))->subject('Skolas.rigaskarte.lv - dokumenta pārbaude.');
            });
        }
        $redirect = config('auth.views.web.redirect_to');

        return Redirect::to($redirect)  // todo REDIRECT
//        return Redirect::to('/')
            ->with('success', 'Dokuments veiksmīgi pievienots.<br />
Ir uzsākta personas datu pārbaude, šobrīd portāls Jums ir pieejams ar limitētu funkcionalitāti.<br />
Pēc personas datu pārbaudes Jūs informēs pa norādīto e-pasta adresi.');

    }
}