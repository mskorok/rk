<?php

namespace App\Http\Controllers\accountant;

use App\Http\Controllers\BaseAccountantController;
use App\Models\ManualPayment;
use App\Models\Settings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use SimpleXMLElement;

class AccountantImportController extends BaseAccountantController {

    protected $links = 'mimport';

    public function getIndex()
    {
        $title = 'Ienākošo maksājumi datu imports (Fidavista)';

        // Show the page
        return View::make('accountant/import', compact('title'));
    }

    public function getCsv()
    {
        $title = 'Ienākošo maksājumi datu imports (CSV)';

        // Show the page
        return View::make('accountant/importcsv', compact('title'));
    }

    public function postIndex()
    {
        if ( ! Input::hasFile('file'))
        {
            return Redirect::back()->with('error', 'Fails nav pievienots!');
        }

        $file = Input::file('file');

        $extension = strtolower($file->getClientOriginalExtension());
        $mime      = $file->getMimeType();
        $path      = $file->getRealPath();

        if (($extension!='xml') OR ($mime!='application/xml'))
        {
            return Redirect::back()->with('error', 'Nekorekta formāta fails!');
        }

        if (file_exists($path))
        {
            $file_contents = file_get_contents($path);

            // parse XML file
            $data = new SimpleXMLElement($file_contents);

            $settings = Settings::first();

            if ($settings->konts != $data->Statement->AccountSet->AccNo)
                return Redirect::back()->with('error', 'Kļūda! Nesakrīt sistēmas un saņēmēja konta numurs failā.');

            if ($settings->reg_nr != $data->Statement->ClientSet->LegalId)
                return Redirect::back()->with('error', 'Kļūda! Nesakrīt sistēmas un saņēmēja reģistrācijas numurs failā.');

            if (trim($data->Statement->AccountSet->CcyStmt->Ccy) != 'EUR' )
                return Redirect::back()->with('error', 'Kļūda! Faila valūta nav EUR.');

            // counters for payment records
            $created = 0;
            $existing = 0;
            $errors = 0;

            $payment_data = $data->Statement->AccountSet->CcyStmt->TrxSet;
            $total = count($payment_data);

            try
            {
                $ret = DB::transaction(function() use($payment_data, $errors, $existing, $created)
                {
                    foreach ($payment_data as $row)
                    {
                        if ($row->TypeCode != 'INP')
                        {
                            $errors++;
                            continue;
                        }

                        $amount = $row->AccAmt;
                        $comments_string = $row->PmtInfo;
                        $comments_array = explode(" ",$comments_string);

                        $found = 0;
                        $manual_payment = null;

                        foreach ($comments_array as $slug)
                        {
                            $slug = strtolower(trim(strip_tags($slug)));
                            if (strlen($slug) == 10) // payment code length
                            {
                                $manual_payment = ManualPayment::where("payment_code",$slug)->first();

                                if (isset($manual_payment->id))
                                {
                                    $found = 1;
                                }
                            }
                        }

                        if ($found == 1)
                        {
                            if ($manual_payment->confirmed == 1)
                            {
                                $existing++;
                            }
                            else {

                                $this->confirm_payment($manual_payment,$amount,'accountant/mimport');

                                $created++;
                            }
                        }
                        else $errors++;
                    }

                    return [$errors, $existing, $created];
                });

                $created = $ret[2];
                $existing = $ret[1];
                $errors = $ret[0];

            }
            catch (Exception $e)
            {
                return Redirect::back()->with('error', 'Kļūda ievietojot DB - '.$e->getMessage().'!');
            }

            return Redirect::back()->with('success', 'Imports veiksmīgs.
            <br />Ieraksti failā: <b>'.$total.'</b>
            <br />Ievietoti ieraksti: <b>'.$created.'</b>
            <br />Jau eksistējoši ieraksti: <b>'.$existing.'</b>
            <br />Sistēmā neatrasti ieraksti: <b>'.$errors.'</b>');

        }
        else {
            return Redirect::back()->with('error', 'Kļūda augšuplādējot failu!');
        }
    }

    public function postCsv()
    {
        if (!Input::hasFile('file'))
        {
            return Redirect::back()->with('error', 'Fails nav pievienots!');
        }

        $file = Input::file('file');

        $extension = strtolower($file->getClientOriginalExtension());
        $mime      = $file->getMimeType();
        $path      = $file->getRealPath();

        if (($extension!='csv') OR ($mime!='text/plain'))
        {
            return Redirect::back()->with('error', 'Nekorekta formāta fails!');
        }

        if (file_exists($path))
        {
            // parse CSV file to an array
            $data = [];
            if (($handle = fopen($path, "r")) !== FALSE)
            {
                while (($row = fgetcsv($handle, 1000, ";")) !== FALSE)
                {
                   $data[] = $row;
                }
                fclose($handle);
            }
            array_shift($data);

            // filter only incoming payments
            $data = array_filter($data, array($this, 'filterIncoming'));

            $settings = Settings::first();

            // check for correct accounts and currency
            foreach ($data as $row)
            {
                if ($settings->konts != $row[0])
                    return Redirect::back()->with('error', 'Kļūda! Nesakrīt sistēmas un saņēmēja konta numurs failā.');

                if (trim($row[10]) != 'EUR' )
                    return Redirect::back()->with('error', 'Kļūda! Faila valūta nav EUR.');
            }

            // counters for payment records
            $created = 0;
            $existing = 0;
            $errors = 0;

            $total = count($data);

            try
            {
                $ret = DB::transaction(function() use($data, $errors, $existing, $created)
                {
                    foreach ($data as $row)
                    {
                        $amount = floatval(str_replace(",",".",$row[7]));
                        $comments_string = $row[9];
                        $comments_array = explode(" ",$comments_string);

                        $found = 0;
                        $manual_payment = null;

                        foreach ($comments_array as $slug)
                        {
                            $slug = strtolower(trim(strip_tags($slug)));
                            if (strlen($slug) == 10) // payment code length
                            {
                                $manual_payment = ManualPayment::where("payment_code",$slug)->first();

                                if (isset($manual_payment->id))
                                {
                                    $found = 1;
                                }
                            }
                        }

                        if ($found == 1)
                        {
                            if ($manual_payment->confirmed == 1)
                            {
                                $existing++;
                            }
                            else {

                                $this->confirm_payment($manual_payment,$amount,'accountant/mimport2');

                                $created++;
                            }
                        }
                        else $errors++;
                    }

                    return [$errors, $existing, $created];
                });

                $created = $ret[2];
                $existing = $ret[1];
                $errors = $ret[0];

            }
            catch (Exception $e)
            {
                return Redirect::back()->with('error', 'Kļūda ievietojot DB - '.$e->getMessage().'!');
            }

            return Redirect::back()->with('success', 'Imports veiksmīgs.
            <br />Ieraksti failā: <b>'.$total.'</b>
            <br />Ievietoti ieraksti: <b>'.$created.'</b>
            <br />Jau eksistējoši ieraksti: <b>'.$existing.'</b>
            <br />Sistēmā neatrasti ieraksti: <b>'.$errors.'</b>');

        }
        else {
            return Redirect::back()->with('error', 'Kļūda augšuplādējot failu!');
        }
    }

    protected  function filterIncoming($row)
    {
        return $row[6]=="C";
    }

}