<?php

namespace App\Http\Controllers\accountant;

use App\Http\Controllers\BaseAccountantController;
use App\Models\Account;
use App\Models\Eticket;
use App\Models\ManualPayment;
use App\Models\Owner;
use App\Models\Seller;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AccountantController extends BaseAccountantController {

    protected $links = 'accountant';

    public function getIndex()
    {
        $data = Transaction::select('transactions.id','amount','confirmed','transactions.created_at','sellers.seller_name')
            ->leftJoin('accounts', 'transactions.from_account_id', '=', 'accounts.id')
            ->leftJoin('sellers', 'accounts.owner_id', '=', 'sellers.owner_id')
            ->Nizm()
            ->Ok()
            ->get();

        return View::make($this->links.'/index', compact('data'));
    }

    public function getHistory()
    {
        $data = Transaction::select('transactions.id','amount','confirmed','transactions.created_at','sellers.seller_name')
            ->leftJoin('accounts', 'transactions.from_account_id', '=', 'accounts.id')
            ->leftJoin('sellers', 'accounts.owner_id', '=', 'sellers.owner_id')
            ->Izm()
            ->Ok()
            ->get();

        return View::make($this->links.'/history', compact('data'));
    }

    public function getConfirm($id)
    {
        $data = Transaction::with('from_account')->where('id',$id)->Nizm()->Ok()->firstOrFail();

        $owner_id = $data->from_account->owner_id;
        $seller = Cache::rememberForever('seller_byowner_'.$owner_id, function() use ($owner_id)
        {
            return Seller::where("owner_id",$owner_id)->get()->first();
        });

        return View::make($this->links.'/confirm', compact('data','seller'));
    }

    public function postConfirm($id)
    {
        $trans = Transaction::with('from_account')->where('id',$id)->Nizm()->Ok()->firstOrFail();

        $trans->confirmed  = 1;

        if ($trans->save())
        {
            return Redirect::to('accountant/index')->with('success', 'Maksājums apstiprināts.');
        }
        else {
            return Redirect::to('accountant/confirm/'.$trans->id)->with('error', 'Neizdevās apstiprināt!');
        }
    }

    public function getPdf($id)
    {
        $data = Transaction::with('from_account')->where('id',$id)->Ok()->firstOrFail();

        return response()->download(public_path('assets/pdfs').'/'.$data->comments.'.pdf', $data->id.'.pdf');
    }

    public function getMtransfer()
    {
        $data = ManualPayment::with("user")->where("confirmed",0)->get();

        return View::make($this->links.'/mtransfer', compact('data'));
    }

    public function getMhistory()
    {
        $data = ManualPayment::where("confirmed",1)->get();

        return View::make($this->links.'/mhistory', compact('data'));
    }

    public function getMconfirm($id)
    {
        $data = ManualPayment::findOrFail($id);

        if ($data->confirmed==1)
            return Redirect::to('accountant/mtransfer')->with('error', 'Maksājums jau ir apstiprināts!');

        $eticket_nr = Eticket::where('id',$data->eticket_id)->first()->nr;

        return View::make($this->links.'/mconfirm', compact('data', 'eticket_nr'));
    }

    public function postMconfirm($id)
    {
        $payment = ManualPayment::findOrFail($id);

        if ($payment->confirmed==1)
        {
            return Redirect::to('accountant/mtransfer')->with('error', 'Maksājums jau ir apstiprināts!');
        }

        //check if first payment
        if ($payment->user->rk_confirmed_user_id == 0)
        {
            $konts = trim(Input::get( 'konts' ));

            if ($konts == '')
                return Redirect::back()->withInput()->with('error','Jāaizpilda lauks - bankas konts!');

            //send email to RK
            if (App::environment() == 'production')
            {
                Mail::send('mail.rkfirstpayment', ["payment" => $payment, "payment_user" => $payment->user, "summa" => Input::get( 'summa' ), "konts" => $konts ],
                    function($message) {
                        $message->to(Config::get('mail.rsemail1'))->bcc(Config::get('mail.rsemail2'))->subject('Skolas.rigaskarte.lv - pirmā maksājuma apstiprinājums.');
                    });
            }

            $user = User::findOrFail($payment->user->id);
            $user->rk_confirmed_user_id = 1;
            $user->rk_confirmed = date("Y-m-d H:i:s");
            $user->konts = $konts;
            $user->save();
        }

        $eticket = Eticket::findOrFail($payment->eticket_id);
        $all_eticket_ids = Eticket::where('pk', $eticket->pk)->lists("id")->toArray();

        // check for monthly limit
        $monthly_sum = ManualPayment::whereIn("eticket_id",$all_eticket_ids)
            ->where("created_at",'LIKE',date("Y-m").'%')
            ->where("id", '!=', $id)
            ->sum("amount") + intval(Input::get( 'summa' )*100);

        if ($monthly_sum > 14000)
        {
            try {
                $all_eticket_nrs = implode(", ",Eticket::where('pk', $eticket->pk)->lists("nr")->toArray());
                $account = Account::where("id",$eticket->account_id)->first();
                $owner = ($account->owner_id != 2) ? Owner::findOrFail($account->owner_id)->owner_name : 'Brīvpusdienu konts';

                //send email to RK
                if (App::environment() == 'production')
                {
                    Mail::send('mail.rkmonthlylimit', ["eticket" => $eticket, "all_eticket_nrs" => $all_eticket_nrs, "payment" => $payment, "monthly_sum" => $monthly_sum, "owner" => $owner, "summa" => Input::get( 'summa' ) ],
                        function($message) {
                            $message->to(Config::get('mail.rsemail1'))->bcc(Config::get('mail.rsemail2'))->subject('Skolas.rigaskarte.lv - personai šomēnes ir sasniegts 140.00 EUR maksājumu limits.');
                        });
                }
            }
            catch (\Exception $e) {
                return Redirect::back()->withInput()->with('error','Kļūda apstrādājot datus - '.$e->getMessage());
            }
        }

        // check for yearly limit
        $year_sum = ManualPayment::whereIn("eticket_id",$all_eticket_ids)
            ->where("created_at",'LIKE',date("Y").'%')
            ->where("id", '!=', $id)
            ->sum("amount") + intval(Input::get( 'summa' )*100);

        if ($year_sum > 168000)
        {
            try {
                $all_eticket_nrs = implode(", ",Eticket::where('pk', $eticket->pk)->lists("nr")->toArray());
                $account = Account::where("id",$eticket->account_id)->first();
                $owner = ($account->owner_id != 2) ? Owner::findOrFail($account->owner_id)->owner_name : 'Brīvpusdienu konts';

                //send email to RK
                if (App::environment() == 'production')
                {
                    Mail::send('mail.rkyearlylimit', ["eticket" => $eticket, "all_eticket_nrs" => $all_eticket_nrs, "payment" => $payment, "year_sum" => $year_sum, "owner" => $owner, "summa" => Input::get( 'summa' ) ],
                        function($message) {
                            $message->to(Config::get('mail.rsemail1'))->bcc(Config::get('mail.rsemail2'))->subject('Skolas.rigaskarte.lv - personai šogad ir sasniegts 1680.00 EUR maksājumu limits.');
                        });
                }
            }
            catch (\Exception $e) {
                return Redirect::back()->withInput()->with('error','Kļūda apstrādājot datus - '.$e->getMessage());
            }
        }

        return $this->confirm_payment($payment,Input::get( 'summa' ),'accountant/mtransfer');
    }

}