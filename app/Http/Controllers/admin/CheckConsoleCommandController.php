<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use DebugBar\DebugBar;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;

class CheckConsoleCommandController extends AdminController
{
    protected $links = 'check_console_command';

    private function getCommandData($command)
    {
        switch ($command) {
            case 1:
                // +
                $result['commandName'] = 'corectionssellerpayments';
                $result['title'] = 'Ģenerēt tirgotāju izmaksas 3x mēnesī';
                break;
            case 2:
                // +
                $result['commandName'] = 'frommonitoring';
                $result['title'] = 'Atjaunot informāciju no API monitoringa';
                break;
            case 3:
                // +
                $result['commandName'] = 'deleteunconfirmed';
                $result['title'] = 'Dzēst neapstiprinātās lietotāju reģistrācijas pēc 5 dienām';
                break;
            case 4:
                // +
                $result['commandName'] = 'disablecsv';
                $result['title'] = 'Deaktivizēt trešo statusu no CSV improtētajiem, kas nav atnākuši no ZZ';
                break;
            case 5:
                // +
                $result['commandName'] = 'genextra';
                $result['title'] = 'Ģenerēt trūkstošās tirgotāju izmaksas';
                break;
            case 6:
                // +
                $result['commandName'] = 'genbalances';
                $result['title'] = 'Ģenerēt tirgotāju dienas beigu bilances';
                break;
            case 7:
                // +
                $result['commandName'] = 'genfreemonth';
                $result['title'] = 'Ģenerēt tirgotāju brīvpusdienu datus par pēdējo mēnesi';
                break;
            case 8:
                // +
                $result['commandName'] = 'genpayments';
                $result['title'] = 'Ģenerēt tirgotāju izmaksas 3x mēnesī';
                break;
            case 22:
                // +
                $result['commandName'] = 'regenpayments';
                $result['title'] = 'Reģenerēt tirgotāju izmaksas 3x mēnesī';
                break;
            case 9:
                // ---
                // Kļūda - personai nav skolas ID 191298-10312
                $result['commandName'] = 'getfreemealpersons';
                $result['title'] = 'Atjaunot informāciju no API par personām ar brīvpusdienām';
                break;
            case 10:
                // ---
                // Nav saņemti tirgotāju reg. nr. dati skolai - 3371802952 Hokeja skola "PĒRKONS" ...
                $result['commandName'] = 'getschooldata';
                $result['title'] = 'Atjaunot skolu informāciju no API';
                break;
            case 11:
                // Command "importcsv" is not defined.
                $result['commandName'] = 'importcsv';
                $result['title'] = 'Importēt info no CSV faila un sanemt karsu datus no API';
                break;
            case 12:
                // Command "importcsv" is not defined.
                $result['commandName'] = 'importcsv';
                $result['title'] = 'Importēt info no CSV faila, bez RK';
                break;
            case 13:
                // +
                $result['commandName'] = 'regen';
                $result['title'] = 'Pārģenerācija';
                break;
            case 14:
                // +
                $result['commandName'] = 'report6';
                $result['title'] = 'DIENAS ATSKAITE DARĪJUMI LIETOTĀJI - visas personas, visi etaloni';
                break;
            case 15:
                // +
                $result['commandName'] = 'report7';
                $result['title'] = 'Ikdienas atskaite nr.7 - visi tirgotāji pa skolām';
                break;
            case 16:
                // +
                $result['commandName'] = 'report8';
                $result['title'] = 'Mēneša atskaite nr.8 - visi tirgotāji pa skolām';
                break;
            case 17:
                // +
                $result['commandName'] = 'reportkopgalds';
                $result['title'] = 'Neatrastie kopgalda darījumi par vakardienu';
                break;
            case 18:
                // +
                $result['commandName'] = 'sendfreedaily';
                $result['title'] = 'Nosūtīt brīvpusdienu datus par vakardienu';
                break;
            case 19:
                // +
                $result['commandName'] = 'updatealletickets';
                $result['title'] = 'Atjaunot informāciju no RK API par visiem aktīvajiem e-taloniem';
                break;
            case 20:
                // +
                $result['commandName'] = 'updatedot';
                $result['title'] = 'Atjaunot informāciju autorizatoram par personām ar brīvpusdienām';
                break;
            case 21:
                // +
                $result['commandName'] = 'updatetrans';
                $result['title'] = 'Papildināt vakardienas transakcijas ar tirgotāju reģistrācijas numuriem un brīvpusdienu tipiem';
                break;
            default:
                $result = [];
                break;
        }
        return $result;
    }

    public function getConsoleCommand($command)
    {
        \Debugbar::disable();

        $commandData = $this->getCommandData($command);
        extract($commandData);
        // Show the page
        switch ($command) {
            case 8: //genpayments
            case 17: //genpayments
                return View::make('admin/' . $this->links . '/index_date', compact('title', 'command', 'commandName'));
                break;
            default:
                return View::make('admin/' . $this->links . '/index', compact('title', 'command', 'commandName'));
                break;
        }                
    }

    public function postConsoleCommand($command)
    {
        $commandData = $this->getCommandData($command);
        extract($commandData);
        Artisan::call($commandName);
        $resultText = Artisan::output();
        $resultText = explode("\n", $resultText);
        $resultText = !empty($resultText) ? $resultText : 'Empty result';
        switch ($command) {
            case 8: //genpayments
            case 17: //genpayments
                return View::make('admin/' . $this->links . '/index_date', compact('title', 'command', 'commandName', 'resultText'));
                break;
            default:
                return View::make('admin/' . $this->links . '/index', compact('title', 'command', 'commandName', 'resultText'));
                break;
        }                
    }

}
