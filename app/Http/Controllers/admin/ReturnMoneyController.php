<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Account;
use App\Models\Eticket;
use App\Models\Mpos;
use App\Models\OwnerAccess;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ReturnMoneyController extends AdminController {

    protected $links = 'returnmoney';

    /**
     * User Model
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Display a listing of available suers
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = 'Atgriezt maksājumu';

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('data', 'title'));
    }

    /**
     * Search for transactions
     *
     * @return Response
     */
    public function postSearch()
    {
        $eticket = Input::get('eticket');
        $datums  = Input::get('datums');

        $et = Eticket::Nr($eticket)->first();
        if (!isset($et->id))
        {
            return Redirect::to('admin/'.$this->links.'/index')
                ->withInput()
                ->with('error','Nav atrasts e-talons!');
        }

        $eticket_account_id = $et->account_id;

        if (strlen($datums)!=10)
        {
            return Redirect::to('admin/'.$this->links.'/index')
                ->withInput()
                ->with('error','Jānorāda datums!');
        }

        $data = Transaction::Ok()
            ->where('from_account_id', '=', $eticket_account_id)
            ->where('created_at', 'LIKE', $datums.'%')
            ->get();

        if (!$data->isEmpty())
        {
            $input = Input::all();

            $title = 'Atrastās transakcijas';

            return View::make('admin/'.$this->links.'/list', compact('data', 'title'));
        }

        return Redirect::to('admin/'.$this->links.'/index')
            ->withInput()
            ->with('error','Nav atrasta neviena transakcija!');
    }

    public function getReturn($trans_id)
    {
        $data = Transaction::Ok()
            ->where('id', '=', $trans_id)
            ->first();

        if (!isset($data->id))
        {
            return Redirect::to('admin/'.$this->links.'/index')
                ->withInput()
                ->with('error','Nav atrasta šāda transakcija!');
        }

        // find mpos owner
        $seller_mpos_account = Account::where("id",$data->to_account_id)->first();

        $mpos = Mpos::where('account_id',$seller_mpos_account->id)->first();
        if (!$mpos->id)
        {
            return Redirect::to('admin/'.$this->links.'/index')
                ->withInput()
                ->with('error','Kļūda atlasot MPOS!');
        }

        $seller_user_id = OwnerAccess::O($mpos->owner_id)
            ->value('user_id');

        // check if seller MPOS has enough balance
        $mpos_balance = $seller_mpos_account->balance;
        if ($mpos_balance < $data->amount)
        {
            return Redirect::to('admin/'.$this->links.'/index')
                ->withInput()
                ->with('error','Nepietiek līdzekļu MPOS kontā!');
        }

        try {
            DB::transaction(function() use($data,$seller_mpos_account,$seller_user_id)
            {
                // create TMP transaction
                $t = New Transaction;
                $t->amount = $data->amount;
                $t->operation_id = 1;
                $t->from_account_id = $seller_mpos_account->id;
                $t->to_account_id = 999997; // tmp account
                $t->mpos_id = $data->mpos_id;
                $t->comments = $data->from_account_id;
                $t->t_status = 'P';
                $t->confirmed = 1;

                if (!$t->save()) throw New Exception();

                // decrease MPOS balance
                $seller_mpos_account->balance = $seller_mpos_account->balance-$data->amount;

                if (!$seller_mpos_account->save()) throw New Exception();

                // create ticket
                $t = New Ticket();
                $t->user_id         = Auth::user()->id;
                $t->recipient_id    = $seller_user_id;
                $t->ticket_type     = 4; // naudas atgriesana
                $t->mpos_id         = $data->mpos_id;
                $t->object_id       = $data->from_account_id;
                $t->eticket_nr      = $data->from_eticket_nr;
                $t->ticket_status   = 0; // jauns ieraksts
                $t->content      = $data->amount;

                if (!$t->save()) throw New Exception();
            });
        }
        catch(Exception $e) {
            return Redirect::to('admin/'.$this->links.'/index')
                ->withInput()
                ->with('error','Kļūda saglabājot datus!');
        }

        $input["id"] = $trans_id;

        return Redirect::to('admin/'.$this->links.'/index')
            ->with('success','Ieraksts veiksmīgi izveidots. Nosūtits paziņojums tirgotājam.');

    }

}