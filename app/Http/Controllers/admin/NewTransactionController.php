<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Dotations;
use App\Models\Eticket;
use App\Models\Mpos;
use App\Models\ObjectDotationPrices;
use App\Models\Objekts;
use App\Models\SchoolPrices;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class NewTransactionController extends AdminController {

    protected $links = 'newtrans';

    public function getIndex()
    {
        // Title
        $title = 'Manuāla brīvpusdienu transakcija - meklēt e-talonu';

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     * Manual load data from file
     *
     * @return Response
     */
    public function postIndex()
    {
        $pk = Input::get( 'pk' );
        $eticket = Input::get( 'eticket' );

        if (trim($pk)=='' AND trim($eticket)=='')
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nav nekas ievadīts!');
        }

        if ((strlen($eticket)>10) AND (strlen($eticket)<8) AND (strlen($pk)!=12))
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nekorekti dati!');
        }

        $r = Eticket::where('eticket_status',1);

        if ($pk!='')
            $r->where('pk',$pk);
        else
            $r->where('nr','like','%'.$eticket.'%');

        // TMP
        //$r->where('free_meals_from','<=',date("Y-m-d"));
        //$r->where('free_meals_until','>=',date("Y-m-d"));

        $record = $r->first();

        if (isset($record->id))
        {
            // get objects and price
            $objekti = $record->activeObjekti();
            if (count($objekti) == 0)
                return Redirect::back()
                    ->withInput()
                    ->with('error', 'E-talonam nav atrasts neviens objekts!');

            $odp = ObjectDotationPrices::get()->toArray();
            $objekti_masivs = [];
            foreach ($odp as $odp_row)
            {
                if (in_array($odp_row["object_id"],$objekti))
                {
                    if ( ! isset($objekti_masivs[$odp_row["object_id"]]))
                    {
                        $objekts_name = Objekts::findOrFail($odp_row["object_id"])->object_name;

                        //$objekti_masivs[$odp_row["object_id"]] = ["price" => $odp_row["price"], "dotation_id" => $odp_row["id"], "name" => $objekts_name];
                        $objekti_masivs[$odp_row["object_id"]] = ["price" => 1, "dotation_id" => 1, "name" => $objekts_name];
                    }
                }
            }

            // Title
            $title = 'Manuālā brīvpusdienu transakcija - izveidot jaunu';

            // Show the page
            return View::make('admin/'.$this->links.'/create', compact('title','record','objekti_masivs'));
        }
        else return Redirect::back()
                ->withInput()
                ->with('error', 'Nav atrasts atbilstošs e-talons!');
    }

    public function postCreate()
    {
        $eticket_id = Input::get( 'eticket_id' );
        $object_id = intval(Input::get( 'objekts' ));
        $datums = trim(Input::get( 'datums' ));
        $akts = Input::get( 'akts' );

        if ($datums=='' || $object_id<=0)
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Jāievada visi lauki!');
        }

        // check that date is no later than 10th of next month
        $datums2 = new Carbon($datums);
        $now = new Carbon();

        if ($now->diffInMonths($datums2) >= 2)
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Var izveidot ierakstus tikai par iepriekšējo mēnesi!');
        }

        if ($now->day > 10 && $now->month > $datums2->month)
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Iepriekšējā mēneša ierakstus var izveidot tikai līdz 10 datumam!');
        }

        if ($datums2->gt($now))
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Ierakstus nevar izveidot nākotnes datumiem!');
        }

        exit();

        $eticket = Eticket::findOrFail($eticket_id);
        $objekts = Objekts::findOrFail($object_id);

        // get objects and price
        $objekti = $eticket->activeObjekti();
        if (count($objekti) == 0)
            return Redirect::back()
                ->withInput()
                ->with('error', 'E-talonam nav atrasts neviens objekts!');

        $mpos = Mpos::where("object_id",$object_id)->first();
        if ( ! isset($mpos->id))
            return Redirect::back()
                ->withInput()
                ->with('error', 'E-talonam nav atrasts atbilstošs MPOS ieraksts!');

        // check for existing free meal record for this person, this day
        $exists = $this->free_meal_exists_today($eticket, $datums);

        if ($exists > 0)
            return Redirect::back()
                ->withInput()
                ->with('error', 'Personai šodien jau eksistē brīvpusdienu ieraksts!');

        try {
            DB::transaction(function () use ($mpos,$eticket,$object_id,$akts,$datums)
            {
//                $mpos_account = Account::findOrFail($mpos->account_id);
//                $mpos_account->balance = $mpos_account->balance + $odp->price;
//                $mpos_account->save();

//                $seller_reg_nr = '';
//                $school_price = SchoolPrices::where("object_id",$odp->object_id)
//                    ->where("price",sprintf("%.2f",$odp->price/100))
//                    ->first();
//
//                if (isset($school_price->regnr)) $seller_reg_nr = $school_price->regnr;

                // new transaction
                $t = New Transaction;
                $t->mpos_id = $mpos->id;
                $t->amount = 1;
                $t->operation_id = 1;
                $t->from_account_id = 2;
                $t->from_eticket_nr = $eticket->nr;
                $t->to_account_id = $mpos->account_id;
                $t->object_id = $object_id;
                $t->confirmed = 1;
                $t->dotation_id = 1;
                $t->seller_reg_nr = '';
                $t->t_status = "P";
                $t->comments = "Manual transaction, akta nr. " . $akts;
                $t->created_at = $datums;
                $t->save();
            });
        }
        catch (Exception $e) {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Neizdevās saglabāt ierakstu - '.$e->getMessage().'!');
        }

        return Redirect::to('admin/'.$this->links)->with('success', 'Dati saglabāti.');
    }

    public function zero()
    {
        // all zero transactions for previous month
        $menesis = new Carbon();
        $menesis->subMonth();
        $periods = substr($menesis->toDateString(),0,7);

        $all = Transaction::where("from_account_id",'>', 2)
            ->Ok()
            ->where("created_at", 'LIKE', $periods . '-%')
            ->where("amount", 0)
            ->where("dotation_id",null)
            ->get();

        echo 'Atrasti kopā: '.count($all).'<br />';

        $changed = 0;
        foreach ($all as $row)
        {
            $eticket = Eticket::where("nr",$row->from_eticket_nr)->first();

            if ( ! isset($eticket->id)) continue;

            $exists = $this->free_meal_exists_today($eticket, $row->created_at);

            if ($exists == 0)
            {
                $old_account_id = $row->from_account_id;
                $comment = 'fromzero('.$old_account_id.')';

                // get objects and price
                $objekti = $eticket->activeObjekti();
                if (count($objekti) == 0) continue;

                $dotations = Dotations::where("pk",$eticket->pk)->lists("dotation_id")->toArray();
                if (count($dotations) == 0) continue;

                $odp = ObjectDotationPrices::whereIn("id",$dotations)
                    ->where("object_id",$row->object_id)
                    ->first();

                if ( ! isset($odp->object_id)) continue;

                $seller_reg_nr = '';
                $school_price = SchoolPrices::where("object_id",$odp->object_id)
                    ->where("price",sprintf("%.2f",$odp->price/100))
                    ->first();

                if (isset($school_price->regnr)) $seller_reg_nr = $school_price->regnr;

                $row->from_account_id = 2;
                $row->dotation_id = 2;
                $row->seller_reg_nr = $seller_reg_nr;
                $row->comments = (strlen(trim($row->comments))>0) ? $row->comments.' | '.$comment : $comment;
                $row->save();

                $changed++;
            }
        }

        echo 'Laboti kopā: '.$changed.'<br />';
    }

    /**
     * @param $eticket
     * @param $datums
     * @return mixed
     */
    private function free_meal_exists_today($eticket, $datums)
    {
        $eticket_nrs = Eticket::where("pk", $eticket->pk)->lists("nr")->toArray();

        $exists = Transaction::where("t_status", "P")
            ->where("dotation_id", ">", 0)
            ->where("from_account_id", 2)
            ->where("created_at", 'LIKE', substr($datums, 0, 10) . '%')
            ->whereIn("from_eticket_nr", $eticket_nrs)
            ->count();
        return $exists;
    }

}