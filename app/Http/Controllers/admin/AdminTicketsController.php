<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Account;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\Ticket;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminTicketsController extends AdminController {

    protected $links = 'tickets';

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.management');

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     * Show the form for cancelling
     *
     * @return Response
     */
    public function getCancel($objekts)
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.cancel_eticket_request');

        // Show the page
        return View::make('admin/'.$this->links.'/cancel', compact('title','objekts'));
    }

    /**
     * Cancel resource
     *
     * @return Response
     */
    public function postCancel($objekts)
    {
        $t = Ticket::find($objekts->id);
        if ($t->id)
        {
            try {
                $et = Eticket::where('nr',$objekts->eticket_nr)->where('eticket_status',3)->first();
                $acc = Account::find($et->account_id);

                $acc->delete();
                $et->delete();
            }
            catch (\Exception $e)
            {
                return Redirect::to('admin/'.$this->links.'/')->with('error','Neizdevās atcelt ierakstu.');
            }

            $t->ticket_status = 2;
            $t->completed_when = date("Y-m-d H:i:s");
            if (!$t->save())
            {
                return Redirect::to('admin/'.$this->links.'/'.$objekts->id.'/cancel')
                    ->with('error','Neizdevās atcelt ierakstu.')
                    ->withErrors($t->getErrors());
            }

            return Redirect::to('admin/'.$this->links.'/')->with('success', Lang::get('admin/'.$this->links.'/messages.cancel.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/'.$this->links.'/'.$objekts->id.'/cancel')
                ->with('error','Neizdevās atrast ierakstu.');
        }
    }

    /**
     * Show the form for confirming custom e-ticket
     *
     * @return Response
     */
    public function getConfirm($objekts)
    {
        // Title
        $title = 'Izveidot šādu ierakstu pie atļautajiem e-taloniem';

        // Show the page
        return View::make('admin/'.$this->links.'/confirm', compact('title','objekts'));
    }

    /**
     * Confirm custom resource
     *
     * @return Response
     */
    public function postConfirm($objekts)
    {
        $t = Ticket::find($objekts->id);
        if ($t->id)
        {
            $pk = substr($objekts->content,strpos($objekts->content,'PK')+3,12);

            try
            {
               $exists = EticketsBase::where('pk',$pk)->count();

               if ($exists>0)
               {
                    // update if necessary
                    $ex_record = EticketsBase::where('pk',$pk)->first();
                    $pos = strpos($ex_record->etickets, $objekts->eticket_nr);
                    if ($pos === false)
                    {
                        $ex_record->etickets = $ex_record->etickets.','.$objekts->eticket_nr;
                        $ex_record->save();
                    }
               }
               else {
                    // create ticket_base record
                    $etb = New EticketsBase();
                    $etb->pk = $pk;
                    $etb->etickets = $objekts->eticket_nr;
                    $etb->custom = 1;
                    if (!$etb->save())
                    {
                        // Redirect on failure
                        return Redirect::to('admin/'.$this->links.'/'.$objekts->id.'/confirm')
                            ->withInput()
                            ->with('error','Neizdevās atrast ierakstu.');
                    }
               }
            }
            catch(\Exception $exception)
            {
                return Redirect::to('admin/'.$this->links.'/'.$objekts->id.'/confirm')
                    ->with('error','Neizdevās pabeigt ierakstu.')
                    ->with('error',$exception->getMessage());
            }

            // apstiprinat e-talona ierakstu
            $unconfirmed_eticket = Eticket::where('eticket_status',3)->where('nr',$objekts->eticket_nr)->first();
            $unconfirmed_eticket->eticket_status=1;
            if (!$unconfirmed_eticket->save())
            {
                return Redirect::to('admin/'.$this->links.'/'.$objekts->id.'/confirm')
                    ->with('error','Neizdevās apstiprināt e-talonu!');
            }

            // operations
            DB::table('eticket_opers')->insert(
                array('eticket_id' => $unconfirmed_eticket->id, 'operation_id' => 1)
            );

            // update user active e-tickets cache
            Cache::forget('active_etickets_'.$objekts->user_id);
            Cache::forget('user_accounts_et_'.$objekts->user_id);
            Cache::forget('user_accounts_etg_'.$objekts->user_id);

            $t->ticket_status = 1;
            $t->completed_when = date("Y-m-d H:i:s");
            if (!$t->save())
            {
                $etb->delete();

                return Redirect::to('admin/'.$this->links.'/'.$objekts->id.'/confirm')
                    ->with('error','Neizdevās pabeigt ierakstu.')
                    ->withErrors($t->getErrors());
            }

            return Redirect::to('admin/'.$this->links.'/')->with('success', Lang::get('admin/'.$this->links.'/messages.confirm.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/'.$this->links.'/'.$objekts->id.'/confirm')
                ->withInput()
                ->with('error','Neizdevās atrast ierakstu.');
        }
    }

    /**
     * Show a list of all the objects formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {

        $data = Ticket::leftjoin('users AS u', 'tickets.user_id', '=', 'u.id')->
            leftjoin('users AS u2', 'tickets.recipient_id', '=', 'u2.id')->
            leftjoin('tickets_types', 'tickets.ticket_type', '=', 'tickets_types.id')->
            leftjoin('objects', 'tickets.object_id', '=', 'objects.id')->
            select('tickets.id','u.username','u.surname','u2.username as username2','u2.surname as surname2','tickets.owner_id','mpos_id','object_id','eticket_nr','ticket_status','tickets_types.name','completed_when','content','tickets.created_at','ticket_type','objects.object_name')
            ->orderBy("tickets.id","DESC");

        return Datatables::of($data)
        
        ->edit_column('username','{!! $username !!} {!! $surname !!}')
        ->edit_column('username2','{!! $username2 !!} {!! $surname2 !!}')
        ->edit_column('object_id','@if ($ticket_type!=3) 
                                        {!! $object_name !!}
                                    @endif')
        ->edit_column('ticket_status','@if ($ticket_status == 0)
                                        Gaida uz apstiprinājumu
                                    @elseif ($ticket_status == 1)
                                        Izpildīts
                                    @elseif ($ticket_status == 2)
                                        Atcelts
                                    @else
                                        Nepieciešams apstiprināt
                                    @endif')

        ->remove_column('id','surname','surname2','ticket_type','object_name')

        ->add_column('actions', '@if ($ticket_status==0 && $ticket_type==1) <a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/confirm\' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get(\'button.confirm\') }}}</a>
            <a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/cancel\' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get(\'button.cancel\') }}}</a> @endif')

        ->make();
    }


}