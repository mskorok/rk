<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Banner;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminBannersController extends AdminController
{

    protected $links = 'banners';

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Initializer.
     *
     * @access   public
     * @param User $user
     * @throws \Exception
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Display a listing of resources
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        // Title
        $title = \Lang::get('admin/'.$this->links.'/title.management');

        // Show the page
        return \View::make('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     *
     *
     * @param Request $request
     * @param Banner $banner
     * @return \Illuminate\Contracts\View\View|View
     */
    public function getEdit(Request $request, Banner $banner)
    {
        if ($banner instanceof Banner) {
            $title = trans('admin/'.$this->links.'/title.obj_edit');
            $old = $request->old();
            if (!empty($old) && !isset($old['active'])) {
                $old['active'] = false;
            }
            $action = route('banner_edit_post', ['banner' => $banner->id]);
            return view('admin/banners/edit', compact('banner', 'title', 'action', 'old'));
        } else {
            return \Redirect::to('admin/'.$this->links)
                ->with('error', trans('admin/'.$this->links.'/messages.does_not_exist'));
        }
    }

    /**
     *
     * @param Request $request
     * @param Banner $banner
     * @return \Illuminate\Contracts\View\View | RedirectResponse
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function postEdit(Request $request, Banner $banner)
    {
        if ($banner instanceof Banner) {
            $data = $request->input();
            if (!isset($data['active']) || (int) $data['active'] !== 1) {
                $data['active'] = 0;
            }
            $data['exposition_time_to'] = $data['exposition_time_to'] !== '' ? $data['exposition_time_to'] : null;
            $data['exposition_time_from'] = $data['exposition_time_from'] !== '' ? $data['exposition_time_from'] : null;
            unset($data['\\'], $data['/']);
            $validator = \Validator::make($data, Banner::EDIT_VALIDATION_RULES);
            if ($validator->passes()) {
                $banner->fill($data);
                if ($banner->save()) {
                    return redirect()->to('/admin/banners/')
                        ->with('success', trans('admin/'.$this->links.'/messages.edit.success'));
                } else {
                    // Redirect on failure
                    return redirect()->to('admin/'.$this->links.'/' . $banner->id . '/edit')
                        ->withInput()
                        ->withErrors($banner->getErrors());
                }
            } else {
                return redirect()->back()->withInput()->withErrors($validator->errors()->toArray());
            }
        }
        return redirect()->to('/admin/banners/');
    }

    /**
     * @return View
     */
    public function getCreate()
    {
        $title = trans('admin/'.$this->links.'/title.obj_create');
        $action = route('banner_create_post');
        return view('admin/banners/create', compact('title', 'action'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function postCreate(Request $request)
    {
        $banner = new Banner();

        $data = $request->input();
        unset($data['\\'], $data['/']);
        $validator = \Validator::make($data, Banner::VALIDATION_RULES);
        if ($validator->passes()) {
            $banner->fill($data);
            if ($banner->save()) {
                return redirect()->to('/admin/banners/')
                    ->with('success', trans('admin/'.$this->links.'/messages.create.success'));
            } else {
                // Redirect on failure
                return redirect()->to('admin/'.$this->links.'/' . $banner->id . '/edit')
                    ->withInput()
                    ->withErrors($banner->getErrors());
            }
        } else {
            return redirect()->back()->withInput()->withErrors($validator->errors()->toArray());
        }
    }
    public function delete($banner)
    {
        if ($banner instanceof Banner) {
            try {
                $banner->delete();
            } catch (\RuntimeException $e) {
                return redirect()->back()->withErrors(['not_deleted' => $e->getMessage()]);
            }
        } else {
            $item = Banner::find((int) $banner);
            if ($item instanceof Banner) {
                try {
                    $item->delete();
                } catch (\RuntimeException $e) {
                    return redirect()->back()->withErrors(['not_deleted' => $e->getMessage()]);
                }
            }
        }
        return redirect()->back();
    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        /** @var \Illuminate\Database\Query\Builder $data */
        $data = Banner::select(
            [
                'id',
                'name',
                'type',
                'sort',
                'active',
                'size',
                'exposition_time_from',
                'exposition_time_to',
            ]
        );

        return Datatables::of($data)

            ->add_column(
                'actions',
                '<a href="{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/edit\' ) }}" class="btn btn-sm btn-primary">{{ Lang::get(\'button.edit\') }}</a>&nbsp;<a href="{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/delete\' ) }}" class="btn btn-sm btn-danger">{{ Lang::get(\'button.delete\') }}</a>'
            )
            ->remove_column('id')
            ->make();
    }
}
