<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Eticket;
use App\Models\EticketsObjekti;
use App\Models\GroupObjekts;
use App\Models\Objekts;
use App\Models\ObjGroup;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminObjGroupController extends AdminController
{

    protected $links = 'objgroup';

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/' . $this->links . '/title.management');

        $data = ObjGroup::all();
        foreach ($data as &$single) {
            $tmp = GroupObjekts::where('ogroup_id', $single->id)->lists('object_id')->toArray();
            $obj = array();
            foreach ($tmp as $object_id) {
                $obj[] = Objekts::where('id', intval($object_id))->value('object_name');
            }
            $single->objekti = implode("<br />", $obj);
        }

        // Show the page
        return View::make('admin/' . $this->links . '/index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // Title
        $title = Lang::get('admin/' . $this->links . '/title.create_a_new_obj');

        // Mode
        $mode = 'create';

        // all group data
        $data = Objekts::all();

        // Show the page
        return View::make('admin/' . $this->links . '/create_edit', compact('title', 'mode', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        $skolas = Input::get('object_id');

        if (count($skolas) < 2)
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nav norādīti vismaz 2 objekti!');

        $exists = ObjGroup::whereHas('objects', function ($query) use ($skolas) {
                $query->whereIn('object_id', $skolas);
            })
            ->count();
        if ($exists > 0)
            return Redirect::back()
                ->withInput()
                ->with('error', 'Šāda grupa jau eksistē!');

        try {
            $objekts = DB::transaction(function () use ($skolas) {
                $objekts = New ObjGroup;
                $objekts->ogroup_name = Input::get('ogroup_name');

                if (!$objekts->save())
                    throw new \Exception("Kļūda saglabājot OGJGROUP datus - " . $objekts->getErrors());

                foreach ($skolas as $s) {
                    $og = New GroupObjekts;
                    $og->ogroup_id = $objekts->id;
                    $og->object_id = $s;
                    if (!$og->save())
                        throw new \Exception("Kļūda saglabājot GROUPOBJ datus - " . $og->getErrors());

                    $this->update_object_etickets($skolas);
                }

                return $objekts;
            });
        } catch (\Exception $e) {
            Log::error("Kļūda izveidojot objektu grupu - " . $e->getMessage());
            return Redirect::back()
                ->withInput()
                ->withErrors($e->getMessage());
        }

        return Redirect::to('admin/' . $this->links . '/' . $objekts->id . '/edit')
            ->with('success', Lang::get('admin/' . $this->links . '/messages.create.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $objekts
     * @return Response
     */
    public function getEdit($objekts)
    {
        if ($objekts->id) {
            // all group data
            $data = Objekts::orderBy('object_name')->get();

            // Title
            $title = Lang::get('admin/' . $this->links . '/title.obj_edit');
            // mode
            $mode = 'edit';

            $oo = array();
            foreach ($objekts->objects as $o) {
                $oo[] = intval($o->object_id);
            }

            return View::make('admin/' . $this->links . '/create_edit', compact('objekts', 'title', 'mode', 'data', 'oo'));
        } else {
            return Redirect::to('admin/' . $this->links)->with('error', Lang::get('admin/' . $this->links . '/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postEdit($objekts)
    {
        $skolas = Input::get('object_id');

        if (count($skolas) < 2)
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nav norādīti vismaz 2 objekti!');

        try {
            DB::transaction(function () use ($skolas, $objekts) {
                $updateo = ObjGroup::find($objekts->id);

                $updateo->ogroup_name = Input::get('ogroup_name');
                if (!$updateo->save())
                    throw new \Exception("Kļūda saglabājot OGJGROUP datus - " . $updateo->getErrors());

                // remove old
                $old_go_ids = GroupObjekts::where('ogroup_id', $objekts->id)->lists("object_id")->toArray();

                $this->update_object_etickets($old_go_ids, 'update', $skolas);

                GroupObjekts::where('ogroup_id', $objekts->id)->delete();

                // add new
                foreach ($skolas as $s) {
                    $og = New GroupObjekts;
                    $og->ogroup_id = $objekts->id;
                    $og->object_id = $s;
                    if (!$og->save())
                        throw new \Exception("Kļūda saglabājot GROUPOBJ datus - " . $og->getErrors());
                }
            });
        } catch (\Exception $e) {
            Log::error("Kļūda saglabājot objektu grupu - " . $e->getMessage());
            return Redirect::back()
                ->withInput()
                ->withErrors($e->getMessage());
        }

        return Redirect::back()
            ->with('success', Lang::get('admin/' . $this->links . '/messages.edit.success'));
    }

    /**
     * Remove resource page.
     *
     * @param $objekts
     * @return Response
     */
    public function getDelete($objekts)
    {
        // Title
        $title = Lang::get('admin/' . $this->links . '/title.obj_delete') . ' - ' . $objekts->ogroup_name;

        $obj = $objekts->objects()->lists("object_id")->toArray();

        $eticket_count = EticketsObjekti::where("pirma", 0)
            ->whereIn("object_id", $obj)
            ->count();

        $eticket_count = count($obj) > 0 ? round($eticket_count / count($obj)) : 0;

        // Show the page
        return View::make('admin/' . $this->links . '/delete', compact('objekts', 'title', 'eticket_count'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postDelete($objekts)
    {
        $id = $objekts->id;
        $og = ObjGroup::findOrFail($id);

        try {
            DB::transaction(function () use ($og, $id) {
                $og->delete();

                $old_go_ids = GroupObjekts::where('ogroup_id', $id)->lists("object_id")->toArray();

                $this->update_object_etickets($old_go_ids, 'delete');

                GroupObjekts::where('ogroup_id', $id)->delete();
            });
        } catch (\Exception $e) {
            Log::error("Kļūda dzēšot objektu grupu - " . $e->getMessage());
            return Redirect::back()
                ->withInput()
                ->withErrors($e->getMessage());
        }

        return Redirect::to('admin/' . $this->links)
            ->with('success', Lang::get('admin/' . $this->links . '/messages.delete.success'));
    }

    /**
     * Show a list of all
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $data = ObjGroup::leftjoin('objects_ogroups AS og', 'ogroups.id', '=', 'objects_ogroups.ogroup_id')->
            select(array('objects_ogroups.id', 'objects_ogroups.ogroup_name', '', 'objects_ogroups.created_at'));

        return Datatables::of($data)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/' . $this->links . '/\' . $id . \'/edit\' ) }}}" class="btn btn-mini">{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/' . $this->links . '/\' . $id . \'/delete\' ) }}}" class="btn btn-mini btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')
            ->remove_column('id')
            ->make();
    }

    /**
     * @param $object_id
     * @throws Exception
     */
    protected function update_object_etickets($object_ids, $method = 'create', $new_object_ids = [])
    {
        $all_object_ids = $object_ids;
        $etickets_ids = EticketsObjekti::whereIn("object_id", $all_object_ids)
            ->where("pirma", 1)
            ->lists("eticket_id")->toArray();
        $object_etickets = Eticket::whereIn("id", $etickets_ids)->get();
        foreach ($object_etickets as $et) {
            if ($method == 'update' || $method == 'delete') {
                EticketsObjekti::where("pirma", 0)
                    ->where("eticket_id", $et->id)
                    ->whereIn("object_id", $all_object_ids)
                    ->delete();
            }

            if ($method != 'delete') {
                $object_ids = ($method == 'update') ? $new_object_ids : $all_object_ids;

                $primary = $et->pirmais_objekts();
                $primary_id = (isset($primary->id)) ? $primary->id : 0;
                if (($key = array_search($primary_id, $object_ids)) !== false) {
                    unset($object_ids[$key]);
                }

                foreach ($object_ids as $id) {
                    $data = ['eticket_id' => $et->id, 'object_id' => $id, 'pirma' => 0];
                    $eo = EticketsObjekti::firstOrCreate($data);
                }
            }

            $et->group_id = 0;
            if (!$et->save())
                throw new \Exception("Kļūda saglabājot eticket datus - " . $et->getErrors());
        }
    }

}
