<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Account;
use App\Models\Owner;
use App\Models\Seller;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminSellersController extends AdminController {

    protected $links = 'sellers';

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Seller Model
     * @var Seller
     */
    protected $sellers;

    public function __construct(User $user, Seller $sellers)
    {
        parent::__construct();
        $this->user = $user;
        $this->sellers = $sellers;
    }

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.management');

        // Grab all the groups
        $data = $this->sellers;

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.create_a_new_obj');

        // Mode
        $mode = 'create';

        // Show the page
        return View::make('admin/'.$this->links.'/create_edit', compact('title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {

        $owner = New Owner;
        $owner->owner_type = 3;
        $owner->owner_name = Input::get( 'seller_name' );
        if ($owner->save())
        {
            $objekts = New Seller;
            $objekts->owner_id = $owner->id;
            $objekts->seller_name = Input::get( 'seller_name' );
            $objekts->jur_adrese = Input::get( 'jur_adrese' );
            $objekts->talrunis = Input::get( 'talrunis' );
            $objekts->banka = Input::get( 'banka' );
            $objekts->bankas_kods = Input::get( 'bankas_kods' );
            $objekts->konts = Input::get( 'konts' );
            $objekts->reg_nr = Input::get( 'reg_nr' );
            $objekts->contract_nr = Input::get( 'contract_nr' );
            $objekts->contract_person = Input::get( 'contract_person' );

            if ($objekts->save())
            {
                // create seller monetary account
                $acc = New Account;
                $acc->owner_id = $owner->id;
                $acc->account_type = 3;
                $acc->account_status = 1;
                if (!$acc->save())
                {
                    $objekts->delete();
                    $owner->delete();
                    return Redirect::to('admin/'.$this->links.'/create')->withInput()->with('error', 'Neizdevās izveidot kontu!');
                }

                // create seller free meal account
                $acc2 = New Account;
                $acc2->owner_id = $owner->id;
                $acc2->account_type = 7;
                $acc2->account_status = 1;
                if (!$acc2->save())
                {
                    $objekts->delete();
                    $owner->delete();
                    return Redirect::to('admin/'.$this->links.'/create')->withInput()->with('error', 'Neizdevās izveidot brīvpusdienu kontu!');
                }

                Cache::forget('all_seller_ids');
                Cache::forget('all_sellers');

                return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')->with('success', Lang::get('admin/'.$this->links.'/messages.create.success'));
            }
            else {
                $owner->delete();
                return Redirect::to('admin/'.$this->links.'/create')
                    ->withInput()
                    ->withErrors($objekts->getErrors())
                    ->with('error','Kļūda!');
            }
        }
        else {
            return Redirect::to('admin/'.$this->links.'/create')
                ->withInput()
                ->withErrors($owner->getErrors())
                ->with('error','Kļūda!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $objekts
     * @return Response
     */
    public function getEdit($objekts)
    {
        if ( $objekts->id )
        {
            // Title
            $title = Lang::get('admin/'.$this->links.'/title.obj_edit');
            // mode
            $mode = 'edit';

            return View::make('admin/'.$this->links.'/create_edit', compact('objekts', 'title', 'mode'));
        }
        else
        {
            return Redirect::to('admin/'.$this->links)->with('error', Lang::get('admin/'.$this->links.'/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postEdit($objekts)
    {

        $updateo = Seller::find($objekts->id);

        // update
        $updateo->seller_name = Input::get( 'seller_name' );
        $updateo->jur_adrese = Input::get( 'jur_adrese' );
        $updateo->talrunis = Input::get( 'talrunis' );
        $updateo->banka = Input::get( 'banka' );
        $updateo->bankas_kods = Input::get( 'bankas_kods' );
        $updateo->konts = Input::get( 'konts' );
        $updateo->reg_nr = Input::get( 'reg_nr' );
        $updateo->contract_nr = Input::get( 'contract_nr' );
        $updateo->contract_person = Input::get( 'contract_person' );

        if ($updateo->save())
        {
            Cache::forget('seller_byowner_'.$updateo->owner_id);

            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')->with('success', Lang::get('admin/'.$this->links.'/messages.edit.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')
                ->withInput()
                ->withErrors($updateo->getErrors());
        }

    }

    /**
     * Remove resource page.
     *
     * @param $objekts
     * @return Response
     */
    public function getDelete($objekts)
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.obj_delete').' - '.$objekts->seller_name;

        // Show the page
        return View::make('admin/'.$this->links.'/delete', compact('objekts', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postDelete($objekts)
    {
        $owner_id = $objekts->owner_id;
        $id = $objekts->id;
        $objekts->delete();

        $objekts = Seller::find($id);
        if ( empty($objekts) )
        {
            Cache::forget('all_seller_ids');
            Cache::forget('all_sellers');
            Cache::forget('seller_byowner_'.$owner_id);

            return Redirect::to('admin/'.$this->links)->with('success', Lang::get('admin/'.$this->links.'/messages.delete.success'));
        }
        else return Redirect::to('admin/'.$this->links)->with('error', Lang::get('admin/'.$this->links.'/messages.delete.error'));
    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $data = Seller::select(array('id','seller_name','jur_adrese','talrunis','banka','bankas_kods','konts','contract_nr','contract_person','created_at'));

        return Datatables::of($data)

        ->add_column('actions', '<a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/edit\' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')

        ->edit_column('contract_nr','{!!$contract_nr!!}<br />{!!$contract_person!!}')
        ->remove_column('id')
        ->remove_column('contract_person')

        ->make();
    }


}