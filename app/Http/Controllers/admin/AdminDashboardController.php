<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\View;

class AdminDashboardController extends AdminController
{

    /**
     * Admin dashboard
     *
     */
    public function getIndex()
    {
        return View::make('admin/dashboard');
    }
}
