<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\User;
use DebugBar\DebugBar;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class ChangeUserController extends AdminController
{

    protected $links = 'changeuser';

    /**
     * User Model
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Display a listing of available suers
     *
     * @return Response | \Illuminate\Contracts\View\View
     */
    public function getIndex()
    {
        \Debugbar::disable();

        // Title
        $title = 'Pārslēgties kā lietotājam';

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     * Change user
     *
     * @return Response |RedirectResponse
     */
    public function postChange()
    {
        $email = Input::get('users');
        $password = Input::get('pass');

        if (trim($password) === '') {
            return Redirect::to('admin/'.$this->links.'/index')
                ->withInput()
                ->with('error', 'Nav ievadīta parole!');
        }

        // check users password
        if (! Hash::check($password, Auth::user()->password)) {
            return Redirect::to('admin/'.$this->links.'/index')
                ->withInput()
                ->with('error', 'Nepareiza parole!');
        }

        $res = User::select('id')->where('email', '=', $email)->get()->first();

        if (! is_null($res)) {
            $input = Input::all();
            unset($input["pass"]);

            $id = $res->id;
            Auth::loginUsingId($id);

            return redirect()->route('user_redirect');

//            return Redirect::to('/')->with('success', 'Lietotāja pārslēgšana sekmīga!');
        }

        return Redirect::to('admin/'.$this->links.'/index')
            ->withInput()
            ->with('error', 'Kļūda atlasot lietotāju!');
    }

    /**
     * Get JSON data
     *
     * @param $q
     * @return Response | JsonResponse
     */
    public function getFetch($q)
    {
        return response()->json(\App\Models\User::where('email', 'LIKE', '%' . $q . '%')->lists('email'));
    }

}