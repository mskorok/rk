<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Eticket;
use App\Models\Objekts;
use App\Models\School;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

class NewEticketController extends AdminController {

    protected $links = 'neweticket';

    public function getIndex()
    {
        // Title
        $title = 'Meklēt e-talonu';

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     * Manual load data from file
     *
     * @return Response
     */
    public function postIndex()
    {
        $pk = Input::get( 'pk' );
        $eticket = Input::get( 'eticket' );

        if (trim($pk)=='' AND trim($eticket)=='')
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nav nekas ievadīts!');
        }

        if ((strlen($eticket)>10) AND (strlen($eticket)<8) AND (strlen($pk)!=12))
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nekorekti dati!');
        }

        $r = Eticket::where('eticket_status',1);

        if ($pk!='')
            $r->where('pk',$pk);
        else
            $r->where('nr','like','%'.$eticket.'%');

        $record = $r->first();

        if (isset($record->id))
            return Redirect::to('admin/'.$this->links.'/'.$record->id.'/edit/');
        else
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nav atrasts e-talons!');
    }

    public function getCreate()
    {
        // Title
        $title = 'Izveidot e-talonu';

        // Show the page
        return View::make('admin/'.$this->links.'/create', compact('title'));
    }

    /*
    public function postCreate()
    {
        $pk = Input::get( 'pk' );

        if ($pk=='')
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Jāievada personas kods!');
        }

        // call APIs
        $results = $this->create_eticket_api_calls($pk, 1);

        if ($results["r2"] == 'error')
            return Redirect::back()
                ->withInput()
                ->with('error',"Kļūda saņemot datus, lūdzu mēģiniet vēlreiz!");

        if ($results["r2"] == 'error1')
            return Redirect::back()
                ->withInput()
                ->with('error',"Kļūda, šādi dati nav atrasti ZZ API!");

        // check declared status only for first type
        if ($results["r2"]["DeclaredRiga"] == "true")
        {
            $mas = isset($results["r2"]["FreeMeals"]["FreeMealPeriod"]) ? App::make('App\Console\Commands\BaseCommand')->find_current_periods($results["r2"]["FreeMeals"]["FreeMealPeriod"]) : ['0000-00-00','0000-00-00'];
        }
        else {
            $mas  = [];
        }
        $mas2 = isset($results["r2"]["UnusedMeals"]["UnusedMeal"]) ? App::make('App\Console\Commands\BaseCommand')->find_current_periods($results["r2"]["UnusedMeals"]["UnusedMeal"]) : ['0000-00-00','0000-00-00'];
        $mas3 = isset($results["r2"]["RdMeals"]["RdMeal"]) ? App::make('App\Console\Commands\BaseCommand')->find_current_periods($results["r2"]["RdMeals"]["RdMeal"]) : ['0000-00-00','0000-00-00'];
        $mas4 = isset($results["r2"]["TableRdMeals"]["TableRdMeal"]) ? App::make('App\Console\Commands\BaseCommand')->find_current_periods($results["r2"]["TableRdMeals"]["TableRdMeal"]) : ['0000-00-00','0000-00-00'];

        if ($mas[1] == '0000-00-00' && $mas2[1] == '0000-00-00' && $mas3[1] == '0000-00-00')
            return Redirect::back()
                ->withInput()
                ->with('error',"Kļūda, šim personas kodam nav brīvpusdienu!");

        Session::put('results', $results);

        if ($results["r1"] == 'error1' || $results["r1"] == 'error')
        {
            $title = 'Nav atrasti e-taloni, tie jāievada manuāli';
            return View::make('admin/'.$this->links.'/create2', compact('title','results'));
        }

        if (is_array($results["r1"]) && count($results["r1"])==0)
        {
            $title = 'Nav atrasti e-taloni, tie jāievada manuāli';
            return View::make('admin/'.$this->links.'/create2', compact('title','results'));
        }

        // create e-tickets that not exist
        $eticket_nrs = $results["r1"];
        foreach ($eticket_nrs as $eticket_nr)
        {
            // check for existing
            $q = Eticket::where('nr',$eticket_nr);
            $existing = $q->get();

            if ( ! isset($existing->id))
            {
                $school = School::findOrFail($results["r2"]["School"]["SchoolId"]);
                $objekts = Objekts::where("object_regnr",$school->reg_nr)->first();
                if ( ! isset ($objekts->id))
                {
                    Log::error("admin_addeticket - nevaru atrast objektu ar reg. nr. " . $school->reg_nr);
                }
                else $eticket_id = App::make('App\Http\Controllers\etickets\EticketsController')->save_new_eticket($eticket_nr, $pk, $results["r2"]["name"], '0000', $results["r2"]["School"]["SchoolClass"], 0, 2, $objekts->id, $school->id, $mas, $mas2, $mas3, $mas4);

                if ( ! isset($eticket_id))
                    return Redirect::back()
                        ->withInput()
                        ->with('error',"Kļūda, skatīt kļūdu LOGā");

                if ( intval($eticket_id) <= 0)
                    return Redirect::back()
                        ->withInput()
                        ->with('error',$eticket_id);
            }
        }

        App::make('BaseController')->add_pk_to_monitoring_list($pk);
        Session::forget('results');

        return Redirect::to('admin/'.$this->links.'/'.$eticket_id.'/edit')->with('success', 'Dati saglabāti,');
    }

    public function postCreate2()
    {
        if ( ! Session::has('results'))
        {
            return Redirect::back()
                ->with( 'error', "Trūkst API datu!" );
        }

        $results = Session::get('results');
        $pk = $results["r2"]["pk"];
        $mas = (isset($results["r2"]["FreeMeals"]["FreeMealPeriod"])) ? $results["r2"]["FreeMeals"]["FreeMealPeriod"] : [];
        $mas2 = (isset($results["r2"]["UnusedMeals"]["UnusedMeal"])) ? $results["r2"]["UnusedMeals"]["UnusedMeal"] : [];
        $mas3 = (isset($results["r2"]["RdMeals"]["RdMeal"])) ? $results["r2"]["RdMeals"]["RdMeal"] : [];
        $mas4 = (isset($results["r2"]["TableRdMeals"]["TableRdMeal"])) ? $results["r2"]["TableRdMeals"]["TableRdMeal"] : [];

        $eticket_nrs_string = Input::get( 'etickets' );
        $eticket_nrs = explode(",",$eticket_nrs_string);

        if (count($eticket_nrs)==0)
            return Redirect::back()
                ->with( 'error', "Trūkst e-talonu numuru!" );

        foreach ($eticket_nrs as $eticket_nr)
        {
            $eticket_nr = trim($eticket_nr);

            // check for existing
            $q = Eticket::where('nr',$eticket_nr);
            $existing = $q->get();

            if ( ! isset($existing->id))
            {
                $school = School::findOrFail($results["r2"]["School"]["SchoolId"]);
                $objekts = Objekts::where("object_regnr",$school->reg_nr)->first();
                if ( ! isset ($objekts->id))
                {
                    Log::error("admin_addeticket - nevaru atrast objektu ar reg. nr. " . $school->reg_nr);
                }
                else $eticket_id = App::make('App\Http\Controllers\etickets\EticketsController')->save_new_eticket($eticket_nr, $pk, $results["r2"]["name"], '0000', $results["r2"]["School"]["SchoolClass"], 0, 2, $objekts->id, $school->id, $mas, $mas2, $mas3, $mas4);

                if ( ! isset($eticket_id))
                    return Redirect::back()
                        ->withInput()
                        ->with('error',"Kļūda, skatīt kļūdu LOGā");

                if ( intval($eticket_id) <= 0)
                    return Redirect::back()
                        ->withInput()
                        ->with('error',$eticket_id);
            }
        }

        App::make('BaseController')->add_pk_to_monitoring_list($pk);
        Session::forget('results');

        return Redirect::to('admin/'.$this->links.'/'.$eticket_id.'/edit')->with('success', 'Dati saglabāti,');
    }
     * 
     */

    public function getEdit($id)
    {
        $record = Eticket::findOrFail($id);

        if (!$record)
        {
            return Redirect::to('/admin/'.$this->links)
                ->withInput()
                ->with('error', 'Ieraksts nav atrasts!');
        }

        $objekti = $record->objekti->lists("object_name")->toArray();

        // Title
        $title = 'Labot bāzes ierakstu';

        // Show the page
        return View::make('admin/'.$this->links.'/edit', compact('title','record','objekti'));
    }

    public function postEdit()
    {
        $id = intval(Input::get( 'id' ));
        $pk = Input::get( 'pk' );
        $nr = Input::get( 'nr' );
        $name = Input::get( 'name' );
        $grade = strtoupper(Input::get( 'grade' ));
        $free_meals_from = Input::get( 'free_meals_from' );
        $free_meals_until = Input::get( 'free_meals_until' );
        $second_meals_from = Input::get( 'second_meals_from' );
        $second_meals_until = Input::get( 'second_meals_until' );
        $third_meals_from = Input::get( 'third_meals_from' );
        $third_meals_until = Input::get( 'third_meals_until' );
        $kopgalds_from = Input::get( '$kopgalds_from' );
        $kopgalds_until = Input::get( '$kopgalds_until' );

        if (strlen($nr)==8) $nr = '00'.$nr;

        if (($id<=0) OR ($pk=='') OR ($nr=='') OR (strlen($grade)<2))
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nekorekti dati!');
        }

        $record = Eticket::findOrFail($id);
        if (!$record)
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Ieraksts nav atrasts!');
        }

        $record->pk = $pk;
        $record->nr = $nr;
        $record->name = $name;
        $record->grade = $grade;
        $record->free_meals_from = $free_meals_from;
        $record->free_meals_until = $free_meals_until;
        $record->second_meals_from = $second_meals_from;
        $record->second_meals_until = $second_meals_until;
        $record->third_meals_from = $third_meals_from;
        $record->third_meals_until = $third_meals_until;
        $record->kopgalds_from = $kopgalds_from;
        $record->kopgalds_until = $kopgalds_until;
        if (!$record->save())
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Kļūda saglabājot - '.$record->getErrors());
        }

        return Redirect::back()->with('success', 'Dati saglabāti');
    }

}