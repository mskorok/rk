<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Eticket;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class TransferMoneyController extends AdminController
{

    protected $links = 'transfermoney';

    public function getIndex()
    {
        // Title
        $title = 'Atlikuma pārcelšana';

        // Show the page
        return View::make('admin/' . $this->links . '/index', compact('data', 'title'));
    }
    
    public function postTransfer()
    {
        $eticketFrom = Input::get('eticket_from');
        $eticketTo = Input::get('eticket_to');
        $summa = Input::get('summa');
        $summa = str_replace(',', '.', $summa) * 100;
        
        $eticketFrom = !empty($eticketFrom) ? explode(',', $eticketFrom)[0] : '';
        $eticketTo = !empty($eticketTo) ? explode(',', $eticketTo)[0] : '';

        $etFrom = Eticket::Nr($eticketFrom)->first();
        if (!isset($etFrom->id)) {
            return Redirect::to('admin/' . $this->links . '/index')
                ->withInput()
                ->with('error', 'Nav atrasts e-talons!');
        }
        $etTo = Eticket::Nr($eticketTo)->first();
        if (!isset($etTo->id)) {
            return Redirect::to('admin/' . $this->links . '/index')
                ->withInput()
                ->with('error', 'Nav atrasts e-talons!');
        }
        if($etFrom->account->owner_id != $etTo->account->owner_id){
            return Redirect::to('admin/' . $this->links . '/index')
                ->withInput()
                ->with('error', 'E-taloni pieder dažādiem lietotājiem!');            
        }
        if (empty($summa) || ($summa <= 0)) {
            return Redirect::to('admin/' . $this->links . '/index')
                ->withInput()
                ->with('error', 'Nekorekta summa!');
        }
        if ((float)$summa > (float)$etFrom->balance) {
            return Redirect::to('admin/' . $this->links . '/index')
                ->withInput()
                ->with('error', 'E-talona bilance ir mazāka, neka norādīta summa!');
        }

        try {
            DB::transaction(function() use($etFrom, $etTo, $summa) {
                // create TMP transaction
                $t = New Transaction;
                $t->amount = $summa;
                $t->operation_id = 1;
                $t->from_account_id = $etFrom->account->id;
                $t->to_account_id = $etTo->account->id;
                $t->from_eticket_nr = $etFrom->nr;
                $t->to_eticket_nr = $etTo->nr;
                $t->mpos_id = 0;
                $t->t_status = '';
                $t->confirmed = 1;

                if (!$t->save()){
                    throw New Exception();
                }

                $etFrom->balance -= $summa;
                $etTo->balance += $summa;
                        
                $accountFrom = $etFrom->account;
                $accountTo = $etTo->account;
                
                $accountFrom->balance -= $summa;
                $accountTo->balance += $summa;

                if (
                        !$etFrom->save() || 
                        !$etTo->save() ||
                        !$accountFrom->save() ||
                        !$accountTo->save()
                    ){
                    throw New Exception();
                }
            });
        } catch (Exception $e) {
            return Redirect::to('admin/' . $this->links . '/index')
                ->withInput()
                ->with('error', 'Kļūda saglabājot datus!');
        }

        return Redirect::to('admin/' . $this->links . '/index')
            ->with('success', 'Atlikuma pārcelšana veiksmīgi pabeigta.');
    }

    public function getFetch($q)
    {
        $roleIds = Auth::user()->currentRoleIds();

        if (array_search(1, $roleIds)=== false) {
            exit();
        }

        $result = Eticket::where('nr', 'LIKE', '%' . $q . '%')
            ->select('nr', 'name')->take(10)->get()->toArray();

        $data = array();
        foreach ($result as $row) {
            array_push($data, $row["nr"].', '.$row["name"]);
        }

        return response()->json($data);
    }    
}
