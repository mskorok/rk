<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\ObjectDotationPrices;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminDotationsController extends AdminController {

    protected $links = 'dotations';

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.management');

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $objekts
     * @return Response
     */
    public function getEdit($objekts)
    {
        if ( $objekts->id )
        {
            // Title
            $title = Lang::get('admin/'.$this->links.'/title.obj_edit');
            // mode
            $mode = 'edit';

            return View::make('admin/'.$this->links.'/create_edit', compact('objekts', 'title', 'mode'));
        }
        else return Redirect::back()->with('error', Lang::get('admin/'.$this->links.'/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postEdit($objekts)
    {
        $updateo = ObjectDotationPrices::find($objekts->id);

        // update
        $updateo->heading = Input::get( 'heading' );

        if ($updateo->save())
        {
            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')
                ->with('success', Lang::get('admin/'.$this->links.'/messages.edit.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')
                ->withInput()
                ->withErrors($updateo->getErrors());
        }

    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $data = ObjectDotationPrices:: leftjoin('objects', 'object_dotation_prices.object_id', '=', 'objects.id')
            ->select(array('object_dotation_prices.id','object_id','objects.object_name','heading','price','object_dotation_prices.updated_at'));

        return Datatables::of($data)

        ->edit_column('price','{!! sprintf("%.2f",$price/100) !!} EUR')
        ->add_column('actions', '<a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/edit\' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get(\'button.edit\') }}}</a>')

        ->remove_column('id')
        ->remove_column('object_id')

        ->make();
    }


}