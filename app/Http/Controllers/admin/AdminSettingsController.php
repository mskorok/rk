<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Eticket;
use App\Models\Settings;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminSettingsController extends AdminController {

    protected $links = 'settings';

    /**
     * User Model
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.management');

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $objekts
     * @return Response
     */
    public function getEdit($objekts)
    {
        if ( $objekts->id )
        {
            // Title
            $title = Lang::get('admin/'.$this->links.'/title.obj_edit');
            // mode
            $mode = 'edit';

            return View::make('admin/'.$this->links.'/create_edit', compact('objekts', 'title', 'mode'));
        }
        else
        {
            return Redirect::to('admin/'.$this->links)->with('error', Lang::get('admin/'.$this->links.'/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postEdit($objekts)
    {
        $updateo = Settings::find($objekts->id);

        // check confirmation type
        $current_type = $updateo->confirm_type;
        $new_type = Input::get( 'confirm_type' );

        if ($current_type!=$new_type)
        {
            $unfinished = Eticket::where('eticket_status','>=',5)->count();
            if ($unfinished>0)
            {
                return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')
                    ->withInput()
                    ->with('error','Nevar mainit tipu, ja ir neapstiprināti e-taloni!');
            }
        }

        // update
        $updateo->lang = Input::get( 'lang' );
        $updateo->banned_ips = Input::get( 'banned_ips' );
        $updateo->wrong_password = Input::get( 'wrong_password' );
        $updateo->import_path = Input::get( 'import_path' );
        $updateo->confirm_type = Input::get( 'confirm_type' );

        $updateo->jur_adrese = Input::get( 'jur_adrese' );
        $updateo->talrunis = Input::get( 'talrunis' );
        $updateo->banka = Input::get( 'banka' );
        $updateo->bankas_kods = Input::get( 'bankas_kods' );
        $updateo->konts = Input::get( 'konts' );
        $updateo->reg_nr = Input::get( 'reg_nr' );
        $updateo->name = Input::get( 'name' );

        $updateo->payment_form_message = Input::get( 'payment_form_message' );
        $updateo->payment_form_details = Input::get( 'payment_form_details' );

        $updateo->dokuments_text = Input::get( 'dokuments_text' );
        $updateo->yearly_text = Input::get( 'yearly_text' );
        $updateo->yearly_date = Input::get( 'yearly_date' );

        if ($updateo->save())
        {
            Cache::forget('banned_ip_array');
            Cache::forget('confirm_type');
            Cache::forget('settings');

            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')->with('success', Lang::get('admin/'.$this->links.'/messages.edit.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')
                ->withInput()
                ->withErrors($updateo->getErrors());
        }

    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $data = Settings::select(array('id','lang','banned_ips','wrong_password','import_path','confirm_type','jur_adrese','talrunis','banka','bankas_kods','konts','reg_nr','name', 'dokuments_text'));

        return Datatables::of($data)

        ->edit_column('jur_adrese','{{{$name}}}<br />{{{$jur_adrese}}}<br />{{{$talrunis}}}<br />{{{$banka}}}<br />{{{$bankas_kods}}}<br />{{{$konts}}}<br />{{{$reg_nr}}}')
        ->edit_column('confirm_type','
            @if ($confirm_type=="base_table")
                Automātiski no bāzes tabulas
            @elseif ($confirm_type !== "others_settings")
                Manuāli, 2 soļu process
            @endif')
        ->add_column('actions', '<a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/edit\' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get(\'button.edit\') }}}</a>')

        ->remove_column('id')
        ->remove_column('talrunis')
        ->remove_column('banka')
        ->remove_column('bankas_kods')
        ->remove_column('konts')
        ->remove_column('reg_nr')
        ->remove_column('name')

        ->make();
    }


}