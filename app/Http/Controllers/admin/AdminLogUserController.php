<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\LogUser;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\View;

class AdminLogUserController extends AdminController
{

    protected $links = 'loguser';

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = 'Lietotāju darbību žurnāls';
        //$data = [];
        $data = LogUser::leftjoin('users', 'log_user.user_id', '=', 'users.id')
            ->select(array('log_user.id', 'users.username', 'op', 'ip', 'log_user.created_at', 'value'))
            ->limit(1000)
            ->get()
            ->toArray();
        // Show the page
        return View::make('admin/' . $this->links . '/index', compact('data', 'title'));
    }

    /**
     * Show a list of all the data formatted for Datatables.
     *
     * @return Datatables JSON
     */
    /*
    public function getData()
    {
        $data = LogUser::leftjoin('users', 'log_user.user_id', '=', 'users.id')
            ->select(array('log_user.id','users.username','op','ip','log_user.created_at','value'));

        return Datatables::of($data)
            ->remove_column('id')
            ->make();
    }
     * 
     */

}
