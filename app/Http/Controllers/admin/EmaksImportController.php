<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Eticket;
use App\Models\Owner;
use App\Models\OwnerAccess;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class EmaksImportController extends AdminController
{

    public function index()
    {
//        $new_object_id = 1;
//        $skola_id = 605;

        $new_object_id = 122;
        $skola_id = 4888;

        $user_res = DB::connection('emaks_mysql')->select('select * from users where id >= 9 AND deleted_at IS NULL AND confirmed = 1 AND login_count > 0 AND terms = 1');
        foreach ($user_res as $row_user)
        {
            // check if user exists
            $exists = User::where("email",$row_user->email)->count();

            if ($exists > 0) continue;

            // check all in emaks DB
            $user_roles = DB::connection('emaks_mysql')->select('select count(*) as sk from assigned_roles where user_id = ?',[$row_user->id]);

            if ($user_roles[0]->sk > 0) continue;

            $owner_access = DB::connection('emaks_mysql')->select('select * from owner_access where user_id = ? AND access_status = 1 AND deleted_at IS NULL',[$row_user->id]);

            if ( ! isset($owner_access[0])) continue;

            $owner_id = $owner_access[0]->owner_id;

            $owner = DB::connection('emaks_mysql')->select('select * from owners where id = ?',[$owner_id])[0];

            $account_access = DB::connection('emaks_mysql')->select('select * from account_access where user_id = ? AND access_status = 1 AND deleted_at IS NULL',[$row_user->id])[0];
            $account_id = $account_access->account_id;

            $account = DB::connection('emaks_mysql')->select('select * from accounts where id = ?',[$account_id])[0];

            $eticket_accounts = DB::connection('emaks_mysql')->select('select * from accounts where id != ? AND account_type = 2 AND owner_id = ?',[$account_id,$owner_id]);

            $old_etickets = [];
            if (count($eticket_accounts) > 0)
            {
                foreach ($eticket_accounts as $et_account)
                {
                    $eticket = DB::connection('emaks_mysql')->select('select * from etickets where account_id = ?',[$et_account->id]);
                    if (isset($eticket[0]))
                        $old_etickets[] = $eticket[0];
                }
            }

            // create new user
            $user = new User();
            $user->password = $row_user->password;
            $user->password_confirmation = $row_user->password;
            $user->username = $row_user->username;
            $user->surname = $row_user->surname;
            $user->email = $row_user->email;
            $user->phone = $row_user->phone;
            $user->confirmed = 1;
            $user->terms = 2;
            if ( ! $user->save())
            {
                dd($user->getErrors());
            }
            else {

                // set old password
                DB::update('update users set password = ? where id = ? LIMIT 1', array($row_user->password, $user->id));

                // create account and owner
                try {

                    DB::transaction(function () use ($user, $old_etickets, $new_object_id, $skola_id)
                    {

                        // create owner
                        $own = new Owner;
                        $own->owner_type=1;
                        $own->owner_name = $user->username.' '.$user->surname;
                        $own->save();

                        if ( $own->id )
                        {

                            // create owner access
                            $oacc = new OwnerAccess;
                            $oacc->user_id = $user->id;
                            $oacc->owner_id = $own->id;
                            $oacc->access_status = 1;
                            $oacc->access_level = 1;
                            $oacc->save();

                            if ($oacc->id)
                            {

                                // create account
                                $acc = New Account;
                                $acc->owner_id = $own->id;
                                $acc->account_type=1;
                                $acc->account_status=1;
                                $acc->save();

                                if ($acc->id)
                                {
                                    // create account access
                                    $access = New AccountAccess;
                                    $access->user_id = $user->id;
                                    $access->account_id = $acc->id;
                                    $access->access_status=1;
                                    $access->access_level=1;
                                    $access->save();

                                    if ($access->id)
                                    {
                                        // create etickets
                                        foreach ($old_etickets as $old_eticket)
                                        {
                                            $mas = ["DateFrom" => "0000-00-00", "DateTill" => "0000-00-00"];
                                            $eticket_id = App::make('App\Http\Controllers\etickets\EticketsController')->save_new_eticket(
                                                $old_eticket->nr, $old_eticket->pk, $old_eticket->name, $old_eticket->pin, $old_eticket->grade,
                                                $old_eticket->limit_type, $own->id, $new_object_id, $skola_id, $mas);

                                            if ( ! is_int($eticket_id)) throw New Exception($eticket_id.'5');

                                            // update some eticket fields
                                            $eticket = Eticket::findOrFail($eticket_id);
                                            $eticket->limit_at = $old_eticket->limit_at;
                                            $eticket->limit = $old_eticket->limit;
                                            $eticket->turnover = $old_eticket->turnover;
                                            if ( ! $eticket->forceSave()) throw New Exception($eticket->getErrors().'6');
                                        }
                                    }
                                    else throw New Exception($access->getErrors().'1');
                                }
                                else throw New Exception($acc->getErrors().'2');
                            }
                            else throw New Exception($oacc->getErrors().'3');
                        }
                        else throw New Exception($own->getErrors().'4');

                    });
                }
                catch(Exception $e) {

                    $user->delete();

                    dd($e->getMessage());
                }

            }

            //dd($row_user);
        }

    }

}