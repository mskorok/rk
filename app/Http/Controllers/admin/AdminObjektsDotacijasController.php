<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Objekts;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminObjektsDotacijasController extends AdminController {

    protected $links = 'objektudotacijas';

    public function index()
    {
        // Title
        $title = 'Objektu dotāciju ģenerēšana';

        $vakardiena = Carbon::yesterday();
        $sodiena = Carbon::today();

        $data = Objekts::with("dotationHistoryTwoDays")
            ->select('id','object_name','object_regnr')
            ->get();

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('data', 'title', 'vakardiena', 'sodiena'));
    }

    public function regen($object_id)
    {
        $object_id = intval($object_id);
        if ($object_id <= 0)
        {
            return Redirect::back()->withError("Norādīts nekorekts objekts!");
        }

        $exitCode = Artisan::call('updatedot', [
            'obj' => $object_id,
            'force' => 1,
        ]);

        if ($exitCode > 0)
        {
            return Redirect::back()->withError("Kļūda ģenerējot datus! Sīkāk skatīt kļūdu žurnālā.");
        }

        return Redirect::back()->withSuccess("Dati veiksmīgi pārģenerēti.");
    }

}