<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Eticket;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;

class AdminEticketsController extends AdminController
{

    protected $links = 'etickets';

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/' . $this->links . '/title.management');

        // Show the page
        return View::make('admin/' . $this->links . '/index', compact('title'));
    }

    /**
     * Show a list of all data for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        //$all_objects = Objekts::lists("object_name","id");

        $data = Eticket::leftjoin('eticket_limit_types', 'etickets.limit_type', '=', 'eticket_limit_types.id')->
            leftjoin('accounts', 'etickets.account_id', '=', 'accounts.id')->
            leftjoin('owners', 'accounts.owner_id', '=', 'owners.id')->
            leftjoin('eticket_objects', 'etickets.id', '=', 'eticket_objects.eticket_id')->
            leftjoin('objects', 'objects.id', '=', 'eticket_objects.object_id')->
            select([
                'etickets.nr', 
                'owners.owner_name', 
                'etickets.name as child_name', 
                'etickets.eticket_status', 
                'etickets.pin', 
                'etickets.balance',
                'eticket_limit_types.name as tname', 
                'etickets.limit', 
                'etickets.free_meals_from', 
                'etickets.free_meals_until', 
                DB::raw("GROUP_CONCAT(objects.object_name ORDER BY objects.object_name DESC SEPARATOR '<br/>') as objekti"), 
                'etickets.created_at'                
            ])
            ->groupBy("etickets.id");

        return Datatables::of($data)
            ->edit_column('eticket_status', '@if ($eticket_status==1)
                    Aktīvs
                @else
                    Neaktīvs
                @endif')
            ->edit_column('balance', '{!! sprintf("%.2f",$balance/100) !!} EUR')
            ->edit_column('limit', '{!! sprintf("%.2f",$limit/100) !!} EUR')
            ->edit_column('owner_name', '{!! $owner_name !!}<br /> {!! $child_name !!}')
            ->edit_column('free_meals_from', '@if ($free_meals_from!="0000-00-00") Brīvpusdienas<br/>{!! $free_meals_from !!} -<br /> {!! $free_meals_until !!} @else - @endif')
            ->remove_column('child_name')
            ->remove_column('free_meals_until')
            ->make();
    }

}
