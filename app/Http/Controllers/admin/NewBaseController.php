<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\EticketsBase;
use App\Models\Objekts;
use App\Models\School;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

class NewBaseController extends AdminController {

    protected $links = 'newbase';

    public function getIndex()
    {
        $objects = Objekts::all();

        // Title
        $title = 'Meklēt bāzes ierakstu';

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('title','objects'));
    }

    /**
     * Manual load data from file
     *
     * @return Response
     */
    public function postIndex()
    {
        $pk = Input::get( 'pk' );
        $eticket = Input::get( 'eticket' );

        if (trim($pk)=='' AND trim($eticket)=='')
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nav nekas ievadīts!');
        }

        if ((strlen($eticket)>10) AND (strlen($eticket)<8) AND (strlen($pk)!=12))
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nekorekti dati!');
        }

        if (trim($pk)!='' AND trim($eticket)!='')
        {
            Session::flash('warning', 'Ja aizpildīti abi lauki, meklēts tiks pēc pirmā!');
        }

        if ($pk!='')
            $record = EticketsBase::where('pk',$pk)->first();
        else
            $record = EticketsBase::where('etickets','like','%'.$eticket.'%')->first();

        if (($record) AND ($record->count()>0))
            return Redirect::to('admin/'.$this->links.'/'.$record->id.'/edit/');
        else
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nav atrasts ieraksts!');
    }

    public function getCreate()
    {
        $skolas = School::orderBy("heading","ASC")->lists("heading","id")->toArray();

        // Title
        $title = 'Izveidot bāzes ierakstu';

        // Show the page
        return View::make('admin/'.$this->links.'/create', compact('title','skolas'));
    }

    public function postCreate()
    {
        $pk = Input::get( 'pk' );
        $eticket = Input::get( 'etickets' );
        $name = Input::get( 'name' );
        $surname = Input::get( 'surname' );
        $grade = strtoupper(Input::get( 'grade' ));
        $school_id = intval(Input::get( 'skola' ));

        if (strlen($eticket)==8) $eticket = '00'.$eticket;

        if (($pk=='') OR ($eticket=='') OR (strlen($grade)<2) OR ($school_id<=0))
        {
            return Redirect::to('admin/newbase/create')
                ->withInput()
                ->with('error', 'Nekorekti dati!');
        }

        // check for existing
        $q = EticketsBase::where('pk',$pk);
        $q->where('etickets','like','%'.$eticket.'%');
        $record = $q->get();

        if ($record->count()>0)
            return Redirect::to('admin/newbase/create')
                ->withInput()
                ->with('error', 'Ieraksts ar šādu personas kodu vai e-talonu jau eksitē!');

        $record  = New EticketsBase;
        $record->pk = $pk;
        $record->etickets = $eticket;
        $record->name = $name;
        $record->surname = $surname;
        $record->grade = $grade;
        $record->school_id = $school_id;
        $record->custom = 1;
        $record->brivpusdienas = 1;
        if (!$record->save())
        {
            return Redirect::to('admin/newbase/create')
                ->withInput()
                ->with('error', 'Kļūda saglabājot - '.$record->getErrors());
        }

        return Redirect::to('admin/newbase/'.$record->id.'/edit')->with('success', 'Dati saglabāti.');
    }

    public function getEdit($id)
    {

        $record = EticketsBase::Id($id)->Confirmed()->first();

        if (!$record)
        {
            return Redirect::to('/admin/newbase')
                ->withInput()
                ->with('error', 'Ieraksts nav atrasts!');
        }

        $skolas = School::orderBy("heading","ASC")->lists("heading","id")->toArray();

        // Title
        $title = 'Labot bāzes ierakstu';

        // Show the page
        return View::make('admin/'.$this->links.'/edit', compact('title','record','skolas'));
    }

    public function postEdit()
    {
        $id = intval(Input::get( 'id' ));
        $pk = Input::get( 'pk' );
        $etickets = Input::get( 'etickets' );
        $name = Input::get( 'name' );
        $surname = Input::get( 'surname' );
        $grade = strtoupper(Input::get( 'grade' ));
        $school_id = intval(Input::get( 'skola' ));

        if (strlen($etickets)==8) $etickets = '00'.$etickets;

        if (($id<=0) OR ($pk=='') OR ($etickets=='') OR (strlen($grade)<2) OR ($school_id<=0))
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Nekorekti dati!');
        }

        $record = EticketsBase::Id($id)->Confirmed()->first();
        if (!$record)
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Ieraksts nav atrasts!');
        }

        $record->pk = $pk;
        $record->etickets = $etickets;
        $record->name = $name;
        $record->surname = $surname;
        $record->grade = $grade;
        $record->school_id = $school_id;
        $record->custom = 1;
        $record->brivpusdienas = 1;
        if (!$record->save())
        {
            return Redirect::back()
                ->withInput()
                ->with('error', 'Kļūda saglabājot - '.$record->getErrors());
        }

        return Redirect::back()->with('success', 'Dati saglabāti');
    }

}