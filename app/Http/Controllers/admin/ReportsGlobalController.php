<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;
use App\Models\Transaction;
use DateTime;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class ReportsGlobalController extends BaseController {

    protected $links = 'global';

	public function getIndex()
	{
        // get selected search fields
        $no = trim(Input::get('no'));
        $lidz = trim(Input::get('lidz'));

        if ($no!='')
        {
            $no2 = date( 'Y-m-d', strtotime($no));
        }
        else $no2='';

        if ($lidz!='')
        {
            $lidz2 = new DateTime($lidz);
            $lidz2->setTime(23,59,59);
            $lidz2 = $lidz2->format('Y-m-d H:i:s');
        }
        else $lidz2='';

        $d = Transaction::with(array('from_account','to_account'));        
        $d->where(function($query)
        {
            $query->where('from_account_id',0);
            $query->orWhere('to_account_id','=',999999);
        });
        $d->where(function($query) use ($no2,$lidz2)
        {
            if ($no2!='') 
            {
                $query->where('created_at' ,'>=',$no2);
            }

            if ($lidz2!='') 
            {
                $query->where('created_at' ,'<=',$lidz2);
            }
        });
        
        $data_totals = $d->get();

        $data = $d->paginate(50);

        // calculate totals
        $totals["in"] = 0;
        $totals["out"] = 0;
        foreach ($data_totals as $key => $single)
        {
            if ($single->from_account_id == 0)
            {
                $totals["in"] = $totals["in"] + $single->amount;
            }
            else if ($single->to_account_id == 999999)
            {
                $totals["out"] = $totals["out"] + $single->amount;
            }
        }

        // Title
        $title = 'Ienākošo un izejošo maksājumu pārskats';

        // Show the page
        $formview = View::make('reports.partials.datesubmit',compact('no','lidz'));
        return View::make('admin/reports/'.$this->links.'/index', compact('title','data','no','lidz','totals'))->nest('dateintervalssubmit', 'reports.partials.datesubmit')
            ->with('dateintervalssubmit',$formview);
	}

}