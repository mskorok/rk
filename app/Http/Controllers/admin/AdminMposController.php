<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Account;
use App\Models\Mpos;
use App\Models\Objekts;
use App\Models\Owner;
use App\Models\Transaction;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminMposController extends AdminController {

    protected $links = 'mpos';

    /**
     * User Model
     * @var User
     */
    protected $user;

    protected $objekti;
    protected $owners;

    public function __construct(User $user, Objekts $objekti, Owner $owners)
    {
        parent::__construct();
        $this->user = $user;
        $this->objekti = $objekti;
        $this->owners = $owners;
    }

    /**
     * Display a listing of MPOS devices
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/mpos/title.mpos_management');

        // Show the page
        return View::make('admin/mpos/index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $objekti = $this->objekti->all();
        $sellers = $this->owners->Sell()->get();

        // Title
        $title = Lang::get('admin/mpos/title.create_a_new_mpos');

        // no transaction yet
        $t = 0;

        // Mode
        $mode = 'create';

        // Show the page
        return View::make('admin/mpos/create_edit', compact('objekti','sellers','title', 'mode','t'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(Request $request)
    {
        try
        {
            $mpos = New Mpos($request->except("\\"));
            if (!$mpos->save()) throw new \Exception($mpos->getErrors(),1);

            // create account
            $acc = New Account;
            $acc->owner_id = $mpos->id;
            $acc->account_type = 6; // mpos
            $acc->account_status = 1;
            if (!$acc->save()) throw new \Exception('Kļūda izveidojot kontu!',2);

            $mpos->account_id = $acc->id;
            $mpos->mpos_status = ' ';
            $mpos->mpos_serial = $this->geerate_new_serial();
            if (!$mpos->save()) throw new \Exception('Kļūda saglabājot papilddatus!',2);

        }
        catch (\Exception $e)
        {
            if ($e->getCode()==1)
            {
                return Redirect::to('admin/'.$this->links.'/create')->withInput()->withErrors($mpos->getErrors());
            }
            else {
                $mpos->delete();
                if (isset($acc)) $acc->delete();
                return Redirect::to('admin/'.$this->links.'/create')->withInput()->with('error', $e->getMessage());
            }
            
        }

        Cache::forget('all_mpos');
        Cache::forget('seller_mpos_accounts_'.$mpos->owner_id);
        Cache::forget('seller_objects_'.$mpos->owner_id);

        return Redirect::to('admin/mpos/' . $mpos->id . '/edit')->with('success', Lang::get('admin/mpos/messages.create.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $mpos
     * @return Response
     */
    public function getEdit($mpos)
    {
        if ( $mpos->id )
        {
            $objekti = $this->objekti->all();
            $sellers = $this->owners->Sell()->get();

            // check if no transactions yet
            $t = Transaction::Acc($mpos->account_id)->Ok()->count();

            // Title
            $title = Lang::get('admin/mpos/title.mpos_edit');
            // mode
            $mode = 'edit';

            return View::make('admin/mpos/create_edit', compact('objekti','sellers','mpos', 'title', 'mode','t'));
        }
        else
        {
            return Redirect::to('admin/mpos')->with('error', Lang::get('admin/mpos/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $mpos
     * @return Response
     */
    public function postEdit($mpos)
    {
        $updatempos = Mpos::find($mpos->id);

        Cache::forget('seller_mpos_accounts_'.$updatempos->owner_id);
        Cache::forget('seller_objects_'.$updatempos->owner_id);

        // check if no transactions yet
        $t = Transaction::Acc($mpos->account_id)->Ok()->count();

        // update
        $updatempos->mpos_type = Input::get( 'mpos_type' );
        $updatempos->mpos_name = Input::get( 'mpos_name' );
        $updatempos->mpos_serial = Input::get( 'mpos_serial' );
        $updatempos->operation_id = Input::get( 'operation_id' );
        $updatempos->firmware_version = Input::get( 'firmware_version' );
        $updatempos->secret_key = Input::get( 'secret_key' );

        if ($updatempos->mpos_status == '')
            $updatempos->mpos_status = ' ';

        if ($updatempos->mpos_serial == 'NEWMPOS')
            $updatempos->mpos_serial = $this->geerate_new_serial();

        if ($t==0)
        {
            $updatempos->object_id = Input::get( 'object_id' );
            $updatempos->owner_id = Input::get( 'owner_id' );
        }

        if ($updatempos->save())
        {
            Cache::forget('all_mpos');
            Cache::forget('seller_mpos_accounts_'.$updatempos->owner_id);
            Cache::forget('seller_objects_'.$updatempos->owner_id);
            Cache::forget('mpos_'.$updatempos->id);

            return Redirect::to('admin/mpos/' . $mpos->id . '/edit')->with('success', Lang::get('admin/mpos/messages.edit.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/mpos/' . $mpos->id . '/edit')->withInput()->withErrors($updatempos->getErrors());
        }

    }

    /**
     * Remove resource page.
     *
     * @param $mpos
     * @return Response
     */
    public function getDelete($mpos)
    {
        // Title
        $title = Lang::get('admin/mpos/title.mpos_delete').' - '.$mpos->mpos_name;

        // Show the page
        return View::make('admin/mpos/delete', compact('mpos', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $mpos
     * @return Response
     */
    public function postDelete($mpos)
    {
        $id = $mpos->id;
        $account_id = $mpos->account_id;  
        $owner_id = $mpos->owner_id;

        // check if no transactions yet
        $t = Transaction::Acc($mpos->account_id)->Ok()->count();
        if ($t>0)
        {
            return Redirect::to('admin/mpos')->with('error', 'Iekārtu nevar dzēst, ar šo iekārtu jau veiktas transakcijas!');
        }

        if ($account_id>0)
        {
            $acc = Account::findOrFail($account_id);
            $acc->account_status = 0;
            if (!$acc->save()) return Redirect::to('admin/mpos')->with('error', 'Neizdevās deaktivizēt kontu!');
        }

        $mpos->delete();

        // Was the comment post deleted?
        $mpos = Mpos::find($id);
        if ( empty($mpos) )
        {
            Cache::forget('all_mpos');
            Cache::forget('seller_mpos_accounts_'.$owner_id);
            Cache::forget('seller_objects_'.$owner_id);
            Cache::forget('mpos_'.$id);
            
            return Redirect::to('admin/mpos')->with('success', Lang::get('admin/mpos/messages.delete.success'));
        }
        else
        {
            return Redirect::to('admin/mpos')->with('error', Lang::get('admin/mpos/messages.delete.error'));
        }
    }

    /**
     * Restart MPOS
     *
     * @param $mpos
     * @return Response
     */
    public function getRestart($mpos)
    {
        if ($mpos->id)
        {
            $mpos->reboot_flag = 1;
            if (!$mpos->save()) return Redirect::to('admin/mpos')->with('error', Lang::get('admin/mpos/messages.restart.error'));

            return Redirect::to('admin/mpos')->with('success', Lang::get('admin/mpos/messages.restart.success'));
        }
    }

    /**
     * Save MPOS configuartion to file
     *
     * @param $mpos
     * @return Response
     */
    public function getConf($mpos)
    {
        if ($mpos->id)
        {
            $path= '/';

            $myFile = $mpos->id.".cfg";
            $fh = @fopen($path.$myFile, 'w');
            if (!$fh) return Redirect::to('admin/mpos')->with('error', Lang::get('admin/mpos/messages.conf.error'));

            $stringData = $mpos->mpos_name;
            fwrite($fh, $stringData);
            fclose($fh);

            return Redirect::to('admin/mpos')->with('success', Lang::get('admin/mpos/messages.conf.success'));
        }
    }

    /**
     * Uplaod firmware
     *
     * @param $mpos
     * @return Response
     */
    public function getFw($mpos)
    {
        // Title
        $title = Lang::get('admin/mpos/title.mpos_firmware');

        // Show the page
        return View::make('admin/mpos/fw', compact('mpos', 'title'));
    }

    /**
     * Save firmware
     *
     * @param $mpos
     * @return Response
     */
    public function postFw($mpos)
    {
        if ($mpos->id)
        {
            $path= '/htdocs/ewallet';

            $myFile = $mpos->id.".dat";

            $fails              = Input::file('fails');
            $firmware_version   = Input::get('fw_version');

            try {
                $uploadSuccess = Input::file('fails')->move($path, $myFile);
            }
            catch (Exception $e)
            {
                return Redirect::to('admin/mpos')->with('error', Lang::get('admin/mpos/messages.fw.error'));
            }

            if($uploadSuccess) {
                $mpos->firmware_version = $firmware_version;
                if (!$mpos->save())
                {
                    return Redirect::to('admin/mpos')->with('error', Lang::get('admin/mpos/messages.fw.error'));
                }

                return Redirect::to('admin/mpos')->with('success', Lang::get('admin/mpos/messages.fw.success'));
            }
            else {
                return Redirect::to('admin/mpos')->with('error', Lang::get('admin/mpos/messages.fw.error'));
            }
        }
    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $comments = Mpos::leftjoin('objects', 'mpos.object_id', '=', 'objects.id')->
        leftjoin('owners', 'mpos.owner_id','=', 'owners.id')->
        select(array('mpos.id','mpos.mpos_name','mpos_type','objects.object_name','owners.owner_name','last_online','firmware_version','mpos_serial','secret_key','mpos.created_at','reboot_flag'));

        return Datatables::of($comments)

        ->edit_column('mpos_type','@if($mpos_type==1)
                            Standarta
                        @else
                            Cits
                        @endif')

        ->edit_column('last_online','@if($reboot_flag==1)
                            {!! $last_online !!} <br /><b>GOING TO REBOOT NOW</b>
                        @else
                            {!! $last_online !!}
                        @endif')

        ->edit_column('firmware_version','{!! $firmware_version !!} <br /><a href="{{{ URL::to(\'admin/mpos/\' . $id . \'/fw\' ) }}}" class="iframe btn btn-xs btn-primary">{{{ Lang::get(\'button.upload_firmware\') }}}</a>')

        ->add_column('actions', '<a href="{{{ URL::to(\'admin/mpos/\' . $id . \'/edit\' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/mpos/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
                <a href="{{{ URL::to(\'admin/mpos/\' . $id . \'/restart\' ) }}}" class="btn btn-xs btn-primary">{{{ Lang::get(\'button.restart\') }}}</a>
                <a href="{{{ URL::to(\'admin/mpos/\' . $id . \'/conf\' ) }}}" class="btn btn-xs btn-primary">{{{ Lang::get(\'button.upload_conf\') }}}</a>
            ')

        ->remove_column('id')
        ->remove_column('reboot_flag')

        ->make();
    }

    private function geerate_new_serial()
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $serial = substr(str_shuffle($chars),0,10);

        // check
        $exists = Mpos::where("mpos_serial",$serial)->first();
        if (isset($exists->id))
            $serial = substr(str_shuffle($chars),0,10);

        return $serial;
    }


}