<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Models\Eticket;
use App\Models\Settings;
use App\Models\TopSlider;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminSliderController extends AdminController
{

    protected $links = 'top_slider';

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Initializer.
     *
     * @access   public
     * @param User $user
     * @throws \Exception
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Display a listing of resources
     *
     * @return \Illuminate\Contracts\View\View |View
     */
    public function getIndex()
    {
        // Title
        $title = trans('admin/'.$this->links.'/title.management');

        // Show the page
        return view('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @param TopSlider $slider
     * @return \Illuminate\Contracts\View\View|RedirectResponse|View
     */
    public function getEdit(Request $request, TopSlider $slider)
    {
        if ($slider instanceof TopSlider) {
            $title = trans('admin/'.$this->links.'/title.obj_edit');
            $old = $request->old();
            $action = route('slider_edit_post', ['banner' => $slider->id]);
            return view('admin/'.$this->links.'/edit', compact('slider', 'title', 'old', 'action'));
        } else {
            return redirect()->to('admin/'.$this->links)
                ->with('error', trans('admin/'.$this->links.'/messages.does_not_exist'));
        }
    }

    /**
     *
     * @param Request $request
     * @param TopSlider $slider
     * @return \Illuminate\Contracts\View\View | RedirectResponse
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function postEdit(Request $request, TopSlider $slider)
    {
        if ($slider instanceof TopSlider) {
            $slider->fill($request->input());
            if ($slider->save()) {
                return redirect()->to('/admin/top_slider/')
                    ->with('success', trans('admin/'.$this->links.'/messages.edit.success'));
            } else {
                // Redirect on failure
                return redirect()->to('admin/'.$this->links.'/' . $slider->id . '/edit')
                    ->withInput()
                    ->withErrors($slider->getErrors());
            }
        }
        return redirect()->to('/admin/top_slider/');
    }

    /**
     * @return View
     */
    public function getCreate()
    {
        $title = trans('admin/'.$this->links.'/title.obj_create');
        $action = route('slider_create_post');
        return view('admin/'.$this->links.'/create', compact('title', 'action'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function postCreate(Request $request)
    {
        $slider = new TopSlider();
        $slider->fill($request->input());
        if ($slider->save()) {
            return redirect()->to('/admin/top_slider/')
                ->with('success', trans('admin/'.$this->links.'/messages.create.success'));
        } else {
            // Redirect on failure
            return \Redirect::to('admin/'.$this->links.'/' . $slider->id . '/edit')
                ->withInput()
                ->withErrors($slider->getErrors());
        }
    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        /** @var \Illuminate\Database\Query\Builder $data */
        $data = TopSlider::select(
            [
                'id',
                'name',
                'sort',
                'active',
                'exposition_time_from',
                'exposition_time_to'
            ]
        );

        return Datatables::of($data)
            ->add_column('actions', '<a href="{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/edit\' ) }}" class="btn btn-sm btn-primary">{{ Lang::get(\'button.edit\') }}</a>')
            ->remove_column('id')
            ->make();
    }
}
