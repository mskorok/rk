<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\AdminController;

use App\Models\AssignedRoles;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminUsersController extends AdminController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission)
    {
        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->roles = [];
        $this->user_assigned_roles = [];
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/users/title.user_management');

        // Grab all the users
        $users = $this->user;

        // Show the page
        return View::make('admin/users/index', compact('users', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // All roles
        $roles = $this->role->all();

        // Get all the available permissions
        $permissions = $this->permission->all();

        // Selected groups
        $selectedRoles = Input::old('roles', array());

        // Selected permissions
        $selectedPermissions = Input::old('permissions', array());

		// Title
		$title = Lang::get('admin/users/title.create_a_new_user');

		// Mode
		$mode = 'create';

        $html = '';

		// Show the page
		return View::make('admin/users/create_edit', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode','html'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        $this->user->username = Input::get( 'username' );
        $this->user->surname = Input::get( 'surname' );
        $this->user->email = Input::get( 'email' );
        $this->user->password = bcrypt(Input::get( 'password' ));
        $this->user->password_confirmation = Input::get( 'password_confirmation' );

        $this->user->confirmed = Input::get( 'confirm' );

        $this->user->last_login = date("Y-m-d");

        // Save if valid. Password field will be hashed before save
        $this->user->save();

        if ( $this->user->id )
        {
            if (intval(Input::get( 'role' )) > 0)
                $this->user->roles()->sync([intval(Input::get( 'role' ))]);

            // Redirect to the new user page
            return Redirect::to('admin/users/' . $this->user->id . '/edit')->with('success', Lang::get('admin/users/messages.create.success'));
        }
        else
        {
            // Get validation errors (see Ardent package)
            $error = $this->user->errors()->all();

            return Redirect::to('admin/users/create')
                ->withInput(Input::except('password'))
                ->with( 'error', $error );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getShow($user)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($user)
    {
        if ( $user->id )
        {
            $roles = $this->role->all();

            // Title
        	$title = Lang::get('admin/users/title.user_update');
        	// mode
        	$mode = 'edit';

        	return View::make('admin/users/create_edit', compact('user', 'roles', 'title', 'mode'));
        }
        else
        {
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit($user, Request $request)
    {
        if ( ! $user->isValid())
        {
            return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/users/messages.edit.failure2'))->with('error', $user->getErrors());
        }

        $user->username = $request->get( 'username' );
        $user->surname = $request->get( 'surname' );
        $user->email = $request->get( 'email' );
        $user->confirmed = $request->get( 'confirm' );
        $user->phone = $request->get( 'phone' );

        if ($user->id === Auth::user()->id)
            $user->confirmed = 1;

        $password = $request->get( 'password' );
        $passwordConfirmation = $request->get( 'password_confirmation' );

        if( ! empty($password))
        {
            if($password !== $passwordConfirmation)
            {
                return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/users/messages.password_does_not_match'));
            }

            $user->password = bcrypt($password);
            // The password confirmation will be removed from model
            // before saving. This field will be used in Ardent's
            // auto validation.
            $user->password_confirmation = $passwordConfirmation;
        }

        // update role
        if (intval($request->get( 'role' )) > 0)
            $user->roles()->sync([intval($request->get( 'role' ))]);

        if ( ! $user->save())
        {
            return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/users/messages.edit.failure'))->with('error', $user->getErrors());
        }

        Cache::forget('roleids_'.$user->id);

        return Redirect::to('admin/users/' . $user->id . '/edit')->with('success', Lang::get('admin/users/messages.edit.success'));
    }

    /**
     * Remove user page.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($user)
    {
        // Title
        $title = Lang::get('admin/users/title.user_delete').' - '.$user->username.' '.$user->surname;

        // Show the page
        return View::make('admin/users/delete', compact('user', 'title'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete($user)
    {
        // Check if we are not trying to delete ourselves
        if ($user->id === Auth::user()->id)
        {
            // Redirect to the user management page
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.delete.impossible'));
        }

        AssignedRoles::where('user_id', $user->id)->delete();

        $id = $user->id;
        $user->delete();

        $user = User::where("id",$id)->first();
        if ( empty($user) )
        {
            return Redirect::to('admin/users')->with('success', Lang::get('admin/users/messages.delete.success'));
        }
        else return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.delete.error'));
    }

    /**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $this->roles = Role::all()->keyBy('id');
        $results = DB::select('select distinct(user_id),role_id from role_user');
        foreach ($results as $row)
        {
            $this->user_assigned_roles[$row->user_id] = isset($this->roles[$row->role_id]) ? $this->roles[$row->role_id]->name : '';
        }

        $users = User::select(array('users.id', 'users.username','users.surname','users.email','users.phone', 'users.id as place' , 'users.confirmed', 'users.created_at', 'users.last_login', 'users.login_count'));

        return Datatables::of($users)

        ->edit_column('confirmed','@if($confirmed)
                            Jā
                        @else
                            Nē
                        @endif')

        ->edit_column('last_login','{{{$last_login}}} ({{{$login_count}}}x)')
        ->edit_column('place',function($row) {
            return isset($this->user_assigned_roles[$row->id]) ? $this->user_assigned_roles[$row->id] : '';
        })

        ->add_column('actions', '<a href="{{{ URL::to(\'admin/users/\' . $id . \'/edit\' ) }}}" class="btn btn-xs btn-primary">{{{ Lang::get(\'button.edit\') }}}</a>
                                @if($username == \'admin\')
                                @else
                                    <a href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
                                @endif')

        ->remove_column('id')
        ->remove_column('login_count')

        ->make();
    }
}
