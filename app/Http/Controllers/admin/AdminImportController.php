<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\EticketsBase;
use App\Models\Settings;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminImportController extends AdminController {

    protected $links = 'import';

    /**
     * User Model
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
        exit();
    }

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.management');

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('title'));
    }

    /**
     * Manual relaod data from file
     *
     * @return Response
     */
    public function getReload()
    {
        $settings = Settings::all()->first();

        $filename = $settings->import_path;

        if (file_exists($filename)) 
        {
            $file_handle = fopen($filename, 'r');
            while (!feof($file_handle) ) {
                $line[] = fgetcsv($file_handle, 1024,"=");
            }
            fclose($file_handle);   

            // clean table
            EticketsBase::NC()->delete();
            
            // insert new data
            foreach ($line as $l)
            {

                $l_ex = explode(",",$l[1]);
                foreach ($l_ex as &$value)
                    $value = str_pad($value, 10, '0', STR_PAD_LEFT); 
                $l_ex = implode(",",$l_ex);

                $b = New EticketsBase;
                $b->pk = $l[0];
                $b->etickets = $l_ex;
                $b->save();
            }

            return Redirect::to('admin/'.$this->links)->with('success', 'Imports pabeigts.');
        } 
        else {
            return Redirect::to('admin/'.$this->links)->with('error', 'Fails nav atrasts vai pieejams!');
        }

        
    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $data = EticketsBase::select(array('id','pk','etickets','custom'));

        return Datatables::of($data)

        ->remove_column('id')
        ->edit_column('custom','@if ($custom==1) 
                                    Manuāli izveidots 
                                @else
                                    Importēts
                                @endif')

        ->make();
    }


}