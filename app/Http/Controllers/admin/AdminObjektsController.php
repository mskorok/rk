<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Account;
use App\Models\Dotations;
use App\Models\Entrance;
use App\Models\EticketsObjekti;
use App\Models\GroupObjekts;
use App\Models\Mpos;
use App\Models\Objekts;
use App\Models\Owner;
use App\Models\School;
use App\Models\Transaction;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminObjektsController extends AdminController {

    protected $links = 'objekti';

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Objects Model
     * @var Objekts
     */
    protected $objekts;

    public function __construct(User $user, Objekts $objekts)
    {
        parent::__construct();
        $this->user = $user;
        $this->objekts = $objekts;
    }

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.management');

        // Grab all the groups
        $data = $this->objekts;

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $dot = Dotations::all();
        $schools = School::orderBy("heading","ASC")->get();

        // Title
        $title = Lang::get('admin/'.$this->links.'/title.create_a_new_obj');

        // Mode
        $mode = 'create';

        // Show the page
        return View::make('admin/'.$this->links.'/create_edit', compact('title', 'mode', 'dot','schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(Request $request)
    {

        $name = Input::get( 'object_name' );
        $school_id = Input::get( 'school_id' );

        try
        {
            $objekts = New Objekts($request->except("school_id"));
            if (!$objekts->save()) throw new \Exception($objekts->getErrors(),1);

            // create owner
            $owner = New Owner;
            $owner->owner_type = 4;
            $owner->owner_name = $name;
            if (!$owner->save()) throw new \Exception('Kļūda izveidojot owner!',2);

            // save owner to object
            $objekts->owner_id = $owner->id;
            //$objekts->grades = $klases;
            if (!$objekts->save()) throw new \Exception($objekts->getErrors(),2);

            // create account
            $acc = New Account;
            $acc->owner_id = $owner->id;
            $acc->account_type = 4;
            $acc->account_status = 1;
            if (!$acc->save()) throw new \Exception('Kļūda izveidojot account!',2);

            // find school
            $school = School::find($school_id);
            if (isset($school->reg_nr))
            {
                $objekts->object_regnr = $school->reg_nr;
                if (!$objekts->save()) throw new \Exception($objekts->getErrors(),1);
            }

        }
        catch (\Exception $e)
        {
            if ($e->getCode()==1)
            {
                return Redirect::to('admin/'.$this->links.'/create')->withInput()->withErrors($objekts->getErrors());
            }
            else {
                $objekts->delete();
                if (isset($owner)) $owner->delete();
                if (isset($acc)) $acc->delete();
                return Redirect::to('admin/'.$this->links.'/create')->withInput()->with('error', $e->getMessage());
            }

        }

        Cache::forget('all_object_ids');
        Cache::forget('all_objects');

        return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')->with('success', Lang::get('admin/'.$this->links.'/messages.create.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $objekts
     * @return Response
     */
    public function getEdit($objekts)
    {
        if ( $objekts->id )
        {
            $schools = School::orderBy("heading","ASC")->get();

            // Title
            $title = Lang::get('admin/'.$this->links.'/title.obj_edit');
            // mode
            $mode = 'edit';

            return View::make('admin/'.$this->links.'/create_edit', compact('objekts', 'title', 'mode','schools'));
        }
        else return Redirect::to('admin/'.$this->links)->with('error', Lang::get('admin/'.$this->links.'/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postEdit($objekts)
    {

        $updateo = Objekts::find($objekts->id);

        // update
        $updateo->object_type = Input::get( 'object_type' );
        $updateo->object_name = Input::get( 'object_name' );
        $updateo->object_regnr= Input::get( 'object_regnr' );
        $updateo->object_country = Input::get( 'object_country' );
        $updateo->object_city = Input::get( 'object_city' );
        $updateo->object_address = Input::get( 'object_address' );
        $updateo->object_ip = Input::get( 'object_ip' );
        $updateo->age = str_replace(" ","",Input::get( 'age' ));
        $updateo->kods = Input::get( 'kods' );
        $updateo->entrance_ip = Input::get( 'entrance_ip' );
        $updateo->sync_path = Input::get( 'sync_path' );
        $updateo->sync_enable = Input::get( 'sync_enable' );

        if ($updateo->save())
        {
            Cache::forget('all_objects');

            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')->with('success', Lang::get('admin/'.$this->links.'/messages.edit.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')
                ->withInput()
                ->withErrors($updateo->getErrors());
        }

    }

    /**
     * Remove resource page.
     *
     * @param $objekts
     * @return Response
     */
    public function getDelete($objekts)
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.obj_delete').' - '.$objekts->object_name;

        // Show the page
        return View::make('admin/'.$this->links.'/delete', compact('objekts', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postDelete($objekts)
    {
        $id = $objekts->id;

        //check for usage
        $sk = EticketsObjekti::Obj($id)->count();
        $sk += Entrance::Obj($id)->count();
        $sk += Mpos::Obj($id)->count();
        $sk += GroupObjekts::Obj($id)->count();
        $sk += Transaction::Obj($id)->count();

        if ($sk>0)
            return Redirect::to('admin/'.$this->links)->with('error', 'Objekts tiek izmantots sistēmā!');

        // delete school price reverse ids
        DB::table('schools_prices')
            ->where('school_id', $objekts->school_id)
            ->update(['object_id' => 0]);

        $objekts->delete();
        $objekts = Objekts::find($id);

        if ( empty($objekts) )
        {
            Cache::forget('all_object_ids');
            Cache::forget('all_objects');

            return Redirect::to('admin/'.$this->links)->with('success', Lang::get('admin/'.$this->links.'/messages.delete.success'));
        }
        else return Redirect::to('admin/'.$this->links)->with('error', Lang::get('admin/'.$this->links.'/messages.delete.error'));
    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $data = Objekts::select(array('id','object_name','object_regnr','object_type','object_country','object_city','object_address','object_ip','kods','created_at'));

        return Datatables::of($data)

            ->edit_column('object_type','@if($object_type==1)
                            Skola
                        @else
                            Cits
                        @endif')

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/edit\' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')

            ->remove_column('id')

            ->make();
    }


}