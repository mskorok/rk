<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Models\Auths;
use App\Models\Objekts;
use App\Models\User;
use Bllim\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdminAuthsController extends AdminController {

    protected $links = 'auths';

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Model
     * @var Auths
     */
    protected $auths;

    public function __construct(Auths $auths)
    {
        parent::__construct();
        $this->auths = $auths;
    }

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.management');

        // Grab all the groups
        $data = $this->auths;

        // Show the page
        return View::make('admin/'.$this->links.'/index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {

        $objekti = Objekts::get();

        // Title
        $title = Lang::get('admin/'.$this->links.'/title.create_a_new_obj');

        // Mode
        $mode = 'create';

        // Show the page
        return View::make('admin/'.$this->links.'/create_edit', compact('title', 'mode','objekti'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(Request $request)
    {
        $objekts = New Auths($request->except("\\"));

        // Was the comment saved with success?
        if ($objekts->save())
        {
            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')->with('success', Lang::get('admin/'.$this->links.'/messages.create.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/'.$this->links.'/create')
                ->withInput()
                ->withErrors($objekts->getErrors());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $objekts
     * @return Response
     */
    public function getEdit($objekts)
    {

        if ( $objekts->id )
        {
            $objekti = Objekts::get();

            // Title
            $title = Lang::get('admin/'.$this->links.'/title.obj_edit');
            // mode
            $mode = 'edit';

            return View::make('admin/'.$this->links.'/create_edit', compact('objekts', 'title', 'mode', 'objekti'));
        }
        else
        {
            return Redirect::to('admin/'.$this->links)->with('error', Lang::get('admin/'.$this->links.'/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postEdit($objekts)
    {

        $updateo = Auths::find($objekts->id);

        // update
        $updateo->authorisator_name = Input::get( 'authorisator_name' );
        $updateo->secret_key = Input::get( 'secret_key' );
        $updateo->object_id = Input::get( 'object_id' );


        if ($updateo->save())
        {
            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')->with('success', Lang::get('admin/'.$this->links.'/messages.edit.success'));
        }
        else {
            // Redirect on failure
            return Redirect::to('admin/'.$this->links.'/' . $objekts->id . '/edit')
                ->withInput()
                ->withErrors($updateo->getErrors());
        }

    }

    /**
     * Remove resource page.
     *
     * @param $objekts
     * @return Response
     */
    public function getDelete($objekts)
    {
        // Title
        $title = Lang::get('admin/'.$this->links.'/title.obj_delete').' - '.$objekts->authorisator_name;

        // Show the page
        return View::make('admin/'.$this->links.'/delete', compact('objekts', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $objekts
     * @return Response
     */
    public function postDelete($objekts)
    {
        $id = $objekts->id;
        $objekts = Auths::find($id);
        $objekts->delete();

        $objekts2 = Auths::find($id);
        if ( empty($objekts2) )
        {
            return Redirect::to('admin/'.$this->links)->with('success', Lang::get('admin/'.$this->links.'/messages.delete.success'));
        }
        else
        {
            return Redirect::to('admin/'.$this->links)->with('error', Lang::get('admin/'.$this->links.'/messages.delete.error'));
        }
    }

    /**
     * Show a list of all the MPOS devices formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $data = Auths::leftjoin('objects', 'authorisators.object_id', '=', 'objects.id')
            ->select(array('authorisators.id','objects.object_name','authorisator_name','authorisators.secret_key','authorisators.created_at'));

        return Datatables::of($data)

        ->add_column('actions', '<a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/edit\' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/'.$this->links.'/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')

        ->remove_column('id')

        ->make();
    }


}