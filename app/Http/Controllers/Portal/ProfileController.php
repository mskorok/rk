<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\BaseController;
use App\Http\Traits\Ajax;
use App\Models\ManualPayment;
use App\Models\Profile;
use App\Models\Upload;
use App\Services\CardService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Owner;
use App\Models\OwnerAccess;
use App\Models\Settings;
use App\Models\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;

class ProfileController extends BaseController
{
    use Ajax;

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profile()
    {
        return redirect()->route('edit_profile');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function editProfile(Request $request)
    {
        \Debugbar::disable();
        if (!\Auth::check()) {
            return redirect()->route('landing')
                ->with('error', trans('errors.register_encoded_error'));
        }
        /** @var UserService $userService */
        $userService = app('UserService');
        $userService->setUser(\Auth::user());
        $user = \Auth::user();
        $unread = $userService->getUnreadNotifications();

        $docExpired = (strtotime($userService->getDocumentDate()) < strtotime('+90 days'));
        $editEmail = $userService->refreshEmail($request);
        $profile = $userService->getProfile();
        $email = $profile->email ? : $user->email;
        $userName = $user->username;
        $userSurname = $user->surname;
        $pk = $user->pk;
        $positiveBalance = $userService->getBalance();
        $cardNumbers = $userService->getCards();
        $userBank = $userService->getBank();
        $userAccount = $userService->getBankAccount();
        $phone = $profile->phone ? : $user->phone;
        $address = $profile->address ? : $user->address;
        $citizenship = $profile->citizenship ? : $userService->getCitizenship();
        $citizenIndex = $userService->getCitizenshipIndex();
        $docType = $userService->getDocType();
        $docTypeIndex = $userService->getDocTypeIndex();
        $docNumber = $userService->getDocNumber();
        $documentDate = $userService->getDocumentDate();
        $image = Upload::where('parent_id', 0)->where('user_id', \Auth::id())->orderBy('id', 'desc')->first();
        $avatarPath = $image ? 'avatars/'.$image->filename : 'assets/portal/img/avatar.png';


        //Prevent possible operator error

        $trans = [];

        $citizenshipTrans = trans('portal/profile.citizenship');
        $documentTrans = trans('portal/profile.document');
        $reason = trans('portal/profile.reason');

        $citizenshipTrans = str_replace("\n", '', $citizenshipTrans);
        $documentTrans = str_replace("\n", '', $documentTrans);
        $reason = str_replace("\n", '', $reason);

        $trans['citizenship'] = $citizenshipTrans;
        $trans['document'] = $documentTrans;
        $trans['reason'] = $reason;

        return view(
            'portal.views.profile',
            compact(
                'email',
                'editEmail',
                'docExpired',
                'userName',
                'userSurname',
                'positiveBalance',
                'cardNumbers',
                'userBank',
                'userAccount',
                'phone',
                'address',
                'citizenship',
                'docType',
                'docNumber',
                'documentDate',
                'citizenIndex',
                'docTypeIndex',
                'pk',
                'unread',
                'avatarPath',
                'trans'
            )
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|View
     * @throws \RuntimeException
     */
    public function steps(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->route('landing')
                ->with('error', trans('errors.register_encoded_error'));
        }

        $bankTimer = 0;
        $photoAttached = false;

        $old = $request->old();
        $user = \Auth::user();
        $profile = $user->profile();
        if (!$user->isBankConfirmed()) {
            if (empty($old) && ! \Session::has('register_encoded')) {
                return redirect()->to('https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit');
            }
            if (empty($old)) {
                $bankTimer = 1;
                $encoded_data = \Session::get('register_encoded');
//              $encoded_data = "JzCWhrvz7HVP7T3c3znKE7y9Rxsyg7TNZGpIRE+6ucMo7yQ5rJ+xujUgOo2Zhd3UFK672ctUNCF8oo7/2fKEk3I1a8Dwq0jW/koc9HQZnMHv0hGOoOxFNYbFMJ8db9NLGd1d9HK/obBKJqQD0dM0TJzRmEPM/atTwop/3qHB2ec=";
//              $encoded_data = 'MadRotJpb2AR1kKhcYqGhmELQ9qOMlCNA9/9SCSdVyiXlJAN+rZwtpD1//XvKNh690fmmGmKbKMPHTJjuj8WY0pjnTCL47XRBSMIWdRZKiybx3FrCIV+vimVPJKwaTknxQtX2o7JDBxST6lyxNHUlSKw1CGpBvkNK1o9M8Nieec=';
                try {
                    $decrypted = app('Decrypt')->decrypt($encoded_data);
                    $data = json_decode($decrypted);
                    $timestamp = strtotime(str_replace('T', ' ', $data->Timestamp));
                } catch (\Exception $e) {
                    \Log::error('register - bad data='.$e->getMessage());
                    return redirect()->route('require')->with('error', trans('errors.bad_data_error'));
                }

                $server_timestamp = (time() + (10*60));

                if ($timestamp > $server_timestamp || (strlen($timestamp) < 6)) {
                    return redirect()->to('https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit');
                }

                if (isset($data->PersonCode) && !empty($data->PersonCode)) {
                    $oldUser = User::where('pk', $data->PersonCode)->first();
                    if ($oldUser instanceof User) {
                        $user = \Auth::user();
                        $newEmail = $user->email;
                        \Auth::login($oldUser, true);
                        $user = \Auth::user();
                        $oldEmail = $oldUser->email;
                        /** @var UserService $service */
                        $service = app('UserService');
                        $service->sendReloginMail($oldEmail, $newEmail);
                        $user->bank_confirmed = true;
                        $user->save();
                        return redirect()->route('dashboard');
                    }
                }
            } else {
                $data = new \stdClass();
                if (isset($old['username'])) {
                    $data->FirstName = $old['username'];
                } else {
                    return redirect()->route('require')->with('error', trans('errors.bad_data_error'));
                }

                if (isset($old['surname'])) {
                    $data->LastName = $old['surname'];
                } else {
                    return redirect()->route('require')->with('error', trans('errors.bad_data_error'));
                }

                if (isset($old['pk'])) {
                    $data->PersonCode = $old['pk'];
                } else {
                    return redirect()->route('require')->with('error', trans('errors.bad_data_error'));
                }
            }

            if (!((boolean) $user->profile_filled)
                && $profile instanceof Profile
                && $profile->pk !== ''
                && strlen($profile->pk) > 0) {
                $user->bank_confirmed = true;
                if (!$user->api_token) {
                    $user->api_token = str_random(60);
                }
                $user->save();
            }
        } else {
            $data = new \stdClass();
            $profile = $user->profile();
            if ($profile instanceof Profile) {
                $data->LastName = ($user->surname !== '') ? $user->surname : $profile->surname;
                $data->FirstName = ($user->username !== '') ? $user->username : $profile->username;
                $data->PersonCode = ($user->pk !== '') ? $user->pk : $profile->pk;
            } else {
                $data->LastName = $user->surname;
                $data->FirstName = $user->username;
                $data->PersonCode = $user->pk;
            }
        }


        $profile = $user->profile();
        if ($profile instanceof Profile && !((boolean) $user->profile_filled) && count($old) === 0) {
            $old = $profile->toArray();
            $old['has_status'] = (int) $old['has_status'] !== 1 ? 'no' : 'yes';
            $old['is_manager'] = (int) $old['is_manager'] !== 1 ? 'no' : 'yes';
            $old['is_owner'] = (int) $old['is_owner'] !== 1 ? 'no' : 'yes';
            unset($old['doc_copy']);
        }

        if ($profile instanceof Profile) {
            if (!empty($profile->doc_copy)) {
                $photoAttached = file_exists(public_path().$profile->doc_copy);
            }
        }

        $email = \Auth::user()->email;
        /** @var UserService $userService */
        $userService = app('UserService');
        $citizenIndex = $userService->getCitizenshipIndex();
        $docTypeIndex = $userService->getDocTypeIndex();
        if (!empty($old)) {
            $citizenIndex = $userService->getCitizenshipIndexFromOld($old);
            $docTypeIndex = $userService->getDocTypeIndexFromOld($old);
        }

        //Prevent possible operator error

        $trans = [];

        $citizenship = trans('portal/profile.citizenship');
        $document = trans('portal/profile.document');
        $fundsPlaceholder = trans('portal/partials/register/steps_section_3.funds_placeholder');

        $citizenship = str_replace("\n", '', $citizenship);
        $document = str_replace("\n", '', $document);
        $fundsPlaceholder = str_replace("\n", '', $fundsPlaceholder);

        $trans['citizenship'] = $citizenship;
        $trans['document'] = $document;
        $trans['funds_placeholder'] = $fundsPlaceholder;


        return view(
            'portal.views.register.register_steps',
            compact('email', 'data', 'old', 'citizenIndex', 'docTypeIndex', 'trans', 'bankTimer', 'photoAttached')
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function register(Request $request)
    {
        $user = \Auth::user();
        $params = $request->all();
        if (isset($params['\\'])) {
            unset($params['\\']);
        }
        // custom validator
        $custom = array(
            'required' => trans('validation.custom.required'),
            'email' => trans('validation.custom.email'),
            'phone' => trans('validation.custom.phone'),
            'size' => trans('validation.custom.size'),
            'integer' => trans('validation.custom.integer'),
            'unique' => trans('validation.custom.unique'),
        );
        $profile = $user->profile();
        if (!$profile) {
            $profile = new Profile();
        }
        $photoAttached = file_exists(public_path().$profile->doc_copy);

        /** @var UserService $userService */
        $userService = app('UserService');
        $params['phone'] = (int) $params['phone'];
        $params['has_status'] = $params['has_status'] !== 'no';
        $params['is_manager'] = $params['is_manager'] !== 'no';
        $params['is_owner'] = $params['is_owner'] !== 'no';
        if (!isset($params['is_accept_terms'])) {
            $params['is_accept_terms'] = false;
        }

        $params['is_accept_terms'] = $params['is_accept_terms'] === 'on';
        $params['user_id'] = $user->id;
        $params['income_month'] = $userService->sanitizeZero($params['income_month']);
        $params['income_month'] = $userService->sanitizeZero($params['income_year']);
        $params['income_month'] = $userService->sanitizeZero($params['income_amount']);
        $params['income_month'] = $userService->sanitizeZero($params['outgoing_month']);
        $params['income_month'] = $userService->sanitizeZero($params['outgoing_year']);
        $params['income_month'] = $userService->sanitizeZero($params['outgoing_amount']);
        /** @var array $params */
        $params = array_map(function ($param) {
            return is_string($param) ? trim(strip_tags($param)) : $param;
        }, $params);
        $errors = new MessageBag;
        if (!array_key_exists('is_accept_terms', $params)
            || (array_key_exists('is_accept_terms', $params) && $params['is_accept_terms'] !== true)
        ) {
            $errors->add('is_accept_terms', trans('errors.terms_not_accepted'));
            return redirect()->back()->exceptInput('doc_copy')->withErrors($errors->toArray());
        }
        if ((int) $params['pk'] === 0) {
            $user->bank_confirmed = 0;
            $user->save();
            return redirect()->to('https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit');
        }
        // existing PK
        $exists = User::where('pk', $params['pk'])->count();
        if ($exists > 0) {
            $errors->add('is_registered', trans('errors.already_registered'));
            return redirect()->back()->exceptInput('doc_copy')->withErrors($errors->toArray());
        }
        $validator = \Validator::make($params, Profile::VALIDATION_RULES, $custom);
        if (!isset($params['doc_copy']) && !empty($photoAttached)) {
            $validator = \Validator::make($params, Profile::VALIDATION_RULES_PHOTO_ATTACHED, $custom);
        }
        if ($validator->passes()) {
            $profile->fill($params);
            $profile->user_id = $user->id;
            $profile->setPhotoUploads(true);
            $profile->save();
            $params['dokuments'] = $profile->doc_copy;
            $params['terms'] = $params['is_accept_terms'] ? 3 : 0;
            /** @var UserService $userService */
            $userService = app('UserService');
            if (!$userService->fillUser($params)) {
                $errors->merge($userService->getErrors());
                return redirect()->back()->exceptInput('doc_copy')->withErrors($errors->toArray());
            }
            if (!$userService->afterSave()) {
                $errors->merge($userService->getErrors());
                return redirect()->back()->exceptInput('doc_copy')->withErrors($errors->toArray());
            }

            return redirect()->route('dashboard');
        } else {
            $errors->merge($validator->errors()->toArray());
            return redirect()->back()->exceptInput('doc_copy')->withErrors($errors->toArray());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditProfile(Request $request)
    {
        $params = $request->all();
        if (isset($params['\\'])) {
            unset($params['\\']);
        }
        if (isset($params[''])) {
            unset($params['']);
        }
        /** @var UserService $userService */
        $userService = app('UserService');
        $profile = $userService->getProfile();
        $params['user_id'] = $profile->user_id;
        $oldProfile = clone $profile;
        $bag = new MessageBag();
        $userService->refreshPassword($request, $bag);
        if ($bag->any()) {
            return redirect()->back()->exceptInput('doc_copy')
                ->withErrors($bag->toArray());
        } else {
            unset($params['password_new_confirm']);
            unset($params['password_current']);
            unset($params['password_new']);
        }
        // custom validator
        $custom = array(
            'required' => trans('validation.custom.required'),
            'email' => trans('validation.custom.email'),
            'phone' => trans('validation.custom.phone'),
            'size' => trans('validation.custom.size'),
            'integer' => trans('validation.custom.integer'),
            'unique' => trans('validation.custom.unique'),
        );
        if ($profile->email !== \Auth::user()->email) {
            $rules = Profile::SAVE_EMAIL_VALIDATION_RULES;
        } else {
            $rules = Profile::SAVE_VALIDATION_RULES;
        }
        if (isset($params['doc_copy']) && $params['doc_copy'] !== '') {
            $profile->setPhotoUploads(true);
            $rules['doc_copy'] = 'image|mimes:jpg,png,jpeg';
        }
        $validator = \Validator::make($params, $rules, $custom);

        if ($validator->passes()) {
            $profile->forceFill($params);
            try {
                $profile->setBag(new MessageBag());
                try {
                    $profile->save();
                    $profileBag = $profile->getBag();
                    if ($profileBag->any()) {
                        $profile->forceFill($oldProfile->attributesToArray());
                        $profile->setUpload(false);
                        $profile->save();
                        return redirect()->back()->exceptInput('doc_copy')
                            ->withErrors($profileBag->toArray());
                    }
                } catch (\Exception $exception) {
                    $profileBag = $profile->getBag();
                    $profile->forceFill($oldProfile->attributesToArray());
                    $profile->setUpload(false);
                    $profile->save();
                    return redirect()->back()->exceptInput('doc_copy')
                        ->withErrors($profileBag->toArray());
                }
                $bag = $userService->refreshUser($profile, $bag);
                if ($bag->any()) {
                    $profile->forceFill($oldProfile->attributesToArray());
                    $profile->setUpload(false);
                    $profile->save();
                    return redirect()->back()->exceptInput('doc_copy')
                        ->withErrors($bag->toArray());
                }
                return redirect()->route('dashboard');
            } catch (\RuntimeException $exception) {
                \Log::error('edit profile exception -'.$exception->getMessage());
                $bag->add('errors', $exception->getMessage());
                return redirect()->back()->exceptInput('doc_copy')
                    ->withErrors($bag->toArray());
            }
        } else {
            $bag->merge($validator->errors()->toArray());
            return redirect()->back()->exceptInput('doc_copy')
                ->withErrors($bag->toArray());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function registerSave(Request $request)
    {
        if ($this->isAjax()) {
            $params = $request->all();
            if (isset($params['\\'])) {
                unset($params['\\']);
            }
            $user = \Auth::user();
            /** @var UserService $userService */
            $userService = app('UserService');
            $params['username'] = !empty($user->username) ? $user->username : $params['username'];
            $params['surname'] = !empty($user->surname) ? $user->surname : $params['surname'];
            $params['pk'] = !empty($user->pk) ? $user->pk : $params['pk'];
            $params['income_month'] = $userService->sanitizeZero($params['income_month']);
            $params['income_month'] = $userService->sanitizeZero($params['income_year']);
            $params['income_month'] = $userService->sanitizeZero($params['income_amount']);
            $params['income_month'] = $userService->sanitizeZero($params['outgoing_month']);
            $params['income_month'] = $userService->sanitizeZero($params['outgoing_year']);
            $params['income_month'] = $userService->sanitizeZero($params['outgoing_amount']);



            $params['user_id'] = $user->id;
            $profile = $userService->getProfile();
            // custom validator
            $custom = array(
                'required' => trans('validation.custom.required'),
                'email' => trans('validation.custom.email'),
                'phone' => trans('validation.custom.phone'),
                'size' => trans('validation.custom.size'),
                'integer' => trans('validation.custom.integer'),
                'unique' => trans('validation.custom.unique'),
            );

            $params['has_status'] = isset($params['has_status']) ? $params['has_status'] !== 'no' : false;
            $params['is_manager'] = isset($params['is_manager']) ? $params['is_manager'] !== 'no' : true;
            $params['is_owner'] = isset($params['is_owner']) ? $params['is_owner'] !== 'no' : true;
            $params['is_accept_terms'] = isset($params['is_accept_terms']) ? $params['is_accept_terms'] === 'on': false;
            $validator = \Validator::make($params, Profile::SAVE_VALIDATION_RULES, $custom);
            if ($validator->passes()) {
                $oldProfile = clone $profile;
                $profile->fill($params);
                $profile->setUpload(true);
                try {
                    $profile->setBag(new MessageBag());

                    try {
                        $profile->save();
                        $bag = $profile->getBag();
                        if ($bag->any()) {
                            $profile->forceFill($oldProfile->attributesToArray());
                            $profile->setUpload(false);
                            $profile->save();
                            return response()->json(['error' => $bag->toArray()]);
                        }
                    } catch (\Exception $exception) {
                        $bag = $profile->getBag();
                        $profile->forceFill($oldProfile->attributesToArray());
                        $profile->setUpload(false);
                        $profile->save();
                        return response()->json(['error' => $bag->toArray()]);
                    }

                    if (!empty($profile->pk)) {
                        $u = User::where('pk', $params['pk'])->first();
                        if (!$u) {
                            $user->bank_confirmed = true;
                            $user->save();
                        }
                    }
                    return response()->json(['success' => $profile->id]);
                } catch (\RuntimeException $exception) {
                    return response()->json(['error' => $exception->getMessage()]);
                }
            } else {
                $errors = $validator->errors();
//                $errors = serialize($errors->toArray());
                return response()->json(['error' => $errors->toArray()]);
            }
        } else {
            return redirect()->route('landing');
        }
    }

    /**
     * @param Request $request
     * @return View
     */
    public function eCard(Request $request)
    {
        $user = \Auth::user();
        $cards = $user->cards;
        $old = $request->old();
        $created = $user->cardIsNotFinished();
        /** @var CardService $service */
        $service = app('CardService');
        $schools = $service->getActiveSchools();
        $requisites = Settings::first();
        $filled = false;

        //Prevent possible operator error

        $trans = [];

        $addChildPhoto = trans('portal/partials/modals/add_child_photo.add_child_photo');
        $downloadChildPhoto = trans('portal/partials/modals/add_child_photo.download_child_photo');
        $addChildPhotoBoys = trans('portal/partials/modals/add_child_photo.boys');
        $addChildPhotoGirls = trans('portal/partials/modals/add_child_photo.girls');
        $addImage = trans('portal/partials/modals/add_child_photo.add_image');
        $allowedFormats = trans('portal/partials/profile/section_step_3.allowed_formats');
        $maxImageSize = trans('portal/partials/profile/section_step_3.max_image_size');
        $addChildPhotoError = trans('portal/partials/modals/add_child_photo.error');
        $addChildPhotoSave = trans('portal/partials/modals/add_child_photo.save');
        $eCardNumber = trans('portal/partials/register/section_step_1.e_card_number');
        $howToFind = trans('portal/partials/register/section_step_1.how_to_find_card_number');
        $fieldRequired = trans('validation.custom.error_text');
        $daily = trans('portal/partials/register/section_step_1.daily');
        $weekly = trans('portal/partials/register/section_step_1.weekly');
        $monthly = trans('portal/partials/register/section_step_1.monthly');
        $max140 = trans('validation.max.numeric', ['attribute' => 'Amount', 'max' => 140]);
        $name = trans('portal/partials/register/section_step_1.name');
        $surname = trans('portal/partials/register/section_step_1.surname');
        $personalCode = trans('portal/partials/register/section_step_1.personal_identifier');
        $class = trans('portal/partials/register/section_step_1.class');
        $addChildPhoto1 = trans('portal/partials/register/section_step_1.add_child_photo');
        $eCardLimit = trans('portal/register/ecard.limit');
        $selectSchool = trans('portal/register/ecard.select_school');


        $addChildPhoto = str_replace("\n", '', $addChildPhoto);
        $downloadChildPhoto = str_replace("\n", '', $downloadChildPhoto);
        $addChildPhotoBoys = str_replace("\n", '', $addChildPhotoBoys);
        $addChildPhotoGirls = str_replace("\n", '', $addChildPhotoGirls);
        $addImage = str_replace("\n", '', $addImage);
        $allowedFormats = str_replace("\n", '', $allowedFormats);
        $maxImageSize = str_replace("\n", '', $maxImageSize);
        $addChildPhotoError = str_replace("\n", '', $addChildPhotoError);
        $addChildPhotoSave = str_replace("\n", '', $addChildPhotoSave);
        $eCardNumber = str_replace("\n", '', $eCardNumber);
        $howToFind = str_replace("\n", '', $howToFind);
        $fieldRequired = str_replace("\n", '', $fieldRequired);
        $daily = str_replace("\n", '', $daily);
        $weekly = str_replace("\n", '', $weekly);
        $monthly = str_replace("\n", '', $monthly);
        $max140 = str_replace("\n", '', $max140);
        $name = str_replace("\n", '', $name);
        $surname = str_replace("\n", '', $surname);
        $personalCode = str_replace("\n", '', $personalCode);
        $class = str_replace("\n", '', $class);
        $addChildPhoto1 = str_replace("\n", '', $addChildPhoto1);
        $eCardLimit = str_replace("\n", '', $eCardLimit);
        $selectSchool = str_replace("\n", '', $selectSchool);


        $trans['add_child_photo'] = $addChildPhoto;
        $trans['download_child_photo'] = $downloadChildPhoto;
        $trans['add_child_photo_boys'] = $addChildPhotoBoys;
        $trans['add_child_photo_girls'] = $addChildPhotoGirls;
        $trans['add_image'] = $addImage;
        $trans['allowed_formats'] = $allowedFormats;
        $trans['max_image_size'] = $maxImageSize;
        $trans['add_child_photo_error'] = $addChildPhotoError;
        $trans['add_child_photo_save'] = $addChildPhotoSave;
        $trans['e_card_number'] = $eCardNumber;
        $trans['how_to_find'] = $howToFind;
        $trans['field_required'] = $fieldRequired;
        $trans['daily'] = $daily;
        $trans['weekly'] = $weekly;
        $trans['monthly'] = $monthly;
        $trans['max_140'] = $max140;
        $trans['name'] = $name;
        $trans['surname'] = $surname;
        $trans['pk'] = $personalCode;
        $trans['class'] = $class;
        $trans['add_child_photo_1'] = $addChildPhoto1;
        $trans['e_card_limit'] = $eCardLimit;
        $trans['select_school'] = $selectSchool;


        if (\Session::has('created_payments')) {
            $filled = true;
        }
        return view(
            'portal.views.register.register_ecard',
            compact('user', 'schools', 'cards', 'created', 'old', 'requisites', 'filled', 'trans')
        );
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCard(Request $request)
    {
        $params = $request->all();
        $params['user_id'] = \Auth::id();
        if (isset($params['\\'])) {
            unset($params['\\']);
        }
        $user = \Auth::user();
        $bag = new MessageBag();
        /** @var CardService $service */
        $service = app('CardService');
        $bag = $service->saveCards($params, $bag);
        if ($bag->any()) {
            return redirect()->back()->withInput()->withErrors($bag->toArray());
        }
        try {
            $bag = $service->createTickets($bag);
        } catch (\RuntimeException $e) {
            \Log::error('ticket not saved - '.$e->getMessage());
            $bag->add('ticket_not_saved', $e->getMessage());
        }

        if ($bag->any()) {
            return redirect()->back()->withInput()->withErrors($bag->toArray());
        }
        try {
            $user->has_card = true;
            $user->card_not_finished = true;
            $user->save();
        } catch (\RuntimeException $e) {
            \Log::error('user not saved - '.$e->getMessage());
            $bag->add('user_not_saved', $e->getMessage());
        }
        if ($bag->any()) {
            return redirect()->back()->withInput()->withErrors($bag->toArray());
        }
        return redirect()->route('eCard');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveCard(Request $request)
    {
        $params = $request->all();
        $params['user_id'] = \Auth::id();
        if (isset($params['\\'])) {
            unset($params['\\']);
        }
        $user = \Auth::user();
        $bag = new MessageBag();
        /** @var CardService $service */
        $service = app('CardService');
        $bag = $service->saveCards($params, $bag, true);
        if ($bag->any()) {
            return redirect()->back()->withInput()->withErrors($bag->toArray());
        }
        try {
            $user->has_card = true;
            $user->save();
        } catch (\RuntimeException $e) {
            \Log::error('user not saved - '.$e->getMessage());
            $bag->add('user_not_saved', $e->getMessage());
        }
        if ($bag->any()) {
            return redirect()->back()->withInput()->withErrors($bag->toArray());
        }
        return redirect()->route('landing');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fillCard(Request $request)
    {
        $params = $request->all();
        if (isset($params['\\'])) {
            unset($params['\\']);
        }
        $bag = new MessageBag();
        /** @var CardService $service */
        $service = app('CardService');
        $bag = $service->saveTopUpCards($params, $bag);
        if ($bag->any()) {
            return redirect()->back()->withErrors($bag->toArray());
        }
        $bag = $service->createPayments($bag);
        if ($bag->any()) {
            return redirect()->back()->withErrors($bag->toArray());
        }
        try {
            $request->session()->set('created_payments', 1);
        } catch (\RuntimeException $e) {
            \Log::error('user not saved - '.$e->getMessage());
            $bag->add('user_not_saved', $e->getMessage());
        }
        if ($bag->any()) {
            return redirect()->back()->withErrors($bag->toArray());
        }
        return redirect()->route('eCard');
    }

    public function filledCard(Request $request)
    {
        $user = \Auth::user();
        $bag = new MessageBag();
        try {
            $user->refill_account_not_created = true;
            $user->save();
        } catch (\RuntimeException $e) {
            \Log::error('user not saved - '.$e->getMessage());
            $bag->add('user_not_saved', $e->getMessage());
        }
        if ($bag->any()) {
            return redirect()->back()->withErrors($bag->toArray());
        }
        $request->session()->forget('created_payments');
        return redirect()->route('dashboard');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveFillCard(Request $request)
    {
        $params = $request->all();
        if (isset($params['\\'])) {
            unset($params['\\']);
        }
        $bag = new MessageBag();
        /** @var CardService $service */
        $service = app('CardService');
        $bag = $service->saveTopUpCards($params, $bag);
        if ($bag->any()) {
            return redirect()->back()->withErrors($bag->toArray());
        }
        return redirect()->route('landing');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \RuntimeException
     */
    public function interrupt(Request $request)
    {
        $request->session()->set('interrupt_pop_up', 1);
        return redirect()->route('landing');
    }

    public function getPdf($id)
    {
        $manual_payment = ManualPayment::findOrFail($id);

        if ($manual_payment->user_id !== \Auth::user()->id && \App::environment() !== 'testing') {
            return null;
        }

        $rekviziti = str_replace('\\n', '<br />', Settings::first()->payment_form_details);
        $view = view('etickets/mtransfer/pdf', compact('manual_payment', 'rekviziti'));

        $html = '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
                <head profile="http://gmpg.org/xfn/11">
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><style type="text/css">body { font-family: DejaVu Sans, sans-serif; }</style></head><body>' .
            $view
            . '</body></html>';

        return \PDF::loadHTML($html)->download(trans('portal/partials/register/section_step_1.payment').'.pdf');
    }

    public function deleteCheck(Request $request)
    {
        if ($this->isAjax()) {
            $params = $request->all();
            $credentials = ['email' => $params['remove_profile_email'], 'password' => $params['remove_password']];
            if (\Auth::validate($credentials)) {
                return response()->json(['success' => $request->all()]);
            }
            return response()->json(['error' => trans('errors.password_not_valid')]);
        } else {
            return redirect()->route('landing');
        }
    }

    public function delete(Request $request)
    {
        if ($this->isAjax()) {
            $user = \Auth::user();
            $id = \Auth::id();
            $userService = app('UserService');
            $userService->sendDeleteUserLetter($user);
            $user->delete_reason = $request->input('remove_reason');
            $user->save();
            $bag = new MessageBag();

            if ($user->delete()) {
                try {
                    $accesses = OwnerAccess::ao()->u($id)->get();
                    OwnerAccess::sub()->where('user_id', $id)->delete();
                    foreach ($accesses as $access) {
                        $account = Account::o($access->owner_id)->first();
                        if ($account) {
                            $account->account_status = 0;
                            $account->save();
                        }
                        $owner = Owner::find($access->owner_id);
                        if ($owner) {
                            $owner->delete();
                        }
                    }
                } catch (\RuntimeException $exception) {
                    return redirect()->back()->withErrors(['save_error' => $exception->getMessage()]);
                }
/*****************   old version *****************************/
//                //find owner
//                $ownerAccess = OwnerAccess::ao()->u($id)->first();
//                if ($ownerAccess) {
//                    // account disable
//                    $account = Account::o($ownerAccess->owner_id);
//                    if ($account) {
//                        $account->account_status=0;
//                        $account->save();
//                    }
//                }
//
//                OwnerAccess::sub()->where('owner_id', $id)->delete();
            } else {
                $bag->add('user_not_saved', trans('errors.not_saved'));
                return redirect()->back()->withErrors($bag->toArray());
            }
            return response()->json(['success' => $request->all()]);
        } else {
            return redirect()->route('landing');
        }
    }

    public function changeEmail(Request $request)
    {
        if ($this->isAjax()) {
            /** @var UserService $userService */
            $userService = app('UserService');
            $userService->sendChangeEmailLetter($request);
            return response()->json(['success' => $request->all()]);
        } else {
            return redirect()->route('landing');
        }
    }
}
