<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Traits\Ajax;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;

class CommonController extends BaseController
{
    use Ajax;

    /**
     * @var string|\Symfony\Component\Translation\TranslatorInterface
     */
    protected $subject;

    /**
     * The mailer instance.
     *
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    protected $mailer;


    /**
     * Initializer.
     *
     * @access   public
     * @param Mailer $mailer
     * @throws \Exception
     */
    public function __construct(Mailer $mailer)
    {
        parent::__construct();
        $this->mailer = $mailer;
        $this->subject = trans('passwords.reset_subject');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function terms()
    {
        return view('portal.views.terms_of_use');
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function feedback(Request $request)
    {
        if ($this->isAjax()) {
            try {
                $data = $request->all();
                $validator = $this->feedbackValidator($data);
                if ($validator->fails()) {
                    return response()->json(['error' => $validator->failed()]);
                }
                $this->sendFeedback($data);
                return response()->json(['success' => $data]);
            } catch (\RuntimeException $exception) {
                return response()->json(['error' => $exception->getMessage()]);
            }
        }
        return false;
    }

    /**
     * Get a validator for an incoming feedback request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function feedbackValidator(array $data)
    {
        return \Validator::make($data, [
            'support_name' => 'required|string|min:2',
            'support_surname' => 'required|string|min:2',
            'support_phone' => 'required|numeric|min:8',
            'support_email' => 'required|string|email|max:255|',
            'support_question' => 'required|string|min:2'
        ]);
    }

    protected function sendFeedback($data)
    {
        $view = 'portal.views.mail.feedback';
        $subject = trans(
            'portal/mail/feedback.subject',
            ['name' => $data['support_name'], 'surname' => $data['support_surname']]
        );
        $from = $data['support_email'];
        $to = 'skolas@rigaskarte.lv';
        $this->mailer->send($view, [
            'name' => $data['support_name'],
            'surname' => $data['support_surname'],
            'email' => $data['support_email'],
            'phone' => $data['support_phone'],
            'text' => $data['support_question']
        ], function ($m) use ($to, $from, $subject) {
            /** @var $m Message */
            $m->to($to);
            $m->from($from);
            $m->subject($subject);
        });
    }
}
