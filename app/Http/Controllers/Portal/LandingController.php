<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\BaseController;
use App\Http\Traits\Ajax;
use App\Models\Banner;
use App\Models\Settings;
use App\Models\TopSlider;
use App\Services\UserService;
use Illuminate\Http\Request;

class LandingController extends BaseController
{
    use Ajax;

    public function landing()
    {
        $topSlider = Settings::where('name', '=', 'top_slider')->first();
        if ($topSlider) {
            $topSlider = $topSlider->dokuments_text;
        }
        $section1 = Settings::where('name', '=', 'land.section_1')->first();
        if ($section1) {
            $section1 = $section1->dokuments_text;
        }
        $section2 = Settings::where('name', '=', 'land.section_2')->first();
        if ($section2) {
            $section2 = $section2->dokuments_text;
        }
        $section3 = Settings::where('name', '=', 'land.section_3')->first();
        if ($section3) {
            $section3 = $section3->dokuments_text;
        }
        $section4 = Settings::where('name', '=', 'land.section_4')->first();
        if ($section4) {
            $section4 = $section4->dokuments_text;
        }
        $section5 = Settings::where('name', '=', 'land.section_5')->first();
        if ($section5) {
            $section5 = $section5->dokuments_text;
        }
        $section6 = Settings::where('name', '=', 'land.section_6')->first();
        if ($section6) {
            $section6 = $section6->dokuments_text;
        }
        $section7 = Settings::where('name', '=', 'land.section_7')->first();
        if ($section7) {
            $section7 = $section7->dokuments_text;
        }
        $section8 = Settings::where('name', '=', 'land.section_8')->first();
        if ($section8) {
            $section8 = $section8->dokuments_text;
        }
        $sectionCard = Settings::where('name', '=', 'land.section_card')->first();
        if ($sectionCard) {
            $sectionCard = $sectionCard->dokuments_text;
        }

        $topSliders = TopSlider::where('active', '=', 1)->get();
        $collection = Banner::where('active', '=', 1)
            ->orderBy('sort')
            ->get();
        $banners = [];
        /** @var Banner $item */
        foreach ($collection as $item) {
            $to = $item->exposition_time_to && $item->exposition_time_to > new \DateTime('now');
            $from = $item->exposition_time_from && $item->exposition_time_from < new \DateTime('now');
            if ($to && $from) {
                $banners[] = $item;
            } elseif (!$item->exposition_time_from && $to) {
                $banners[] = $item;
            } elseif (!$item->exposition_time_to && $from) {
                $banners[] = $item;
            } elseif (!$item->exposition_time_to && !$item->exposition_time_from) {
                $banners[] = $item;
            }
        }
        if (count($banners)) {
            /** @var Banner $banner */
            $banner = $banners[0];
            switch ($banner->size) {
                case '1140 х 250':
                    $size['l'] = 1;
                    $size['m'] = 1;
                    $size['s'] = 1;
                    break;
                case '555 х 160':
                    $size['l'] = 2;
                    $size['m'] = 1;
                    $size['s'] = 1;
                    break;
                case '360 х 160':
                    $size['l'] = 3;
                    $size['m'] = 2;
                    $size['s'] = 1;
                    break;
                case '262 х 160':
                    $size['l'] = 4;
                    $size['m'] = 3;
                    $size['s'] = 2;
                    break;
                default:
                    $size['l'] = 4;
                    $size['m'] = 3;
                    $size['s'] = 2;
                    break;
            }
        } else {
            $size['l'] = 4;
            $size['m'] = 3;
            $size['s'] = 2;
        }

        return view(
            'portal.views.main',
            compact(
                'topSlider',
                'section1',
                'section2',
                'section3',
                'section4',
                'section5',
                'section6',
                'section7',
                'section8',
                'sectionCard',
                'topSliders',
                'banners',
                'size'
            )
        );
    }

    public function friends(Request $request)
    {
        if ($this->isAjax()) {
            $email = $request->input('email');
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                /** @var UserService $service */
                $service = app('UserService');
                $service->sendMailToFriends($email);
                return response()->json(['success' => 'success']);
            } else {
                return response()->json(['errors' => true]);
            }
        } else {
            throw new \RuntimeException('Only ajax request');
        }
    }
}
