<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\BaseController;
use App\Http\Traits\Ajax;
use App\Models\Card;
use App\Models\Eticket;
use App\Models\Image;
use App\Models\Upload;
use App\Models\User;
use App\Models\PermittedIp;
use Illuminate\Http\Request;

class RegisterController extends BaseController
{
    use Ajax;

    public function register(Request $request)
    {
        $email = $request->input('email');
        return view('portal.views.register.register', ['disable' => true, 'email' => $email]);
    }

    public function eCard()
    {
        return view('portal.views.register.register_ecard');
    }

    public function registerRequire()
    {
        return view('portal.views.register.register_require');
    }

    public function success(Request $request)
    {
        $email = $request->input('email');
        return view('portal.views.register.register_success', ['email' => $email]);
    }

    public function thanks()
    {
        return view('portal.views.register.register_thanks');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function createAvatar(Request $request)
    {
        if ($this->isAjax()) {
            $type = (int) $request->input('type');
            $src = $request->input('src');
            $path = DIRECTORY_SEPARATOR .'avatars' . DIRECTORY_SEPARATOR;
            $filename = uniqid('avatar', false) . '_' . time().'.png';
            $file = public_path() .  $path . $filename;
            try {
                $ifp = fopen($file, 'w+');
                fwrite($ifp, base64_decode($src));
                fclose($ifp);
                $size = filesize($file);
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            $image = ($type === AVATAR_TYPE_USER) ? new Upload() : new Image();
            $image->filename = $filename;
            $image->path = $path;
            $image->size = $size;
            $image->parent_id = 0;
            $image->mimetype = 'image/png';
            $image->extension = 'png';
            $image->user_id = \Auth::check() ? \Auth::id() : 2;
            try {
                $image->save();
                //test
                return response()->json(
                    ['success' => 'success', 'id' => $image->id, 'path' => $path.$filename]
                );
                //end test
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        } else {
            die('only ajax request');
        }
    }
}
