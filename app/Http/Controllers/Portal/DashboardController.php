<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\BaseController;
use App\Http\Traits\Ajax;
use App\Models\Card;
use App\Services\CardService;
use App\Services\ETicketService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class DashboardController extends BaseController
{
    use  Ajax;

    public function dashboard(Request $request)
    {
        /** @var ETicketService $service */
        $service = app('ETicketService');
        /** @var UserService $userService */
        $userService = app('UserService');
        $unread = $userService->getUnreadNotifications();
        $eTickets = $service->getETickets();
        $notifications = $service->getNotifications($request);
        $payments = $service->getInvoices($request);
        $transactions = $service->getTransactions();
        $weekPayments = $service->getWeekendPayments(0);
        $currentWeek = (new \DateTime('now'))->format('W');
        $startWeek = new \DateTime('this week');
        $endWeek = (new \DateTime('this week'))->modify('+1 week');
        $weekBefore = (new \DateTime('this week'))->modify('-1 week');
        $averageCheck = $service->getAverageChecks();
        $lastRefill = $service->getLastRefill();
        $nextRefill = $service->getNextRefill();
        $cardService = app('CardService');
        $schools = $cardService->getActiveSchools();

        //Prevent possible operator error

        $limit = trans('portal/dashboard.limit');
        $selectSchool = trans('portal/dashboard.select_school');
        $selectPaymentType = trans('portal/dashboard.select_payment_type');
        $transactionType = trans('portal/dashboard.transaction_type');
        $status = trans('portal/dashboard.status');
        $invoiceCard = trans('portal/dashboard.card');
        $today = trans('portal/dashboard.today');
        $yesterday = trans('portal/dashboard.yesterday');
        $last7 = trans('portal/dashboard.last_7_days');
        $last30 = trans('portal/dashboard.last_30_days');
        $thisMonth = trans('portal/dashboard.this_month');
        $prevMonth = trans('portal/dashboard.previous_month');

        $limit = str_replace("\n", '', $limit);
        $selectSchool = str_replace("\n", '', $selectSchool);
        $selectPaymentType = str_replace("\n", '', $selectPaymentType);
        $transactionType = str_replace("\n", '', $transactionType);
        $status = str_replace("\n", '', $status);
        $invoiceCard = str_replace("\n", '', $invoiceCard);
        $today = str_replace("\n", '', $today);
        $yesterday = str_replace("\n", '', $yesterday);
        $last7 = str_replace("\n", '', $last7);
        $last30 = str_replace("\n", '', $last30);
        $thisMonth = str_replace("\n", '', $thisMonth);
        $prevMonth = str_replace("\n", '', $prevMonth);

        $trans = [];
        $trans['limit'] = $limit;
        $trans['select_school'] = $selectSchool;
        $trans['select_payment_type'] = $selectPaymentType;
        $trans['transaction_type'] = $transactionType;
        $trans['status'] = $status;
        $trans['invoice_card'] = $invoiceCard;
        $trans['today'] = $today;
        $trans['yesterday'] = $yesterday;
        $trans['last7'] = $last7;
        $trans['last30'] = $last30;
        $trans['this_month'] = $thisMonth;
        $trans['prev_month'] = $prevMonth;
        $trans['custom'] = trans('portal/dashboard.custom');
        $trans['applay'] = trans('portal/dashboard.applay');
        $trans['cancel'] = trans('portal/dashboard.cancel');

        return view(
            'portal.views.dashboard',
            compact(
                'eTickets',
                'notifications',
                'payments',
                'weekPayments',
                'startWeek',
                'endWeek',
                'weekBefore',
                'schools',
                'transactions',
                'nextRefill',
                'lastRefill',
                'averageCheck',
                'currentWeek',
                'unread',
                'trans'
            )
        );
    }

    public function returnOk()
    {
        return view(
            'portal.views.return_ok'
        );
    }

    public function returnFail()
    {
        return view(
            'portal.views.return_fail'
        );
    }

    public function testCard()
    {
        return view(
            'portal.views.test'
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     * @throws \BadFunctionCallException
     */
    public function ajaxFillCard(Request $request)
    {
        if ($this->isAjax()) {
            try {
                /** @var UserService $userService */
                $userService = app('UserService');
                $id = (int) $request->input('eticket_id');
                $i = $request->input('i');
                $key = 'top_up_sum_'.$i;
                $amount = $request->input($key);
                if ((float) $amount < 1) {
                    return response()->json(
                        ['error' => trans('validation.min.numeric', ['attribute' => 'Sum', 'min' => 1])]
                    );
                }
                $amount = $userService->sanitizeZero($amount);
                /** @var ETicketService $service */
                $service = app('ETicketService');
                $service->getETicket($id);
                $bag = $service->createManualPayment($amount);
                if ($bag->any()) {
                    return response()->json(['error' => $bag->toArray()]);
                }
                $html = $service->createPaymentHtml();
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            return response()->json(['success' => 'refill ajax', 'html' => $html]);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxEditCard(Request $request)
    {
        if ($this->isAjax()) {
            try {
                /** @var UserService $userService */
                $userService = app('UserService');
                $id = (int) $request->input('eticket_id');
                $i = $request->input('i');
                $type = $request->input('e'.$i.'_period');
                $amount = (float) $request->input('e'.$i.'_amount');
                $amount = $userService->sanitizeZero($amount, true);
                $childPhotoId = $request->input('child_photo_'.$i);
                /** @var ETicketService $service */
                $service = app('ETicketService');
                $limitType = $service->getLimits($type);
                $limit = $amount*100;
                $service->getETicket($id);
                $eTicket = $service->getCurrentETicket();
                $eTicket->thumb_id = $childPhotoId;
                $eTicket->limit_type = $limitType;
                $eTicket->limit = (int) $limit;
                $eTicket->save();
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            return response()->json(['success' => 'E card edited ajax']);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxHistory(Request $request)
    {
        if ($this->isAjax()) {
            /** @var ETicketService $service */
            $service = app('ETicketService');
            try {
                $i = $request->input('i');
                $perPage = (int) $request->input('per_page_'.$i);
                $html = $service->createHistoryHtml($request)->render();
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            return response()->json(['success' => 'history ajax', 'html' => $html, 'per_page' => $perPage]);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxBlockCard(Request $request)
    {
        if ($this->isAjax()) {
            $user = \Auth::user();
            $id = (int) $request->input('eticket_id');
            /** @var ETicketService $service */
            $service = app('ETicketService');
            try {
                $blocked = $service->blockCard($id);
                \Cache::forget('active_etickets_'.$user->id);
                \Cache::forget('all_active_etickets_'.$user->id);
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            if ($blocked) {
                return response()->json(['success' => 'card blocked ajax']);
            }
            return response()->json(['error' => 'error card blocked ajax']);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxUnblockCard(Request $request)
    {
        if ($this->isAjax()) {
            $user = \Auth::user();
            $id = (int) $request->input('eticket_id');
            /** @var ETicketService $service */
            $service = app('ETicketService');
            try {
                $blocked = $service->unblockCard($id);
                \Cache::forget('active_etickets_'.$user->id);
                \Cache::forget('all_active_etickets_'.$user->id);
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            if ($blocked) {
                return response()->json(['success' => 'card unblocked ajax']);
            }
            return response()->json(['error' => 'error card unblocked ajax']);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxDeleteCard(Request $request)
    {
        if ($this->isAjax()) {
            $id = (int) $request->input('eticket_id');
            /** @var ETicketService $service */
            $service = app('ETicketService');
            try {
                $deleted = $service->deleteCard($id);
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            if ($deleted) {
                return response()->json(['success' => 'card deleted ajax']);
            }
            return response()->json(['error' => 'error card deleted ajax']);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxWeekExpenses(Request $request)
    {
        if ($this->isAjax()) {
            $offset = (int) $request->input('offset');
            /** @var ETicketService $service */
            $service = app('ETicketService');
            try {
                $start = $service->getDateFromOffset($offset);
                $end = $service->getDateFromOffset($offset, false);
                $current = (int) (new \DateTime('now'))->format('W');
                $current = $offset + $current;
                $payments = $service->getWeekendPayments($offset);
                $html = [];
                foreach ($payments as $i => $payment) {
                    $html[$i] = $payment->html->render();
                }
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            return response()->json(
                [
                    'success' => 'week expenses ajax',
                    'html' => $html,
                    'start' => $start,
                    'end' => $end,
                    'current' => $current
                ]
            );
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxNotifications(Request $request)
    {
        if ($this->isAjax()) {
            /** @var ETicketService $service */
            $service = app('ETicketService');
            try {
                $notifications = $service->getNotifications($request);
                $perPage = (int) $request->input('per_page');
                $html = view(
                    'portal.views.partials.profile.table_notifications',
                    compact('notifications')
                )->render();
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            return response()->json(['success' => 'payments ajax', 'html' => $html, 'per_page' => $perPage]);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxPayments(Request $request)
    {
        if ($this->isAjax()) {
            /** @var ETicketService $service */
            $service = app('ETicketService');
            try {
                $payments = $service->getInvoices($request);
                $perPage = (int) $request->input('per_page');
                $html = view(
                    'portal.views.partials.profile.table_payments',
                    compact('payments')
                )->render();
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            return response()->json(['success' => 'payments ajax', 'html' => $html, 'per_page' => $perPage]);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    public function ajaxDeletePayment(Request $request)
    {
        if ($this->isAjax()) {
            /** @var ETicketService $service */
            $service = app('ETicketService');
            $id = $request->input('id');
            try {
                $service->deletePayment($id);
            } catch (\RuntimeException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
            return response()->json(['success' => 'delete payment ajax']);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \BadFunctionCallException
     */
    public function ajaxAddCard(Request $request)
    {
        if ($this->isAjax()) {
            $params = $request->all();
            if (isset($params['\\'])) {
                unset($params['\\']);
            }
            $params['user_id'] = \Auth::id();
            $params['i'] = 0;
            $user = \Auth::user();
            $bag = new MessageBag();
            /** @var CardService $service */
            $service = app('CardService');
            /** @var MessageBag $bag */
            $bag = $service->saveCard(0, $params, $bag);
            $card = $service->getLastCard();
            if ($card instanceof Card && !$bag->any()) {
                try {
                    /** @var MessageBag $bag */
                    $bag = $service->createTicket($card, $bag);
                    if (!$bag->any()) {
                        $user->has_card = true;
                        $user->card_not_finished = true;
                        $user->save();
                        return response()->json(['success' => 'add card ajax']);
                    }
                } catch (\RuntimeException $e) {
                    \Log::error('user not saved - '.$e->getMessage());
                    $bag->add('user_not_saved', $e->getMessage());
                }
            }

            return response()->json(['error' => $bag->toArray()]);
        } else {
            throw new \BadFunctionCallException('Only for ajax request');
        }
    }
}
