<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.7.9
 * Time: 12:32
 */

namespace App\Http\Controllers;

/**
 * Class RedirectController
 * @package App\Http\Controllers
 */
class RedirectController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirect()
    {
        $redirectRk = config('auth.views.web.rk_path');
        $redirectCms = config('auth.views.web.cms_path');
        $redirectAdmin = config('auth.views.web.admin_path');
        $redirectSchool = config('auth.views.web.school_path');
        $redirectSeller = config('auth.views.web.seller_path');
        $redirectUser = config('auth.views.web.user_redirect_path');
        $redirectAccountant = config('auth.views.web.accountant_path');
        $user = \Auth::user();
        $roles = $user->currentRoleIds();
//        if (!$user->hasPk() || !$user->isFirstConfirmed()) {
//            return redirect()->to($redirectUser);
//        }
        if (in_array(1, $roles, false)) {
            return redirect()->to($redirectAdmin);
        } elseif (in_array(2, $roles, false)) {
            return redirect()->to($redirectSchool);
        } elseif (in_array(3, $roles, false)) {
            return redirect()->to($redirectSeller);
        } elseif (in_array(4, $roles, false)) {
            return redirect()->to($redirectAccountant);
        } elseif (in_array(5, $roles, false)) {
            return redirect()->to($redirectRk);
        } elseif (in_array(6, $roles, false)) {
            return redirect()->to($redirectCms);
        } else {
            return redirect()->to($redirectUser);
        }
    }
}
