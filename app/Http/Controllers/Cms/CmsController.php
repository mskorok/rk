<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.12.9
 * Time: 12:37
 */

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\AdminController;

class CmsController extends AdminController
{
    public function getIndex()
    {
        return view('cms.index');
    }
}
