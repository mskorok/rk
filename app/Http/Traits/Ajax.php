<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.11.7
 * Time: 10:40
 */

namespace App\Http\Traits;

trait Ajax
{
    public function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])
        && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    }
}
