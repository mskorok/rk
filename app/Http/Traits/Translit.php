<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.1.8
 * Time: 14:02
 */

namespace App\Http\Traits;

trait Translit
{
    protected function translit($str)
    {
        $s = $str;
        $s = preg_replace('|\&[^;]+;|U', '', $s);
        $s = strip_tags($s);

        $s = str_replace('a', 'a', $s);
        $s = str_replace('c', 'c', $s);
        $s = str_replace('e', 'e', $s);
        $s = str_replace('g', 'g', $s);
        $s = str_replace('i', 'i', $s);
        $s = str_replace('k', 'k', $s);
        $s = str_replace('l', 'l', $s);
        $s = str_replace('n', 'n', $s);
        $s = str_replace('�', 's', $s);
        $s = str_replace('u', 'u', $s);
        $s = str_replace('�', 'z', $s);

        $s = $this->numericEntityUtf8($s);

        // lat
        $s = str_replace('&#257;', 'a', $s);
        $s = str_replace('&#269;', 'c', $s);
        $s = str_replace('&#275;', 'e', $s);
        $s = str_replace('&#291;', 'g', $s);
        $s = str_replace('&#299;', 'i', $s);
        $s = str_replace('&#311;', 'k', $s);
        $s = str_replace('&#316;', 'l', $s);
        $s = str_replace('&#326;', 'n', $s);
        $s = str_replace('&#353;', 's', $s);
        $s = str_replace('&#363;', 'u', $s);
        $s = str_replace('&#382;', 'z', $s);
        // lat CAPITAL
        $s = str_replace('&#256;', 'a', $s);
        $s = str_replace('&#268;', 'c', $s);
        $s = str_replace('&#274;', 'e', $s);
        $s = str_replace('&#290;', 'g', $s);
        $s = str_replace('&#298;', 'i', $s);
        $s = str_replace('&#310;', 'k', $s);
        $s = str_replace('&#315;', 'l', $s);
        $s = str_replace('&#325;', 'n', $s);
        $s = str_replace('&#352;', 's', $s);
        $s = str_replace('&#362;', 'u', $s);
        $s = str_replace('&#381;', 'z', $s);
        // rus
        $s = str_replace('&#1081;', 'i', $s);
        $s = str_replace('&#1094;', 'ch', $s);
        $s = str_replace('&#1091;', 'u', $s);
        $s = str_replace('&#1082;', 'k', $s);
        $s = str_replace('&#1077;', 'e', $s);
        $s = str_replace('&#1085;', 'n', $s);
        $s = str_replace('&#1075;', 'g', $s);
        $s = str_replace('&#1096;', 'sh', $s);
        $s = str_replace('&#1097;', 'sh', $s);
        $s = str_replace('&#1079;', 'z', $s);
        $s = str_replace('&#1093;', 'h', $s);
        $s = str_replace('&#1098;', '', $s);
        $s = str_replace('&#1092;', 'f', $s);
        $s = str_replace('&#1099;', 'i', $s);
        $s = str_replace('&#1074;', 'v', $s);
        $s = str_replace('&#1072;', 'a', $s);
        $s = str_replace('&#1087;', 'p', $s);
        $s = str_replace('&#1088;', 'r', $s);
        $s = str_replace('&#1086;', 'o', $s);
        $s = str_replace('&#1083;', 'l', $s);
        $s = str_replace('&#1076;', 'd', $s);
        $s = str_replace('&#1078;', 'zh', $s);
        $s = str_replace('&#1101;', 'e', $s);
        $s = str_replace('&#1103;', 'ja', $s);
        $s = str_replace('&#1095;', 'ch', $s);
        $s = str_replace('&#1089;', 's', $s);
        $s = str_replace('&#1084;', 'm', $s);
        $s = str_replace('&#1080;', 'i', $s);
        $s = str_replace('&#1090;', 't', $s);
        $s = str_replace('&#1100;', '', $s);
        $s = str_replace('&#1073;', 'b', $s);
        $s = str_replace('&#1102;', 'ju', $s);
        $s = str_replace('&#1105;', 'jo', $s);
        // rus CAPITAL
        $s = str_replace('&#1049;', 'i', $s);
        $s = str_replace('&#1062;', 'ch', $s);
        $s = str_replace('&#1059;', 'u', $s);
        $s = str_replace('&#1050;', 'k', $s);
        $s = str_replace('&#1045;', 'e', $s);
        $s = str_replace('&#1053;', 'n', $s);
        $s = str_replace('&#1043;', 'g', $s);
        $s = str_replace('&#1064;', 'sh', $s);
        $s = str_replace('&#1065;', 'sh', $s);
        $s = str_replace('&#1047;', 'z', $s);
        $s = str_replace('&#1061;', 'h', $s);
        $s = str_replace('&#1066;', '', $s);
        $s = str_replace('&#1060;', 'f', $s);
        $s = str_replace('&#1067;', 'i', $s);
        $s = str_replace('&#1042;', 'v', $s);
        $s = str_replace('&#1040;', 'a', $s);
        $s = str_replace('&#1055;', 'p', $s);
        $s = str_replace('&#1056;', 'r', $s);
        $s = str_replace('&#1054;', 'o', $s);
        $s = str_replace('&#1051;', 'l', $s);
        $s = str_replace('&#1044;', 'd', $s);
        $s = str_replace('&#1046;', 'zh', $s);
        $s = str_replace('&#1069;', 'e', $s);
        $s = str_replace('&#1071;', 'ja', $s);
        $s = str_replace('&#1063;', 'ch', $s);
        $s = str_replace('&#1057;', 's', $s);
        $s = str_replace('&#1052;', 'm', $s);
        $s = str_replace('&#1048;', 'i', $s);
        $s = str_replace('&#1058;', 't', $s);
        $s = str_replace('&#1068;', '', $s);
        $s = str_replace('&#1041;', 'b', $s);
        $s = str_replace('&#1070;', 'ju', $s);
        $s = str_replace('&#1025;', 'jo', $s);
        // swe
        $s = str_replace('&#5767168;', 'o', $s);
        $s = str_replace('&#16384;', 'a', $s);
        $s = str_replace('&#20480;', 'a', $s);
        // swe CAPITAL
        $s = str_replace('&#1408;', 'o', $s);
        $s = str_replace('&#320;', 'a', $s);
        // nor
        $s = str_replace('&#24576;', 'ae', $s);
        // nor CAPITAL
        $s = str_replace('&#384;', 'ae', $s);

        $s = strtolower($s);
        $s = preg_replace('|[^a-z0-9_\ \,\.\-]|', '', $s);
        $s = chop(trim($s));
        //$s = str_replace(' ', '_', $s);
        //$s = str_replace('-', '_', $s);
        //$s = str_replace(',', '_', $s);
        //$s = str_replace('.', '_', $s);
        while (strpos($s, '__') !== false) {
            $s = str_replace('__', '_', $s);
        }
        if (substr($s, -1) === '_') {
            $s = substr($s, 0, -1);
        }
        return($s);
    }
    protected function numericEntityUtf8($utf8_string)
    {
        $out = '';
        $ns = strlen($utf8_string);
        for ($nn = 0; $nn < $ns; $nn++) {
            $ch = $utf8_string [$nn];
            $ii = ord($ch);

            if ($ii < 128) {
                $out .= $ch;
            } elseif ($ii >>5 == 6) {
                $b1 = ($ii & 31);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b2 = ($ii & 63);

                $ii = ($b1 * 64) + $b2;

                $ent = sprintf('&#%d;', $ii);
                $out .= $ent;
            } elseif ($ii >>4 == 14) {
                $b1 = ($ii & 31);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b2 = ($ii & 63);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b3 = ($ii & 63);

                $ii = ((($b1 * 64) + $b2) * 64) + $b3;

                $ent = sprintf('&#%d;', $ii);
                $out .= $ent;
            } elseif ($ii >>3 == 30) {
                $b1 = ($ii & 31);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b2 = ($ii & 63);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b3 = ($ii & 63);

                $nn++;
                $ch = $utf8_string [$nn];
                $ii = ord($ch);
                $b4 = ($ii & 63);

                $ii = ((((($b1 * 64) + $b2) * 64) + $b3) * 64) + $b4;

                $ent = sprintf('&#%d;', $ii);
                $out .= $ent;
            }
        }
        return $out;
    }
}
