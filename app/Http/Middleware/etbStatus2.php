<?php

namespace App\Http\Middleware;

use App\Models\EticketsBase;
use Closure;
use Illuminate\Support\Facades\Redirect;

class etbStatus2
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $eticket_id = $request->segment(3);

        $objekts = EticketsBase::find($eticket_id);

        if ($objekts->custom != 2) {
            return Redirect::to('skola/bindex')->with('error', \Lang::get('etickets/etickets/messages.does_not_exist'));
        }
        return $next($request);
    }
}
