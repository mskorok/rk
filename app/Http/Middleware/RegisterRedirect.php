<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.27.7
 * Time: 16:23
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RegisterRedirect
{
    public function handle(Request $request, Closure $next)
    {
        $user = \Auth::user();
        $route = $request->route()->getName();

        if ($route !== 'success'
            && $route !== 'require'
            && $route !== 'registerSteps'
            && $route !== 'thanks'
            && $route !== 'eCard'
            && $user->isRkConfirmed()
            && $user->isFirstConfirmed()
            && $user->hasPk()
        ) {
            return $next($request); // if user is rk confirmed and it`s not register routes to do nothing
        }

        if (($route === 'success'
            || $route === 'require'
            || $route === 'registerSteps'
            || $route === 'thanks'
            || $route === 'eCard') && $user->isRkConfirmed() && $user->isFirstConfirmed() && $user->hasPk()
        ) {
            // if user rk confirmed and it`s register routes - redirect to dashboard
            return redirect()->to(route('dashboard'));
        }

        if ($route !== 'success'
            && $route !== 'require'
            && $route !== 'registerSteps'
            && $route !== 'thanks'
            && $route !== 'eCard'
            && $route !== 'dashboard'
            && $route !== 'profile'
            && $route !== 'edit_profile'
            && $user->isFirstConfirmed()
            && $user->hasPk()
        ) {
            return $next($request);
        }

        if ($route !== 'success'
            && $route !== 'require'
            && $route !== 'registerSteps'
            && $route !== 'thanks'
            && $route !== 'eCard'
            && $user->refillAccountIsNotCreated()
            && $user->isFirstConfirmed()
            && $user->hasPk()
        ) {
            return $next($request);// dashboard, profile, edit_profile
        }

        if ($route === 'registerSteps'
            && !$user->isFirstConfirmed()
            && !$user->hasPk()
        ) {
            return $next($request);
        }

        if (!$user->isActivated()) {
            if ($route !== 'success') {
                return redirect()->to(route('success'));
            }
        } elseif ($user->isActivated() && !$user->isBankConfirmed()) {
            if ($route !== 'require') {
                return redirect()->to(route('require'));
            }
        } elseif ($user->isActivated()
            && !$user->isProfileFilled()) {
            if ($route !== 'registerSteps') {
                return redirect()->to(route('registerSteps'));
            }
        } elseif (!$user->isFirstConfirmed()) {
            if ($route !== 'thanks') {
                return redirect()->to(route('thanks'));
            }
        } elseif (!$user->hasCard()) {
            if ($route !== 'eCard') {
                return redirect()->to(route('eCard'));
            }
        } elseif (!$user->cardIsNotFinished()) {
            if ($route !== 'eCard') {
                return redirect()->to(route('eCard'));
            }
        } elseif (!$user->refillAccountIsNotCreated()) {
            if ($route !== 'eCard') {
                return redirect()->to(route('eCard'));
            }
        } else {
            if ($route === 'profile' || $route === 'dashboard' || $route === 'edit_profile') {
                return $next($request);
            } else {
                return redirect()->to(route('dashboard'));
            }
        }
        return $next($request);
    }
}
