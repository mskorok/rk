<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;

class objectuser
{
    /**
     * parbaudam vai lietotajs ir objekts vai administrators
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $u = Auth::user();

        if (isset($u)) {
            $roleids = Cache::remember('roleids_' . $u->id, 30, function () use ($u) {
                return $u->currentRoleIds();
            });

            if (count($roleids) > 0 && (in_array(2, $roleids, false) || in_array(1, $roleids, false))) {
                return $next($request);
            }
        }

        return Redirect::to('/')->with('error', 'Nav atrasts!');
    }
}
