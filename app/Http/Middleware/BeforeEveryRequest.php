<?php

namespace App\Http\Middleware;

use App\Models\Settings;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class BeforeEveryRequest
{
    /**
     * Global checks
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array($request->segment(1), Config::get('app.languages'))) {
            Session::put('locale', $request->segment(1));
            $uri = \Request::getRequestUri();
            $path = \Request::path();
            $tre = substr(\Request::getRequestUri(), 3);
            return Redirect::to(substr(\Request::getRequestUri(), 3));
        }
        if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        }

        if (Auth::check() AND $request->segment(2)!='forceterms' AND $request->segment(2)!='logout' AND $request->segment(1)!='terms' AND $request->segment(1)!='user')
        {
            $terms = Auth::user()->terms;
            $u = Auth::user();
            $roleids = Cache::remember('roleids_'.$u->id, 30, function () use ($u) {
                return $u->currentRoleIds();
            });

            if ($terms == 2 && count($roleids)==0) {
                return Redirect::to('user/forceterms/');
            }
        }

        $settings = Cache::rememberForever('settings', function () {
            return Settings::get()->first();
        });
        $yearly_date = $settings->yearly_date;

        // check for yearly info update
        $yearly_date = substr(trim($yearly_date), 5, 9);

        if (Auth::check() AND $request->segment(1)!='yearly' AND $request->segment(1)!='user' AND date("m-d") == $yearly_date AND intval(substr(Auth::user()->yearly_updated,0,4)) != intval(date("Y")))
            return Redirect::to('user/yearly/');

        return $next($request);
    }
}
