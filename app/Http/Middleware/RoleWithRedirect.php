<?php

namespace App\Http\Middleware;

use Closure;
use Laratrust\Middleware\LaratrustRole;
use Illuminate\Contracts\Auth\Guard;

class RoleWithRedirect extends LaratrustRole
{
    protected $redirectPath;


    public function __construct(Guard $auth)
    {
        parent::__construct($auth);
        $this->redirectPath = config('auth.views.web.login_path');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        if (!is_array($roles)) {
            $roles = explode(self::DELIMITER, $roles);
        }

        if ($this->auth->guest() || !$request->user()->hasRole($roles)) {
            return redirect($this->redirectPath);
        }

        return $next($request);
    }
}
