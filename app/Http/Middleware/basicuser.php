<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;

class basicuser
{
    /**
     * parbaudam vai lietotajs ir bez lomam vai ar admin lomu
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $u = Auth::user();

        if (isset($u)) {
            $roleids = Cache::remember('roleids_' . $u->id, 30, function () use ($u) {
                return $u->currentRoleIds();
            });

            // admin
            if (count($roleids) > 0 && in_array(1, $roleids, false)) {
                return $next($request);
            }

            // rk
            if (count($roleids)>0 && in_array(5, $roleids, false)) {
                return $next($request);
            }

            // no roles
            if (count($roleids) === 0) {
                return $next($request);
            }
        }

        return Redirect::to('/')->with('error', 'Nav atrasts!');
    }
}
