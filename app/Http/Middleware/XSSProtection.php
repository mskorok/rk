<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

class XSSProtection
{
    /**
     * The following method loops through all request input and strips out all tags from
     * the request. This to ensure that users are unable to set ANY HTML within the form
     * submissions, but also cleans up input.
     *
     * @param Request $request
     * @param callable|\Closure $next
     * @return mixed
     */

    public function handle(Request $request, \Closure $next)
    {
        // TMP FIX for prod
        if (in_array(strtolower($request->method()), ['get'], false)) {
            $all = $request->all();
            foreach ($all as $key => $input) {
                $key = str_replace('\\', "", $key);
                $all[$key] = $input;
            }
            $request->replace($all);
        }

        if (!in_array(strtolower($request->method()), ['put', 'post'], false)) {
            return $next($request);
        }

        $input = $request->except("\\");

        array_walk_recursive($input, function (&$input) {
            if (is_string($input)) {
                $input = strip_tags($input, '<p><a><br><li><ul><b><strong><br/><br /><ol>');
            }
        });

        $request->merge($input);

        return $next($request);
    }
}
