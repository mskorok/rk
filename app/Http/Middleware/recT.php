<?php

namespace App\Http\Middleware;

use App\Models\Ticket;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class recT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ticket_id = $request->segment(3);

        $ticket = Ticket::find($ticket_id);

        $user_id = Auth::user()->id;

        if ($ticket->recipient_id != $user_id) {
            return Redirect::to('etickets/etickets')->with('error', 'Nav atrasts!');
        } elseif ($ticket->ticket_status != 0) {
            return Redirect::to('etickets/etickets')->with('error', 'Nav atrasts!');
        }

        return $next($request);
    }
}
