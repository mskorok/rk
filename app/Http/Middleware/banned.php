<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class banned
{
    /**
     * banned IP check
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip_array = Cache::rememberForever('banned_ip_array', function () {
            $banned_ips = trim('1.2.3.4');
            return array_map('trim', explode(",", $banned_ips));
        });

        if (! App::environment('testing') && in_array($_SERVER['REMOTE_ADDR'], $ip_array, false)) {
            exit('Your IP address is blocked!');
        }

        return $next($request);
    }
}
