<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\PermittedIp;

class UnderConstruction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $ip = trim($_SERVER["REMOTE_ADDR"]);
//        $exist = PermittedIp::where('ip', $ip)->pluck('id')->first();
//        if (empty($exist)) {
//            return redirect()->route('blocked_ip');
//        }
        return $next($request);
    }
}
