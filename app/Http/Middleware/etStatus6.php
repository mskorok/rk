<?php

namespace App\Http\Middleware;

use App\Models\Eticket;
use Closure;
use Illuminate\Support\Facades\Redirect;

class etStatus6
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $eticket_id = $request->segment(3);

        $objekts = Eticket::find($eticket_id);

        if ($objekts->eticket_status !=6) {
            return Redirect::to('rk/index')->with('error', \Lang::get('etickets/etickets/messages.does_not_exist'));
        }
        return $next($request);
    }
}
