<?php

namespace App\Http\Middleware;

use App\Models\Account;
use App\Models\Eticket;
use App\Models\OwnerAccess;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;

class ownerEt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $eticket_id = $request->segment(3);

        $objekts = Eticket::find($eticket_id);

        $all_accounts = Cache::remember('user_accounts_et_'.Auth::user()->id, 30, function () {
            // get all owners for user
            $all_owners = array();
            $owners = OwnerAccess::with('owner')->Ao()->U(Auth::user()->id)->get();
            foreach ($owners as $o) {
                array_push($all_owners, $o->owner->id);
            }

            // get all eticket accounts (type=2) for owners
            $all_accounts = array();
            foreach ($all_owners as $o) {
                $accounts = Account::O($o)->Et()->get();
                foreach ($accounts as $a) {
                    array_push($all_accounts, $a->id);
                }
            }

            return $all_accounts;
        });

        if (!in_array($objekts->account_id, $all_accounts, false)) {
            return Redirect::to('etickets/etickets')
                ->with('error', Lang::get('etickets/etickets/messages.does_not_exist').' Kļūda atlasot kontu!');
        }
        return $next($request);
    }
}
