<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Support\Facades\App;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '*fetchaccess',
        'user/register',
        'ret/*',
        'portal/feedback',
        'portal/profile/save',
        'portal/profile/delete',
        'portal/profile/delete/check',
        'portal/profile/change/email'
    ];

    public function handle($request, Closure $next)
    {
        if ('testing' !== App::environment()) {
            return parent::handle($request, $next);
        }

        return $next($request);
    }
}
