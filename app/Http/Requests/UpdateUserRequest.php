<?php

namespace App\Http\Requests;

class UpdateUserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'email|required',
            'cpassword' => 'required'
        ];
    }
}
