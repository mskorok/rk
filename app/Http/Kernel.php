<?php

namespace App\Http;

use App\Http\Middleware\RegisterRedirect;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        \App\Http\Middleware\BeforeEveryRequest::class,
        \App\Http\Middleware\XSSProtection::class,
        \App\Http\Middleware\banned::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'xss' => \App\Http\Middleware\XSSProtection::class,
        'basicuser' => \App\Http\Middleware\basicuser::class,
        'rkuser' => \App\Http\Middleware\rkuser::class,
        'objectuser' => \App\Http\Middleware\objectuser::class,
        'banned' => \App\Http\Middleware\banned::class,
        'ownerEt' => \App\Http\Middleware\ownerEt::class,
        'etStatus5' => \App\Http\Middleware\etStatus5::class,
        'etStatus6' => \App\Http\Middleware\etStatus6::class,
        'etbStatus2' => \App\Http\Middleware\etbStatus2::class,
        'bothT' => \App\Http\Middleware\bothT::class,
        'recT' => \App\Http\Middleware\recT::class,
        'role' => \App\Http\Middleware\RoleWithRedirect::class,
        'permission' => \Laratrust\Middleware\LaratrustPermission::class,
        'ability' => \Laratrust\Middleware\LaratrustAbility::class,
        'rr' => RegisterRedirect::class,
        'underConstr' => \App\Http\Middleware\UnderConstruction::class,
    ];
}
