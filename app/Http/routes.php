<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

\Route::model('user', 'App\Models\User');
\Route::model('mpos', 'App\Models\Mpos');
\Route::model('objekts', 'App\Models\Objekts');
\Route::model('auths', 'App\Models\Auths');
\Route::model('etickets', 'App\Models\Eticket');
\Route::model('eticketb', 'App\Models\EticketsBase');
\Route::model('seller', 'App\Models\Seller');
\Route::model('sett', 'App\Models\Settings');
\Route::model('banner', 'App\Models\Banner');
\Route::model('slider', 'App\Models\TopSlider');
\Route::model('ticket', 'App\Models\Ticket');
\Route::model('conn', 'App\Models\OwnerAccess');
\Route::model('objdotations', 'App\Models\ObjectDotationPrices');
\Route::model('objgroup', 'App\Models\ObjGroup');
\Route::model('roles', 'App\Models\Role');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */

\Route::group(
    ['prefix' => 'admin', 'namespace' => 'admin', 'middleware' => ['auth','role:Administrators']],
    function () {
    # User Management
        \Route::get('users/{user}/show', 'AdminUsersController@getShow')
            ->where('user', '[0-9]+');
        \Route::get('users/{user}/edit', 'AdminUsersController@getEdit')
            ->where('user', '[0-9]+');
        \Route::post('users/{user}/edit', 'AdminUsersController@postEdit')
            ->where('user', '[0-9]+');
        \Route::get('users/{user}/delete', 'AdminUsersController@getDelete')
            ->where('user', '[0-9]+');
        \Route::post('users/{user}/delete', 'AdminUsersController@postDelete')
            ->where('user', '[0-9]+');
        \Route::controller('users', 'AdminUsersController');

    # MPOS Management
        \Route::get('mpos/{mpos}/fw', 'AdminMposController@getFw')
            ->where('mpos', '[0-9]+');
        \Route::post('mpos/{mpos}/fw', 'AdminMposController@postFw')
            ->where('mpos', '[0-9]+');
        \Route::get('mpos/{mpos}/conf', 'AdminMposController@getConf')
            ->where('mpos', '[0-9]+');
        \Route::get('mpos/{mpos}/restart', 'AdminMposController@getRestart')
            ->where('mpos', '[0-9]+');
        \Route::get('mpos/{mpos}/delete', 'AdminMposController@getDelete')
            ->where('mpos', '[0-9]+');
        \Route::post('mpos/{mpos}/delete', 'AdminMposController@postDelete')
            ->where('mpos', '[0-9]+');
        \Route::get('mpos/{mpos}/edit', 'AdminMposController@getEdit')
            ->where('mpos', '[0-9]+');
        \Route::post('mpos/{mpos}/edit', 'AdminMposController@postEdit')
            ->where('mpos', '[0-9]+');
        \Route::controller('mpos', 'AdminMposController');

    # Objects Management
        \Route::get('objekti/{objekts}/delete', 'AdminObjektsController@getDelete')
            ->where('objekts', '[0-9]+');
        \Route::post('objekti/{objekts}/delete', 'AdminObjektsController@postDelete')
            ->where('objekts', '[0-9]+');
        \Route::get('objekti/{objekts}/edit', 'AdminObjektsController@getEdit')
            ->where('objekts', '[0-9]+');
        \Route::post('objekti/{objekts}/edit', 'AdminObjektsController@postEdit')
            ->where('objekts', '[0-9]+');
        \Route::controller('objekti', 'AdminObjektsController');

    # Authorisators Management
        \Route::get('auths/{auths}/delete', 'AdminAuthsController@getDelete')
            ->where('auths', '[0-9]+');
        \Route::post('auths/{auths}/delete', 'AdminAuthsController@postDelete')
            ->where('auths', '[0-9]+');
        \Route::get('auths/{auths}/edit', 'AdminAuthsController@getEdit')
            ->where('auths', '[0-9]+');
        \Route::post('auths/{auths}/edit', 'AdminAuthsController@postEdit')
            ->where('auths', '[0-9]+');
        \Route::controller('auths', 'AdminAuthsController');

    # E-tickets Management
        \Route::controller('etickets', 'AdminEticketsController');

    # Sellers Management
        \Route::get('sellers/{seller}/delete', 'AdminSellersController@getDelete')
            ->where('seller', '[0-9]+');
        \Route::post('sellers/{seller}/delete', 'AdminSellersController@postDelete')
            ->where('seller', '[0-9]+');
        \Route::get('sellers/{seller}/edit', 'AdminSellersController@getEdit')
            ->where('seller', '[0-9]+');
        \Route::post('sellers/{seller}/edit', 'AdminSellersController@postEdit')
            ->where('seller', '[0-9]+');
        \Route::controller('sellers', 'AdminSellersController');

    # Settings
        \Route::get('settings/{sett}/edit', 'AdminSettingsController@getEdit')
            ->where('sett', '[0-9]+');
        \Route::post('settings/{sett}/edit', 'AdminSettingsController@postEdit')
            ->where('sett', '[0-9]+');
        \Route::controller('settings', 'AdminSettingsController');

    # Banners
        \Route::get(
            'banners/{banner}/edit',
            [
                'uses' => 'AdminBannersController@getEdit',
                'as' => 'banner_edit_get'
            ]
        )
            ->where('banner', '[0-9]+');
        \Route::post(
            'banners/{banner}/edit',
            [
                'uses' => 'AdminBannersController@postEdit',
                'as' => 'banner_edit_post'
            ]
        )->where('banner', '[0-9]+');
        \Route::get(
            'banners/{banner}/delete',
            [
                'uses' => 'AdminBannersController@delete',
                'as' => 'banner_delete'
            ]
        )->where('banner', '[0-9]+');
        \Route::get(
            'banners/create',
            [
                'uses' => 'AdminBannersController@getCreate',
                'as' => 'banner_create_get'
            ]
        );
        \Route::post(
            'banners/create',
            [
                'uses' => 'AdminBannersController@postCreate',
                'as' => 'banner_create_post'
            ]
        );

        \Route::controller('banners', 'AdminBannersController');

    # TopSlider
        \Route::get(
            'top_slider/{slider}/edit',
            [
                'uses' => 'AdminSliderController@getEdit',
                'as' => 'slider_edit_get'
            ]
        )->where('slider', '[0-9]+');
        \Route::post(
            'top_slider/{slider}/edit',
            [
                'uses' => 'AdminSliderController@postEdit',
                'as' => 'slider_edit_post'
            ]
        )
            ->where('slider', '[0-9]+');
        \Route::get(
            'top_slider/create',
            [
                'uses' => 'AdminSliderController@getCreate',
                'as' => 'slider_create_get'
            ]
        );
        \Route::post(
            'top_slider/create',
            [
                'uses' => 'AdminSliderController@postCreate',
                'as' => 'slider_create_post'
            ]
        );
        \Route::controller('top_slider', 'AdminSliderController');

    # Import
        \Route::controller('import', 'AdminImportController');
        \Route::get('newbase/{b}/edit', 'NewBaseController@getEdit')
            ->where('b', '[0-9]+');
        \Route::controller('newbase', 'NewBaseController');

        \Route::get('neweticket/{b}/edit', 'NewEticketController@getEdit')
            ->where('b', '[0-9]+');
        \Route::controller('neweticket', 'NewEticketController');

        \Route::controller('newtrans', 'NewTransactionController');

        \Route::get('nullestransakcijas', 'NewTransactionController@zero');
        \Route::get('emaksimport', 'EmaksImportController@index');

    # Tickets
        \Route::get('tickets/{ticket}/confirm', 'AdminTicketsController@getConfirm')
            ->where('ticket', '[0-9]+');
        \Route::post('tickets/{ticket}/confirm', 'AdminTicketsController@postConfirm')
            ->where('ticket', '[0-9]+');
        \Route::get('tickets/{ticket}/cancel', 'AdminTicketsController@getCancel')
            ->where('ticket', '[0-9]+');
        \Route::post('tickets/{ticket}/cancel', 'AdminTicketsController@postCancel')
            ->where('ticket', '[0-9]+');
        \Route::controller('tickets', 'AdminTicketsController');

    #Reports
        \Route::get('reports/global', 'ReportsGlobalController@getIndex');
        \Route::post('reports/global', 'ReportsGlobalController@getIndex');

    #Change to user
        \Route::get('changeuser', 'ChangeUserController@getIndex');
        \Route::get('changeuser/index', 'ChangeUserController@getIndex');
        \Route::post('changeuser/change', 'ChangeUserController@postChange');
        \Route::get('changeuser/fetch/{q}', 'ChangeUserController@getFetch');

    # Dotations Management
        \Route::get('dotations/{objdotations}/edit', 'AdminDotationsController@getEdit')
            ->where('objdotations', '[0-9]+');
        \Route::post('dotations/{objdotations}/edit', 'AdminDotationsController@postEdit')
            ->where('objdotations', '[0-9]+');
        \Route::controller('dotations', 'AdminDotationsController');

    # Grouping objects
        \Route::get('objgroup/{objgroup}/delete', 'AdminobjgroupController@getDelete')
            ->where('objgroup', '[0-9]+');
        \Route::post('objgroup/{objgroup}/delete', 'AdminobjgroupController@postDelete')
            ->where('objgroup', '[0-9]+');
        \Route::get('objgroup/{objgroup}/edit', 'AdminobjgroupController@getEdit')
            ->where('objgroup', '[0-9]+');
        \Route::post('objgroup/{objgroup}/edit', 'AdminobjgroupController@postEdit')
            ->where('objgroup', '[0-9]+');
        \Route::controller('objgroup', 'AdminObjGroupController');

    # Money return
        \Route::get('returnmoney', 'ReturnMoneyController@getIndex');
        \Route::get('returnmoney/index', 'ReturnMoneyController@getIndex');
        \Route::post('returnmoney/search', 'ReturnMoneyController@postSearch');
        \Route::get('returnmoney/return/{id}', 'ReturnMoneyController@getReturn')
            ->where('id', '[0-9]+');

    # Money transfer
        \Route::get('transfermoney', 'TransferMoneyController@getIndex');
        \Route::get('transfermoney/index', 'TransferMoneyController@getIndex');
        \Route::post('transfermoney/transfer', 'TransferMoneyController@postTransfer');
        \Route::get(
            'transfermoney/fetcheticket/{q}','TransferMoneyController@getFetch'
        )->where('q', '[a-z0-9 ]+');
        
    # User logged operations
        \Route::controller('loguser', 'AdminLogUserController');

        \Route::get('objektudotacijas', 'AdminObjektsDotacijasController@index');
        \Route::get('objektudotacijas/regen/{object_id}/', 'AdminObjektsDotacijasController@regen')
            ->where('object_id', '[0-9]+');

        \Route::get('logviewer', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    # Admin Dashboard
        \Route::get('/', 'AdminDashboardController@getIndex');
    //        Route::controller('/', 'AdminDashboardController');
        
    # Check Console Command
        \Route::get('check_console_command/{id}', 'CheckConsoleCommandController@getConsoleCommand')
            ->where('id', '[0-9]+');
        \Route::post('check_console_command/{id}', 'CheckConsoleCommandController@postConsoleCommand')
            ->where('id', '[0-9]+');
    }
);

# CMS list
\Route::group(
    ['prefix' => 'cms', 'middleware' => ['auth','role:CMS|Administrators', 'underConstr']],
    function () {
        # Banners
        \Route::get(
            'banners/{banner}/edit',
            [
                'uses' => 'Cms\CmsBannersController@getEdit',
                'as' => 'cms_banner_edit_get'
            ]
        )
            ->where('banner', '[0-9]+');
        \Route::post(
            'banners/{banner}/edit',
            [
                'uses' => 'Cms\CmsBannersController@postEdit',
                'as' => 'cms_banner_edit_post'
            ]
        )->where('banner', '[0-9]+');
        \Route::get(
            'banners/{banner}/delete',
            [
                'uses' => 'Cms\CmsBannersController@delete',
                'as' => 'cms_banner_delete'
            ]
        )->where('banner', '[0-9]+');
        \Route::get(
            'banners/create',
            [
                'uses' => 'Cms\CmsBannersController@getCreate',
                'as' => 'cms_banner_create_get'
            ]
        );
        \Route::post(
            'banners/create',
            [
                'uses' => 'Cms\CmsBannersController@postCreate',
                'as' => 'cms_banner_create_post'
            ]
        );

        \Route::controller('banners', 'Cms\CmsBannersController');

        # TopSlider
        \Route::get(
            'top_slider/{slider}/edit',
            [
                'uses' => 'Cms\CmsSliderController@getEdit',
                'as' => 'cms_slider_edit_get'
            ]
        )->where('slider', '[0-9]+');
        \Route::post(
            'top_slider/{slider}/edit',
            [
                'uses' => 'Cms\CmsSliderController@postEdit',
                'as' => 'cms_slider_edit_post'
            ]
        )
            ->where('slider', '[0-9]+');
        \Route::get(
            'top_slider/create',
            [
                'uses' => 'Cms\CmsSliderController@getCreate',
                'as' => 'cms_slider_create_get'
            ]
        );
        \Route::post(
            'top_slider/create',
            [
                'uses' => 'Cms\CmsSliderController@postCreate',
                'as' => 'cms_slider_create_post'
            ]
        );
        \Route::controller('top_slider', 'Cms\CmsSliderController');
        \Route::get(
            'translations/{group?}',
            [
                'uses' => 'Cms\CmsTranslationsController@getIndex',
                'as' => 'cms_translations'
            ]
        )->where('group', '.*');
        \Route::get(
            'translations_view/{group?}',
            [
                'uses' => 'Cms\CmsTranslationsController@getView',
                'as' => 'cms_translations_view'
            ]
        )->where('group', '.*');
        \Route::post(
            'translations_import',
            [
                'uses' => 'Cms\CmsTranslationsController@postImport',
                'as' => 'cms_translations_post_import'
            ]
        );
        \Route::post(
            'translations_find',
            [
                'uses' => 'Cms\CmsTranslationsController@postFind',
                'as' => 'cms_translations_post_find'
            ]
        );
        \Route::post(
            'translations_publish/{group}',
            [
                'uses' => 'Cms\CmsTranslationsController@postPublish',
                'as' => 'cms_translations_post_publish'
            ]
        )->where('group', '.*');
        \Route::post(
            'translations_edit/{group}',
            [
                'uses' => 'Cms\CmsTranslationsController@postEdit',
                'as' => 'cms_translations_post_edit'
            ]
        )->where('group', '.*');
        \Route::post(
            'translations_add/{group}',
            [
                'uses' => 'Cms\CmsTranslationsController@postAdd',
                'as' => 'cms_translations_post_add'
            ]
        )->where('group', '.*');
        \Route::post(
            'translations_delete/{group}/{key}',
            [
                'uses' => 'Cms\CmsTranslationsController@postDelete',
                'as' => 'cms_translations_post_delete'
            ]
        )->where('group', '.*');
        \Route::get('', 'Cms\CmsController@getIndex');
    }
);

# RK list
\Route::group(['prefix' => 'rk', 'middleware' => ['auth','role:RK|Administrators', 'underConstr']], function () {
    \Route::get(
        'index/{user}/cancel',
        ['middleware' => 'rkuser', 'uses' => '\App\Http\Controllers\rk\RkController@getCancel']
    )->where('user', '[0-9]+');
    \Route::post(
        'index/{user}/cancel',
        ['middleware' => 'rkuser', 'uses' => '\App\Http\Controllers\rk\RkController@postCancel']
    )->where('user', '[0-9]+');
    \Route::get(
        'index/{user}/confirm',
        ['middleware' => 'rkuser', 'uses' => '\App\Http\Controllers\rk\RkController@getConfirm']
    )->where('user', '[0-9]+');
    \Route::post(
        'index/{user}/confirm',
        ['middleware' => 'rkuser', 'uses' => '\App\Http\Controllers\rk\RkController@postConfirm']
    )->where('user', '[0-9]+');
    \Route::get(
        'index/{user}/deactivate',
        ['middleware' => 'rkuser', 'uses' => '\App\Http\Controllers\rk\RkController@getDeact']
    )->where('user', '[0-9]+');
    \Route::post(
        'index/{user}/deactivate',
        ['middleware' => 'rkuser', 'uses' => '\App\Http\Controllers\rk\RkController@postDeact']
    )->where('user', '[0-9]+');
    \Route::get('dldoc/{doc}', ['middleware' => 'rkuser', 'uses' => '\App\Http\Controllers\rk\RkController@dldoc']);
    \Route::get('data', ['middleware' => 'rkuser', 'uses' => '\App\Http\Controllers\rk\RkController@getData']);
    \Route::get('index', '\App\Http\Controllers\rk\RkController@getIndex');
    \Route::get('/', '\App\Http\Controllers\rk\RkController@getIndex');
});

\Route::group(['prefix' => 'etickets', 'middleware' => 'auth', 'underConstr'], function () {
    # E-tickets Management
    \Route::get(
        'etickets/{etickets}/del',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@getDel'
        ]
    );
    \Route::post(
        'etickets/{etickets}/del',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@postDel'
        ]
    );
    \Route::get(
        'etickets/{etickets}/edit',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@getEdit'
        ]
    );
    \Route::post(
        'etickets/{etickets}/edit',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@postEdit'
        ]
    );
    \Route::get(
        'etickets/{etickets}/disable',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@getDisable'
        ]
    );
    \Route::post(
        'etickets/{etickets}/disable',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@postDisable'
        ]
    );
    \Route::get(
        'etickets/{etickets}/enable',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@getEnable'
        ]
    );
    \Route::post(
        'etickets/{etickets}/enable',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@postEnable'
        ]
    );
    \Route::get(
        'etickets/{etickets}/put',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@getPut'
        ]
    );
    \Route::post(
        'etickets/{etickets}/put',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@postPut'
        ]
    );
    \Route::get(
        'etickets/{nr}/wait',
        [
            'middleware' => ['ownerEt','basicuser'],
            'uses' => '\App\Http\Controllers\etickets\EticketsController@getWait'
        ]
    );
    \Route::get('etickets/fetchdefobj/{q}', '\App\Http\Controllers\etickets\EticketsController@getFetchDefObj')
        ->where('q', '[0-9.]+');
    \Route::controller('etickets', '\App\Http\Controllers\etickets\EticketsController');

    # Incoming bank payments
    \Route::controller('mtransfer', '\App\Http\Controllers\etickets\TransferController');
    \Route::get('mtransfer/pdf/{pdf}', [
        'uses' => '\App\Http\Controllers\etickets\TransferController@getPdf',
        'as' => 'payment_pdf',
    ])->where('pdf', '[0-9]+');

    # Messages
    \Route::get(
        'messages/{ticket}/mconfirm',
        [
            'middleware' => 'recT',
            'uses' => '\App\Http\Controllers\etickets\MessagesController@getMconfirm'
        ]
    )->where('ticket', '[0-9]+');
    \Route::post(
        'messages/{ticket}/mconfirm',
        [
            'middleware' => 'recT',
            'uses' => '\App\Http\Controllers\etickets\MessagesController@postMconfirm'
        ]
    )->where('ticket', '[0-9]+');
    \Route::get(
        'messages/{ticket}/mcancel',
        [
            'middleware' => 'recT',
            'uses' => '\App\Http\Controllers\etickets\MessagesController@getMcancel'
        ]
    )->where('ticket', '[0-9]+');
    \Route::controller('messages', '\App\Http\Controllers\etickets\MessagesController');

    // Delete profile
    \Route::get(
        'delete',
        [
            'middleware' => 'basicuser',
            'uses' => '\App\Http\Controllers\etickets\DeleteController@getIndex'
        ]
    );
    \Route::post(
        'delete',
        [
            'middleware' => 'basicuser',
            'uses' => '\App\Http\Controllers\etickets\DeleteController@postIndex'
        ]
    );

    \Route::get('/', '\App\Http\Controllers\etickets\EticketsController@getIndex');
});

// Reports
\Route::group(['prefix' => 'reports', 'middleware' => 'auth', 'underConstr'], function () {
    # Enduser accounts
    \Route::get('account', '\App\Http\Controllers\reports\EnduserController@getAccount');
    \Route::post('account', '\App\Http\Controllers\reports\EnduserController@getAccount');

    # Enduser etickets
    \Route::get('eticket', '\App\Http\Controllers\reports\EnduserController@getEticket');
    \Route::post('eticket', '\App\Http\Controllers\reports\EnduserController@getEticket');

    \Route::get(
        'eticket/fetch/{q}',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\EnduserController@getFetch'
        ]
    )->where('q', '[a-z0-9 ]+');
    \Route::get(
        'account/fetch/{q}',
        [
            'middleware' => 'role:Gramatvedis|Administrators',
            'uses' => '\App\Http\Controllers\reports\EnduserController@getFetch2'
        ]
    )->where('q', '.*');

    # Seller report
    \Route::get(
        'seller',
        [
            'middleware' => 'role:Tirgotajs|Gramatvedis|Administrators',
            'uses' => '\App\Http\Controllers\reports\SellRepController@getSeller'
        ]
    );
    \Route::post(
        'seller',
        [
            'middleware' => 'role:Tirgotajs|Gramatvedis|Administrators',
            'uses' => '\App\Http\Controllers\reports\SellRepController@getSeller'
        ]
    );
    \Route::get(
        'sellerxls/{data}/',
        [
            'middleware' => 'role:Tirgotajs|Gramatvedis|Administrators',
            'uses' => '\App\Http\Controllers\reports\SellRepController@getXls'
        ]
    );

    # Object report
    \Route::get(
        'objekts',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\ObjRepController@getObjekts'
        ]
    );
    \Route::post(
        'objekts',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\ObjRepController@getObjekts'
        ]
    );
    \Route::get(
        'objetickets',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\ObjRepController@getObjEtickets'
        ]
    );
    \Route::post(
        'objetickets',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\ObjRepController@getObjEtickets'
        ]
    );
    \Route::get(
        'objseller',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\ObjRepController@getObjSeller'
        ]
    );
    \Route::post(
        'objseller',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\ObjRepController@getObjSeller'
        ]
    );
    \Route::get(
        'entrance',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\EntranceController@getEntrance'
        ]
    );
    \Route::post(
        'entrance',
        [
            'middleware' => 'role:Objekts|Administrators',
            'uses' => '\App\Http\Controllers\reports\EntranceController@getEntrance'
        ]
    );

    # Salidzinasanas akts report
    \Route::get(
        'akts',
        [
            'middleware' => 'role:Gramatvedis|Administrators',
            'uses' => '\App\Http\Controllers\reports\AktsRepController@index'
        ]
    );
    \Route::post(
        'akts',
        [
            'middleware' => 'role:Gramatvedis|Administrators',
            'uses' => '\App\Http\Controllers\reports\AktsRepController@index'
        ]
    );

    # Brivpusdienas gramatveziem
    \Route::get('grfreemeals', '\App\Http\Controllers\reports\GrFreeRepController@index');
    \Route::post('grfreemeals', '\App\Http\Controllers\reports\GrFreeRepController@index');

    # Seller total report
    \Route::get(
        'selltotals',
        [
            'middleware' => 'role:Tirgotajs|Administrators',
            'uses' => '\App\Http\Controllers\reports\SelltotalsRepController@index'
        ]
    );
    \Route::post(
        'selltotals',
        [
            'middleware' => 'role:Tirgotajs|Administrators',
            'uses' => '\App\Http\Controllers\reports\SelltotalsRepController@index'
        ]
    );

    # Daily money report
    \Route::get('money', '\App\Http\Controllers\reports\MoneyRepController@index');
    \Route::post('money', '\App\Http\Controllers\reports\MoneyRepController@index');
    \Route::get('moneyxls/{no}/{lidz}/', '\App\Http\Controllers\reports\MoneyRepController@xls')
        ->where('no', "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])")
        ->where('lidz', "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])");

    \Route::controller('payments', '\App\Http\Controllers\reports\PaymentsController');
});

// Izmaksa
\Route::group(['prefix' => 'outgoing', 'middleware' => ['auth','role:Tirgotajs']], function () {
    \Route::get('history', '\App\Http\Controllers\OutgoingController@getHistory');
    \Route::get('pdf/{pdf}', '\App\Http\Controllers\OutgoingController@getPdf')
        ->where('pdf', '[0-9]+');
    \Route::get('freepdf/{pdf}', '\App\Http\Controllers\OutgoingController@getFreePdf')
        ->where('pdf', '[0-9]+');
});

# Accountant list
\Route::group(['prefix' => 'accountant', 'middleware' => ['auth','role:Gramatvedis|Administrators']], function () {
    \Route::get('index', '\App\Http\Controllers\accountant\AccountantController@getIndex');
    \Route::get('history', '\App\Http\Controllers\accountant\AccountantController@getHistory');
    \Route::get('confirm/{q}', '\App\Http\Controllers\accountant\AccountantController@getConfirm')
        ->where('q', '[0-9]+');
    \Route::post('confirm/{q}', '\App\Http\Controllers\accountant\AccountantController@postConfirm')
        ->where('q', '[0-9]+');
    \Route::get('pdf/{pdf}', '\App\Http\Controllers\accountant\AccountantController@getPdf')
        ->where('pdf', '[0-9]+');

    \Route::get('mtransfer', '\App\Http\Controllers\accountant\AccountantController@getMtransfer');
    \Route::get('mhistory', '\App\Http\Controllers\accountant\AccountantController@getMhistory');
    \Route::get('mconfirm/{q}', '\App\Http\Controllers\accountant\AccountantController@getMconfirm')
        ->where('q', '[0-9]+');
    \Route::post('mconfirm/{q}', '\App\Http\Controllers\accountant\AccountantController@postMconfirm')
        ->where('q', '[0-9]+');

    \Route::get('mimport', '\App\Http\Controllers\accountant\AccountantImportController@getIndex');
    \Route::post('mimport', 'AccountantImportController@postIndex');

    \Route::get('mimport2', '\App\Http\Controllers\accountant\AccountantImportController@getCsv');
    \Route::post('mimport2', '\App\Http\Controllers\accountant\AccountantImportController@postCsv');
});

# Objekts/skola list
\Route::group(['prefix' => 'skola', 'middleware' => ['auth','objectuser','role:Objekts|Administrators']], function () {
    \Route::get(
        'index/{etickets}/cancel',
        [
            'middleware' => 'etStatus5',
            'uses' => '\App\Http\Controllers\skola\SkolaController@getCancel'
        ]
    )->where('etickets', '[0-9]+');
    \Route::post(
        'index/{etickets}/cancel',
        [
            'middleware' => 'etStatus5',
            'uses' => '\App\Http\Controllers\skola\SkolaController@postCancel'
        ]
    )->where('etickets', '[0-9]+');
    \Route::get(
        'index/{etickets}/confirm',
        [
            'middleware' => 'etStatus5',
            'uses' => '\App\Http\Controllers\skola\SkolaController@getConfirm'
        ]
    )->where('etickets', '[0-9]+');
    \Route::post(
        'index/{etickets}/confirm',
        [
            'middleware' => 'etStatus5',
            'uses' => '\App\Http\Controllers\skola\SkolaController@postConfirm'
        ]
    )->where('etickets', '[0-9]+');
    \Route::get(
        'index/{eticketb}/cancelb',
        [
            'middleware' => 'etbStatus2',
            'uses' => '\App\Http\Controllers\skola\SkolaController@getCancelb'
        ]
    )->where('eticketb', '[0-9]+');
    \Route::post(
        'index/{eticketb}/cancelb',
        [
            'middleware' => 'etbStatus2',
            'uses' => '\App\Http\Controllers\skola\SkolaController@postCancelb'
        ]
    )->where('eticketb', '[0-9]+');
    \Route::get(
        'index/{eticketb}/confirmb',
        [
            'middleware' => 'etbStatus2',
            'uses' => '\App\Http\Controllers\skola\SkolaController@getConfirmb'
        ]
    )->where('eticketb', '[0-9]+');
    \Route::post(
        'index/{eticketb}/confirmb',
        [
            'middleware' => 'etbStatus2',
            'uses' => '\App\Http\Controllers\skola\SkolaController@postConfirmb'
        ]
    )->where('eticketb', '[0-9]+');
    \Route::get('index', '\App\Http\Controllers\skola\SkolaController@getIndex');
    \Route::get('bindex', '\App\Http\Controllers\skola\SkolaController@getIndexb');
});

# DOC upload
\Route::group(['prefix' => 'dokuments', 'middleware' => 'auth'], function () {
    \Route::get('/', '\App\Http\Controllers\DokumentsController@getIndex');
    \Route::post('/', '\App\Http\Controllers\DokumentsController@postIndex');
});

# Portal



/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

// Authentication Routes...
//Route::get('login', 'Auth\PortalAuthController@showLoginForm');
\Route::post('login', 'Auth\PortalAuthController@login');
\Route::get('logout', [
    'uses' => 'Auth\PortalAuthController@getLogout',
    'middleware' => 'auth',
    'as' => 'logout'
]);

// Registration Routes...
//Route::get('register', 'Auth\PortalAuthController@showRegistrationForm');
\Route::post('portal/registration', [
    'uses' =>'Auth\PortalAuthController@register',
    'as' => 'registration'
]);
\Route::get('portal/registration/confirm/{token}', [
    'uses' =>'Auth\PortalAuthController@registerConfirm',
    'as' => 'registration_confirm'
]);

// Password Reset Routes...
\Route::any('password/ajax/email', [
    'uses' => 'Auth\PasswordController@resetAjax',
    'as' => 'ajax_reset'
]);
\Route::post('password/reset', [
    'uses' => 'Auth\PasswordController@reset',
    'as' => 'reset'
]);
\Route::get('password/reset/{token?}', [
    'uses' => 'Auth\PasswordController@showResetForm',
    'as' =>'reset_token'
]);
\Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');

\Route::get(
    '/redirect',
    [
        'uses' => 'RedirectController@redirect',
        'as' => 'user_redirect',
        'middleware' => ['auth']
    ]
);

\Route::group(['prefix' => 'portal', 'middleware' => ['underConstr']], function () {

    // Card integration
    \Route::any('/return/ok', [
        'uses' =>'Portal\DashboardController@returnOk',
        'as' => 'return_ok',
        'middleware' => [
//            'auth'
        ]
    ]);

    \Route::any('/return/fail', [
        'uses' =>'Portal\DashboardController@returnFail',
        'as' => 'return_fail',
        'middleware' => [
//            'auth'
        ]
    ]);

    \Route::any('/test/card', [
        'uses' =>'Portal\DashboardController@testCard',
        'as' => 'return_fail',
        'middleware' => [
//            'auth'
        ]
    ]);

    // End card integration

    \Route::post('/registration', [
        'uses' =>'Auth\PortalAuthController@register',
        'as' => 'registration'
    ]);
    \Route::get('/registration/confirm/{token}', [
        'uses' =>'Auth\PortalAuthController@registerConfirm',
        'as' => 'registration_confirm'
    ]);
    \Route::get('/', [
        'uses' => 'Portal\DashboardController@dashboard',
        'as' => 'dashboard',
        'middleware' => [
            'auth',
            'rr'
        ]
    ]);
    \Route::get('/landing', [
        'uses' => 'Portal\LandingController@landing',
        'as' => 'landing',
    ]);
    \Route::post('/friends', [
        'uses' => 'Portal\LandingController@friends',
        'as' => 'friends',
    ]);
    //edit profile data
    \Route::get('/profile', [
        'uses' => 'Portal\ProfileController@editProfile',
        'as' => 'profile',
        'middleware' => ['auth', 'rr']
    ]);
    \Route::get('/profile/edit', [
        'uses' => 'Portal\ProfileController@editProfile',
        'as' => 'edit_profile',
        'middleware' => ['auth', 'rr']
    ]);
    \Route::post('/profile', [
        'uses' => 'Portal\ProfileController@postEditProfile',
        'as' => 'profile_edit',
        'middleware' => ['auth']
    ]);
    //first profile data
    \Route::post('/steps', [
        'uses' =>'Portal\ProfileController@register',
        'as' => 'registration_profile',
        'middleware' => ['auth']
    ]);
    //save  partial data for first profile registration
    \Route::post('/profile/save', [
        'uses' =>'Portal\ProfileController@registerSave',
        'as' => 'profile_save',
        'middleware' => 'auth:api'
    ]);
    \Route::post('/profile/delete/check', [
        'uses' => 'Portal\ProfileController@deleteCheck',
        'as' => 'profile_delete_check',
        'middleware' => 'auth:api'
    ]);
    \Route::post('/profile/delete', [
        'uses' => 'Portal\ProfileController@delete',
        'as' => 'profile_delete',
        'middleware' => 'auth:api'
    ]);
    \Route::post('/profile/change/email', [
        'uses' => 'Portal\ProfileController@changeEmail',
        'as' => 'profile_change_email',
        'middleware' => 'auth:api'
    ]);
    \Route::get('/terms', [
        'uses' => 'Portal\CommonController@terms',
        'as' => 'terms'
    ]);
    \Route::post('/feedback', [
        'uses' => 'Portal\CommonController@feedback',
        'as' => 'feedback',
        'middleware' => 'auth:api'
    ]);
    \Route::get('/register', [
        'uses' => 'Portal\RegisterController@register',
        'as' => 'register',
        'middleware' => 'guest'
    ]);
    \Route::get('/e-card', [
        'uses' => 'Portal\ProfileController@eCard',
        'as' => 'eCard',
        'middleware' => ['auth', 'rr']
    ]);
    \Route::post('/create/card', [
        'uses' => 'Portal\ProfileController@postCard',
        'as' => 'create_card',
        'middleware' => ['auth']
    ]);
    \Route::post('/edit/card', [
        'uses' => 'Portal\DashboardController@ajaxEditCard',
        'as' => 'edit_card',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/save/card', [
        'uses' => 'Portal\ProfileController@saveCard',
        'as' => 'save_card',
        'middleware' => ['auth']
    ]);
    \Route::post('/refill/card', [
        'uses' => 'Portal\ProfileController@fillCard',
        'as' => 'fill_card',
        'middleware' => ['auth']
    ]);
    \Route::get('/filled/card', [
        'uses' => 'Portal\ProfileController@filledCard',
        'as' => 'filled_card',
        'middleware' => ['auth']
    ]);
    \Route::post('/refill/card/ajax', [
        'uses' => 'Portal\DashboardController@ajaxFillCard',
        'as' => 'ajax_fill_card',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/save/refill/card', [
        'uses' => 'Portal\ProfileController@saveFillCard',
        'as' => 'save_fill_card',
        'middleware' => ['auth']
    ]);
    \Route::post('/ajax/history', [
        'uses' => 'Portal\DashboardController@ajaxHistory',
        'as' => 'ajax_history',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/week/expenses', [
        'uses' => 'Portal\DashboardController@ajaxWeekExpenses',
        'as' => 'week_expenses',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/ajax/notifications', [
        'uses' => 'Portal\DashboardController@ajaxNotifications',
        'as' => 'ajax_notifications',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/ajax/payments', [
        'uses' => 'Portal\DashboardController@ajaxPayments',
        'as' => 'ajax_payments',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/ajax/add/card', [
        'uses' => 'Portal\DashboardController@ajaxAddCard',
        'as' => 'ajax_add_card',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/block/card', [
        'uses' => 'Portal\DashboardController@ajaxBlockCard',
        'as' => 'ajax_block_card',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/unblock/card', [
        'uses' => 'Portal\DashboardController@ajaxUnblockCard',
        'as' => 'ajax_unblock_card',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/ajax/delete/card', [
        'uses' => 'Portal\DashboardController@ajaxDeleteCard',
        'as' => 'ajax_delete_card',
        'middleware' => ['auth:api']
    ]);
    \Route::post('/ajax/delete/payment', [
        'uses' => 'Portal\DashboardController@ajaxDeletePayment',
        'as' => 'ajax_delete_payment',
        'middleware' => ['auth:api']
    ]);
    \Route::get('/interrupt/refill/card', [
        'uses' => 'Portal\ProfileController@interrupt',
        'as' => 'interrupt_fill_card',
        'middleware' => ['auth']
    ]);
    \Route::get('/payment/pdf/{pdf}', [
        'uses' => 'Portal\ProfileController@getPdf',
        'as' => 'download_payment',
        'middleware' => ['auth']
    ]);
    \Route::get('/require', [
        'uses' => 'Portal\RegisterController@registerRequire',
        'as' => 'require',
        'middleware' => ['auth', 'rr']
    ]);
    \Route::get('/steps', [
        'uses' => 'Portal\ProfileController@steps',
        'as' => 'registerSteps',
        'middleware' => ['auth', 'rr']
    ]);
    \Route::get('/success', [
        'uses' => 'Portal\RegisterController@success',
        'as' => 'success'
    ]);
    \Route::get('/thanks', [
        'uses' => 'Portal\RegisterController@thanks',
        'as' => 'thanks',
        'middleware' => ['auth', 'rr']
    ]);
    \Route::post('/avatar', [
        'uses' => 'Portal\RegisterController@createAvatar',
        'as' => 'create_avatar',
        'middleware' => ['auth:api']
    ]);
});

\Route::get('user/yearly', 'UserController@getYearly');
\Route::get('user/yearly/no', '\App\Http\Controllers\UserController@getYearlyNo');

\Route::get('auth/login', '\App\Http\Controllers\Auth\AuthController@getLogin');
\Route::post('auth/login', 'Auth\AuthController@postLogin');
\Route::get(
    'auth/logout',
    [
        'middleware' => 'auth',
        'uses' => 'Auth\AuthController@getLogout',
        'as' => 'logout_old'
    ]
);
\Route::post('auth/user', ['middleware' => 'auth', 'uses' => '\App\Http\Controllers\Auth\AuthController@postUser']);
\Route::get('auth/user', ['middleware' => 'auth', 'uses' => '\App\Http\Controllers\Auth\AuthController@getUser']);

\Route::controller('user', '\App\Http\Controllers\UserController');

#static pages
\Route::get('terms', '\App\Http\Controllers\StaticController@getTerms');
\Route::get('cinfo', '\App\Http\Controllers\StaticController@getCinfo');
\Route::post('contact', '\App\Http\Controllers\StaticController@postContact');
\Route::get('contact', '\App\Http\Controllers\StaticController@getContact');
\Route::get('aprikojuma_instrukcija', '\App\Http\Controllers\StaticController@aprikojuma_instrukcija');
\Route::get('aprikojuma_instrukcijaru', '\App\Http\Controllers\StaticController@aprikojuma_instrukcijaru');
\Route::get('kludu_pazinojumi', '\App\Http\Controllers\StaticController@kludu_pazinojumi');
\Route::get('isa_instrukcija', '\App\Http\Controllers\StaticController@isa_instrukcija');
\Route::get('under-construction', ['uses' => 'UnderConstructionController@getIndex', 'as'=> 'blocked_ip']);

#old
\Route::get('/old', ['middleware' => 'auth', 'uses' => '\App\Http\Controllers\Auth\AuthController@getUser']);
# deafault, tmp
\Route::get('', ['middleware' => 'auth', 'uses' => '\App\Http\Controllers\Auth\AuthController@getUser']);
\Route::get('', ['middleware' => 'auth', 'uses' => '\App\Http\Controllers\Auth\AuthController@getUser']);
\Route::get('/', ['middleware' => 'underConstr', 'uses' => 'Portal\LandingController@landing', 'as' => 'home']);
