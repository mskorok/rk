<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
        \App\Console\Commands\dailyMonitoringChanges::class,
        \App\Console\Commands\deleteUnconfirmedUsersDaily::class,
        \App\Console\Commands\generateExtraSellerPayments::class,
        \App\Console\Commands\generateSellerDailyBalance::class,
        \App\Console\Commands\generateSellerMonthlyFreeMealReport::class,
        \App\Console\Commands\generateSellerPayments::class,
        \App\Console\Commands\reGenerateSellerPayments::class,
        \App\Console\Commands\corectionsSellerPayments::class,
        \App\Console\Commands\getFreeMealPersonData::class,
        \App\Console\Commands\getSchoolData::class,
        \App\Console\Commands\report6Daily::class,
        \App\Console\Commands\report7Daily::class,
        \App\Console\Commands\report8MOnthly::class,
        \App\Console\Commands\sendFreeMealDaily::class,
        \App\Console\Commands\updateDotationsDaily::class,
        \App\Console\Commands\updateTransactionsDaily::class,
        //\App\Console\Commands\importCsv::class,
        //\App\Console\Commands\importCsv2::class,
        \App\Console\Commands\disableCsv::class,
        \App\Console\Commands\updateAllEtickets::class,
        \App\Console\Commands\regen::class,
        \App\Console\Commands\reportKopgaldsDaily::class,
        \Rap2hpoutre\CreateUser\Command::class,
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
        $schedule->command('deleteunconfirmed')->dailyAt('00:05')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));
        $schedule->command('genbalances')->dailyAt('00:10')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));

        if (app()->environment() == 'production')
        {
            $schedule->command('genfreemonth')->monthlyOn(1, '00:25')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));
            $schedule->command('genpayments')->cron('40 0 1,11,21 * *')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));

            $schedule->command('updatetrans')->dailyAt('00:35')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));
            $schedule->command('sendfreedaily')->dailyAt('01:00')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));

            $schedule->command('reportkopgalds')->dailyAt('01:30')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));
            $schedule->command('report6')->dailyAt('01:45')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));
            $schedule->command('report7')->dailyAt('02:05')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));
            $schedule->command('report8')->monthlyOn(1, '02:15')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));

            $schedule->command('updatealletickets')->dailyAt('02:30')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));

            $schedule->command('getschooldata')->dailyAt('06:00')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));

            $schedule->command('frommonitoring')->dailyAt('06:15')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));
            $schedule->command('getfreemealpersons')->dailyAt('06:30')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));
        }

        $schedule->command('updatedot')->dailyAt('07:30')->withoutOverlapping()->appendOutputTo(storage_path('cron.log'));

	}

}
