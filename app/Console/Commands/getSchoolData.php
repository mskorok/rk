<?php

namespace App\Console\Commands;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

use App\Models\Mpos;
use App\Models\ObjectDotationPrices;
use App\Models\Objekts;
use App\Models\School;
use App\Models\SchoolPrices;
use App\Models\Seller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class getSchoolData extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'getschooldata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atjaunot skolu informāciju no API';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // total start timer
        $start = time();

        list($status, $result) = $this->make_curl_request();

        if ($status !== 0)
            return;

        // process data
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $result);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string = str_replace('<s:', '<', $xml_string);
        $xml_string = str_replace('<a:', '<', $xml_string);
        $xml_string = str_replace('<b:', '<', $xml_string);

        $res = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
        $result2 = $res["Body"]["GetInstitutionListResponse"]["GetInstitutionListResult"];

        $email_text = '';

        foreach ($result2 as $school_array2) {
            foreach ($school_array2 as $school_array) {
                if ($school_array["Deleted"] == 'false' && $school_array["City"] == 'RĪGA') {
                    $reg_nr = is_array($school_array["RegistrationNo"]) ? "" : $school_array["RegistrationNo"];

                    $exists = School::where("id", $school_array["Id"])->first();
                    if (isset($exists->id)) {
                        // update
                        if ($exists->changed != substr($school_array["IzmChangeDate"], 0, 10)) {
                            $s = $exists;
                            $s->heading = $school_array["Name"];
                            $s->reg_nr = $reg_nr;
                            $s->changed = $school_array["IzmChangeDate"];
                            $s->equal_price = 0;
                            if (!$s->save()) {
                                $this->error("Kļūda atjaunojot skolas " . $school_array["Name"] . " datus - " . json_encode($s->getErrors()));
                                Log::error("getschooldata - DB update (" . json_encode($school_array) . ") error - " . json_encode($s->getErrors()));
                            }
                        }
                        $s = $exists;
                    } else {
                        $s = New School();
                        $s->id = $school_array["Id"];
                        $s->heading = $school_array["Name"];
                        $s->reg_nr = $reg_nr;
                        $s->changed = $school_array["IzmChangeDate"];
                        if (!$s->save()) {
                            $this->error("Kļūda saglabājot skolas " . $school_array["Name"] . " datus - " . json_encode($s->getErrors()));
                            Log::error("getschooldata - DB save (" . json_encode($school_array) . ") error - " . json_encode($s->getErrors()));
                        }
                        $s = School::findOrFail($school_array["Id"]);
                    }

                    // lunch prices
                    $school_vat = 0;
                    $object_exists = null;
                    if (strlen($reg_nr) > 5) {
                        $object_exists = Objekts::where("object_regnr", $reg_nr)->first();
                        $all_school_objects = Objekts::where("object_regnr", $reg_nr)->get();
                    }

                    if (isset($s->id) && isset($object_exists->id)) {
                        if (isset($school_array["LunchPrices"]["LunchPrice"]) && is_array($school_array["LunchPrices"]["LunchPrice"])) {
                            $school_equal = 1;
                            $school_equal_price = 0;
                            $sk = 0;
                            foreach ($school_array["LunchPrices"]["LunchPrice"] as $lp) {
                                $lp_vat = ($lp["Vat"] == 'true') ? 1 : 0;
                                $lp_price_with_pvn = ($lp_vat == 0) ? substr($lp["Price"], 0, 4) : round(substr($lp["Price"], 0, 4) * 1.21, 2);

                                if (isset($lp["ClassGroup"])) {
                                    if ($sk == 0) {
                                        $school_equal_price = $lp_price_with_pvn;
                                    } else {
                                        if ($school_equal_price != $lp_price_with_pvn) {
                                            $school_equal = 0;
                                        }
                                    }
                                    $sk++;

                                    if ($s->id <= 0) {
                                        $this->error("Kļūda saglabājot skolas cenas, nekorekts school_id: " . $s->id . " skolas dati - " . json_encode($school_array));
                                        Log::error("getschooldata - DB prices save error, bad school_id ('.$s->id.') - " . json_encode($school_array));
                                    }

                                    // vat for object
                                    $school_vat = $lp_vat;

                                    // seller reg nr. for school
                                    $regnr_array = [];

                                    // school can have multiple objects with same registration number!!!
                                    foreach ($all_school_objects as $object_exists) {
                                        // check if exists
                                        $current_price = SchoolPrices::where("school_id", $s->id)
                                                ->where("grade", $lp["ClassGroup"])
                                                ->where("object_id", $object_exists->id)
                                                ->first();

                                        if (isset($current_price->id)) {
                                            $current_price->price = $lp_price_with_pvn;
                                            $current_price->vat = $lp_vat;
                                            $current_price->regnr = $lp["RegNo"];
                                            if (!$current_price->save()) {
                                                $this->error("Kļūda atjaunojot skolas cenas " . $school_array["Name"] . " datus - " . json_encode($current_price->getErrors()));
                                                Log::error("getschooldata - DB prices update (" . json_encode($lp) . ") error - " . json_encode($current_price->getErrors()));
                                            }
                                        } else {
                                            $sp = New SchoolPrices();
                                            $sp->school_id = $s->id;
                                            $sp->object_id = $object_exists->id;
                                            $sp->grade = $lp["ClassGroup"];
                                            $sp->price = $lp_price_with_pvn;
                                            $sp->vat = $lp_vat;
                                            $sp->regnr = $lp["RegNo"];
                                            if (!$sp->save()) {
                                                $this->error("Kļūda saglabājot skolas cenas " . $school_array["Name"] . " datus - " . json_encode($sp->getErrors()));
                                                Log::error("getschooldata - DB prices save (" . json_encode($lp) . ") error - " . json_encode($sp->getErrors()));
                                            }
                                        }

                                        if (!in_array($lp["RegNo"], $regnr_array))
                                            $regnr_array[] = $lp["RegNo"];
                                    }
                                }
                            }

                            if ($school_equal == 1) {
                                // update school equal price
                                $s->equal_price = $school_equal_price;
                                if (!$s->save()) {
                                    $this->error("Kļūda saglabājot skolas equal price " . $school_array["Name"] . " datus - " . json_encode($s->getErrors()));
                                    Log::error("getschooldata - DB update equal price (" . json_encode($school_array) . ") error - " . json_encode($s->getErrors()));
                                }

                                // update ODP data
                                foreach ($all_school_objects as $object_exists) {
                                    $data_row = reset($school_array["LunchPrices"]["LunchPrice"]);
                                    $price_in_cents = substr($data_row["Price"], 0, 4) * 100;

                                    $lp_vat = ($data_row["Vat"] == 'true') ? 1 : 0;
                                    $lp_price_with_pvn = ($lp_vat == 0) ? $price_in_cents : round($price_in_cents * 1.21);

                                    $this->save_odp($object_exists, $lp_price_with_pvn, 'Komplekts 1.-12. kl.', 1);
                                }
                            } else {
                                $unique_data_rows = [];
                                foreach ($school_array["LunchPrices"]["LunchPrice"] as $data_row) {
                                    $lp_vat = ($data_row["Vat"] == 'true') ? 1 : 0;
                                    $lp_price_with_pvn = ($lp_vat == 0) ? substr($data_row["Price"], 0, 4) * 100 : round(substr($data_row["Price"], 0, 4) * 1.21, 2) * 100;

                                    if (!isset($unique_data_rows[$lp_price_with_pvn])) {
                                        $unique_data_rows[$lp_price_with_pvn] = [$data_row["ClassGroup"]];
                                    } else {
                                        $unique_data_rows[$lp_price_with_pvn][] = $data_row["ClassGroup"];
                                    }
                                }

                                $odp_ids = [];
                                foreach ($all_school_objects as $object_exists) {
                                    foreach ($unique_data_rows as $price => $data_row) {
                                        $odp_ids[] = $this->save_odp($object_exists, $price, 'Komplekts ' . implode(",", $data_row) . ' kl.', 0);
                                    }
                                }

                                // ODP cleanup
                                foreach ($all_school_objects as $object_exists) {
                                    $this->odp_cleanup($odp_ids, $object_exists->id);
                                }
                            }
                        }
                    }

                    // object data
                    foreach ($all_school_objects as $object_exists) {
                        if (isset($object_exists->id)) {
                            // TMP no overwrite
                            //$object_exists->object_name = $school_array["Name"];
                            $object_exists->object_city = $school_array["City"];
                            $object_exists->object_address = $school_array["Address"];
                            $object_exists->vat = $school_vat;
                            if (!$object_exists->save()) {
                                $this->error("Kļūda saglabājot objekta " . $object_exists->id . " datus - " . json_encode($object_exists->getErrors()));
                                Log::error("getschooldata - object DB save (" . json_encode($school_array) . ") error - " . json_encode($object_exists->getErrors()));
                            }
                        }
                    }

                    // current registered seller reg.nrs in this school
                    if (isset($regnr_array)) {
                        $email_text .= $this->check_seller_reg_nr_diff($reg_nr, $regnr_array, $school_array);
                    } else
                        $this->error("Nav saņemti tirgotāju reg. nr. dati skolai - " . $reg_nr . " " . $school_array["Name"]);
                }
            }
        }

        // send diff email
        if (strlen($email_text) > 2) {
            Mail::send('mail.report', ["info" => $email_text], function ($message) {
                $message->to(Config::get('mail.adminemail'))->subject('Skolas.rigaskarte.lv - nesakrīt saņemtie tirgotāju reg. nr.');
            });
        }


        // total time
        $end = time();
        $this->log_api_data("zz", date("Y-m-d H:i:s") . " - getschooldata total time in seconds=" . ($end - $start));

        $this->info('Skolu informācijas atjaunošana sekmīgi pabeigta');
    }

    /**
     * @param $object_exists
     * @param $price_in_cents
     * @param string $heading
     * @param int $single_price_per_object
     * @return mixed
     */
    private function save_odp($object_exists, $price_in_cents, $heading = '', $single_price_per_object = 0)
    {
        $exists_odp = ObjectDotationPrices::where("object_id", $object_exists->id)
            ->where("seller_account_id", 2)
            ->where("price", $price_in_cents)
            ->first();

        if (!$exists_odp) {
            $master_free_meals_account_id = 2;

            $odp = New ObjectDotationPrices();
            $odp->heading = $heading;
            $odp->object_id = $object_exists->id;
            $odp->price = $price_in_cents;
            $odp->seller_account_id = $master_free_meals_account_id;
            if (!$odp->save()) {
                $this->error("Kļūda saglabājot ODP datus - " . json_encode($odp->getErrors()));
                Log::error("getschooldata - ODP DB save error - " . json_encode($odp->getErrors()));
            }

            // delete others if now only one price
            if ($single_price_per_object == 1) {
                ObjectDotationPrices::where("object_id", $object_exists->id)
                    ->where("seller_account_id", $master_free_meals_account_id)
                    ->where("id", "!=", $odp->id)
                    ->delete();
            }
        } else
            $odp = $exists_odp;

        return $odp->id;
    }

    /**
     * @param array $odp_ids
     * @param $object_id
     */
    private function odp_cleanup($odp_ids = [], $object_id)
    {
        ObjectDotationPrices::where("object_id", $object_id)
            ->where("seller_account_id", 2)
            ->whereNotIn("id", $odp_ids)
            ->delete();

        return;
    }

    /**
     * @param $reg_nr
     * @param $regnr_array
     * @param $school_array
     */
    private function check_seller_reg_nr_diff($reg_nr, $regnr_array, $school_array)
    {
        $text = '';

        $school_objects = Objekts::where("object_regnr", $reg_nr)->lists("id")->all();
        if (count($school_objects) > 0 && count($regnr_array) > 0) {
            $all_mpos_owner_ids = Mpos::whereIn("object_id", $school_objects)->lists("owner_id")->all();
            $all_seller_regnrs = array_unique(Seller::whereIn("owner_id", $all_mpos_owner_ids)->lists("reg_nr")->all());

            if (count(array_diff($all_seller_regnrs, $regnr_array)) > 0) { // add to email text
                $text = "Nesakrīt tirgotāju reg. nr.  " . $school_array["Name"] . "<br /> Esošie: " . implode(", ", $all_seller_regnrs) . " <br /> Saņemtie: " . implode(", ", $regnr_array) . "<br /><br />";
            }
        }

        return $text;
    }

    /**
     * @return array
     */
    protected function make_curl_request()
    {
        $xml = 
        '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://tempuri.org/IMonitoringService/GetInstitutionList</wsa:Action>
            </soap:Header>
            <soap:Body>
               <tem:GetInstitutionList/>
            </soap:Body>
        </soap:Envelope>';

        $headers = array(
            "POST HTTP/1.1",
            "Accept-Encoding: gzip,deflate",
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/GetInstitutionList"',
            "Content-length: " . strlen($xml),
            "Host: " . Config::get('services.rs.zzurl2'),
            "Connection: Keep-Alive",
            "User-Agent: skolas.rigaskarte.lv",
        );

        $url = Config::get('services.rs.zzurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, Config::get('services.curl.timeout'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLCERT, storage_path() . "/zz_cert.pem");
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, Config::get('services.rs.certkey'));
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $this->error("Kļūda saņemot datus - " . curl_error($ch));
            Log::error("getschooldata - CURL error - " . curl_error($ch));
            return array(1, "");
        }
        $info = curl_getinfo($ch);
        curl_close($ch);


        // LOG data to file
        $this->log_api_data("zz", "getschooldata", $result, $info["url"], $info["total_time"]);

        return array(0, $result);
    }

}
