<?php

namespace App\Console\Commands;

ini_set("memory_limit", -1);

use DateTime;
use App\Models\Eticket;
use App\Models\Mpos;
use App\Models\Objekts;
use App\Models\Seller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class reportKopgaldsDaily extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'reportkopgalds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Neatrastie kopgalda darījumi par vakardienu';
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reportkopgalds {exec_date?}';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = time();

        $execDate = $this->argument('exec_date');
        $execDate = !empty($execDate) ? $execDate : Input::get('exec_date');
        $datetime = new DateTime('yesterday');
        $vakardiena = !empty($execDate) ? $execDate : $datetime->format('Y-m-d');

        $table = [];
        $all_transactions = Transaction::with("frome")
            ->where("from_account_id", '=', 2)
            ->where("dotation_id", '=', -1)
            ->where("freemeals_type", '=', 4)
            ->where("updated_at", 'LIKE', $vakardiena . '%')
            ->get();

        try {
            foreach ($all_transactions as $transaction) {

                $reason = null;
                if (!isset($transaction->frome->pk)) {
                    $reason = 'E-talons neeksistē';
                } elseif (!isset($transaction->frome->kopgalds_from)) {
                    $reason = 'Personai ir cita veida finansējums';
                } else {
                    $from = $transaction->frome->kopgalds_from;
                    $until = $transaction->frome->kopgalds_until;
                    if ($from == '0000-00-00' || $until == '0000-00-00') {
                        $reason = 'Personai ir cita veida finansējums';
                    } else if ($from > $vakardiena || $until < $vakardiena) {
                        $reason = 'Personai nav finansējuma';
                    }
                }
                if(!empty($reason)){
                    $object = Mpos::select('mpos_name')->where(['id' => $transaction->mpos_id])->first();
                    $table[] = [$vakardiena, substr($transaction->updated_at, 11), $object->mpos_name, $transaction->from_eticket_nr, $reason];
                }
                
            }

            if (count($table) > 0) {
                Excel::create('Neatrastie kopgalda darījumi par vakardienu ' . $vakardiena, function($excel) use ($vakardiena, $table) {
                    $excel->setTitle('Skolas.rigaskarte.lv - Neatrastie kopgalda darījumi par vakardienu ' . $vakardiena);
                    $excel->setCreator('skolas.rigaskarte.lv')->setCompany('skolas.rigaskarte.lv');
                    $excel->setDescription('Skolas.rigaskarte.lv - Neatrastie kopgalda darījumi par vakardienu ' . $vakardiena);

                    $excel->sheet('Atskaite ' . $vakardiena, function($sheet) use($table) {
                        $sheet->setOrientation('landscape');
                        $sheet->getPageSetup()->setFitToPage(false);
                        $sheet->getDefaultRowDimension()->setRowHeight(-1);
                        $sheet->setWidth(array(
                            'A' => 15,
                            'B' => 15,
                            'C' => 30,
                            'D' => 15,
                            'E' => 50,
                        ));
                        $sheet->setHeight(0, 40);

                        $sheet->fromArray($table, null, 'A1', false, false);

                        $sheet->prependRow(['Datums', 'Laiks', 'Objekts', 'Kartes Nr.', 'Iemesls']);

                        $sheet->freezeFirstRow();

                        $sheet->cells('A1:E1', function($cells) {
                            $cells->setFontWeight('bold')->setBackground('#CCCCCC');
                        });
                        $sheet->getStyle('A1:E1')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('A2:B3999')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('D2:E3999')->getAlignment()->setWrapText(true);

                        $sheet->setColumnFormat(array(
                            'F2:G3999' => '0.00'
                        ));
                    });
                })->store('xlsx');

                //send email to RK
                Mail::send('mail.report', ["info" => "Neatrastie kopgalda darījumi par vakardienu " . $vakardiena], function($message) use ($vakardiena) {
                    $message->to(Config::get('mail.rsemail2'))->subject('Skolas.rigaskarte.lv - Neatrastie kopgalda darījumi par vakardienu ' . $vakardiena);
                    $message->attach(storage_path("exports/Neatrastie kopgalda darījumi par vakardienu " . $vakardiena . ".xlsx"));
                });

                $end = time();
                $this->info('Atskaite veiksmīgi ģenerēta un nosūtīta - ('.$vakardiena.' | ' . ($end - $start) . 's).');
            } else {
                $end = time();
                $this->info('Nav šādu datu par vakardienu '.$vakardiena.' - (' . ($end - $start) . 's).');
            }
        } catch (\Exception $e) {
            $this->error('Kļūda apstrādājot datus - ' . $e->getMessage());
        }
    }

}
