<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Seller;
use App\Models\SellerBalances;
use App\Models\SellerPayments;
use App\Models\Settings;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class generateSellerPayments extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'genpayments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ģenerēt tirgotāju izmaksas 3x mēnesī';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'genpayments {exec_date?}';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $execDate = Input::get('exec_date');
        $time = !empty($execDate) ? $execDate : 'now';
        $sodiena = new Carbon($time);
        if ($sodiena->day < 11) {
            $sodiena->subMonth();
            $periods = substr($sodiena->toDateString(), 0, 7) . '-3';
            $periods_no = substr($sodiena->toDateString(), 0, 7) . '-21';
            $sodiena2 = $sodiena;
            $sodiena2->addMonth();
            $sodiena2->day = 0;
            $periods_lidz = $sodiena2->toDateString();
        } else if ($sodiena->day < 21) {
            $periods = substr($sodiena->toDateString(), 0, 7) . '-1';
            $periods_no = substr($sodiena->toDateString(), 0, 7) . '-01';
            $periods_lidz = substr($sodiena->toDateString(), 0, 7) . '-10';
        } else {
            $periods = substr($sodiena->toDateString(), 0, 7) . '-2';
            $periods_no = substr($sodiena->toDateString(), 0, 7) . '-11';
            $periods_lidz = substr($sodiena->toDateString(), 0, 7) . '-20';
        }

        $sellers = Seller::with("owner")->get();

        // check if has positive currency balance
        foreach ($sellers as $key => $seller) {
            $data = SellerBalances::where("seller_id", $seller->id)
                ->where("datums", "=", $periods_lidz)
                ->first();

            if (empty($data->balance_paid)){
                unset($sellers[$key]);
            }
        }

        foreach ($sellers as $key => $seller) {
            $seller_owner_account = Account::Sell()->O($seller->owner->id)->first();

            // check if not already transferred
            $exists = Transaction::where("to_account_id", 999999)
                ->where("from_account_id", $seller_owner_account->id)
                ->where("comments", "LIKE", '%' . $periods)
                ->count();

            if ($exists > 0)
                continue;

            // seller balance for third of the month
            // sum of seller daily balances
            $owner_id = $seller->owner_id;
            if ($owner_id > 0) {
                // get daily amounts
                $data = SellerBalances::where("seller_id", $seller->id)
                    ->where("datums", ">=", $periods_no)
                    ->where("datums", "<=", $periods_lidz)
                    ->get();

                $total = $data->sum("daily_paid");
                $mpos_amounts = [];

                // get all days with payments
                foreach ($data as $row) {
                    if ($row["daily_paid"] > 0) {
                        $masivs = json_decode($row["mpos_account_amounts"]);
                        foreach ($masivs as $mpos_row) {
                            if ($mpos_row->amount_paid > 0)
                                $mpos_amounts[] = $mpos_row;
                        }
                    }
                }

                // group all payments by mposes
                $grouped_mpos_amounts = [];
                foreach ($mpos_amounts as $daily) {
                    if (!isset($grouped_mpos_amounts[$daily->mpos_id])) {
                        $grouped_mpos_amounts[$daily->mpos_id] = [
                            "account_id" => $daily->account_id,
                            "amount_paid" => $daily->amount_paid,
                        ];
                    } else
                        $grouped_mpos_amounts[$daily->mpos_id]["amount_paid"] += $daily->amount_paid;
                }

                if ($total > 0) {
                    try {

                        // get seller account and transfer money from MPOSes
                        $seller_acc = Account::O($owner_id)->Sell()->first();
                        if ($seller_acc->id > 0) {
                            DB::transaction(function () use ($seller_acc, $grouped_mpos_amounts, $periods, $periods_no, $periods_lidz, $seller, $total) {

                                foreach ($grouped_mpos_amounts as $mpos_id => $m) {
                                    if ($m["account_id"] > 0) {
                                        $mpos_account = Account::findOrFail($m["account_id"]);
                                        if ($mpos_account->balance < $m["amount_paid"])
                                            throw new \Exception('MPOS account ' . $m["account_id"] . ' missing funds! Required: ' . $m["amount_paid"] / 100, 1);

                                        $t = New Transaction;

                                        $t->amount = $m["amount_paid"];
                                        $t->operation_id = 1;
                                        $t->mpos_id = $mpos_id;
                                        $t->from_account_id = $m["account_id"];
                                        $t->to_account_id = $seller_acc->id;
                                        $t->confirmed = 1;
                                        $t->save();

                                        $seller_acc->balance = $seller_acc->balance + $m["amount_paid"];
                                        $seller_acc->save();

                                        $mpos_account->balance = $mpos_account->balance - $m["amount_paid"];
                                        $mpos_account->save();
                                    }
                                }

                                // from seller account to bank transfer
                                $outputName = str_random(20) . '-' . $periods;

                                // seller transaction
                                $t = New Transaction;
                                $t->amount = $total;
                                $t->operation_id = 1;
                                $t->from_account_id = $seller_acc->id;
                                $t->to_account_id = 999999;
                                $t->confirmed = 0;
                                $t->comments = $outputName;
                                $t->save();

                                $seller_acc->balance = $seller_acc->balance - $total;
                                $seller_acc->save();

                                // seller payment record
                                $sp = New SellerPayments;
                                $sp->amount = $total;
                                $sp->seller_id = $seller->id;
                                $sp->transaction_id = $t->id;
                                $sp->status = 1;
                                $sp->period_code = $periods;
                                $sp->save();

                                $payment_id = $sp->id;

                                // generate PDF
                                $data = Transaction::with('from_account')->where('id', $t->id)->Ok()->firstOrFail();

                                $settings = Settings::firstOrFail();

                                $datums_no = $periods_no;
                                $datums_lidz = $periods_lidz;

                                $html = 
                                '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
                                    <head profile="http://gmpg.org/xfn/11">
                                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                                        <style tyle="text/css">body { font-family: DejaVu Sans, sans-serif; }</style>
                                    </head>'.
                                    '<body>' .
                                        View::make('accountant/receipt', compact('data', 'seller', 'settings', 'datums_no', 'datums_lidz', 'payment_id')).
                                    '</body>'.
                                '</html>';

                                if (!is_dir(public_path('assets/pdfs'))) {
                                    mkdir(public_path('assets/pdfs'), 0755, true);
                                }

                                $pdfPath = public_path('assets/pdfs') . '/' . $outputName . '.pdf';

                                $content = \PDF::loadHTML($html)->output();

                                File::put($pdfPath, $content);
                            });
                        } else
                            throw new \Exception('No seller account!', 1);
                    } catch (\Exception $e) {
                        $this->error('Neizdevās sagatavot pārskaitījumu!' . $e->getMessage());
                    }
                } else
                    $this->info('Tirgotājam ar ID ' . $seller->id . ' nav ko izmaksāt!');
            } else
                Throw New \Exception('no owner');
        }

        $this->info('Tirgotāju izmaksu ģenerēšana sekmīgi pabeigta');
    }

}
