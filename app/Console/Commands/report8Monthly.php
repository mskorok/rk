<?php

namespace App\Console\Commands;

use App\Models\Mpos;
use App\Models\Seller;
use App\Models\Transaction;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class report8MOnthly extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'report8';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mēneša atskaite nr.8 - visi tirgotāji pa skolām';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $men["01"] = 'janvāris';
        $men["02"] = 'februāris';
        $men["03"] = 'marts';
        $men["04"] = 'aprīlis';
        $men["05"] = 'maijs';
        $men["06"] = 'jūnijs';
        $men["07"] = 'jūlijs';
        $men["08"] = 'augusts';
        $men["09"] = 'septembris';
        $men["10"] = 'oktobris';
        $men["11"] = 'novembris';
        $men["12"] = 'decembris';

        $datetime = new DateTime('last month');
        $vakardiena = $datetime->format('Y-m');
        $month_number = $datetime->format('m');
        $month_name = strtoupper($men[$month_number]);

        $table = [];
        $all_sellers = Seller::orderBy("seller_name")->get();

        try {
            foreach ($all_sellers as $seller) {
                $mposes = Mpos::where("owner_id", $seller->owner_id)->orderBy("id")->get();
                foreach ($mposes as $mpos) {
                    $daily_balance = Transaction::where("to_account_id", $mpos->account_id)
                        ->Ok()
                        ->where("from_account_id", '!=', 2)
                        ->where("created_at", 'LIKE', $vakardiena . '%')
                        ->sum("amount");

                    $table[] = [$seller->seller_name, $mpos->mpos_name, sprintf("%.2f", $daily_balance / 100), $vakardiena];
                }
            }

            Excel::create('MĒNEŠA ATSKAITE DARĪJUMI TIRGOTĀJI ' . $month_name, function($excel) use ($vakardiena, $table, $month_name) {
                $excel->setTitle('Skolas.rigaskarte.lv - MĒNEŠA ATSKAITE DARĪJUMI TIRGOTĀJI ' . $month_name);
                $excel->setCreator('skolas.rigaskarte.lv')->setCompany('skolas.rigaskarte.lv');
                $excel->setDescription('Skolas.rigaskarte.lv - MĒNEŠA ATSKAITE DARĪJUMI TIRGOTĀJI ' . $month_name);

                $excel->sheet('Atskaite', function($sheet) use($table) {
                    $sheet->setOrientation('portrait');
                    $sheet->getPageSetup()->setFitToPage(false);
                    $sheet->getDefaultRowDimension()->setRowHeight(-1);
                    $sheet->setWidth(array(
                        'A' => 30,
                        'B' => 30,
                        'C' => 15,
                        'D' => 12,
                    ));
                    $sheet->setHeight(0, 40);

                    $sheet->fromArray($table, null, 'A1', false, false);

                    $sheet->prependRow(['Tirgotājs', 'Skola', 'Mēneša summa EUR', 'Mēnesis']);

                    $sheet->freezeFirstRow();

                    $sheet->cells('A1:Z1', function($cells) {
                        $cells->setFontWeight('bold')->setBackground('#CCCCCC');
                    });
                    $sheet->getStyle('A1:Z1')->getAlignment()->setWrapText(true);
                    $sheet->getStyle('A2:B3999')->getAlignment()->setWrapText(true);

                    $sheet->setColumnFormat(array(
                        'C2:C3999' => '0.00'
                    ));
                });
            })->store('xlsx');

            //send email to RK
            Mail::send('mail.report', ["info" => "MĒNEŠA ATSKAITE DARĪJUMI TIRGOTĀJI " . $month_name], function($message) use ($month_name) {
                $message->to(Config::get('mail.rsemail1'))->cc(Config::get('mail.rsemail2'))->subject('Skolas.rigaskarte.lv - MĒNEŠA ATSKAITE DARĪJUMI TIRGOTĀJI ' . $month_name);
                $message->attach(storage_path() . "/exports/MĒNEŠA ATSKAITE DARĪJUMI TIRGOTĀJI " . $month_name . ".xlsx");
            });

            $this->info('Atskaite veiksmīgi ģenerēta un nosūtīta.');
        } catch (\Exception $e) {
            $this->error('Kļūda apstrādājot datus - ' . $e->getMessage());
        }
    }

}
