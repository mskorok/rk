<?php

namespace App\Console\Commands;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

use App\Models\Dotations;
use App\Models\Eticket;
use App\Models\ObjectDotationHistory;
use App\Models\ObjectDotationPrices;
use App\Models\Objekts;
use App\Models\SchoolPrices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class updateDotationsDaily extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    //protected $name = 'updatedotations';
    protected $signature = 'updatedot {obj=0} {force=0}';
    protected $email_text = '';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atjaunot informāciju autorizatoram par personām ar brīvpusdienām';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $obj = intval($this->argument('obj'));
        $force = intval($this->argument('force'));

        if ($obj <= 0) {
            $all_objects = Objekts::pluck("id")->all();
        } else
            $all_objects = Objekts::where("id", $obj)->orderBy("id", "ASC")->pluck("id")->all();

        foreach ($all_objects as $object_id) {
            $this->process_object($object_id, $force);
        }

        if (strlen($this->email_text) > 5) {
            Mail::send('mail.report', ["info" => $this->email_text], function ($message) {
                $message->to(Config::get('mail.adminemail'))->cc(Config::get('mail.adminemail2'))->subject('Skolas.rigaskarte.lv - objekti, kam liela dotāciju skaita starpība!');
            });
        }

        $this->info('Dotāciju informācijas atjaunošana sekmīgi pabeigta');
    }

    /**
     * @param $object_id
     */
    protected function process_object($object_id, $force = 0)
    {
        $used_dotation_ids = [];

        $all_active_etickets = $this->get_all_active_etickets($object_id);

        $object_odps = ObjectDotationPrices::where("object_id", $object_id)->pluck("id")->all();

        // object yesterday dotation count
        $yesterday_count = Dotations::whereIn("dotation_id", $object_odps)->count();

        if (App::environment() != 'testing')
            $this->info("Objektam " . $object_id . " vakar bija dotācijas: " . $yesterday_count);

        $today_count = 0;
        $final_time = strtotime(date("Y-m-d"));

        DB::beginTransaction();
        foreach ($all_active_etickets as $et) {
            if (
                    (($et->free_meals_until == '0000-00-00') || (strtotime($et->free_meals_until) < $final_time))
                    AND ( ($et->second_meals_until == '0000-00-00') || (strtotime($et->second_meals_until) < $final_time))
                    AND ( ($et->third_meals_until == '0000-00-00') || (strtotime($et->third_meals_until) < $final_time))
            ) {
                Dotations::where("pk", $et->pk)
                    ->whereIn("dotation_id", $object_odps)
                    ->delete();
            } else {
                if (strtotime($et->third_meals_from) > $final_time && $et->third_meals_until != '0000-00-00') {
                    Dotations::where("pk", $et->pk)
                        ->whereIn("dotation_id", $object_odps)
                        ->delete();

                    continue;
                }

                $grade = intval($et->grade);
                if($grade <= 4){
                    $kkk=1;
                }

                try {
                    $school_price = intval(round(floatval(SchoolPrices::where("object_id", $object_id)
                                                    ->where("grade", $grade)
                                                    ->first()
                                            ->price) * 100));
                } catch (\Exception $e) {
                    $school_price = 0;
                }

                if ($school_price <= 0) {
                    Log::error("Nevareja atrast cenu, object_id=$object_id , grade=$grade");
                    continue;
                }

                $odp = ObjectDotationPrices::where("object_id", $object_id)
                        ->where("price", $school_price)
                        ->get();

                if ($odp->count() == 0) {
                    Log::error("Nevareja atrast ODP, object_id=$object_id , price=$school_price");
                    continue;
                }

                // free meal dotations
                $joined_date_from = '';
                $joined_date_until = '';

                // check all, pick by priority
                if (strtotime($et["third_meals_from"]) <= time() && strtotime($et["third_meals_until"]) >= $final_time) {
                    $joined_date_from = substr($et["third_meals_from"], 0, 10);
                    $joined_date_until = substr($et["third_meals_until"], 0, 10);
                } else {
                    if (strtotime($et["second_meals_from"]) <= time() && strtotime($et["second_meals_until"]) >= $final_time) {
                        $joined_date_from = substr($et["second_meals_from"], 0, 10);
                        $joined_date_until = substr($et["second_meals_until"], 0, 10);
                    } else {
                        if (strtotime($et["free_meals_from"]) <= time() && strtotime($et["free_meals_until"]) >= $final_time) {
                            $joined_date_from = substr($et["free_meals_from"], 0, 10);
                            $joined_date_until = substr($et["free_meals_until"], 0, 10);
                        }
                    }
                }

                if (trim($joined_date_from) == '' || trim($joined_date_until) == '') {
                    continue;
                }

                // each dotation record for this eticket
                foreach ($odp as $odp_row) {
                    $dotation_exists = Dotations::where("pk", $et->pk)
                        ->where("dotation_id", $odp_row->id)
                        ->first();

                    if (isset($dotation_exists->id)) {
                        $dot = $dotation_exists;
                        if ($dot->until_date != $joined_date_until) {
                            $dot->from_date = $joined_date_from;
                            $dot->until_date = $joined_date_until;
                            $dot->import_date = date("Y-m-d");
                            if (!$dot->save()) {
                                $this->error("Kļūda atjaunojot dotāciju datumu - " . json_encode($dot->getErrors()));
                                Log::error("upddailydot - dot DB update error - " . json_encode($dot->getErrors()));
                            } else {
                                $used_dotation_ids[] = $dotation_exists->id;
                            }
                        } else {
                            $used_dotation_ids[] = $dotation_exists->id;
                        }
                    } else {
                        $dot = New Dotations();
                        $dot->pk = $et->pk;
                        $dot->dotation_id = $odp_row->id;
                        $dot->from_date = $joined_date_from;
                        $dot->until_date = $joined_date_until;
                        $dot->import_date = date("Y-m-d");
                        $dot->last_used = "2015-01-01";
                        if (!$dot->save()) {
                            $this->error("Kļūda izveidojot dotāciju ierakstu - " . json_encode($dot->getErrors()));
                            Log::error("upddailydot - dot DB insert error - " . json_encode($dot->getErrors()));
                        } else {
                            $used_dotation_ids[] = $dot->id;
                        }
                    }
                }
            }
        }

        // remove unused dotations
        if (App::environment() == 'production' || App::environment() == 'testing') {
            Dotations::whereIn("dotation_id", $object_odps)
                ->whereNotIn("id", array_unique($used_dotation_ids))
                ->delete();
        }

        $today_count = Dotations::whereIn("dotation_id", $object_odps)->count();

        // calculate diff and send email
        $st = 100;
        if ($yesterday_count > 0) {
            $st = round(($today_count / $yesterday_count) * 100);

            // force
            if ($force == 1)
                $st = 100;

            if ($st <= 70) {
                // add to email
                $this->email_text .= 
                    "Objektam " . Objekts::findOrFail($object_id)->object_name . " liela dotāciju skaita starpība:<br />".
                    "Vakar: " . $yesterday_count . "<br />".
                    "Šodien: " .$today_count . "<br />".
                    "Attiecība: " . $st . "%<br />";
            }
        }

        if ($st <= 70) {
            DB::rollBack();
        } else {
            DB::commit();
        }

        if ($force == 1) {
            ObjectDotationHistory::updateOrCreate(["object_id" => $object_id, "datums" => date("Y-m-d")], [
                "skaits" => $today_count,
                "regen" => 1
            ]);
        } else {
            ObjectDotationHistory::updateOrCreate(["object_id" => $object_id, "datums" => date("Y-m-d")], [
                "skaits" => $today_count
            ]);
        }

        if (App::environment() != 'testing')
            $this->info("Objektam " . $object_id . " šodien dotācijas: " . $today_count);

        return $today_count;
    }

    /**
     * @param $object_id
     * @return mixed
     */
    protected function get_all_active_etickets($object_id)
    {
        $all_active_etickets = Eticket::A()
                ->whereHas('objekti', function ($query) use ($object_id) {
                    $query->where('object_id', $object_id);
                })
                ->where(function ($query) {
                    $query->where("free_meals_until", "!=", "0000-00-00")
                    ->orWhere("second_meals_until", "!=", "0000-00-00")
                    ->orWhere("third_meals_until", "!=", "0000-00-00");
                })
                ->get();

        return $all_active_etickets;
    }

}
