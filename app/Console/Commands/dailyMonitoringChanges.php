<?php

namespace App\Console\Commands;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

use App\Models\DailyLogRk;
use App\Models\DailyLogZz;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\Objekts;
use App\Models\School;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class dailyMonitoringChanges extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'frommonitoring';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atjaunot informāciju no API monitoringa';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // total start timer
        $start = time();

        $rk_batch = [];
        $used_pks = [];
        $person_data = [];

        $xml = 
        '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://tempuri.org/IMonitoringService/GetMonitoringChanges</wsa:Action>
            </soap:Header>
            <soap:Body>
               <tem:GetMonitoringChanges>
                  <tem:request>
                     <zzd:Header>
                        <zzd1:From>?</zzd1:From>
                     </zzd:Header>
                     <zzd:ChangesSince>' . substr(Carbon::yesterday(), 0, 10) . '</zzd:ChangesSince>
                  </tem:request>
               </tem:GetMonitoringChanges>
            </soap:Body>
        </soap:Envelope>';

        $headers = array(
            "POST HTTP/1.1",
            "Accept-Encoding: gzip,deflate",
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/GetMonitoringChanges"',
            "Content-length: " . strlen($xml),
            "Host: " . Config::get('services.rs.zzurl2'),
            "Connection: Keep-Alive",
            "User-Agent: skolas.rigaskarte.lv",
        );

        $url = Config::get('services.rs.zzurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 59);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLCERT, storage_path() . "/zz_cert.pem");
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, Config::get('services.rs.certkey'));
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $this->error("Kļūda saņemot datus - " . curl_error($ch));
            Log::error("dailymonitoring - CURL error - " . curl_error($ch));
            return;
        }
        $info = curl_getinfo($ch);
        curl_close($ch);

        // LOG data to file
        $this->log_api_data("zz", "dailymonitoringchanges", $result, $info["url"], $info["total_time"]);

        // process data
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $result);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string = str_replace('<s:', '<', $xml_string);
        $xml_string = str_replace('<a:', '<', $xml_string);
        $xml_string = str_replace('<b:', '<', $xml_string);

        $res = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
        $result2 = isset($res["Body"]["GetMonitoringChangesResponse"]["GetMonitoringChangesResult"]["Changes"]) ?
                $res["Body"]["GetMonitoringChangesResponse"]["GetMonitoringChangesResult"]["Changes"] : [];

        if (!isset($result2["PersonData"])) {
            $this->info("Saņemta tukša atbilde!");
            return;
        }

        $rrr = (isset($result2["PersonData"]["PersonId"])) ? $result2 : $result2["PersonData"];
        foreach ($rrr as $row) {
            // log in DB
            $this->log_db_zz($row);

            if (!isset($row["School"]["SchoolId"]))
                continue;

            if (!isset($row["School"]["SchoolClass"]))
                continue;

            if (!$row["IsAlive"] == "true" || !is_array($row["School"]) || intval($row["School"]["SchoolId"]) <= 0)
                continue;

            $person_etickets = Eticket::where("pk", $row["PersonId"])->get();
            if ($person_etickets->count() > 0) {
                foreach ($person_etickets as $eticket) {
                    // name and grade
                    $eticket->name = $row["PersonName"] . ' ' . $row["PersonSurname"];
                    $eticket->grade = $row["School"]["SchoolClass"];

                    // free meals if exists, all types
                    $free = isset($row["FreeMeals"]["FreeMealPeriod"]) ? $this->find_current_periods($row["FreeMeals"]["FreeMealPeriod"]) : ['0000-00-00', '0000-00-00'];
                    $unused = isset($row["UnusedMeals"]["UnusedMeal"]) ? $this->find_current_periods($row["UnusedMeals"]["UnusedMeal"]) : ['0000-00-00', '0000-00-00'];
                    $rd = isset($row["RdMeals"]["RdMeal"]) ? $this->find_current_periods($row["RdMeals"]["RdMeal"]) : ['0000-00-00', '0000-00-00'];

                    $kopgalds = isset($row["TableRdMeals"]["TableRdMeal"]) ? $this->find_current_periods($row["TableRdMeals"]["TableRdMeal"]) : ['0000-00-00', '0000-00-00'];

                    // check declared status only for first type
                    if ($row["DeclaredRiga"] == "true") {
                        $eticket->free_meals_from = $free[0];
                        $eticket->free_meals_until = $free[1];
                    } else {
                        $eticket->free_meals_from = '0000-00-00';
                        $eticket->free_meals_until = '0000-00-00';
                    }

                    $eticket->second_meals_from = $unused[0];
                    $eticket->second_meals_until = $unused[1];
                    $eticket->third_meals_from = $rd[0];
                    $eticket->third_meals_until = $rd[1];
                    $eticket->kopgalds_from = $kopgalds[0];
                    $eticket->kopgalds_until = $kopgalds[1];

                    $this->info("Updating eticket $eticket->nr for PK $eticket->pk ");

                    if (!$eticket->save()) {
                        $this->error("Kļūda atjaunojot etalona datus - " . json_encode($eticket->getErrors()));
                        Log::error("dailymonitoring, etickets - DB update - " . json_encode($eticket->getErrors()));
                    }

                    // school
                    if (isset($row["School"]["SchoolId"])) {
                        $school_reg_nr = School::findOrFail($row["School"]["SchoolId"])->reg_nr;
                        $new_object = Objekts::where("object_regnr", $school_reg_nr)->first();
                        if (isset($new_object->id)) {
                            if (isset($eticket->pirmais_objekts()->id)) {
                                $old_object_id = intval($eticket->pirmais_objekts()->id);
                                if ($old_object_id != $new_object->id) {
                                    $this->info("Updating eticket objects $eticket->nr for PK $eticket->pk ");
                                    $eticket->saveObjekti([$new_object->id], 0);
                                }
                            }
                        }
                    }
                }
            } else {
                // check if needs to be inserted in base data with free meals

                $free1 = isset($row["FreeMeals"]["FreeMealPeriod"]);
                $free2 = isset($row["UnusedMeals"]["UnusedMeal"]);
                $free3 = isset($row["RdMeals"]["RdMeal"]);

                $free4 = isset($row["TableRdMeals"]["TableRdMeal"]);

                if ($free1 || $free2 || $free3 || $free4) {
                    $free_to_check = $free3 ? 
                        $row["RdMeals"]["RdMeal"] : 
                        ($free4 ? 
                            $row["TableRdMeals"]["TableRdMeal"] : 
                            ($free2 ? 
                                $row["UnusedMeals"]["UnusedMeal"] :
                                $row["FreeMeals"]["FreeMealPeriod"]
                            )
                        );

                    if (is_array($free_to_check) && count($free_to_check) > 1) {
                        $rk_batch[] = $row["PersonId"];

                        $pk = $row["PersonId"];
                        if (!in_array($pk, $used_pks)) {
                            array_push($used_pks, $pk);
                            $person_data[$pk] = $row;
                        }

                        // process every batch
                        if (count($rk_batch) == 100) {
                            $this->rk_batch_call($person_data, $rk_batch);
                            $rk_batch = [];
                        }
                    }
                }
            }
        }

        // process remaining
        if (count($rk_batch) > 0) {
            $this->rk_batch_call($person_data, $rk_batch);
        }

        // total time
        $end = time();
        $this->log_api_data("zz", date("Y-m-d H:i:s") . " - dailymonitoringchanges total time in seconds=" . ($end - $start));

        $this->info('Informācijas atjaunošana no monitoringa sekmīgi pabeigta');
    }

    /**
     * @param $row
     * @return mixed
     */
    private function rk_batch_call($person_data, $pks = [])
    {
        // RK API call
        $xml = 
        '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <GetCardsByPersonId xmlns="http://www.rigaskarte.lv/dpa/addon">
                        <personCodes>';
                        foreach ($pks as $pk) {
                            $xml .= '<string>' . $pk . '</string>';
                        }
                        $xml .= 
                        '</personCodes>
                    </GetCardsByPersonId>
                </soap:Body>
            </soap:Envelope>';

        $headers = array(
            "POST  HTTP/1.1",
            "Host: 91.231.68.12",
            "Content-type: text/xml; charset=\"utf-8\"",
            "SOAPAction: \"http://www.rigaskarte.lv/dpa/addon/GetCardsByPersonsIds\"",
            "Content-length: " . strlen($xml)
        );

        $url = Config::get('services.rs.rkurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, Config::get('services.rs.rk'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 58);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $rk_result = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch) || $http_status != 200) {
            $this->error("Kļūda saņemot RK API datus - " . curl_error($ch));
            Log::error("dailymonitoring, RK API - CURL error - " . curl_error($ch));
        }
        $info = curl_getinfo($ch);
        curl_close($ch);

        // LOG data to file
        $this->log_api_data("rk", "get etickets", $rk_result, $info["url"], $info["total_time"]);

        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $rk_result);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string = str_replace('<soap:', '<', $xml_string);
        $res1 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
        $result1 = isset($res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"]) ?
                $res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"] : [];

        $pk_etickets = [];

        foreach ($result1 as $eticket_row) {
            // log in DB
            $this->log_db_rk($eticket_row);

            if (!isset($eticket_row["SerialNumber"]))
                continue;

            if (strtotime(substr($eticket_row["ExpirationDate"], 0, 10)) > time()) {
                if (!isset($pk_etickets[$eticket_row["PersonCode"]]))
                    $pk_etickets[$eticket_row["PersonCode"]] = [];
                array_push($pk_etickets[$eticket_row["PersonCode"]], $eticket_row["SerialNumber"]);

                // insert in etickets those e-tickets which does not exist
                $exists = Eticket::where("nr", $eticket_row["SerialNumber"])->first();
                if (!isset($exists->id)) {
                    $school = School::find($person_data[$eticket_row["PersonCode"]]["School"]["SchoolId"]);
                    if (!isset($school->reg_nr))
                        continue;

                    $objekts = Objekts::where("object_regnr", $school->reg_nr)->first();
                    if (!isset($objekts->id)) {
                        $this->error("Kļūda - nevaru atrast objektu ar reg. nr. " . $school->reg_nr);
                        Log::error("getfreemealpersondata - nevaru atrast objektu ar reg. nr. " . $school->reg_nr);
                    } else {

                        // check declared status only for first type
                        if ($person_data[$eticket_row["PersonCode"]]["DeclaredRiga"] == "true") {
                            $mas = isset($person_data[$eticket_row["PersonCode"]]["FreeMeals"]["FreeMealPeriod"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["FreeMeals"]["FreeMealPeriod"]) : ['0000-00-00', '0000-00-00'];
                        } else {
                            $mas = [];
                        }
                        $mas2 = isset($person_data[$eticket_row["PersonCode"]]["UnusedMeals"]["UnusedMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["UnusedMeals"]["UnusedMeal"]) : ['0000-00-00', '0000-00-00'];
                        $mas3 = isset($person_data[$eticket_row["PersonCode"]]["RdMeals"]["RdMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["RdMeals"]["RdMeal"]) : ['0000-00-00', '0000-00-00'];
                        $mas4 = isset($person_data[$eticket_row["PersonCode"]]["TableRdMeals"]["TableRdMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["TableRdMeals"]["TableRdMeal"]) : ['0000-00-00', '0000-00-00'];

                        App::make('App\Http\Controllers\etickets\EticketsController')->save_new_eticket(
                            $eticket_row["SerialNumber"], 
                            $eticket_row["PersonCode"], 
                            $person_data[$eticket_row["PersonCode"]]["PersonName"] . ' ' . $person_data[$eticket_row["PersonCode"]]["PersonSurname"], 
                            "0000", 
                            $person_data[$eticket_row["PersonCode"]]["School"]["SchoolClass"], 
                            0, 
                            2, 
                            $objekts->id, 
                            $person_data[$eticket_row["PersonCode"]]["School"]["SchoolId"], 
                            $mas, 
                            $mas2, 
                            $mas3, 
                            $mas4
                        );
                    }
                }
            }
        }

        foreach ($pk_etickets as $current_pk => $current_etickets) {
            $etickets_string = implode(",", $current_etickets);

            // add or update DB, etickets base
            if ($etickets_string !== '' && isset($person_data[$current_pk])) {
                if (!isset($person_data[$current_pk]["School"]["SchoolClass"]))
                    continue;

                $base = EticketsBase::where("pk", $current_pk)->first();

                if (isset($base->id)) {
                    $base->etickets = $etickets_string;
                    $base->custom = 0;
                    $base->name = $person_data[$current_pk]["PersonName"];
                    $base->surname = $person_data[$current_pk]["PersonSurname"];
                    $base->grade = $person_data[$current_pk]["School"]["SchoolClass"];
                    $base->school_id = $person_data[$current_pk]["School"]["SchoolId"];

                    // check declared status only for first type
                    if ($person_data[$current_pk]["DeclaredRiga"] == "true") {
                        $base->free_meals_from = (isset($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]["DateFrom"])) ? substr($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]["DateFrom"], 0, 10) : '0000-00-00';
                        $base->free_meals_until = (isset($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]["DateTill"])) ? substr($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]["DateTill"], 0, 10) : '0000-00-00';
                    } else {
                        $base->free_meals_from = '0000-00-00';
                        $base->free_meals_until = '0000-00-00';
                    }

                    if(isset($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"])){
                        $base->second_meals_from = (isset($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]["DateFrom"])) ? substr($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]["DateFrom"], 0, 10) : '0000-00-00';
                        $base->second_meals_until = (isset($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]["DateTill"])) ? substr($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]["DateTill"], 0, 10) : '0000-00-00';
                    }else{
                        $base->second_meals_from = '0000-00-00';
                        $base->second_meals_until = '0000-00-00';
                    }
                    $base->third_meals_from = (isset($person_data[$current_pk]["RdMeals"]["RdMeal"]["DateFrom"])) ? substr($person_data[$current_pk]["RdMeals"]["RdMeal"]["DateFrom"], 0, 10) : '0000-00-00';
                    $base->third_meals_until = (isset($person_data[$current_pk]["RdMeals"]["RdMeal"]["DateTill"])) ? substr($person_data[$current_pk]["RdMeals"]["RdMeal"]["DateTill"], 0, 10) : '0000-00-00';

                    $base->kopgalds_from = (isset($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]["DateFrom"])) ? substr($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]["DateFrom"], 0, 10) : '0000-00-00';
                    $base->kopgalds_until = (isset($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]["DateTill"])) ? substr($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]["DateTill"], 0, 10) : '0000-00-00';

                    $base->brivpusdienas = ($base->free_meals_until != '0000-00-00' || $base->second_meals_until != '0000-00-00' || $base->third_meals_until != '0000-00-00') ? 1 : 0;
                    if (!$base->save()) {
                        $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                        Log::error("dailymonitoring, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                    }
                } else {
                    $base = New EticketsBase();
                    $base->pk = $current_pk;
                    $base->etickets = $etickets_string;
                    $base->custom = 0;
                    $base->name = $person_data[$current_pk]["PersonName"];
                    $base->surname = $person_data[$current_pk]["PersonSurname"];
                    $base->grade = $person_data[$current_pk]["School"]["SchoolClass"];
                    $base->school_id = $person_data[$current_pk]["School"]["SchoolId"];

                    // check declared status only for first type
                    if ($person_data[$current_pk]["DeclaredRiga"] == "true") {
                        $base->free_meals_from = (isset($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]["DateFrom"])) ? substr($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]["DateFrom"], 0, 10) : '0000-00-00';
                        $base->free_meals_until = (isset($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]["DateTill"])) ? substr($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]["DateTill"], 0, 10) : '0000-00-00';
                    } else {
                        $base->free_meals_from = '0000-00-00';
                        $base->free_meals_until = '0000-00-00';
                    }

                    if(isset($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"])){
                        $base->second_meals_from = (isset($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]["DateFrom"])) ? substr($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]["DateFrom"], 0, 10) : '0000-00-00';
                        $base->second_meals_until = (isset($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]["DateTill"])) ? substr($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]["DateTill"], 0, 10) : '0000-00-00';
                    }else {
                        $base->second_meals_from = '0000-00-00';
                        $base->second_meals_until = '0000-00-00';
                    }
                    $base->third_meals_from = (isset($person_data[$current_pk]["RdMeals"]["RdMeal"]["DateFrom"])) ? substr($person_data[$current_pk]["RdMeals"]["RdMeal"]["DateFrom"], 0, 10) : '0000-00-00';
                    $base->third_meals_until = (isset($person_data[$current_pk]["RdMeals"]["RdMeal"]["DateTill"])) ? substr($person_data[$current_pk]["RdMeals"]["RdMeal"]["DateTill"], 0, 10) : '0000-00-00';
                    $base->kopgalds_from = (isset($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]["DateFrom"])) ? substr($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]["DateFrom"], 0, 10) : '0000-00-00';
                    $base->kopgalds_until = (isset($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]["DateTill"])) ? substr($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]["DateTill"], 0, 10) : '0000-00-00';

                    $base->brivpusdienas = ($base->free_meals_until != '0000-00-00' || $base->second_meals_until != '0000-00-00' || $base->third_meals_until != '0000-00-00') ? 1 : 0;
                    if (!$base->save()) {
                        $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                        Log::error("dailymonitoring, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                    }
                }
            }
        }

        return;
    }

}
