<?php

namespace App\Console\Commands;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

use App\Http\Controllers\RollingCurlX;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\Objekts;
use App\Models\School;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class importCsv extends Command
{

    protected $total_etickets;
    protected $person_data;

    public function __construct()
    {
        $this->total_etickets = 0;
        $this->person_data = [];

        parent::__construct();
    }

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'importcsv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importēt info no CSV faila un sanemt karsu datus no API';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $result = [];
        $result2 = [];
        try {
            $handle = fopen(storage_path("viis_berni_ekartes.csv"), "r");
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $result[] = $data;
            }
            fclose($handle);

            foreach ($result as $row) {
                $row2 = $row;
                $row2[4] = substr($row[4], 0, 6) . '-' . substr($row[4], 6, 5);
                $result2[$row2[4]] = $row2;
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        $total_pks = count($result2);

        $all_schools = School::has("prices")->lists("heading", "id")->all();
        $all_school_regnrs = School::lists("reg_nr", "id")->all();
        $all_objects = Objekts::lists("id", "object_regnr")->all();

        foreach ($result2 as $key => $row) {
            $school_name = $row[5];
            if (!in_array($school_name, $all_schools)) {
                unset($result2[$key]);
            } else {
                $school_id = array_search($school_name, $all_schools);
                $school_reg_nr = isset($all_school_regnrs[$school_id]) ? $all_school_regnrs[$school_id] : 0;

                if ($school_id > 0 && intval($school_reg_nr) > 10) {
                    $object_id = isset($all_objects[$school_reg_nr]) ? $all_objects[$school_reg_nr] : 0;

                    if ($object_id > 0) {
                        $result2[$key][8] = $school_id;
                        $result2[$key][9] = $object_id;
                    } else
                        unset($result2[$key]);
                } else
                    unset($result2[$key]);
            }
        }

        $used_pks = [];

        $total_usable_pks = count($result2);

        $this->info('PK failā: ' . $total_pks);
        $this->info('Izmantojami PK (sistēmā esošas skolas): ' . $total_usable_pks);

        // TMP for testing
        //$result2 = array_slice($result2, 2000, 1000, true);
        //

        $split_to = array_chunk($result2, 100);

        $RCX = new RollingCurlX(5);

        $this->output->progressStart(count($split_to));

        foreach ($split_to as $split_50) {
            // get person etickets from RK API
            $pks = [];

            foreach ($split_50 as $person_row) {
                $pk = $person_row[4];
                if (!in_array($pk, $used_pks)) {
                    array_push($used_pks, $pk);
                    array_push($pks, $pk);
                    $this->person_data[$pk] = $person_row;
                }
            }

            // RK API call
            $xml = '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <GetCardsByPersonsIds xmlns="http://www.rigaskarte.lv/dpa/addon">
                  <personCodes>';
            foreach ($pks as $pk) {
                $xml .= '<string>' . $pk . '</string>';
            }
            $xml .= '</personCodes>
                </GetCardsByPersonsIds>
              </soap:Body>
            </soap:Envelope>';

            $headers = array(
                "POST  HTTP/1.1",
                "Host: 91.231.68.12",
                "Content-type: text/xml; charset=\"utf-8\"",
                "SOAPAction: \"http://www.rigaskarte.lv/dpa/addon/GetCardsByPersonsIds\"",
                "Content-length: " . strlen($xml)
            );

            $url = Config::get('services.rs.rkurl');

            $options = [
                CURLOPT_HTTPAUTH => CURLAUTH_ANY,
                CURLOPT_USERPWD => Config::get('services.rs.rk'),
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_FOLLOWLOCATION => true
            ];

            $RCX->addRequest($url, $xml, [$this, 'callback_functn'], [], $options, $headers);
            $RCX->setTimeout(999000); //in milliseconds
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL,$url);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//            curl_setopt($ch, CURLOPT_USERPWD, Config::get('services.rs.rk'));
//            curl_setopt($ch, CURLOPT_POST, true);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//            curl_setopt($ch, CURLOPT_TIMEOUT, 900);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            $rk_result = curl_exec($ch);
//            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//            if (curl_errno($ch) || $http_status != 200)
//            {
//                $this->error("Kļūda saņemot RK API datus ($http_status) - " . curl_error($ch));
//                Log::error("getfreepersons, RK API ($http_status) - CURL error - " . curl_error($ch));
//                return;
//            }
//            curl_close($ch);
//            $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $rk_result);
//            $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
//            $xml_string =str_replace('<soap:', '<', $xml_string);
//            $res1 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
//            $result1 = isset($res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"]) ?
//                $res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"] : [];
            //$this->output->progressAdvance();
        }

        $RCX->execute();

        $this->output->progressFinish();

        $this->info('Imports pabeigts');
        $this->info('Kopā e-taloni: ' . $this->total_etickets);
    }

    // callback
    public function callback_functn($response, $url, $request_info, $user_data, $time)
    {
        Log::info($time);

        if ($request_info["http_code"] != 200) {
            $this->error("Kļūda saņemot RK API datus - " . $request_info["http_code"]);
            Log::error("getfreepersons, RK API  - CURL error - " . $request_info["http_code"]);
            return;
        }

        $rk_result = $response;
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $rk_result);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string = str_replace('<soap:', '<', $xml_string);
        $res1 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
        $result1 = isset($res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"]) ?
                $res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"] : [];

        $this->process_rk_data($result1, []);

        $this->output->progressAdvance();
    }

    /**
     * @param $result1
     * @param $pk_etickets
     * @return array
     */
    private function process_rk_data($result1, $pk_etickets = [])
    {
        foreach ($result1 as $eticket_row) {
            if (!isset($eticket_row["SerialNumber"])) {
                $this->error("Nav sanemts e-talons - " . json_encode($eticket_row));
                continue;
            }

            if (strtotime(substr($eticket_row["ExpirationDate"], 0, 10)) > time()) {
                if (!isset($pk_etickets[$eticket_row["PersonCode"]])) {
                    $pk_etickets[$eticket_row["PersonCode"]] = [];
                }
                array_push($pk_etickets[$eticket_row["PersonCode"]], $eticket_row["SerialNumber"]);

                // insert in etickets those e-tickets which does not exist
                $exists = Eticket::where("nr", $eticket_row["SerialNumber"])->first();
                if (!isset($exists->id)) {
                    $mas3 = ['DateFrom' => '2016-08-31', 'DateTill' => '2017-06-01'];

                    try {
                        App::make('App\Http\Controllers\etickets\EticketsController')->save_new_eticket(
                            $eticket_row["SerialNumber"], 
                            $eticket_row["PersonCode"], 
                            $this->person_data[$eticket_row["PersonCode"]][1] . ' ' . $this->person_data[$eticket_row["PersonCode"]][2] . ' ' . $this->person_data[$eticket_row["PersonCode"]][3], 
                            "0000", 
                            $this->person_data[$eticket_row["PersonCode"]][6] . $this->person_data[$eticket_row["PersonCode"]][7], 
                            0, 
                            2, 
                            $this->person_data[$eticket_row["PersonCode"]][9], 
                            $this->person_data[$eticket_row["PersonCode"]][8], 
                            [], 
                            [], 
                            $mas3
                        );

                        $this->total_etickets++;
                    } catch (\Exception $e) {
                        Log::error("error saving eticket - " . $e->getMessage() . " \n" . $e->getTraceAsString());
                        $this->error("error saving eticket - " . $e->getMessage());
                    }
                }
            }
            //else $this->error("E-talons ar beigušos termiņu - ".json_encode($eticket_row));
        }

        foreach ($pk_etickets as $current_pk => $current_etickets) {
            $etickets_string = implode(",", $current_etickets);

            // add or update DB, etickets base
            if ($etickets_string !== '' && isset($this->person_data[$current_pk])) {
                $base = EticketsBase::where("pk", $current_pk)->first();

                if (isset($base->id)) {
                    $base->etickets = $etickets_string;
                    $base->custom = 0;
                    $base->name = trim($this->person_data[$current_pk][1] . ' ' . $this->person_data[$current_pk][2]);
                    $base->surname = $this->person_data[$current_pk][3];
                    $base->grade = trim($this->person_data[$current_pk][6] . $this->person_data[$current_pk][7]);
                    $base->school_id = $this->person_data[$current_pk][8];
                    $base->free_meals_from = '0000-00-00';
                    $base->free_meals_until = '0000-00-00';
                    $base->second_meals_from = '0000-00-00';
                    $base->second_meals_until = '0000-00-00';
                    $base->third_meals_from = '2016-08-31';
                    $base->third_meals_until = '2017-06-01';
                    $base->brivpusdienas = 1;
                    if (!$base->save()) {
                        $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                        Log::error("dailymonitoring, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                    }
                } else {
                    $base = New EticketsBase();
                    $base->pk = $current_pk;
                    $base->etickets = $etickets_string;
                    $base->custom = 0;
                    $base->name = trim($this->person_data[$current_pk][1] . ' ' . $this->person_data[$current_pk][2]);
                    $base->surname = $this->person_data[$current_pk][3];
                    $base->grade = trim($this->person_data[$current_pk][6] . $this->person_data[$current_pk][7]);
                    $base->school_id = $this->person_data[$current_pk][8];
                    $base->free_meals_from = '0000-00-00';
                    $base->free_meals_until = '0000-00-00';
                    $base->second_meals_from = '0000-00-00';
                    $base->second_meals_until = '0000-00-00';
                    $base->third_meals_from = '2016-08-31';
                    $base->third_meals_until = '2017-06-01';
                    $base->brivpusdienas = 1;
                    if (!$base->save()) {
                        $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                        Log::error("dailymonitoring, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                    }
                }
            }
        }

        return;
    }

}
