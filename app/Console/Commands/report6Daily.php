<?php

namespace App\Console\Commands;

ini_set("memory_limit", -1);

use App\Models\Eticket;
use App\Models\Mpos;
use App\Models\Objekts;
use App\Models\Seller;
use App\Models\Transaction;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class report6Daily extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'report6';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DIENAS ATSKAITE DARĪJUMI LIETOTĀJI - visas personas, visi etaloni';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = time();

        $datetime = new DateTime('yesterday');
        $vakardiena = $datetime->format('Y-m-d');

        $table = [];
        $all_etickets = Eticket::select('id', 'account_id', 'name', 'nr')
            ->where("account_id", '!=', 2)
            ->get();

        try {
            foreach ($all_etickets as $eticket) {
                $objekti_ids = $eticket->activeObjekti();
                $objekti = implode(", ", Objekts::whereIn("id", $objekti_ids)->lists("object_name")->toArray());

                $owner_ids = Mpos::whereIn("object_id", $objekti_ids)->lists("owner_id")->toArray();
                $sellers = implode(", ", Seller::whereIn("owner_id", $owner_ids)->lists("seller_name")->toArray());

                $daily_balance = Transaction::where("from_account_id", $eticket->account_id)
                    ->Ok()
                    ->where("created_at", 'LIKE', $vakardiena . '%')
                    ->sum("amount");

                if (isset($eticket->account->owner->owner_name))
                    $table[] = [$eticket->account->owner->owner_name, $eticket->name, $eticket->nr, $objekti, $sellers, sprintf("%.2f", $daily_balance / 100), sprintf("%.2f", $eticket->account->balance / 100), $vakardiena];
            }

            Excel::create('DIENAS ATSKAITE DARĪJUMI LIETOTĀJI ' . $vakardiena, function($excel) use ($vakardiena, $table) {
                $excel->setTitle('Skolas.rigaskarte.lv - DIENAS ATSKAITE DARĪJUMI LIETOTĀJI ' . $vakardiena);
                $excel->setCreator('skolas.rigaskarte.lv')->setCompany('skolas.rigaskarte.lv');
                $excel->setDescription('Skolas.rigaskarte.lv - DIENAS ATSKAITE DARĪJUMI LIETOTĀJI ' . $vakardiena);

                $excel->sheet('Atskaite ' . $vakardiena, function($sheet) use($table) {
                    $sheet->setOrientation('landscape');
                    $sheet->getPageSetup()->setFitToPage(false);
                    $sheet->getDefaultRowDimension()->setRowHeight(-1);
                    $sheet->setWidth(array(
                        'A' => 20,
                        'B' => 20,
                        'C' => 11,
                        'D' => 25,
                        'E' => 25,
                        'F' => 12,
                        'G' => 9,
                        'H' => 12,
                    ));
                    $sheet->setHeight(0, 40);

                    $sheet->fromArray($table, null, 'A1', false, false);

                    $sheet->prependRow(['E-konta pārvaldnieks', 'E-kartes lietotājs', 'E-kartes Nr.', 'Skola', 'Tirgotājs', 'Dienas apgrozījums EUR', 'Atlikums EUR', 'Datums']);

                    $sheet->freezeFirstRow();

                    $sheet->cells('A1:Z1', function($cells) {
                        $cells->setFontWeight('bold')->setBackground('#CCCCCC');
                    });
                    $sheet->getStyle('A1:Z1')->getAlignment()->setWrapText(true);
                    $sheet->getStyle('A2:B3999')->getAlignment()->setWrapText(true);
                    $sheet->getStyle('D2:E3999')->getAlignment()->setWrapText(true);

                    $sheet->setColumnFormat(array(
                        'F2:G3999' => '0.00'
                    ));
                });
            })->store('xlsx');

            //send email to RK
            Mail::send('mail.report', ["info" => "DIENAS ATSKAITE DARĪJUMI LIETOTĀJI " . $vakardiena], function($message) use ($vakardiena) {
                $message->to(Config::get('mail.rsemail1'))->cc(Config::get('mail.rsemail2'))->subject('Skolas.rigaskarte.lv - DIENAS ATSKAITE DARĪJUMI LIETOTĀJI ' . $vakardiena);
                $message->attach(storage_path() . "/exports/DIENAS ATSKAITE DARĪJUMI LIETOTĀJI " . $vakardiena . ".xlsx");
            });

            $end = time();
            $this->info('Atskaite veiksmīgi ģenerēta un nosūtīta - (' . ($end - $start) . 's).');
        } catch (\Exception $e) {
            $this->error('Kļūda apstrādājot datus - ' . $e->getMessage());
        }
    }

}
