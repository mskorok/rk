<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Seller;
use App\Models\SellerBalances;
use App\Models\SellerPayments;
use App\Models\Settings;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class reGenerateSellerPayments extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'regenpayments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reģenerēt tirgotāju izmaksas 3x mēnesī';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regenpayments {exec_date?}';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $execDate = $this->ask('Executing date (YYYY-mm-dd)?');
        $transactionId = $this->ask('Transaction ID?');
        $sellerId = $this->ask('Seller ID?');
        $payment_id = $this->ask('Payment ID?');
        /*
        $execDate = '2017-09-11';
        $transactionId = 700276;
        $sellerId = 15;
        $payment_id = 931;
         * 
         */
        
        $sodiena = new Carbon($execDate);
        if ($sodiena->day < 11) {
            $sodiena->subMonth();
            $periods = substr($sodiena->toDateString(), 0, 7) . '-3';
            $periods_no = substr($sodiena->toDateString(), 0, 7) . '-21';
            $sodiena2 = $sodiena;
            $sodiena2->addMonth();
            $sodiena2->day = 0;
            $periods_lidz = $sodiena2->toDateString();
        } else if ($sodiena->day < 21) {
            $periods = substr($sodiena->toDateString(), 0, 7) . '-1';
            $periods_no = substr($sodiena->toDateString(), 0, 7) . '-01';
            $periods_lidz = substr($sodiena->toDateString(), 0, 7) . '-10';
        } else {
            $periods = substr($sodiena->toDateString(), 0, 7) . '-2';
            $periods_no = substr($sodiena->toDateString(), 0, 7) . '-11';
            $periods_lidz = substr($sodiena->toDateString(), 0, 7) . '-20';
        }

        $seller = Seller::where("id", $sellerId)->first();
        // generate PDF
        $data = Transaction::with('from_account')->where('id', $transactionId)->Ok()->firstOrFail();
        $outputName = $data->comments;
        
        $settings = Settings::firstOrFail();

        $datums_no = $periods_no;
        $datums_lidz = $periods_lidz;

        $html = 
        '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
            <head profile="http://gmpg.org/xfn/11">
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <style tyle="text/css">body { font-family: DejaVu Sans, sans-serif; }</style>
            </head>'.
            '<body>' .
                View::make('accountant/receipt', compact('data', 'seller', 'settings', 'datums_no', 'datums_lidz', 'payment_id')).
            '</body>'.
        '</html>';

        if (!is_dir(public_path('assets/pdfs'))) {
            mkdir(public_path('assets/pdfs'), 0755, true);
        }

        $pdfPath = public_path('assets/pdfs') . '/' . $outputName . '.pdf';

        $content = \PDF::loadHTML($html)->output();

        File::put($pdfPath, $content);

        $this->info('Tirgotāju izmaksu reģenerēšana sekmīgi pabeigta');
    }

}
