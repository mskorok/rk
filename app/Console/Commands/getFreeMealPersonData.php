<?php

namespace App\Console\Commands;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

use App\Models\DailyLogZz;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\EticketsObjekti;
use App\Models\GroupObjekts;
use App\Models\Objekts;
use App\Models\School;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class getFreeMealPersonData extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'getfreemealpersons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atjaunot informāciju no API par personām ar brīvpusdienām';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // total start timer
        $start = time();

        $xml = 
        '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://tempuri.org/IMonitoringService/GetPersonsWithLowIncome</wsa:Action>
            </soap:Header>
            <soap:Body>
                <GetPersonsWithLowIncome xmlns="http://tempuri.org/">
                    <periodType></periodType>
                </GetPersonsWithLowIncome>
            </soap:Body>
        </soap:Envelope>';

        $headers = array(
            "POST HTTP/1.1",
            "Accept-Encoding: gzip,deflate",
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/GetPersonsWithLowIncome"',
            "Content-length: " . strlen($xml),
            "Host: " . Config::get('services.rs.zzurl2'),
            "Connection: Keep-Alive",
            "User-Agent: skolas.rigaskarte.lv",
        );

        $url = Config::get('services.rs.zzurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 990);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLCERT, storage_path() . "/zz_cert.pem");
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, Config::get('services.rs.certkey'));
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $this->error("Kļūda saņemot lowincome datus - " . curl_error($ch));
            Log::error("getlowincome - CURL error - " . curl_error($ch));
            return;
        }
        $info = curl_getinfo($ch);
        curl_close($ch);

        // LOG data to file
        $this->log_api_data("zz", "getlowincome", $result, $info["url"], $info["total_time"]);

        // process data
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $result);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string = str_replace('<s:', '<', $xml_string);
        $xml_string = str_replace('<a:', '<', $xml_string);
        $xml_string = str_replace('<b:', '<', $xml_string);

        $res = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
        $result2 = $res["Body"]["GetPersonsWithLowIncomeResponse"]["GetPersonsWithLowIncomeResult"]["Changes"]["PersonData"];

        $used_pks = [];

        // process ZZ result, leaving to RK only new PKs
        $this->info("Kopā no ZZ saņemti ieraksti: " . count($result2));
        foreach ($result2 as $key => $row) {
            // log in DB
            $this->log_db_zz($row);

            if (!isset($row["School"]["SchoolId"]))
                continue;

            // check all if valid
            if (!$row["IsAlive"] == "true" || !is_array($row["School"]) || intval($row["School"]["SchoolId"]) <= 0)
                unset($result2[$key]);

            // check if exists
            if (Eticket::A()->where("pk", $row["PersonId"])->count() > 0) {
                $school = School::find($row["School"]["SchoolId"]);
                if (isset($school->reg_nr)) {
                    $objekts = Objekts::where("object_regnr", $school->reg_nr)->first();

                    if (!isset($objekts->id)) {
                        $this->error("Kļūda - nevaru atrast objektu ar reg. nr. " . $school->reg_nr);
                        Log::error("getfreemealpersondata - nevaru atrast objektu ar reg. nr. " . $school->reg_nr);
                    } else {

                        // update all currrent etickets for this person
                        $person_etickets = Eticket::A()->where("pk", $row["PersonId"])->get();
                        foreach ($person_etickets as $eticket) {
                            $eticket->name = $row["PersonName"] . ' ' . $row["PersonSurname"];
                            $eticket->grade = $row["School"]["SchoolClass"];

                            $free = isset($row["FreeMeals"]["FreeMealPeriod"]) ? $this->find_current_periods($row["FreeMeals"]["FreeMealPeriod"]) : ['0000-00-00', '0000-00-00'];
                            $unused = isset($row["UnusedMeals"]["UnusedMeal"]) ? $this->find_current_periods($row["UnusedMeals"]["UnusedMeal"]) : ['0000-00-00', '0000-00-00'];
                            $rd = isset($row["RdMeals"]["RdMeal"]) ? $this->find_current_periods($row["RdMeals"]["RdMeal"]) : ['0000-00-00', '0000-00-00'];
                            $kopgalds = isset($row["TableRdMeals"]["TableRdMeal"]) ? $this->find_current_periods($row["TableRdMeals"]["TableRdMeal"]) : ['0000-00-00', '0000-00-00'];

                            // check declared status only for first type
                            if ($row["DeclaredRiga"] == "true") {
                                $eticket->free_meals_from = $free[0];
                                $eticket->free_meals_until = $free[1];
                            } else {
                                $eticket->free_meals_from = '0000-00-00';
                                $eticket->free_meals_until = '0000-00-00';
                            }

                            $eticket->second_meals_from = $unused[0];
                            $eticket->second_meals_until = $unused[1];
                            $eticket->third_meals_from = $rd[0];
                            $eticket->third_meals_until = $rd[1];
                            $eticket->kopgalds_from = $kopgalds[0];
                            $eticket->kopgalds_until = $kopgalds[1];
                            if (!$eticket->save()) {
                                $this->error("Kļūda - nevaru saglabāt izmaiņas e-talonam: " . $eticket->id);
                                Log::error("getfreemealpersondata - nevaru saglabāt izmaiņas e-talonam: " . $eticket->id);
                            }

                            // update e-tickets objects
                            $this->update_eticket_objects($eticket, $objekts);
                        }
                    }
                }

                // add to recieved array
                $used_pks[] = $row["PersonId"];

                // remove from RK API array
                unset($result2[$key]);
            }
        }

        $this->info("Tiks pieprasiti no RK jauni ieraksti: " . count($result2));

        $split_to = array_chunk($result2, 100);

        foreach ($split_to as $split_50) {
            // get person etickets from RK API
            $pks = [];
            $person_data = [];

            foreach ($split_50 as $person_row) {
                $pk = $person_row["PersonId"];
                if (!in_array($pk, $used_pks)) {
                    array_push($used_pks, $pk);
                    array_push($pks, $pk);
                    $person_data[$pk] = $person_row;
                }
            }

            // RK API call
            $xml = '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <GetCardsByPersonsIds xmlns="http://www.rigaskarte.lv/dpa/addon">
                  <personCodes>';
            Log::info(["checkRK" => $pks]);
            foreach ($pks as $pk) {
                $xml .= '<string>' . $pk . '</string>';
            }
            $xml .= '</personCodes>
                </GetCardsByPersonsIds>
              </soap:Body>
            </soap:Envelope>';

            $headers2 = array(
                "POST  HTTP/1.1",
                "Host: 91.231.68.12",
                "Content-type: text/xml; charset=\"utf-8\"",
                "SOAPAction: \"http://www.rigaskarte.lv/dpa/addon/GetCardsByPersonsIds\"",
                "Content-length: " . strlen($xml)
            );

            $url2 = Config::get('services.rs.rkurl');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url2);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers2);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, Config::get('services.rs.rk'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 990);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $rk_result = curl_exec($ch);
            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (curl_errno($ch) || $http_status != 200) {
                $this->error("Kļūda saņemot RK API datus ($http_status) - " . curl_error($ch));
                Log::error("getfreepersons, RK API ($http_status) - CURL error - " . curl_error($ch));
                return;
            }
            $info = curl_getinfo($ch);
            curl_close($ch);

            // LOG data to file
            $this->log_api_data("rk", "get etickets", $rk_result, $info['url'], $info['total_time']);

            $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $rk_result);
            $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
            $xml_string = str_replace('<soap:', '<', $xml_string);
            $res1 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
            $result1 = isset($res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"]) ?
                    $res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"] : [];

            foreach ($result1 as $eticket_row) {
                if (!isset($eticket_row["SerialNumber"]))
                    continue;

                $this->process_single_eticket_rk_response($eticket_row, $person_data);
            }

            $this->process_all_persons_to_base_table($person_data);
            $this->pk_etickets = [];
        }

        // delete all current free meal periods, that are not received
        $used_pks = array_unique($used_pks);

        $this->cleanup($used_pks);

        // total time
        $end = time();
        $this->log_api_data("zz", "getfreemealpersons total time in seconds=" . ($end - $start));

        $this->info('Personu informācijas atjaunošana sekmīgi pabeigta');
    }

    /**
     * @param $used_pks
     */
    protected function cleanup($used_pks)
    {
        $cleanup = Eticket::A()
                ->whereNotIn("pk", $used_pks)
                ->where(function ($query) {
                    $query->where('free_meals_until', '!=', '0000-00-00');
                    $query->orWhere('second_meals_until', '!=', '0000-00-00');
                    $query->orWhere('third_meals_until', '!=', '0000-00-00');
                    $query->orWhere('kopgalds_until', '!=', '0000-00-00');
                })
                ->get();

        foreach ($cleanup as $eticket) {
            $eticket->free_meals_from = '0000-00-00';
            $eticket->free_meals_until = '0000-00-00';
            $eticket->second_meals_from = '0000-00-00';
            $eticket->second_meals_until = '0000-00-00';
            $eticket->third_meals_from = '0000-00-00';
            $eticket->third_meals_until = '0000-00-00';
            $eticket->kopgalds_from = '0000-00-00';
            $eticket->kopgalds_until = '0000-00-00';
            if (!$eticket->save()) {
                $this->error("Kļūda - nevaru saglabāt izmaiņas e-talonam: " . $eticket->id);
                Log::error("getfreemealpersondatadel - nevaru saglabāt izmaiņas e-talonam: " . $eticket->id);
            }
        }

        // same for eticketbase
        $cleanup = EticketsBase::whereNotIn("pk", $used_pks)
                ->where(function ($query) {
                    $query->where('free_meals_until', '!=', '0000-00-00');
                    $query->orWhere('second_meals_until', '!=', '0000-00-00');
                    $query->orWhere('third_meals_until', '!=', '0000-00-00');
                })
                ->get();

        foreach ($cleanup as $eticket) {
            $eticket->free_meals_from = '0000-00-00';
            $eticket->free_meals_until = '0000-00-00';
            $eticket->second_meals_from = '0000-00-00';
            $eticket->second_meals_until = '0000-00-00';
            $eticket->third_meals_from = '0000-00-00';
            $eticket->third_meals_until = '0000-00-00';
            $eticket->kopgalds_from = '0000-00-00';
            $eticket->kopgalds_until = '0000-00-00';
            $eticket->brivpusdienas = 0;
            if (!$eticket->save()) {
                $this->error("Kļūda - nevaru saglabāt izmaiņas e-talona bazes ierakstam: " . $eticket->id);
                Log::error("getfreemealpersondatadel - nevaru saglabāt izmaiņas e-talona bazes ierakstam: " . $eticket->id);
            }
        }
    }

    /**
     * @param Eticket $eticket
     * @param Objekts $objekts
     */
    protected function update_eticket_objects(Eticket $eticket, Objekts $objekts)
    {
        EticketsObjekti::where("eticket_id", $eticket->id)->delete();

        $eto = New EticketsObjekti;
        $eto->eticket_id = $eticket->id;
        $eto->object_id = $objekts->id;
        $eto->pirma = 1;
        $eto->save();

        // check for grouped objects
        $ogroup = GroupObjekts::where("object_id", $objekts->id)->first();
        $ogroup_id = isset($ogroup->ogroup_id) ? $ogroup->ogroup_id : 0;

        if ($ogroup_id > 0) {
            $other_objects = GroupObjekts::where("object_id", '!=', $objekts->id)
                    ->where("ogroup_id", $ogroup_id)
                    ->lists("object_id")
                    ->toArray();

            if (count($other_objects) > 0) {
                foreach ($other_objects as $other) {
                    $eto = New EticketsObjekti;
                    $eto->eticket_id = $eticket->id;
                    $eto->object_id = $other;
                    $eto->pirma = 0;
                    $eto->save();
                }
            }
        }
    }

}
