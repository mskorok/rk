<?php

namespace App\Console\Commands;

use App\Models\Objekts;
use App\Models\School;
use App\Models\Transaction;
use App\Models\Seller;
use App\Models\Mpos;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class sendFreeMealDaily extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sendfreedaily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Nosūtīt brīvpusdienu datus par vakardienu';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = time();
        
        //fix kopgalds transactions seller reg number
        $getAllKopgaldsTrans = DB::table('transactions')->select('id', 'mpos_id')->where([
                'freemeals_type' => 4,
                'seller_reg_nr' => '',
                'sent_to_api' => '0000-00-00',
            ])->get();
        $count = 0;
        $this->info("Recived " . count($getAllKopgaldsTrans) . " transactions\n");
        
        if (!empty($getAllKopgaldsTrans)) {
            $curentMposId = 0;
            $curentSellerRegNr = 0;
            $count = 0;
            $this->info("Recived " . count($curentSellerRegNr) . " transactions\n");
            foreach ($getAllKopgaldsTrans as $mposId) {
                if ($curentMposId != (int) $mposId->mpos_id) {
                    $curentMposId = $mposId->mpos_id;
                    $owner = Mpos::select('owner_id')->where('id', $curentMposId)->first();
                    if (empty($owner)) {
                        $this->info($mposId->id . ", ");
                    } else {
                        $getSellerId = Seller::select('reg_nr')->where('owner_id', $owner->owner_id)->first();
                        $curentSellerRegNr = $getSellerId->reg_nr;
                    }
                }

                $updateTrans = DB::table('transactions')->where('id', $mposId->id)->update(['seller_reg_nr' => $curentSellerRegNr, 'comments' => 'TICKET SCAN FIXED', 'from_account_id' => 2, 'dotation_id' => -1]);
                $count++;
            }
        } else {
            $this->info("Request is empty\n");
        }
        $this->info("Fixed transactions " . $count . "\n");


        // get current month unsent data
        list($all_used_transaction_ids, $xml_string) = $this->get_data();

        if (count($all_used_transaction_ids) > 0) {
            // API call
            $headers = array(
                "POST HTTP/1.1",
                "Accept-Encoding: gzip,deflate",
                'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/SendMeals"',
                "Content-length: " . strlen($xml_string),
                "Host: " . Config::get('services.rs.zzurl2'),
                "Connection: Keep-Alive",
                "User-Agent: skolas.rigaskarte.lv",
            );

            $url = Config::get('services.rs.zzurl');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_FAILONERROR, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 9900);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSLCERT, storage_path() . "/zz_cert.pem");
            curl_setopt($ch, CURLOPT_SSLCERTPASSWD, Config::get('services.rs.certkey'));
            $result = curl_exec($ch);

            if (curl_errno($ch)) {
                $this->error("Kļūda saņemot datus - " . curl_error($ch));
                Log::error("sendfreeday - CURL error - " . curl_error($ch));

                $email_text = "Kļūda nosūtot ikdienas datus - " . curl_error($ch);
                Mail::send('mail.report', ["info" => $email_text], function ($message) {
                    $message->to(Config::get('mail.adminemail'))->subject('Skolas.rigaskarte.lv - kļūda nosūtot ikdienas datus!');
                });

                return;
            }
            $info = curl_getinfo($ch);
            curl_close($ch);

            // LOG data to file
            $this->log_api_data("zz", "sendfreedaily", $xml_string . "\n" . $result, $info["url"], $info["total_time"]);

            if (strpos($result, "Error") > 1) {
                $this->error($result);

                $email_text = "Kļūda nosūtot ikdienas datus - <br />" . $result;
                Mail::send('mail.report', ["info" => $email_text], function ($message) {
                    $message->to(Config::get('mail.adminemail'))->subject('Skolas.rigaskarte.lv - kļūda nosūtot ikdienas datus!');
                });
            } else {
                // update sent dates for all used transactions
                $sodiena = new Carbon();
                $sod = $sodiena->toDateString();

                DB::beginTransaction();

                foreach ($all_used_transaction_ids as $t_id) {
                    DB::update('update transactions set sent_to_api = ? where id = ? LIMIT 1', [$sod, $t_id]);
                }

                DB::commit();

                $this->info($result);
                $this->info('Brīvpusdienu datu nosūtīšana sekmīgi pabeigta');
            }
        }

        // check if there are older unsent data
        $months_to_send = $this->get_older_months_to_send();
        
        if(empty($months_to_send)){
            $end = time();
            $this->info('Nav datu nosūtīšanai - (' . ($end - $start) . 's).');
            return ;
        }

        foreach ($months_to_send as $older_month) {
            // send older data, thats not been sent
            list($all_used_transaction_ids, $xml_string) = $this->get_data($older_month);

            if (count($all_used_transaction_ids) > 0) {
                // API call
                $headers = array(
                    "POST HTTP/1.1",
                    "Accept-Encoding: gzip,deflate",
                    'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/SendMeals"',
                    "Content-length: " . strlen($xml_string),
                    "Host: " . Config::get('services.rs.zzurl2'),
                    "Connection: Keep-Alive",
                    "User-Agent: skolas.rigaskarte.lv",
                );

                $url = Config::get('services.rs.zzurl');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_string);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_FAILONERROR, false);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 9900);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSLCERT, storage_path() . "/zz_cert.pem");
                curl_setopt($ch, CURLOPT_SSLCERTPASSWD, Config::get('services.rs.certkey'));
                $result = curl_exec($ch);

                if (curl_errno($ch)) {
                    $this->error("Kļūda saņemot datus - " . curl_error($ch));
                    Log::error("sendfreedayold - CURL error - " . curl_error($ch));

                    $email_text = "Kļūda nosūtot vecākus datus - " . curl_error($ch);
                    Mail::send('mail.report', ["info" => $email_text], function ($message) {
                        $message->to(Config::get('mail.adminemail'))->subject('Skolas.rigaskarte.lv - kļūda nosūtot vecākus datus!');
                    });

                    return;
                }
                $info = curl_getinfo($ch);
                curl_close($ch);

                // LOG data to file
                $this->log_api_data("zz", "sendfreedailyolder ($older_month)", $xml_string . "\n" . $result, $info["url"], $info["total_time"]);

                if (strpos($result, "Error") > 1) {
                    $this->error($result);

                    $email_text = "Kļūda nosūtot vecākus datus - <br />" . $result;
                    Mail::send('mail.report', ["info" => $email_text], function ($message) {
                        $message->to(Config::get('mail.adminemail'))->subject('Skolas.rigaskarte.lv - kļūda nosūtot vecākus datus!');
                    });
                } else {
                    // update sent dates for all used transactions
                    $sodiena = new Carbon();
                    $sod = $sodiena->toDateString();

                    DB::beginTransaction();

                    foreach ($all_used_transaction_ids as $t_id) {
                        DB::update('update transactions set sent_to_api = ? where id = ? LIMIT 1', [$sod, $t_id]);
                    }

                    DB::commit();

                    $this->info($result);
                    $this->info('Vecāku brīvpusdienu datu nosūtīšana (' . $older_month . ' sekmīgi pabeigta');
                }
            }
        }
    }

    /**
     * @param string $older
     * @return array
     */
    protected function get_data($older = '')
    {
        $objekti = Objekts::lists("object_regnr", "id")->toArray();

        $all_used_transaction_ids = [];

        $all_objects = [];
        foreach ($objekti as $object_id => $object_regnr) {
            // check if not already done
            if ($older != '') {
                $periods = $older;

                $rows = Transaction::with("frome")
                    ->where("object_id", $object_id)
                    ->Ok()
                    ->where("from_account_id", 2)
                    ->where("created_at", 'LIKE', $older . '-%')
                    ->where("dotation_id", '!=', 0)
                    ->where("seller_reg_nr", ">", 0)
                    ->where("sent_to_api", "0000-00-00")
                    ->get();
            } else {

                $sodiena = new Carbon();
                $periods = substr($sodiena->toDateString(), 0, 7);

                $rows = Transaction::with("frome")
                    ->where("object_id", $object_id)
                    ->Ok()
                    ->where("from_account_id", 2)
                    ->where("created_at", ">=", $periods . "-01")
                    ->where("dotation_id", '!=', 0)
                    ->where("seller_reg_nr", ">", 0)
                    ->where("sent_to_api", "0000-00-00")
                    ->get();
            }

            if ($rows->count() > 0) {
                foreach ($rows as $row) {
                    if (!is_null($row->frome)) {
                        $school_regnr = School::where("reg_nr", $row->frome->pirmais_objekts()->object_regnr)->lists("id")->toArray();
                        $all_objects[$school_regnr[0]][] = [
                            "pk" => $row->frome->pk,
                            "grade" => intval($row->frome->grade),
                            "id" => $row->id,
                            "date" => str_replace(" ", "T", $row->created_at) . ".0Z",
                            "regno" => $row->seller_reg_nr,
                            "cardid" => $row->from_eticket_nr,
                        ];
                    }
//                    else
//                    {
//                        $school_regnr = School::where("reg_nr", Objekts::select('object_regnr')->where('id', $row->object_id)->first()->object_regnr)->lists("id")->toArray();
//                        $all_objects[$school_regnr[0]][] = [
//                            "pk" => '999999-99999',
//                            "grade" => '99',
//                            "id" => $row->id,
//                            "date" => str_replace(" ", "T", $row->created_at) . ".0Z",
//                            "regno" => $row->seller_reg_nr,
//                            "cardid" => $row->from_eticket_nr,
//                        ];
//                    }

                    array_push($all_used_transaction_ids, $row->id);
                }
            }
        }

        $all_used_transaction_ids = array_unique($all_used_transaction_ids);

        // group child data in schools
        $final = [];
        foreach ($all_objects as $school_id => $school) {
            $school_data = [];
            foreach ($school as $row) {
                $pk = $row["pk"];
                $grade = $row["grade"];
                unset($row["pk"]);
                unset($row["grade"]);

                if (!isset($school_data[$pk])) {
                    $school_data[$pk] = [
                        "grade" => $grade,
                        "meals" => [$row],
                    ];
                } else {
                    array_push($school_data[$pk]["meals"], $row);
                }
            }

            $final[$school_id] = $school_data;
        }

        if (!isset($periods))
            $periods = date("Y-m");
        $xml_string = $this->format_xml_string($periods, $final);

        return array($all_used_transaction_ids, $xml_string);
    }

    /**
     * @param $periods
     * @param $final
     * @return string
     */
    protected function format_xml_string($periods, $final)
    {
        $xml_string = 
            '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
                <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                    <wsa:Action>http://tempuri.org/IMonitoringService/SendMeals</wsa:Action>
                </soap:Header>
                <soap:Body>
                    <tem:SendMeals xmlns="http://tempuri.org/">
                    <tem:request>
                    <zzd:Header>
                        <zzd1:From>skolas.rigaskarte.lv</zzd1:From>
                    </zzd:Header>
                    <zzd:Month>' . substr($periods, 0, 7) . '</zzd:Month>
                    <zzd:SchoolList>';

        foreach ($final as $school_id => $school_data) {
            $xml_string .= '<zzd:School>
                <zzd:Id>' . $school_id . '</zzd:Id>
                <zzd:ChildList>';

            foreach ($school_data as $pk => $person_data) {
                $xml_string .= '<zzd:Child>
                    <zzd:Id>' . $pk . '</zzd:Id>
                    <zzd:ClassGroup>' . $person_data["grade"] . '</zzd:ClassGroup>
                    <zzd:MealTimeList>';

                foreach ($person_data["meals"] as $meal) {
                    $xml_string .= '<zzd:MealTime>
                        <zzd:Id>' . $meal["id"] . '</zzd:Id>
                        <zzd:Date>' . $meal["date"] . '</zzd:Date>
                        <zzd:RegNo>' . $meal["regno"] . '</zzd:RegNo>
                        <zzd:CardId>' . $meal["cardid"] . '</zzd:CardId>
                    </zzd:MealTime>';
                }

                $xml_string .= '</zzd:MealTimeList>
                    </zzd:Child>';
            }

            $xml_string .= '</zzd:ChildList>
                </zzd:School>';
        }

        $xml_string .= '</zzd:SchoolList>
                    </tem:request>
                </tem:SendMeals>
            </soap:Body>
        </soap:Envelope>';
        return $xml_string;
    }

    private function get_older_months_to_send()
    {
        $sodiena = new Carbon();
        $periods = substr($sodiena->toDateString(), 0, 7);

        return Transaction::select(DB::raw('distinct(DATE_FORMAT(created_at,\'%Y-%m\')) as ym'))
            ->Ok()
            ->where("from_account_id", 2)
            ->where("created_at", ">=", "2017-01-01")
            ->where("created_at", "<", $periods . "-01")
            ->where("dotation_id", '!=', 0)
            ->where("seller_reg_nr", ">", 0)
            ->where("sent_to_api", "0000-00-00")
            ->pluck("ym")
            ->all();
    }

}
