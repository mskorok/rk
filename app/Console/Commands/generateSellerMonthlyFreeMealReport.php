<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Mpos;
use App\Models\Seller;
use App\Models\SellerPayments;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class generateSellerMonthlyFreeMealReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'genfreemonth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ģenerēt tirgotāju brīvpusdienu datus par pēdējo mēnesi';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $sodiena = new Carbon();
        $sodiena->subMonth();
        $periods = substr($sodiena->toDateString(), 0, 7);

        $sellers = Seller::with("owner")->get();

        foreach ($sellers as $key => $seller) {
            $seller_free_meals_account = Account::SellerFreeMeals()->O($seller->owner->id)->first();

            // check if not already done
            $exists = Transaction::where("to_account_id", 999996)
                ->Ok()
                ->where("from_account_id", $seller_free_meals_account->id)
                ->where("comments", "LIKE", '%' . $periods . "-FREE")
                ->where("confirmed", 1)
                ->count();

            if ($exists > 0)
                continue;

            $owner_id = $seller->owner_id;
            if ($owner_id > 0) {
                // all MPOS account IDS
                $mpos_account_ids = Mpos::Own($owner_id)->lists("account_id")->toArray();

                // count
                $count = Transaction::Ok()
                    ->where('from_account_id', '=', 2)
                    ->whereIn('to_account_id', $mpos_account_ids)
                    ->where("created_at", "LIKE", $periods . '%')
                    ->count();

                if ($count > 0) {
                    try {
                        DB::transaction(function () use ($seller_free_meals_account, $periods, $seller, $mpos_account_ids, $count) {
                            // from seller free meals account to outgoing transfer
                            $outputName = $periods . "-FREE";

                            // seller outgoing transaction
                            $t = New Transaction;
                            $t->amount = 0;
                            $t->operation_id = 1;
                            $t->from_account_id = $seller_free_meals_account->id;
                            $t->to_account_id = 999996;
                            $t->confirmed = 1;
                            $t->comments = $outputName;
                            $t->save();

                            // seller payment record
                            $sp = New SellerPayments;
                            $sp->amount = 0;
                            $sp->seller_id = $seller->id;
                            $sp->transaction_id = $t->id;
                            $sp->status = 1;
                            $sp->period_code = $periods . "-FREE";
                            $sp->save();
                        });
                    } catch (\Exception $e) {
                        $this->error('Neizdevās sagatavot pārskaitījumu!' . $e->getMessage());
                    }
                } else
                    $this->info('Tirgotājam ar ID ' . $seller->id . ' nav reģistrēti brīvpusdienu darījumi!');
            }
        }

        $this->info('Tirgotāju brīvpusdienu datu ģenerēšana sekmīgi pabeigta');
    }

}
