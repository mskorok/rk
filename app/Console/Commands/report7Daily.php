<?php

namespace App\Console\Commands;

use App\Models\Mpos;
use App\Models\Seller;
use App\Models\Transaction;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class report7Daily extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'report7';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ikdienas atskaite nr.7 - visi tirgotāji pa skolām';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $datetime = new DateTime('yesterday');
        $vakardiena = $datetime->format('Y-m-d');

        $table = [];
        $all_sellers = Seller::orderBy("seller_name")->get();

        try {
            foreach ($all_sellers as $seller) {
                $mposes = Mpos::where("owner_id", $seller->owner_id)->orderBy("id")->get();
                foreach ($mposes as $mpos) {
                    $daily_balance = Transaction::where("to_account_id", $mpos->account_id)
                        ->Ok()
                        ->where("from_account_id", '!=', 2)
                        ->where("created_at", 'LIKE', $vakardiena . '%')
                        ->sum("amount");

                    $table[] = [$seller->seller_name, $mpos->mpos_name, sprintf("%.2f", $daily_balance / 100), $vakardiena];
                }
            }

            Excel::create('DIENAS ATSKAITE DARĪJUMI TIRGOTĀJI ' . $vakardiena, function($excel) use ($vakardiena, $table) {
                $excel->setTitle('Skolas.rigaskarte.lv - DIENAS ATSKAITE DARĪJUMI TIRGOTĀJI ' . $vakardiena);
                $excel->setCreator('skolas.rigaskarte.lv')->setCompany('skolas.rigaskarte.lv');
                $excel->setDescription('Skolas.rigaskarte.lv - DIENAS ATSKAITE DARĪJUMI TIRGOTĀJI ' . $vakardiena);

                $excel->sheet('Atskaite', function($sheet) use($table) {
                    $sheet->setOrientation('portrait');
                    $sheet->getPageSetup()->setFitToPage(false);
                    $sheet->getDefaultRowDimension()->setRowHeight(-1);
                    $sheet->setWidth(array(
                        'A' => 30,
                        'B' => 30,
                        'C' => 15,
                        'D' => 12,
                    ));
                    $sheet->setHeight(0, 40);

                    $sheet->fromArray($table, null, 'A1', false, false);

                    $sheet->prependRow(['Tirgotājs', 'Skola', 'Dienas apgrozījums EUR', 'Datums']);

                    $sheet->freezeFirstRow();

                    $sheet->cells('A1:Z1', function($cells) {
                        $cells->setFontWeight('bold')->setBackground('#CCCCCC');
                    });
                    $sheet->getStyle('A1:Z1')->getAlignment()->setWrapText(true);
                    $sheet->getStyle('A2:B3999')->getAlignment()->setWrapText(true);

                    $sheet->setColumnFormat(array(
                        'C2:C3999' => '0.00'
                    ));
                });
            })->store('xlsx');

            //send email to RK
            Mail::send('mail.report', ["info" => "DIENAS ATSKAITE DARĪJUMI TIRGOTĀJI " . $vakardiena], function($message) use ($vakardiena) {
                $message->to(Config::get('mail.rsemail1'))->cc(Config::get('mail.rsemail2'))->subject('Skolas.rigaskarte.lv - DIENAS ATSKAITE DARĪJUMI TIRGOTĀJI ' . $vakardiena);
                $message->attach(storage_path() . "/exports/DIENAS ATSKAITE DARĪJUMI TIRGOTĀJI " . $vakardiena . ".xlsx");
            });

            $this->info('Atskaite veiksmīgi ģenerēta un nosūtīta.');
        } catch (\Exception $e) {
            $this->error('Kļūda apstrādājot datus - ' . $e->getMessage());
        }
    }

}
