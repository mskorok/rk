<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Mpos;
use App\Models\Seller;
use App\Models\SellerPayments;
use App\Models\Settings;
use App\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;

class generateExtraSellerPayments extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'genextra';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ģenerēt trūkstošās tirgotāju izmaksas';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $periods = '2017-02-2';
        $periods_no = '2017-02-11';
        $periods_lidz = '2017-02-21';
        //$out_periods_lidz = "2016-11-12";
        $sellers = Seller::with("owner")->get();

        foreach ($sellers as $key => $seller) {
            $seller_owner_account = Account::Sell()->O($seller->owner->id)->first();

            // check if not already transferred
            $exists = Transaction::where("to_account_id", 999999)
                ->where("from_account_id", $seller_owner_account->id)
                ->where("comments", "LIKE", '%' . $periods)
                ->count();

            if ($exists > 0)
                continue;

            // sum of remaining seller MPOS balances
            $owner_id = $seller->owner_id;
            $seller_acc = Account::O($owner_id)->Sell()->first();
            $total = 0;

            if ($owner_id > 0) {
                $mpos_amounts = [];

                // get MPOS devices and their accounts
                $mposes = Mpos::with('account')->Own($owner_id)->get();

                foreach ($mposes as $mpos) {
                    $total_paid_transactions_in = Transaction::where("to_account_id", $mpos->account_id)
                        ->Ok()
                        ->where("from_account_id", '!=', 2)
                        ->where("from_account_id", '>', 0)
                        ->where("amount", '>', 0)
                        ->where("created_at", ">=", $periods_no)
                        ->where("created_at", "<", $periods_lidz)
                        //->where('local_id', 'LIKE', '%-p')
                        ->sum("amount");

//                    if ($total_paid_transactions_in > 0)
//                    {
//                        $this->info($mpos->id);
//                        var_dump($total_paid_transactions_in);
//                    }

                    $total_paid_transactions_out = 0;
//                    $total_paid_transactions_out = Transaction::where("from_account_id",$mpos->account_id)
//                        ->Ok()
//                        ->where("to_account_id",$seller_acc->id)
//                        ->where("amount",'>',0)
//                        ->where("updated_at", ">=", $periods_no)
//                        ->where("updated_at", "<=", $out_periods_lidz)
//                        ->sum("amount");

                    $diff = $total_paid_transactions_in - $total_paid_transactions_out;

                    if ($mpos->account != NULL) {
                        $mpos_amounts[$mpos->id] = $diff;
                        $total = $total + $diff;
                    }
                }

                if ($total > 0) {
                    try {

                        // get seller account and transfer money from MPOSes
                        if ($seller_acc->id > 0) {
                            $this->info('Tirgotājam ar ID ' . $seller->id . ' jāizmaksā - ' . $total . '!');

                            DB::transaction(function () use ($seller_acc, $mpos_amounts, $periods, $periods_no, $periods_lidz, $seller, $total) {
                                foreach ($mpos_amounts as $mpos_id => $amount) {
                                    $mpos = Cache::rememberForever('mpos_' . $mpos_id, function() use ($mpos_id) {
                                        return Mpos::findOrFail($mpos_id);
                                    });

                                    if ($amount > 0) {
                                        $mpos_account = Account::findOrFail($mpos->account_id);
                                        if ($mpos_account->balance < $amount)
                                            throw new \Exception('MPOS account ' . $mpos->account_id . ' missing funds! Required: ' . $amount / 100 . ', exiting: ' . $mpos_account->balance / 100, 1);

                                        $t = New Transaction;
                                        $t->amount = $amount;
                                        $t->operation_id = 1;
                                        $t->mpos_id = $mpos_id;
                                        $t->from_account_id = $mpos->account_id;
                                        $t->to_account_id = $seller_acc->id;
                                        $t->confirmed = 1;
                                        $t->created_at = $periods_lidz . ' 01:01:01';
                                        $t->updated_at = $periods_lidz . ' 01:01:01';
                                        $t->save();

                                        $seller_acc->balance = $seller_acc->balance + $amount;
                                        $seller_acc->save();

                                        $mpos_account->balance = $mpos_account->balance - $amount;
                                        $mpos_account->save();
                                    }
                                }

                                // from seller account to bank transfer
                                $outputName = str_random(20) . '-' . $periods;

                                // seller transaction
                                $t = New Transaction;
                                $t->amount = $total;
                                $t->operation_id = 1;
                                $t->from_account_id = $seller_acc->id;
                                $t->to_account_id = 999999;
                                $t->confirmed = 0;
                                $t->comments = $outputName;
                                $t->created_at = $periods_lidz . ' 01:01:01';
                                $t->updated_at = $periods_lidz . ' 01:01:01';
                                $t->save();

                                $seller_acc->balance = $seller_acc->balance - $total;
                                $seller_acc->save();

                                // seller payment record
                                $sp = New SellerPayments;
                                $sp->amount = $total;
                                $sp->seller_id = $seller->id;
                                $sp->transaction_id = $t->id;
                                $sp->status = 1;
                                $sp->period_code = $periods;
                                $sp->save();

                                $payment_id = $sp->id;

                                // generate PDF
                                $data = Transaction::with('from_account')->where('id', $t->id)->Ok()->firstOrFail();

                                $settings = Settings::firstOrFail();

                                $datums_no = $periods_no;
                                $datums_lidz = $periods_lidz;

                                $html = 
                                '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
                                    <head profile="http://gmpg.org/xfn/11">
                                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                                        <style tyle="text/css">body { font-family: DejaVu Sans, sans-serif; }</style>
                                    </head>'.
                                    '<body>' .
                                        View::make('accountant/receipt', compact('data', 'seller', 'settings', 'datums_no', 'datums_lidz', 'payment_id')). 
                                    '</body>'.
                                '</html>';

                                if (!is_dir(public_path('assets/pdfs'))) {
                                    mkdir(public_path('assets/pdfs'), 0755, true);
                                }

                                $pdfPath = public_path('assets/pdfs') . '/' . $outputName . '.pdf';

                                $content = \PDF::loadHTML($html)->output();

                                File::put($pdfPath, $content);
                            });
                        } else
                            throw new \Exception('No seller account!', 1);
                    } catch (\Exception $e) {
                        $this->error('Neizdevās sagatavot pārskaitījumu!' . $e->getMessage());
                    }
                } else
                    $this->info('Tirgotājam ar ID ' . $seller->id . ' nav ko izmaksāt!');
            } else
                Throw New \Exception('no owner');
        }

        $this->info('Trūkstošo tirgotāju izmaksu ģenerēšana sekmīgi pabeigta');
    }

}
