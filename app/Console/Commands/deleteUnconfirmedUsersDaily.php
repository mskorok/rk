<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class deleteUnconfirmedUsersDaily extends Command
{

    protected $name = 'deleteunconfirmed';
    protected $description = 'Dzēst neapstiprinātās lietotāju reģistrācijas pēc 5 dienām';

    public function fire()
    {
        $datums = Carbon::now()->subDays(5);
        $datestring = $datums->format("Y-m-d");

        $unconfirmed_users = User::where("terms", 3)
            ->where("confirmed", 0)
            ->where("deleted_at", null)
            ->where("created_at", '<', $datestring . '%')
            ->get();

        $count = $unconfirmed_users->count();

        foreach ($unconfirmed_users as $user) {
            $user->delete();
        }

        // truncate daily log tables
        DB::table('daily_log_rk')->truncate();
        DB::table('daily_log_zz')->truncate();

        $this->info('Neapstiprināto lietotāju reģistrāciju dzēšana sekmīgi pabeigta - dzēsta(s) ' . $count . ' reģistrācija(s).');
    }

}
