<?php

namespace App\Console\Commands;

use App\Models\Seller;
use DateTime;

class generateSellerDailyBalance extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'genbalances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ģenerēt tirgotāju dienas beigu bilances';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $datetime = new DateTime('yesterday');
        $sodiena = $datetime->format('Y-m-d');

        $sellers = Seller::all();

        foreach ($sellers as $key => $seller) {
            $this->calculateSellerDaily($sodiena, $seller);
        }

        $this->info('Tirgotāju bilanču ģenerēšana sekmīgi pabeigta');
    }

}
