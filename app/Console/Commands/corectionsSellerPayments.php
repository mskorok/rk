<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Seller;
use App\Models\Mpos;
use App\Models\SellerBalances;
use App\Models\SellerPayments;
use App\Models\SellerPayoutId;
use App\Models\Settings;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;

class corectionsSellerPayments extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'corectionssellerpayments';
//    protected $signature = 'corectionssellerpayments {transID?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ģenerēt tirgotāju izmaksas 3x mēnesī';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    protected function disableOldYears()
    {
        $getAllOldPayments = SellerPayments::select('id')->where('created_at', '<', '2017-01-01 23:59:59')->where('complete', '=', 0)->get();
        if (empty($getAllOldPayments)) {
            return;
        }

        foreach ($getAllOldPayments as $payment) {
            $complete = SellerPayments::where(['id' => $payment->id])
                    ->update(['complete' => 1]);
        }
    }

    protected function transactions($mData)
    {
        $dates = [];
        switch (substr($mData->period_code, 8)) {
            case 1:
                $dates = ['from' => date("Y-m", strtotime($mData->period_code)) . "-01 00:00:00", 'until' => date("Y-m", strtotime($mData->period_code)) . "-10 23:59:59"];
                break;
            case 2:
                $dates = ['from' => date("Y-m", strtotime($mData->period_code)) . "-11 00:00:00", 'until' => date("Y-m", strtotime($mData->period_code)) . "-20 23:59:59"];
                break;
            case 3:
                $dates = ['from' => date("Y-m", strtotime($mData->period_code)) . "-21 00:00:00", 'until' => date("Y-m", strtotime($mData->period_code . " + 1 month")) . "-01 00:00:00"];
                break;
        }

        $ownerID = Seller::where('id', '=', $mData->seller_id)->first();
        if (empty($ownerID)) {
            $this->error('Nav tada tirgotaja ar ID ' . $mData->seller_id);
            die();
        }

        if (empty($dates)) {
            $this->error('Nekorets periods => ' . $dates['from'] . ' ; ' . $dates['until']);
            die();
        }
        $allTrans = DB::select(
            "SELECT tr.id,from_account_id, to_account_id, authorisator_id, from_eticket_nr, tr.object_id, 
                tr.amount, tr.created_at, tr.updated_at, mpos_name, m.account_id as mpos_acc_id, owner_name, m.owner_id
            FROM transactions as tr
                join mpos as m on m.id = tr.mpos_id
                join owners as ow on ow.id = m.owner_id
            where tr.created_at between '" . $dates['from'] . "' and '" . $dates['until'] . "'
                and from_account_id > 2
                and amount > 0
                and ow.id = " . $ownerID->owner_id . "
                and t_status != 'F'
                and from_eticket_nr is not null
            order by tr.updated_at desc"
        );

        $totalAount = 0;

        if (empty($allTrans)) {
            $this->error('Nav neviena ieraksta no DB pār periodu ' . $dates['from'] . '<--->' . $dates['until']);
//            die();  
            return 0;
        }

        foreach ($allTrans as $trans) {
            $totalAount += intval($trans->amount);
        }

        if ($totalAount == $mData->amount) {
            $checkRecord = SellerPayoutId::where(['payout_trans_id' => $mData->transaction_id, 'seller_id' => $mData->seller_id])->first();
            if (!empty($checkRecord)) {
                $this->error("Db eksiste ieraksti ar tadiem datiem --- tranzakcijas ID => " . $mData->transaction_id . " ; Tirgotaja ID " . $mData->seller_id);
                die();
            }

            foreach ($allTrans as $transID) {
                $insertIds = new SellerPayoutId();
                $insertIds->trans_id = $transID->id;
                $insertIds->payout_trans_id = $mData->transaction_id;
                $insertIds->seller_id = $mData->seller_id;
                try {
                    if ($insertIds->save()) {
                        $updatePaymnets = SellerPayments::where(['seller_id' => $mData->seller_id, 'transaction_id' => $mData->transaction_id])
                            ->update(['complete' => 1]);
                    }
                } catch (Exception $ex) {
                    $this->error('Nevar saglabat transzakcijas DB');
                    $this->error($ex);
                    die();
                }
            }
        } else {
            $this->warn("\tSummas nesakrit, javeic izmaksu korekciju");
            return (['status' => 1, 'data' => $allTrans, 'result_sum' => $totalAount, 'dates' => $dates]);
        }
    }

    public function fire()
    {
        $this->info('Notiek Izmaksu izolacija par 2016 gadu.');

        #disable all 2016 year
        $this->disableOldYears();

        $this->info('Uzsakta tirgotaju izmaksas parbaude.');

        $allpayOuts = SellerPayments::where('complete', '=', 0)
            ->join('sellers', 'sellers.id', '=', 'seller_payments.seller_id')
            ->where('seller_payments.created_at', '>', '2016-01-02')
            ->where('period_code', 'not like', '%FREE')
            ->get();
        if (empty($allpayOuts)) {
            $this->info('Visas saģenerētas izmaksas atbilst ienakošam tranzakcijām.' . PHP_EOL);
            exit("Izmaksu korekcija pabeigta, neveicot izmaiņas DB.");
        }

        foreach ($allpayOuts as $payment) {
            $this->info("\tNotiek tirgotaja parbaude => " . $payment->seller_name . " periods pār " . $payment->period_code);
            $result = $this->transactions($payment);
//            dd($result);
            if ($result['status'] != 1) {
                continue;
            }

            $checkSum = 0;
            $transIds = [];
//            dd($result['data']);
            foreach ($result['data'] as $key => $data) {
                if (date("Y-m-d", strtotime($data->created_at)) == date("Y-m-d", strtotime($data->updated_at))) {
                    continue;
                }
                $checkSum += $data->amount;
                array_push($transIds, ['id' => $data->id, 'owner_id' => $data->owner_id, 'amount' => $data->amount, 'to_mpos_id' => $data->to_account_id]);
            }

            if (empty($transIds)) {
                $this->error("Netiek sameklēti nepareizas tranzakcijas\nJāveic manualu izpeti datiem " . $payment);
                continue;
            }

            $outputName = str_random(20) . '-' . date("Y-m-d", strtotime($result['dates']['from'])) . ' līdz ' . date("Y-m-d", strtotime($result['dates']['until']));
            $this->info($outputName);
//            dd($transIds);
            DB::transaction(function () use ($checkSum, $payment, $transIds, $outputName, $result) {
                $totalBalance = 0;
                foreach ($transIds as $id) {
                    $getMposBalance = Account::where('id', $id['to_mpos_id'])->first();
                    if (intval($id['amount']) > intval($getMposBalance->balance)) {
                        $this->error("Nepietiek naudas uz MPOS konta " . intval($id['amount']) . ' <-----> ' . intval($getMposBalance->balance));
                        die();
                    }

                    $newBalnace = intval($getMposBalance->balance) - intval($id['amount']);
                    $decreaseBalance = Account::where('id', '=', $getMposBalance->id)
                            ->update(['balance' => $newBalnace]);
                    $totalBalance = intval($id['amount']);
                }

                //add new payuot transaction

                $newPayOutTrans = New Transaction;
                $newPayOutTrans->amount = $totalBalance;
                $newPayOutTrans->operation_id = 1;
                $newPayOutTrans->from_account_id = Account::select('id')->where('owner_id', '=', $payment->owner_id)->first()->id;
                $newPayOutTrans->to_account_id = 999999;
                $newPayOutTrans->confirmed = 0;
                $newPayOutTrans->comments = $outputName;
                $newPayOutTrans->save();
                $gettransId = $newPayOutTrans->id;

                //add new seller paymnet record
                $newPayment = new SellerPayments();
                $newPayment->seller_id = $payment->seller_id;
                $newPayment->period_code = $payment->period_code . "-FIX";
                $newPayment->status = 1;
                $newPayment->complete = 1;
                $newPayment->transaction_id = $gettransId;
                $newPayment->amount = $checkSum;
                $newPayment->save();
                $getPaymentID = $newPayment->id;

//                $mposBalance = (intval($getMposBalance->balance) - intval($checkSum));
//                $setBalance = Account::where('id', '=', $transIds[0]['id'])->update(['balance' => $mposBalance]);
                //add new recodrs with ids
                $newIds = new SellerPayoutId();
                $newIds->trans_id = $gettransId;
                $newIds->payout_trans_id = $getPaymentID;
                $newIds->seller_id = $payment->seller_id;
                $newIds->save();

                //update fixed Payment
                $updateFixPayment = SellerPayments::where(['transaction_id' => $payment->transaction_id, 'seller_id' => $payment->seller_id])
                        ->update(['complete' => 1]);

                // generate PDF
                $data = Transaction::with('from_account')->where('id', $gettransId)->Ok()->firstOrFail();

                $settings = Settings::firstOrFail();

                $datums_no = date("Y-m-d", strtotime($result['dates']['from']));
                $datums_lidz = date("Y-m-d", strtotime($result['dates']['until']));
                $seller = Seller::where("owner_id", "=", $payment->owner_id)->first();
                $payment_id = $newPayment->id;

                $html = 
                '<html xmlns="http://www.w3.org/1999/xhtml" lang="lv-LV">
                    <head profile="http://gmpg.org/xfn/11">
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <style tyle="text/css">
                            body { font-family: DejaVu Sans, sans-serif; }
                        </style>
                    </head>
                    <body>'.
                        View::make('accountant/receipt', compact('data', 'seller', 'settings', 'datums_no', 'datums_lidz', 'payment_id')).
                    '</body>'.
                '</html>';

                if (!is_dir(public_path('assets/pdfs'))) {
                    mkdir(public_path('assets/pdfs'), 0755, true);
                }

                $pdfPath = public_path('assets/pdfs') . '/' . $outputName . '.pdf';

                $content = \PDF::loadHTML($html)->output();

                File::put($pdfPath, $content);
            });
        }
        $this->info('Izmaksu korekcija pabeigta veiksmigi');
    }

}
