<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Mpos;
use App\Models\Seller;
use App\Models\Transaction;

class regen extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'regen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'pargeneracija';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $datumi = $this->getDatesFromRange("2017-02-02", "2017-02-25");
        $sellers = Seller::all();

        foreach ($datumi as $datums) {
            foreach ($sellers as $seller) {
                $this->info("Aprekinam SELLERAM $seller->id, uz $datums");
                $this->calculateSellerDaily($datums, $seller);
            }
        }

        foreach ($sellers as $seller) {
            $this->info("Aprekinam izmaksu korekciju SELLERIM $seller->id");
            $owner_id = $seller->owner_id;
            $seller_acc = Account::O($owner_id)->Sell()->first();

            $mposs = Mpos::with('account')->Own($owner_id)->get();

            foreach ($mposs as $mpos) {
                $total_paid_transactions_in = Transaction::where("to_account_id", $mpos->account_id)
                    ->Ok()
                    ->where("from_account_id", '!=', 2)
                    ->where("from_account_id", '>', 0)
                    ->where("amount", '>', 0)
                    ->sum("amount");

                $total_paid_transactions_out = Transaction::where("from_account_id", $mpos->account_id)
                    ->Ok()
                    ->where("to_account_id", $seller_acc->id)
                    ->where("amount", '>', 0)
                    ->sum("amount");

                $balance = $total_paid_transactions_in - $total_paid_transactions_out;

                $mpos_account = Account::findOrFail($mpos->account_id);
                $mpos_account->balance = $balance;
                $mpos_account->save();
            }
        }

        $this->info('OK');
    }

}
