<?php

namespace App\Console\Commands;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

use App\Models\Eticket;
use App\Models\Mpos;
use App\Models\Seller;
use App\Models\Transaction;
use DateTime;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class updateTransactionsDaily extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'updatetrans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Papildināt vakardienas transakcijas ar tirgotāju reģistrācijas numuriem un brīvpusdienu tipiem';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $datetime = new DateTime('yesterday');
        $datums = $datetime->format('Y-m-d');

        $all_transactions = Transaction::Ok()
            ->where("dotation_id", ">", 0)
            ->where("seller_reg_nr", "")
            ->where("created_at", 'LIKE', $datums . '%')
            ->get();

        foreach ($all_transactions as $t) {
            $this->process_transaction($t, $datums);
        }

        $this->info('Vakardienas transakciju informācijas atjaunošana sekmīgi pabeigta');

        $all_transactions = Transaction::Ok()
            ->where("dotation_id", ">", 0)
            ->where("seller_reg_nr", "")
            ->get();

        foreach ($all_transactions as $t) {
            $this->process_transaction($t, $datums);
        }

        $this->info('Vecāku transakciju informācijas atjaunošana sekmīgi pabeigta');
    }

    /**
     * @param $mpos_id
     * @return mixed
     */
    private function get_transaction_regnr($mpos_id)
    {
        $mpos = Cache::remember('mpos_' . $mpos_id, 120, function() use ($mpos_id) {
            return Mpos::findOrFail($mpos_id);
        });

        $owner_id = $mpos->owner_id;

        $seller = Cache::remember('seller_byowner_' . $owner_id, 120, function() use ($owner_id) {
            return Seller::where("owner_id", $owner_id)->get()->first();
        });

        return $seller->reg_nr;
    }

    /**
     * @param $eticket
     * @param $datums
     * @return int
     */
    private function get_freemeal_type($eticket, $datums)
    {
        $freemeals_type = 99;

        // use one of valid intervals by priority
        if (strtotime($eticket->third_meals_from) <= strtotime($datums) && strtotime($eticket->third_meals_until) > strtotime($datums)) {
            $freemeals_type = 3;
        }

        if (strtotime($eticket->kopgalds_from) <= strtotime($datums) && strtotime($eticket->kopgalds_until) > strtotime($datums)) {
            $freemeals_type = 4;
        }

        if (strtotime($eticket->second_meals_from) <= strtotime($datums) && strtotime($eticket->second_meals_until) > strtotime($datums)) {
            $freemeals_type = 2;
        }

        if (strtotime($eticket->free_meals_from) <= strtotime($datums) && strtotime($eticket->free_meals_until) > strtotime($datums)) {
            $freemeals_type = 1;
        }
        return $freemeals_type;
    }

    /**
     * @param $t
     * @param $datums
     * @return bool
     */
    private function process_transaction($t, $datums)
    {
        try {
            $reg_nr = $this->get_transaction_regnr($t->mpos_id);
            $eticket = Eticket::where("nr", $t->from_eticket_nr)->first();
            $freemeals_type = $this->get_freemeal_type($eticket, $datums);
        } catch (Exception $e) {
            $this->error("Kļūda atlasot transakcijas regnr datus - " . json_encode($e->getMessage()));
            Log::error("updatetransactionsdaily - cant get regnr - " . json_encode($e->getTrace()));

            $freemeals_type = 99;
            $reg_nr = '';
        }

        if ($freemeals_type == 99)
            $this->error("Kļūda atlasot brīvpusdienu veidu intervālu!");

        $updated = DB::update('update transactions set seller_reg_nr = ?, freemeals_type =? where id = ? LIMIT 1', [$reg_nr, $freemeals_type, $t->id]);
        if ($updated <= 0) {
            $this->error("Kļūda saglabājot transakcijas regnr datus - " . json_encode($t->id));
            Log::error("updatetransactionsdaily - cant save regnr - " . json_encode($t->id));
        }

        return true;
    }

}
