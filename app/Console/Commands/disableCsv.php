<?php

namespace App\Console\Commands;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

use App\Http\Controllers\RollingCurlX;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\Objekts;
use App\Models\School;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class disableCsv extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'disablecsv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deaktivizēt trešo statusu no CSV improtētajiem, kas nav atnākuši no ZZ';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // total start timer
        $start = time();

        $xml = 
        '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsa:Action>http://tempuri.org/IMonitoringService/GetPersonsWithLowIncome</wsa:Action>
            </soap:Header>
            <soap:Body>
                <GetPersonsWithLowIncome xmlns="http://tempuri.org/">
                    <periodType></periodType>
                </GetPersonsWithLowIncome>
            </soap:Body>
        </soap:Envelope>';

        $headers = array(
            "POST HTTP/1.1",
            "Accept-Encoding: gzip,deflate",
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/GetPersonsWithLowIncome"',
            "Content-length: " . strlen($xml),
            "Host: " . Config::get('services.rs.zzurl2'),
            "Connection: Keep-Alive",
            "User-Agent: skolas.rigaskarte.lv",
        );

        $url = Config::get('services.rs.zzurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 990);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLCERT, storage_path() . "/zz_cert.pem");
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, Config::get('services.rs.certkey'));
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $this->error("Kļūda saņemot lowincome datus - " . curl_error($ch));
            Log::error("getlowincome - CURL error - " . curl_error($ch));
            return;
        }
        curl_close($ch);

        // process data
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $result);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string = str_replace('<s:', '<', $xml_string);
        $xml_string = str_replace('<a:', '<', $xml_string);
        $xml_string = str_replace('<b:', '<', $xml_string);

        $res = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
        $result2 = $res["Body"]["GetPersonsWithLowIncomeResponse"]["GetPersonsWithLowIncomeResult"]["Changes"]["PersonData"];

        $used_pks = [];

        $this->info("No ZZ saņemti PK : " . count($result2));

        foreach ($result2 as $person_row) {
            $pk = $person_row["PersonId"];
            $used_pks[] = $pk;
        }

        $csv_etickets = Eticket::whereNotIn("pk", $used_pks)->where("third_meals_until", "2017-06-01")->get();

        $this->info("DB esoši deaktivizējami PK : " . $csv_etickets->count());

        foreach ($csv_etickets as $eticket) {
            $eticket->third_meals_from = '0000-00-00';
            $eticket->third_meals_until = '0000-00-00';
            if (!$eticket->save()) {
                $this->error("Kļūda saglabājot eticket datus - " . json_encode($eticket->getErrors()));
                Log::error("disablecsv, eticket - DB save error - " . json_encode($eticket->getErrors()));
            }

            $base = EticketsBase::where("pk", $eticket->pk)->first();

            if (isset($base->id)) {
                $base->custom = 0;
                $base->third_meals_from = '0000-00-00';
                $base->third_meals_until = '0000-00-00';
                $base->brivpusdienas = 0;
                if (!$base->save()) {
                    $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                    Log::error("disablecsv, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                }
            }
        }

        // total time
        $end = time();

        $this->info('Nederigo CSV ierakstu deaktivizēšana sekmīgi pabeigta: (' . ($end - $start) . ')');
    }

}
