<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\DailyLogRk;
use App\Models\DailyLogZz;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\Mpos;
use App\Models\Objekts;
use App\Models\School;
use App\Models\Seller;
use App\Models\SellerBalances;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class BaseCommand extends Command
{

    protected $name = 'base';
    protected $pk_etickets = [];

    public function __construct()
    {
        $this->pk_etickets = [];

        parent::__construct();
    }

    protected function getDatesFromRange($date_time_from, $date_time_to)
    {
        // cut hours, because not getting last day when hours of time to is less than hours of time_from
        // see while loop
        $start = Carbon::createFromFormat('Y-m-d', substr($date_time_from, 0, 10));
        $end = Carbon::createFromFormat('Y-m-d', substr($date_time_to, 0, 10));

        $dates = [];

        while ($start->lte($end)) {

            $dates[] = $start->copy()->format('Y-m-d');

            $start->addDay();
        }

        return $dates;
    }

    protected function calculateSellerDaily($sodiena, Seller $seller)
    {
        // seller balance
        // sum of mpos balances
        $owner_id = $seller->owner_id;

        if ($owner_id > 0) {
            $total = 0;
            $mpos_amounts = [];
            $total_paid_day = 0;
            $total_paid_day_out = 0;

            $seller_free_meals_account = Account::SellerFreeMeals()->O($owner_id)->firstOrFail();

            // get MPOS devices and their accounts
            $mpos = Mpos::with('account')->Own($owner_id)->get();
            foreach ($mpos as $device) {
                if ($device->account != NULL) {
                    $total_paid_from_transactions_in = Transaction::where("to_account_id", $device->account_id)
                        ->Ok()
                        ->where("from_account_id", '!=', 2)
                        ->where("created_at", 'LIKE', $sodiena . '%')
                        ->sum("amount");

                    $total_paid_from_transactions_out = Transaction::where("from_account_id", $device->account_id)
                        ->Ok()
                        ->where("to_account_id", '!=', $device->account_id)
                        ->where("to_account_id", '!=', $seller_free_meals_account->id)
                        ->where("created_at", 'LIKE', $sodiena . '%')
                        ->sum("amount");

                    $total_paid_day = $total_paid_day + $total_paid_from_transactions_in;

                    $total_paid_day_out = $total_paid_day_out + $total_paid_from_transactions_out;

                    // calculate array with individual MPOS payment sums for that date
                    $mpos_amounts[] = [
                        "mpos_id" => $device->id,
                        "account_id" => $device->account->id,
                        "amount_paid" => $total_paid_from_transactions_in,
                        "paid_out" => $total_paid_from_transactions_out,
                    ];

                    $total = $total + $total_paid_from_transactions_in - $total_paid_from_transactions_out;
                }
            }

            // saglabajam DB, parrakstam ja jau eksiste
            try {
                $previous_day = SellerBalances::where('seller_id', $seller->id)
                    ->where('datums', '<', $sodiena)
                    ->orderBy("datums", "DESC")
                    ->first();

                if (isset($previous_day->id)) {
                    $prev_paid = $previous_day->balance_paid;
                    $total = $previous_day->balance + $total;
                } else
                    $prev_paid = 0;

                $balance_paid = $prev_paid + $total_paid_day - $total_paid_day_out;

                $daily = SellerBalances::where('seller_id', $seller->id)
                    ->where('datums', $sodiena)
                    ->first();

                DB::transaction(function() use($seller, $total, $sodiena, $daily, $mpos_amounts, $total_paid_day, $balance_paid, $total_paid_day_out) {
                    if (isset($daily->id)) {
                        $daily->balance = $total;
                        $daily->balance_paid = $balance_paid;
                        $daily->daily_paid = $total_paid_day;
                        $daily->paid_out = $total_paid_day_out;
                        $daily->mpos_account_amounts = json_encode($mpos_amounts);
                        $daily->save();
                    } else {
                        $daily = New SellerBalances();
                        $daily->seller_id = $seller->id;
                        $daily->datums = $sodiena;
                        $daily->balance = $total;
                        $daily->balance_paid = $balance_paid;
                        $daily->daily_paid = $total_paid_day;
                        $daily->paid_out = $total_paid_day_out;
                        $daily->mpos_account_amounts = json_encode($mpos_amounts);
                        $daily->save();
                    }
                });
            } catch (\Exception $e) {
                $this->error('Kļūda ievietojot DB (tirgotajs:' . $seller->id . ') -' . $e->getMessage());
            }
        } else
            Throw New \Exception('no owner');
    }

    /**
     * @param $api
     * @param $method
     * @param $data
     * @param $curl_url
     * @param $curl_time
     */
    protected function log_api_data($api, $method = "", $data = "", $curl_url = "", $curl_time = "")
    {
        $d1 = date("Y-m-d");
        $d = date("Y-m-d H:i:s");

        $curl = ($curl_time != "") ? $d . " - " . $curl_url . " CURL time in seconds=" . $curl_time . "\n" : "";

        File::append(storage_path() . "/" . $d1 . "_" . $api . "_log.txt", $d . " - " . $method . "\n" . $data . "\n" . $curl);
    }

    /**
     * @param $eticket_row
     * @param $person_data
     * @return array
     */
    protected function process_single_eticket_rk_response($eticket_row, $person_data)
    {
        // log in DB
        $this->log_db_rk($eticket_row);

        if (strtotime(substr($eticket_row["ExpirationDate"], 0, 10)) > time()) {
            if (!isset($this->pk_etickets[$eticket_row["PersonCode"]])) {
                $this->pk_etickets[$eticket_row["PersonCode"]] = [];
            }
            array_push($this->pk_etickets[$eticket_row["PersonCode"]], $eticket_row["SerialNumber"]);

            // insert in etickets those e-tickets which does not exist
            $exists = Eticket::where("nr", $eticket_row["SerialNumber"])->first();
            if (!isset($exists->id)) {
                if (!isset($person_data[$eticket_row["PersonCode"]]["School"]["SchoolId"])) {
                    $this->error("Kļūda - personai nav skolas ID " . $eticket_row["PersonCode"]);
                    Log::error("getfreemealpersondata - personai nav skolas ID. " . $eticket_row["PersonCode"]);
                    return;
                }

                $school = School::find($person_data[$eticket_row["PersonCode"]]["School"]["SchoolId"]);

                if (isset($school->reg_nr)) {
                    $objekts = Objekts::where("object_regnr", $school->reg_nr)->first();

                    if (!isset($objekts->id)) {
                        $this->error("Kļūda - nevaru atrast objektu ar reg. nr. " . $school->reg_nr);
                        Log::error("getfreemealpersondata - nevaru atrast objektu ar reg. nr. " . $school->reg_nr);
                    } else {

                        $mas = isset($person_data[$eticket_row["PersonCode"]]["FreeMeals"]["FreeMealPeriod"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["FreeMeals"]["FreeMealPeriod"]) : ['0000-00-00', '0000-00-00'];
                        $mas2 = isset($person_data[$eticket_row["PersonCode"]]["UnusedMeals"]["UnusedMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["UnusedMeals"]["UnusedMeal"]) : ['0000-00-00', '0000-00-00'];
                        $mas3 = isset($person_data[$eticket_row["PersonCode"]]["RdMeals"]["RdMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["RdMeals"]["RdMeal"]) : ['0000-00-00', '0000-00-00'];
                        $mas4 = isset($person_data[$eticket_row["PersonCode"]]["TableRdMeals"]["TableRdMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["TableRdMeals"]["TableRdMeal"]) : ['0000-00-00', '0000-00-00'];

                        App::make('App\Http\Controllers\etickets\EticketsController')->save_new_eticket(
                            $eticket_row["SerialNumber"],
                            $eticket_row["PersonCode"],
                            $person_data[$eticket_row["PersonCode"]]["PersonName"] . ' ' . $person_data[$eticket_row["PersonCode"]]["PersonSurname"],
                            "0000", 
                            $person_data[$eticket_row["PersonCode"]]["School"]["SchoolClass"], 
                            0, 
                            2,
                            $objekts->id,
                            $person_data[$eticket_row["PersonCode"]]["School"]["SchoolId"], 
                            $mas, 
                            $mas2, 
                            $mas3, 
                            $mas4
                        );
                    }
                }
            }
        }

        return;
    }

    /**
     * @param $person_data
     * @return mixed
     */
    protected function process_all_persons_to_base_table($person_data)
    {
        foreach ($this->pk_etickets as $current_pk => $current_etickets) {
            $etickets_string = implode(",", $current_etickets);

            // add or update DB, etickets base
            if ($etickets_string !== '' && isset($person_data[$current_pk])) {
                $base = EticketsBase::where("pk", $current_pk)->first();

                $free = isset($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]) ? $this->find_current_periods($person_data[$current_pk]["FreeMeals"]["FreeMealPeriod"]) : ['0000-00-00', '0000-00-00'];
                $unused = isset($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]) ? $this->find_current_periods($person_data[$current_pk]["UnusedMeals"]["UnusedMeal"]) : ['0000-00-00', '0000-00-00'];
                $rd = isset($person_data[$current_pk]["RdMeals"]["RdMeal"]) ? $this->find_current_periods($person_data[$current_pk]["RdMeals"]["RdMeal"]) : ['0000-00-00', '0000-00-00'];
                $kopgalds = isset($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]) ? $this->find_current_periods($person_data[$current_pk]["TableRdMeals"]["TableRdMeal"]) : ['0000-00-00', '0000-00-00'];

                if (isset($base->id)) {
                    if ($etickets_string == $base->etickets)
                        continue;

                    $base->etickets = $etickets_string;
                    $base->custom = 0;
                    $base->name = $person_data[$current_pk]["PersonName"];
                    $base->surname = $person_data[$current_pk]["PersonSurname"];
                    $base->grade = $person_data[$current_pk]["School"]["SchoolClass"];
                    $base->school_id = $person_data[$current_pk]["School"]["SchoolId"];
                    $base->free_meals_from = $free[0];
                    $base->free_meals_until = $free[1];
                    $base->second_meals_from = $unused[0];
                    $base->second_meals_until = $unused[1];
                    $base->third_meals_from = $rd[0];
                    $base->third_meals_until = $rd[1];
                    $base->kopgalds_from = $kopgalds[0];
                    $base->kopgalds_until = $kopgalds[1];
                    $base->brivpusdienas = ($base->free_meals_until != '0000-00-00' || $base->second_meals_until != '0000-00-00' || $base->third_meals_until != '0000-00-00') ? 1 : 0;

                    if (!$base->save()) {
                        $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                        Log::error("dailymonitoring, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                    }
                } else {

                    if (!isset($person_data[$current_pk]["School"]["SchoolClass"])) {
                        $this->error("Kļūda - personai nav klases " . $current_pk);
                        Log::error("getfreemealpersondata - personai nav klases. " . $current_pk);

                        continue;
                    }

                    $base = New EticketsBase();
                    $base->pk = $current_pk;
                    $base->etickets = $etickets_string;
                    $base->custom = 0;
                    $base->name = $person_data[$current_pk]["PersonName"];
                    $base->surname = $person_data[$current_pk]["PersonSurname"];
                    $base->grade = $person_data[$current_pk]["School"]["SchoolClass"];
                    $base->school_id = $person_data[$current_pk]["School"]["SchoolId"];
                    $base->free_meals_from = $free[0];
                    $base->free_meals_until = $free[1];
                    $base->second_meals_from = $unused[0];
                    $base->second_meals_until = $unused[1];
                    $base->third_meals_from = $rd[0];
                    $base->third_meals_until = $rd[1];
                    $base->kopgalds_from = $kopgalds[0];
                    $base->kopgalds_until = $kopgalds[1];
                    $base->brivpusdienas = ($base->free_meals_until != '0000-00-00' || $base->second_meals_until != '0000-00-00' || $base->third_meals_until != '0000-00-00') ? 1 : 0;

                    if (!$base->save()) {
                        $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                        Log::error("dailymonitoring, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                    }
                }
            }
        }

        return;
    }

    protected function log_db_zz($row)
    {
        $free = isset($row["FreeMeals"]["FreeMealPeriod"]) ? $this->find_current_periods($row["FreeMeals"]["FreeMealPeriod"]) : ['0000-00-00', '0000-00-00'];
        $unused = isset($row["UnusedMeals"]["UnusedMeal"]) ? $this->find_current_periods($row["UnusedMeals"]["UnusedMeal"]) : ['0000-00-00', '0000-00-00'];
        $rd = isset($row["RdMeals"]["RdMeal"]) ? $this->find_current_periods($row["RdMeals"]["RdMeal"]) : ['0000-00-00', '0000-00-00'];

        $kopgalds = isset($row["TableRdMeals"]["TableRdMeal"]) ? $this->find_current_periods($row["TableRdMeals"]["TableRdMeal"]) : ['0000-00-00', '0000-00-00'];

        DailyLogZz::updateOrCreate(["pk" => $row["PersonId"]], [
            "datums" => date("Y-m-d"),
            "data" => json_encode($row),
            "name" => $row["PersonName"] . ' ' . $row["PersonSurname"],
            "age" => $row["Age"],
            "school_id" => (isset($row["School"]["SchoolId"])) ? intval($row["School"]["SchoolId"]) : 0,
            "grade" => isset($row["School"]["SchoolClass"]) ? (is_array($row["School"]["SchoolClass"])) ? "" : $row["School"]["SchoolClass"] : '',
            "riga" => filter_var($row["DeclaredRiga"], FILTER_VALIDATE_BOOLEAN),
            "alive" => filter_var($row["IsAlive"], FILTER_VALIDATE_BOOLEAN),
            "free_meals_from" => $free[0],
            "free_meals_until" => $free[1],
            "second_meals_from" => $unused[0],
            "second_meals_until" => $unused[1],
            "third_meals_from" => $rd[0],
            "third_meals_until" => $rd[1],
            "kopgalds_from" => $kopgalds[0],
            "kopgalds_until" => $kopgalds[1],
        ]);
    }

    protected function log_db_rk($eticket_row)
    {
        DailyLogRk::updateOrCreate(["pk" => $eticket_row["PersonCode"]], [
            "datums" => date("Y-m-d"),
            "data" => json_encode($eticket_row),
            "nr" => $eticket_row["SerialNumber"],
            "expires" => substr($eticket_row["ExpirationDate"], 0, 10),
            "blacklisted" => filter_var($eticket_row["Blacklisted"], FILTER_VALIDATE_BOOLEAN),
        ]);
    }

    /**
     * @param $r
     * @return array
     */
    public function find_current_periods($r)
    {
        // single interval
        if (isset($r["DateFrom"])) {
            return [substr($r["DateFrom"], 0, 10), substr($r["DateTill"], 0, 10)];
        } else { // multiple intervals or empty
            if (count($r) > 0) {
                $today = strtotime(date("Y-m-d"));
                $tomorrow = $today + 60 * 60 * 24;

                $holding = [];

                foreach ($r as $row) {
                    // missing fields
                    if (!isset($row["DateFrom"]) || !isset($row["DateTill"])) {
                        return ['0000-00-00', '0000-00-00'];
                    }

                    // period not started yet
                    if (strtotime(substr($row["DateTill"], 0, 10)) < $today) { // period has ended
                        continue;
                    } else if (strtotime(substr($row["DateFrom"], 0, 10)) <= $today AND strtotime(substr($row["DateTill"], 0, 10)) >= $today) { // valid period
                        return [substr($row["DateFrom"], 0, 10), substr($row["DateTill"], 0, 10)];
                    } else if (strtotime(substr($row["DateFrom"], 0, 10)) > $tomorrow) { // future period
                        $holding = [substr($row["DateFrom"], 0, 10), substr($row["DateTill"], 0, 10)];
                    }
                }

                // return future period, if none active found
                if (count($holding) == 2) {
                    return $holding;
                }
            } else
                return ['0000-00-00', '0000-00-00'];
        }

        return ['0000-00-00', '0000-00-00'];
    }

}
