<?php

namespace App\Console\Commands;

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

use App\Models\DailyLogRk;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\Objekts;
use App\Models\School;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class updateAllEtickets extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'updatealletickets';
    protected $pk_etickets;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atjaunot informāciju no RK API par visiem aktīvajiem e-taloniem';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // total start timer
        $start = time();

        // all active etickets
        $persons = Eticket::with("getObjekti")
            ->A()
            ->where("free_meals_until", '!=', "0000-00-00")
            ->orWhere("second_meals_until", '!=', "0000-00-00")
            ->orWhere("third_meals_until", '!=', "0000-00-00")
            ->orWhere("kopgalds_until", '!=', "0000-00-00")
            ->groupBy('pk')
            ->get()->toArray();
        $this->info("Tiks pārbaudītī personas kodi: " . count($persons));

        if (count($persons) > 0) {
            // all schools
            $all_schools = School::lists("id", "reg_nr")->all();

            // all object regnrs
            $all_objects = Objekts::lists("object_regnr", "id")->all();

            $split_to = array_chunk($persons, 100);

            // split in chunks of 100
            foreach ($split_to as $part_100) {
                // get person etickets from RK API
                $person_data = [];

                // person row array from etickets table
                foreach ($part_100 as $person_row) {
                    $pk = $person_row["pk"];
                    $person_data[$pk] = $person_row;
                    $object_id = 0;
                    $school_id = 0;
                    foreach ($person_row["get_objekti"] as $obj) {
                        if ($obj["pirma"] == 1) {
                            $object_id = $obj["object_id"];
                        }
                    }

                    if (isset($all_objects[$object_id])) {
                        $reg_nr = $all_objects[$object_id];
                        if (isset($all_schools[$reg_nr])) {
                            $school_id = $all_schools[$reg_nr];
                        }
                    }

                    $person_data[$pk]["object_id"] = $object_id;
                    $person_data[$pk]["school_id"] = $school_id;
                }

                // RK API call
                $xml = 
                    '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                        <soap:Body>
                            <GetCardsByPersonsIds xmlns="http://www.rigaskarte.lv/dpa/addon">
                            <personCodes>';
                Log::info(["checkRKAll" => $part_100]);
                foreach ($part_100 as $pk) {
                    $xml .= '<string>' . $pk["pk"] . '</string>';
                }
                $xml .= '</personCodes>
                    </GetCardsByPersonsIds>
                  </soap:Body>
                </soap:Envelope>';

                $headers2 = array(
                    "POST  HTTP/1.1",
                    "Host: 91.231.68.12",
                    "Content-Type: text/xml; charset=\"utf-8\"",
                    "SOAPAction: \"http://www.rigaskarte.lv/dpa/addon/GetCardsByPersonsIds\"",
                    "Content-length: " . strlen($xml)
                );

                $url2 = Config::get('services.rs.rkurl');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url2);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers2);
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($ch, CURLOPT_USERPWD, Config::get('services.rs.rk'));
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_TIMEOUT, 990);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $rk_result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if (curl_errno($ch) || $http_status != 200) {
                    $this->error("Kļūda saņemot RK API datus ($http_status) - " . curl_error($ch));
                    Log::error("getfreepersons, RK API ($http_status) - CURL error - " . curl_error($ch));
                    return;
                }
                $info = curl_getinfo($ch);
                curl_close($ch);

                // LOG data to file
                $this->log_api_data("rk", "get multiple etickets", $rk_result, $info['url'], $info['total_time']);

                $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $rk_result);
                $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
                $xml_string = str_replace('<soap:', '<', $xml_string);
                $res1 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);
                $result1 = isset($res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"]) ?
                        $res1["Body"]["GetCardsByPersonsIdsResponse"]["GetCardsByPersonsIdsResult"]["LsrCardInfo"] : [];

                foreach ($result1 as $eticket_row) {
                    if (!isset($eticket_row["SerialNumber"]))
                        continue;

                    $this->process_single_eticket_from_db($eticket_row, $person_data);
                }

                $this->process_all_persons_from_db($person_data);
                $this->pk_etickets = [];
            }
        }

        // total time
        $end = time();
        $this->log_api_data("zz", "updatealletickets total time in seconds=" . ($end - $start));

        $this->info('Visu e-talonu informācijas atjaunošana sekmīgi pabeigta');
    }

    /**
     * @param $eticket_row
     * @param $person_data
     * @return array
     */
    protected function process_single_eticket_from_db($eticket_row, $person_data)
    {
        // log in DB
        $this->log_db_rk($eticket_row);

        if (strtotime(substr($eticket_row["ExpirationDate"], 0, 10)) > time()) {
            if (!isset($this->pk_etickets[$eticket_row["PersonCode"]])) {
                $this->pk_etickets[$eticket_row["PersonCode"]] = [];
            }
            array_push($this->pk_etickets[$eticket_row["PersonCode"]], $eticket_row["SerialNumber"]);
            $this->pk_etickets[$eticket_row["PersonCode"]] = array_unique($this->pk_etickets[$eticket_row["PersonCode"]]);

            // insert in etickets those e-tickets which does not exist
            $exists = Eticket::where("nr", $eticket_row["SerialNumber"])->first();
            if (!isset($exists->id)) {
                $school_id = $person_data[$eticket_row["PersonCode"]]["school_id"];

                if ($school_id > 0) {
                    $objekts_id = $person_data[$eticket_row["PersonCode"]]["object_id"];

                    if ($objekts_id <= 0) {
                        $this->error("Kļūda - nevaru atrast objektu etalonam " . $person_data[$eticket_row["PersonCode"]]["nr"]);
                        Log::error("getfreemealpersondata - nevaru atrast objektu etalonam " . $person_data[$eticket_row["PersonCode"]]["nr"]);
                    } else {

                        // check declared status only for first type
                        $mas = isset($person_data[$eticket_row["PersonCode"]]["FreeMeals"]["FreeMealPeriod"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["FreeMeals"]["FreeMealPeriod"]) : ['0000-00-00', '0000-00-00'];
                        $mas2 = isset($person_data[$eticket_row["PersonCode"]]["UnusedMeals"]["UnusedMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["UnusedMeals"]["UnusedMeal"]) : ['0000-00-00', '0000-00-00'];
                        $mas3 = isset($person_data[$eticket_row["PersonCode"]]["RdMeals"]["RdMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["RdMeals"]["RdMeal"]) : ['0000-00-00', '0000-00-00'];
                        $mas4 = isset($person_data[$eticket_row["PersonCode"]]["TableRdMeals"]["TableRdMeal"]) ? $this->find_current_periods($person_data[$eticket_row["PersonCode"]]["TableRdMeals"]["TableRdMeal"]) : ['0000-00-00', '0000-00-00'];

                        App::make('App\Http\Controllers\etickets\EticketsController')->save_new_eticket(
                            $eticket_row["SerialNumber"], 
                            $eticket_row["PersonCode"], 
                            $person_data[$eticket_row["PersonCode"]]["name"], 
                            "0000", 
                            $person_data[$eticket_row["PersonCode"]]["grade"], 
                            0, 
                            2, 
                            $objekts_id, 
                            $person_data[$eticket_row["PersonCode"]]["school_id"], 
                            $mas, 
                            $mas2, 
                            $mas3, 
                            $mas4
                        );
                    }
                }
            }
        }

        return;
    }

    /**
     * @param $person_data
     * @return mixed
     */
    protected function process_all_persons_from_db($person_data)
    {
        foreach ($this->pk_etickets as $current_pk => $current_etickets) {
            $etickets_string = implode(",", $current_etickets);

            // add or update DB, etickets base
            if ($etickets_string !== '' && isset($person_data[$current_pk])) {
                $base = EticketsBase::where("pk", $current_pk)->first();

                $name_arr = explode(" ", $person_data[$current_pk]["name"]);
                $name2 = array_pop($name_arr);
                $name1 = (is_array($name_arr)) ? implode(" ", $name_arr) : $name_arr;

                if (isset($base->id)) {
                    if ($etickets_string == $base->etickets)
                        continue;

                    $base->etickets = $etickets_string;
                    $base->custom = 0;
                    $base->name = $name1;
                    $base->surname = $name2;
                    $base->grade = $person_data[$current_pk]["grade"];
                    $base->school_id = $person_data[$current_pk]["school_id"];
                    $base->free_meals_from = $person_data[$current_pk]["free_meals_from"];
                    $base->free_meals_until = $person_data[$current_pk]["free_meals_until"];
                    $base->second_meals_from = $person_data[$current_pk]["second_meals_from"];
                    $base->second_meals_until = $person_data[$current_pk]["second_meals_until"];
                    $base->third_meals_from = $person_data[$current_pk]["third_meals_from"];
                    $base->third_meals_until = $person_data[$current_pk]["third_meals_until"];
                    $base->kopgalds_from = $person_data[$current_pk]["kopgalds_from"];
                    $base->kopgalds_until = $person_data[$current_pk]["kopgalds_until"];
                    $base->brivpusdienas = ($base->free_meals_until != '0000-00-00' || $base->second_meals_until != '0000-00-00' || $base->third_meals_until != '0000-00-00') ? 1 : 0;

                    if (!$base->save()) {
                        $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                        Log::error("dailymonitoring, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                    }
                } else {
                    $base = New EticketsBase();
                    $base->pk = $current_pk;
                    $base->etickets = $etickets_string;
                    $base->custom = 0;
                    $base->name = $name1;
                    $base->surname = $name2;
                    $base->grade = $person_data[$current_pk]["grade"];
                    $base->school_id = $person_data[$current_pk]["school_id"];
                    $base->free_meals_from = $person_data[$current_pk]["free_meals_from"];
                    $base->free_meals_until = $person_data[$current_pk]["free_meals_until"];
                    $base->second_meals_from = $person_data[$current_pk]["second_meals_from"];
                    $base->second_meals_until = $person_data[$current_pk]["second_meals_until"];
                    $base->third_meals_from = $person_data[$current_pk]["third_meals_from"];
                    $base->third_meals_until = $person_data[$current_pk]["third_meals_until"];
                    $base->kopgalds_from = $person_data[$current_pk]["kopgalds_from"];
                    $base->kopgalds_until = $person_data[$current_pk]["kopgalds_until"];
                    $base->brivpusdienas = ($base->free_meals_until != '0000-00-00' || $base->second_meals_until != '0000-00-00' || $base->third_meals_until != '0000-00-00') ? 1 : 0;

                    if (!$base->save()) {
                        $this->error("Kļūda saglabājot etickets_base datus - " . json_encode($base->getErrors()));
                        Log::error("dailymonitoring, etickets_base - DB insert error - " . json_encode($base->getErrors()));
                    }
                }
            }
        }

        return;
    }

}
