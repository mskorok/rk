<?php
/**
 * Created by PhpStorm.
 * User: michail
 * Date: 29.02.16
 * Time: 15:43
 */

namespace App\Contracts;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface PhotoUploads
 * @package App\Contracts
 */
interface PhotoUpload
{
    /**
     * @param Model $model
     * @param $file_column
     * @return mixed
     */
    public static function createImages(Model $model, $file_column);

    /**
     * @param Model $model
     * @param $file_column
     */
    public static function deleteImages(Model $model, $file_column);

    /**
     * @param Model $model
     * @param string $name
     * @param string $version
     * @return \Carbon\Carbon|\Illuminate\Support\Collection|int|mixed|null|string|static
     */
    public function getImage(Model $model, $name, $version);

    /**
     * @return array
     */
    public function getVersions();

    /**
     * @param $key
     * @return bool|array
     */
    public function getVersion($key);
}
