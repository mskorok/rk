<?php
namespace App\Listeners;

use App\Models\LogUser;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class LogoutListener
{
    public function handle($event)
    {
        if (App::environment() !== 'testing') {
            $user = Auth::user();

            // log event in DB
            LogUser::create([
                'user_id' => $user->id,
                'op' => 'logout',
                'value' => '',
                'ip' => isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : ''
            ]);

            // notifications
            Cache::forget('last_login_'.$user->id);
        }
    }
}
