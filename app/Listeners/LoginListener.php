<?php
namespace App\Listeners;

use App\Models\LogUser;
use App\Models\Ticket;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class LoginListener
{
    public function handle($event)
    {
        if (App::environment() !== 'testing') {
            $user = Auth::user();

            // log event in DB
            LogUser::create([
                'user_id' => $user->id,
                'op' => 'login',
                'value' => json_encode(
                    Input::except("_token", "_method", "cpassword", "password", "password_confirmation", "submit")
                ),
                'ip' => isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : ''
            ]);

            // notifications
            $user_id = $user->id;
            $last_login = $user->last_login;
            Cache::forever('last_login_'.$user_id, $last_login);

            $sk = Ticket::where(function ($query) use ($user_id) {
                $query->where('user_id', '=', $user_id)->orWhere('recipient_id', '=', $user_id);
            })->where(function ($query) use ($last_login) {
                $query->where('completed_when', '>=', $last_login)
                    ->orWhere('created_at', '>=', $last_login)
                    ->orWhere('updated_at', '>=', $last_login);
            })
            ->count();

            if ($sk > 0) {
                Session::flash('info', 'Jums pieejami jauni paziņojumi!');
            }
        }
    }
}
