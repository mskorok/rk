<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.7.8
 * Time: 12:14
 */

namespace App\Services;

use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Card;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\ManualPayment;
use App\Models\OwnerAccess;
use App\Models\Seller;
use App\Models\Settings;
use App\Models\Ticket;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder as QB;

class ETicketService
{
    /**
     * @var Eticket[]
     */
    protected $eTickets;

    /**
     * @var \App\Models\User|null
     */
    protected $user;
    /**
     * @var ManualPayment
     */
    protected $payment;

    /**
     * @var MessageBag
     */
    protected $bag;

    /**
     * @var Eticket
     */
    protected $currentETicket;

    /**
     * @var Settings
     */
    protected $requisites;

    /**
     * ETicketService constructor.
     * @throws \BadFunctionCallException
     */
    public function __construct()
    {
        $user = \Auth::user();
        if ($user) {
            /** @var UserService $userService */
            $userService = app('UserService');
            $this->eTickets = $userService->getETickets();
//            $this->eTickets = $userService->getETicketsWithGranted();
            $this->user = $user;
            $this->payment = null;
            $this->bag = new MessageBag();
            $this->currentETicket = null;
            $this->requisites = Settings::first();
        } else {
            throw new \BadFunctionCallException('User not authorizes');
        }
    }

    /**
     * @param $offset
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getWeekendPayments($offset)
    {
        $start = $this->getDateFromOffset($offset);

        $payments = [];

        $i = 0;
        /** @var Eticket $ticket */
        foreach ($this->eTickets as $ticket) {
            $i++;
            $payment = new \stdClass();
            for ($j = 0; $j < 7; $j++) {
                if ($j !== 0) {
                    /** @var \DateTime $current */
                    $current = new \DateTime($start);
                    $current->modify('+'.$j.' days');
                } else {
                    $current = new \DateTime($start);
                }



                $brivpusdienas = $this->getBrivpusdienas($current, $ticket);
                /** @var Collection $data */
                $collection = $this->getDayPurchases($current, $ticket);
                $sum = 0;

                foreach ($collection as $item) {
                    /** @var Transaction $item */
                    $sum += (int) $item->amount/100;
                }
                $sum = (float) $sum;
                switch ($j) {
                    case 0:
                        $payment->monday = $sum;
                        $payment->monday_free = (bool) $brivpusdienas;
                        break;
                    case 1:
                        $payment->tuesday = $sum;
                        $payment->tuesday_free = (bool) $brivpusdienas;
                        break;
                    case 2:
                        $payment->wednesday = $sum;
                        $payment->wednesday_free = (bool) $brivpusdienas;
                        break;
                    case 3:
                        $payment->thursday = $sum;
                        $payment->thursday_free = (bool) $brivpusdienas;
                        break;
                    case 4:
                        $payment->friday = $sum;
                        $payment->friday_free = (bool) $brivpusdienas;
                }
            }


            $payment->card_id = $ticket->id;

            $payment->sum_free = 0;
            if ($payment->monday_free) {
                $payment->sum_free++;
            }
            if ($payment->tuesday_free) {
                $payment->sum_free++;
            }
            if ($payment->wednesday_free) {
                $payment->sum_free++;
            }
            if ($payment->thursday_free) {
                $payment->sum_free++;
            }
            if ($payment->friday_free) {
                $payment->sum_free++;
            }

            $payment->sum = $payment->monday + $payment->tuesday +
                $payment->wednesday + $payment->thursday + $payment->friday;
            $html = view('portal.views.partials.profile.week_payments', compact('payment'));
            $payment->html = $html;
            $payments[$i] = $payment;
        }

        return $payments;
    }

    /**
     * @return string
     */
    public function getDefaultDateRange()
    {
        $start = (new \DateTime('now'))->modify('-1 month')->format('Y-m-d');
        $end = (new \DateTime('now'))->format('Y-m-d');

        return $start.' - '.$end;
    }

    /**
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getTransactions()
    {
        $dateRange = $this->getDefaultDateRange();

        $transactions = [];
        /** @var Eticket $ticket */
        foreach ($this->eTickets as $i => $ticket) {
            $result = $this->getCardTransactions($ticket->id, $dateRange, 0);
            $transactions[$ticket->id] = $result;
        }

        return $transactions;
    }

    /**
     * @param $id
     * @param $dateRange
     * @param $dealType
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getCardTransactions($id, $dateRange, $dealType)
    {
        $userAccountIds = $this->getUserAccountIds(1);
        /** @var Eticket $eTicket */
        $eTicket = Eticket::find($id);

        $start = $this->parseDataRange($dateRange);
        $end = $this->parseDataRange($dateRange, false);
        /** @var Builder $qb */
        $qb = Transaction::select(
            [
                'transactions.created_at',
                'reg_nr',
                'seller_reg_nr',
                'seller_name',
                'to_account_id',
                'from_eticket_nr',
                'amount',
                'transactions.id',
                'to_eticket_nr'
            ]
        );
        $qb->whereDate('transactions.created_at', '>=', $start->format('Y-m-d'))
            ->whereDate('transactions.created_at', '<=', $end->format('Y-m-d'));
        $qb->leftJoin('sellers', 'seller_reg_nr', '=', 'reg_nr');
        if ($dealType === 0) {
            $qb->whereNested(function ($qb) use ($eTicket) {
                $qb->where('from_eticket_nr', '=', $eTicket->nr);
                $qb->orWhere('to_eticket_nr', '', $eTicket->nr);
            });
        }
        if ($dealType === 1) {
            $qb->where('to_eticket_nr', '=', $eTicket->nr);
        }
        if ($dealType === 2) {
            $qb->where('from_eticket_nr', '=', $eTicket->nr);
        }

        $qb->where('from_account_id', '>=', 0)
            ->where('to_account_id', '>=', 0)
            ->where('t_status', '!=', 'F')
            ->where('amount', '!=', 0)
            ->whereNull('err_text')
            ->whereNull('err_code');
        $qb->orderBy('transactions.created_at', 'desc');
        $qb->limit(100);

        $collection = $qb->get(
            [
                'transactions.created_at',
                'reg_nr',
                'seller_reg_nr',
                'seller_name',
                'to_account_id',
                'from_eticket_nr',
                'amount',
                'transactions.id',
                'to_eticket_nr'
            ]
        );

        $items = $collection->toArray();
        $transactions = [];

        /** @var array $items */
        foreach ($items as $data) {
            $transaction = new \stdClass();
            $transaction->date = $data['created_at'];
            $transaction->id = $data['id'];
            if ($dealType === 1) {
                $transaction->from = trans('portal/partials/profile/section_1.user_account');
                $transaction->to = $eTicket->nr;
                $transaction->operation = trans('portal/partials/profile/section_1.refill');
            } elseif ($dealType === 2) {
                $transaction->from = $eTicket->nr;
                if (in_array($data['to_account_id'], $userAccountIds, false)) {
                    $transaction->to = trans('portal/partials/profile/section_1.user_account');
                    $transaction->operation = trans('portal/partials/profile/section_1.transfer');
                } elseif (!empty($data['to_eticket_nr'])) {
                    $transaction->to = $data['to_eticket_nr'];
                    $transaction->operation = trans('portal/partials/profile/section_1.transfer');
                } else {
                    $transaction->to = $data['seller_name'];
                    $transaction->operation = trans('portal/partials/profile/section_1.purchase');
                }
            } else {
                if (empty($data['from_eticket_nr'])) {
                    $transaction->from = trans('portal/partials/profile/section_1.user_account');
                    $transaction->to = $eTicket->nr;
                    $transaction->operation = trans('portal/partials/profile/section_1.refill');
                } else {
                    $transaction->from = $eTicket->nr;
                    if (in_array($data['to_account_id'], $userAccountIds, false)) {
                        $transaction->to = trans('portal/partials/profile/section_1.user_account');
                        $transaction->operation = trans('portal/partials/profile/section_1.transfer');
                    } elseif (!empty($data['to_eticket_nr'])) {
                        $transaction->to = $data['to_eticket_nr'];
                        $transaction->operation = trans('portal/partials/profile/section_1.transfer');
                    } else {
                        $transaction->to = $data['seller_name'];
                        $transaction->operation = trans('portal/partials/profile/section_1.purchase');
                    }
                }
            }
            $amount = (float) $data['amount'];

            $transaction->sum = number_format($amount/100, 2, '.', ' ');
            $transactions[] = $transaction;
        }

        return $transactions;
    }

    /**
     * @param null $type
     * @return array
     */
    public function getUserAccountIds($type = null)
    {
        $ownerAccesses = OwnerAccess::ao()->u($this->user->id)->get();
        $ids = [];
        /** @var OwnerAccess $access */
        foreach ($ownerAccesses as $access) {
            $ids[] = $access->owner_id;
        }
        /** @var Collection $accounts */
        $qb = Account::whereIn('owner_id', $ids);
        if ($type) {
            $qb->where('account_type', '=', (int) $type);
        }
        $qb->where('account_status', '=', 1);

        $accounts = $qb->get();

        $userAccountIds = [];
        /** @var Account $account */
        foreach ($accounts as $account) {
            $userAccountIds[] = $account->id;
        }
        return $userAccountIds;
    }

    /**
     * @return array
     */
    public function getCardIds()
    {
        $ids = [];
        /** @var Eticket $ticket */
        foreach ($this->eTickets as $ticket) {
            $ids[] = $ticket->nr;
        }
        return $ids;
    }

    /**
     * @return array
     */
    public function getETicketsIds()
    {
        $ids = [];
        foreach ($this->eTickets as $ticket) {
            $ids[] = $ticket->id;
        }
        return $ids;
    }

    /**
     * @param $offset
     * @param bool $s
     * @return string
     */
    public function getDateFromOffset($offset, $s = true)
    {
        $absOffset = abs($offset);
        if ((int)$offset === 0) {
            $start = (new \DateTime('this week'))->format('d.m.Y');
            $end = (new \DateTime('this week'))->modify('+1 week')->format('d.m.Y');
        } elseif ((int) $offset > 0) {
            $start = (new \DateTime('this week'))->modify('+'.$absOffset.' week')->format('d.m.Y');
            $endOffset = $offset +1;
            $end = (new \DateTime('this week'))->modify('+'.$endOffset.' week')->format('d.m.Y');
        } else {
            $endOffset = $offset +1;
            $endOffset = abs($endOffset);
            $start = (new \DateTime('this week'))->modify('-'.$absOffset.' week')->format('d.m.Y');
            $end = (new \DateTime('this week'))->modify('-'.$endOffset.' week')->format('d.m.Y');
        }
        if ($s) {
            return $start;
        } else {
            return $end;
        }
    }

    /**
     * @param Request $request
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getNotifications(Request $request)
    {
        $defaultDateRange = $this->getDefaultDateRange();
        $dateRange = $request->input('notification_daterange', $defaultDateRange);

        $type = (int) $request->input('notification_type', 0);
        $start = $this->parseDataRange($dateRange);
        $end = $this->parseDataRange($dateRange, false);


        /** @var \Illuminate\Database\Query\Builder $tickets */
        $tickets = Ticket::whereDate('created_at', '>=', $start->format('Y-m-d'))
            ->whereDate('created_at', '<=', $end->format('Y-m-d'));
        if (!empty($type)) {
            $tickets->where('ticket_type', '=', $type);
        }
        $id = $this->user->id;
        $tickets->whereNested(function (Builder $tickets) use ($id) {
            $tickets->where('user_id', $id);
            $tickets->orWhere('recipient_id', $id);
        });
        $notes = $tickets->get();

        $notifications = [];
        /** @var Ticket $note */
        foreach ($notes as $note) {
            $notification = new \stdClass();
            $notification->id = $note->id;
            $notification->operation = '';
            $notification->date = $note->created_at;
            if ($type === 0) {
                $notification->status = trans('portal/partials/profile/section_2.all');
            } else {
                $notification->status = $type === 1
                    ? trans('portal/partials/profile/section_2.finished')
                    : trans('portal/partials/profile/section_2.waiting');
            }
            $notification->operation_type = $note->type->name;
            $notification->text = $note->content;
            $notifications[] = $notification;
        }
        return $notifications;
    }

    /**
     * @param Request $request
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getInvoices(Request $request)
    {
        $defaultDateRange = $this->getDefaultDateRange();
        $dateRange = $request->input('invoice_daterange', $defaultDateRange);
        $card = (int) $request->input('invoice_ecard', 0);
        $type = (int) $request->input('invoice_type', 0);
        $start = $this->parseDataRange($dateRange);
        $end = $this->parseDataRange($dateRange, false);
        $end->modify('+1 day');
        $end->modify('-1 second');
        $eTicket = Eticket::where('nr', '=', $card)->first();

        /** @var \Illuminate\Database\Query\Builder $manualPayments */
        $manualPayments = ManualPayment::whereDate('created_at', '>=', $start->format('Y-m-d'))
            ->whereDate('created_at', '<=', $end->format('Y-m-d'));
        if ($type === 1) {
            $manualPayments->where('confirmed', '=', 1);
        }

        if ($type === 2) {
            $manualPayments->where('confirmed', '=', 0);
        }

        if ($card !== 0 && $eTicket instanceof Eticket) {
            $manualPayments->where('eticket_id', '=', $eTicket->id);
        } else {
            $ids = $this->getETicketsIds();
            $manualPayments->whereIn('eticket_id', $ids);
        }


        $payments = $manualPayments->get();
        $invoices = [];

        /** @var ManualPayment $payment */
        foreach ($payments as $payment) {
            $invoice = new \stdClass();
            $invoice->id = $payment->id;
            $invoice->date = $payment->created_at;
            $invoice->card_number = $payment->eticket->nr;
            $invoice->isDeletable = !($payment->amount > 0);
            if ($payment->amount > 0) {
                $amount = (float) $payment->amount;
            } else {
                $amount = (float) $payment->user_amount;
            }
            $invoice->sum =  number_format($amount/100, 2, '.', ' ');
            $invoice->status = (bool) $payment->confirmed
                ? trans('portal/partials/profile/section_3.approved')
                : trans('portal/partials/profile/section_3.wait_for_approve');
            $invoice->code = $payment->payment_code;
            $invoice->document = route('download_payment', ['pdf' => $payment->id]);
            $invoices[] = $invoice;
        }
        return $invoices;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \InvalidArgumentException
     */
    public function createHistoryHtml(Request $request)
    {
        $id = (int) $request->input('eticket_id');
        $i = $request->input('i');
        $dateRange = $request->input('deal_daterange_'.$i);
        $dealType = (int) $request->input('deal_type_'.$i);

        $transactions = $this->getCardTransactions($id, $dateRange, $dealType);
        return view('portal.views.partials.profile.table_history', ['i' => $i, 'transactions'=> $transactions]);
    }

    /**
     * @param $limitType
     * @return int
     */
    public function getLimits($limitType)
    {
        switch ($limitType) {
            case 'day':
                return 1;
            case 'week':
                return 2;
            case 'month':
                return 3;
            default:
                return 1;
        }
    }

    /**
     * @param integer $amount
     * @return MessageBag
     */
    public function createManualPayment($amount)
    {
        $this->payment = null;
        // check for confirmed user
        if ((int)$this->user->terms !== 1) {
            $this->bag->add('term_not_confirmed', trans('errors.term_not_confirmed'));
            return $this->bag;
        }
        // check for three unconfirmed payments for this eticket
        $waiting = ManualPayment::where('user_id', $this->user->id)
            ->where('confirmed', 0)
            ->where('eticket_id', $this->currentETicket->id)
            ->count();
        if ($waiting >= 3) {
            $this->bag->add('error', trans('errors.many_attempts', ['count' => $waiting]));
            return $this->bag;
        }

        $owner = OwnerAccess::ao()->u($this->user->id)->first()->owner;
        $account = AccountAccess::ao()->u($this->user->id)->first()->account;

        if ($account->owner_id !== $owner->id) {
            $this->bag->add('account_not_found', trans('errors.account_not_found'));
            return $this->bag;
        }

        $amount = (int) round($amount);
        $amount *= 100;
        
        // check for monthly limit in single payment
        if ($amount <= 0) {
            $this->bag->add('amount_exceeded', trans('validation.min.numeric', ['attribute' => 'Sum', 'min' => 1]));
            return $this->bag;
        }
        if ($amount > 14000) {
            $this->bag->add('amount_exceeded', trans('errors.amount_exceeded'));
            return $this->bag;
        }

        // payment identification
        $payment_code = strtolower(str_random(10));

        $manualPayment = new ManualPayment();

        $manualPayment->user_amount = $amount;
        $manualPayment->user_id = $this->user->id;
        $manualPayment->eticket_id = $this->currentETicket->id;
        $manualPayment->payment_code = $payment_code;
        $manualPayment->confirmed = 0;

        if (!$manualPayment->save()) {
            $errors = $manualPayment->getErrors()->toArray();
            foreach ($errors as $key => $error) {
                $error = is_array($error) ? $error[0] : $error;
                $this->bag->add($key, $error);
            }
            return $this->bag;
        }
        \Cache::forget('user_balance_' . $this->user->id);
        $this->payment = $manualPayment;
        try {
            /** @var Card $card */
            $card = $this->getCardByETicket();
            if ($card instanceof Card) {
                $card->payment_code = $manualPayment->payment_code;
                $card->payment_id = $manualPayment->id;
                $card->top_up_sum = $amount;
                $card->save();
            }
        } catch (\RuntimeException $e) {
            $this->bag->add('payment_create_error', $e->getMessage());
        }


        return $this->bag;
    }

    /**
     * @param $id
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @throws \Exception
     */
    public function deletePayment($id)
    {
        $payment = ManualPayment::findOrFail($id);
        $payment->delete();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View | string
     * @throws \Throwable
     */
    public function createPaymentHtml()
    {
        $requisites = $this->requisites;
        $eTicket = $this->currentETicket;
        $payment = $this->payment;
        return view('portal.views.partials.profile.payment', compact('eTicket', 'requisites', 'payment'))->render();
    }

    /**
     * @param $id
     */
    public function getETicket($id)
    {
        $this->currentETicket = null;
        foreach ($this->eTickets as $eTicket) {
            if ($eTicket->id === $id) {
                $this->currentETicket = $eTicket;
                break;
            }
        }
    }

    /**
     * @param string $dataRange
     * @param bool $start
     * @return \DateTime
     */
    public function parseDataRange($dataRange, $start = true)
    {
        if ($dataRange) {
            $dates = explode(' - ', $dataRange);
        } else {
            $dates[0] = (new \DateTime('now'))->format('Y-m-d');
            $end = new \DateTime('now');
            $dates[1] = $end->modify('-1 month')->format('Y-m-d');
        }

        $dates = array_map(function ($date) {
            return trim($date);
        }, $dates);
        if ($start) {
            return new \DateTime($dates[0]);
        } else {
            return new \DateTime($dates[1]);
        }
    }

    /**
     * @return string
     */
    public function getLastRefill()
    {
        $userAccountIds = $this->getUserAccountIds(1);
        $transaction = Transaction::where('from_account_id', 0)
            ->where('authorisator_id', '=', 0)
            ->where('mpos_id', '=', 0)
            ->whereNull('from_eticket_nr')
            ->whereNull('to_eticket_nr')
            ->where('confirmed', '=', 1)
            ->orderBy('created_at', 'desc')
            ->whereIn('to_account_id', $userAccountIds)
            ->first();
        if ($transaction instanceof Transaction) {
            return $transaction->created_at->format('Y-m-d');
        }
        return 0;
    }

    /**
     * @return int|mixed
     * @throws \InvalidArgumentException
     */
    public function getNextRefill()
    {
        $check = $this->getAverageChecks();
        $sum = 0;
        foreach ($check as $item) {
            $sum += $item;
        }

        $balance = 0;
        /** @var Eticket $ticket */
        foreach ($this->eTickets as $ticket) {
            $balance += $ticket->balance;
        }
        $balance /= 100;
        $days = 0;
        if ($sum > 0) {
            $days = floor($balance/$sum);
        }
        if ($days === 0) {
            $day = new \DateTime('now');
            $dNumber = $day->format('N');
            if ((int) $dNumber === 6) {
                $day->modify('+2 day');
            } elseif ((int) $dNumber === 7) {
                $day->modify('+1 day');
            }
            return $day->format('Y-m-d');
        } else {
            $day = new \DateTime('now');
            $day->modify('+'.$days.' days');
            $dNumber = $day->format('N');
            if ((int) $dNumber === 6) {
                $day->modify('+2 day');
            } elseif ((int) $dNumber === 7) {
                $day->modify('+1 day');
            }
            return $day->format('Y-m-d');
        }
    }

    /**
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getAverageChecks()
    {
        $this->currentETicket = null;
        $checks = [];
        foreach ($this->eTickets as $ticket) {
            $this->currentETicket = $ticket;
            $checks[$ticket->id] = $this->getAverageCheck();
        }
        if (count($this->eTickets) > 0) {
            $this->currentETicket = $this->eTickets[0];
        }
        return $checks;
    }

    /**
     * @return int
     * @throws \InvalidArgumentException
     */
    public function getAverageCheck()
    {
        $start = new \DateTime('now');
        $start->modify('-10 days');
        $days = 0;
        $amount = 0;
        for ($j = 0; $j < 10; $j++) {
            if ($j !== 0) {
                /** @var \DateTime $current */
                $current = clone $start;
                $current->modify('+'.$j.' days');
            } else {
                $current = clone $start;
            }
            if ($current === new \DateTime('now')) {
                break;
            }
            $sum = 0;
            /** @var Eticket $ticket */
            $collection = $this->getDayPurchases($current, $this->currentETicket);
            /** @var Transaction $item */
            foreach ($collection as $item) {
                $sum += (int) $item->amount/100;
            }
            $sum = (float) $sum;

            if ($sum !== 0) {
                $days++;
            }
            $amount += $sum;
        }
        if ($days !== 0) {
            $amount /= $days;
        }
        return $amount;
    }

    /**
     * @param $id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function blockCard($id)
    {
        /** @var Eticket $eTicket */
        $eTicket = Eticket::findOrFail($id);
        $ids = $this->getETicketsIds();
        if (!in_array($eTicket->id, $ids, true)) {
            return false;
        }
        $eTicket->eticket_status = 2;
        if (!$eTicket->save()) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function unblockCard($id)
    {
        /** @var Eticket $eTicket */
        $eTicket = Eticket::findOrFail($id);
        $ids = $this->getETicketsIds();
        if (!in_array($eTicket->id, $ids, true)) {
            return false;
        }
        if ((int) $eTicket->eticket_status === 2) {
            $eTicket->eticket_status = 1;
            if (!$eTicket->save()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws \InvalidArgumentException
     * @throws \OutOfRangeException
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function deleteCard($id)
    {
        /** @var Eticket $eTicket */
        $eTicket = Eticket::findOrFail($id);
        $ids = $this->getETicketsIds();
        if (!in_array($eTicket->id, $ids, true)) {
            return false;
        }
        if ($eTicket->balance > 0) {
            throw new \OutOfRangeException();
        }
        // check if free meal ticket
        $freeMeal = 0;
        $base = EticketsBase::where('pk', $eTicket->pk)->first();
        if ($base) {
            $baseEtickets = explode(',', $base->etickets);
            if (in_array($eTicket->nr, $baseEtickets, false)) {
                $freeMeal = 1;
            }
        }
        $account = Account::et()->find($eTicket->account_id);
        if ($freeMeal === 1) {
            $account->owner_id = 2;
            $account->save();

            $eTicket->pin = '0000';
            $eTicket->limit_type = 0;
            $eTicket->limit = 0;
            $eTicket->save();
        } else {
            // delete e-ticket
            $eTicket->eticket_status = 0;
            $eTicket->save();

            // delete account owner and deactivate
            $account->owner_id = 0;
            $account->account_status = 0;
            $account->save();

            // delete objects
            $eTicket->DetachObjekti();

            // delete e-ticket operations
            \DB::table('eticket_opers')->where('eticket_id', $eTicket->id)->delete();
        }


        return true;
    }

    /**
     * @return Card|null
     */
    protected function getCardByETicket()
    {
        /** @var CardService $service */
        $service = app('CardService');
        foreach ($this->user->cards as $card) {
            $number = $service->getNumber($card);
            if ((string) $number === (string) $this->currentETicket->nr) {
                return $card;
            }
        }
        return null;
    }

    /**
     * @param \DateTime $current
     * @param Eticket $ticket
     * @return Collection |array
     */
    protected function getDayPurchases(\DateTime $current, Eticket $ticket)
    {
        /** @var Builder $qb */
        $qb = Transaction::select(['amount'])
            ->whereNull('dotation_id')
            ->where('freemeals_type', '=', 0)
            ->where('t_status', '!=', 'F')
            ->where('from_account_id', '!=', 2)
            ->where('amount', '!=', 0)
            ->whereNull('err_text')
            ->whereNull('err_code')
            ->whereNull('to_eticket_nr')
            ->whereDate('created_at', '=', $current->format('Y-m-d'))
            ->where('from_eticket_nr', '=', $ticket->nr)
        ;

        return $qb->get();
    }

    /**
     * @param \DateTime $current
     * @param Eticket $ticket
     * @return bool
     */
    protected function getBrivpusdienas(\DateTime $current, Eticket $ticket)
    {
        /** @var Builder $qb */
        $qb = Transaction::select()
            ->where('dotation_id', '>', 0)
            ->where('freemeals_type', '>', 0)
            ->where('t_status', '!=', 'F')
            ->where('from_account_id', '=', 2)
            ->where('amount', '!=', 0)
            ->whereNull('err_text')
            ->whereNull('err_code')
            ->whereNull('to_eticket_nr')
            ->whereDate('created_at', '=', $current->format('Y-m-d'))
            ->where('from_eticket_nr', '=', $ticket->nr)
        ;

        /** @var Collection $data */
        $cnt = $qb->count();

        return $cnt > 0;
    }

    /***************** GETTERS and SETTERS *************************/
    /**
     * @return \App\Models\Eticket[]
     */
    public function getETickets()
    {
        return $this->eTickets;
    }

    /**
     * @param \App\Models\Eticket[] $eTickets
     */
    public function setETickets($eTickets)
    {
        $this->eTickets = $eTickets;
    }

    /**
     * @return \App\Models\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \App\Models\User|null $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return ManualPayment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param ManualPayment $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return MessageBag
     */
    public function getBag()
    {
        return $this->bag;
    }

    /**
     * @param MessageBag $bag
     */
    public function setBag($bag)
    {
        $this->bag = $bag;
    }

    /**
     * @return Eticket
     */
    public function getCurrentETicket()
    {
        return $this->currentETicket;
    }

    /**
     * @param Eticket $currentETicket
     */
    public function setCurrentETicket($currentETicket)
    {
        $this->currentETicket = $currentETicket;
    }

    /**
     * @return Settings
     */
    public function getRequisites()
    {
        return $this->requisites;
    }

    /**
     * @param Settings $requisites
     */
    public function setRequisites($requisites)
    {
        $this->requisites = $requisites;
    }
}
