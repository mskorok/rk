<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.31.7
 * Time: 21:00
 */

namespace App\Services;

use App\Http\Controllers\RollingCurlX;
use App\Http\Traits\Translit;
use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Card;
use App\Models\Dotations;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\EticketsObjekti;
use App\Models\GroupObjekts;
use App\Models\ManualPayment;
use App\Models\ObjectDotationPrices;
use App\Models\Objekts;
use App\Models\Owner;
use App\Models\OwnerAccess;
use App\Models\School;
use App\Models\SchoolPrices;
use App\Models\User;
use Carbon\Carbon;
use Elasticsearch\Common\Exceptions\RuntimeException;
use Illuminate\Support\MessageBag;

class CardService
{
    use Translit;
    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $errors;

    protected $result1;

    protected $result2;

    protected $results;

    protected $apiData;

    protected $test;

    protected $save;

    protected $lastCard;
    public function __construct()
    {
        $this->user = \Auth::user();
        $this->errors = [];
        $this->apiData = [];
        $this->test = false;
        $this->save = false;
        $this->lastCard = null;
    }

    /**
     * @param MessageBag $bag
     * @return MessageBag
     */
    public function createTickets(MessageBag $bag)
    {
        $cards = $this->user->cards;
        /** @var Card $card */
        try {
            foreach ($cards as $card) {
                $bag = $this->createTicket($card, $bag);
            }
        } catch (\RuntimeException $e) {
            $bag->add('create_talon', $e->getMessage());
        }
        return $bag;
    }

    /**
     * @param array $params
     * @param MessageBag $bag
     * @param bool $save
     * @return MessageBag
     */
    public function saveCards(array $params, MessageBag $bag, $save = false)
    {
        if (isset($params['i'])) {
            $i = (int) $params['i'];
        } else {
            $bag->add('error', 'param i not exist');
            return $bag;
        }

        for ($j = 1; $j <= $i; $j++) {
            try {
                if (!$this->fillCard($j, $params, $save)) {
                    foreach ($this->errors as $key => $error) {
                        $message = isset($error[0]) ? $error[0] : 'error';
                        $bag->add('card_' . $j, $key.'  '.$message);
                    }
                }
            } catch (\RuntimeException $e) {
                $bag->add('card_' . $j, $e->getMessage());
            }
        }
        return $bag;
    }

    public function saveCard($j, array $params, MessageBag $bag)
    {

        $card = Card::where('user_id', '=', $this->user->id)
            ->where('pk_1', $params["n{$j}_pk_1"])
            ->where('pk_2', $params["n{$j}_pk_2"])
            ->where('pk_3', $params["n{$j}_pk_3"])
            ->where('pk_4', $params["n{$j}_pk_4"])
            ->where('pk_5', $params["n{$j}_pk_5"])
            ->where('pk_6', $params["n{$j}_pk_6"])
            ->where('pk_7', $params["n{$j}_pk_7"])
            ->where('pk_8', $params["n{$j}_pk_8"])
            ->where('pk_9', $params["n{$j}_pk_9"])
            ->where('pk_10', $params["n{$j}_pk_10"])
            ->where('pk_11', $params["n{$j}_pk_11"])
            ->first();
        if (!$card) {
            $card = new Card();
        }
        $this->errors = [];
        $rules = $this->save ? Card::SAVE_VALIDATION_RULES : Card::VALIDATION_RULES;

        $lastCard = Card::orderBy('id', 'desc')->first();
        if ($lastCard instanceof Card) {
            $data['i'] = $lastCard->i + 1;
        } else {
            $data['i'] = ((int) $j === 0) ? 1 : $j;
        }
        $data['card_1'] = $params["n{$j}_card_number_1"];
        $data['card_2'] = $params["n{$j}_card_number_2"];
        $data['card_3'] = $params["n{$j}_card_number_3"];
        $data['card_4'] = $params["n{$j}_card_number_4"];
        $data['card_5'] = $params["n{$j}_card_number_5"];
        $data['card_6'] = $params["n{$j}_card_number_6"];
        $data['card_7'] = $params["n{$j}_card_number_7"];
        $data['card_8'] = $params["n{$j}_card_number_8"];
        $data['card_9'] = $params["n{$j}_card_number_9"];
        $data['card_10'] = $params["n{$j}_card_number_10"];

        $data['pk_1'] = $params["n{$j}_pk_1"];
        $data['pk_2'] = $params["n{$j}_pk_2"];
        $data['pk_3'] = $params["n{$j}_pk_3"];
        $data['pk_4'] = $params["n{$j}_pk_4"];
        $data['pk_5'] = $params["n{$j}_pk_5"];
        $data['pk_6'] = $params["n{$j}_pk_6"];
        $data['pk_7'] = $params["n{$j}_pk_7"];
        $data['pk_8'] = $params["n{$j}_pk_8"];
        $data['pk_9'] = $params["n{$j}_pk_9"];
        $data['pk_10'] = $params["n{$j}_pk_10"];
        $data['pk_11'] = $params["n{$j}_pk_11"];

        $data['limits'] = $params["n{$j}_period"];
        $data['name'] = $params["n{$j}_name"];
        $data['surname'] = $params["n{$j}_surname"];
        $data['school_id'] = $params["n{$j}_school"];
        $data['class'] = $params["n{$j}_class"];
        $data['user_id'] = $params['user_id'];
        $data['child_photo'] = $params["child_photo_{$j}"];
        $data['amount'] = $params["n{$j}_amount"];
        $validator = \Validator::make($data, $rules);
        if ($validator->passes()) {
            try {
                $data['amount'] = $data['amount']*100;
                $card->fill($data);
                $card->save();
                $this->lastCard = $card;
                return $bag;
            } catch (\RuntimeException $exception) {
                \Log::error('save_card_'.$j.'_error - '.$exception->getMessage());
                $bag->add('Card not saved', 'Card not saved');
                return $bag;
            }
        } else {
            \Log::error('card validation error save card- '.json_encode($validator->errors()));
            foreach ($validator->errors()->toArray() as $key => $value) {
                $bag->add($key, $value);
            }
            return $bag;
        }
    }

    /**
     * @param Card $card
     * @return int|string
     */
    public function getNumber(Card $card)
    {
        $nr = (string) $card->card_1;
        $nr .= (string) $card->card_2;
        $nr .= (string) $card->card_3;
        $nr .= (string) $card->card_4;
        $nr .= (string) $card->card_5;
        $nr .= (string) $card->card_6;
        $nr .= (string) $card->card_7;
        $nr .= (string) $card->card_8;
        $nr .= (string) $card->card_9;
        $nr .= (string) $card->card_10;
        return (string) $nr;
    }

    /**
     * @param array $params
     * @param MessageBag $bag
     * @return MessageBag
     */
    public function saveTopUpCards(array $params, MessageBag $bag)
    {
        $i = count($this->user->cards);
        for ($j = 1; $j <= $i; $j++) {
            $card = Card::where('user_id', '=', $this->user->id)->where('i', '=', $j)->first();
            if (!$card) {
                continue;
            }
            /** @var UserService $service */
            $service = app('UserService');
            $params['top_up_sum_' . $j] = $service->sanitizeZero($params['top_up_sum_' . $j]);
            $data['top_up_sum'] = $params['top_up_sum_' . $j];
            $validator = \Validator::make($data, Card::FILL_VALIDATION_RULES);
            if ($validator->passes()) {
                try {
                    $card->top_up_sum = $data['top_up_sum'];
                    $card->save();
                } catch (\RuntimeException $exception) {
                    \Log::error('save_top_up_card_'.$j.'_error - '.$exception->getMessage());
                    $bag->add('save_top_up_card_'.$j.'_error', $exception->getMessage());
                }
            } else {
                \Log::error('card top up  validation error - '.json_encode($validator->errors()));
                $this->errors = $validator->errors()->toArray();
                foreach ($this->errors as $key => $error) {
                    $message = isset($error[0]) ? $error[0] : 'error';
                    $bag->add('save_top_up_card_' . $j, $key.'  '.$message);
                }
            }
        }
        return $bag;
    }

    public function createPayments(MessageBag $bag)
    {
        $cards = $this->user->cards;
        /** @var Card $card */
        try {
            foreach ($cards as $card) {
                $bag = $this->createManualPayment($card, $bag);
            }
        } catch (\RuntimeException $e) {
            $bag->add('create_talon', $e->getMessage());
        }
        return $bag;
    }

    /**
     * @param Card $card
     * @param MessageBag $bag
     * @return mixed
     */
    protected function createManualPayment(Card $card, MessageBag $bag)
    {
        // check for confirmed user
        if ((int)\Auth::user()->terms !== 1) {
            $bag->add('term_not_confirmed', trans('errors.term_not_confirmed'));
            return $bag;
        }

        $eTicket = $this->getETicketFromCard($card);
        if (!$eTicket) {
            try {
                $bag = $this->createTicket($card, $bag);
            } catch (\RuntimeException $exception) {
                $bag->add('eticket not created', $exception->getMessage());
                return $bag;
            }
            $eTicket = $this->getETicketFromCard($card);
            if (!$eTicket) {
                $bag->add('eticket not found', 'Eticket not found');
                return $bag;
            }
        }

        $amount = (int) round($card->top_up_sum);
        $amount *= 100;

        $user = \Auth::user();

        // check for three unconfirmed payments for this eticket
        $waiting = ManualPayment::where('user_id', $user->id)
            ->where('confirmed', 0)
            ->where('eticket_id', $eTicket->id)
            ->count();

        if ($waiting >= 3) {
            $bag->add('error', trans('errors.many_attempts', ['count' => $waiting]));
            return $bag;
        }

        $owner = OwnerAccess::ao()->u($user->id)->first()->owner;
        $account = AccountAccess::ao()->u($user->id)->first()->account;

        if ($account->owner_id !== $owner->id) {
            $bag->add('account_not_found', trans('errors.account_not_found'));
            return $bag;
        }

        // check for monthly limit in single payment
        if ($amount <= 0) {
            $this->bag->add('amount_exceeded', trans('validation.min.numeric', ['attribute' => 'Sum', 'min' => 1]));
            return $this->bag;
        }        
        if ($amount > 14000) {
            $bag->add('amount_exceeded', trans('errors.amount_exceeded'));
            return $bag;
        }

        // payment identification
        $payment_code = strtolower(str_random(10));

        $manualPayment = new ManualPayment();

        $manualPayment->user_amount = $amount;
        $manualPayment->user_id = \Auth::user()->id;
        $manualPayment->eticket_id = $eTicket->id;
        $manualPayment->payment_code = $payment_code;
        $manualPayment->confirmed = 0;

        if (!$manualPayment->save()) {
            $errors = $manualPayment->getErrors()->toArray();
            foreach ($errors as $key => $error) {
                $error = is_array($error) ? $error[0] : $error;
                $bag->add($key, $error);
            }
            return $bag;
        }
        $card->payment_code = $payment_code;
        $card->payment_id = $manualPayment->id;
        $card->save();
        \Cache::forget('user_balance_' . $user->id);
        return $bag;
    }

    /**
     * @param Card $card
     * @param MessageBag $bag
     * @return MessageBag
     * @throws \RuntimeException
     */
    public function createTicket(Card $card, MessageBag $bag)
    {
        $this->errors = [];
        $owner = null;
        $object = null;
        try {
            $owner = OwnerAccess::ao()->u($this->user->id)->first()->owner;
            $nr = $card->school->reg_nr;
            $object = Objekts::where('object_regnr', $nr)->first();
        } catch (\RuntimeException $e) {
            \Log::error('create e-ticket - get object|owner error - ' . json_encode($e->getMessage()));
            throw new \RuntimeException(trans('errors.school_not_in_program').'  1');
        }
        if (!$object) {
            throw new \RuntimeException(trans('errors.school_not_in_program').'  2');
        }
        if (!$owner) {
            throw new \RuntimeException(trans('errors.owner_not_found'));
        }
        /** @var Owner $owner */
        /** @var Objekts $object */
        try {
            $number = $this->getNumber($card);
            $pk = $this->getPk($card);
            $base = EticketsBase::pk($number, $pk)->where('custom', '<=', 1)->where('brivpusdienas', 1)->first();
            if ($this->test) {
                $base = null;
            }
            $alreadyExists = Eticket::a()->nr($number)->first();
            if ($alreadyExists
                && (!isset($alreadyExists->account->owner_id) || $alreadyExists->account->owner_id !== 2)
            ) {
                throw new \RuntimeException(trans('errors.already_registered'));
            }

            if (!$eTicket = $this->activateETicket($card, $owner, $object)) {
                if (count($this->errors) > 0) {
                    foreach ($this->errors as $key => $value) {
                        $bag->add($key, $value);
                    }
                    $this->errors = [];
                    return $bag;
                }
                if ($base instanceof EticketsBase) {
                    $eTicket =$this->createETicketFomBase($card, $owner, $object);
                } else {
                    if (!$this->test) {
                        // make API calls
                        try {
                            $this->createEticketApiCalls($pk);
                        } catch (\Exception $exception) {
                            $bag->add('apiCall', $exception->getMessage());
                        }
                        try {
                            $this->checkResultsApi($card);
                        } catch (\Exception $exception) {
                            $bag->add('checkResult', $exception->getMessage());
                        }
                        // add to monitoring list
                        try {
                            $monitoring = $this->addPkToMonitoringList($pk);
                            if ($monitoring !== 'OK') {
                                $bag->add('data_processing_error', trans('data_processing_error'));
                                return $bag;
                            }
                        } catch (\Exception $exception) {
                            $bag->add('monitoring', $exception->getMessage());
                        }
                    }
                    if ($bag->any()) {
                        return $bag;
                    }

                    try {
                        $eTicket = $this->saveNewETicket($card, $owner, $object);
                        \DB::table('eticket_opers')->insert(
                            array('eticket_id' => $eTicket->id, 'operation_id' => 1)
                        );
                    } catch (\RuntimeException $exception) {
                        $bag->add('eticket not saved', $exception->getMessage());
                    }
                    $this->apiData = [];
                }
            }

            // update user active e-tickets cache
            \Cache::forget('active_etickets_' . $this->user->id);
            \Cache::forget('all_active_etickets_' . $this->user->id);
            \Cache::forget('user_accounts_et_' . $this->user->id);
            \Cache::forget('user_accounts_etg_' . $this->user->id);
        } catch (\RuntimeException $e) {
            $bag->add('create_ticket', $e->getMessage());
            return $bag;
        }
        return $bag;
    }

    public function getActiveSchools()
    {
        $qb = School::where('schools.reg_nr', '!=', '')
            ->distinct()
            ->join('objects', 'schools.reg_nr', '=', 'objects.object_regnr')
            ->orderBy('reg_nr');
        $query = $qb->getQuery();
        $sql = $query->toSql();
        $results = $qb->get(['schools.id', 'heading', 'reg_nr'])->toArray();
        $res = [];
        $numbers = [];
        foreach ($results as $result) {
            if (!in_array($result['reg_nr'], $numbers, false)) {
                $nr = $result['reg_nr'];
                $numbers[] = $nr;
                $res[$nr] = ['id' => $result['id'], 'nr' => $result['reg_nr'], 'name' => $result['heading']];
            } else {
                $nr = $result['reg_nr'];
                $value = $res[$nr];
                if ((int) $value['id'] < (int) $result['id']) {
                    $res[$nr] = ['id' => $result['id'], 'nr' => $result['reg_nr'], 'name' => $result['heading']];
                }
            }
        }
        $schools = [];
        foreach ($res as $item) {
            $schools[$item['id']] = $item['name'];
        }
        asort($schools);
        return $schools;
    }

    /**
     * @param Card $card
     * @return int|string
     */
    protected function getPk(Card $card)
    {
        $pk = $card->pk_1;
        $pk .= $card->pk_2;
        $pk .= $card->pk_3;
        $pk .= $card->pk_4;
        $pk .= $card->pk_5;
        $pk .= $card->pk_6 . '-';
        $pk .= $card->pk_7;
        $pk .= $card->pk_8;
        $pk .= $card->pk_9;
        $pk .= $card->pk_10;
        $pk .= $card->pk_11;
        return $pk;
    }

    /**
     * @param Card $card
     * @param Owner $owner
     * @param Objekts $object
     * @return Eticket
     * @throws \RuntimeException
     */
    protected function saveNewETicket(Card $card, Owner $owner, Objekts $object)
    {
        $number = $this->getNumber($card);
        $pk = $this->getPk($card);
        $limit_type = $this->getLimits($card);
        /** @var EticketsBase $base */
        $base = EticketsBase::pk($number, $pk)->where('custom', '<=', 1)->where('brivpusdienas', 1)->first();
        if ($this->test) {
            $base = null;
            $this->results = [];
            $this->apiData[2] = [];
        }
        if ($base) {
            $class = $base->grade;
            $name = $this->translit($card->name . ' ' . $card->surname);
        } elseif (count($this->results) > 0) {
            $class = $this->results['r2']['School']['SchoolClass'];
            $name = $this->results['r2']['name'];
        } else {
            $class = preg_replace('/[a-zA-Z]/', '', $card->class);
            $name = $this->translit($card->name . ' ' . $card->surname);
        }
        \DB::beginTransaction();

        if (strlen($number) === 8) {
            $number = '00' . $number;
        }

        try {
            /** @var Eticket $eTicket */
            $eTicket = Eticket::a()->nr($number)->where('pk', '=', $pk)->first();
            if ($eTicket) {
                /** @var Account $account */
                $account = Account::findOrFail($eTicket->account_id);
                if ($account) {
                    $account->owner_id = $owner->id;
                    $account->account_type = 2;
                    $account->account_status = 1;
                    $account->save();
                }

                $eTicket->pin = '0000';
                $eTicket->limit_type = $limit_type;
                $eTicket->limit = (int) round($card->amount);
                $eTicket->thumb_id = $card->child_photo;
                $eTicket->eticket_status = 1;
                $eTicket->school_id = $card->school_id;
                $eTicket->thumb_id = $card->child_photo;
                $eTicket->grade = strtolower($card->class);
                if (!$eTicket->save()) {
                    \Log::error('error, changing free eticket - ' . $eTicket->getErrors());
                    throw new \RuntimeException('error, changing free eticket', 1);
                }
                
                EticketsObjekti::where("eticket_id", $eTicket->id)->delete();
            } else {
                // create account
                $acc = new Account;
                $acc->account_type = 2;
                $acc->account_status = 1;
                $acc->owner_id = $owner->id;
                $acc->save();

                // create new e-ticket
                $eTicket = new Eticket;
                $eTicket->nr = $number;
                $eTicket->pk = $pk;
                $eTicket->name = $name;
                $eTicket->pin = '0000';
                $eTicket->grade = strtolower($card->class);
                $eTicket->limit_type = $limit_type;
                $eTicket->limit = (float) $card->amount;
                $eTicket->eticket_status = 1;
                $eTicket->balance = 0;
                $eTicket->turnover = 0;
                $eTicket->thumb_id = $card->child_photo;
                $eTicket->school_id = $card->school_id;
                $eTicket->account_id = $acc->id;

                if (isset($this->apiData[0], $this->apiData[0]['DateFrom'], $this->apiData[0]['DateTill'])) {
                    if (count($this->apiData[0]) >= 2) {
                        $eTicket->free_meals_from = substr($this->apiData[0]['DateFrom'], 0, 10);
                        $eTicket->free_meals_until = substr($this->apiData[0]['DateTill'], 0, 10);
                    } elseif (count($this->apiData[0]) === 2) {
                        $eTicket->free_meals_from = substr($this->apiData[0][0], 0, 10);
                        $eTicket->free_meals_until = substr($this->apiData[0][1], 0, 10);
                    }
                }

                if (isset($this->apiData[1], $this->apiData[1]['DateFrom'], $this->apiData[1]['DateTill'])) {
                    if (count($this->apiData[1]) >= 2) {
                        $eTicket->second_meals_from = substr($this->apiData[1]['DateFrom'], 0, 10);
                        $eTicket->second_meals_until = substr($this->apiData[1]['DateTill'], 0, 10);
                    } elseif (count($this->apiData[1]) === 2) {
                        $eTicket->second_meals_from = substr($this->apiData[1][0], 0, 10);
                        $eTicket->second_meals_until = substr($this->apiData[1][1], 0, 10);
                    }
                }

                if (isset($this->apiData[2], $this->apiData[2]['DateFrom'], $this->apiData[2]['DateTill'])) {
                    if (count($this->apiData[2]) >= 2) {
                        $eTicket->third_meals_from = substr($this->apiData[2]['DateFrom'], 0, 10);
                        $eTicket->third_meals_until = substr($this->apiData[2]['DateTill'], 0, 10);
                    } elseif (count($this->apiData[2]) === 2) {
                        $eTicket->third_meals_from = substr($this->apiData[2][0], 0, 10);
                        $eTicket->third_meals_until = substr($this->apiData[2][1], 0, 10);
                    }
                }

                if (isset($this->apiData[3], $this->apiData[3]['DateFrom'], $this->apiData[3]['DateTill'])) {
                    if (count($this->apiData[3]) >= 2) {
                        $eTicket->kopgalds_from = substr($this->apiData[3]['DateFrom'], 0, 10);
                        $eTicket->kopgalds_until = substr($this->apiData[3]['DateTill'], 0, 10);
                    } elseif (count($this->apiData[3]) === 2) {
                        $eTicket->kopgalds_from = substr($this->apiData[3][0], 0, 10);
                        $eTicket->kopgalds_until = substr($this->apiData[3][1], 0, 10);
                    }
                }

                if (!$eTicket->save()) {
                    \Log::error('error, creating eticket - ' . $eTicket->getErrors());
                    throw new \RuntimeException('error, creating eticket', 1);
                }

                // operations
                \DB::table('eticket_opers')->insert(['eticket_id' => $eTicket->id, 'operation_id' => 1]);

                // free meal dotations
                // first, calculate which period to use if available both
                $joined_date_from = '';
                $joined_date_until = '';

                if (isset($this->apiData[2])) {
                    $mas3 = array_values($this->apiData[2]);
                }
                if (isset($this->apiData[1])) {
                    $mas2 = array_values($this->apiData[1]);
                }
                if (isset($this->apiData[0])) {
                    $mas = array_values($this->apiData[0]);
                }
                $time = time();
                // use one of valid intervals by priority
                if (isset($mas3[0], $mas3[1]) && strtotime($mas3[0]) <= $time && strtotime($mas3[1]) > $time) {
                    $joined_date_from = substr($mas3[0], 0, 10);
                    $joined_date_until = substr($mas3[1], 0, 10);
                }

                if (isset($mas2[0], $mas2[1]) && strtotime($mas2[0]) <= $time && strtotime($mas2[1]) > $time) {
                    $joined_date_from = substr($mas2[0], 0, 10);
                    $joined_date_until = substr($mas2[1], 0, 10);
                }

                if (isset($mas[0], $mas[1]) && strtotime($mas[0]) <= $time && strtotime($mas[1]) > $time) {
                    $joined_date_from = substr($mas[0], 0, 10);
                    $joined_date_until = substr($mas[1], 0, 10);
                }

                // if there is a period to use
                if (trim($joined_date_from) !== '' && trim($joined_date_until) !== '') {
                    // check for persons existing free meal data
                    $dot_exists = Dotations::where('pk', $pk)->first();
                    if ($dot_exists) {
                        if ($dot_exists->until_date !== $joined_date_until) {
                            $dot_exists->until_date = $joined_date_until;
                            $dot_exists->last_used = substr(new Carbon('yesterday'), 0, 10);
                            if (!$dot_exists->save()) {
                                \Log::error("error, updating person dotation ($pk) - ".$dot_exists->getErrors());
                                throw new \RuntimeException('error, updating person dotation', 1);
                            }
                        }
                    } else {
                        if (strtotime($joined_date_from) <= time() && trim($joined_date_from) !== '') {
                            // calculate available dotation ids based on school and grade
                            try {
                                $regNumber = School::findOrFail($card->school_id)->reg_nr;
                                $dot_object_id = Objekts::where('object_regnr', $regNumber)->first()->id;
                                $grade_price = SchoolPrices::where('school_id', $card->school_id)
                                        ->whereGrade((int) $class)->first()->price;
                                $grade_price *= 100;
                                $dotation = ObjectDotationPrices::where('object_id', $dot_object_id)
                                    ->wherePrice($grade_price)->first();
                            } catch (\Exception $e) {
                                \Log::error("error finding dotation for child ($pk) - ".$e->getMessage());
                                throw new \RuntimeException('error finding dotation for child', 1);
                            }

                            if ($dotation) {
                                $dot = new Dotations();
                                $dot->pk = $pk;
                                $dot->dotation_id = $dotation->id;
                                $dot->from_date = $joined_date_from;
                                $dot->until_date = $joined_date_until;
                                $dot->import_date = date('Y-m-d');
                                $dot->last_used = '2015-01-01';
                                if (!$dot->save()) {
                                    \Log::error(
                                        'error, creating person dotation ($pk) ($joined_date_from) - '.$dot->getErrors()
                                    );
                                    throw new \RuntimeException('error, creating person dotation', 1);
                                }
                            }
                        }
                    }
                }
            }
            
            $eto = new EticketsObjekti;
            $eto->eticket_id = $eTicket->id;
            $eto->object_id = $object->id;
            $eto->pirma = 1;
            $eto->save();

            /** @var GroupObjekts $objectsGroup */
            $objectsGroup = GroupObjekts::where('object_id', $object->id)->first();

            if ($objectsGroup) {
                $other_objects = GroupObjekts::where('object_id', '!=', $object->id)
                    ->where('ogroup_id', $objectsGroup->id)
                    ->lists('object_id')
                    ->toArray();

                if (count($other_objects) > 0) {
                    $objectToAdd = [];
                    foreach ($other_objects as $other) {
                        $objectToAdd[] = $other;
                    }
                    $eTicket->saveObjekti($objectToAdd, 1, 0); //1=create new default
                }
            }
            
        } catch (\Exception $e) {
            \Log::error('create e-ticket - DB save error - ' . $e->getMessage() . ' | ' . $e->getLine());
            \DB::rollBack();

            throw new \RuntimeException('Kļūda saglabājot datus - ' . $e->getMessage());
        }

        \DB::commit();

        return $eTicket;
    }

    private function activateETicket(Card $card, Owner $owner, Objekts $object)
    {
        $number = $this->getNumber($card);
        $pk = $this->getPk($card);
        $name = $this->translit($card->name . ' ' . $card->surname);
        // check if e-ticket is created and inactive
        $inactiveETicket = Eticket::nr($number)->where('eticket_status', 0)->first();
        $base = EticketsBase::pk($number, $pk)->where('custom', '<=', 1)->where('brivpusdienas', 1)->first();

        if ($inactiveETicket && $base === null) {
            try {
                // check for old account
                $acc = Account::find($inactiveETicket->account_id);
                if ($acc) {
                    $acc->owner_id = $owner->id;
                    $acc->account_status = 1;
                    if (!$acc->save()) {
                        throw new \RuntimeException('Could not update an account', 1);
                    }
                } else {
                    // create account
                    $acc = new Account;
                    $acc->account_type = 2;
                    $acc->account_status = 1;
                    $acc->owner_id = $owner->id;
                    if (!$acc->save()) {
                        throw new \RuntimeException('Could not create an account', 1);
                    }
                }

                $inactiveETicket->eticket_status = 1;
                $inactiveETicket->pin = '0000';
                $inactiveETicket->name = $name;

                if (!$inactiveETicket->save()) {
                    throw new \RuntimeException('Could not update an eticket', 1);
                }

                $inactiveETicket->DetachObjekti();
                $inactiveETicket->saveObjekti([$object->id], 1); //1=create new default
                
                // check for grouped objects
                $ogroup = GroupObjekts::where("object_id", $object->id)->first();
                $ogroup_id = isset($ogroup->ogroup_id) ? $ogroup->ogroup_id : 0;

                if ($ogroup_id > 0) {
                    $other_objects = GroupObjekts::where("object_id", '!=', $object->id)
                        ->where("ogroup_id", $ogroup_id)
                        ->lists("object_id")
                        ->toArray();

                    if (count($other_objects) > 0) {
                        $objectToAdd = [];
                        foreach ($other_objects as $other) {
                            $objectToAdd[] = $other;
                        }
                        $inactiveETicket->saveObjekti($objectToAdd, 1, 0); //1=create new default
                    }
                }                

                return $inactiveETicket;
            } catch (\Exception $e) {
                \Log::error('create e-ticket - DB restore error - ' . json_encode($e->getMessage()));
                $this->errors['restore_eticket_error'] = $e->getMessage();
                $this->errors['not_saved'] = trans('errors.not_saved');
                return false;
            }
        }
        return false;
    }

    protected function createETicketFomBase(Card $card, Owner $owner, Objekts $object)
    {
        $number = $this->getNumber($card);
        $pk = $this->getPk($card);
        $name = $this->translit($card->name . ' ' . $card->surname);
        $base = EticketsBase::pk($number, $pk)->where('custom', '<=', 1)->where('brivpusdienas', 1)->first();
        // compare data
        $baseFullName = $base->name.' '.$base->surname;
        $base_name_translit = $this->translit($baseFullName);

        if (levenshtein($base_name_translit, $name) > 1) {
            throw new RuntimeException(trans('errors.person_name_surname_error'));
        }
        if (levenshtein($card->class, $base->grade) > 1) {
            throw new \RuntimeException(trans('errors.class_error'));
        }
        if ($card->school_id != $base->school_id) {
            throw new \RuntimeException(trans('errors.school_error'));
        }

        // add to monitoring list
        $monitoring = $this->addPkToMonitoringList($pk);
        if ($monitoring !== 'OK') {
            throw new \RuntimeException(trans('errors.data_processing_error'));
        }

        // everything ok - save
        $mas = ['DateFrom' => $base->free_meals_from, 'DateTill' => $base->free_meals_until];
        $mas2 = ['DateFrom' => $base->second_meals_from, 'DateTill' => $base->second_meals_until];
        $mas3 = ['DateFrom' => $base->third_meals_from, 'DateTill' => $base->third_meals_until];
        $mas4 = ['DateFrom' => $base->kopgalds_from, 'DateTill' => $base->kopgalds_until];
        $this->apiData = [$mas, $mas2, $mas3, $mas4];
        $eticket = $this->saveNewETicket($card, $owner, $object);
        $this->apiData = [];

        return $eticket;
    }

    /**
     * @param Card $card
     * @throws \RuntimeException
     */
    protected function checkResultsApi($card)
    {
        $results = $this->results;
        if (!isset($results['r1'], $results['r2'])) {
            throw new \RuntimeException(trans('errors.check_data_error'));
        }
        $class = preg_replace('/[a-zA-Z]/', '', $card->class);
        $name = $this->translit($card->name . ' ' . $card->surname);
        if ($results['r1'] === 'error' || $results['r2'] === 'error') {
            throw new \RuntimeException(trans('errors.check_data_error'));
        }

        if ($results['r1'] === 'error1' || $results['r2'] === 'error1') {
            throw new \RuntimeException(trans('errors.data_not_found'));
        }

        if ($results['r1'] === 0) {
            throw new \RuntimeException(trans('errors.card_not_found'));
        }

        if ($results['r1'] !== $results['r2']['pk']) {
            throw new \RuntimeException(trans('errors.card_owner_error'));
        }

        if ($results['r1'] !== $results['r2']['pk']) {
            throw new \RuntimeException(trans('errors.card_owner_error'));
        }

        if (!isset($results['r2']['School']['SchoolId'])) {
            throw new \RuntimeException(trans('errors.person_not_in_this_school'));
        }

        if ((int) $results['r2']['School']['SchoolId'] !== (int) $card->school_id) {
            throw new \RuntimeException(trans('errors.person_not_in_this_school'));
        }

        if ((int) $class !== (int) $results['r2']['School']['SchoolClass']) {
            throw new \RuntimeException(trans('errors.person_not_in_this_class'));
        }

        if (levenshtein($name, $this->translit($results['r2']['name'])) > 1) {
            throw new \RuntimeException(trans('errors.person_name_surname_error'));
        }

        $mas = isset($results['r2']['FreeMeals']['FreeMealPeriod'])
            ? \App::make('App\Console\Commands\BaseCommand')
                ->find_current_periods($results['r2']['FreeMeals']['FreeMealPeriod'])
            : ['0000-00-00','0000-00-00'];
        $mas2 = isset($results['r2']['UnusedMeals']['UnusedMeal'])
            ? \App::make('App\Console\Commands\BaseCommand')
                ->find_current_periods($results['r2']['UnusedMeals']['UnusedMeal'])
            : ['0000-00-00','0000-00-00'];
        $mas3 = isset($results['r2']['RdMeals']['RdMeal'])
            ? \App::make('App\Console\Commands\BaseCommand')
                ->find_current_periods($results['r2']['RdMeals']['RdMeal'])
            : ['0000-00-00','0000-00-00'];
        $mas4 = isset($results['r2']['TableRdMeals']['TableRdMeal'])
            ? \App::make('App\Console\Commands\BaseCommand')
                ->find_current_periods($results['r2']['TableRdMeals']['TableRdMeal'])
            : ['0000-00-00','0000-00-00'];
        $this->apiData = [$mas, $mas2, $mas3, $mas4];
    }
    /**
     * @param Card $card
     * @return int
     */
    protected function getLimits(Card $card)
    {
        switch ($card->limits) {
            case 'day':
                return 1;
            case 'week':
                return 2;
            case 'month':
                return 3;
            default:
                return 1;
        }
    }

    /**
     * @param int $j
     * @param array $params
     * @param bool $save
     * @return bool
     * @internal param Card $card
     */
    protected function fillCard($j, array $params, $save = false)
    {

        $card = Card::where('user_id', '=', $this->user->id)->where('i', '=', $j)->first();
        if (!$card) {
            $card = new Card();
        }
        $this->errors = [];
        $rules = $save ? Card::SAVE_VALIDATION_RULES : Card::VALIDATION_RULES;

        $data['i'] = $j;
        $data['card_1'] = $params["n{$j}_card_number_1"];
        $data['card_2'] = $params["n{$j}_card_number_2"];
        $data['card_3'] = $params["n{$j}_card_number_3"];
        $data['card_4'] = $params["n{$j}_card_number_4"];
        $data['card_5'] = $params["n{$j}_card_number_5"];
        $data['card_6'] = $params["n{$j}_card_number_6"];
        $data['card_7'] = $params["n{$j}_card_number_7"];
        $data['card_8'] = $params["n{$j}_card_number_8"];
        $data['card_9'] = $params["n{$j}_card_number_9"];
        $data['card_10'] = $params["n{$j}_card_number_10"];

        $data['pk_1'] = $params["n{$j}_pk_1"];
        $data['pk_2'] = $params["n{$j}_pk_2"];
        $data['pk_3'] = $params["n{$j}_pk_3"];
        $data['pk_4'] = $params["n{$j}_pk_4"];
        $data['pk_5'] = $params["n{$j}_pk_5"];
        $data['pk_6'] = $params["n{$j}_pk_6"];
        $data['pk_7'] = $params["n{$j}_pk_7"];
        $data['pk_8'] = $params["n{$j}_pk_8"];
        $data['pk_9'] = $params["n{$j}_pk_9"];
        $data['pk_10'] = $params["n{$j}_pk_10"];
        $data['pk_11'] = $params["n{$j}_pk_11"];

        $data['limits'] = $params["n{$j}_period"];
        $data['name'] = $params["n{$j}_name"];
        $data['surname'] = $params["n{$j}_surname"];
        $data['school_id'] = $params["n{$j}_school"];
        $data['class'] = $params["n{$j}_class"];
        $data['user_id'] = $this->user->id;
        $data['child_photo'] = $params["child_photo_{$j}"];
        $data['amount'] = (float) $params["n{$j}_amount"];
        $validator = \Validator::make($data, $rules);
        if ($validator->passes()) {
            try {
                $card->fill($data);
                $card->save();
                return true;
            } catch (\RuntimeException $exception) {
                \Log::error('save_card_'.$j.'_error - '.$exception->getMessage());
                $this->errors['Card not saved'] = 'Card not saved';
                return false;
            }
        } else {
            \Log::error('card validation error fill card - '.json_encode($validator->errors()));
            $this->errors = $validator->errors()->toArray();
            return false;
        }
    }

    /**
     * @param Card $card
     * @return Eticket
     */
    public function getETicketFromCard(Card $card)
    {
        $cardNumber = $this->getNumber($card);
        return Eticket::nr($cardNumber)->first();
    }

    protected function createEticketApiCalls($pk, $all_rk_tickets = 0)
    {
        $this->results = [];
        \Session::put('all_rk_tickets', $all_rk_tickets);

        $RCX = new RollingCurlX(2);

        //request 1 -  zz dats
        $xml = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"><wsa:Action>http://tempuri.org/IMonitoringService/GetPersonData</wsa:Action></soap:Header>
            <soap:Body>
              <tem:GetPersonData>
                 <!--Optional:-->
                 <tem:request>
                    <zzd:Header>
                       <zzd1:From></zzd1:From>
                    </zzd:Header>
                    <zzd:Person>
                       <zzd:PersonId>'.$pk.'</zzd:PersonId>
                    </zzd:Person>
                 </tem:request>
              </tem:GetPersonData>
            </soap:Body>
            </soap:Envelope>';

        $hostFromConfig = \Config::get('services.rs.zzurl2');

        $headers = array(
            'POST HTTP/1.1',
            'Accept-Encoding: gzip,deflate',
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/GetPersonData"',
            'Content-length: '.strlen($xml),
            'Host: '.$hostFromConfig,
            'Connection: Keep-Alive',
            'User-Agent: skolas.rigaskarte.lv',
        );

        $url = \Config::get('services.rs.zzurl');
        $options = [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSLCERT => storage_path().'/zz_cert.pem',
            CURLOPT_SSLCERTPASSWD => \Config::get('services.rs.certkey'),
        ];

        $RCX->addRequest($url, $xml, [$this, 'callbackFunctions'], [], $options, $headers);

        //request 2 - RK
        $xml2 = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
          <soap:Body>
            <GetCardsByPersonId xmlns="http://www.rigaskarte.lv/dpa/addon">
              <personCode>'.$pk.'</personCode>
            </GetCardsByPersonId>
          </soap:Body>
        </soap:Envelope>';

        $headers2 = array(
            'POST  HTTP/1.1',
            'Host: 91.231.68.12',
            'Content-type: text/xml; charset="utf-8"',
            'SOAPAction: "http://www.rigaskarte.lv/dpa/addon/GetCardsByPersonId"',
            'Content-length: '.strlen($xml2)
        );

        $url2 = \Config::get('services.rs.rkurl');
        $options2 = [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => \Config::get('services.rs.rk'),
        ];

        $RCX->addRequest($url2, $xml2, [$this, 'callbackFunctions'], [], $options2, $headers2);

        $RCX->setTimeout(90000); //in milliseconds
        $RCX->execute();

        $this->results = ['r1' => $this->result1, 'r2' => $this->result2];
        return $this->results;
    }

    public function callbackFunctions($response, $url, $request_info)
    {
        if ($url === \Config::get('services.rs.rkurl')) {
            if ($request_info['http_code'] !== 200) {
                $this->result1 = 'error';
            } else {
                // return all valid rk tickets
                $all_rk_tickets = \Session::get('all_rk_tickets', 0);

                // LOG data to file
                $log_file = \Session::get('custom_log_file', date('Y-m').'_rk_log.txt');
                \File::append(storage_path().'/'.$log_file, date('Y-m-d H:i:s').' - RK get etickets\n'.$response.'\n');

                // process data
                $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $response);
                $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
                $xml_string =str_replace('<soap:', '<', $xml_string);
                $res1 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);

                if (! isset($res1['Body']['GetCardsByPersonIdResponse']['GetCardsByPersonIdResult']['LsrCardInfo'])) {
                    $this->result1 = 'error1';
                } else {
                    $result1 = $res1['Body']['GetCardsByPersonIdResponse']['GetCardsByPersonIdResult']['LsrCardInfo'];

                    if ($all_rk_tickets === 0) {
                        if (isset($result1[0]['ExpirationDate'])) {
                            $result1 = $result1[0];
                        }

                        $result = (strtotime(substr($result1['ExpirationDate'], 0, 10)) > time())
                            ? $result1['PersonCode']
                            : 0;

                        $this->result1 = $result;
                    } else {
                        $ets = [];

                        // single
                        if (isset($result1['ExpirationDate'])) {
                            if (strtotime(substr($result1['ExpirationDate'], 0, 10)) > time()) {
                                $ets[] = $result1['SerialNumber'];
                            }
                        } else {
                            // multiple
                            //
                            foreach ($result1 as $et) {
                                if (strtotime(substr($et['ExpirationDate'], 0, 10)) > time()) {
                                    $ets[] = $et['SerialNumber'];
                                }
                            }
                        }

                        $this->result1 = $ets;
                    }
                }
            }
        } else {
            if ($request_info['http_code'] !== 200) {
                $this->result2 = 'error';
            } else {
                // LOG data to file
                $log_file = \Session::get('custom_log_file', date('Y-m').'_zz_log.txt');
                \File::append(storage_path().'/'.$log_file, date('Y-m-d H:i:s')." - getchilddata\n".$response.'\n');

                // process data
                $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $response);
                $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
                $xml_string = str_replace('<s:', '<', $xml_string);
                $xml_string = str_replace('<a:', '<', $xml_string);
                $xml_string = str_replace('<b:', '<', $xml_string);

                $res2 = json_decode(json_encode(simplexml_load_string($xml_string)), 1);

                if (! isset($res2['Body']['GetPersonDataResponse']['GetPersonDataResult'])) {
                    //
                } else {
                    $result2 = $res2['Body']['GetPersonDataResponse']['GetPersonDataResult'];

                    $this->result2 = 'error1';

                    $result = [
                        'pk' => $result2['Person']['PersonId'],
                        'name' => $result2['Person']['PersonName'].' '.$result2['Person']['PersonSurname'],
                        'School' => $result2['School'],
                        'FreeMeals' => $result2['FreeMeals'],
                        'UnusedMeals' => $result2['UnusedMeals'],
                        'RdMeals' => $result2['RdMeals']
                    ];

                    $this->result2 = $result;
                }
            }
        }
    }

    protected function addPkToMonitoringList($pk)
    {
        $xml = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
   <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"><wsa:Action>http://tempuri.org/IMonitoringService/UpdateMonitoringList</wsa:Action></soap:Header>
   <soap:Body>
      <tem:UpdateMonitoringList>
         <!--Optional:-->
         <tem:request>
            <zzd:Header>
               <zzd1:From>'.date("Y-m-d").'</zzd1:From>
            </zzd:Header>
            <zzd:Operations>
               <!--Zero or more repetitions:-->
               <zzd:MonitoringOperation>
                  <zzd:PersonId>'.$pk.'</zzd:PersonId>
                  <zzd:Operation>add</zzd:Operation>
               </zzd:MonitoringOperation>
            </zzd:Operations>
         </tem:request>
      </tem:UpdateMonitoringList>
   </soap:Body>
</soap:Envelope>';

        $host = \Config::get('services.rs.zzurl2');
        $headers = array(
            'POST HTTP/1.1',
            'Accept-Encoding: gzip,deflate',
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/UpdateMonitoringList"',
            'Content-length: '.strlen($xml),
            'Host: '.$host,
            'Connection: Keep-Alive',
            'User-Agent: skolas.rigaskarte.lv',
        );

        $url =  \Config::get('services.rs.zzurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLCERT, storage_path().'/zz_cert.pem');
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, \Config::get('services.rs.certkey'));

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // LOG data to file
        $log_file = \Session::get('custom_log_file', date('Y-m').'_zz_log.txt');
        \File::append(
            storage_path().'/'.$log_file,
            date('Y-m-d H:i:s')." - addtomonitoring ($http_code) \n".$response.'\n'
        );

        if ($http_code !== 200) {
            return 'error';
        }

        // process data
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $response);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string = str_replace('<s:', '<', $xml_string);
        $xml_string = str_replace('<a:', '<', $xml_string);
        $xml_string = str_replace('<b:', '<', $xml_string);

        $res = json_decode(json_encode(simplexml_load_string($xml_string)), 1);

        // need OK
        $result = isset ($res['Body']['UpdateMonitoringListResponse']['UpdateMonitoringListResult']['Results']['MonitoringOperationResult']['OperationAnswer']) ?
            substr($res['Body']['UpdateMonitoringListResponse']['UpdateMonitoringListResult']['Results']['MonitoringOperationResult']['OperationAnswer'],0,2) : '';

        // log and return if other response
        if ($result !== 'OK') {
            \Log::error('error, add to monitoring list ($pk) - '.implode(',', $res['Body']['UpdateMonitoringListResponse']['UpdateMonitoringListResult']['Results']['MonitoringOperationResult']));

            if (isset($res['Body']['UpdateMonitoringListResponse']['UpdateMonitoringListResult']['Results']['MonitoringOperationResult']['AnswerDescription'])) {
                $answer_description = trim($res['Body']['UpdateMonitoringListResponse']['UpdateMonitoringListResult']['Results']['MonitoringOperationResult']['AnswerDescription']);
                if (strpos($answer_description, 'jau ir ieraksts ar personas kodu') !== false) {
                    return 'OK';
                }
            }

            return 'error1';
        }

        return 'OK';
    }

    protected function getMonitoringListPks()
    {
        $xml = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
	<soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"><wsa:Action>http://tempuri.org/IMonitoringService/GetMonitoringList</wsa:Action></soap:Header>
   <soap:Body>
      <tem:GetMonitoringList/>
   </soap:Body>
</soap:Envelope>';

        $host = \Config::get('services.rs.zzurl2');

        $headers = array(
            'POST HTTP/1.1',
            'Accept-Encoding: gzip,deflate',
            'Content-Type: application/soap+xml;charset=UTF-8;action="http://tempuri.org/IMonitoringService/GetMonitoringList"',
            'Content-length: '.strlen($xml),
            'Host: '.$host,
            'Connection: Keep-Alive',
            'User-Agent: skolas.rigaskarte.lv',
        );

        $url = \Config::get('services.rs.zzurl');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLCERT, storage_path().'/zz_cert.pem');
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, \Config::get('services.rs.certkey'));

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // LOG data to file
        $log_file = \Session::get('custom_log_file', date('Y-m').'_zz_log.txt');
        \File::append(
            storage_path().'/'.$log_file,
            date('Y-m-d H:i:s').' - getmonitoringlist ($http_code) \n'.$response.'\n'
        );

        if ($http_code !== 200) {
            return 'error';
        }

        // process data
        $xml_string = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $response);
        $xml_string = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xml_string);
        $xml_string = str_replace('<s:', '<', $xml_string);
        $xml_string = str_replace('<a:', '<', $xml_string);
        $xml_string = str_replace('<b:', '<', $xml_string);

        $res = json_decode(json_encode(simplexml_load_string($xml_string)), 1);

        return isset($res['Body']['GetMonitoringListResponse']['GetMonitoringListResult']['string']) ?
            $res['Body']['GetMonitoringListResponse']['GetMonitoringListResult']['string'] : [];
    }

    /**
     * @param boolean $test
     */
    public function setTest($test)
    {
        $this->test = $test;
    }

    /**
     * @param boolean $save
     */
    public function setSave($save)
    {
        $this->save = $save;
    }

    /**
     * @return Card
     */
    public function getLastCard()
    {
        return $this->lastCard;
    }
}
