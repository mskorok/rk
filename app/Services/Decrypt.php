<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.18.7
 * Time: 09:11
 */

namespace App\Services;

class Decrypt
{
    /**
     * @param string $string
     * @return bool|string
     */
    public function decrypt($string = '')
    {
        $key = base64_decode(config('services.rs.key'));
        $iv = base64_decode(config('services.rs.iv'));

        $string = base64_decode($string);

        return $this->stripPadding(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $string, MCRYPT_MODE_CBC, $iv));
    }

    /**
     * @param $string
     * @return bool|string
     */
    protected function stripPadding($string)
    {
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if (preg_match("/$slastc{".$slast."}/", $string)) {
            return substr($string, 0, strlen($string)-$slast);
        } else {
            return false;
        }
    }
}
