<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 17.18.7
 * Time: 13:07
 */

namespace App\Services;

use App\Console\Commands\deleteUnconfirmedUsersDaily;
use App\Models\Account;
use App\Models\AccountAccess;
use App\Models\Card;
use App\Models\Eticket;
use App\Models\EticketsBase;
use App\Models\EticketsObjekti;
use App\Models\ManualPayment;
use App\Models\Objekts;
use App\Models\School;
use App\Models\Upload;
use App\Models\Owner;
use App\Models\OwnerAccess;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;

class UserService
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var array
     */
    protected $params;

    /**
     * @var User
     */
    protected $user;

    protected $newEmail;

    /**
     * The mailer instance.
     *
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->user = \Auth::user();
    }

    /**
     * @param array $data
     * @return bool
     * @internal param User $user
     */
    public function fillUser(array $data)
    {
        $user = $this->user;
        if ($data['is_manager'] === false &&  $data['manager_status'] === '') {
            $this->errors['manager_status'] = trans('errors.empty');
        }

        if ($data['is_owner'] === false &&  $data['owner_status'] === '') {
            $this->errors['owner_status'] = trans('errors.empty');
        }

        if (strtotime($data['document_date']) < strtotime('+90 days')) {
            $this->errors['document_date'] = trans('errors.document_expired');
        }

        if (count($this->errors) > 0) {
            return false;
        }

//        $data['terms'] = $data['is_accept_terms'] ? 3 : 0;
        $data['info'] = $data['doc_type'].' | '.$data['doc_number'].' | '.$data['document_date'];
        $data['pazime'] = (boolean) $data['has_status'];
        $data['pazime_text'] = $data['social_status'];

        if ($data['funds_origin'] === 'Cita uz kartes saņemto līdzeklu izcelsme' &&  $data['others_funds'] === '') {
            $this->errors['izcelsme2'] = trans('errors.empty');
            return false;
        }
        //  JSON fields
        $data['citi'] = json_encode([
            'pilsoniba' => $data['citizenship'],
            'maks1' => $data['income_month'],
            'maks2' => $data['outgoing_month'],
            'maks3' => $data['income_year'],
            'maks4' => $data['outgoing_year'],
            'maks5' => $data['income_amount'],
            'maks6' => $data['outgoing_amount'],
            'izcelsme1' => $data['funds_origin'],
            'izcelsme2' => $data['others_funds'],
            'darijumi1' => (boolean) $data['is_manager'],
            'darijumi2' => $data['manager_status'],
            'ipasnieks1' => (boolean) $data['is_owner'],
            'ipasnieks2' => $data['owner_status'],
        ]);
        unset(
            $data['is_accept_terms'],
            $data['doc_type'],
            $data['doc_number'],
            $data['document_date'],
            $data['has_status'],
            $data['social_status'],
            $data['citizenship'],
            $data['income_month'],
            $data['outgoing_month'],
            $data['income_year'],
            $data['outgoing_year'],
            $data['income_amount'],
            $data['outgoing_amount'],
            $data['is_manager'],
            $data['manager_status'],
            $data['is_owner'],
            $data['owner_status'],
            $data['user_id'],
            $data['doc_copy'],
            $data['doc_copy_path'],
            $data['doc_copy_image_id'],
            $data['id'],
            $data['/'],
            $data['\\'],
            $data['funds_origin'],
            $data['others_funds'],
            $data['']
        );
        $data['profile_filled'] = true;
        if (!$user->api_token) {
            $data['api_token'] = str_random(60);
        }
        $result = null;
        try {
            $email = $user->email;
            $user->forceFill($data);
            $result = $user->save();
            if ($user->email !== $email) {
                $this->sendChangedEmailLetter($user->email, $email);
            }
        } catch (\RuntimeException $exception) {
            \Log::error('not_saved_user - ' . json_encode($exception->getMessage()));
            $this->errors['not_saved_user'] = trans('errors.user_not_saved').'_'.$exception->getMessage();
        }
        if ($result) {
            $this->user = $user;
        }
        return $result;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function afterSave()
    {
        if ($this->user instanceof  User) {
            try {
                // create owner
                $owner = new Owner;
                $owner->owner_type=1;
                $owner->owner_name = $this->user->username.' '.$this->user->surname;
                $owner->save();

                if (isset($owner->id)) {
                    // create owner access
                    $oacc = new OwnerAccess;
                    $oacc->user_id = $this->user->id;
                    $oacc->owner_id = $owner->id;
                    $oacc->access_status = 1;
                    $oacc->access_level = 1;
                    $oacc->save();

                    if ($oacc->id) {
                        // create account
                        $acc = new Account;
                        $acc->owner_id = $owner->id;
                        $acc->account_type=1;
                        $acc->account_status=1;
                        $acc->save();

                        if ($acc->id) {
                            // create account access
                            $access = new AccountAccess;
                            $access->user_id = $this->user->id;
                            $access->account_id = $acc->id;
                            $access->access_status=1;
                            $access->access_level=1;
                            $access->save();

                            if (!$access->id) {
                                $owner->delete();
                                $oacc->delete();
                                $acc->delete();
                                throw new \RuntimeException($access->getErrors().'1');
                            }
                        } else {
                            $oacc->delete();
                            $owner->delete();
                            throw new \RuntimeException($acc->getErrors().'2');
                        }
                    } else {
                        $owner->delete();
                        throw new \RuntimeException($oacc->getErrors().'3');
                    }
                } else {
                    throw new \RuntimeException($owner->getErrors().'4');
                }
            } catch (\RuntimeException $e) {
                \Log::error('after save exception - ' . json_encode($e->getMessage()));
                $this->errors['after_save'] = $e->getMessage();
                return false;
            }

            \Cache::forget('all_user_ids');
            \Session::forget('register_encoded');

            //send verification mail to user
            //---------------------------------------------------------
            $this->sendMail();
//            $this->sendNewUserNotificationMail();
            return true;
        }
        return false;
    }

    /**
     * @return bool
     * @internal param Profile $profile
     */
    public function addImages()
    {
        /** @var Profile $profile */
        $profile = $this->user->profile();

        if ($profile && $profile->avatar !== null) {
            $upl = new Upload;
            $upl->filename = $profile->avatar;
            $upl->path = config('uploads.avatar');
            $upl->extension = pathinfo($profile->avatar, PATHINFO_EXTENSION);
            $upl->user_id = $this->user->id;
            $upl->parent_id = 0;
            $upl->save();
        }

        if ($profile && $profile->doc_copy !== null) {
            $upl = new Upload;
            $upl->filename = $profile->doc_copy;
            $upl->path = config('uploads.doc_copy');
            $upl->extension = pathinfo($profile->doc_copy, PATHINFO_EXTENSION);
            $upl->user_id = $this->user->id;
            $upl->parent_id = 0;
            $upl->save();
        }
        return true;
    }

    /**
     * @return Profile
     */
    public function getProfile()
    {
        $profile = Profile::where('user_id', '=', \Auth::id())->get()->last();
        if (!$profile) {
            $profile = $this->createProfile();
        }
        return $profile;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function refreshEmail(Request $request)
    {
        if ($request->has('old_email') && $request->has('new_email') && $request->has('token')) {
            if ($this->user->confirmation_code === $request->input('token')) {
                if ($request->has('date') && $request->input('old_email') === $this->user->email) {
                    if (strtotime($request->input('date')) > strtotime('-1 day')) {
                        /** @var Profile $profile */
                        $profile = $this->user->profile();
                        $email = $request->input('new_email');
                        $existingUser = User::where('email', $email)->first();
                        if ($existingUser) {
                            return false;
                        }
                        $profile->email = $email;
                        $profile->save();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function refreshProfile(Request $request)
    {
        $profile = $this->user->profile();
        $params = $request->all();
        if (!$profile) {
            $profile = new Profile();
            $params['user_id'] = $this->user->id;
        }
        $params['username'] = $this->user->username;
        $params['surname'] = $this->user->surname;
        $params['pk'] = $this->user->pk;
        /** @var array $params */
        $params = array_map(function ($param) {
            return is_string($param) ? trim(strip_tags($param)) : $param;
        }, $params);
        $validator = \Validator::make($params, Profile::VALIDATION_RULES);
        if ($validator->passes()) {
            $profile->forceFill($params);
            $profile->setPhotoUploads(true);
            $profile->save($params);
        }

        return $profile;
    }

    /**
     * @param Profile $profile
     * @param MessageBag $bag
     * @return MessageBag
     * @internal param Request $request
     */
    public function refreshUser(Profile $profile, MessageBag $bag)
    {
        try {
            $data = $profile->getAttributes();
            $data['terms'] = $this->user->terms;
            if (!$this->fillUser($data)) {
                $bag->add('save_user_exception', trans('errors.save_user_exception'));
            }
        } catch (\RuntimeException $exception) {
            \Log::error('save profile exception - ' . json_encode($exception->getMessage()));
            $bag->add('save_profile_exception', $exception->getMessage());
        }
        return $bag;
    }

    /**
     * @param Request $request
     * @param MessageBag $bag
     * @return bool|MessageBag
     */
    public function refreshPassword(Request $request, MessageBag $bag)
    {
        $params = $request->all();
        $errors = [];
        if (!isset($params['password_new_confirm'], $params['password_current'], $params['password_new'])
            || (
                strlen($params['password_new_confirm']) < 4
                && strlen($params['password_current']) < 4
                && strlen($params['password_new']) < 4
            )
        ) {
            return false;
        }
        $confirm = trim(strip_tags($params['password_new_confirm']));
        $new = trim(strip_tags($params['password_new']));
        $current = trim(strip_tags($params['password_current']));

        $errors = $this->checkPassword($new, 'New', $errors);
        $errors = $this->checkPassword($current, 'Current', $errors);
        if ($new !== $confirm) {
            $errors['password_new_confirm'] = trans('errors.password_not_confirmed');
        }
        $credentials = ['email' => \Auth::user()->email, 'password' => $params['password_current']];
        if (!\Auth::validate($credentials)) {
            $errors['password_current'] = trans('errors.bad_current_password');
        }

        if (count($errors) === 0) {
            $this->user->password = bcrypt($params['password_new']);
            $this->user->save();
            return true;
        } else {
            foreach ($errors as $key => $error) {
                $bag->add($key, $error);
            }
            return $bag;
        }
    }

    /**
     * @param $password
     * @param $name
     * @param $errors
     * @return array
     */
    protected function checkPassword($password, $name, $errors)
    {
        if (strlen($password) < 8) {
            $errors[$name.'_short'] = trans('errors.password_too_short', ['pass' => $name]);
        }

        if (!preg_match('/\d+/', $password)) {
            $errors[$name.'_number'] = trans('errors.password_must_has_number');
        }

        if (!preg_match('/[a-z]+/', $password)) {
            $errors[$name.'_lowercase'] = trans('errors.password_lowercase', ['pass' =>$name]);
        }
        if (!preg_match('/[A-Z]+/', $password)) {
            $errors[] = trans('errors.password_uppercase', ['pass' =>$name]);
        }
        return $errors;
    }

    /**
     * @return int
     */
    public function getBalance()
    {
        try {
            $ownerId = (int) OwnerAccess::ao()->u($this->user->id)->first()->owner->id;
            $account = AccountAccess::ao()->u($this->user->id)->first()->account;
            if (isset($account->owner_id, $ownerId) && (int) $account->owner_id === $ownerId) {
                /** @var  Account $account */
                return $account->balance;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            \Log::error('get balance exception - ' . json_encode($e->getMessage()));
            return 0;
        }
    }

    /**
     * @param bool $granted
     * @return string
     */
    public function getCards($granted = false)
    {
        $allOwners = array();
        $owners = OwnerAccess::with('owner')->ao()->u($this->user->id)->get();
        foreach ($owners as $owner) {
            $allOwners[] = $owner->id;
        }

        if ($granted) {
            // get granted ownerships for user
            /** @var array $grantedAccesses */
            $grantedAccesses = $this->user->grantedOwnerships();
            /** @var OwnerAccess $ownerAccess */
            foreach ($grantedAccesses as $ownerAccess) {
                $allOwners[] = $ownerAccess->owner->id;
            }
        }

        $allAccounts = Account::whereIn('owner_id', $allOwners)->et()->lists('id')->all();

        $cards = count($allAccounts) !== 0 ? Eticket::a()->whereIn('account_id', $allAccounts)->get() : [];
        $result = '';
        if ($cards) {
            /** @var Eticket[] $cards */
            /** @var Eticket $card */
            foreach ($cards as $card) {
                $result .= $card->nr.', ';
            }
            substr($result, 0, -2);
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getBank()
    {
        return 'BANKA';
    }

    /**
     * @return string
     */
    public function getBankAccount()
    {
        return 'KONTA NUMURS';
    }

    /**
     * @return Account | null
     */
    public function getAccount()
    {
        return AccountAccess::ao()->u($this->user->id)->first()->account;
    }

    /**
     * @return string
     */
    public function getCitizenship()
    {
        /** @var Profile $profile */
        $profile = $this->user->profile();
        if (isset($profile->citizenship) && !empty($profile->citizenship)) {
            return $profile->citizenship;
        }

        $others = $this->getOthersJson();
        if (!$others) {
            return '';
        }
        return isset($others->pilsoniba) ?  $others->pilsoniba : '';
    }

    /**
     * @return int
     */
    public function getCitizenshipIndex()
    {
        $profile = $this->user->profile();
        $citizenship = isset($profile->citizenship) && !empty($profile->citizenship) ? $profile->citizenship : $this->getCitizenship();
        $citizenIndex = false;
        switch ($citizenship) {
            case 'Latvijas pilsonis':
                $citizenIndex = 1;
                break;
            case 'Latvijas nepilsonis':
                $citizenIndex = 2;
                break;
            case 'Uzturēšanas atļauja':
                $citizenIndex = 3;
                break;
            default:
                $citizenIndex = false;
        }
        return $citizenIndex;
    }

    /**
     * @param $old
     * @return int
     */
    public function getCitizenshipIndexFromOld($old)
    {
        $citizenship = isset($old['citizenship']) ? $old['citizenship'] : null;
        $citizenIndex = false;
        switch ($citizenship) {
            case 'Latvijas pilsonis':
                $citizenIndex = 1;
                break;
            case 'Latvijas nepilsonis':
                $citizenIndex = 2;
                break;
            case 'Uzturēšanas atļauja':
                $citizenIndex = 3;
                break;
            default:
                $citizenIndex = false;
        }
        return $citizenIndex;
    }

    /**
     * @return string
     */
    public function getDocNumber()
    {
        $profile = $this->user->profile();
        if (isset($profile->doc_number)) {
            return $profile->doc_number;
        }
        $info = $this->getInfoArray();
        return isset($info[1]) ? $info[1] : '';
    }

    /**
     * @return string
     */
    public function getDocType()
    {
        $profile = $this->user->profile();
        if (isset($profile->doc_type) && !empty($profile->doc_type)) {
            return $profile->doc_type;
        }
        $info = $this->getInfoArray();
        return isset($info[0]) ? $info[0] : '';
    }

    /**
     * @return bool|int
     */
    public function getDocTypeIndex()
    {
        $docType = $this->getDocType();
        $docTypeIndex = false;
        switch ($docType) {
            case 'Pase':
                $docTypeIndex = 1;
                break;
            case 'eID':
                $docTypeIndex = 2;
                break;
            case 'Cits documents':
                $docTypeIndex = 3;
                break;
            default:
                $docTypeIndex = false;
        }
        return $docTypeIndex;
    }

    /**
     * @param $old
     * @return bool|int
     */
    public function getDocTypeIndexFromOld($old)
    {
        $docType = isset($old['doc_type']) ? $old['doc_type'] : null;
        $docTypeIndex = false;
        switch ($docType) {
            case 'Pase':
                $docTypeIndex = 1;
                break;
            case 'eID':
                $docTypeIndex = 2;
                break;
            case 'Cits documents':
                $docTypeIndex = 3;
                break;
            default:
                $docTypeIndex = false;
        }
        return $docTypeIndex;
    }

    /**
     * @param null $time
     * @return null|string
     */
    public function getDocumentDate($time = null)
    {
        $format = $time ? 'Y-m-d H:i:s' : 'Y-m-d';
        $profile = $this->user->profile();
        if (isset($profile->document_date)) {
            return $profile->document_date;
        }
        $info = $this->getInfoArray();
        if (!isset($info[2])) {
            return (new \DateTime('now'))->format($format);
        }
        try {
            $date = new \DateTime($info[2]);
            if ($date instanceof \DateTime) {
                return $info[2];
            }
        } catch (\InvalidArgumentException $e) {
            \Log::error('invalid date exception - ' . json_encode($e->getMessage()));
            return (new \DateTime('now'))->format($format);
        }
        return (new \DateTime('now'))->format($format);
    }

    /**
     * @return string
     */
    public function hasStatus()
    {
        return (bool) $this->user->pazime;
    }

    /**
     * @return string
     */
    public function getSocialStatus()
    {
        return (string) $this->user->pazime_text;
    }

    /**
     * @return bool
     */
    public function isManager()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return true;
        }
        return isset($others->darijumi1) ? (boolean) $others->darijumi1 : true;
    }

    /**
     * @return bool
     */
    public function isOwner()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return true;
        }
        return isset($others->ipasnieks1) ? (boolean) $others->ipasnieks1 : true;
    }

    /**
     * @return string
     */
    public function getManagerStatus()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return '';
        }
        return isset($others->darijumi2) ? $others->darijumi2 : '';
    }

    /**
     * @return string
     */
    public function getOwnerStatus()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return '';
        }
        return isset($others->ipasnieks2) ? $others->ipasnieks2 : '';
    }

    /**
     * @return bool
     */
    public function isAcceptTerms()
    {
        return (int) $this->user->terms !== 0;
    }

    /**
     * @return int
     */
    public function getIncomeMonth()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return 0;
        }
        return isset($others->max1) ? (int) $others->max1 : 0;
    }

    /**
     * @return int
     */
    public function getIncomeYear()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return 0;
        }
        return isset($others->max3) ? (int) $others->max3 : 0;
    }

    /**
     * @return int
     */
    public function getIncomeAmount()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return 0;
        }
        return isset($others->max5) ? (int) $others->max5 : 0;
    }

    /**
     * @return int
     */
    public function getOutgoingMonth()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return 0;
        }
        return isset($others->max2) ? (int) $others->max2 : 0;
    }

    /**
     * @return int
     */
    public function getOutgoingYear()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return 0;
        }
        return isset($others->max4) ? (int) $others->max4 : 0;
    }

    /**
     * @return int
     */
    public function getOutgoingAmount()
    {
        $others = $this->getOthersJson();
        if (!$others) {
            return 0;
        }
        return isset($others->max6) ? (int) $others->max6 : 0;
    }

    /**
     * @return bool
     */
    public function hasManualPayments()
    {
        /** @var \Illuminate\Database\Eloquent\Collection|static[] $manualPayments */
        $manualPayments = $this->user->manualPayments()->getResults();
        return $manualPayments->count() > 0;
    }

    public function hasOldManualPayments()
    {
        /** @var \Illuminate\Database\Eloquent\Collection|static[] $manualPayments */
        $manualPayments = $this->user->manualPayments()->getResults();
        /** @var ManualPayment $manualPayment */
        foreach ($manualPayments as $manualPayment) {
            $dt = $manualPayment->getAttributeUpdatedAt();
            if (!$dt) {
                $dt = $manualPayment->getAttributeCreatedAt();
            }
            $dt = $dt::tomorrow();
            if ($dt < new \DateTime('now')) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function hasETickets()
    {
        $eTickets = $this->getETickets();
        return count($eTickets) > 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getETickets()
    {
        $access = OwnerAccess::where('user_id', '=', $this->user->id)->first();
        if ($access) {
            $owner = $access->owner;
        } else {
            return [];
        }

        if (!$owner) {
            return [];
        }

        /** @var \Illuminate\Database\Eloquent\Collection|static[] $accounts */
        $accounts = Account::where('owner_id', '=', $owner->id)->get();
        $eTickets = [];
        foreach ($accounts as $account) {
            $eTicket = Eticket::where('account_id', '=', $account->id)->first();
            if ($eTicket) {
                $eTickets[] = $eTicket;
            }
        }
        return $eTickets;
    }

    public function getETicketsWithGranted()
    {
        /** @var array $granted */
        $granted = $this->user->grantedOwnerships();
        $ids = [];
        /** @var OwnerAccess $access */
        foreach ($granted as $access) {
            $ids[] = $access->owner_id;
        }
        $ids = [];
        /** @var Collection $accounts */
        $accounts = Account::whereIn('owner_id', $ids);
        foreach ($accounts as $account) {
            $ids[] = $account->id;
        }
        /** @var Collection $etickets */
        $etickets = Eticket::whereIn('account_id', $ids);
        $result = [];
        foreach ($etickets as $eticket) {
            $result[] = $eticket;
        }
        return $result;
    }

    public function createCardsFromETickets()
    {
        $eTickets = $this->getETickets();
        $i = 0;
        foreach ($eTickets as $eTicket) {
            $i++;
            $this->createCardFromETicket($eTicket, $i);
        }
        $this->user->has_card = true;
        $this->user->card_not_finished = true;
        $this->user->save();
    }

    public function setCardFinished()
    {
        $this->user->refill_account_not_created = true;
        $this->user->save();
    }

    public function getUnreadNotifications()
    {
        return [];//TODO
    }

    /**
     * @param $param
     * @param bool $float
     * @return int | float
     */
    public function sanitizeZero($param, $float = false)
    {
        $param = (string) $param;
        $param = ltrim($param, '0');
        if ($float) {
            return (float) $param;
        }
        return (int) $param;
    }

    /**
     * @param Request $request
     */
    public function sendChangeEmailLetter(Request $request)
    {
        $email = $this->user->email;
        $view = 'portal.views.mail.change_email';
        $user = $this->user;
        $subject = trans('portal/mail/change_email.subject');
        $oldEmail = $request->input('old_email');
        $newEmail = $request->input('email_change');
        if ($email) {
            $this->mailer->send($view, compact('newEmail', 'oldEmail', 'user'), function ($m) use ($email, $subject) {
                /** @var $m Message */
                $m->to($email);
                $m->subject($subject);
            });
        }
    }

    /**
     * @param $email
     * @param $oldEmail
     */
    public function sendChangedEmailLetter($email, $oldEmail)
    {
        $view = 'portal.views.mail.changed_email';
        $user = $this->user;
        $subject = trans('portal/mail/changed_email.subject', ['old_email' => $oldEmail, 'email' => $email]);
        if ($email) {
            $this->mailer->send($view, compact('email', 'oldEmail', 'user'), function ($m) use ($email, $subject) {
                /** @var $m Message */
                $m->to($email);
                $m->subject($subject);
            });
        }
    }

    /**
     * @param User $user
     */
    public function sendDeleteUserLetter(User $user)
    {
        $view = 'portal.views.mail.delete_user';
        $email = $user->email;
        $subject = trans('portal/mail/delete_user.subject');
        $this->mailer->send($view, compact('email', 'user'), function ($m) use ($email, $subject) {
            /** @var $m Message */
            $m->to($email);
            $m->subject($subject);
        });
    }

    /**
     * Send mail after update user
     * @param $email
     */
    public function sendMailToFriends($email)
    {
        $view = 'portal.views.mail.friends';
        $user = $this->user;
        $subject = trans('portal/mail/friends.subject');
        if ($email) {
            $this->mailer->send($view, compact('email', 'user'), function ($m) use ($email, $subject) {
                /** @var $m Message */
                $m->to($email);
                $m->subject($subject);
            });
        }
    }

    /**
     * Send mail after relogin user
     * @param $oldEmail
     * @param $newEmail
     */
    public function sendReloginMail($oldEmail, $newEmail)
    {
        $view = 'portal.views.mail.relogin';
        $subject = trans('portal/mail/relogin.subject');
        $email = config('mail.from.address');
        $this->mailer->send($view, ['new' => $newEmail, 'old' => $oldEmail], function ($m) use ($email, $subject) {
            /** @var $m Message */
            $m->to($email);
            $m->subject($subject);
        });
    }

    /**
     * Send mail after update user
     */
    protected function sendMail()
    {
        $email = $this->user->email;
        $view = 'portal.views.mail.profile';
        $user = $this->user;
        $subject = trans('portal/mail/profile.subject');
        if ($email) {
            $this->mailer->send($view, compact('email', 'user'), function ($m) use ($email, $subject) {
                /** @var $m Message */
                $m->to($email);
                $m->subject($subject);
            });
        }
    }

    /**
     * Send mail after update user
     */
    protected function sendNewUserNotificationMail()
    {
        $email = config('mail.from.address');
        $view = 'portal.views.mail.new_user_notification';
        $user = $this->user;
        $subject = trans('portal/mail/new_user_notification.subject');
        if ($email) {
            $this->mailer->send($view, compact('user'), function ($m) use ($email, $subject) {
                /** @var $m Message */
                $m->to($email);
                $m->subject($subject);
            });
        }
    }

    /**
     * @return array
     */
    protected function getInfoArray()
    {
        $info = $this->user->info;
        if ($info === '') {
            return [];
        }
        $info = explode('|', $info);
        return array_map('trim', $info);
    }

    /**
     * @return null|string
     */
    protected function getOthersJson()
    {
        $others = $this->user->citi;
        if ($others === '') {
            return null;
        }

        try {
            return json_decode($others);
        } catch (\InvalidArgumentException $e) {
            \Log::error('other json invalid argument exception - ' . json_encode($e->getMessage()));
            return null;
        }
    }

    /**
     * @param Profile|null $profile
     * @return Profile
     */
    protected function createProfile(Profile $profile = null)
    {
        if (!($profile instanceof Profile)) {
            $profile = new Profile();
            $profile->user_id = $this->user->id;
        }
        $profile->username = $this->user->username;
        $profile->pk = $this->user->pk;
        $profile->surname = $this->user->surname;
        $profile->email = $this->user->email;
        $profile->phone = $this->user->phone;
        $profile->address = $this->user->address;
        $profile->citizenship = $this->getCitizenship();
        $profile->doc_type = $this->getDocType();
        $profile->doc_number = $this->getDocNumber();
        $profile->document_date = $this->getDocumentDate();
        $profile->has_status = $this->hasStatus();
        $profile->social_status = $this->getSocialStatus();
        $profile->income_month = $this->getIncomeMonth();
        $profile->income_year = $this->getIncomeYear();
        $profile->income_amount = $this->getIncomeAmount();
        $profile->outgoing_month = $this->getOutgoingMonth();
        $profile->outgoing_year = $this->getOutgoingYear();
        $profile->outgoing_amount = $this->getOutgoingAmount();
        $profile->is_manager = $this->isManager();
        $profile->is_owner = $this->isOwner();
        $profile->manager_status = $this->getManagerStatus();
        $profile->owner_status = $this->getOwnerStatus();
        $profile->is_accept_terms = $this->user->terms;
        $profile->doc_copy = $this->user->dokuments;
        $profile->save();
        return $profile;
    }

    protected function createCardFromETicket(Eticket $eTicket, $i)
    {
        $card = new Card();
        $cardArray = $this->getCurdNumberArray($eTicket->nr);
        $pkArray = $this->getCurdNumberArray($eTicket->pk);
        list(
            $card->card_1,
            $card->card_2,
            $card->card_3,
            $card->card_4,
            $card->card_5,
            $card->card_6,
            $card->card_7,
            $card->card_8,
            $card->card_9,
            $card->card_10
            ) = $cardArray;

        $card->pk_1 = $pkArray[0];
        $card->pk_2 = $pkArray[1];
        $card->pk_3 = $pkArray[2];
        $card->pk_4 = $pkArray[3];
        $card->pk_5 = $pkArray[4];
        $card->pk_6 = $pkArray[5];
        $card->pk_7 = $pkArray[7];
        $card->pk_8 = $pkArray[8];
        $card->pk_9 = $pkArray[9];
        $card->pk_10 = $pkArray[10];
        $card->pk_11 = $pkArray[11];

        switch ($eTicket->limit_type) {
            case 1:
                $card->limits = 'day';
                break;
            case 2:
                $card->limits = 'week';
                break;
            case 3:
                $card->limits = 'month';
                break;
            default:
                $card->limits = 'day';
        }
        $name = explode(' ', $eTicket->name);

        /** @var Objekts $object */
        $object = EticketsObjekti::where('eticket_id', '=', $eTicket->id)->where('pirma', '=', 1)->first();
        if ($object) {
            $nr = $object->object_regnr;
            $school = School::where('reg_nr', $nr)->first();
        } else {
            $school = null;
        }
        list($card->name, $card->surname) = $name;
        $card->amount = $eTicket->limit/100;

        $card->school_id = $school ? $school->id : null;
        $card->class = $eTicket->grade;
        $card->user_id = $this->user->id;
        $card->child_photo = 0;
        $card->i = $i;
        $card->save();
    }

    /**
     * @param string $string
     * @return array
     */
    protected function getCurdNumberArray($string)
    {
        $result = [];
        $len = strlen($string);
        for ($i = 0; $i < $len; $i++) {
            $result[] = $string[$i];
        }
        return $result;
    }

    /*********************** SETTERS AND GETTERS ***************************/

    /**
     * @return array
     */
    public function getErrors()
    {
        $errors = $this->errors;
        $this->errors = [];
        return $errors;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getNewEmail()
    {
        return $this->newEmail;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
