<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        TokenMismatchException::class,
        HttpResponseException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(\Exception $e)
    {
        if ($e instanceof \Exception && App::environment() === 'production') {
            if (! $e instanceof NotFoundHttpException &&
                ! $e instanceof ModelNotFoundException &&
                ! $e instanceof ValidationException &&
                ! $e instanceof TokenMismatchException &&
                ! $e instanceof HttpResponseException) {
                Mail::send(
                    'error.email.error',
                    ['emailbody' => $e->getMessage(), 'trace' => $e->getTraceAsString()],
                    function ($m) {
                        $m->to(Config::get('mail.adminemail'))
                            ->cc(Config::get('mail.adminemail2'))
                            ->subject('Skolas.rigaskarte.lv error');
                    }
                );
            }
        }

        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response | RedirectResponse
     */
    public function render($request, Exception $e)
    {
        $redirect = config('auth.views.web.login_path');

        if ($this->isHttpException($e)) {
            if ($e instanceof ModelNotFoundException) {
                return redirect()->to($redirect)->withErrors('error', trans('errors.model_not_found')); // todo REDIRECT
//                return Redirect::to('/')->with('error', 'Neeksistējošs ieraksts!');
            } elseif ($e instanceof NotFoundHttpException) {
                return redirect()->to($redirect)->withErrors('error', trans('errors.page_not_found'));// todo REDIRECT
//                return Redirect::to('/')->with('error', 'Neeksistējoša saite!');
            } elseif ($e instanceof TokenMismatchException) {
                return redirect()->to($redirect)->withErrors('error', trans('errors.time_spent')); // todo REDIRECT
//                return Redirect::to('/')->with('error', 'Ievades forma bija atvērta pārāk ilgi!');
            }
        } elseif ($e instanceof TokenMismatchException) {
            return redirect()->to($redirect)->withErrors('error', trans('errors.time_spent')); // todo REDIRECT
//            return Redirect::to('/')->with('error', 'Ievades forma bija atvērta pārāk ilgi!');
        } else {
            return response()->view('errors.other', [], 500);
        }

        return parent::render($request, $e);
    }
}
