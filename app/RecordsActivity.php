<?php

namespace App;

use App\Models\LogUser;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use ReflectionClass;

trait RecordsActivity
{
    /**
     * Register the necessary event listeners.
     *
     * @return void
     */
    protected static function bootRecordsActivity()
    {
        if (App::environment('testing')) {
            return;
        }

        foreach (static::getModelEvents() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($event, $model);
            });
        }
    }

    /**
     * Record activity for the model.
     *
     * @param  string $event
     * @return void
     */
    public function recordActivity($event, $model)
    {
        if (App::environment('testing')) {
            return;
        }

        $input = Input::except("_token", "_method", "cpassword", "password", "password_confirmation", "submit");

        $data = array_merge($model->toArray(), $input);
        if (count($data)==0) {
            return;
        }

        $ip = '127.0.0.1';
        if (!App::environment('testing')) {
            $ip = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1';
        }

        $user_id = isset(Auth::user()->id) ? Auth::user()->id : 0;

        LogUser::create([
            'user_id' => $user_id,
            'op' => $this->getActivityName($this, $event),
            'value' => json_encode($data),
            'ip' => $ip
        ]);
    }

    /**
     * Prepare the appropriate activity name.
     *
     * @param  mixed  $model
     * @param  string $action
     * @return string
     */
    protected function getActivityName($model, $action)
    {
        $name = strtolower((new ReflectionClass($model))->getShortName());

        return "{$action}_{$name}";
    }

    /**
     * Get the model events to record activity for.
     *
     * @return array|boolean
     */
    protected static function getModelEvents()
    {
        if (App::environment('testing')) {
            return false;
        }

        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }

        return [
            'created', 'deleted', 'updated'
        ];
    }
}
