<?php

namespace App\Models;

use App\RecordsActivity;
use Carbon\Carbon;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\ManualPayment
 *
 * @property int $id
 * @property int $user_id
 * @property int $eticket_id
 * @property int $user_amount
 * @property int $amount
 * @property int $confirmed
 * @property string $confirmed_date
 * @property int $accountant_id
 * @property int $transaction_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $payment_code
 * @property-read \App\Models\User $accountant
 * @property-read \App\Models\Eticket $eticket
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @property-read \App\Models\Transaction $transaction
 * @property-read \App\Models\User $user
 * @method static Builder|ManualPayment whereAccountantId($value)
 * @method static Builder|ManualPayment whereAmount($value)
 * @method static Builder|ManualPayment whereConfirmed($value)
 * @method static Builder|ManualPayment whereConfirmedDate($value)
 * @method static Builder|ManualPayment whereCreatedAt($value)
 * @method static Builder|ManualPayment whereEticketId($value)
 * @method static Builder|ManualPayment whereId($value)
 * @method static Builder|ManualPayment wherePaymentCode($value)
 * @method static Builder|ManualPayment whereTransactionId($value)
 * @method static Builder|ManualPayment whereUpdatedAt($value)
 * @method static Builder|ManualPayment whereUserAmount($value)
 * @method static Builder|ManualPayment whereUserId($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class ManualPayment extends Model
{
    use RecordsActivity;

    protected $table = 'manual_payments';

    public $rules = [
        'user_id' => 'required|integer',
        'eticket_id' => 'required|integer',
        'user_amount' => 'required|integer',
        'amount' => 'integer',
        'confirmed' => 'integer|between:0,1',
        'confirmed_date' => 'date',
        'accountant_id' => 'integer',
        'transaction_id' => 'integer',
        'payment_code' => '',
    ];

    protected $guarded = array('id');

    public $autoHydrateEntityFromInput = true;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function eticket()
    {
        return $this->hasOne('App\Models\Eticket', 'id', 'eticket_id');
    }

    public function accountant()
    {
        return $this->belongsTo('App\Models\User', 'accountant_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id');
    }

    public function humanAmount()
    {
        return sprintf("%.2f", $this->amount/100).' EUR';
    }

    public function humanUserAmount()
    {
        return sprintf("%.2f", $this->user_amount/100).' EUR';
    }

    public function first_payment()
    {
        return $this->user ? (($this->user->rk_confirmed_user_id == 0) ? 'JĀ' : '') : '';
    }

    public function created_date()
    {
        $d = new Carbon($this->created_at);
        return $d->toDateString();
    }

    /**
     * @return Carbon
     */
    public function getAttributeCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return Carbon
     */
    public function getAttributeUpdatedAt()
    {
        return $this->updated_at;
    }
}
