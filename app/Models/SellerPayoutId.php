<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\SellerPayoutId
 *
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 * @property int $id
 * @property int $trans_id
 * @property int $payout_trans_id
 * @property int $seller_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SellerPayoutId whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SellerPayoutId whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SellerPayoutId wherePayoutTransId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SellerPayoutId whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SellerPayoutId whereTransId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SellerPayoutId whereUpdatedAt($value)
 */
class SellerPayoutId extends Model
{
    use RecordsActivity;

    protected $table = 'seller_payout_ids';

//    public $rules = [
//        'seller_id' => 'required|integer',
//        'transaction_id' => 'required|integer',
//        'amount' => 'required|integer',
//        'period_code' => 'required',
//        'status' => 'required|integer|between:0,1',
//    ];

    protected $guarded = ['id'];
}
