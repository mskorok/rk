<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DocType
 *
 * @property int $id
 * @property string $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DocType extends Model
{
    const VALIDATION_RULES = [
        'type' => 'string|min:3|max:255|required'
    ];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes = []);
        $this->table = 'doc_type';
        $this->fillable = ['type'];
    }
}
