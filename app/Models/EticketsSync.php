<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\EticketsSync
 *
 * @property int $record_id
 * @property int $eticket_id
 * @property int $authorisator_id
 * @property string $record_state
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|EticketsSync whereAuthorisatorId($value)
 * @method static Builder|EticketsSync whereEticketId($value)
 * @method static Builder|EticketsSync whereRecordId($value)
 * @method static Builder|EticketsSync whereRecordState($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class EticketsSync extends Model
{
    use RecordsActivity;

    protected $table = 'etickets_sync';

    public $rules = [
        'eticket_id' => 'integer',
        'authorisator_id' => 'integer',
    ];

    public $timestamps = false;
    public $primaryKey = 'record_id';
}
