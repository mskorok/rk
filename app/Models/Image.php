<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Upload
 *
 * @property int $id
 * @property string $filename
 * @property string $path
 * @property int $size
 * @property string $extension
 * @property string $mimetype
 * @property int $user_id
 * @property int $parent_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Upload whereCreatedAt($value)
 * @method static Builder|Upload whereDeletedAt($value)
 * @method static Builder|Upload whereExtension($value)
 * @method static Builder|Upload whereFilename($value)
 * @method static Builder|Upload whereId($value)
 * @method static Builder|Upload whereMimetype($value)
 * @method static Builder|Upload whereParentId($value)
 * @method static Builder|Upload wherePath($value)
 * @method static Builder|Upload whereSize($value)
 * @method static Builder|Upload whereUpdatedAt($value)
 * @method static Builder|Upload whereUserId($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Image extends Model
{

    use RecordsActivity;

    protected $table = 'images';

    protected $guarded = ['id'];

}
