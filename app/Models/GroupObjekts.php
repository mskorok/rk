<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\GroupObjekts
 *
 * @property int $id
 * @property int $object_id
 * @property int $ogroup_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|GroupObjekts obj($id)
 * @method static Builder|GroupObjekts whereCreatedAt($value)
 * @method static Builder|GroupObjekts whereId($value)
 * @method static Builder|GroupObjekts whereObjectId($value)
 * @method static Builder|GroupObjekts whereOgroupId($value)
 * @method static Builder|GroupObjekts whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class GroupObjekts extends Model
{
    use RecordsActivity;

    protected $table = 'objects_ogroups';

    public $rules = [
        'object_id' => 'required',
        'ogroup_id' => 'required',
    ];

    /**
     * Scope - objekts
     */
    public function scopeObj($query, $id)
    {
        return $query->where('object_id', $id);
    }
}
