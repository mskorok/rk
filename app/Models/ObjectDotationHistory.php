<?php

namespace App\Models;

use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\ObjectDotationHistory
 *
 * @property int $id
 * @property string $datums
 * @property int $object_id
 * @property int $skaits
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $regen
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|ObjectDotationHistory whereCreatedAt($value)
 * @method static Builder|ObjectDotationHistory whereDatums($value)
 * @method static Builder|ObjectDotationHistory whereId($value)
 * @method static Builder|ObjectDotationHistory whereObjectId($value)
 * @method static Builder|ObjectDotationHistory whereRegen($value)
 * @method static Builder|ObjectDotationHistory whereSkaits($value)
 * @method static Builder|ObjectDotationHistory whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class ObjectDotationHistory extends Model
{
    protected $table = 'object_dotation_history';

    public $rules = [
        'datums' => 'required',
        'object_id' => 'integer|required',
        'skaits' => 'integer|required',
        'regen' => 'integer',
    ];

    protected $guarded = ['id'];
}
