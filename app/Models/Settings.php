<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Settings
 *
 * @property int $id
 * @property string $lang
 * @property string $banned_ips
 * @property int $wrong_password
 * @property string $import_path
 * @property bool $lvl
 * @property string $confirm_type
 * @property string $name
 * @property string $jur_adrese
 * @property string $talrunis
 * @property string $banka
 * @property string $bankas_kods
 * @property string $konts
 * @property string $reg_nr
 * @property string $payment_form_message
 * @property string $payment_form_details
 * @property string $dokuments_text
 * @property string $yearly_text
 * @property string $yearly_date
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Settings whereBanka($value)
 * @method static Builder|Settings whereBankasKods($value)
 * @method static Builder|Settings whereBannedIps($value)
 * @method static Builder|Settings whereConfirmType($value)
 * @method static Builder|Settings whereDokumentsText($value)
 * @method static Builder|Settings whereId($value)
 * @method static Builder|Settings whereImportPath($value)
 * @method static Builder|Settings whereJurAdrese($value)
 * @method static Builder|Settings whereKonts($value)
 * @method static Builder|Settings whereLang($value)
 * @method static Builder|Settings whereLvl($value)
 * @method static Builder|Settings whereName($value)
 * @method static Builder|Settings wherePaymentFormDetails($value)
 * @method static Builder|Settings wherePaymentFormMessage($value)
 * @method static Builder|Settings whereRegNr($value)
 * @method static Builder|Settings whereTalrunis($value)
 * @method static Builder|Settings whereWrongPassword($value)
 * @method static Builder|Settings whereYearlyDate($value)
 * @method static Builder|Settings whereYearlyText($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Settings extends Model
{
    use RecordsActivity;

    protected $table = 'settings';

    public $timestamps = false;

    public $rules = [
        'lang' => 'between:1,2',
        'banned_ips' => 'between:0,999',
        'wrong_password' => 'required|integer',
        'import_path' => 'between:0,255',
        'confirm_type' => 'between:0,20',
        'jur_adrese' => 'between:0,255',
        'talrunis' => 'between:0,50',
        'banka' => 'between:0,100',
        'bankas_kods' => 'between:0,10',
        'konts' => 'between:0,50',
        'reg_nr' => 'between:0,20'
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;
}
