<?php

namespace App\Models;

use Esensi\Model\Model;

/**
 * App\Models\Citizenship
 *
 * @property int $id
 * @property string $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Citizenship whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Citizenship whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Citizenship whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Citizenship whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class Citizenship extends Model
{
    const VALIDATION_RULES = [
        'type' => 'string|min:3|max:255|required'
    ];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes = []);
        $this->table = 'citizenship';
        $this->fillable = ['type'];
    }
}
