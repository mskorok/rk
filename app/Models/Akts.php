<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;

/**
 * App\Models\Akts
 *
 * @property int $id
 * @property int $seller_id
 * @property string $datums_no
 * @property string $datums_lidz
 * @property int $akts_nr
 * @property string $akts_nrt
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereAktsNr($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereAktsNrt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereDatumsLidz($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereDatumsNo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereSellerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Akts whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class Akts extends Model
{
    use RecordsActivity;

    protected $table = 'akti';

    public $rules = [
        'seller_id' => 'integer|required',
        'datums_no' => 'date|required',
        'datums_lidz' => 'date|required',
        'akts_nr' => 'integer|required',
        'akts_nrt' => 'required',
        'user_id' => 'integer|required'
    ];

    public $timestamps = true;

    protected $guarded = ['id'];
}
