<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;

/**
 * App\Models\Account
 *
 * @property int $id
 * @property int $owner_id
 * @property string $account_type
 * @property string $account_status
 * @property string $account_name
 * @property int $balance
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-read \App\Models\Owner $owner
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account et()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account etg()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account mpos()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account o($owner_id)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account sell()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account sellerFreeMeals()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account u()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account whereAccountName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account whereAccountStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account whereAccountType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account whereBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account whereOwnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Account whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class Account extends Model
{

    use RecordsActivity;

    protected $table = 'accounts';

    public $rules = array(
        'owner_id' => 'required',
        'account_type' => 'required|integer',
        'account_status' => 'required|integer',
        'account_name' => 'alpha_num',
        'balance' => 'integer'
    );

    protected $guarded = array('id');

    protected $fillable = [
        'owner_id',
        'account_type',
        'account_status',
        'account_name',
        'balance'
    ];

    public $autoHydrateEntityFromInput = true;

    /**
     * Scope - owner
     */
    public function scopeO($query, $owner_id)
    {
        return $query->where('owner_id', '=', $owner_id);
    }

    /**
     * Scope - user account
     */
    public function scopeU($query)
    {
        return $query->where('account_type', '=', 1);
    }

    /**
     * Scope - e-ticket account
     */
    public function scopeEt($query)
    {
        return $query->where('account_type', '=', 2);
    }

    /**
     * Scope - seller account
     */
    public function scopeSell($query)
    {
        return $query->where('account_type', '=', 3);
    }


    

    /**
     * Scope - e-ticket group account
     */
    public function scopeEtg($query)
    {
        return $query->where('account_type', '=', 5);
    }

    /**
     * Scope - mpos account
     */
    public function scopeMpos($query)
    {
        return $query->where('account_type', '=', 6);
    }

    /**
     * Scope - seller free meals account
     */
    public function scopeSellerFreeMeals($query)
    {
        return $query->where('account_type', '=', 7);
    }

    /**
     * Get the owner
     *
     * @return Owner
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\Owner', 'owner_id');
    }

    public function humanBalance()
    {
        $b = '';
        if (isset($this->balance)) {
            $b = sprintf("%.2f", intval($this->balance)/100);
        }
        return $b;
    }
}
