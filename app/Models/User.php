<?php

namespace App\Models;

use App\RecordsActivity;
use Esensi\Model\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laratrust\Traits\LaratrustUserTrait;
use LaravelArdent\Ardent\Ardent;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $user_type
 * @property string $user_status
 * @property string $password
 * @property string $hash_type
 * @property string $username
 * @property string $surname
 * @property string $email
 * @property string $phone
 * @property string $confirmation_code
 * @property bool $confirmed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $last_login
 * @property int $login_count
 * @property string $deleted_at
 * @property int $terms
 * @property string $remember_token
 * @property string $pk
 * @property string $address
 * @property string $info
 * @property int $pazime
 * @property string $pazime_text
 * @property string $citi
 * @property string $dokuments
 * @property string $yearly_status
 * @property string $yearly_updated
 * @property string $konts
 * @property int $rk_confirmed_user_id
 * @property string $rk_confirmed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OwnerAccess[] $accessToOwners
 * @property-read \App\Models\UserComments $comment
 * @property-read string $encrypted
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCiti($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereConfirmationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereConfirmed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereDokuments($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereHashType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereKonts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereLoginCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePazime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePazimeText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePk($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRkConfirmed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRkConfirmedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRoleIs($role = '')
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereSurname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereTerms($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUserStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUserType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereYearlyStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereYearlyUpdated($value)
 * @method static \Illuminate\Database\Query\Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 * @method bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 * @property int $bank_confirmed
 * @property int $profile_filled
 * @property int $has_card
 * @property int $card_not_finished
 * @property int $refill_account_not_created
 * @property string|null $doc_copy
 * @property string|null $document_path
 * @property int|null $document_image_id
 * @property string|null $avatar
 * @property string|null $avatar_path
 * @property int|null $avatar_image_id
 * @property string|null $api_token
 * @property string|null $delete_reason
 * @property int|null $doc_type_id
 * @property int|null $citizenship_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAvatarImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAvatarPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereBankConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCardNotFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCitizenshipId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeleteReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDocCopy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDocTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDocumentImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDocumentPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereHasCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereProfileFilled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRefillAccountNotCreated($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Card[] $cards
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ManualPayment[] $manualPayments
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, LaratrustUserTrait, RecordsActivity, SoftDeletes;

    public static $customMessages = array(
        'required' => 'Nav korekti aizpildīti obligātie lauki!'
    );
// uncomment for user creation from console : php artisan user:create
    protected $fillable = [
        'username', 'email', 'password', 'confirmation_code', 'api_token', 'delete_reason'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    public $autoPurgeRedundantAttributes = true;

    /**
     * Find a model by its primary key.
     * If {@link $throwOnFind} is set, will use {@link findOrFail} internally.
     *
     * Edgars 2015
     * overwrite to dehash the ID
     *
     * @param  mixed $id
     * @param  array $columns
     * @return Ardent|Collection
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public static function find($id, $columns = ['*'])
    {
        $debug = debug_backtrace(false);

        // dehash the id
        $arr = Hashids::decode($id);
        if (count($arr) === 1) {
            $id = $arr[0];
        }

        if ($debug[1]['function'] !== 'findOrFail') {
            return self::findOrFail($id, $columns);
        } else {
            return parent::find($id, $columns);
        }
    }

    /**
     * Get user by username
     * @param $username
     * @return mixed
     */
    public function getUserByUsername($username)
    {
        return static::where('username', '=', $username)->first();
    }

    /**
     * Save roles inputted from multiselect
     * @param $inputRoles
     */
    public function saveRoles($inputRoles)
    {
        if (count($inputRoles) === 1 && $inputRoles[0] == 0) {
            $this->roles()->detach();
        } elseif (!empty($inputRoles)) {
            $this->roles()->sync($inputRoles);
        } else {
            $this->roles()->detach();
        }
    }

    /**
     * Returns user's current role ids only.
     * @return array|bool
     */
    public function currentRoleIds()
    {
        $roles = $this->roles;
        $roleIds = false;
        if (!empty($roles)) {
            $roleIds = array();
            foreach ($roles as &$role) {
                $roleIds[] = $role->id;
            }
        }
        return $roleIds;
    }

    /**
     * Get the sellers
     *
     * @return Seller
     */
    public function accessToOwners()
    {
        return $this->hasMany('App\Models\OwnerAccess', 'user_id');
    }

    /**
     * Returns user's access grants from other users
     * @return array|bool
     */
    public function grantedOwnerships($type = 1)
    {
        $s = $this->accessToOwners;

        $roleIds = false;
        if (!empty($s)) {
            $roleIds = array();
            foreach ($s as &$single) {
                if ($single->access_status == 4) {
                    if ($type == 1 && $single->access_level == 1) {
                        $roleIds[] = $single;
                    } elseif ($type == 2 && $single->access_level < 3) {
                        $roleIds[] = $single;
                    }
                }
            }
        }

        return $roleIds;
    }

    /**
     * Returns user's current seller ids only.
     * @return array|bool
     */
    public function currentSellerIds()
    {
        $sellers = $this->accessToOwners;
        $roleIds = false;
        if (!empty($sellers)) {
            $roleIds = array();
            foreach ($sellers as &$seller) {
                if ($seller->access_status == 3) {
                    $roleIds[] = $seller->owner_id;
                }
            }
        }
        return $roleIds;
    }

    /**
     * Returns user's current seller access level
     * @return array|bool
     */
    public function currentSellerLevel()
    {
        $sellers = $this->accessToOwners;
        if (!empty($sellers)) {
            foreach ($sellers as &$seller) {
                if ($seller->access_status==3) {
                    return $seller->access_level;
                }
            }
        }
        return 0;
    }

    /**
     * Returns user's current object ids only.
     * @return array|bool
     */
    public function currentObjektsIds()
    {
        $sellers = $this->accessToOwners;
        $roleIds = false;
        if (!empty($sellers)) {
            $roleIds = array();
            foreach ($sellers as &$seller) {
                if ($seller->access_status == 2) {
                    $roleIds[] = $seller->owner_id;
                }
            }
        }
        return $roleIds;
    }

    /**
     * Returns user's current object access level
     * @return array|bool
     */
    public function currentObjektsLevel()
    {
        $sellers = $this->accessToOwners;
        if (!empty($sellers)) {
            foreach ($sellers as &$seller) {
                if ($seller->access_status == 2) {
                    return $seller->access_level;
                }
            }
        }
        return 0;
    }

    /**
     * Redirect after auth.
     * If ifValid is set to true it will redirect a logged in user.
     * @param $redirect
     * @param bool $ifValid
     * @return mixed
     */
    public static function checkAuthAndRedirect($redirect, $ifValid = false)
    {
        // Get the user information
        $user = Auth::user();
        $redirectTo = false;

        if (empty($user->id) && ! $ifValid) {// Not logged in redirect, set session.
            Session::put('loginRedirect', $redirect);
            $redirectTo = Redirect::to('user/login')
                ->with('notice', Lang::get('user/user.login_first'));
        } elseif (!empty($user->id) && $ifValid) { // Valid user, we want to redirect.
            $redirectTo = Redirect::to($redirect);
        }

        return array($user, $redirectTo);
    }

    public function fullname()
    {
        return $this->username.' '.$this->surname;
    }

    public function comment()
    {
        return $this->hasOne('App\Models\UserComments', 'pk', 'pk');
    }

    public function isActivated()
    {
        if ($this->confirmed) {
            return true;
        } else {
            return false;
        }
    }

    public function isBankConfirmed()
    {
        if ($this->pk === '' && !$this->bank_confirmed) {
            return false;
        } else {
            return true;
        }
    }

    public function isRkConfirmed()
    {
        return $this->rk_confirmed_user_id !== 0;
    }

    public function hasPk()
    {
        return $this->pk !== '';
    }

    public function isFirstConfirmed()
    {
        return $this->terms === 1;
    }

    public function isProfileFilled()
    {
        if ($this->hasPk()) {
            $this->profile_filled = true;
        }
        return (boolean) $this->profile_filled;
    }

    public function hasCard()
    {
        $service = app('UserService');
        $service->setUser($this);
        if (!$this->has_card && $service->hasETickets()) {
            $service->createCardsFromETickets();
            return true;
        }
        $count = 0;
        if (is_array($this->cards)) {
            $count = count($this->cards);
        } elseif ($cards = $this->cards instanceof  Collection) {
            $count = $this->cards->count();
        }

        if ($count === 0 && $service->hasETickets()) {
            $service->createCardsFromETickets();
            return true;
        }

        if ($this->has_card && $count === 0) {
            return false;
        }
        return (boolean) $this->has_card;
    }

    public function cardIsNotFinished()
    {
        if (!$this->hasCard()) {
            return false;
        }
        $service = app('UserService');
        $service->setUser($this);
        if (!$this->refill_account_not_created && $service->hasOldManualPayments()) {
            $service->setCardFinished();
            return true;
        }
        return (boolean) $this->card_not_finished;
    }

    public function refillAccountIsNotCreated()
    {
        return (boolean) $this->refill_account_not_created;
    }

    public function displayDate()
    {
        return date('m-d-y', strtotime($this->created_at));
    }

    /**
     * overwrite Entrust method, but with caching
     * @param $role
     * @param $requireAll
     * @return bool
     */
    public function hasRole($role, $requireAll = null)
    {
        $u = Auth::user();
        $roleids = Cache::remember('roleids_'.$u->id, 30, function () use ($u) {
            return $u->currentRoleIds();
        });

        $roles = Cache::rememberForever('roles', function () {
            return Role::lists("name", "id")->toArray();
        });

        $user_roles = array_intersect_key($roles, array_flip($roleids));

        $exists = false;
        foreach ($role as $r) {
            if (in_array($r, $user_roles, false)) {
                $exists = true;
                break;
            }
        }

        return $exists;
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile')->getResults();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards()
    {
        return $this->hasMany('App\Models\Card');
    }

    public function manualPayments()
    {
        return $this->hasMany('App\Models\ManualPayment');
    }
}
