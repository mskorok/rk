<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;

/**
 * App\Models\PermittedIp
 *
 * @property int $id
 * @property string $ip
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermittedIp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermittedIp whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class PermittedIp extends Model
{

    use RecordsActivity;

    protected $table = 'permitted_ip';
    public $rules = array(
        'ip' => 'required|ip',
    );
    protected $guarded = array('id');
    protected $fillable = [
        'ip',
    ];

}
