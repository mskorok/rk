<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Seller
 *
 * @property int $id
 * @property int $owner_id
 * @property string $seller_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $jur_adrese
 * @property string $talrunis
 * @property string $banka
 * @property string $bankas_kods
 * @property string $konts
 * @property string $reg_nr
 * @property string $contract_nr
 * @property string $contract_person
 * @property-read string $encrypted
 * @property-read \App\Models\Owner $owner
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Seller whereBanka($value)
 * @method static Builder|Seller whereBankasKods($value)
 * @method static Builder|Seller whereContractNr($value)
 * @method static Builder|Seller whereContractPerson($value)
 * @method static Builder|Seller whereCreatedAt($value)
 * @method static Builder|Seller whereId($value)
 * @method static Builder|Seller whereJurAdrese($value)
 * @method static Builder|Seller whereKonts($value)
 * @method static Builder|Seller whereOwnerId($value)
 * @method static Builder|Seller whereRegNr($value)
 * @method static Builder|Seller whereSellerName($value)
 * @method static Builder|Seller whereTalrunis($value)
 * @method static Builder|Seller whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Seller extends Model
{
    use RecordsActivity;

    protected $table = 'sellers';

    public $rules = [
        'owner_id' => 'integer',
        'seller_name' => 'min:2',
        'jur_adrese' => 'between:0,255',
        'talrunis' => 'between:0,50',
        'banka' => 'between:0,100',
        'bankas_kods' => 'between:0,10',
        'konts' => 'between:0,50',
        'reg_nr' => 'between:0,20',
        'contract_nr' => 'between:0,50',
        'contract_person' => 'between:0,100'
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;
    
    /**
     * Get the owner
     *
     * @return Owner
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\Owner', 'owner_id');
    }
}
