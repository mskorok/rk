<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;

/**
 * App\Models\AssignedRoles
 *
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AssignedRoles whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AssignedRoles whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AssignedRoles whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class AssignedRoles extends Model
{

    use RecordsActivity;

    protected $guarded = [];

    public $rules = [];
}
