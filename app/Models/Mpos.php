<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Mpos
 *
 * @property int $id
 * @property int $object_id
 * @property int $account_id
 * @property int $operation_id
 * @property int $owner_id
 * @property string $mpos_type
 * @property string $mpos_status
 * @property string $mpos_name
 * @property string $last_online
 * @property string $firmware_version
 * @property string $mpos_owner_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $secret_key
 * @property string $reboot_flag
 * @property string $mpos_serial
 * @property-read \App\Models\Account $account
 * @property-read string $encrypted
 * @property-read \App\Models\Objekts $object
 * @property-read \App\Models\Owner $owner
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Mpos obj($id)
 * @method static Builder|Mpos own($owner_id)
 * @method static Builder|Mpos whereAccountId($value)
 * @method static Builder|Mpos whereCreatedAt($value)
 * @method static Builder|Mpos whereFirmwareVersion($value)
 * @method static Builder|Mpos whereId($value)
 * @method static Builder|Mpos whereLastOnline($value)
 * @method static Builder|Mpos whereMposName($value)
 * @method static Builder|Mpos whereMposOwnerType($value)
 * @method static Builder|Mpos whereMposSerial($value)
 * @method static Builder|Mpos whereMposStatus($value)
 * @method static Builder|Mpos whereMposType($value)
 * @method static Builder|Mpos whereObjectId($value)
 * @method static Builder|Mpos whereOperationId($value)
 * @method static Builder|Mpos whereOwnerId($value)
 * @method static Builder|Mpos whereRebootFlag($value)
 * @method static Builder|Mpos whereSecretKey($value)
 * @method static Builder|Mpos whereUpdatedAt($value)
 * @method static Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class Mpos extends Model
{
    use RecordsActivity;

    public $rules = [
        'mpos_name' => 'required|min:2',
        'mpos_type' => 'numeric|between:1,2',
        'firmware_version' => 'alpha_num',
        'object_id' => 'integer',
        'owner_id' => 'integer',
        'account_id' => 'integer',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    /**
     * Scope - Owner
     */
    public function scopeOwn($query, $owner_id)
    {
        return $query->where('owner_id', $owner_id);
    }

    /**
     * Scope - Object
     */
    public function scopeObj($query, $id)
    {
        return $query->where('object_id', $id);
    }

    /**
     * Get the MPOS owner
     *
     * @return Owner
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\Owner', 'owner_id');
    }

    /**
     * Get the MPOS object
     *
     * @return Object
     */
    public function object()
    {
        return $this->belongsTo('App\Models\Objekts', 'object_id');
    }

    /**
     * Get the MPOS account
     *
     * @return Account
     */
    public function account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }
}
