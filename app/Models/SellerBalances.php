<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\SellerBalances
 *
 * @property int $id
 * @property int $seller_id
 * @property string $datums
 * @property int $balance
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $mpos_account_amounts
 * @property int $balance_paid
 * @property int $balance_free
 * @property int $daily_paid
 * @property int $daily_free
 * @property int $paid_out
 * @property int $free_out
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|SellerBalances whereBalance($value)
 * @method static Builder|SellerBalances whereBalanceFree($value)
 * @method static Builder|SellerBalances whereBalancePaid($value)
 * @method static Builder|SellerBalances whereCreatedAt($value)
 * @method static Builder|SellerBalances whereDailyFree($value)
 * @method static Builder|SellerBalances whereDailyPaid($value)
 * @method static Builder|SellerBalances whereDatums($value)
 * @method static Builder|SellerBalances whereFreeOut($value)
 * @method static Builder|SellerBalances whereId($value)
 * @method static Builder|SellerBalances whereMposAccountAmounts($value)
 * @method static Builder|SellerBalances wherePaidOut($value)
 * @method static Builder|SellerBalances whereSellerId($value)
 * @method static Builder|SellerBalances whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class SellerBalances extends Model
{
    use RecordsActivity;

    protected $table = 'seller_daily_balance';

    public $rules = [
        'seller_id' => 'required',
        'datums' => 'required',
        'balance' => 'integer',
        'balance_paid' => 'integer',
        'daily_paid' => 'integer',
        'mpos_account_amounts' => '',
    ];

    protected $guarded = ['id'];
}
