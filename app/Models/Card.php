<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Card
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $surname
 * @property int|null $school_id
 * @property string|null $class
 * @property int $card_1
 * @property int $card_2
 * @property int $card_3
 * @property int $card_4
 * @property int $card_5
 * @property int $card_6
 * @property int $card_7
 * @property int $card_8
 * @property int $card_9
 * @property int $card_10
 * @property int $pk_1
 * @property int $pk_2
 * @property int $pk_3
 * @property int $pk_4
 * @property int $pk_5
 * @property int $pk_6
 * @property int $pk_7
 * @property int $pk_8
 * @property int $pk_9
 * @property int $pk_10
 * @property int $pk_11
 * @property string $limits
 * @property float $amount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\School|null $school
 * @method static Builder|$this whereAmount($value)
 * @method static Builder|$this whereCard1($value)
 * @method static Builder|$this whereCard10($value)
 * @method static Builder|$this whereCard2($value)
 * @method static Builder|$this whereCard3($value)
 * @method static Builder|$this whereCard4($value)
 * @method static Builder|$this whereCard5($value)
 * @method static Builder|$this whereCard6($value)
 * @method static Builder|$this whereCard7($value)
 * @method static Builder|$this whereCard8($value)
 * @method static Builder|$this whereCard9($value)
 * @method static Builder|$this whereClass($value)
 * @method static Builder|$this whereCreatedAt($value)
 * @method static Builder|$this whereId($value)
 * @method static Builder|$this whereLimits($value)
 * @method static Builder|$this whereName($value)
 * @method static Builder|$this wherePk1($value)
 * @method static Builder|$this wherePk10($value)
 * @method static Builder|$this wherePk11($value)
 * @method static Builder|$this wherePk2($value)
 * @method static Builder|$this wherePk3($value)
 * @method static Builder|$this wherePk4($value)
 * @method static Builder|$this wherePk5($value)
 * @method static Builder|$this wherePk6($value)
 * @method static Builder|$this wherePk7($value)
 * @method static Builder|$this wherePk8($value)
 * @method static Builder|$this wherePk9($value)
 * @method static Builder|$this whereSchoolId($value)
 * @method static Builder|$this whereSurname($value)
 * @method static Builder|$this whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @method static Builder|$this whereUserId($value)
 * @property-read \App\Models\User|null $user
 * @property int|null $i
 * @method static Builder|$this whereI($value)
 * @property int|null $child_photo
 * @method static Builder|$this whereChildPhoto($value)
 * @property float $top_up_sum
 * @method static Builder|$this whereTopUpSum($value)
 * @property string|null $payment_code
 * @method static Builder|$this wherePaymentCode($value)
 * @property int|null $payment_id
 * @method static Builder|$this wherePaymentId($value)
 *
 */
class Card extends Model
{
    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes = []);
        $this->table = 'cards';
        $this->fillable = [
            'i',
            'child_photo',
            'name',
            'surname',
            'school_id',
            'class',
            'card_1',
            'card_2',
            'card_3',
            'card_4',
            'card_5',
            'card_6',
            'card_7',
            'card_8',
            'card_9',
            'card_10',
            'pk_1',
            'pk_2',
            'pk_3',
            'pk_4',
            'pk_5',
            'pk_6',
            'pk_7',
            'pk_8',
            'pk_9',
            'pk_10',
            'pk_11',
            'limits',
            'amount',
            'top_up_sum',
            'user_id',
            'payment_code',
            'payment_id'
        ];
    }

    const VALIDATION_RULES = [
        'i' => 'integer|required',
        'name' => 'string|min:2|max:255|required',
        'surname' => 'string|min:2|max:255|required',
        'school_id' => 'integer|required',
        'user_id' => 'integer|required',
        'class' => 'string|min:2|max:255|required|regex:/^[0-9]{1,2}[.]?[a-z]{1,2}?$/',
        'card_1' => 'integer|required',
        'card_2' => 'integer|required',
        'card_3' => 'integer|required',
        'card_4' => 'integer|required',
        'card_5' => 'integer|required',
        'card_6' => 'integer|required',
        'card_7' => 'integer|required',
        'card_8' => 'integer|required',
        'card_9' => 'integer|required',
        'card_10' => 'integer|required',
        'pk_1' => 'integer|required',
        'pk_2' => 'integer|required',
        'pk_3' => 'integer|required',
        'pk_4' => 'integer|required',
        'pk_5' => 'integer|required',
        'pk_6' => 'integer|required',
        'pk_7' => 'integer|required',
        'pk_8' => 'integer|required',
        'pk_9' => 'integer|required',
        'pk_10' => 'integer|required',
        'pk_11' => 'integer|required',
        'limits' => 'string|min:3|max:255|required',
        'amount' => 'numeric|min:1|max:141|required',
        'child_photo' => 'integer'
    ];

    const SAVE_VALIDATION_RULES = [
        'i' => 'integer|required',
        'name' => 'string|min:2|max:255',
        'surname' => 'string|min:2|max:255',
        'school_id' => 'integer',
        'user_id' => 'integer|required',
        'class' => 'string|min:2|max:255|regex:/^[0-9]{1,2}[.]?[a-z]{1,2}?$/',
        'card_1' => 'integer',
        'card_2' => 'integer',
        'card_3' => 'integer',
        'card_4' => 'integer',
        'card_5' => 'integer',
        'card_6' => 'integer',
        'card_7' => 'integer',
        'card_8' => 'integer',
        'card_9' => 'integer',
        'card_10' => 'integer',
        'pk_1' => 'integer',
        'pk_2' => 'integer',
        'pk_3' => 'integer',
        'pk_4' => 'integer',
        'pk_5' => 'integer',
        'pk_6' => 'integer',
        'pk_7' => 'integer',
        'pk_8' => 'integer',
        'pk_9' => 'integer',
        'pk_10' => 'integer',
        'pk_11' => 'integer',
        'limits' => 'string|min:3|max:255',
        'amount' => 'numeric|min:1|max:255',
        'child_photo' => 'integer'
    ];

    const FILL_VALIDATION_RULES = [
        'top_up_sum' => 'numeric|min:1|max:255|required'
    ];

    const SAVE_FILL_VALIDATION_RULES = [
        'top_up_sum' => 'numeric|min:1|max:255'
    ];

    public function school()
    {
        return $this->belongsTo('App\Models\School');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getCardNumber()
    {
        return app('CardService')->getNumber($this);
    }
}
