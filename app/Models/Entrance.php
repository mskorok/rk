<?php

namespace App\Models;

use App\RecordsActivity;
use Carbon\Carbon;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Entrance
 *
 * @property int $id
 * @property int $object_id
 * @property string $pk
 * @property string $virziens
 * @property int $turnicet_id
 * @property string $datums
 * @property string $laiks
 * @property int $valid_id
 * @property string $status
 * @property string $eticket_nr
 * @property-read \App\Models\Eticket $Eticket
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Entrance obj($id)
 * @method static Builder|Entrance whereDatums($value)
 * @method static Builder|Entrance whereEticketNr($value)
 * @method static Builder|Entrance whereId($value)
 * @method static Builder|Entrance whereLaiks($value)
 * @method static Builder|Entrance whereObjectId($value)
 * @method static Builder|Entrance wherePk($value)
 * @method static Builder|Entrance whereStatus($value)
 * @method static Builder|Entrance whereTurnicetId($value)
 * @method static Builder|Entrance whereValidId($value)
 * @method static Builder|Entrance whereVirziens($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Entrance extends Model
{
    use RecordsActivity;

    protected $table = 'entrance_log';

    public $rules = array(
        'object_id' => 'required|integer',
        'pk' => 'required|size:12',
        'virziens' => 'size:1',
        'turnicet_id' => 'numeric',
        'datums' => 'date',
        'laiks' => '',
        'valid_id' => 'integer',
        'status' => '',
        'eticket_nr' => '',
    );

    public $timestamps = false;

    protected $guarded = array('id');

    /**
     * Scope - objekts
     */
    public function scopeObj($query, $id)
    {
        return $query->where('object_id', $id);
    }

    public function Eticket()
    {
        return $this->belongsTo(Eticket::class, 'pk', 'pk');
    }

    public function enter_laiks()
    {
        if ($this->enter === '0000-00-00 00:00:00') {
            return '-';
        }

        $d = $date = new Carbon($this->enter);
        return $d->toTimeString().' | '.$this->enter_turnicet_id;
    }

    public function exit_laiks()
    {
        if ($this->exit === '0000-00-00 00:00:00') {
            return '-';
        }

        $d = $date = new Carbon($this->exit);
        return $d->toTimeString().' | '.$this->exit_turnicet_id;
    }
}
