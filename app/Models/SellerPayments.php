<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\SellerPayments
 *
 * @property int $id
 * @property int $seller_id
 * @property int $transaction_id
 * @property int $amount
 * @property string $period_code
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|SellerPayments whereAmount($value)
 * @method static Builder|SellerPayments whereCreatedAt($value)
 * @method static Builder|SellerPayments whereId($value)
 * @method static Builder|SellerPayments wherePeriodCode($value)
 * @method static Builder|SellerPayments whereSellerId($value)
 * @method static Builder|SellerPayments whereStatus($value)
 * @method static Builder|SellerPayments whereTransactionId($value)
 * @method static Builder|SellerPayments whereUpdatedAt($value)
 * @method static Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class SellerPayments extends Model
{
    use RecordsActivity;

    protected $table = 'seller_payments';

    public $rules = [
        'seller_id' => 'required|integer',
        'transaction_id' => 'required|integer',
        'amount' => 'required|integer',
        'period_code' => 'required',
        'status' => 'required|integer|between:0,1',
    ];

    protected $guarded = ['id'];
}
