<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\TicketTypes
 *
 * @property int $id
 * @property string $name
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|TicketTypes whereId($value)
 * @method static Builder|TicketTypes whereName($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class TicketTypes extends Model
{
    use RecordsActivity;

    protected $table = 'tickets_types';

    public $timestamps = false;

    public $rules = [
        'name' => 'required',
    ];

    protected $guarded = ['id'];
}
