<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\AccountAccess
 *
 * @property int $id
 * @property int $user_id
 * @property int $account_id
 * @property string $access_status
 * @property string $access_level
 * @property string $starts_when
 * @property string $ends_when
 * @property string $comments
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \App\Models\Account $account
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess ao()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess u($user_id)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereAccessLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereAccessStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereAccountId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereComments($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereEndsWhen($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereStartsWhen($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 * @method bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess onlyTrashed()
 * @method bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AccountAccess withoutTrashed()
 */
class AccountAccess extends Model
{
    use RecordsActivity;
    use SoftDeletes;

    protected $table = 'account_access';

    protected $dates = ['deleted_at'];

    public $rules = [
        'user_id' => 'required',
        'account_id' => 'required',
        'access_status' => 'numeric|between:1,2',
        'access_level' => 'integer',
        'starts_when' => 'date',
        'ends_when' => 'date',
        'comments' => 'between:0,255',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    /**
     * Scope - Active & owner
     */
    public function scopeAo($query)
    {
        return $query->where('access_status', '=', 1)->where('access_level', '=', 1);
    }

    /**
     * Scope - user
     */
    public function scopeU($query, $user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }

    /**
     * Get the account
     *
     * @return Account
     */
    public function account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }

    /**
     * Get the user
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
