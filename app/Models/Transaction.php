<?php

namespace App\Models;

use App\RecordsActivity;
use Carbon\Carbon;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Transaction
 *
 * @property int $id
 * @property int $authorisator_id
 * @property string $local_id
 * @property int $mpos_id
 * @property int $operation_id
 * @property int $amount
 * @property int $from_account_id
 * @property int $to_account_id
 * @property string $from_eticket_nr
 * @property string $to_eticket_nr
 * @property int $object_id
 * @property string $comments
 * @property string $t_status
 * @property string $confirmed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $parent_id
 * @property int $dotation_id
 * @property string $seller_reg_nr
 * @property int $freemeals_type
 * @property string $err_code
 * @property string $err_text
 * @property string $sent_to_api
 * @property-read \App\Models\Eticket $fromEt
 * @property-read \App\Models\Account $from_account
 * @property-read \App\Models\Eticket $frome
 * @property-read string $encrypted
 * @property-read \App\Models\Mpos $mpos
 * @property-read \App\Models\Objekts $object
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @property-read \App\Models\Eticket $toEt
 * @property-read \App\Models\Account $to_account
 * @method static Builder|Transaction acc($account_id)
 * @method static Builder|Transaction izm()
 * @method static Builder|Transaction nizMFree()
 * @method static Builder|Transaction nizm()
 * @method static Builder|Transaction obj($id)
 * @method static Builder|Transaction ok()
 * @method static Builder|Transaction set()
 * @method static Builder|Transaction whereAmount($value)
 * @method static Builder|Transaction whereAuthorisatorId($value)
 * @method static Builder|Transaction whereComments($value)
 * @method static Builder|Transaction whereConfirmed($value)
 * @method static Builder|Transaction whereCreatedAt($value)
 * @method static Builder|Transaction whereDotationId($value)
 * @method static Builder|Transaction whereErrCode($value)
 * @method static Builder|Transaction whereErrText($value)
 * @method static Builder|Transaction whereFreemealsType($value)
 * @method static Builder|Transaction whereFromAccountId($value)
 * @method static Builder|Transaction whereFromEticketNr($value)
 * @method static Builder|Transaction whereId($value)
 * @method static Builder|Transaction whereLocalId($value)
 * @method static Builder|Transaction whereMposId($value)
 * @method static Builder|Transaction whereObjectId($value)
 * @method static Builder|Transaction whereOperationId($value)
 * @method static Builder|Transaction whereParentId($value)
 * @method static Builder|Transaction whereSellerRegNr($value)
 * @method static Builder|Transaction whereSentToApi($value)
 * @method static Builder|Transaction whereTStatus($value)
 * @method static Builder|Transaction whereToAccountId($value)
 * @method static Builder|Transaction whereToEticketNr($value)
 * @method static Builder|Transaction whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Transaction extends Model
{
    use RecordsActivity;

    protected $table = 'transactions';

    public $rules = [
        'authorisator_id' => 'integer',
        'local_id' => 'integer',
        'mpos_id' => 'integer',
        'operation_id' => 'required|integer',
        'amount' => 'required|integer',
        'eticket_id' => 'integer',
        'from_account_id' => 'required|integer',
        'to_account_id' => 'required|integer',
        'from_eticket_nr' => 'alpha_num',
        'to_eticket_nr' => 'alpha_num',
        'object_id' => 'integer',
        'comments' => 'between:0,255',
        't_status' => 'size:1',
        'confirmed' => 'size:1'
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    public function scopeOk($query)
    {
        return $query->where('from_account_id', '>=', 0)
            ->where('to_account_id', '>=', 0)
            ->where('t_status', '!=', 'F')
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('local_id', 'NOT LIKE', '%-p');
                })
                ->orWhere(function ($query) {
                    $query->where('local_id', 'LIKE', '%-p');
                    $query->where('amount', '>=', 0);
                });
            });
    }

    /**
     * Scope - objekts
     */
    public function scopeObj($query, $id)
    {
        return $query->where('object_id', $id);
    }


    /**
     * Scope - account_id
     */
    public function scopeAcc($query, $account_id)
    {
        return $query->where('from_account_id', '=', $account_id)->orWhere('to_account_id', '=', $account_id);
    }

    /**
     * Scope - dotations
     */
    public function scopeSet($query)
    {
        return $query->where('comments', 'LIKE', 'SET:%');
    }

    /**
     * Scope - nepabeigta izmaksa
     */
    public function scopeNizm($query)
    {
        return $query->where('confirmed', '=', 0)->where('to_account_id', '=', 999999);
    }

    /**
     * Scope - pabeigta izmaksa
     */
    public function scopeIzm($query)
    {
        return $query->where('confirmed', '=', 1)->where('to_account_id', '=', 999999);
    }

    /**
     * Scope - neparskaititas brivpusdienas
     */
    public function scopeNizMFree($query)
    {
        return $query->where('created_at', 'LIKE', date("Y").'-'.date("m").'-%')
            ->where('from_account_id', '=', 2);
    }

    /**
     * Get from account
     *
     * @return Account
     */
    public function from_account()
    {
        return $this->belongsTo('App\Models\Account', 'from_account_id');
    }

    /**
     * Get to account
     *
     * @return Account
     */
    public function to_account()
    {
        return $this->belongsTo('App\Models\Account', 'to_account_id');
    }

    public function mpos()
    {
        return $this->belongsTo('App\Models\Mpos', 'mpos_id');
    }

    public function object()
    {
        return $this->belongsTo('App\Models\Objekts', 'object_id');
    }

    /**
     * Get from e-ticket
     *
     * @return Eticket
     */
    public function fromEt()
    {
        return $this->hasOne('App\Models\Eticket', 'name')->orWhere('nr', $this->from_eticket_nr);
    }

    /**
     * Get full from e-ticket row
     *
     * @return Eticket
     */
    public function frome()
    {
        return $this->hasOne('App\Models\Eticket', 'nr', 'from_eticket_nr');
    }

    /**
     * Get to e-ticket
     *
     * @return Eticket
     */
    public function toEt()
    {
        return $this->hasOne('App\Models\Eticket', 'pk')->orWhere('nr', $this->to_eticket_nr);
    }

    /**
     * @return string
     */
    public function created_date()
    {
        $d = new Carbon($this->created_at);
        return $d->toDateString();
    }

    public function humanAmount()
    {
        return sprintf("%.2f", $this->amount/100).' EUR';
    }
}
