<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\UserComments
 *
 * @property int $id
 * @property string $pk
 * @property int $last_editor_id
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\User $editor
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|UserComments whereCreatedAt($value)
 * @method static Builder|UserComments whereData($value)
 * @method static Builder|UserComments whereId($value)
 * @method static Builder|UserComments whereLastEditorId($value)
 * @method static Builder|UserComments wherePk($value)
 * @method static Builder|UserComments whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class UserComments extends Model
{
    use RecordsActivity;

    protected $table = 'users_comments';

    public $rules = [
        'pk' => 'required|unique:users_comments',
        'last_editor_id' => 'integer|required',
        'data' => '',
    ];

    protected $guarded = ['id'];

    public function editor()
    {
        return $this->hasOne('App\Models\User', 'last_editor_id');
    }
}
