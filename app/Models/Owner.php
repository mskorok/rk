<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Aloko\Elasticquent\ElasticquentTrait;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Owner
 *
 * @property int $id
 * @property string $owner_type
 * @property string $owner_name
 * @property string $reg_nr
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Owner sell()
 * @method static Builder|Owner u()
 * @method static Builder|Owner whereCreatedAt($value)
 * @method static Builder|Owner whereId($value)
 * @method static Builder|Owner whereOwnerName($value)
 * @method static Builder|Owner whereOwnerType($value)
 * @method static Builder|Owner whereRegNr($value)
 * @method static Builder|Owner whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Owner extends Model
{
    use RecordsActivity;
    use ElasticquentTrait;

    public $fillable = ['owner_type', 'owner_name'];

    private $mapProps = [
        'owner_name' => [
            'type' => 'string',
            "analyzer" => "standard",
        ]
    ];

    private $customAnalyzer = [
        'myAnalyzer' => [
            'type' => 'stop',
            "stopwords" => [","]
        ]
    ];

    public static function createIndexWithCustomAnalyzer($shards = null, $replicas = null)
    {
        $instance = new static;

        $client = $instance->getElasticSearchClient();

        $params = [
            'index' => 'default',
            'body' => [
                'settings' => [
                    'number_of_shards' => $shards,
                    'number_of_replicas' => $replicas,
                    'analysis' => [
                        'analyzer' => $instance->customAnalyzer,
                    ]
                ],
                'mappings' => [
                    'owners' => [
                        'properties' => $instance->mapProps,
                    ],
                ]
            ]
        ];

        return $client->indices()->create($params);
    }

    protected $table = 'owners';

    public $rules = [
        'owner_name' => 'required|min:2',
        'owner_type' => 'numeric|between:1,4',
        'reg_nr' => 'alpha_num'
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    /**
     * Scope - seller
     */
    public function scopeSell($query)
    {
        return $query->where('owner_type', 3);
    }

    /**
     * Scope - user
     */
    public function scopeU($query)
    {
        return $query->where('owner_type', 1);
    }
}
