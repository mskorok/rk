<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\EticketsObjekti
 *
 * @property int $id
 * @property int $eticket_id
 * @property int $object_id
 * @property int $pirma
 * @property-read \App\Models\Eticket $eticket
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|EticketsObjekti et($id)
 * @method static Builder|EticketsObjekti etPrim($id)
 * @method static Builder|EticketsObjekti obj($id)
 * @method static Builder|EticketsObjekti whereEticketId($value)
 * @method static Builder|EticketsObjekti whereId($value)
 * @method static Builder|EticketsObjekti whereObjectId($value)
 * @method static Builder|EticketsObjekti wherePirma($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class EticketsObjekti extends Model
{
    use RecordsActivity;

    protected $table = 'eticket_objects';

    public $rules = [
        'eticket_id' => 'integer',
        'object_id' => 'integer',
        'pirma' => 'between:0,1',
    ];

    protected $fillable = ['eticket_id', 'object_id', 'pirma'];

    public $timestamps = false;

    /**
     * Scope - e-ticket
     */
    public function scopeEt($query, $id)
    {
        return $query->where('eticket_id', $id);
    }

    /**
     * Scope - objekts
     */
    public function scopeObj($query, $id)
    {
        return $query->where('object_id', $id);
    }

    public function scopeEtPrim($query, $id)
    {
        return $query->where('eticket_id', $id)->where('pirma', 1);
    }

    /**
     * E-tickets
     *
     * @return Eticket
     */
    public function eticket()
    {
        return $this->belongsTo('App\Models\Eticket', 'eticket_id');
    }
}
