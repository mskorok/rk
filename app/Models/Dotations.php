<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Dotations
 *
 * @property int $id
 * @property string $pk
 * @property int $dotation_id
 * @property string $last_used
 * @property string $from_date
 * @property string $until_date
 * @property int $avail
 * @property string $import_date
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Dotations whereAvail($value)
 * @method static Builder|Dotations whereDotationId($value)
 * @method static Builder|Dotations whereFromDate($value)
 * @method static Builder|Dotations whereId($value)
 * @method static Builder|Dotations whereImportDate($value)
 * @method static Builder|Dotations whereLastUsed($value)
 * @method static Builder|Dotations wherePk($value)
 * @method static Builder|Dotations whereUntilDate($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Dotations extends Model
{
    use RecordsActivity;

    protected $table = 'dotations';

    public $rules = array(
        'pk' => 'required:size:12',
        'dotation_id' => 'required|integer',
        'last_used' => '',
        'from_date' => 'date',
        'until_date' => 'date',
        'import_date' => 'date',
    );

    protected $guarded = array('id');
    public $timestamps = false;

    public $autoHydrateEntityFromInput = true;
}
