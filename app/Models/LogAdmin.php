<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\LogAdmin
 *
 * @property int $id
 * @property int $user_id
 * @property string $op
 * @property string $value
 * @property string $ip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @property-read \App\Models\User $user
 * @method static Builder|LogAdmin whereCreatedAt($value)
 * @method static Builder|LogAdmin whereId($value)
 * @method static Builder|LogAdmin whereIp($value)
 * @method static Builder|LogAdmin whereOp($value)
 * @method static Builder|LogAdmin whereUpdatedAt($value)
 * @method static Builder|LogAdmin whereUserId($value)
 * @method static Builder|LogAdmin whereValue($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class LogAdmin extends Model
{
    use RecordsActivity;

    protected $table = 'log_admin';

    public $rules = [
        'user_id' => 'required|integer',
        'op' => 'between:1,100',
        'value' => 'between:0,255',
        'ip' => 'ip',
    ];

    protected $guarded = ['id'];

    /**
     * Get the user
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
