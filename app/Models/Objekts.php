<?php

namespace App\Models;

use App\RecordsActivity;
use Carbon\Carbon;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Objekts
 *
 * @property int $id
 * @property int $owner_id
 * @property string $object_type
 * @property string $object_name
 * @property string $object_country
 * @property string $object_city
 * @property string $object_address
 * @property string $object_ip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $allow_d
 * @property string $age
 * @property int $kods
 * @property int $show
 * @property string $grades
 * @property string $entrance_ip
 * @property string $sync_path
 * @property int $sync_enable
 * @property string $object_regnr
 * @property int $vat
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ObjectDotationHistory[] $dotationHistoryTwoDays
 * @property-read string $encrypted
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ObjGroup[] $ogroups
 * @property-read \App\Models\Owner $owner
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Objekts whereAge($value)
 * @method static Builder|Objekts whereAllowD($value)
 * @method static Builder|Objekts whereCreatedAt($value)
 * @method static Builder|Objekts whereEntranceIp($value)
 * @method static Builder|Objekts whereGrades($value)
 * @method static Builder|Objekts whereId($value)
 * @method static Builder|Objekts whereKods($value)
 * @method static Builder|Objekts whereObjectAddress($value)
 * @method static Builder|Objekts whereObjectCity($value)
 * @method static Builder|Objekts whereObjectCountry($value)
 * @method static Builder|Objekts whereObjectIp($value)
 * @method static Builder|Objekts whereObjectName($value)
 * @method static Builder|Objekts whereObjectRegnr($value)
 * @method static Builder|Objekts whereObjectType($value)
 * @method static Builder|Objekts whereOwnerId($value)
 * @method static Builder|Objekts whereShow($value)
 * @method static Builder|Objekts whereSyncEnable($value)
 * @method static Builder|Objekts whereSyncPath($value)
 * @method static Builder|Objekts whereUpdatedAt($value)
 * @method static Builder|Objekts whereVat($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Objekts extends Model
{
    use RecordsActivity;

    protected $table = 'objects';

    public $rules = [
        'owner_id' => 'integer',
        'object_name' => 'required|min:2',
        'object_type' => 'numeric|between:1,2',
        'object_country' => 'min:2',
        'object_city' => 'min:2',
        'object_address' => 'min:2',
        'object_ip' => '',
        'allow_d' => 'integer',
        'age' => 'between:0,99',
        'kods' => 'integer',
        'grades' => '',
        'entrance_ip' => 'ip',
        'sync_path' => 'between:0,255',
        'sync_enable' => 'size:1',
        'object_regnr' => '',
        'vat' => 'integer|between:0,1',
    ];

    protected $fillable = [
        'owner_id' => 'integer',
        'object_name',
        'object_type',
        'object_country',
        'object_city',
        'object_address',
        'object_ip',
        'allow_d',
        'age',
        'kods',
        'entrance_ip',
        'sync_path',
        'sync_enable',
        'object_regnr',
        'vat',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    /**
     * Get the owner
     *
     * @return Owner
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\Owner', 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ogroups()
    {
        return $this->belongsToMany('App\Models\Objgroup')->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function dotationHistoryTwoDays()
    {
        $sodiena = Carbon::today();
        $vakardiena = Carbon::yesterday();

        return $this->hasMany('App\Models\ObjectDotationHistory', "object_id")
            ->where('object_dotation_history.datums', $sodiena->toDateString())
            ->orWhere('object_dotation_history.datums', $vakardiena->toDateString());
    }
}
