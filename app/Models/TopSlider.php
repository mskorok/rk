<?php

namespace App\Models;

use App\Http\Traits\Uploadable;
use Esensi\Model\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\TopSlider
 *
 * @property int $id
 * @property string $name
 * @property int $sort
 * @property int $active
 * @property string $path
 * @property string|null $path_sm
 * @property string|null $path_xs
 * @property \Carbon\Carbon|null $exposition_time_to
 * @property \Carbon\Carbon|null $exposition_time_from
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|$this whereActive($value)
 * @method static Builder|$this whereCreatedAt($value)
 * @method static Builder|$this whereExpositionTimeFrom($value)
 * @method static Builder|$this whereExpositionTimeTo($value)
 * @method static Builder|$this whereId($value)
 * @method static Builder|$this whereName($value)
 * @method static Builder|$this wherePath($value)
 * @method static Builder|$this wherePathSm($value)
 * @method static Builder|$this wherePathXs($value)
 * @method static Builder|$this whereSort($value)
 * @method static Builder|$this whereUpdatedAt($value)
 * @method static Builder|Model without($relations)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider whereExpositionTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider whereExpositionTimeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider wherePathSm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider wherePathXs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TopSlider whereUpdatedAt($value)
 */
class TopSlider extends Model
{
    use Uploadable;

    const VALIDATION_RULES = [
        'name' => 'string|min:3|max:255|required',
        'sort' => 'numeric',
        'path' => 'image|mimes:jpg,png,jpeg|required',
        'path_sm' => 'image|mimes:jpg,png,jpeg',
        'path_xs' => 'image|mimes:jpg,png,jpeg',
        'exposition_time_to' => 'date',
        'exposition_time_from' => 'date'
    ];

    protected $file_columns;

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes = []);
        $this->table = 'top_slider';
        $this->guarded = ['id'];
        $this->file_columns = [
            'path',
            'path_sm',
            'path_xs'
        ];
        $this->dates = ['exposition_time_to', 'exposition_time_from'];
    }
}
