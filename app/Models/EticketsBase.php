<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\EticketsBase
 *
 * @property int $id
 * @property string $pk
 * @property string $etickets
 * @property int $custom
 * @property string $name
 * @property string $surname
 * @property string $grade
 * @property int $object_id
 * @property int $brivpusdienas
 * @property int $school_id
 * @property string $free_meals_from
 * @property string $free_meals_until
 * @property string $second_meals_from
 * @property string $second_meals_until
 * @property string $third_meals_from
 * @property string $third_meals_until
 * @property string $kopgalds_from
 * @property string $kopgalds_until
 * @property-read string $encrypted
 * @property-read \App\Models\Objekts $objekts
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|EticketsBase confirmed()
 * @method static Builder|EticketsBase id($id)
 * @method static Builder|EticketsBase nC()
 * @method static Builder|EticketsBase pk($nr, $pk)
 * @method static Builder|EticketsBase whereBrivpusdienas($value)
 * @method static Builder|EticketsBase whereCustom($value)
 * @method static Builder|EticketsBase whereEtickets($value)
 * @method static Builder|EticketsBase whereFreeMealsFrom($value)
 * @method static Builder|EticketsBase whereFreeMealsUntil($value)
 * @method static Builder|EticketsBase whereGrade($value)
 * @method static Builder|EticketsBase whereId($value)
 * @method static Builder|EticketsBase whereKopgaldsFrom($value)
 * @method static Builder|EticketsBase whereKopgaldsUntil($value)
 * @method static Builder|EticketsBase whereName($value)
 * @method static Builder|EticketsBase whereObjectId($value)
 * @method static Builder|EticketsBase wherePk($value)
 * @method static Builder|EticketsBase whereSchoolId($value)
 * @method static Builder|EticketsBase whereSecondMealsFrom($value)
 * @method static Builder|EticketsBase whereSecondMealsUntil($value)
 * @method static Builder|EticketsBase whereSurname($value)
 * @method static Builder|EticketsBase whereThirdMealsFrom($value)
 * @method static Builder|EticketsBase whereThirdMealsUntil($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class EticketsBase extends Model
{
    use RecordsActivity;

    protected $table = 'etickets_base';

    public $timestamps = false;

    public $autoPurgeRedundantAttributes = true;

    public $rules = [
        'pk' => 'required',
        'etickets' => '',
        'custom' => 'size:1',
        'name' => '',
        'surname' => '',
        'grade' => '',
        'object_id' => 'integer',
        'school_id' => 'integer',
        'brivpusdienas' => 'integer|between:0,1',
        'free_meals_from' => '',
        'free_meals_until' => '',
        'second_meals_from' => '',
        'second_meals_until' => '',
        'third_meals_from' => '',
        'third_meals_until' => '',
        'kopgalds_from' => '',
        'kopgalds_until' => ''
    ];

    protected $guarded = ['id'];

    /**
     * Scope - PK
     */
    public function scopePk($query, $nr, $pk)
    {
        return $query->where('etickets', 'like', "%$nr%")->where('pk', $pk);
    }

    /**
     * Scope - NotCustom
     */
    public function scopeNC($query)
    {
        return $query->where('custom', "0");
    }

    public function scopeId($query, $id)
    {
        return $query->where('id', $id);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('custom', '<=', 1);
    }

    public function objekts()
    {
        return $this->belongsTo('App\Models\Objekts', 'object_id');
    }
}