<?php

namespace App\Models;

use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\DailyLogRk
 *
 * @property int $id
 * @property string $datums
 * @property string $pk
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $nr
 * @property string $expires
 * @property bool $blacklisted
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|DailyLogRk whereBlacklisted($value)
 * @method static Builder|DailyLogRk whereCreatedAt($value)
 * @method static Builder|DailyLogRk whereData($value)
 * @method static Builder|DailyLogRk whereDatums($value)
 * @method static Builder|DailyLogRk whereExpires($value)
 * @method static Builder|DailyLogRk whereId($value)
 * @method static Builder|DailyLogRk whereNr($value)
 * @method static Builder|DailyLogRk wherePk($value)
 * @method static Builder|DailyLogRk whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class DailyLogRk extends Model
{
    protected $table = 'daily_log_rk';

    public $rules = [
        'datums' => 'required',
        'pk' => 'required',
        'data' => 'required'
    ];

    public $timestamps = true;

    protected $guarded = ['id'];
}
