<?php

namespace App\Models;

use App\Http\Traits\Uploadable;
use Esensi\Model\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Banner
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $sort
 * @property string $size
 * @property string|null $path
 * @property string|null $path_sm
 * @property string|null $path_xs
 * @property string|null $code
 * @property \Carbon\Carbon|null $exposition_time_to
 * @property \Carbon\Carbon|null $exposition_time_from
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|$this whereCode($value)
 * @method static Builder|$this whereCreatedAt($value)
 * @method static Builder|$this whereExpositionTimeFrom($value)
 * @method static Builder|$this whereExpositionTimeTo($value)
 * @method static Builder|$this whereId($value)
 * @method static Builder|$this whereName($value)
 * @method static Builder|$this wherePath($value)
 * @method static Builder|$this wherePathSm($value)
 * @method static Builder|$this wherePathXs($value)
 * @method static Builder|$this whereSize($value)
 * @method static Builder|$this whereSort($value)
 * @method static Builder|$this whereType($value)
 * @method static Builder|$this whereUpdatedAt($value)
 * @method static Builder|Model without($relations)
 * @mixin \Eloquent
 * @property int $active
 * @method static Builder|$this whereActive($value)
 * @property string|null $link
 *
 */
class Banner extends Model
{
    use Uploadable;

    const VALIDATION_RULES = [
        'name' => 'string|min:3|max:255|required',
        'type' => 'in:image,flash,html,ajax_html,xml|required',
        'sort' => 'numeric',
        'size' => 'in:1140 х 250,555 х 160,360 х 160,262 х 160|required',
        'path' => 'image|mimes:jpg,png,jpeg|required',
        'path_sm' => 'image|mimes:jpg,png,jpeg',
        'path_xs' => 'image|mimes:jpg,png,jpeg',
        'link' => 'string',
        'code' => 'string',
        'exposition_time_to' => 'date',
        'exposition_time_from' => 'date'
    ];

    const EDIT_VALIDATION_RULES = [
        'name' => 'string|min:3|max:255|required',
        'type' => 'in:image,flash,html,ajax_html,xml|required',
        'sort' => 'numeric',
        'size' => 'in:1140 х 250,555 х 160,360 х 160,262 х 160|required',
        'path' => 'image|mimes:jpg,png,jpeg',
        'path_sm' => 'image|mimes:jpg,png,jpeg',
        'path_xs' => 'image|mimes:jpg,png,jpeg',
        'code' => 'string',
        'exposition_time_to' => 'date',
        'exposition_time_from' => 'date'
    ];


    protected $file_columns;

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes = []);
        $this->table = 'banners';
        $this->guarded = ['id'];
        $this->file_columns = [
            'path',
            'path_sm',
            'path_xs'
        ];
        $this->dates = ['exposition_time_to', 'exposition_time_from'];
    }
}
