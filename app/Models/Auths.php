<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use App\Models\Owner;

/**
 * App\Models\Auths
 *
 * @property int $id
 * @property int $object_id
 * @property string $authorisator_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $secret_key
 * @property int $sync_session
 * @property string $last_sync
 * @property string $last_sync_full
 * @property-read string $encrypted
 * @property-read \App\Models\Owner $owner
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereAuthorisatorName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereLastSync($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereLastSyncFull($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereObjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereSecretKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereSyncSession($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Auths whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class Auths extends Model
{
    use RecordsActivity;

    protected $table = 'authorisators';

    public $rules = [
        'authorisator_name' => 'required|min:2',
        'object_id' => 'integer',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    /**
     * Get the owner
     *
     * @return Owner
     */
    public function owner()
    {
        return $this->belongsTo(Owner::class, 'owner_id');
    }
}
