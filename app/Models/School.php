<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\School
 *
 * @property int $id
 * @property string $heading
 * @property string $reg_nr
 * @property string $changed
 * @property int $status
 * @property float $equal_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-read \App\Models\Objekts $objekts
 * @property-read \Illuminate\Database\Eloquent\Collection|SchoolPrices[] $prices
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|School whereChanged($value)
 * @method static Builder|School whereCreatedAt($value)
 * @method static Builder|School whereEqualPrice($value)
 * @method static Builder|School whereHeading($value)
 * @method static Builder|School whereId($value)
 * @method static Builder|School whereRegNr($value)
 * @method static Builder|School whereStatus($value)
 * @method static Builder|School whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class School extends Model
{
    use RecordsActivity;

    protected $table = 'schools';

    public $rules = [
        'id' => 'integer|unique:schools',
        'heading' => 'required',
        'reg_nr' => '',
        'changed' => '',
        'status' => 'integer',
        'equal_price' => '',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    public function objekts()
    {
        return $this->hasOne('App\Models\Objekts', 'object_regnr', 'reg_nr');
    }

    public function prices()
    {
        return $this->hasMany('App\Models\SchoolPrices', 'school_id', 'id');
    }
}
