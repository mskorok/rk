<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Log;
use LaravelArdent\Ardent\Ardent;
use Vinkla\Hashids\Facades\Hashids;

/**
 * App\Models\Eticket
 *
 * @property int $id
 * @property string $nr
 * @property int $account_id
 * @property string $eticket_status
 * @property string $pin
 * @property int $balance
 * @property int $limit_type
 * @property int $limit
 * @property int $turnover
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $sync_state
 * @property int $group_id
 * @property string $pk
 * @property string $name
 * @property int $thumb_id
 * @property string $limit_at
 * @property string $grade
 * @property string $free_meals_from
 * @property string $free_meals_until
 * @property string $second_meals_from
 * @property string $second_meals_until
 * @property string $third_meals_from
 * @property string $third_meals_until
 * @property string $kopgalds_from
 * @property string $kopgalds_until
 * @property-read \App\Models\Account $account
 * @property-read string $encrypted
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Objekts[] $objekti
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Objekts[] $objekti_grouped
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|Eticket a()
 * @method static Builder|Eticket group($id)
 * @method static Builder|Eticket nr($nr)
 * @method static Builder|Eticket whereAccountId($value)
 * @method static Builder|Eticket whereBalance($value)
 * @method static Builder|Eticket whereCreatedAt($value)
 * @method static Builder|Eticket whereEticketStatus($value)
 * @method static Builder|Eticket whereFreeMealsFrom($value)
 * @method static Builder|Eticket whereFreeMealsUntil($value)
 * @method static Builder|Eticket whereGrade($value)
 * @method static Builder|Eticket whereGroupId($value)
 * @method static Builder|Eticket whereId($value)
 * @method static Builder|Eticket whereKopgaldsFrom($value)
 * @method static Builder|Eticket whereKopgaldsUntil($value)
 * @method static Builder|Eticket whereLimit($value)
 * @method static Builder|Eticket whereLimitAt($value)
 * @method static Builder|Eticket whereLimitType($value)
 * @method static Builder|Eticket whereName($value)
 * @method static Builder|Eticket whereNr($value)
 * @method static Builder|Eticket wherePin($value)
 * @method static Builder|Eticket wherePk($value)
 * @method static Builder|Eticket whereSecondMealsFrom($value)
 * @method static Builder|Eticket whereSecondMealsUntil($value)
 * @method static Builder|Eticket whereSyncState($value)
 * @method static Builder|Eticket whereThirdMealsFrom($value)
 * @method static Builder|Eticket whereThirdMealsUntil($value)
 * @method static Builder|Eticket whereThumbId($value)
 * @method static Builder|Eticket whereTurnover($value)
 * @method static Builder|Eticket whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 * @property int|null $school_id
 * @method static Builder|$this whereSchoolId($value)
 */
class Eticket extends Model
{
    use RecordsActivity;

    protected $table = 'etickets';

    public $autoPurgeRedundantAttributes = true;

    public $rules = [
        'nr' => 'required|unique:etickets|size:10',
        'account_id' => 'integer',
        'eticket_status' => 'min:1',
        'pin' => 'size:4|regex:/(\d){4}/',
        'balance' => 'min:1',
        'limit_type' => 'integer',
        'limit' => 'numeric|min:0',
        'turnover' => 'integer',
        'group_id' => 'integer',
        'pk' => 'required|regex:/(\d){6}-(\d){5}/',
        'name' => 'required',
        //'age' => 'between:2,3',
        'thumb_id' => 'integer',
        'free_meals_from' => '',
        'free_meals_until' => '',
        'grade' => '',
    ];

    public static $customMessages = [
        'unique' => 'Šāds :attribute jau ir reģistrēts!',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    /**
     * Find a model by its primary key.
     *
     * Edgars 2015
     * overwrite to dehash the ID
     *
     * @param  mixed $id
     * @param  array $columns
     * @return Ardent|Collection
     */
    public static function find($id, $columns = ['*'])
    {
        $debug = debug_backtrace(false);

        // dehash the id
        $arr = Hashids::decode($id);
        if (count($arr) === 1) {
            $id = $arr[0];
        }

        if ($debug[1]['function'] !== 'findOrFail') {
            return self::findOrFail($id, $columns);
        } else {
            return parent::find($id, $columns);
        }
    }

    /**
     * Scope - nr
     */
    public function scopeNr($query, $nr)
    {
        return $query->where('nr', '=', $nr);
    }

    /**
     * Scope - group
     */
    public function scopeGroup($query, $id)
    {
        return $query->where('group_id', '=', $id);
    }

    /**
     * Scope - active
     */
    public function scopeA($query)
    {
        return $query->where('eticket_status', 1);
    }

    /**
     * Get the account
     *
     * @return Account
     */
    public function account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }

    /**
     * Objekti
     *
     * @return Objekts
     */
    public function getObjekti()
    {
        return $this->hasMany('App\Models\EticketsObjekti', 'eticket_id');
    }

    /**
     * Returns selected objects
     * @return array|bool
     */
    public function activeObjekti()
    {
        $d = $this->getObjekti;
        $rd = false;
        if (!empty($d)) {
            $rd = [];
            foreach ($d as &$dd) {
                $rd[] = $dd->object_id;
            }
        }
        return $rd;
    }

    /**
     * Remove objects, return state
     *
     * @return boolean
     */
    public function DetachObjekti()
    {
        EticketsObjekti::Et($this->id)->delete();

        return true;
    }

    public function objekti()
    {
        return $this->belongsToMany('App\Models\Objekts', 'eticket_objects', 'eticket_id', 'object_id');
    }

    public function objekti_grouped()
    {
        return $this->belongsToMany('App\Models\Objekts', 'eticket_objects', 'eticket_id', 'object_id')
            ->where("pirma", 0);
    }

    public function pirmais_objekts()
    {
        foreach ($this->getObjekti as $o) {
            if ($o->pirma === 1) {
                return Objekts::find($o->object_id);
            }
        }
        return '-';
    }

    /**
     * Save objects inputted from multiselect
     * @param $input
     */
    public function saveObjekti($input, $adddef, $pirma = 1)
    {
        if ($adddef === 0) {
            if (!empty($input)) {
                $this->objekti()->sync($input);
            } else {
                $this->DetachObjekti();
            }
        } else {
            foreach ($input as $object_id) {
                $eto = new EticketsObjekti;
                $eto->eticket_id = $this->id;
                $eto->object_id = $object_id;
                $eto->pirma = $pirma;
                if (!$eto->save()) {
                    Log::error($eto->getErrors());
                }
            }
        }
    }

    public function humanLimit()
    {
        return sprintf("%.2f", $this->limit/100);
    }

    /**
     * Returns the balance in human format
     *
     * @return string
     */
    public function humanBalance()
    {
        return sprintf("%.2f", $this->balance/100);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($eticket) {
            $limit = $eticket->limit*100;
            $eticket->limit = (int) $limit;
            $eticket->eticket_status = 1;
        });

        static::saved(function ($eticket) {
            if ($eticket->id > 0) {
                EticketsSync::where('eticket_id', $eticket->id)->delete();

                $obj = $eticket->objekti;
                if (count($obj) > 0) {
                    foreach ($obj as $o) {
                        $auths = Auths::where('object_id', $o->id)
                            ->lists('id')->toArray();

                        foreach ($auths as $a) {
                            // create new record for changed e-ticket, for e-ticket sync
                            $log = new EticketsSync;
                            $log->eticket_id = $eticket->id;
                            $log->authorisator_id = $a;
                            if (!$log->save()) {
                                Log::error('error creating eticket sync entry - '.$log->getErrors());
                            }
                        }
                    }
                }
            }
        });
    }
}
