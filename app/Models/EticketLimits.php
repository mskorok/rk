<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\EticketLimits
 *
 * @property int $id
 * @property string $name
 * @property int $days
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|EticketLimits whereDays($value)
 * @method static Builder|EticketLimits whereId($value)
 * @method static Builder|EticketLimits whereName($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class EticketLimits extends Model
{
    use RecordsActivity;

    protected $table = 'eticket_limit_types';

    public $timestamps = false;

    public $rules = [
        'name' => 'required',
        'days' => 'required|integer',
    ];

    protected $guarded = ['id'];
}
