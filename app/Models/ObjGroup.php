<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\ObjGroup
 *
 * @property int $id
 * @property string $ogroup_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GroupObjekts[] $objects
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|ObjGroup whereCreatedAt($value)
 * @method static Builder|ObjGroup whereId($value)
 * @method static Builder|ObjGroup whereOgroupName($value)
 * @method static Builder|ObjGroup whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class ObjGroup extends Model
{
    use RecordsActivity;

    protected $table = 'ogroups';

    public $rules = [
        'ogroup_name' => 'required|between:2,255|unique:ogroups',
    ];

    public function objects()
    {
        return $this->hasMany('App\Models\GroupObjekts', 'ogroup_id');
    }
}
