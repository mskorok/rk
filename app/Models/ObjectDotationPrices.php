<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\ObjectDotationPrices
 *
 * @property int $id
 * @property string $heading
 * @property int $object_id
 * @property int $price
 * @property int $seller_account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $dotation_type
 * @property string $ex_code
 * @property int $importer_id
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|ObjectDotationPrices whereCreatedAt($value)
 * @method static Builder|ObjectDotationPrices whereDotationType($value)
 * @method static Builder|ObjectDotationPrices whereExCode($value)
 * @method static Builder|ObjectDotationPrices whereHeading($value)
 * @method static Builder|ObjectDotationPrices whereId($value)
 * @method static Builder|ObjectDotationPrices whereImporterId($value)
 * @method static Builder|ObjectDotationPrices whereObjectId($value)
 * @method static Builder|ObjectDotationPrices wherePrice($value)
 * @method static Builder|ObjectDotationPrices whereSellerAccountId($value)
 * @method static Builder|ObjectDotationPrices whereUpdatedAt($value)
 * @method static Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 */
class ObjectDotationPrices extends Model
{
    use RecordsActivity;

    protected $table = 'object_dotation_prices';

    public $rules = [
        'heading' => 'required|min:3',
        'object_id' => 'integer|required',
        'price' => 'integer|required',
        'seller_account_id' => 'integer|required', // account FROM which money is transferred (probably 2)
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;
}
