<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

/**
 * App\Models\Ticket
 *
 * @property int $id
 * @property int $user_id
 * @property int $recipient_id
 * @property int $owner_id
 * @property int $mpos_id
 * @property int $object_id
 * @property string $ticket_status
 * @property string $ticket_type
 * @property string $completed_when
 * @property string $content
 * @property string $eticket_nr
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @property-read TicketTypes $type
 * @method static Builder|Ticket u($id)
 * @method static Builder|Ticket whereCompletedWhen($value)
 * @method static Builder|Ticket whereContent($value)
 * @method static Builder|Ticket whereCreatedAt($value)
 * @method static Builder|Ticket whereEticketNr($value)
 * @method static Builder|Ticket whereId($value)
 * @method static Builder|Ticket whereMposId($value)
 * @method static Builder|Ticket whereObjectId($value)
 * @method static Builder|Ticket whereOwnerId($value)
 * @method static Builder|Ticket whereRecipientId($value)
 * @method static Builder|Ticket whereTicketStatus($value)
 * @method static Builder|Ticket whereTicketType($value)
 * @method static Builder|Ticket whereUpdatedAt($value)
 * @method static Builder|Ticket whereUserId($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class Ticket extends Model
{
    use RecordsActivity;

    protected $table = 'tickets';

    public $rules = [
        'user_id' => 'integer',
        'recipient_id' => 'integer',
        'owner_id' => 'integer',
        'mpos_id' => 'integer',
        'object_id' => 'integer',
        'ticket_status' => 'size:1',
        'ticket_type' => 'between:0,10',
        'completed_when' => 'date',
        'content' => 'between:0,255',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    /**
     * Scope - users messages, sent and received
     */
    public function scopeU($query, $id)
    {
        return $query->where('user_id', $id)->orWhere('recipient_id', $id);
    }

    /**
     * Get the ticket type
     *
     * @return TicketTypes
     */
    public function type()
    {
        return $this->belongsTo('App\Models\TicketTypes', 'ticket_type');
    }

    public static function boot()
    {
        parent::boot();

        static::saved(function ($ticket) {
            if ($ticket->recipient_id == 0) { // sent to admin
                if (App::environment() === 'production') {
                    Mail::send('emails.ticket', array(), function ($message) {
                        $message->to('olegs@telematika.lv', 'Administrators')
                            ->subject('Jauns paziņojums SKOLAS.RIGASKARTE.LV sistēmā.');
                    });
                }
            }
        });
    }
}
