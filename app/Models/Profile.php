<?php

namespace App\Models;

use App\Http\Traits\Uploadable;
use Esensi\Model\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Profile
 *
 * @property int $id
 * @property int $user_id
 * @property string $username
 * @property string $surname
 * @property string $pk
 * @property int|null $email
 * @property int|null $phone
 * @property string|null $address
 * @property string|null $citizenship
 * @property string|null $doc_type
 * @property string|null $doc_number
 * @property string|null $document_date
 * @property int $has_status
 * @property string|null $social_status
 * @property float|null $income_month
 * @property float|null $income_year
 * @property float|null $income_amount
 * @property float|null $outgoing_month
 * @property float|null $outgoing_year
 * @property float|null $outgoing_amount
 * @property int $is_manager
 * @property string|null $manager_status
 * @property int $is_owner
 * @property string|null $owner_status
 * @property int $is_accept_terms
 * @property string|null $doc_copy
 * @property string|null $doc_copy_path
 * @property string|null $doc_copy_image_id
 * @property string|null $avatar
 * @property string|null $avatar_path
 * @property string|null $avatar_image_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @property-read \App\Models\User $user
 * @method static Builder|$this whereAddress($value)
 * @method static Builder|$this whereAvatar($value)
 * @method static Builder|$this whereAvatarImageId($value)
 * @method static Builder|$this whereAvatarPath($value)
 * @method static Builder|$this whereCitizenship($value)
 * @method static Builder|$this whereCreatedAt($value)
 * @method static Builder|$this whereDocCopy($value)
 * @method static Builder|$this whereDocCopyImageId($value)
 * @method static Builder|$this whereDocCopyPath($value)
 * @method static Builder|$this whereDocNumber($value)
 * @method static Builder|$this whereDocType($value)
 * @method static Builder|$this whereDocumentDate($value)
 * @method static Builder|$this whereEmail($value)
 * @method static Builder|$this whereHasStatus($value)
 * @method static Builder|$this whereId($value)
 * @method static Builder|$this whereIncomeAmount($value)
 * @method static Builder|$this whereIncomeMonth($value)
 * @method static Builder|$this whereIncomeYear($value)
 * @method static Builder|$this whereIsAcceptTerms($value)
 * @method static Builder|$this whereIsManager($value)
 * @method static Builder|$this whereIsOwner($value)
 * @method static Builder|$this whereManagerStatus($value)
 * @method static Builder|$this whereOutgoingAmount($value)
 * @method static Builder|$this whereOutgoingMonth($value)
 * @method static Builder|$this whereOutgoingYear($value)
 * @method static Builder|$this whereOwnerStatus($value)
 * @method static Builder|$this wherePhone($value)
 * @method static Builder|$this wherePk($value)
 * @method static Builder|$this whereSocialStatus($value)
 * @method static Builder|$this whereSurname($value)
 * @method static Builder|$this whereUpdatedAt($value)
 * @method static Builder|$this whereUserId($value)
 * @method static Builder|$this whereUsername($value)
 * @method static Builder|Model without($relations)
 * @mixin \Eloquent
 * @property string $funds_origin
 * @property string $others_funds
 *
 */
class Profile extends Model
{
    use Uploadable;

    protected $file_columns = [
//        'avatar',
        'doc_copy'
    ];

    const VALIDATION_RULES = [
        'username' => 'string|min:3|max:255|required',
        'surname' => 'string|min:3|max:255|required',
        'email' => 'email|required|max:255',
        'phone' => 'numeric|min:8|required',
        'address' => 'string|min:3|max:255|required',
        'citizenship' => 'string|min:3|max:255|required',
        'doc_type' => 'string|min:3|max:255|required',
        'doc_number' => 'string|min:3|max:255|required',
        'document_date' => 'date_format:Y-m-d',
        'has_status' => 'boolean|required',
        'social_status' => 'string|min:3|max:255',
        'income_month' => 'numeric|min:1|max:140|required',
        'income_year' => 'numeric|min:1|max:2500|required',
        'income_amount' => 'numeric|min:1|max:2500|required',
        'outgoing_month' => 'numeric|min:1|max:140|required',
        'outgoing_year' => 'numeric|min:1|max:2500|required',
        'outgoing_amount' => 'numeric|min:1|max:2500|required',
        'is_manager' => 'boolean|required',
        'manager_status' => 'string|min:3|max:255',
        'is_owner' => 'boolean|required',
        'owner_status' => 'string|min:3|max:255',
        'is_accept_terms' => 'boolean|required',
        'doc_copy' => 'image|mimes:jpg,png,jpeg|required',
        'doc_copy_path' => 'string|max:255',
        'doc_copy_image_id' => 'integer',
        'avatar' => 'image|mimes:jpg,png',
        'avatar_path' => 'string|max:255|',
        'avatar_image_id' => 'integer',
        'user_id' => 'integer|required',
        'pk' => 'required|regex:/(\d){6}-(\d){5}/',
        'funds_origin' => 'string|required|max:255',
        'others_funds' => 'string|max:255',
    ];

    const VALIDATION_RULES_PHOTO_ATTACHED = [
        'username' => 'string|min:3|max:255|required',
        'surname' => 'string|min:3|max:255|required',
        'email' => 'email|required|max:255',
        'phone' => 'numeric|min:8|required',
        'address' => 'string|min:3|max:255|required',
        'citizenship' => 'string|min:3|max:255|required',
        'doc_type' => 'string|min:3|max:255|required',
        'doc_number' => 'string|min:3|max:255|required',
        'document_date' => 'date_format:Y-m-d',
        'has_status' => 'boolean|required',
        'social_status' => 'string|min:3|max:255',
        'income_month' => 'numeric|min:1|max:140|required',
        'income_year' => 'numeric|min:1|max:2500|required',
        'income_amount' => 'numeric|min:1|max:2500|required',
        'outgoing_month' => 'numeric|min:1|max:140|required',
        'outgoing_year' => 'numeric|min:1|max:2500|required',
        'outgoing_amount' => 'numeric|min:1|max:2500|required',
        'is_manager' => 'boolean|required',
        'manager_status' => 'string|min:3|max:255',
        'is_owner' => 'boolean|required',
        'owner_status' => 'string|min:3|max:255',
        'is_accept_terms' => 'boolean|required',
        'doc_copy_path' => 'string|max:255',
        'doc_copy_image_id' => 'integer',
        'avatar' => 'image|mimes:jpg,png',
        'avatar_path' => 'string|max:255|',
        'avatar_image_id' => 'integer',
        'user_id' => 'integer|required',
        'pk' => 'required|regex:/(\d){6}-(\d){5}/',
        'funds_origin' => 'string|required|max:255',
        'others_funds' => 'string|max:255',
    ];

    const SAVE_VALIDATION_RULES = [
        'username' => 'string|min:3|max:255|required',
        'surname' => 'string|min:3|max:255|required',
        'pk' => 'required|regex:/(\d){6}-(\d){5}/',
        'email' => 'email|required|max:255',
        'phone' => 'numeric|min:8',
        'address' => 'string|min:3|max:255',
        'citizenship' => 'string|min:3|max:255',
        'doc_type' => 'string|min:3|max:255',
        'doc_number' => 'string|min:3|max:255',
        'document_date' => 'date_format:Y-m-d',
        'has_status' => 'boolean',
        'social_status' => 'string|min:3|max:255',
        'income_month' => 'numeric',
        'income_year' => 'numeric',
        'income_amount' => 'numeric',
        'outgoing_month' => 'numeric',
        'outgoing_year' => 'numeric',
        'outgoing_amount' => 'numeric',
        'is_manager' => 'boolean',
        'manager_status' => 'string|min:3|max:255',
        'is_owner' => 'boolean',
        'owner_status' => 'string|min:3|max:255',
        'is_accept_terms' => 'boolean',
//        'doc_copy' => 'image|mimes:jpg,png,jpeg',
        'doc_copy_path' => 'string|max:255',
        'doc_copy_image_id' => 'integer',
//        'avatar' => 'image|mimes:jpg,png,jpeg',
        'avatar_path' => 'string|max:255|',
        'avatar_image_id' => 'integer',
        'user_id' => 'integer|required',
        'funds_origin' => 'string|max:255',
        'others_funds' => 'string|max:255',
    ];

    const SAVE_EMAIL_VALIDATION_RULES = [
        'username' => 'string|min:3|max:255|required',
        'surname' => 'string|min:3|max:255|required',
        'pk' => 'required|regex:/(\d){6}-(\d){5}/',
        'email' => 'email|required|max:255|unique:users',
        'phone' => 'numeric|min:8',
        'address' => 'string|min:3|max:255',
        'citizenship' => 'string|min:3|max:255',
        'doc_type' => 'string|min:3|max:255',
        'doc_number' => 'string|min:3|max:255',
        'document_date' => 'date_format:Y-m-d',
        'has_status' => 'boolean',
        'social_status' => 'string|min:3|max:255',
        'income_month' => 'numeric',
        'income_year' => 'numeric',
        'income_amount' => 'numeric',
        'outgoing_month' => 'numeric',
        'outgoing_year' => 'numeric',
        'outgoing_amount' => 'numeric',
        'is_manager' => 'boolean',
        'manager_status' => 'string|min:3|max:255',
        'is_owner' => 'boolean',
        'owner_status' => 'string|min:3|max:255',
        'is_accept_terms' => 'boolean',
//        'doc_copy' => 'image|mimes:jpg,png,jpeg',
        'doc_copy_path' => 'string|max:255',
        'doc_copy_image_id' => 'integer',
//        'avatar' => 'image|mimes:jpg,png,jpeg',
        'avatar_path' => 'string|max:255|',
        'avatar_image_id' => 'integer',
        'user_id' => 'integer|required',
        'funds_origin' => 'string|max:255',
        'others_funds' => 'string|max:255',
    ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes = []);
        $this->table = 'profile';
        $this->fillable = [
            'email',
            'username',
            'surname',
            'phone',
            'address',
            'citizenship',
            'doc_type',
            'doc_number',
            'document_date',
            'has_status',
            'social_status',
            'income_month',
            'income_year',
            'income_amount',
            'outgoing_month',
            'outgoing_year',
            'outgoing_amount',
            'is_manager',
            'manager_status',
            'is_owner',
            'owner_status',
            'is_accept_terms',
            'doc_copy',
            'doc_copy_path',
            'doc_copy_image_id',
            'avatar',
            'avatar_path',
            'avatar_image_id'.
            'user_id',
            'pk',
            'funds_origin',
            'others_funds'
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
