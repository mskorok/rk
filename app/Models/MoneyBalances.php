<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\MoneyBalances
 *
 * @property int $id
 * @property string $datums
 * @property int $start_balance
 * @property int $end_balance
 * @property int $incoming
 * @property int $outgoing
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|MoneyBalances whereCreatedAt($value)
 * @method static Builder|MoneyBalances whereDatums($value)
 * @method static Builder|MoneyBalances whereEndBalance($value)
 * @method static Builder|MoneyBalances whereId($value)
 * @method static Builder|MoneyBalances whereIncoming($value)
 * @method static Builder|MoneyBalances whereOutgoing($value)
 * @method static Builder|MoneyBalances whereStartBalance($value)
 * @method static Builder|MoneyBalances whereUpdatedAt($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class MoneyBalances extends Model
{
    use RecordsActivity;

    protected $table = 'money_daily_balance';

    public $rules = [
        'datums' => 'required',
        'start_balance' => 'integer',
        'end_balance' => 'integer',
        'incoming' => 'integer',
        'outgoing' => 'integer',
    ];

    protected $guarded = ['id'];
}
