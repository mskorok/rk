<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\OwnerAccess
 *
 * @property int $id
 * @property int $user_id
 * @property int $owner_id
 * @property string $access_status
 * @property string $access_level
 * @property string $comments
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read string $encrypted
 * @property-read \App\Models\Owner $owner
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @property-read \App\Models\User $user
 * @method static Builder|OwnerAccess ao()
 * @method static Builder|OwnerAccess o($o_id)
 * @method static Builder|OwnerAccess objekts()
 * @method static Builder|OwnerAccess seller()
 * @method static Builder|OwnerAccess sub()
 * @method static Builder|OwnerAccess u($user_id)
 * @method static Builder|OwnerAccess whereAccessLevel($value)
 * @method static Builder|OwnerAccess whereAccessStatus($value)
 * @method static Builder|OwnerAccess whereComments($value)
 * @method static Builder|OwnerAccess whereCreatedAt($value)
 * @method static Builder|OwnerAccess whereDeletedAt($value)
 * @method static Builder|OwnerAccess whereId($value)
 * @method static Builder|OwnerAccess whereOwnerId($value)
 * @method static Builder|OwnerAccess whereUpdatedAt($value)
 * @method static Builder|OwnerAccess whereUserId($value)
 * @method static Builder|\Esensi\Model\Model without($relations)
 * @mixin \Eloquent
 * @method bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OwnerAccess onlyTrashed()
 * @method bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OwnerAccess withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OwnerAccess withoutTrashed()
 */
class OwnerAccess extends Model
{
    use RecordsActivity;
    use SoftDeletes;

    protected $table = 'owner_access';

    protected $dates = ['deleted_at'];

    public $rules = [
        'user_id' => 'required',
        'owner_id' => 'required',
        'access_status' => 'integer|between:1,4',
        'access_level' => 'integer',
        'comments' => 'between:0,255',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;

    /**
     * Scope - seller connection
     */
    public function scopeSeller($query)
    {
        return $query->where('access_status', '=', 3);
    }

    /**
     * Scope - objekts connection
     */
    public function scopeObjekts($query)
    {
        return $query->where('access_status', '=', 2);
    }

    /**
     * Scope - subuser connection
     */
    public function scopeSub($query)
    {
        return $query->where('access_status', '=', 4);
    }

    /**
     * Scope - Active & owner
     */
    public function scopeAo($query)
    {
        return $query->where('access_status', '=', 1)->where('access_level', '=', 1);
    }

    /**
     * Scope - user
     */
    public function scopeU($query, $user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }

    /**
     * Scope - owner
     */
    public function scopeO($query, $o_id)
    {
        return $query->where('owner_id', $o_id);
    }
    
    /**
     * Get the owner
     *
     * @return Owner
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\Owner', 'owner_id');
    }

    /**
     * Get the user
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
