<?php

namespace App\Models;

use App\RecordsActivity;
use \Esensi\Model\Model;
use Esensi\Model\Model as Relations;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\SchoolPrices
 *
 * @property int $id
 * @property int $school_id
 * @property int $object_id
 * @property int $grade
 * @property float $price
 * @property bool $vat
 * @property string $regnr
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|SchoolPrices whereCreatedAt($value)
 * @method static Builder|SchoolPrices whereGrade($value)
 * @method static Builder|SchoolPrices whereId($value)
 * @method static Builder|SchoolPrices whereObjectId($value)
 * @method static Builder|SchoolPrices wherePrice($value)
 * @method static Builder|SchoolPrices whereRegnr($value)
 * @method static Builder|SchoolPrices whereSchoolId($value)
 * @method static Builder|SchoolPrices whereUpdatedAt($value)
 * @method static Builder|SchoolPrices whereVat($value)
 * @method static Builder|Relations without($relations)
 * @mixin \Eloquent
 */
class SchoolPrices extends Model
{
    use RecordsActivity;

    protected $table = 'schools_prices';

    public $rules = [
        'school_id' => 'integer|required',
        'object_id' => 'integer|required',
        'grade' => 'integer|required',
        'price' => 'required',
        'vat' => 'required',
        'regnr' => '',
    ];

    protected $guarded = ['id'];

    public $autoHydrateEntityFromInput = true;
}
