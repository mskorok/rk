<?php

namespace App\Models;

use \Esensi\Model\Model;
use Esensi\Model\Model as Relation;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\DailyLogZz
 *
 * @property int $id
 * @property string $datums
 * @property string $pk
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $name
 * @property string $grade
 * @property int $school_id
 * @property int $age
 * @property string $free_meals_from
 * @property string $free_meals_until
 * @property string $second_meals_from
 * @property string $second_meals_until
 * @property string $third_meals_from
 * @property string $third_meals_until
 * @property bool $riga
 * @property bool $alive
 * @property string $kopgalds_from
 * @property string $kopgalds_until
 * @property-read string $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method static Builder|DailyLogZz whereAge($value)
 * @method static Builder|DailyLogZz whereAlive($value)
 * @method static Builder|DailyLogZz whereCreatedAt($value)
 * @method static Builder|DailyLogZz whereData($value)
 * @method static Builder|DailyLogZz whereDatums($value)
 * @method static Builder|DailyLogZz whereFreeMealsFrom($value)
 * @method static Builder|DailyLogZz whereFreeMealsUntil($value)
 * @method static Builder|DailyLogZz whereGrade($value)
 * @method static Builder|DailyLogZz whereId($value)
 * @method static Builder|DailyLogZz whereKopgaldsFrom($value)
 * @method static Builder|DailyLogZz whereKopgaldsUntil($value)
 * @method static Builder|DailyLogZz whereName($value)
 * @method static Builder|DailyLogZz wherePk($value)
 * @method static Builder|DailyLogZz whereRiga($value)
 * @method static Builder|DailyLogZz whereSchoolId($value)
 * @method static Builder|DailyLogZz whereSecondMealsFrom($value)
 * @method static Builder|DailyLogZz whereSecondMealsUntil($value)
 * @method static Builder|DailyLogZz whereThirdMealsFrom($value)
 * @method static Builder|DailyLogZz whereThirdMealsUntil($value)
 * @method static Builder|DailyLogZz whereUpdatedAt($value)
 * @method static Builder|Relation without($relations)
 * @mixin \Eloquent
 */
class DailyLogZz extends Model
{
    protected $table = 'daily_log_zz';

    public $rules = [
        'datums' => 'required',
        'pk' => 'required',
        'data' => 'required'
    ];

    public $timestamps = true;

    protected $guarded = ['id'];
}
