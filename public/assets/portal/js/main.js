(function ($) {

    pattern = /^[a-z0-9_.-]+@[a-z0-9-]+\.[a-z]{1,10}$/i;

    $.fn.lavalamp = function (options) {

        options = $.extend({
            speed: 500,
            reset: 0,
            color: 'none',
            radius: 0,
            easing: null
        }, options);

        return this.each(function () {

            var nav = $(this),
                current = nav.children('.active'),
                lavalamp;

            // create the lavalamp item
            nav.children('.lavalamp').remove();
            $('<li class="lavalamp hidden-xs"></li>').css({
                width: current.width(),
                height: nav.height(),
                left: parseInt(current.position().left),
                backgroundColor: options.color,
                borderRadius: options.radius
            }).appendTo(this);

            lavalamp = nav.children('.lavalamp');

            // reusable options
            var opts = {
                duration: options.speed,
                easing: options.easing,
                queue: false
            };

            // trigger the lavalamp on mouseover and mouseleave
            var reset;
            nav.children('li:not(.lavalamp)').hover(function () {
                // mouse over
                clearTimeout(reset);
                lavalamp.animate({
                    width: $(this).width(),
                    left: $(this).position().left,
                }, opts);
            }, function () {
                // mouse out
                // if (window.innerWidth > 768) {
                //     $(this).trigger('click').children('a').blur();
                // }
                reset = setTimeout(function () {
                    lavalamp.animate({
                        width: current.width(),
                        left: current.position().left
                    }, opts)
                }, options.reset);
            });

            nav.children('li:not(.lavalamp)').click(function () {
                current = $(this);
            });

            // zero bottom radius if dropdown is shown
            nav.on('show.bs.dropdown', function () {
                lavalamp.css({
                    'border-bottom-left-radius': 0,
                    'border-bottom-right-radius': 0
                })
            });

            // reset borser radius if dropdown is hidden
            nav.on('hide.bs.dropdown', function () {
                lavalamp.css({
                    'border-radius': options.radius
                })
            });

        }); // end each

    };

})(jQuery);

$(document).ready(function () {
    var cr_images = document.querySelectorAll('.cr-image');
    if (cr_images) {
        [].forEach.call(cr_images, function (image) {
            image.setAttribute('style', 'opacity: 1;')

        })
    }

    //$(window).on('resize', function(){
    $('#lavalamp-nav').lavalamp({
        radius: '4px',
        color: '#84a4bb',
        easing: 'easeOutBack'
    });
    //}).trigger('resize');


    // Navigation scroll
    $('.js-scroll-to').click(function () {
        var scroll_el = $(this).attr('href');
        if ($(scroll_el).length != 0) {
            $('a.js-scroll-to').each(function () {
                $(this).parent().removeClass('active');
            });
            $(this).parent().addClass('active');

            $('html, body').animate({scrollTop: $(scroll_el).offset().top - 96}, 500);
        }
        // return false;
    });

    //Owl carousel
    $('.js-owl-carousel-review').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        loop: true,

        autoplay: true,
        autoplayTimeout: 7000,
        autoplayHoverPause: true,

        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2,
                nav: false
            },
            990: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });


    // Slider on the main page
    $('.js-carousel-generic').owlCarousel({
        items: 1,
        loop: true,

        autoplay: true,
        autoplayTimeout: 7000,
        autoplayHoverPause: true,

        nav: true
    });

    // Collapse panels
    $('.js-panel-heading a').click(function() {
        if ($(this).closest('.js-panel-heading').hasClass('open')) {
            $('.js-panel-heading').removeClass('open');
        } else {
            $('.js-panel-heading').removeClass('open');
            $(this).parents('.js-panel-heading').addClass('open');                        
        }
    });
    $('.panel-group .collapse').on('show.bs.collapse', function () {
        $('.panel-group .collapse').not(this).removeClass('in');
    });
    
    $('a.e-card-help').click(function() {
        $('.modal-card-nr-help').modal('show');
    });

    $(document).on('keypress', '#ajax_add_card_form .switch-next div input', function (e) {
      if ((e.which >= 48 && e.which <= 57) || (e.which === 45) ) {
        var next = $(this).data('id') + 1;
        var name = $(this).closest('div.switch-next').data('name');
        $('#' + name + '_' + next).focus(); 
      } else {
        e.preventDefault();
      }
    });
    $(document).on('keydown', '#ajax_add_card_form .switch-next div input', function () {    
      var key = event.keyCode || event.charCode;
      if (key === 8) {
        // backspace
        var this_id = $(this).data('id');
        if (this_id > 0) {
          var next = this_id - 1;
          var name = $(this).closest('div.switch-next').data('name');
          $('#' + name + '_' + next).focus(); 
        }
      } 
      else
      if (key === 9) {
        if ($(this).val().length < 1) {
            event.preventDefault();
        }
      }
    });

    // Preventing closing the dropdown
    $(document).on("click.bs.dropdown.data-api", ".noclose", function (e) {
        e.stopPropagation()
    });

    // Hiding modal scroll
    var $bodyWidth = $('body').width();
    $('.modal').on('show.bs.modal', function () {
        $('body').css({'overflow-y': "hidden"}).css({'padding-right': ($('body').width() - $bodyWidth)});
    });

    $('.modal').on('hidden.bs.modal hide.bs.modal', function () {
        $('body').css({'padding-right': ($('body').width() - $bodyWidth), 'overflow-y': "auto"});
    });

    // Check the email

    $('.email').val('');

    $('span.group-addon.noclose:not(.svg)').click(function () {
        var type = $(this).parent().find('input').attr('type');
        if (type == 'password') {
            $(this).parent().find('input').attr('type', 'text');
            $(this).addClass('active');
        }
        else {
            $(this).parent().find('input').attr('type', 'password');
            $(this).removeClass('active');
        }
    });


    $(document).on("click", '.menu-rotate.closed.active', function () {
        $('.menu-rotate.closed').removeClass('active');
        $('.menu-rotate.opened').addClass('active');
    });
    $(document).on("click", '.menu-rotate.opened.active', function () {
        setTimeout(function () {
            $('.menu-rotate.opened').removeClass('active');
            $('.menu-rotate.closed').addClass('active');
        }, 250);
    });

    $('html').on('click.dropdown.data-api', function () {
        if ($('.menu-rotate.opened').hasClass('active')) {
            setTimeout(function () {
                $('.menu-rotate.opened').removeClass('active');
                $('.menu-rotate.closed').addClass('active');
            }, 250);
        }
    });

    // $('.dropdown-menu').on('hide.bs.dropdown', function {
    //     if ($('.menu-rotate.opened').hasClass('active')) {
    //         setTimeout(function() {
    //             $('.menu-rotate.opened').removeClass('active');        
    //             $('.menu-rotate.closed').addClass('active');
    //         }, 250); 
    //     }
    // });

    $('.btn-group.signout').click(function () {
        window.location.assign(logout_url);
    });

    $('.btn-group.register').click(function () {
        window.location.assign(register_url);
    });
});
var crop_opts = {
    viewport: {
        width: 300,
        height: 300,
        type: 'circle'
    },
    step: 0.05,
    boundary: {
        width: 300,
        height: 300
    },
    showZoomer: false
};
var form_errors = {};
var custom_functions = {
    passwordId: 'password',
    passwordConfirmId: 'password_confirm',
    validateForm: function (selectors, callback, args) {
        args = args || null;
        if (Array.isArray(selectors)) {
            [].forEach.call(selectors, function (selector) {
                var element = document.querySelector(selector);
                if (element) {
                    element.addEventListener('change', function() {
                        if (args) {
                            callback(args);
                        } else {
                            callback();
                        }
                    });
                    element.addEventListener('blur', function() {
                        if (args) {
                            callback(args);
                        } else {
                            callback();
                        }
                    });
                    element.addEventListener('input', function() {
                        if (args) {
                            callback(args);
                        } else {
                            callback();
                        }
                    });
                } else {
                    console.log('el not found', selector)
                }
            });
        }
    },
    validateEmail: function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    validatePassword: function (password) {
        var passwordOk = false;
        if (password.length > 7) {
            var rules = [/[0-9]+/, /[A-Z]+/, /[a-z]+/];
            passwordOk = rules.every(function (r) {
                return r.test(password)
            });
        }
        return passwordOk;
    },
    validatePasswordConfirm: function (input) {
        var pass = document.getElementById(custom_functions.passwordId);
        var confirm = document.getElementById(custom_functions.passwordConfirmId);
        if( pass && confirm) {
            return pass.value === confirm.value;
        } else {
            return false;
        }
    },
    validateCard: function (sum) {
        return $.isNumeric(sum) && sum > 1 && sum <= 141;
    },
    validatePaymentCard: function (number) {
        var string = number.toString();
        return $.isNumeric(sum) && string.length === 12;
    },
    validateSecurityCode: function (number) {
        var string = number.toString();
        return $.isNumeric(sum) && string.length === 3;
    },
    validatePhone: function (phone) {
        var re = /^[0-9]{8,}$/;
        return re.test(phone);
    },
    validateNumeric: function (number) {
        return number !== '' && $.isNumeric(number) && number !== 0 && number !== '0' && number !== 0.00 && number !== '0.00';
    },
    validateNumeric140: function (number) {
        return number !== ''  && parseInt(number) < 141 && parseFloat(number) > 0;
    },
    validateNumeric2500: function (number) {
        return number !== ''  && parseInt(number) < 2501 && parseFloat(number) > 0;
    },
    validateNumericWithNull: function (number) {
        return number !== '' && $.isNumeric(number) && number.toString().length === 1;
    },
    validateString: function (string, i) {
        i = i || 1;
        return string.length > i;
    },
    validateDate: function (date) {
        var re = /^(\d{4})[-](\d{1,2})[-](\d{1,2})$/;
        return re.test(date);
    },
    validateClass: function (string) {
        var re = /^[0-9]{1,2}[.]?[a-z]{1,2}?$/;
        return re.test(string);
    },
    validateDocumentDate: function(date) {
        var plus90 = new Date();
        plus90.setDate(plus90.getDate() + 90);
        var doc_date = new Date(date);
        return doc_date > plus90;
    },
    validateTime: function (time) {
        var re = /^\d{1,2}:\d{2}([ap]m)?$/;
        return re.test(time);
    },
    validateSelect: function (input_selector, button_selector, flag) {
        var remove_disable = typeof(flag) === 'undefined';
        var select = $(input_selector);
        var parent = select.parent();
        var nodes = $('.select2-hidden-accessible');
        var spans = nodes.next('span');
        var j;
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i];
            if ('#'+$(node)[0].id === input_selector) {
                j=i;
            }
        }
        if (select) {
            if ($(select)[0].tagName !== 'SELECT') {
                console.log('wrong tag', $(select)[0].tagName);
                return false;
            }
            select.change(function () {
                $(parent).find('.error-message').hide();
                $(spans[j])[0].classList.add('success');
                $(spans[j])[0].classList.remove('error');
                if (remove_disable) {
                    custom_functions.enable_button(button_selector);
                } else {
                    form_errors[flag] = true;
                }
            });
            if (select.val() === '') {
                custom_functions.disable_button(button_selector);
                form_errors[flag] = false;
                $(spans[j])[0].classList.add('error');
                $(spans[j])[0].classList.remove('success');
                return false;
            } else {
                $(parent).find('.error-message').hide();
                $(spans[j])[0].classList.add('success');
                $(spans[j])[0].classList.remove('error');
                if (remove_disable) {
                    custom_functions.disable_button(button_selector);
                } else {
                    form_errors[flag] = true;
                }
            }
        }
    },
    disable_button: function (button_selector) {
        var buttons;
        if(Array.isArray(button_selector)) {
            [].forEach.call(button_selector, function (selector) {
                buttons = document.querySelectorAll(selector);
                if (buttons) {
                    [].forEach.call(buttons, function (button) {
                        button.setAttribute('disabled', 'disabled');
                    })
                }
            });
        } else if (typeof button_selector === 'string' || button_selector instanceof String) {
            buttons = document.querySelectorAll(button_selector);
            if (buttons) {
                [].forEach.call(buttons, function (button) {
                    button.setAttribute('disabled', 'disabled');
                })
            }
        }
    },
    enable_button: function (button_selector) {
        var buttons;
        if(Array.isArray(button_selector)) {
            buttons = document.querySelectorAll(button_selector);
            if (buttons) {
                [].forEach.call(buttons, function (button) {
                    button.removeAttribute('disabled');
                })
            }
        } else if (typeof button_selector === 'string' || button_selector instanceof String) {
            buttons = document.querySelectorAll(button_selector);
            if (buttons) {
                [].forEach.call(buttons, function (button) {
                    button.removeAttribute('disabled');
                })
            }
        }
    },
    validateInput: function (input_selector, button_selector, i, validator, flag) {
        var remove_disable = typeof(flag) === 'undefined';
        var input = $(input_selector);
        var parent = input.parent();
        i = i ? i : '';
        if (input.val() != '') {
            if (validator(input.val())) {
                $(parent).find('.error-message' + i).hide();
                input.removeClass('error').addClass('success');
                if (remove_disable) {
                    custom_functions.enable_button(button_selector);
                } else {
                    form_errors[flag] = true;
                }
            } else {
                $(parent).find('.error-message' + i).show();
                input.addClass('error').removeClass('success');
                custom_functions.disable_button(button_selector);
                form_errors[flag] = false;
            }
        } else {
            $(parent).find('.error-message' + i).hide();
            input.removeClass('success error');
            custom_functions.disable_button(button_selector);
            form_errors[flag] = false;
        }
        input.blur(function (e) {
            e.stopPropagation();
            e.preventDefault();
            if (input.val() != '') {
                if (validator(input.val())) {
                    $(parent).find('.error-message' + i).hide();
                    input.removeClass('error').addClass('success');
                    if (remove_disable) {
                        custom_functions.enable_button(button_selector);
                    } else {
                        form_errors[flag] = true;
                    }
                } else {
                    $(parent).find('.error-message' + i).show();
                    input.addClass('error').removeClass('success');
                    custom_functions.disable_button(button_selector);
                    form_errors[flag] = false;
                }
            } else {
                $(parent).find('.error-message' + i).hide();
                input.removeClass('success error');
                custom_functions.disable_button(button_selector);
                form_errors[flag] = false;
            }
        }).focus(function () {
            var input = $(this);
            var parent = input.parent();
            $(parent).find('.error-message' + i).hide();
            input.removeClass('success error');
            custom_functions.disable_button(button_selector);
            form_errors[flag] = false;
        });
        document.querySelector(input_selector).addEventListener('input', function (e) {
            if (this.value) {
                var value = this.value;
                if (validator(value)) {
                    $(this).removeClass('error').addClass('success');
                    if (remove_disable) {
                        custom_functions.enable_button(button_selector);
                    } else {
                        form_errors[flag] = true;
                    }
                } else {
                    $(this).addClass('error').removeClass('success');
                    custom_functions.disable_button(button_selector);
                    form_errors[flag] = false;
                }
            }
        });
    },
    validateNotEmpty: function (string) {
        return string.length >= 1 && string !== 0 && string !== '';
    },
    validateTextArea: function (selector) {
        var self = this;
        $(selector).blur(function (e) {
            var textarea = $(this);
            var parent = $(this).parent().parent();
            var button = parent.find('button[type=submit]');
            if (!self.validateString(textarea.val(), 5)) {
                self.show_error($(button)[0]);
            }
        }).focus(function (e) {
            var parent = $(this).parent().parent();
            var button = parent.find('button[type=submit]');
            self.hide_error($(button)[0]);
        });
    },
    show_error: function (input, i) {
        i = i || null;
        var selector = (i) ? 'error-message' + i : 'error-message';
        if (input) {
            input.classList.remove('success');
            input.classList.add('error');
        }
        var parent = input.parentNode;
        var error_containers = parent.getElementsByClassName(selector);
        if (error_containers) {
            [].forEach.call(error_containers, function (error_container) {
                error_container.style.display = 'block';
            });
        }
    },
    hide_error: function (input, i) {
        i = i || null;
        var selector = (i) ? 'error-message' + i : 'error-message';
        if (input) {
            input.classList.remove('error');
            input.classList.add('success');
        }
        var parent = input.parentNode;
        var error_containers = parent.getElementsByClassName(selector);
        if (error_containers) {
            [].forEach.call(error_containers, function (error_container) {
                error_container.style.display = 'none';
            });
        }
    },
    show_success: function (elem, i) {
        i = i || null;
        var parent = elem.parentNode;
        var selector = (i) ? 'success-message' + i : 'success-message';
        var elements = parent.getElementsByClassName(selector);
        [].forEach.call(elements, function (element) {
            element.style.display = 'block';
        });
    },
    hide_success: function (elem, i) {
        i = i || null;
        var parent = elem.parentNode;
        var selector = (i) ? 'success-message' + i : 'success-message';
        var elements = parent.getElementsByClassName(selector);
        [].forEach.call(elements, function (element) {
            element.style.display = 'none';
        });
    },
    show_success_recovery: function () {
        $('#pass_recovery_success').click();
        setTimeout(function () {
            $('.pass-recovery').click();
        }, 200);
    },
    reset: function (input) {
        if (input) {
            var email = input.value;
        } else {
            return false;
        }
        var self = this;
        var url = reset_url + '/?email=' + email;
        var xhr = new XMLHttpRequest();
        var response;
        xhr.onload = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    try {
                        response = JSON.parse(this.response);
                        if (response.error) {
                            self.show_error(input, 1);
                            console.log('email error', response.error);
                        } else if (response.success) {
                            self.show_success_recovery();
                            console.log('email success');
                        }
                    } catch (e) {
                        self.show_error(input);
                        console.log('error', e.message);
                    }
                } else {
                    console.log('error response', this.response);
                    self.show_error(input);
                }
            }
        };
        xhr.open('GET', url, true);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send();
    },
    feedback: function (form) {
        var self = this;
        var url = feedback_url;
        var xhr = new XMLHttpRequest();
        var form_data = new FormData(form);
        var response;
        var button = form.elements[6];
        xhr.onload = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    try {
                        response = JSON.parse(this.response);
                        if (response.success) {
                            self.show_success(button);
                            self.clear_form(form);
                            button.style.display = 'none';
                            setTimeout(function () {
                                button.style.display = 'inline-block';
                            }, 10000);
                            console.log('feedback', response.success);
                        } else if (response.error) {
                            self.show_error(button);
                            console.log('feedback', response.error);
                        }
                    } catch (e) {
                        console.log('error', e.message);
                    }
                } else {
                    console.log('error response', this.response);
                }
            }
        };
        xhr.open('POST', url, true);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send(form_data);
    },
    clear_form: function (form) {
        if (form.tagName === 'FORM') {
            [].forEach.call(form.elements, function (el) {
                if (el.value) {
                    el.value = '';
                    $(el).removeClass('success error');
                }
            });
        }
    }
};


document.addEventListener('DOMContentLoaded', function (e) {
    var ec_privacy = $.cookie('E_Privacy_Directive_EU');
    if (ec_privacy == 'forbidden') {
        $('#ec_directive_disabled').hide();
        // $('#cookies_block').hide();
        $('div.show-cookie-bar').removeClass('show-cookie-bar');
    } else if (ec_privacy != 'allowed') {
        $('div.show-cookie-bar').addClass('show-cookie-bar');
        $('#cookies_block').show();
        $('#cookie_forbidden').click(function () {
            $.cookie('E_Privacy_Directive_EU', 'forbidden', { expires: 365, path: '/' });
            $('#ec_directive_disabled').hide();
            $('div.show-cookie-bar').removeClass('show-cookie-bar');
        });
        $('#cookie_notice').click(function () {
            $.cookie('E_Privacy_Directive_EU', 'allowed', { expires: 365, path: '/' });
            $('div.show-cookie-bar').removeClass('show-cookie-bar');
        })
    } else if (ec_privacy === 'allowed') {
        $('div.show-cookie-bar').removeClass('show-cookie-bar');
    }

    var custom = custom_functions;
    var input_recovery_email = document.getElementById('pass_recovery_email');
    if (input_recovery_email) {
        input_recovery_email.addEventListener('focus', function (e) {
            custom.hide_error(input_recovery_email);
        });
        custom_functions.validateInput(
            '#pass_recovery_email',
            null,
            3,
            custom_functions.validateEmail
        );
        var submit_recovery_button = document.getElementById('pass_recovery_submit');
        if (submit_recovery_button) {
            submit_recovery_button.addEventListener('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                if (input_recovery_email.value) {
                    if (custom.validateEmail(input_recovery_email.value)) {
                        custom.reset(input_recovery_email);
                    } else {
                        custom.show_error(input_recovery_email);
                    }
                }
                return false;
            })
        }
    }


    var feedback_form = document.getElementById('feedback_form');
    if (feedback_form) {
        custom_functions.validateInput(
            '#support_name',
            '#feedback_submit_button',
            null,
            custom_functions.validateString
        );
        custom_functions.validateInput(
            '#support_surname',
            '#feedback_submit_button',
            null,
            custom_functions.validateString
        );
        custom_functions.validateInput(
            '#support_surname',
            '#feedback_submit_button',
            null,
            custom_functions.validateString
        );
        custom_functions.validateInput(
            '#support_email',
            '#feedback_submit_button',
            null,
            custom_functions.validateEmail
        );
        custom_functions.validateInput(
            '#support_phone',
            '#feedback_submit_button',
            null,
            custom_functions.validatePhone
        );
        feedback_form.addEventListener('submit', function (e) {
            e.stopPropagation();
            e.preventDefault();
            var el = this.elements;
            if (el[1].value !== ''
                && el[2].value !== ''
                && el[3].value !== ''
                && el[4].value !== ''
                && el[5].value.length > 5
            ) {
                custom.feedback(feedback_form);
            } else {
                custom.validateTextArea('#support_question');
                custom.hide_success(el[6]);
                custom.show_error(el[6]);
            }
        })
    }
});



var show_minutes = 5;
var timer_time = 0;
function update_modal() {
    if (timer_time > 600) {
        // console.log('update', show_minutes);
        var el = document.getElementById('time_to_logout');
        el.textContent = show_minutes;
        $('.modal-timer').modal('show');
        if (show_minutes === 0) {
            window.location.assign(logout_url);
        }
        show_minutes--;
    }
}
$(document).ready(function () {
    if (auth) {
        // console.log('timer');
        $(document).bind('mousemove keydown scroll', function() {
            // console.log('show minutes', show_minutes);
            if ($('.modal-timer').hasClass('in')) {
                $('.modal-timer').modal('hide');
            }
            timer_time = 0;
            show_minutes = 5;
        });
        setInterval('timer_time++;', 1000);
        setInterval('update_modal();', 60000);
    }
    $('#timer_yes .btn').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('.modal-timer').modal('hide');
    });
    $('#timer_no .btn').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        window.location.assign(logout_url);
    });
});


