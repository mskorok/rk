<?php

use Carbon\Carbon;

ini_set('memory_limit','228M');


class sendFreeMealDailyTest extends TestCase
{
    /**
     * @group artisan
     */
    public function test_format_xml_string()
    {
        $periods = '2016-10-10';
        $final = [
            1 => [
                "111111-22222" => [
                    "grade" => 2,
                    "meals" => [
                        [
                            "id" => "1",
                            "date" => $periods,
                            "regno" => 123,
                            "cardid" => 2
                        ]
                    ]
                ],
            ],
            2 => [
                "111111-33333" => [
                    "grade" => 3,
                    "meals" => [
                        [
                            "id" => "2",
                            "date" => $periods,
                            "regno" => 123,
                            "cardid" => 3
                        ]
                    ]
                ],
                "111111-44444" => [
                    "grade" => 4,
                    "meals" => [
                        [
                            "id" => "3",
                            "date" => $periods,
                            "regno" => 123,
                            "cardid" => 4
                        ]
                    ]
                ],
            ],
        ];

        $com = new \App\Console\Commands\sendFreeMealDaily();

        $reflector = new ReflectionClass( 'App\Console\Commands\sendFreeMealDaily' );
        $method = $reflector->getMethod( 'format_xml_string' );
        $method->setAccessible( true );

        $result = $method->invokeArgs($com,  [$periods, $final] );

        $correct = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
<soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"><wsa:Action>http://tempuri.org/IMonitoringService/SendMeals</wsa:Action></soap:Header>
<soap:Body>
  <tem:SendMeals xmlns="http://tempuri.org/">
  	<tem:request>
        <zzd:Header>
            <zzd1:From>skolas.rigaskarte.lv</zzd1:From>
        </zzd:Header>
        <zzd:Month>2016-10</zzd:Month>
        <zzd:SchoolList><zzd:School>
                <zzd:Id>1</zzd:Id>
                <zzd:ChildList><zzd:Child>
                    <zzd:Id>111111-22222</zzd:Id>
                    <zzd:ClassGroup>2</zzd:ClassGroup>
                    <zzd:MealTimeList><zzd:MealTime>
                        <zzd:Id>1</zzd:Id>
                        <zzd:Date>2016-10-10</zzd:Date>
                        <zzd:RegNo>123</zzd:RegNo>
                        <zzd:CardId>2</zzd:CardId>
                    </zzd:MealTime></zzd:MealTimeList>
                    </zzd:Child></zzd:ChildList>
                </zzd:School><zzd:School>
                <zzd:Id>2</zzd:Id>
                <zzd:ChildList><zzd:Child>
                    <zzd:Id>111111-33333</zzd:Id>
                    <zzd:ClassGroup>3</zzd:ClassGroup>
                    <zzd:MealTimeList><zzd:MealTime>
                        <zzd:Id>2</zzd:Id>
                        <zzd:Date>2016-10-10</zzd:Date>
                        <zzd:RegNo>123</zzd:RegNo>
                        <zzd:CardId>3</zzd:CardId>
                    </zzd:MealTime></zzd:MealTimeList>
                    </zzd:Child><zzd:Child>
                    <zzd:Id>111111-44444</zzd:Id>
                    <zzd:ClassGroup>4</zzd:ClassGroup>
                    <zzd:MealTimeList><zzd:MealTime>
                        <zzd:Id>3</zzd:Id>
                        <zzd:Date>2016-10-10</zzd:Date>
                        <zzd:RegNo>123</zzd:RegNo>
                        <zzd:CardId>4</zzd:CardId>
                    </zzd:MealTime></zzd:MealTimeList>
                    </zzd:Child></zzd:ChildList>
                </zzd:School></zzd:SchoolList>
    </tem:request>
  </tem:SendMeals>
</soap:Body>
</soap:Envelope>';

        $this->assertEquals($result, $correct);
    }

    /**
     * @group artisan
     */
    public function test_get_data()
    {
        $o1 = factory(App\Models\Objekts::class)->create([
            'object_regnr' => 123,
        ]);

        $o2 = factory(App\Models\Objekts::class)->create([
            'object_regnr' => 124,
        ]);

        $eticket1 = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567890,
        ]);

        $eticket2 = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567891,
        ]);

        $eto1 = factory(App\Models\EticketsObjekti::class)->create([
            'object_id' => $o1->id,
            'eticket_id' => $eticket1->id,
        ]);

        $eto2 = factory(App\Models\EticketsObjekti::class)->create([
            'object_id' => $o2->id,
            'eticket_id' => $eticket2->id,
        ]);

        $school1 = factory(App\Models\School::class)->create([
            'reg_nr' => 123,
        ]);

        $school2 = factory(App\Models\School::class)->create([
            'reg_nr' => 124,
        ]);

        $t1 = factory(App\Models\Transaction::class)->create([
            'dotation_id' => 1,
            'object_id' => $o1->id,
            'from_eticket_nr' => '1234567890',
            'from_account_id' => 2,
            'updated_at' => Carbon::now()->subDay(),
            'seller_reg_nr' => 1,
        ]);

        $t2 = factory(App\Models\Transaction::class)->create([
            'dotation_id' => 1,
            'object_id' => $o2->id,
            'from_eticket_nr' => '1234567891',
            'from_account_id' => 2,
            'updated_at' => Carbon::now()->subDay(),
            'seller_reg_nr' => 1,
        ]);

        $com = new \App\Console\Commands\sendFreeMealDaily();

        $reflector = new ReflectionClass( 'App\Console\Commands\sendFreeMealDaily' );
        $method = $reflector->getMethod( 'get_data' );
        $method->setAccessible( true );

        $result = $method->invokeArgs($com,  [] );
        $used_ids = $result[0];
        $xml = $result[1];

        $this->assertEquals([$t1->id, $t2->id], $used_ids);

        $correct = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:zzd="Zzdats.Kavis.Ekartes.Entities.Request" xmlns:zzd1="Zzdats.Kavis.Ekartes.Entities.Common">
<soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"><wsa:Action>http://tempuri.org/IMonitoringService/SendMeals</wsa:Action></soap:Header>
<soap:Body>
  <tem:SendMeals xmlns="http://tempuri.org/">
  	<tem:request>
        <zzd:Header>
            <zzd1:From>skolas.rigaskarte.lv</zzd1:From>
        </zzd:Header>
        <zzd:Month>2016-12</zzd:Month>
        <zzd:SchoolList><zzd:School>
                <zzd:Id>1</zzd:Id>';

        $this->assertContains($correct, $xml);

        $correct = '<zzd:SchoolList><zzd:School>
                <zzd:Id>1</zzd:Id>
                <zzd:ChildList><zzd:Child>
                    <zzd:Id>'.$eticket1->pk.'</zzd:Id>
                    <zzd:ClassGroup>0</zzd:ClassGroup>
                    <zzd:MealTimeList><zzd:MealTime>
                        <zzd:Id>1</zzd:Id>
                        <zzd:Date>'.str_replace(" ", "T", $t1->updated_at) .'.0Z</zzd:Date>
                        <zzd:RegNo>1</zzd:RegNo>
                        <zzd:CardId>'.$eticket1->nr.'</zzd:CardId>
                    </zzd:MealTime></zzd:MealTimeList>
                    </zzd:Child></zzd:ChildList>
                </zzd:School><zzd:School>
                <zzd:Id>2</zzd:Id>
                <zzd:ChildList><zzd:Child>
                    <zzd:Id>'.$eticket2->pk.'</zzd:Id>
                    <zzd:ClassGroup>0</zzd:ClassGroup>
                    <zzd:MealTimeList><zzd:MealTime>
                        <zzd:Id>2</zzd:Id>
                        <zzd:Date>'.str_replace(" ", "T", $t2->updated_at) .'.0Z</zzd:Date>
                        <zzd:RegNo>1</zzd:RegNo>
                        <zzd:CardId>'.$eticket2->nr.'</zzd:CardId>
                    </zzd:MealTime></zzd:MealTimeList>
                    </zzd:Child></zzd:ChildList>
                </zzd:School></zzd:SchoolList>';

        $this->assertContains($correct, $xml);
    }

}