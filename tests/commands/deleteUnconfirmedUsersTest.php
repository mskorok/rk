<?php

ini_set('memory_limit','228M');

use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;

class deleteUnconfirmedUsersTest extends TestCase
{
    /**
     * @group artisan
     */
    public function testAll()
    {
        $this->create_regular_user();
        $u2 = $this->create_regular_user("Second", "second@a.lv");

        $sk = \App\Models\User::count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(2, $sk);

        Artisan::call('deleteunconfirmed');
        $resultAsText = Artisan::output();

        $this->assertContains('Neapstiprināto lietotāju reģistrāciju dzēšana sekmīgi pabeigta - dzēsta(s) 0 reģistrācija(s).', $resultAsText);

        $u2->created_at = Carbon::now()->subDays(6);
        $u2->updated_at = Carbon::now()->subDays(6);
        $u2->terms = 3;
        $u2->confirmed = 0;
        $u2->forceSave();

        Artisan::call('deleteunconfirmed');
        $resultAsText = Artisan::output();

        $this->assertContains('Neapstiprināto lietotāju reģistrāciju dzēšana sekmīgi pabeigta - dzēsta(s) 1 reģistrācija(s).', $resultAsText);

        $sk = \App\Models\User::count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(1, $sk);

        $u2 = $this->create_regular_user("Second2", "second2@a.lv");

        $u2->created_at = Carbon::now()->subDays(3);
        $u2->updated_at = Carbon::now()->subDays(3);
        $u2->terms = 3;
        $u2->confirmed = 0;
        $u2->forceSave();

        Artisan::call('deleteunconfirmed');
        $resultAsText = Artisan::output();

        $this->assertContains('Neapstiprināto lietotāju reģistrāciju dzēšana sekmīgi pabeigta - dzēsta(s) 0 reģistrācija(s).', $resultAsText);

        $u2->created_at = Carbon::now()->subDays(6);
        $u2->updated_at = Carbon::now()->subDays(6);
        $u2->terms = 3;
        $u2->confirmed = 1;
        $u2->forceSave();

        Artisan::call('deleteunconfirmed');
        $resultAsText = Artisan::output();

        $this->assertContains('Neapstiprināto lietotāju reģistrāciju dzēšana sekmīgi pabeigta - dzēsta(s) 0 reģistrācija(s).', $resultAsText);
    }

}