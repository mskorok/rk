<?php

ini_set('memory_limit','228M');


class getSchoolDataTest extends TestCase
{
    /**
     * @group artisan
     */
    public function testODPCleanup()
    {
        factory(App\Models\ObjectDotationPrices::class, 3)->create([
            'object_id' => 3,
            "seller_account_id" => 2
        ]);

        factory(App\Models\ObjectDotationPrices::class)->create([
            'object_id' => 2,
            "seller_account_id" => 2
        ]);

        factory(App\Models\ObjectDotationPrices::class)->create([
            'object_id' => 3,
            "seller_account_id" => 1
        ]);

        $odp = factory(App\Models\ObjectDotationPrices::class)->create([
            'object_id' => 3,
            "seller_account_id" => 2
        ]);

        $com = new \App\Console\Commands\getSchoolData();

        $reflector = new ReflectionClass( 'App\Console\Commands\getSchoolData' );
        $method = $reflector->getMethod( 'odp_cleanup' );
        $method->setAccessible( true );

        $result = $method->invokeArgs($com,  [[$odp->id], 3] );

        $sk = \App\Models\ObjectDotationPrices::where("object_id", '=', 3)->get()->count();
        $this->assertEquals(2, $sk);

        $sk = \App\Models\ObjectDotationPrices::where("object_id", '!=', 3)->get()->count();
        $this->assertEquals(1, $sk);
    }

    /**
     * @group artisan
     */
    public function test_check_seller_reg_nr_diff()
    {
        $school_array["Name"] = 'Skola';
        $reg_nr = 123;
        $regnr_array = [321];

        $o = factory(App\Models\Objekts::class)->create([
            'object_regnr' => $reg_nr,
        ]);

        factory(App\Models\Objekts::class)->create([
            'object_regnr' => 1234,
        ]);

        factory(App\Models\Mpos::class, 2)->create([
            'object_id' => $o->id,
            "owner_id" => 2
        ]);

        factory(App\Models\Seller::class)->create([
            'owner_id' => 2,
            "reg_nr" => 321
        ]);

        $com = new \App\Console\Commands\getSchoolData();

        $reflector = new ReflectionClass( 'App\Console\Commands\getSchoolData' );
        $method = $reflector->getMethod( 'check_seller_reg_nr_diff' );
        $method->setAccessible( true );

        $result = $method->invokeArgs($com,  [$reg_nr, $regnr_array, $school_array] );

        $this->assertEquals($result, '');

        factory(App\Models\Seller::class)->create([
            'owner_id' => 2,
            "reg_nr" => 333
        ]);

        $result = $method->invokeArgs($com,  [$reg_nr, $regnr_array, $school_array] );

        $this->assertContains('Nesakrīt tirgotāju reg. nr.', $result);
        $this->assertContains('Esošie: 321, 333', $result);
        $this->assertContains('Saņemtie: 321', $result);
    }

    /**
     * @group artisan
     */
    public function test_save_odp()
    {
        $price_in_cents = 10;
        $heading = 'name';
        $single_price_per_object = 0;

        $o = factory(App\Models\Objekts::class)->create([
            'object_regnr' => 123,
        ]);

        $odp = factory(App\Models\ObjectDotationPrices::class)->create([
            'object_id' => $o->id,
            "seller_account_id" => 2,
            "price" => $price_in_cents,
        ]);

        $com = new \App\Console\Commands\getSchoolData();

        $reflector = new ReflectionClass( 'App\Console\Commands\getSchoolData' );
        $method = $reflector->getMethod( 'save_odp' );
        $method->setAccessible( true );

        $result = $method->invokeArgs($com,  [$odp, $price_in_cents, $heading, $single_price_per_object] );

        $this->assertEquals($result, $odp->id);


        $o2 = factory(App\Models\Objekts::class)->create([
            'id' => 333,
            'object_regnr' => 333,
        ]);

        $result = $method->invokeArgs($com,  [$o2, 20, $heading, $single_price_per_object] );

        $this->assertNotEquals($result, $o2->id);

        $odp = \App\Models\ObjectDotationPrices::where("id", '=', $result)->first();
        $this->assertTrue( isset ($odp->id));
        $this->assertEquals($result, $odp->id);
        $this->assertEquals(20, $odp->price);
        $this->assertEquals($heading, $odp->heading);

    }

}