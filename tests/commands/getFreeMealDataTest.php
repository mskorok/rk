<?php

ini_set('memory_limit','228M');


class getFreeMealDataTest extends TestCase
{
    /**
     * @group artisan
     */
    public function test_update_eticket_objects()
    {
        $eticket = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567890,
        ]);

        $objekts = factory(App\Models\Objekts::class)->create([
            'object_regnr' => 123,
        ]);

        $eto = factory(App\Models\EticketsObjekti::class)->create([
            'object_id' => $objekts->id,
            'eticket_id' => $eticket->id,
        ]);

        $com = new \App\Console\Commands\getFreeMealPersonData();

        $reflector = new ReflectionClass( 'App\Console\Commands\getFreeMealPersonData' );
        $method = $reflector->getMethod( 'update_eticket_objects' );
        $method->setAccessible( true );

        $result = $method->invokeArgs($com,  [$eticket, $objekts] );

        $sk = \App\Models\EticketsObjekti::where("id", '=', $eto->id)->get()->count();
        $this->assertEquals(0, $sk);

        $sk = \App\Models\EticketsObjekti::where("eticket_id", '=', $eticket->id)->where("pirma", '=', 1)->get()->count();
        $this->assertEquals(1, $sk);

        // check grouped
        factory(App\Models\GroupObjekts::class)->create([
            'object_id' => $objekts->id,
        ]);

        factory(App\Models\GroupObjekts::class)->create([
            'object_id' => 100,
        ]);

        $result = $method->invokeArgs($com,  [$eticket, $objekts] );

        $sk = \App\Models\EticketsObjekti::where("eticket_id", '=', $eticket->id)->where("pirma", '=', 1)->get()->count();
        $this->assertEquals(1, $sk);

        $sk = \App\Models\EticketsObjekti::where("eticket_id", '=', $eticket->id)->where("pirma", '=', 0)->get()->count();
        $this->assertEquals(1, $sk);
    }

    /**
     * @group artisan
     */
    public function test_cleanup()
    {
        $used_pks = ['111111-22222'];

        $et_not_to_touch = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567890,
            'pk' => '111111-22222',
            'free_meals_from' => '2016-02-02',
            'free_meals_until' => '2017-02-02',
        ]);

        $etb_not_to_touch = factory(App\Models\EticketsBase::class)->create([
            'etickets' => 1234567890,
            'pk' => '111111-22222',
            'free_meals_from' => '2016-02-02',
            'free_meals_until' => '2017-02-02',
        ]);

        $et_to_clear = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567891,
            'pk' => '111111-33333',
            'free_meals_from' => '2016-02-02',
            'free_meals_until' => '2017-02-02',
        ]);

        $etb_to_clear = factory(App\Models\EticketsBase::class)->create([
            'etickets' => 1234567891,
            'pk' => '111111-33333',
            'free_meals_from' => '2016-02-02',
            'free_meals_until' => '2017-02-02',
        ]);

        $com = new \App\Console\Commands\getFreeMealPersonData();

        $reflector = new ReflectionClass( 'App\Console\Commands\getFreeMealPersonData' );
        $method = $reflector->getMethod( 'cleanup' );
        $method->setAccessible( true );

        $result = $method->invokeArgs($com,  [$used_pks] );

        $sk = \App\Models\Eticket::where("free_meals_from", '=', '0000-00-00')->get()->count();
        $this->assertEquals(1, $sk);

        $et = \App\Models\Eticket::where("id", '=', $et_not_to_touch->id)->first();
        $this->assertEquals('2016-02-02', $et->free_meals_from);
        $this->assertEquals('2017-02-02', $et->free_meals_until);

        $sk = \App\Models\EticketsBase::where("free_meals_from", '=', '0000-00-00')->get()->count();
        $this->assertEquals(1, $sk);

        $et = \App\Models\Eticket::where("id", '=', $etb_not_to_touch->id)->first();
        $this->assertEquals('2016-02-02', $et->free_meals_from);
        $this->assertEquals('2017-02-02', $et->free_meals_until);

    }

}