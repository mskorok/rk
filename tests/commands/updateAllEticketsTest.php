<?php

ini_set('memory_limit','228M');


class updateAllEticketsTest extends TestCase
{
    /**
     * @group artisan
     */
    public function test_process_all_persons_from_db()
    {
        $et1 = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567890,
            'pk' => '111111-22222',
            'free_meals_from' => '2016-02-02',
            'free_meals_until' => '2017-02-02',
            'grade' => '2a',
        ]);

        $person_data = [
            "111111-22222" => [
                "grade" => '3a',
                "name" => 'Janis Berzins',
                "school_id" => 1,
                'free_meals_from' => '0000-00-00',
                'free_meals_until' => '0000-00-00',
                'second_meals_from' => '2016-10-10',
                'second_meals_until' => '2017-10-10',
                'third_meals_from' => '0000-00-00',
                'third_meals_until' => '0000-00-00',
                'kopgalds_from' => '0000-00-00',
                'kopgalds_until' => '0000-00-00',
            ]
        ];

        $com = new \App\Console\Commands\updateAllEtickets();

        $reflector = new ReflectionClass( 'App\Console\Commands\updateAllEtickets' );
        $method = $reflector->getMethod( 'process_all_persons_from_db' );
        $method->setAccessible( true );

        $refProperty = $reflector->getProperty( 'pk_etickets' );
        $refProperty->setAccessible( true );
        $refProperty->setValue($com, ["111111-22222" => ["1234567890"]]);

        $result = $method->invokeArgs($com,  [$person_data] );

        $sk = \App\Models\EticketsBase::all()->count();
        $this->assertEquals(1, $sk);

        $etb = \App\Models\EticketsBase::where("pk","111111-22222")->first();
        $this->assertEquals($etb->grade, "3a");
        $this->assertEquals($etb->school_id, "1");
        $this->assertEquals($etb->name, "Janis");
        $this->assertEquals($etb->surname, "Berzins");
        $this->assertEquals($etb->free_meals_from, "0000-00-00");
        $this->assertEquals($etb->free_meals_until, "0000-00-00");
        $this->assertEquals($etb->second_meals_from, "2016-10-10");
        $this->assertEquals($etb->second_meals_until, "2017-10-10");

        // test ETB update case
        // test two first names
        // first skip if exual eticket nrs.

        $person_data = [
            "111111-22222" => [
                "grade" => '4a',
                "name" => 'Janis Juris Berzins',
                "school_id" => 1,
                'free_meals_from' => '0000-00-00',
                'free_meals_until' => '0000-00-00',
                'second_meals_from' => '2016-10-10',
                'second_meals_until' => '2017-10-11',
                'third_meals_from' => '0000-00-00',
                'third_meals_until' => '0000-00-00',
                'kopgalds_from' => '0000-00-00',
                'kopgalds_until' => '0000-00-00',
            ]
        ];

        $result = $method->invokeArgs($com,  [$person_data] );

        $sk = \App\Models\EticketsBase::all()->count();
        $this->assertEquals(1, $sk);

        $etb = \App\Models\EticketsBase::where("pk","111111-22222")->first();
        $this->assertEquals($etb->grade, "3a");
        $this->assertEquals($etb->name, "Janis");
        $this->assertEquals($etb->surname, "Berzins");

        // update

        $etb->etickets = '123';
        $etb->forceSave();

        $result = $method->invokeArgs($com,  [$person_data] );

        $sk = \App\Models\EticketsBase::all()->count();
        $this->assertEquals(1, $sk);

        $etb = \App\Models\EticketsBase::where("pk","111111-22222")->first();
        $this->assertEquals($etb->etickets, "1234567890");
        $this->assertEquals($etb->grade, "4a");
        $this->assertEquals($etb->school_id, "1");
        $this->assertEquals($etb->name, "Janis Juris");
        $this->assertEquals($etb->surname, "Berzins");
        $this->assertEquals($etb->free_meals_from, "0000-00-00");
        $this->assertEquals($etb->free_meals_until, "0000-00-00");
        $this->assertEquals($etb->second_meals_from, "2016-10-10");
        $this->assertEquals($etb->second_meals_until, "2017-10-11");
    }

    /**
     * @group artisan2
     */
    public function test_process_single_eticket_from_db()
    {
        $person_data = [
            "111111-22222" => [
                "grade" => '3a',
                "name" => 'Janis Berzins',
                "school_id" => 1,
                "object_id" => '1',
                "nr" => '1234567890',
            ]
        ];

        $eticket_row = [
            "ExpirationDate" => "2016-01-01",
            "PersonCode" => "111111-22222",
            "SerialNumber" => "1234567890",
            "Blacklisted" => "false",
        ];

        // no etickets before
        $sk = \App\Models\Eticket::all()->count();
        $this->assertEquals(0, $sk);

        $com = new \App\Console\Commands\updateAllEtickets();

        $reflector = new ReflectionClass( 'App\Console\Commands\updateAllEtickets' );
        $method = $reflector->getMethod( 'process_single_eticket_from_db' );
        $method->setAccessible( true );

        $refProperty = $reflector->getProperty( 'pk_etickets' );
        $refProperty->setAccessible( true );
        $refProperty->setValue($com, ["111111-22222" => ["1234567890"]]);

        // test expired eticket
        $result = $method->invokeArgs($com,  [$eticket_row, $person_data] );

        $sk = \App\Models\Eticket::all()->count();
        $this->assertEquals(0, $sk);

        // test valid eticket without freemeal data
        $person_data = [
            "111111-22222" => [
                "grade" => '3a',
                "name" => 'Janis Berzins',
                "school_id" => 1,
                'FreeMeals' => [
                    "DateFrom" => '0000-00-00',
                    "DateTill" => '0000-00-00',
                ],
                "object_id" => '1',
                "nr" => '1234567890',
            ]
        ];

        $eticket_row["ExpirationDate"] = "2020-01-01";

        $refProperty->setValue($com, ["111111-22222" => []]);
        $result = $method->invokeArgs($com,  [$eticket_row, $person_data] );

        $sk = \App\Models\DailyLogRk::all()->count();
        $this->assertEquals(1, $sk);

        $log = \App\Models\DailyLogRk::first();
        $this->assertEquals($log->datums, date("Y-m-d"));
        $this->assertEquals($log->pk, $eticket_row["PersonCode"]);
        $this->assertEquals($log->nr, $eticket_row["SerialNumber"]);
        $this->assertEquals($log->expires, $eticket_row["ExpirationDate"]);
        $this->assertEquals($log->blacklisted, 0);

        $this->assertEquals(["111111-22222" => [$eticket_row["SerialNumber"]]], $refProperty->getValue($com));

        $sk = \App\Models\Eticket::all()->count();
        $this->assertEquals(1, $sk);

        $et = \App\Models\Eticket::first();
        $this->assertEquals($et->nr, $eticket_row["SerialNumber"]);
        $this->assertEquals($et->pk, $eticket_row["PersonCode"]);
        $this->assertGreaterThan(0, $et->account->id);
        $this->assertEquals($et->name, 'Janis Berzins');
        $this->assertEquals($et->grade, '3a');
        $this->assertEquals($et->free_meals_from, '0000-00-00');

        // test valid eticket with freemeal
        $person_data = [
            "111111-22222" => [
                "grade" => '3a',
                "name" => 'Janis Berzins',
                "school_id" => 1,
                'FreeMeals' => [
                    "FreeMealPeriod" => [
                        "DateFrom" => '2016-01-01',
                        "DateTill" => '2017-01-01',
                    ],
                ],
                "object_id" => '1',
                "nr" => '1234567890',
            ]
        ];

        $dot1 = factory(App\Models\Dotations::class)->create([
            'pk' => "111111-22222",
            'dotation_id' => 1,
        ]);

        $et->delete();

        $sk = \App\Models\Eticket::all()->count();
        $this->assertEquals(0, $sk);

        $refProperty->setValue($com, ["111111-22222" => []]);
        $result = $method->invokeArgs($com,  [$eticket_row, $person_data] );

        $sk = \App\Models\Eticket::all()->count();
        $this->assertEquals(1, $sk);

        $et = \App\Models\Eticket::first();
        $this->assertEquals($et->nr, $eticket_row["SerialNumber"]);
        $this->assertEquals($et->pk, $eticket_row["PersonCode"]);
        $this->assertGreaterThan(0, $et->account->id);
        $this->assertEquals($et->name, 'Janis Berzins');
        $this->assertEquals($et->grade, '3a');
        $this->assertEquals('2016-01-01', $et->free_meals_from);
        $this->assertEquals('2017-01-01', $et->free_meals_until);

    }

}