<?php

ini_set('memory_limit','228M');

use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;

class updateTransactionsDailyTest extends TestCase
{
    /**
     * @group artisan
     */
    public function testNoValidTransactions()
    {
        factory(App\Models\Transaction::class)->create([
            'dotation_id' => 1,
            'seller_reg_nr' => "123",
        ]);
        factory(App\Models\Transaction::class)->create([
            'seller_reg_nr' => "123",
        ]);
        factory(App\Models\Transaction::class)->create();

        $sk = \App\Models\Transaction::count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(3, $sk);

        Artisan::call('updatetrans');
        $resultAsText = Artisan::output();

        $this->assertContains('Vakardienas transakciju informācijas atjaunošana sekmīgi pabeigta', $resultAsText);

        $sk = \App\Models\Transaction::where("freemeals_type", '!=', 0)->get()->count();
        $this->assertEquals(0, $sk);

        $sk = \App\Models\Transaction::where("seller_reg_nr", '!=', "")->get()->count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(2, $sk);
    }

    /**
     * @group artisan
     */
    public function testTransactionsWithoutEticket()
    {
        factory(App\Models\Transaction::class, 3)->create([
            'dotation_id' => 1,
            'created_at' => Carbon::now()->subDay(),
            'mpos_id' => 12,
        ]);

        $sk = \App\Models\Transaction::count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(3, $sk);

        Artisan::call('updatetrans');
        $resultAsText = Artisan::output();

        $this->assertContains('Vakardienas transakciju informācijas atjaunošana sekmīgi pabeigta', $resultAsText);

        $sk = \App\Models\Transaction::where("freemeals_type", '=', 99)->get()->count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(3, $sk);
    }

    /**
     * @group artisan
     */
    public function testValidTransactions()
    {

        factory(App\Models\Seller::class)->create([
            'owner_id' => 1,
        ]);
        factory(App\Models\Seller::class)->create([
            'owner_id' => 2,
            'reg_nr' => '11111111111',
        ]);

        factory(App\Models\Mpos::class)->create([
            'owner_id' => 1,
        ]);
        $mpos_id = factory(App\Models\Mpos::class)->create([
            'owner_id' => 2,
        ])->id;

        // without free meals
        factory(App\Models\Eticket::class)->create([
            'nr' => '1234567890',
        ]);

        factory(App\Models\Transaction::class)->create([
            'dotation_id' => 1,
            'from_eticket_nr' => '1234567890',
            'created_at' => Carbon::now()->subDay(),
            'mpos_id' => $mpos_id,
        ]);

        // free meals
        $e1 = factory(App\Models\Eticket::class)->create([
            'nr' => '1234567891',
            'free_meals_from' => '2016-01-01',
            'free_meals_until' => '2020-01-01',
        ]);

        factory(App\Models\Transaction::class)->create([
            'dotation_id' => 1,
            'from_eticket_nr' => '1234567891',
            'created_at' => Carbon::now()->subDay(),
            'mpos_id' => $mpos_id,
        ]);

        // second free meals
        $e2 = factory(App\Models\Eticket::class)->create([
            'nr' => '1234567892',
            'second_meals_from' => '2016-01-01',
            'second_meals_until' => '2020-01-01',
        ]);

        factory(App\Models\Transaction::class)->create([
            'dotation_id' => 1,
            'from_eticket_nr' => '1234567892',
            'created_at' => Carbon::now()->subDay(),
            'mpos_id' => $mpos_id,
        ]);

        // third free meals
        $e3 = factory(App\Models\Eticket::class)->create([
            'nr' => '1234567893',
            'third_meals_from' => '2016-01-01',
            'third_meals_until' => '2020-01-01',
        ]);

        factory(App\Models\Transaction::class)->create([
            'dotation_id' => 1,
            'from_eticket_nr' => '1234567893',
            'created_at' => Carbon::now()->subDay(),
            'mpos_id' => $mpos_id,
        ]);

        $sk = \App\Models\Transaction::where("freemeals_type", '=', 1)->get()->count();
        $this->assertEquals(0, $sk);

        $sk = \App\Models\Transaction::where("freemeals_type", '=', 2)->get()->count();
        $this->assertEquals(0, $sk);

        $sk = \App\Models\Transaction::where("freemeals_type", '=', 3)->get()->count();
        $this->assertEquals(0, $sk);

        Artisan::call('updatetrans');
        $resultAsText = Artisan::output();

        $this->assertContains('Vakardienas transakciju informācijas atjaunošana sekmīgi pabeigta', $resultAsText);

        $sk = \App\Models\Transaction::where("freemeals_type", '=', 99)->get()->count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(1, $sk);

        $sk = \App\Models\Transaction::where("freemeals_type", '=', 1)->get()->count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(1, $sk);

        $sk = \App\Models\Transaction::where("freemeals_type", '=', 2)->get()->count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(1, $sk);

        $sk = \App\Models\Transaction::where("freemeals_type", '=', 3)->get()->count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(1, $sk);

        $sk = \App\Models\Transaction::where("seller_reg_nr", '=', '11111111111')->get()->count();
        $this->assertTrue( ! empty($sk));
        $this->assertEquals(4, $sk);

    }
}