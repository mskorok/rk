<?php

ini_set('memory_limit','228M');


class updateDotationsDailyTest extends TestCase
{
    /**
     * @group artisan
     */
    public function test_process_object()
    {
        $o = factory(App\Models\Objekts::class)->create([
            'object_regnr' => 123,
        ]);

        $sp = factory(App\Models\SchoolPrices::class)->create([
            'object_id' => $o->id,
        ]);

        $et1 = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567890,
            'pk' => '111111-22222',
            'free_meals_from' => '2016-02-02',
            'free_meals_until' => '2017-02-02',
            'grade' => '1a',
        ]);

        $eto1 = factory(App\Models\EticketsObjekti::class)->create([
            'object_id' => $o->id,
            'eticket_id' => $et1->id,
        ]);

        $et2 = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567899,
            'pk' => '111111-99999',
            'free_meals_from' => '0000-00-00',
            'free_meals_until' => '0000-00-00',
        ]);

        $eto2 = factory(App\Models\EticketsObjekti::class)->create([
            'object_id' => $o->id,
            'eticket_id' => $et2->id,
        ]);

        $et3 = factory(App\Models\Eticket::class)->create([
            'nr' => 1234567898,
            'pk' => '111111-88888',
            'free_meals_from' => '2016-02-02',
            'free_meals_until' => '2016-05-02',
            'grade' => '1a',
        ]);

        $eto3 = factory(App\Models\EticketsObjekti::class)->create([
            'object_id' => $o->id,
            'eticket_id' => $et3->id,
        ]);

        $odp = factory(App\Models\ObjectDotationPrices::class)->create([
            'object_id' => $o->id,
            "seller_account_id" => 2,
            "price" => 100,
        ]);

        $dot1 = factory(App\Models\Dotations::class)->create([
            'pk' => $et1->pk,
            'dotation_id' => $odp->id,
        ]);

        $dot2 = factory(App\Models\Dotations::class)->create([
            'pk' => $et2->pk,
            'dotation_id' => $odp->id,
        ]);

        $com = new \App\Console\Commands\updateDotationsDaily();

        $reflector = new ReflectionClass( 'App\Console\Commands\updateDotationsDaily' );
        $method = $reflector->getMethod( 'process_object' );
        $method->setAccessible( true );

        $result = $method->invokeArgs($com,  [$o->id] );

        // rollback due to 2:1
        $this->assertEquals(1, $result);

        $sk = \App\Models\Dotations::all()->count();
        $this->assertEquals(2, $sk);

        $res = \App\Models\Dotations::where("id", $dot1->id)->first();
        $this->assertTrue( isset ($res->id));
        $this->assertEquals($et1->pk, $res->pk);
        $this->assertEquals($dot1->dotation_id, $res->dotation_id);
        $this->assertEquals($dot1->import_date, $res->import_date);
        $this->assertEquals($dot1->from_date, $res->from_date);
        $this->assertEquals($dot1->until_date, $res->until_date);

        $res = \App\Models\Dotations::where("id", $dot2->id)->first();
        $this->assertTrue( isset ($res->id));
        $this->assertEquals($et2->pk, $res->pk);
        $this->assertEquals($dot2->dotation_id, $res->dotation_id);
        $this->assertEquals($dot2->import_date, $res->import_date);
        $this->assertEquals($dot2->from_date, $res->from_date);
        $this->assertEquals($dot2->until_date, $res->until_date);

        // remove second dotation
        $dot2->delete();

        $result = $method->invokeArgs($com,  [$o->id] );

        // changes in db
        $this->assertEquals(1, $result);

        $sk = \App\Models\Dotations::all()->count();
        $this->assertEquals(1, $sk);

        $res = \App\Models\Dotations::where("id", $dot1->id)->first();
        $this->assertTrue( isset ($res->id));
        $this->assertEquals($et1->pk, $res->pk);
        $this->assertEquals($dot1->dotation_id, $res->dotation_id);
        $this->assertEquals(date("Y-m-d"), $res->import_date);
        $this->assertEquals($et1->free_meals_from, $res->from_date);
        $this->assertEquals($et1->free_meals_until, $res->until_date);

        // remove all dotations
        $dot1->delete();

        $result = $method->invokeArgs($com,  [$o->id] );

        // changes in db
        $this->assertEquals(1, $result);

        $sk = \App\Models\Dotations::all()->count();
        $this->assertEquals(1, $sk);

        $dot1 = \App\Models\Dotations::first();

        $res = \App\Models\Dotations::where("id", $dot1->id)->first();
        $this->assertTrue( isset ($res->id));
        $this->assertEquals($et1->pk, $res->pk);
        $this->assertEquals($odp->id, $res->dotation_id);
        $this->assertEquals(date("Y-m-d"), $res->import_date);
        $this->assertEquals($et1->free_meals_from, $res->from_date);
        $this->assertEquals($et1->free_meals_until, $res->until_date);
    }

}