<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

ini_set('memory_limit', '228M');

class TestCase extends Illuminate\Foundation\Testing\TestCase {

    //use DatabaseMigrations;
    use DatabaseTransactions;

	protected $baseUrl = 'http://skolas';
    protected $elastic;

    /**
     * Default preparation for each test, between every test
     *
     */
    public function setUp()
    {
        parent::setUp();

        Artisan::call('migrate');
        Artisan::call('db:seed');
        Cache::flush();
    }

    public function tearDown()
    {
        parent::tearDown();

        //Artisan::call('migrate:reset');
    }

	/**
	 * Creates the application.
	 *
	 * @return \Illuminate\Foundation\Application
	 */
	public function createApplication()
	{
        putenv('DB_DEFAULT=sqlite_testing');
        putenv('MAIL_DRIVER=log');

		$app = require __DIR__.'/../bootstrap/app.php';

//        $this->elastic = $this->getMockForTrait('\Aloko\Elasticquent\ElasticquentTrait');
//        $app->instance('\Aloko\Elasticquent\ElasticquentTrait', $this->elastic);

		$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

        Hash::setRounds(5);

		return $app;
	}

    protected function getResponseData($response, $key)
    {
        $content = $response->getOriginalContent();

        $content = $content->getData();

        return $content[$key];
    }

    protected function create_user_with_role($role_id = 1)
    {
        $exists = User::where("email",'user@user.lv')->first();
        if (isset ($exists->id))
            return $exists;

        $user = New User();
        $user->username = 'Regular';
        $user->surname = 'User';
        $user->email = 'user@user.lv';
        $user->confirmed = 1;
        $user->terms = 1;
        $user->forceSave();
        $user->roles()->sync([$role_id]);
        return $user;
    }

    protected function create_admin_user()
    {
        $role = Role::all()->first();

        $exists = User::where("email",'admin@user.lv')->first();
        if (isset ($exists->id))
            return $exists;

        $user = New User();
        $user->username = 'Admin';
        $user->surname = 'User';
        $user->email = 'admin@user.lv';
        $user->confirmed = 1;
        $user->terms = 1;
        $user->forceSave();
        $user->roles()->sync([$role->id]);
        return $user;
    }

    protected function create_regular_user($username = 'Regular', $email = 'regular@user.lv')
    {
        $exists = User::where("email",$email)->first();
        if (isset ($exists->id))
            return $exists;

        $user = New User();
        $user->username = $username;
        $user->surname = 'User';
        $user->email = $email;
        $user->confirmed = 1;
        $user->terms = 1;
        $user->created_at = date("Y-m-d");
        $user->updated_at = date("Y-m-d");
        $user->forceSave();
        return $user;
    }

}
