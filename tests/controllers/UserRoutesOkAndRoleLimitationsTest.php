<?php
use App\Models\Account;
use App\Models\Owner;
use App\Models\OwnerAccess;
use App\Models\User;

ini_set('memory_limit','228M');

class UserRoutesOkAndRoleLimitationsTest extends TestCase
{
	public function __construct()
	{
	}

	public function tearDown()
	{
	}

	public function testRegularUserAdminFails()
	{
		$this->be($this->create_regular_user());

		$this->visit('/admin')
			->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

		$this->assertTrue($this->response->isOk());

        $this->visit('/admin/logviewer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/account/fetch/Test')
        ->seePageIs('/')
        ->see("Sveiki")
        ->see("Regular")
        ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objekts')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objetickets')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objseller')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/seller')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/selltotals')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/entrance')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/akts')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/outgoing/history')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/accountant/mtransfer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/skola/index')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/rk/index')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());
	}

    public function testAdminOk()
    {
        $this->be($this->create_admin_user());

        $this->visit('/admin')
            ->seePageIs('/admin')
            ->see("Admin User")
            ->see("Operācijas");

        $this->assertTrue($this->response->isOk());

        $this->visit('/admin/logviewer')
            ->seePageIs('/admin/logviewer')
            ->see("Level");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/account/fetch/Test')
            ->seePageIs('/reports/account/fetch/Test')
            ->see("[")
            ->see("]");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objekts')
            ->seePageIs('/reports/objekts')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Vēsture - objekta vēsture");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objetickets')
            ->seePageIs('/reports/objetickets')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Objekta e-talonu atskaite");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objseller')
            ->seePageIs('/reports/objseller')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Objekta tirgotāju atskaite");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/seller')
            ->seePageIs('/reports/seller')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Vēsture - tirgotāja vēsture");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/selltotals')
            ->seePageIs('/reports/selltotals')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Tirgotāja kopējais pārskats");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/entrance')
            ->seePageIs('/reports/entrance')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Objekta lietotāju pārvietošanās atskaite");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/akts')
            ->seePageIs('/reports/akts')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Izveidot salīdzināšanas aktu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/outgoing/history')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/accountant/mtransfer')
            ->seePageIs('/accountant/mtransfer')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Ienākošie pārskaitījumi, kas gaida uz apstiprināšanu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/skola/index')
            ->seePageIs('/skola/index')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Apstiprināt E-talonus");

        $this->assertTrue($this->response->isOk());

        $this->visit('/rk/index')
            ->seePageIs('/rk/index')
            ->see("Sveiki")
            ->see("Admin User")
            ->see("Apstiprināt lietotājus");

        $this->assertTrue($this->response->isOk());
    }

    public function testGramatvedisUser()
    {
        $this->be($this->create_user_with_role(4));

        $this->visit('/admin')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/admin/logviewer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/account/fetch/Test')
            ->seePageIs('/reports/account/fetch/Test')
            ->see("[")
            ->see("]");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objekts')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objetickets')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objseller')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/seller')
            ->seePageIs('/reports/seller')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Vēsture - tirgotāja vēsture");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/selltotals')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/entrance')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/akts')
            ->seePageIs('/reports/akts')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Izveidot salīdzināšanas aktu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/outgoing/history')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/accountant/mtransfer')
            ->seePageIs('/accountant/mtransfer')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Ienākošie pārskaitījumi, kas gaida uz apstiprināšanu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/skola/index')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/rk/index')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());
    }

    public function testObjektsUser()
    {
        $this->be($this->create_user_with_role(2));

        $this->visit('/admin')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/admin/logviewer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/account/fetch/Test')
        ->seePageIs('/')
        ->see("Sveiki")
        ->see("Regular")
        ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objekts')
            ->seePageIs('/reports/objekts')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Vēsture - objekta vēsture");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objetickets')
            ->seePageIs('/reports/objetickets')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Objekta e-talonu atskaite");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objseller')
            ->seePageIs('/reports/objseller')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Objekta tirgotāju atskaite");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/seller')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/selltotals')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/entrance')
            ->seePageIs('/reports/entrance')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Objekta lietotāju pārvietošanās atskaite");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/akts')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/outgoing/history')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/accountant/mtransfer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/skola/index')
            ->seePageIs('/skola/index')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Apstiprināt E-talonus");

        $this->assertTrue($this->response->isOk());

        $this->visit('/rk/index')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());
    }

    public function testTirgotajsUser()
    {
        $u = $this->create_user_with_role(3);

        // seed minimal seller records
        $o = new Owner();
        $o->owner_type = 1;
        $o->owner_name = 'Vards';
        $o->forceSave();

        $oa = new OwnerAccess();
        $oa->user_id = $u->id;
        $oa->owner_id = $o->id;
        $oa->access_status = 3;
        $oa->access_level = 1;
        $oa->forceSave();

        $a = new Account();
        $a->account_type = 3;
        $a->account_status = 1;
        $a->owner_id = $o->id;
        $a->forceSave();

        $af = new Account();
        $af->account_type = 7;
        $af->account_status = 1;
        $af->owner_id = $o->id;
        $af->forceSave();

        $this->be($u);

        $this->visit('/admin')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/admin/logviewer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/account/fetch/Test')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objekts')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objetickets')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objseller')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/seller')
            ->seePageIs('/reports/seller')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Vēsture - tirgotāja vēsture");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/selltotals')
            ->seePageIs('/reports/selltotals')
            ->see("Sveiki")
            ->see("Regular User")
            ->see("Tirgotāja kopējais pārskats");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/entrance')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/akts')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/outgoing/history')
            ->seePageIs('/outgoing/history')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Izmaksu vēsture");

        $this->assertTrue($this->response->isOk());

        $this->visit('/accountant/mtransfer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/skola/index')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/rk/index')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());
    }

    public function testRkUser()
    {
        $this->be($this->create_user_with_role(5));

        $this->visit('/admin')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/admin/logviewer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/account/fetch/Test')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objekts')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objetickets')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/objseller')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/seller')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/selltotals')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/entrance')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/reports/akts')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/outgoing/history')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/accountant/mtransfer')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/skola/index')
            ->seePageIs('/')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Labot profilu");

        $this->assertTrue($this->response->isOk());

        $this->visit('/rk/index')
            ->seePageIs('/rk/index')
            ->see("Sveiki")
            ->see("Regular")
            ->see("Apstiprināt lietotājus");

        $this->assertTrue($this->response->isOk());
    }
}