<?php
ini_set('memory_limit','228M');

use App\Models\Mpos;
use Mockery as m;

class MPOSControllerTest extends TestCase 
{
	public function __construct()
	{
	  $this->mock_sell = m::mock('App\Models\Sellers');
	  $this->mock_obj = m::mock('App\Models\Objekts');
	  $this->mock_mpos = m::mock('App\Models\Mpos');
	}

	public function tearDown()
	{
	  Mockery::close();
	}

	/**
	 * @group mpos
     */
	public function testIndex()
	{
		$this->be($this->create_admin_user());

		$this->visit('/admin/mpos');

		$this->assertTrue($this->response->isOk());
	}

	public function testCreateForm()
	{
	    $this->mock_mpos
           ->shouldReceive('all')
           ->once();

		$this->be($this->create_admin_user());

		$this->app->instance('sellers', $this->mock_sell);
      	$this->app->instance('objekti', $this->mock_obj);

      	$this->call('GET', 'admin/mpos/create');

      	$this->assertViewHas('objekti');
      	$this->assertViewHas('sellers');

		$this->visit('/admin/mpos/create')
			->see('Izveidot jaunu iekārtu');

		$this->assertTrue($this->response->isOk());
	}

	public function testSaveMPOSFailure()
	{
        $this->be($this->create_admin_user());

        $this->visit('admin/mpos/create');
        $this->assertTrue($this->response->isOk());

        $this->visit('/admin/mpos/create')
            ->type('', 'mpos_name')
            ->type('V1', 'firmware_version')
            ->press('Apstiprināt')
            ->seePageIs('/admin/mpos/create')
            ->see("Kļūda!");
	}

	public function testSaveMPOSSuccess()
	{
        $this->be($this->create_admin_user());

        $this->call('POST', '/admin/mpos/create',array(
            'mpos_name' => 'MPOS1',
            'mpos_type' => '1',
            'owner_id' => 0,
            'account_id' => 0,
            'object_id' => 0,
            'operation_id' => 0)
        );

        // check from db
        $u = Mpos::orderBy('id','desc')->first();
        $this->assertEquals($u->mpos_name,'MPOS1');
     
        // Should redirect to collection, with a success flash message
        $this->assertRedirectedTo('/admin/mpos/'.$u->id.'/edit');
        $this->assertSessionHas('success');
	}

}