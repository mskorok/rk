<?php
ini_set('memory_limit','228M');

use App\Models\EticketsBase;
use App\Models\Objekts;
use App\Models\School;
use Mockery as m;

class AdminEticketBaseControllerTest extends TestCase 
{
	public function __construct()
	{
	  $this->objmock = m::mock('App\Models\School');
	}

	public function tearDown()
	{
	  Mockery::close();
	}

	public function testIndex()
	{
		$this->be($this->create_admin_user());

		$this->visit('/admin/newbase')
			->see("Meklēt bāzes ierakstu");

		$this->assertTrue($this->response->isOk());
	}

	public function testCreateFormObj()
	{
	    $this->objmock
           ->shouldReceive('all')
           ->once();

        $this->be($this->create_admin_user());
 
      	$this->app->instance('skolas', $this->objmock);
 
      	$this->call('GET', 'admin/newbase/create');
 
      	$this->assertViewHas('skolas');
	}

	public function testCreateFormCrawl()
	{
        $this->be($this->create_admin_user());

        $this->visit('/admin/newbase/create')
            ->see('Izveidot bāzes ierakstu');

        $this->assertTrue($this->response->isOk());
	}

	public function testCreateSearchEdit()
	{
        $this->be($this->create_admin_user());

		// object for input
		$obj = new Objekts();
		$obj->id = 1;
		$obj->object_name = 'O';
		$obj->object_city = '';
		$obj->object_address = '';
		$obj->object_ip = '';
		$obj->forceSave();
		$o = Objekts::orderBy('id','desc')->first();
		$this->assertEquals($o->object_name,'O');

		// create
        $this->call('POST', '/admin/newbase/create',array(
            'name' => 'Vards',
            'surname' => 'Uzvards',
            'pk' => '111111-22222',
            'etickets' => '1010101010',
            'grade' => '1A',
            'skola' => 1,
        ));

        $u = EticketsBase::orderBy('id','desc')->first();
        $this->assertEquals($u->name,'Vards');
        $this->assertEquals($u->custom,1);

        $this->assertRedirectedTo('/admin/newbase/'.$u->id.'/edit');

		// successfull search
        $this->call('POST', '/admin/newbase',array(
            'eticket' => '1010101010'
        ));

        $this->assertRedirectedTo('/admin/newbase/'.$u->id.'/edit');

        // create School
        School::create(["id" => 1, 'heading' => 'Skola', 'changed' => '2016-01-01']);

		// edit found row
        $this->visit('/admin/newbase/1/edit')
            ->type('111111-33333', 'pk')
            ->press('Labot')
            ->seePageIs('/admin/newbase/1/edit')
            ->dontSee("Kļūda!")
            ->see("Dati saglabāti");
		
		// check edited data
		$u = EticketsBase::orderBy('id','desc')->first();
		$this->assertEquals($u->name,'Vards');
		$this->assertEquals($u->pk,'111111-33333');
		$this->assertEquals($u->custom,1);
	}

}