<?php
ini_set('memory_limit','228M');

use App\Models\Seller;
use Illuminate\Support\Facades\Input;
use Mockery as m;

class SellersControllerTest extends TestCase 
{
	public function __construct()
	{
	  $this->smock = m::mock('App\Models\Seller');
	}

	public function tearDown()
	{
	  Mockery::close();
	}

	public function testIndex()
	{
		$this->be($this->create_admin_user());

		$this->visit('/admin/sellers');

		$this->assertTrue($this->response->isOk());
	}

	public function testCreateForm()
	{
        $this->be($this->create_admin_user());

      	$this->visit('/admin/sellers/create')
			->see('Tirgotāji');

        $this->assertTrue($this->response->isOk());
	}

	public function testSaveFailure()
	{
        $this->be($this->create_admin_user());

        $this->visit('admin/sellers/create');
        $this->assertTrue($this->response->isOk());

	    // Set stage for failure
        Input::replace(['seller_name' => '']);

        $this->smock->shouldReceive('create')
            ->once()
            ->andReturn(Mockery::mock(array(
                'isSaved' => false,
                'errors' => array()
            )));

        $this->call('POST', '/admin/sellers',array(
            'seller_name' => '',
            'jur_adrese' => '1',
        ));
	}

	public function testSaveEditDeleteSuccess()
	{
        $this->be($this->create_admin_user());

        $this->smock
	         ->shouldReceive('create')
	         ->once()
             ->andReturn(Mockery::mock(array(
                'isSaved' => false,
                'errors' => array()
             )));

        $this->visit('/admin/sellers/create')
            ->type('Test', 'seller_name')
            ->type('adr', 'jur_adrese')
            ->type('111', 'talrunis')
            ->type('bank', 'banka')
            ->type('bankkods', 'bankas_kods')
            ->type('haba', 'konts')
            ->type('regnr', 'reg_nr')
            ->type('123', 'contract_nr')
            ->type('Jānis', 'contract_person')
            ->press('Apstiprināt')
            ->seePageIs('/admin/sellers/1/edit');

        // check from db
        $u = Seller::orderBy('id','desc')->first();
        $this->assertEquals($u->seller_name,'Test');

        // edit object
        $this->smock
	         ->shouldReceive('update')
	         ->once();

	    $this->call('POST', '/admin/sellers/'.$u->id.'/edit',array(
	    	'seller_name' => 'Testss',
	    	'jur_adrese' => 'adr',
	    	'talrunis' => '111',
	    	'banka' => 'bank',
	    	'bankas_kods' => 'bankkods',
	    	'konts' => 'haba',
	    	'reg_nr' => 'regnr',
            'contract_nr' => '123',
            'contract_person' => 'Jānis',
            '_token' => csrf_token()
	    ));

        // Should redirect to collection, with a success flash message
        $this->assertRedirectedTo('/admin/sellers/'.$u->id.'/edit');
        $this->assertSessionHas('success');

	    // check from db
	    $u = Seller::orderBy('id','desc')->first();
	    $this->assertEquals($u->seller_name,'Testss');

	    //delete object
        $this->visit('/admin/sellers/'.$u->id.'/delete');
        $this->press("Dzēst");

        //check in db
        $u = Seller::orderBy('id','desc')->first();
        $this->assertEquals(isset($u),false);
	}

}