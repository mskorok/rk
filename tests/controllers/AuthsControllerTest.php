<?php
ini_set('memory_limit','228M');

use App\Models\Auths;
use App\Models\Objekts;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use Mockery as m;

class AuthsControllerTest extends TestCase 
{
	public function __construct()
	{
	  $this->amock = m::mock('App\Models\Auths');
	  $this->omock = m::mock('App\Models\Objekts');
	}

	public function tearDown()
	{
	  Mockery::close();
	}

	public function testIndex()
	{
		$this->be($this->create_admin_user());

		$this->visit('/admin/mpos')
		->see("Autorizatori");

		$this->assertTrue($this->response->isOk());
	}

	public function testCreateForm()
	{
        $this->be($this->create_admin_user());
		
 		$this->app->instance('objekti', $this->omock);

      	$this->call('GET', 'admin/auths/create');

      	$this->assertViewHas('objekti');

        $this->visit('/admin/auths/create')
            ->see("Izveidot jaunu autorizatoru");

        $this->assertTrue($this->response->isOk());
	}

	public function testSaveFailure()
	{
        $this->be($this->create_admin_user());

        $this->visit('admin/auths/create');
        $this->assertTrue($this->response->isOk());

        $this->visit('/admin/auths/create')
            ->type('', 'authorisator_name')
            ->press('Apstiprināt')
            ->seePageIs('/admin/auths/create')
            ->see("Kļūda!");
	}

	public function testSaveEditDeleteSuccess()
	{
        $this->be($this->create_admin_user());

        // object for input
        $obj = new Objekts();
        $obj->id = 1;
        $obj->object_name = 'O';
        $obj->object_city = '';
        $obj->object_address = '';
        $obj->object_ip = '';
        $obj->forceSave();
        $o = Objekts::orderBy('id','desc')->first();
        $this->assertEquals($o->object_name,'O');

        $this->visit('/admin/auths/create')
            ->type('Test', 'authorisator_name')
            ->select('1', 'object_id')
            ->press('Apstiprināt')
            ->seePageIs('/admin/auths/1/edit')
            ->dontSee("Kļūda!");

        // check from db
        $u = Auths::orderBy('id','desc')->first();
        $this->assertEquals($u->authorisator_name,'Test');

        // edit object
	    $this->call('POST', '/admin/auths/'.$u->id.'/edit',array(
	    	'object_id' => '1',
	    	'authorisator_name' => 'Tests',
	    	'secret_key' => 'key'
	    ));

        // Should redirect to collection, with a success flash message
        $this->assertRedirectedTo('/admin/auths/'.$u->id.'/edit');
        $this->assertSessionHas('success');

	    // check from db
	    $u = Auths::orderBy('id','desc')->first();
	    $this->assertEquals($u->authorisator_name,'Tests');

	    //delete object
        $this->visit('/admin/auths/'.$u->id.'/delete');
        $this->press("Dzēst");

        //check in db
        $u = Auths::orderBy('id','desc')->first();
        $this->assertEquals(isset($u),false);
	}

}