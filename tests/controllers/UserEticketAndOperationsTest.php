<?php
ini_set('memory_limit','228M');

use App\Models\Settings;
use Mockery as m;

class UserEticketAndOperationsTest extends TestCase
{
	public function __construct()
	{
	  $this->smock = m::mock('App\Models\School');
	}

	public function tearDown()
	{
	  Mockery::close();
	}

	public function testIndex()
	{
		$this->be($this->create_admin_user());

		$this->visit('/etickets/etickets')
            ->see("E-taloni")
            ->see("Dzēst profilu");

		$this->assertTrue($this->response->isOk());
	}

	public function testCreateForm()
	{
        $this->be($this->create_admin_user());
 
      	$this->app->instance('skolas', $this->smock);
 
      	$this->visit('/etickets/etickets/create')
            ->see("Pievienot jaunu e-talonu");
 
      	$this->assertViewHas('skolas');

        $this->assertTrue($this->response->isOk());
	}

	public function testForceterms()
	{
		$this->be($this->create_admin_user());

		$this->visit('/user/forceterms')
			->see("Nepieciešams pabeigt skolas.rigaskarte.lv reģistrāciju")
			->see("Lai veiktu autorizāciju ar internet-banku")
			->see("spiediet šeit");

		$this->assertTrue($this->response->isOk());
	}

    public function testYearly()
    {
        $this->be($this->create_admin_user());

        $sett = Settings::first();
        $sett->yearly_text = 'test';
        $sett->save();

        $this->seeInDatabase('settings', ['yearly_text' => 'test']);

        $this->visit('/user/yearly')
            ->see("test")
            ->see("JĀ")
            ->see("NĒ")
            ->see("Nosūtīt") // hidden
            ->click("NĒ")
            ->seePageIs('/')
            ->see("Labot profilu")
            ->dontSee("Kļūda")
            ->see("Informācija veiksmīgi saglabāta!");

        $this->assertTrue($this->response->isOk());
    }

}