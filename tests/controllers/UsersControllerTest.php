<?php
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

ini_set('memory_limit', '228M');

class UsersControllerTest extends TestCase 
{
    public function __construct()
    {
    }

    public function tearDown()
    {
    }

    private function addpadding($string, $blocksize = 32)
    {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
        return $string;
    }

    private function encrypt($string = "")
    {
        $key = base64_decode(Config::get('services.rs.key'));
        $iv = base64_decode(Config::get('services.rs.iv'));

        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $this->addpadding($string), MCRYPT_MODE_CBC, $iv));
    }

	public function testLoginForm()
	{
		$this->visit('/auth/login')
			->see("Ienākt sistēmā")
			->see("E-pasts");

		$this->assertTrue($this->response->isOk());
	}

	public function testFailLogin()
	{
        $this->visit('/auth/login')
            ->type('a', 'email')
            ->type('b', 'password')
            ->press('Ienākt')
            ->seePageIs('/auth/login')
            ->see("Kļūda!");
	}

	public function testOkLoginEditProfileInfoLogout()
	{
		// create user
		$u = new User;
		$u->username = 'Vards';
		$u->surname = 'Uzvards';
		$u->email = 'test@test.lv';
		$u->phone = '12345678';
		$u->password = '$2y$08$GZ2oymIqz4fsOe5lWjEVee8ugApRzFYDIUKpI0XwSwvW3sFCA0rAe';
		$u->confirmed = 1;
		$u->terms = 1;
		$u->forceSave();

		$u = User::orderBy('id','desc')->first();
		$this->assertEquals($u->username,'Vards');

        $this->visit('/auth/login')
            ->type('test@test.lv', 'email')
            ->type('parole', 'password')
            ->press('Ienākt')
            ->seePageIs('/')
            ->dontSee("Kļūda!")
            ->see("Labot profilu")
            ->see("Sveiki")
            ->see("Vards Uzvards");

        $this->visit('/')
            ->see("Labot profilu")
            ->see("Vārds")
            ->type('+371112', 'phone')
            ->type('Uzvards1', 'surname')
            ->type('parole', 'cpassword')
            ->press('Labot')
            ->seePageIs('/')
            ->dontSee("Kļūda!")
            ->see("Labotā lietotāja informācija ir saglabāta.")
            ->see("Uzvards1")
            ->see("+371112");

        $u = User::orderBy('id','desc')->first();
        $this->assertEquals($u->surname,'Uzvards1');
        $this->assertEquals($u->phone,'+371112');

        $this->visit('/')
            ->see("Labot profilu")
            ->see("Vārds")
            ->click('Iziet')
            ->seePageIs('/auth/login')
            ->dontSee("Kļūda!");
	}

    public function testRegistrationIndexWithoutData()
    {
        $this->call('POST', '/user/index',array(
            'asd' => '',
        ));

        $this->assertRedirectedTo('/auth/login');
        $this->assertSessionHas('error',"Trūkst reģistrācijas datu!");

        // time +15min
        $data = [
            "FirstName" => "Jānis",
            "LastName" => "Bērziņš",
            "PersonCode" => "111111-22222",
            "Timestamp" => time() + (15*60),
        ];

        $encrypted_string = $this->encrypt(json_encode($data));

        Session::put('register_encoded', $encrypted_string);
        $this->call('POST', '/user/index',[],[],[],[],"12345".$encrypted_string);
        $this->assertRedirectedTo('/auth/login');
        $this->assertSessionHas('error',"Saņemtie dati nav korekti, pārāk ilgs pārsūtīšanas laiks! Lūdzu mēģiniet vēlreiz.");
    }

    public function testRegistrationRegisterFormWithoutData()
    {
        Session::forget('register_encoded');
        $this->call('GET', '/user/register');

        $this->assertRedirectedTo('/auth/login');
        $this->assertSessionHas('error',"Trūkst reģistrācijas datu!");
    }

    public function testRegisterFromRS()
    {
        $this->call('POST', '/user/register',[],[],[],[],"asd");
        $this->assertRedirectedTo('/auth/login');
        $this->assertSessionHas('error',"Nav saņemti reģistrācijas dati!");

        $this->call('POST', '/user/register',[],[],[],[],"MadRotJpb2AR1kKhcYqGhmELQ9qOMlCNA9/9SCSdVyiXlJAN+rZwtpD1//XvKNh690fmmGmKbKMPHTJjuj8WY0pjnTCL47XRBSMIWdRZKiybx3FrCIV+vimVPJKwaTknxQtX2o7JDBxST6lyxNHUlSKw1CGpBvkNK1o9M8Nieec=");
        $this->assertRedirectedTo('/auth/login');
        $this->assertSessionHas('error',"Saņemti kļūdaini reģistrācijas dati!");

        // time +15min
        $data = [
            "FirstName" => "Jānis",
            "LastName" => "Bērziņš",
            "PersonCode" => "111111-22222",
            "Timestamp" => time() + (15*60),
        ];

        $encrypted_string = $this->encrypt(json_encode($data));

        $this->call('POST', '/user/register',[],[],[],[],"12345".$encrypted_string);
        $this->assertRedirectedTo('/auth/login');
        $this->assertSessionHas('error',"Saņemtie dati nav korekti, pārāk ilgs pārsūtīšanas laiks! Lūdzu mēģiniet vēlreiz.");

        // current time
        $data = [
            "FirstName" => "JANIS",
            "LastName" => "Bērziņš",
            "PersonCode" => "111111-22222",
            "Timestamp" => time(),
        ];

        $encrypted_string = $this->encrypt(json_encode($data));

        $this->call('POST', '/user/register',[],[],[],[],"12345".$encrypted_string);
        $this->assertRedirectedTo('/user/register');
        $this->assertSessionHas('register_encoded',$encrypted_string);

        $this->visit("/user/register")
            ->see("JANIS")
            ->see("Reģistrēties sistēmā")
            ->see("Piekrītu noteikumiem")
            ->seePageIs('/user/register')
            ->press("submit")
            ->seePageIs('/user/register')
            ->see("Reģistrēties sistēmā")
            ->see("JANIS")
            ->see("Kļūda")
            ->see("Jāapstiprina noteikumi!")
            ->check("terms")
            ->press("submit")
            ->seePageIs('/user/register')
            ->see("Reģistrēties sistēmā")
            ->see("JANIS")
            ->see("Kļūda")
            ->dontSee("Jāapstiprina noteikumi!");

        // check existing person code
		$u = new User;
		$u->username = 'Vards';
		$u->surname = 'Uzvards';
		$u->email = 'test@test.lv';
		$u->phone = '12345678';
		$u->pk = '111111-22222';
		$u->password = '$2y$08$GZ2oymIqz4fsOe5lWjEVee8ugApRzFYDIUKpI0XwSwvW3sFCA0rAe';
		$u->confirmed = 1;
		$u->terms = 1;
		$u->forceSave();

        $this->seeInDatabase('users', ['pk' => '111111-22222']);

        $this->visit("/user/register")
            ->see("JANIS")
            ->see("Reģistrēties sistēmā")
            ->see("Piekrītu noteikumiem")
            ->seePageIs('/user/register')
            ->check("terms")
            ->press("submit")
            ->seePageIs('/user/register')
            ->see("Reģistrēties sistēmā")
            ->see("JANIS")
            ->see("Kļūda")
            ->dontSee("Jāapstiprina noteikumi!")
            ->see('Lietotājs ar šādu personas kodu jau ir reģistrēts sistēmā!');
    }
}