<?php
ini_set('memory_limit','228M');

use App\Models\Objekts;
use App\Models\School;
use Mockery as m;

class ObjektiControllerTest extends TestCase 
{
	public function __construct()
	{
	  $this->omock = m::mock('App\Models\Objekts');
	  $this->dotmock = m::mock('App\Models\Dotations');
	}

	public function tearDown()
	{
	  Mockery::close();
	}

	public function testIndex()
	{
		$this->be($this->create_admin_user());

		$this->visit('/admin/objekti');

		$this->assertTrue($this->response->isOk());
	}

	public function testCreateForm()
	{
        $this->be($this->create_admin_user());

 		$this->app->instance('dot', $this->dotmock);

        $this->visit('/admin/objekti/create')
            ->see('Izveidot jaunu objektu');

      	$this->assertViewHas('dot');

        $this->assertTrue($this->response->isOk());
	}

	public function testSaveFailure()
	{
        $this->be($this->create_admin_user());

        $this->visit('admin/objekti/create');
        $this->assertTrue($this->response->isOk());

        $this->visit('/admin/objekti/create')
            ->type('', 'object_name')
            ->type('LV', 'object_country')
            ->press('Apstiprināt')
            ->seePageIs('/admin/objekti/create')
            ->see("Kļūda!");
	}

	public function testSaveEditDeleteSuccess()
	{
        $this->be($this->create_admin_user());

        // create School
        School::create(["id" => 1, 'heading' => 'Skola', 'changed' => '2016-01-01']);

        $this->visit('/admin/objekti/create')
            ->select('1', 'object_type')
            ->select('1', 'school_id')
            ->type('Test', 'object_name')
            ->type('countr', 'object_country')
            ->type('pilseta', 'object_city')
            ->type('adrese', 'object_address')
            ->type('123456789', 'object_regnr')
            ->type('1.2.3.4', 'object_ip')
            ->press('Apstiprināt')
            ->seePageIs('/admin/objekti/1/edit')
            ->dontSee("Kļūda!");

        // check from db
        $u = Objekts::orderBy('id','desc')->first();
        $this->assertEquals($u->object_name,'Test');

        // edit object
	    $resp = $this->call('POST', '/admin/objekti/'.$u->id.'/edit',array(
	    	'object_type' => '1',
	    	'object_name' => 'Tests',
	    	'object_country' => 'Testc',
	    	'object_city' => 'Testcity',
	    	'object_address' => 'Adrese',
	    	'object_ip' => '1.2.3.4',
	    	'object_regnr' => '1234',
	    	'allow_d' => '0',
	    	'dot' => [],
	    	'kods' => 123,
	    	'show' => 1,
	    	'entrance_ip' => '',
	    	'sync_path' => '',
	    	'sync_enable' => 0
	    ));

	    // Should redirect to collection, with a success flash message
        $this->assertRedirectedTo('/admin/objekti/'.$u->id.'/edit');
        $this->assertSessionHas('success');

	    // check from db
	    $u = Objekts::orderBy('id','desc')->first();
	    $this->assertEquals($u->object_name,'Tests');

        //delete object
        $this->visit('/admin/objekti/'.$u->id.'/delete');
        $this->press("Dzēst");

        //check in db
        $u = Objekts::orderBy('id','desc')->first();
        $this->assertEquals(isset($u),false);
	}

}