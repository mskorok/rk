<?php
require 'includes/config.php';
require 'includes/Merchant.php';


$host = $db_host;
$db   = $db_database;
$user = $db_user;
$pass = $db_pass;
$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);

$trans_id = $_POST['trans_id'];
$error_msg = $_POST['error'];
//$var = ($_POST['var']); //getting additional parameters
echo 'Tehnical error occurred! Please contact merchant! <br><br>' . $error_msg;

$stmt = $pdo->prepare('SELECT client_ip_addr FROM `transaction` WHERE `trans_id` = :trans_id');
$stmt->bindParam(':trans_id', $trans_id);


if (!$stmt->execute()) {
    $error = $stmt->errorInfo()[2];
    $stmt->closeCursor();
    die('*** Invalid query1: ' . $error);
} else {
    $row = $stmt->fetch();
    $client_ip_addr = $row[0];
}
$stmt->closeCursor();

$merchant = new Merchant($ecomm_server_url, $cert_url, $cert_pass, 1);

$resp = $merchant->getTransResult(urlencode($trans_id), $client_ip_addr);
$resp = htmlentities($resp, ENT_QUOTES);
$resp = $error_msg . ' + ' . $resp;
$stmt = $pdo->prepare('INSERT INTO $db_table_error VALUES ("", now(), "ReturnFailURL", :resp)');
$stmt->bindParam(':resp', $resp);

if (!$stmt->execute()) {
    $error = $stmt->errorInfo()[2];
    $stmt->closeCursor();
    die('*** Invalid query2: ' . $error);
}

$stmt->closeCursor();
?>