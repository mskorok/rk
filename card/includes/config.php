<?php

$ecomm_server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
$ecomm_client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
$cert_url             = base_path().'/certs/3132966keystore.pem'; //full path to keystore file
$cert_pass            = 'RK2017'; //keystore password
$currency             = '978'; //978=EUR 840=USD 941=RSD 703=SKK 440=LTL 233=EEK 643=RUB 891=YUM


/*UNCOMMENT THIS WHEN YOU GO TO PRODUCTION SYSTEM, ALSO CHANGE KEYSTORE AND PASSWORD

$ecomm_server_url     = 'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler';
$ecomm_client_url     = 'https://secureshop.firstdata.lv/ecomm/ClientHandler';

*/


//MYSQL config
//!!!!! DO NOT CREATE DATABASE OR TABLE YOURSELF, IT WILL BE DONE AUTOMATICALY. CHANGE ONLY USER, PASS, HOST. !!!!!
$db_user                =     env('DB_USERNAME', 'user1_dev');
$db_pass                =     env('DB_PASSWORD', 'emaks_user1');
$db_host                =     env('DB_HOST', 'localhost');
$db_database            =     env('DB_DATABASE', 'rk_dev');
$db_table_transaction   =     'transaction';
$db_table_batch         =     'batch';
$db_table_error         =     'error';
?>