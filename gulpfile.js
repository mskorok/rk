var gulp = require('gulp');
var gutil = require('gulp-util');
var less = require('gulp-less');
var prefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var csso = require('gulp-csso');

gulp.task('less', function () {
    gulp
        .src('public/assets/css/less/master.less')
        .pipe(less())
        .pipe(prefixer('last 2 versions', 'ie 9'))
        .pipe(csso())
        .pipe(concat('bootstrap.css'))
        .pipe(gulp.dest('public/assets/compiled/css'));
});

gulp.task('css', function() {
    gulp.src(['public/assets/css/*.css'])
        .pipe(concat('other.css'))
        .pipe(prefixer('last 2 versions', 'ie 9'))
        .pipe(csso())
        .pipe(gulp.dest('public/assets/compiled/css'));
});

gulp.task('js', function () {
    gulp.src('public/assets/js/*.js')
        .pipe(uglify())
        .pipe(concat('all.js'))
        .pipe(gulp.dest('public/assets/compiled/js'));
});

gulp.task('watch', function () {
    gulp.watch('public/assets/css/less/*.less', ['less']);
    gulp.watch('public/assets/css/*.css', ['css']);
    gulp.watch('public/assets/js/*.js', ['js']);
});

gulp.task('default', ['less', 'js', 'css', 'watch']);