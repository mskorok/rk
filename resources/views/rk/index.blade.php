@extends('site.layouts.default')

@section('content')
    <div class="page-header">
        <h3>Apstiprināt lietotājus</h3>
    </div>

    <div>
        <table id="users" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th class="span2" style="background-color:#333;color:white;">Vārds uzvārds</th>
                <th class="span2" style="background-color:#333;color:white;">Personas kods</th>
                <th class="span1" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
                <th class="span1" style="background-color:#333;color:white;">Statuss</th>
                <th class="span1" style="background-color:#333;color:white;">Darbības</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        var oTable;
        $(document).ready(function() {
            oTable = $('#users').dataTable( {
                "sAjaxSource": "{!! URL::to('rk/data') !!}",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ieraksti lapā"
                },
                "aoColumns": [
                    {"bSearchable": true },
                    {"bSearchable": true },
                    {"bSearchable": false },
                    {"bSearchable":false},
                    {"bSearchable":false},
                ],
                "bInfo": true,
                "bFilter": true,
                "bSort": true,
                "aaSorting": [[ 2, "desc" ]]
            });
        });
    </script>
@stop