@extends('site.layouts.default')

@section('content')

    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="" autocomplete="off">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

        <div class="form-group">
            <label for="pk">Personas kods</label>
            <input class="form-control" type="text" name="pk" id="pk" disabled value="{{{ $newuser->pk }}}">
        </div>

        <div class="form-group">
            <label for="email">{{{ Lang::get('confide/confide.e_mail') }}}</label>
            <input class="form-control" type="text" name="email" id="email" disabled value="{{{ $newuser->email }}}">
        </div>

        <div class="form-group">
            <label for="phone">{{{ Lang::get('confide/confide.phone') }}}</label>
            <input class="form-control" type="text" name="phone" id="phone" autocomplete="off" disabled value="{{{ $newuser->phone }}}">
            <span style="color:red;">{{{ $errors->first('phone') }}}</span>
        </div>

        <div class="form-group">
            <label for="address">Deklarētā adrese</label>
            <input class="form-control" type="text" name="address" id="address" autocomplete="off" value="{{{ $newuser->address }}}">
            <span style="color:red;">{{{ $errors->first('address') }}}</span>
        </div>

        <div class="form-group {{{ $errors->has('pilsoniba') ? 'has-error' : '' }}}">
            <label for="address">Pilsonība</label>
            <input class="form-control" type="text" name="address" id="address" autocomplete="off" disabled value="{{{ $citi->pilsoniba }}}">
            <span style="color:red;">{{{ $errors->first('pilsoniba') }}}</span>
        </div>

        <div class="form-group {{{ $errors->has('info') ? 'has-error' : '' }}}">
            <div class="row">
                <div class="col-md-4">
                    <label for="info1">Personas dokuments</label>
                    <input class="form-control" type="text" name="address" id="address" autocomplete="off" disabled value="{{{ $info[0] }}}">
                </div>
                <div class="col-md-4">
                    <label for="info">Personas dokumenta numurs</label>
                    <input class="form-control" placeholder="Dokumenta numurs" type="text" name="info" id="info" disabled autocomplete="off" value="{{{ $info[1] }}}">
                </div>
                <div class="col-md-4">
                    <label for="info2">Dokuments derīgs līdz</label>
                    <div class='input-group date' id='datepicker1'>
                        <input class="form-control" placeholder="Derīguma termiņš" type="text" name="info2" id="info2" disabled autocomplete="off" value="{{{ $info[2] }}}">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>

                </div>
            </div>

            <span style="color:red;">{{{ $errors->first('info') }}}</span>
        </div>

        <div class="form-group">
            <label for="pazime">Lūdzu, zemāk norādiet, vai atbilstat kādai no šādām pazīmēm</label>
            <ul class="list-group">
                <li class="list-group-item small">
                    a.       Ārpus Latvijas Republikas ieņemu kādu no šādiem amatiem: valsts vadītājs, parlamenta deputāts, valdības vadītājs, ministrs, ministra vietnieks vai ministra vietnieka vietnieks, valsts sekretārs, augstākās tiesas tiesnesis, konstitucionālās tiesas tiesnesis, augstākās revīzijas (audita) iestādes padomes vai valdes loceklis, centrālās bankas padomes vai valdes loceklis, vēstnieks, pilnvarotais lietvedis, bruņoto spēku augstākais virsnieks, valsts kapitālsabiedrības padomes vai valdes loceklis, vai arī attiecīgo amatu atstāju viena gada laikā;
                </li>
                <li class="list-group-item small">
                    b.      esmu iepriekšminēto personu vecāks, laulātais vai viņam pielīdzināma persona, bērns, viņa laulātais vai laulātajam pielīdzināma persona. Persona par laulātajam pielīdzināmu uzskatāma tikai tad, ja attiecīgās valsts likumi tai nosaka šādu statusu;
                </li>
                <li class="list-group-item small">
                    c.       esmu persona, par kuru ir publiski zināms, ka tai ir darījuma attiecības ar kādu no šī dokumenta 1. punktā, vai kurai ar šo personu kopīgi pieder pamatkapitāls komercsabiedrībā, kā arī fiziskā persona, kura ir vienīgā tāda juridiska veidojuma īpašnieks, par kuru ir zināms, ka tas faktiski izveidots šī dokumenta 1. punktā minētās personas labā;
                </li>
                <li class="list-group-item {{{ $errors->has('pazime') ? 'has-error' : '' }}}">
                    Nē <input type="radio" value="2" name="pazime" <?php if($newuser->pazime== "2") { echo 'checked="checked"'; } ?> />&nbsp;
                    Jā (lūdzu, norādiet valsti un amatu) <input type="radio" value="1" name="pazime" <?php if($newuser->pazime== "1") { echo 'checked="checked"'; } ?> />
                </li>
                <li class="list-group-item {{{ $errors->has('pazime_text') ? 'has-error' : '' }}}">
                    <input class="form-control" type="text" name="pazime_text" id="pazime_text" disabled value="{{{ $newuser->pazime_text }}}" />
                </li>
            </ul>
        </div>

        <div class="form-group">
            <label for="pazime">Ziņas par e-kartes kontā veicamajiem darījumiem un klienta līdzekļu izcelsmi</label>
            <div class="row">
                <div class="col-md-6">
                    <label for="maks1">Plānotie ienākošie/izejošie maksājumi viena kalendārā mēneša laikā (maksimāli 140 EUR)</label>
                </div>
                <div class="col-md-3 {{{ $errors->has('maks1') ? 'has-error' : '' }}}">
                    <input class="form-control" placeholder="Ienākošie, EUR" type="text" name="maks1" id="maks1" autocomplete="off" disabled value="{{{ $citi->maks1 }}}">
                </div>
                <div class="col-md-3 {{{ $errors->has('maks2') ? 'has-error' : '' }}}">
                    <input class="form-control" placeholder="Izejošie, EUR" type="text" name="maks2" id="maks2" autocomplete="off" disabled value="{{{ $citi->maks2 }}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="maks1">Plānotie ienākošie/izejošie maksājumi viena viena kalendārā gada laikā (maksimāli 2500 EUR)</label>
                </div>
                <div class="col-md-3 {{{ $errors->has('maks3') ? 'has-error' : '' }}}">
                    <input class="form-control" placeholder="Ienākošie, EUR" type="text" name="maks3" id="maks3" autocomplete="off" disabled value="{{{ $citi->maks3 }}}">
                </div>
                <div class="col-md-3 {{{ $errors->has('maks4') ? 'has-error' : '' }}}">
                    <input class="form-control" placeholder="Izejošie, EUR" type="text" name="maks4" id="maks4" autocomplete="off" disabled value="{{{ $citi->maks4 }}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="maks1">Plānotais viena pirkuma darījuma vidējais un maksimāli pieļaujamais apjoms) (EUR)</label>
                </div>
                <div class="col-md-3 {{{ $errors->has('maks5') ? 'has-error' : '' }}}">
                    <input class="form-control" placeholder="Vidējais apjoms, EUR" type="text" name="maks5" id="maks5" autocomplete="off" disabled value="{{{ $citi->maks5 }}}">
                </div>
                <div class="col-md-3 {{{ $errors->has('maks6') ? 'has-error' : '' }}}">
                    <input class="form-control" placeholder="Maksimālais apjoms, EUR" type="text" name="maks6" id="maks6" autocomplete="off" disabled value="{{{ $citi->maks6 }}}">
                </div>
            </div>

            <span style="color:red;">{{{ $errors->first('maks') }}}</span>
        </div>

        <div class="form-group {{{ $errors->has('izcelsme') ? 'has-error' : '' }}}">
            <label for="izcelsme1">Uz kartes saņemto līdzekļu izcelsme</label>
            <div class="row">
                <div class="col-md-4">
                    <input class="form-control" type="text" name="izcelsme1" autocomplete="off" disabled value="{{{ $citi->izcelsme1 }}}">
                </div>
                <div class="col-md-8">
                    <input class="form-control" placeholder="norādiet" type="text" name="izcelsme2" id="izcelsme2" autocomplete="off" disabled value="{{{ $citi->izcelsme2 }}}">
                </div>
            </div>

            <span style="color:red;">{{{ $errors->first('izcelsme') }}}</span>
        </div>

        <div class="form-group {{{ $errors->has('darijumi') ? 'has-error' : '' }}}">
            <label for="darijumi1">Visi darījumi ar E-kartes kontā esošajiem līdzekļiem tiks veikti Konta pārvaldnieka un E-kartes lietotāja interesēs</label>
            <div class="row">
                <div class="col-md-4">
                    <select name="darijumi1" id="darijumi1" class="form-control" disabled>
                        <option value="Jā" <?php if($citi->darijumi1 == "Jā") { echo 'selected="selected"'; } ?>>Jā</option>
                        <option value="Nē" <?php if($citi->darijumi1 == "Nē") { echo 'selected="selected"'; } ?>>Nē</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <input class="form-control" placeholder="paskaidrojiet" type="text" name="darijumi2" id="darijumi2" autocomplete="off" disabled value="{{{ $citi->darijumi2 }}}" >
                </div>
            </div>

            <span style="color:red;">{{{ $errors->first('darijumi') }}}</span>
        </div>

        <div class="form-group {{{ $errors->has('ipasnieks') ? 'has-error' : '' }}}">
            <label for="darijumi1">Esmu E-kartes kontā esošo līdzekļu patiesais īpašnieks</label>
            <div class="row">
                <div class="col-md-4">
                    <select name="ipasnieks1" id="ipasnieks1" class="form-control" disabled>
                        <option value="Jā" <?php if($citi->ipasnieks1 == "Jā") { echo 'selected="selected"'; } ?>>Jā</option>
                        <option value="Nē" <?php if($citi->ipasnieks1 == "Nē") { echo 'selected="selected"'; } ?>>Nē</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <input class="form-control" placeholder="paskaidrojiet" type="text" name="ipasnieks2" id="ipasnieks2" autocomplete="off" disabled value="{{{ $citi->ipasnieks2 }}}">
                </div>
            </div>

            <span style="color:red;">{{{ $errors->first('ipasnieks') }}}</span>
        </div>

        <div class="form-group">
            <div class="row">
                @if (trim($newuser->dokuments) != '')
                    <a href="/rk/dldoc/{{ $newuser->id }}" class="btn btn-large btn-info">Lejuplādēt pievienoto dokumentu</a>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="comment">Komentāri</label>
            <div class="row">
                <textarea name="comment" class="form-control">{{{ $comment }}}</textarea>
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <a href="/rk/index" class="btn-cancel">Atcelt</a>
                <button type="submit" class="btn btn-success">Apstiprināt</button>
            </div>
        </div>

    </form>
@stop