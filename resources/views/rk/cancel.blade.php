@extends('site.layouts.default')

@section('content')

    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="" autocomplete="off">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

        <div class="form-group">
            <label for="comment">Komentāri</label>
            <div class="row">
                <textarea name="comment" class="form-control">{{{ $comment }}}</textarea>
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <a href="/rk/index" class="btn-cancel">Atpakaļ</a>
                <button type="submit" class="btn btn-danger">Atcelt lietotāja reģistrāciju</button>
            </div>
        </div>

    </form>
@stop