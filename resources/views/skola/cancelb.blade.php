@extends('site.layouts.default')

@section('content')

	@include('partials.form.formstart', array('action' => ''))

		<div class="form-group">
			<a href="/skola/bindex" class="btn btn-cancel">Atpakaļ</a>
			<button type="submit" class="btn btn-danger">Atcelt e-talona pieprasījumu</button>
		</div>

	</form>
</div>
@stop