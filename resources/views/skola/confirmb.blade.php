@extends('site.layouts.default')

@section('content')

	@include('partials.form.formstart', array('action' => ''))

		@include('partials.form.ipk', array('objekts' => $objekts ))

		@include('partials.form.iname', array('objekts' => $objekts ))

		@include('partials.form.isurname', array('objekts' => $objekts ))

		@include('partials.form.igrade', array('objekts' => $objekts ))

	@include('partials.form.formend', array('cancel' => 'skola/bindex'))
@stop