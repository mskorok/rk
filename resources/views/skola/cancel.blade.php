@extends('site.layouts.default')

@section('content')

	<form class="form-horizontal" method="post" enctype="multipart/form-data" action="" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general" style="float:left;">

			<!-- Form Actions -->
			<div class="control-group">
				<div class="controls">
					<a href="/skola/index" class="btn-cancel">Atpakaļ</a>
					<button type="submit" class="btn btn-danger">Atcelt e-talonu</button>
				</div>
			</div>

			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop