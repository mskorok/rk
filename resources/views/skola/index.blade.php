@extends('site.layouts.default')


@section('content')
	<div class="page-header">
		<h3>
			Apstiprināt E-talonus
		</h3>
	</div>

	<div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="span3" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
					<th class="span3" style="background-color:#333;color:white;">E-talons</th>
					<th class="span3" style="background-color:#333;color:white;">Darbības</th>
				</tr>
			</thead>
			<tbody>
				@if (count($data)>0)
					@foreach ($data as $single)
							<tr>		
								<td>{!! $single->created_at !!}</td>
								<td>{!! $single->nr !!}</td>
								<td>
									<a href="{{{ URL::to('skola/index/' . $single->id . '/cancel' ) }}}" class="btn btn-mini btn-danger">{{{ Lang::get('button.cancel') }}}</a>
									<a href="{{{ URL::to('skola/index/' . $single->id . '/confirm' ) }}}" class="btn btn-mini btn-success">{{{ Lang::get('button.confirm') }}}</a>
								</td>
							</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
@stop