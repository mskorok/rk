@extends('site.layouts.default')

@section('content')

	<form class="form-horizontal" method="post" enctype="multipart/form-data" action="" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general" style="float:left;">

				<div class="control-group {{{ $errors->has('pk') ? 'error' : '' }}}">
					<label class="control-label" for="pk">Personas kods</label>
					<div class="controls">
						<input type="text" name="pk" id="pk" maxlength="12" value="{{{ Input::old('pk', isset($objekts) ? $objekts->pk : null) }}}" />
						{!! $errors->first('pk', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('name') ? 'error' : '' }}}">
					<label class="control-label" for="name">Vārds, uzvārds</label>
					<div class="controls">
						<input type="text" name="name" id="name" value="{{{ Input::old('name', isset($objekts) ? $objekts->name : null) }}}" />
						{!! $errors->first('name', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('grade') ? 'error' : '' }}}">
					<label class="control-label" for="grade">Klase (1A-12Z)</label>
					<div class="controls">
						<input type="text" name="grade" id="grade" maxlength="2" value="{{{ Input::old('grade', isset($objekts) ? $objekts->grade : '1') }}}" />
						{!! $errors->first('grade', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

			<!-- Form Actions -->
			<div class="control-group">
				<div class="controls">
					<a href="/skola/index" class="btn-cancel">Atcelt</a>
					<button type="submit" class="btn btn-success">Saglabāt</button>
				</div>
			</div>

			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop