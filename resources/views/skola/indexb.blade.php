@extends('site.layouts.default')


@section('content')
	<div class="page-header">
		<h3>
			Apstiprināt E-talonu bāzes datus
		</h3>
	</div>

	<div class="container">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="span3" style="background-color:#333;color:white;">E-taloni</th>
					<th class="span3" style="background-color:#333;color:white;">Persona</th>
					<th class="span3" style="background-color:#333;color:white;">Darbības</th>
				</tr>
			</thead>
			<tbody>
				@if (count($data)>0)
					@foreach ($data as $single)
							<tr>		
								<td>{!! $single->etickets !!}</td>
								<td>{!! $single->name !!} {!! $single->surname !!}, {!! $single->pk !!}</td>
								<td>
									<a href="{{{ URL::to('skola/index/' . $single->id . '/cancelb' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get('button.cancel') }}}</a>
									<a href="{{{ URL::to('skola/index/' . $single->id . '/confirmb' ) }}}" class="btn btn-sm btn-success">{{{ Lang::get('button.confirm') }}}</a>
								</td>
							</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
@stop