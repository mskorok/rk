<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @section('meta')
        <meta name="description" content="@lang('portal/layouts/main.description')">
        <meta name="author" content="@lang('portal/layouts/main.author')">
        <meta name="keywords" content="@lang('portal/layouts/main.keywords')">
    @show
    <link rel="shortcut icon" href="{{ asset('assets/portal/assets/ico/favicon.ico') }}">
    <title>
        @yield('title')
    </title>
    @section('css')
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('assets/portal/css/bootstrap.min.css').'?'.time() }}" rel="stylesheet">

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">--}}
        <link href="{{ asset('assets/portal/css/font-awesome.min.css').'?'.time() }}" rel="stylesheet">

        <!-- Libraries styles -->
        <link href="{{ asset('assets/portal/css/owl.carousel.min.css').'?'.time() }}" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="{{ asset('assets/portal/css/icons.css').'?'.time() }}" rel="stylesheet">
        <link href="{{ asset('assets/portal/css/style.css').'?'.time() }}" rel="stylesheet">
        <link href="{{ asset('assets/portal/css/adaptive.css').'?'.time() }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/portal/css/croppie.css').'?'.time() }}" />
    @show
    @section('head_js')
        {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--}}
        <script src="{{ asset('assets/portal/js/croppie.js').'?'.time() }}"></script>
    @show
</head>
<body>
@include('ec_directive_disabled')
@section('body')
    @include('portal.views.partials.cookie')
    @include('portal.views.partials.navbar')
    @yield('content')
    @include('portal.views.partials.subfooter')
    @include('portal.views.partials.footer')
    @if(\Auth::check())
        <script>
            var auth = true;
            var logout_url = '{{ route('logout') }}';
        </script>
        <input type="hidden" name="api_token" value="{{ Auth::user()->api_token }}" id="api_token">
    @else
        <script>
            var auth = false;
            var logout_url = '{{ route('logout') }}';
        </script>
    @endif
@show
@section('modals')
    <!-- Modals
        ================================================== -->
    @include('portal.views.partials.modals.pass_recovery')
    @include('portal.views.partials.modals.pass_recovery_success')

@show
@section('js')
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="{{ asset('assets/portal/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/portal/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/portal/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/portal/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/portal/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.cookie.js') }}"></script>
@show
<script src="{{ asset('assets/portal/js/main.js'.'?v='.time()) }}"></script>
@yield('script')
@yield('custom_script')
</body>
</html>