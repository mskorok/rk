<!-- COOKIES WARNING -->
<div class="cookie-bar" id="cookies_block">
    <div class="container">
        <div class="center-buttons">
            <div class="text">@lang('portal/partials/cookie.text')</div>
            <!-- <div> -->
            <div class="button green" id="cookie_notice">@lang('portal/partials/cookie.ok')</div>
            <div class="button red" id="cookie_forbidden">@lang('portal/partials/cookie.disagree')</div>
            <!-- </div> -->
        </div>
    </div>
</div>
<!-- END: COOKIES WARNING -->