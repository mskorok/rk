<!-- Section Subfooter -->
<section id="subfooter">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6 footer-block"><a href="{{ route('terms') }}"><div class="svg-icon inline"><svg aria-hidden="true" class="icon icon-header-row-arrow"><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-rules') }}"></use></svg></div> @lang('portal/partials/subfooter.terms')</a></div>
                <div class="col-md-6 col-sm-0 col-xs-0 hidden-xs hidden-sm footer-block center-buttons">SIA “RĪGAS KARTE”, 40003979933 | <a href="mailto:support@skolas.rigaskarte.lv"><div class="svg-icon inline"><svg aria-hidden="true" class="icon icon-header-row-arrow"><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-email') }}"></use></svg></div> support@skolas.rigaskarte.lv</a></div>
                <div class="col-md-3 col-sm-6 col-xs-6 footer-block">
                    <div style="float: right;">
                        <div style="font-size: 24px;">
                            <a href="#"><svg aria-hidden="true" class="icon icon-header-row-arrow /*icon-footer-social*/"><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#social-facebook') }}"></use></svg></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End Section Subfooter -->