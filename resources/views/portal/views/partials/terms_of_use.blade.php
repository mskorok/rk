<!-- Terms of use 3 -->
<section id="terms_of_use">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title" style="margin-bottom: 10px;">@lang('portal/partials/terms_of_use.terms')</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3 class="title">@lang('portal/partials/terms_of_use.title')</h3>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div style="margin-bottom: 10px;">
                    <strong>@lang('portal/partials/terms_of_use.system_manager')</strong>@lang('portal/partials/terms_of_use.system_manager_text')
                </div>
                <div style="margin-bottom: 10px;">
                    <strong>@lang('portal/partials/terms_of_use.system')</strong>@lang('portal/partials/terms_of_use.system_text')
                </div>
                <div style="margin-bottom: 10px;">
                    <strong>@lang('portal/partials/terms_of_use.seller')</strong>@lang('portal/partials/terms_of_use.seller_text')
                </div>
                <div style="margin-bottom: 10px;">
                    <strong>@lang('portal/partials/terms_of_use.user')</strong>@lang('portal/partials/terms_of_use.user_text')
                </div>

            </div>
            <div class="col-sm-6">
                <div style="margin-bottom: 10px;">
                    <strong>@lang('portal/partials/terms_of_use.account_manager')</strong>@lang('portal/partials/terms_of_use.account_manager_text')
                </div>
                <div style="margin-bottom: 10px;">
                    <strong>@lang('portal/partials/terms_of_use.account')</strong>@lang('portal/partials/terms_of_use.account_text').
                </div>
                <div style="margin-bottom: 10px;">
                    <strong>@lang('portal/partials/terms_of_use.portal')</strong>@lang('portal/partials/terms_of_use.portal_text')
                </div>
                <div style="margin-bottom: 10px;">
                    <strong>@lang('portal/partials/terms_of_use.visitors')</strong>@lang('portal/partials/terms_of_use.visitors_text')
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 20px;">
            <div class="col-sm-12">
                <ol>
                    <li class="heading">@lang('portal/partials/terms_of_use.conditions')</li>
                    <ol class="spaced-list-items">
                        {!! trans('portal/partials/terms_of_use.conditions_text') !!}
                    </ol>
                    <li class="heading">@lang('portal/partials/terms_of_use.account_activation')</li>
                    <ol class="spaced-list-items">
                        {!! trans('portal/partials/terms_of_use.account_activation_text') !!}
                    </ol>
                    <li class="heading">@lang('portal/partials/terms_of_use.account_using')</li>
                    <ol class="spaced-list-items">
                        {!! trans('portal/partials/terms_of_use.account_using_text') !!}
                    </ol>
                    <li class="heading">@lang('portal/partials/terms_of_use.sellers_terms')</li>
                    <ol class="spaced-list-items">
                        {!! trans('portal/partials/terms_of_use.sellers_terms_text') !!}
                    </ol>
                    <li class="heading">@lang('portal/partials/terms_of_use.return')</li>
                    <ol class="spaced-list-items">
                        {!! trans('portal/partials/terms_of_use.return_text') !!}
                    </ol>
                </ol>
                </ol>
            </div>
        </div>
    </div>
</section><!-- End Terms of use -->