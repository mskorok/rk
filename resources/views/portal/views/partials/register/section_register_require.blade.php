<!-- Section Register Require-->
<section id="register_require">
    <div class="container">
        <h3 class="title">{!! trans('portal/partials/register/section_register_require.title') !!}</h3>
        @if($errors->any())
            <div class="content row" id="submit_error" style="margin-top: 40px; margin-bottom: 40px;">
                @foreach($errors->all() as $error)
                    <div class="col-sm-12">
                        <div class="center-buttons">
                            <div style="float: left; margin-left: 20px; margin-right: 5px; padding-right: 5px; align-self: flex-start;"><img src="{{ asset('assets/portal/img/danger.png') }}" style="width: 24px; height: 24px;"></div>
                            <div style="float: left; text-align: left;">
                                <div style="line-height: 16px; color: red;">
                                    {{ $error }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        <div class="content">
            <div class="require-img text-center">
                <a href="https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit" class="register"><img src="{{ asset('assets/portal/img/bank_1.jpg') }}" alt=""></a>
                <a href="https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit" class="register"><img src="{{ asset('assets/portal/img/bank_2.jpg') }}" alt=""></a>
                <a href="https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit" class="register"><img src="{{ asset('assets/portal/img/bank_3.jpg') }}" alt=""></a>
                <a href="https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit" class="register"><img src="{{ asset('assets/portal/img/bank_4.jpg') }}" alt=""></a>
            </div>

            <p class="text-center">@lang('portal/partials/register/section_register_require.other_banks')
                <a href="mailto:support@rigaskarte.lv">support@rigaskarte.lv</a>
            </p>
        </div>
    </div>
</section>
<!-- End Section Register Require-->