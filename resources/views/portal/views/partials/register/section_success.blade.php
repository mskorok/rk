<!-- Section Register Success -->
@php
    if(!isset($email)) {
            $email = '';
        }
@endphp
<section id="register_success">
    <div class="container">
        <h3 class="title">@lang('portal/partials/register/section_success.title')</h3>
        <div class="content">
            <div class="success-img text-center">
                <img src="{{ asset('assets/portal/img/success.png') }}" alt="">
            </div>

            <p class="text-center">@lang('portal/partials/register/section_success.message1')
                <a href="">{{ $email }}</a>@lang('portal/partials/register/section_success.message2')
            </p>
        </div>
    </div>
</section><!-- End Section Register Success -->