<!-- Section Steps 2 -->
<section class="bg-color">
    <div class="container">
        <div class="content row">
            <div class="subtitle text-center">@lang('portal/partials/register/steps_section_2.title')</div>
            <div class="signs">
                <div class="col-sm-5">
                    <span>A.</span>@lang('portal/partials/register/steps_section_2.a')
                </div>
                <div class="col-sm-3">
                    <span>B.</span>@lang('portal/partials/register/steps_section_2.b')
                </div>
                <div class="col-sm-4">
                    <span>C.</span>@lang('portal/partials/register/steps_section_2.c')
                </div>
            </div>
            <div class="clear"></div>
            <div class="form-group-radio">
                <div class="col-sm-2">
                    <div class="btn-radio">

                        <label for="a-1">
                            <input type="radio" name="has_status" value="no" id="a-1" @if(isset($old['has_status']) &&  $old['has_status'] === 'no') checked @endif @if(!isset($old['has_status'])) checked @endif >
                            <span class="radio-type"></span>&nbsp;@lang('portal/partials/register/steps_section_2.no')
                        </label>

                        <label for="b-1">
                            <input type="radio" name="has_status" value="yes" id="b-1" @if(isset($old['has_status']) &&  $old['has_status'] === 'yes') checked @endif >
                            <span class="radio-type"></span>&nbsp;@lang('portal/partials/register/steps_section_2.yes')
                        </label>

                    </div>
                </div>
            </div>
            <div class="col-sm-10 form-group pale">
                <input type="text" class="form-control input" name="social_status" id="social_status"
                       @if(isset($old['social_status'])) value="{{ $old['social_status'] }}" @endif
                       placeholder="@lang('portal/partials/register/steps_section_3.explain')">
                <span id="social_status_required" style="color: red;float: left; padding-left: 40px;display:none;">@lang('validation.required') &nbsp; *</span>
                <div class="error-message">
                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                    @lang('validation.custom.error_text')
                </div>
            </div>
        </div>
    </div>
</section><!-- End Section Steps 2 -->