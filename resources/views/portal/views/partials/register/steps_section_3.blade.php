<!-- Section Steps 3 -->
<section id="register_steps_3">
    <div class="container">
        <div class="content row">
            <div class="subtitle text-center">@lang('portal/partials/register/steps_section_3.title')</div>
            <div class="details form-group pale">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <input type="text" class="form-control input" name="income_month"
                                   @if(isset($old['income_month'])) value="{{ $old['income_month'] }}" @endif
                                   placeholder="@lang('portal/partials/register/steps_section_3.incoming')">
                            <div class="error-message">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('validation.custom.error_text1', ['max' => 140])
                            </div>
                        </div>
                        <div class="col-sm-6 p-r-0 p-l-0">
                            <p class="details-text info">@lang('portal/partials/register/steps_section_3.text_income_month')</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <input type="text" class="form-control input" name="income_year"
                                   @if(isset($old['income_year'])) value="{{ $old['income_year'] }}" @endif
                                   placeholder="@lang('portal/partials/register/steps_section_3.incoming')">
                            <div class="error-message">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('validation.custom.error_text1', ['max' => 2500])
                            </div>
                        </div>
                        <div class="col-sm-6 p-r-0 p-l-0">
                            <p class="details-text info">@lang('portal/partials/register/steps_section_3.text_income_year')</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <input type="text" class="form-control input" name="income_amount"
                                   @if(isset($old['income_amount'])) value="{{ $old['income_amount'] }}" @endif
                                   placeholder="@lang('portal/partials/register/steps_section_3.amount')">
                            <div class="error-message">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('validation.custom.error_text1', ['max' => 2500])
                            </div>
                        </div>
                        <div class="col-sm-6 p-r-0 p-l-0">
                            <p class="details-text info">@lang('portal/partials/register/steps_section_3.text_income_amount')</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <input type="text" class="form-control input" name="outgoing_month"
                                   @if(isset($old['outgoing_month'])) value="{{ $old['outgoing_month'] }}" @endif
                                   placeholder="@lang('portal/partials/register/steps_section_3.outgoing')">
                            <div class="error-message">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('validation.custom.error_text1', ['max' => 140])
                            </div>
                        </div>
                        <div class="col-sm-6 p-r-0 p-l-0">
                            <p class="details-text info">@lang('portal/partials/register/steps_section_3.text_outgoing_month')</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <input type="text" class="form-control input" name="outgoing_year"
                                   @if(isset($old['outgoing_year'])) value="{{ $old['outgoing_year'] }}" @endif
                                   placeholder="@lang('portal/partials/register/steps_section_3.outgoing')">
                            <div class="error-message">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('validation.custom.error_text1', ['max' => 2500])
                            </div>
                        </div>
                        <div class="col-sm-6 p-r-0 p-l-0">
                            <p class="details-text info">@lang('portal/partials/register/steps_section_3.text_outgoing_year')</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <input type="text" class="form-control input" name="outgoing_amount"
                                   @if(isset($old['outgoing_amount'])) value="{{ $old['outgoing_amount'] }}" @endif
                                   placeholder="@lang('portal/partials/register/steps_section_3.max_amount')">
                            <div class="error-message">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('validation.custom.error_text1', ['max' => 2500])
                            </div>
                        </div>
                        <div class="col-sm-6 p-r-0 p-l-0">
                            <p class="details-text info">@lang('portal/partials/register/steps_section_3.text_outgoing_amount')</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <select  id="funds_origin"  name="funds_origin" class="select2-select profile-funds-origin">
                            <option></option>
                            <option value="Darba alga" @if(isset($old['funds_origin']) && $old['funds_origin'] === 'Darba alga') selected="selected" @endif >@lang('portal/partials/register/steps_section_3.salary')</option>
                            <option value="Pensija" @if(isset($old['funds_origin']) && $old['funds_origin'] === 'Pensija') selected="selected" @endif >@lang('portal/partials/register/steps_section_3.pension')</option>
                            <option value="Stipendija" @if(isset($old['funds_origin']) && $old['funds_origin'] === 'Stipendija') selected="selected" @endif >@lang('portal/partials/register/steps_section_3.stipend')</option>
                            <option value="Pabalsts" @if(isset($old['funds_origin']) && $old['funds_origin'] === 'Pabalsts') selected="selected" @endif >@lang('portal/partials/register/steps_section_3.benefit')</option>
                            <option value="Cita uz kartes saņemto līdzeklu izcelsme" @if(isset($old['funds_origin']) && $old['funds_origin'] === 'Cita uz kartes saņemto līdzeklu izcelsme') selected="selected" @endif >@lang('portal/partials/register/steps_section_3.others_funds')</option>
                        </select>
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('validation.custom.error_text')
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input" name="others_funds" id="others_funds"
                               @if(isset($old['others_funds'])) value="{{ $old['others_funds'] }}" @endif
                               placeholder="@lang('portal/partials/register/steps_section_3.explain')">
                        <span id="others_funds_required" style="color: red;float: left; padding-left: 40px;margin-bottom:1rem; display: none;">@lang('validation.required') &nbsp; *</span>

                    </div>
                    <div class="col-sm-4"></div>
                </div>

                <div class="content row">
                    <p>@lang('portal/partials/register/steps_section_3.desc1')</p>
                    <div class="form-group-radio">
                        <div class="col-sm-2">
                            <div class="btn-radio">

                                <label for="a-2">
                                    <input type="radio" name="is_manager" value="no" id="a-2" @if(isset($old['is_manager']) &&  $old['is_manager'] === 'no') checked @endif>
                                    <span class="radio-type"></span>&nbsp; @lang('portal/partials/register/steps_section_3.no')
                                </label>

                                <label for="b-2">
                                    <input type="radio" name="is_manager" value="yes" id="b-2"  @if(isset($old['is_manager']) &&  $old['is_manager'] === 'yes') checked @endif @if(!isset($old['is_manager'])) checked @endif >
                                    <span class="radio-type"></span>&nbsp; @lang('portal/partials/register/steps_section_3.yes')
                                </label>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input" name="manager_status" id="manager_status"
                               @if(isset($old['manager_status'])) value="{{ $old['manager_status'] }}" @endif
                               placeholder="@lang('portal/partials/register/steps_section_3.explain')">
                        <span id="manager_status_required" style="color: red;float: left; padding-left: 40px;margin-bottom:1rem; display: none;">@lang('validation.required') &nbsp; *</span>
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('validation.custom.error_text')
                        </div>
                    </div>

                    <p>@lang('portal/partials/register/steps_section_3.desc2')</p>
                    <div class="form-group-radio">
                        <div class="col-sm-2">
                            <div class="btn-radio">

                                <label for="a-3">
                                    <input type="radio" name="is_owner" value="no" id="a-3" @if(isset($old['is_owner']) &&  $old['is_owner'] === 'no') checked @endif>
                                    <span class="radio-type"></span>&nbsp; @lang('portal/partials/register/steps_section_3.no')
                                </label>

                                <label for="b-3">
                                    <input type="radio" name="is_owner" value="yes" id="b-3" @if(isset($old['is_owner']) &&  $old['is_owner'] === 'yes') checked @endif @if(!isset($old['is_owner'])) checked @endif >
                                    <span class="radio-type"></span>&nbsp; @lang('portal/partials/register/steps_section_3.yes')
                                </label>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input" name="owner_status" id="owner_status"
                               @if(isset($old['owner_status'])) value="{{ $old['owner_status'] }}" @endif
                               placeholder="@lang('portal/partials/register/steps_section_3.explain')">
                        <span id="owner_status_required" style="color: red;float: left; padding-left: 40px;display: none;">@lang('validation.required') &nbsp; *</span>
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('validation.custom.error_text')
                        </div>
                    </div>

                </div>


                <div class="content row" style="margin-top: 40px; margin-bottom: 40px;">
                    <div class="col-sm-12" style="text-align: center;">

                        <div class="center-buttons">
                            <div class="person-block" style="margin-right: 10px;">
                                <div class="avatar btn-add-new-photo"><img src="{{ asset('assets/portal/img/avatar.png') }}" id="add_new_profile_photo" /></div>
                            </div>
                            <div style="margin-right: 20px;">
                                <div class="form-group pale" style="position: relative; margin-bottom: 0;">
                                    <div class="btn-group small-btn-group pale btn-add-new-photo">
                                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/register/steps_section_3.add_image')</button>
                                        <button type="button" class="btn btn-sm btn-siel ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use></svg></button>
                                    </div>
                                    <div class="btn-group-note">
                                        <span id="avatar_name"></span>
                                        <br />
                                        <span id="avatar_allowed_formats">
                                            @lang('portal/partials/register/steps_section_1.allowed_formats').
                                        </span>
                                        <br />
                                        <span id="avatar_max_image_size">
                                            @lang('portal/partials/register/steps_section_1.max_image_size')
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="content row" id="submit_error" style="margin-top: 40px; margin-bottom: 40px; display: none">
                    <div class="col-sm-12">
                        <div class="center-buttons">
                            <div style="float: left; margin-left: 20px; margin-right: 5px; padding-right: 5px; align-self: flex-start;"><img src="{{ asset('assets/portal/img/danger.png') }}" style="width: 24px; height: 24px;"></div>
                            <div style="float: left; text-align: left;">
                                <div style="line-height: 16px; color: red;">
                                    {!! trans('portal/partials/register/steps_section_3.error') !!}
                                </div>
                            </div>
                        </div>
                        <div class="center-buttons" id="doc_copy_error">
                            <div style="float: left; text-align: left;">
                                <div style="line-height: 16px; color: red;">
                                    {!! trans('errors.doc_copy_not_attached') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                @if($errors->any())
                    <div class="content row" id="submit_error" style="margin-top: 40px; margin-bottom: 40px;">
                        @foreach($errors->all() as $error)
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div style="float: left; margin-left: 20px; margin-right: 5px; padding-right: 5px; align-self: flex-start;"><img src="{{ asset('assets/portal/img/danger.png') }}" style="width: 24px; height: 24px;"></div>
                                    <div style="float: left; text-align: left;">
                                        <div style="line-height: 16px; color: red;">
                                            {{ $error }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif

                <div id="ajax_error"  class="error" style="text-align: center !important; width: 100%; border: 0 !important;">@lang('portal/partials/register/steps_section_3.fill_form')</div>


                <div class="content row" style="margin-top: 20px;">
                    <div class="col-sm-12">
                        <div style="text-align: center;">
                            <div class="btn-radio">
                                <label for="is_accept_terms">
                                    <input type="checkbox" name="is_accept_terms" id="is_accept_terms">
                                    <span class="radio-type"></span>&nbsp; <span id="get_modal_rules">@lang('portal/partials/register/steps_section_3.agree')</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content row stack" style="margin-top: 20px;">
                    <div class="col-sm-6 col-xs-12">
                        <div style="float: right;">
                            <div class="btn-group register" id="register_profile_button">
                                <button type="submit" disabled  class="btn btn-md btn-blue">@lang('portal/partials/register/steps_section_3.register')</button>
                                <button type="submit" disabled class="btn btn-md btn-green ico"><svg aria-hidden="true" id="icones" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-add-user') }}"></use></svg></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="btn-group">
                            <div class="btn btn-md btn-blue" id="save_profile_button" style="width: auto;">@lang('portal/partials/register/steps_section_3.save')</div>
                            <div class="btn btn-md btn-orange ico" id="save_profile_button1"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use></svg></div>
                        </div>
                    </div>
                </div>


            </div>



        </div>
    </div>
</section><!-- End Section Steps 3 -->