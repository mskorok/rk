<!-- Section Register Thanks -->
<section id="register_thanks">
    <div class="container">
        <h3 class="title">@lang('portal/partials/register/section_thanks.title')</h3>
        <div class="content">
            <div class="thanks-img text-center">
                <img src="{{ asset('assets/portal/img/thanks.png') }}" alt="">
            </div>

            <p class="text-center bold">@lang('portal/partials/register/section_thanks.text1')</p>
            <p class="text-center">@lang('portal/partials/register/section_thanks.text2')</p>
        </div>
    </div>
</section><!-- End Section Register Thanks -->