<!-- Section Register -->
<section id="register">
    <div class="container register">
        {{Session::get('registration_sent')}}
        <h3 class="title">@lang('portal/partials/register/section_register.title')</h3>
        <div class="content">
            <form class="form-group pale" id="register_form" method="POST" action="{{ route('registration') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <input type="email" class="form-control input email-register1" autocomplete="off" name="email" id="inputEmail3" placeholder="@lang('portal/partials/register/section_register.email')">
                            <div class="error-message error-message1">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('portal/partials/register/section_register.invalid_email')
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block error-text">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="input-group-icon">
                            <input type="password" class="form-control input password-register1" autocomplete="off" name="password" id="inputPassword3" placeholder="@lang('portal/partials/register/section_register.password')">
                            <div class="text-center input-group-note">@lang('portal/partials/register/section_register.password_terms')</div>
                            <div class="error-message error-message1">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('passwords.invalid_password')
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block error-text">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <span class="group-addon noclose"></span>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="col-sm-6">
                        <div>
                            <input type="email" class="form-control input email-register2" autocomplete="off" name="email_confirmation" id="inputEmail3" placeholder="@lang('portal/partials/register/section_register.email_confirm')">
                            <div class="error-message error-message2">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('portal/partials/register/section_register.invalid_email')
                            </div>
                            <div class="error-message error-message3">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('portal/partials/register/section_register.invalid_email_confirmation')
                            </div>
                        </div>
                        <div class="input-group-icon">
                            <input type="password" class="form-control input password-register2" autocomplete="off" name="password_confirmation" id="inputPassword3" placeholder="@lang('portal/partials/register/section_register.password_confirm')">
                            @if ($errors->has('password'))
                                <span class="help-block error-text">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <div class="error-message error-message4">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('passwords.invalid_password')
                            </div>
                            <div class="error-message error-message5">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('passwords.invalid_password_confirmation')
                            </div>
                            <span class="group-addon noclose"></span>
                        </div>

                        <div class="clear"></div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        @if($disable && $email)
                            {!! trans('passwords.registered', ['email' => $email]) !!}
                            <br>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="text-center center-buttons">
                            <div class="btn-group register" id="register_submit_button">
                                <button type="submit" class="btn btn-md btn-blue register-form"  @if($disable) disabled @endif>@lang('portal/partials/register/section_register.register')</button>
                                <button type="submit" class="btn btn-md btn-green ico" @if($disable) disabled @endif><svg aria-hidden="true" class="icon icon-row-table " id="icones"><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-add-user') }}"></use></svg></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section><!-- End Section Register -->
