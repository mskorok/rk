@include('portal.views.partials.modals.card_nr_help')
	
<!-- Section Steps 1 -->
<section id="register-ecard">
    <div class="container">
        <h3 class="title">
            @lang('portal/partials/register/section_step_1.title1')
            <span class="step-count">@if($created) 1 @else 2 @endif</span>
            @if($created)
                @lang('portal/partials/register/section_step_1.title2')
            @else
                @lang('portal/partials/register/section_step_1.title3')
            @endif
        </h3>
        @if($errors->any())
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="content row" id="submit_error" style="margin-top: 40px; margin-bottom: 40px;">
                            @foreach($errors->all() as $error)
                                <div class="col-sm-12">
                                    <div class="center-buttons">
                                        <div style="float: left; margin-left: 20px; margin-right: 5px; padding-right: 5px; align-self: flex-start;">
                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                 style="width: 24px; height: 24px;"></div>
                                        <div style="float: left; text-align: left;">
                                            <div style="line-height: 16px; color: red;">
                                                {{ $error }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <ul class="nav nav-tabs">
            <!-- <div class="col-xs-6"> -->
            <li @if($created) style="display: none" @else   class="active" @endif><a data-toggle="tab"
                                  href="#add_ecard">@lang('portal/partials/register/section_step_1.add_card')</a></li>
            <!-- </div> -->
            <!-- <div class="col-xs-6"> -->
            <li @if($created) class="active" @endif><a data-toggle="tab"
                   href="#topup_ecard">@lang('portal/partials/register/section_step_1.card_replenishment')</a></li>
            <!-- </div> -->
        </ul>
        <!-- </section> -->
        <!-- <section id="register-ecard"> -->

        <div class="tab-content" >
            <div id="add_ecard" class="tab-pane fade @if(!$created) in active @endif">

                <div class="content row top light-accent /*full-width*/">
                    <div class="col-sm-12">
                        <ol>
                            <li>@lang('portal/partials/register/section_step_1.desc1')</li>
                            <li>@lang('portal/partials/register/section_step_1.desc2')</li>
                        </ol>
                    </div>
                </div>
                <form id="ajax_add_card_form" method="post" action="{{ route('create_card') }}">
                    {{ csrf_field() }}
                    <div id="e_card_container">
                        @if((is_array($cards) && count($cards) > 0) || ($cards instanceof \Illuminate\Database\Eloquent\Collection && $cards->count() > 0))
                            <input type="hidden" name="i" value="{{ count($cards) }}" id="amt">
                            @php
                                /** @var array $cards **/
                            @endphp
                            @foreach($cards as $i => $card)
                                @php
                                    $i++;
                                    $bgClass = ($i & 1) ?  'accent' : 'light-accent';
                                /** @var $cars \App\Models\Card **/
                                @endphp
                                <input type="hidden" name="child_photo_{{ $i }}" id="child_photo_{{ $i }}" value="{{ $card->child_photo }}">
                                <div class="content row {{ $bgClass }}" id="person_{{ $i }}">
                                    <div class="form-group pale">
                                        <div class="col-sm-12">
                                            <div class="content row stack">
                                                <div class="col-sm-9">
                                                    <div class="content row stack">
                                                        <div class="col-sm-4">
                                                            <div class="box-container switch-next" data-name="n{{ $i }}_card_number">
																@for($ii = 1; $ii <= 10; $ii++) 
																	@php
																		$strValue = 'card_'.$ii;
																	@endphp
                                                                <div>
                                                                    <input type="text" class="form-control input"
                                                                            name="n{{ $i }}_card_number_{{ $ii }}" value="{{ $card->$strValue }}" maxlength="1"
                                                                            data-id="1" id="n{{ $i }}_card_number_{{ $ii }}">
                                                                </div>
																@endfor
                                                            </div>
                                                            <div class="btn-group-note">
                                                                @lang('portal/partials/register/section_step_1.e_card_number')
                                                                (<a href="javascript:void 0"
                                                                    class="e-card-help">@lang('portal/partials/register/section_step_1.how_to_find_card_number')</a>)
                                                            </div>
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-xs-12 col-sm-8">
                                                            <div class="col-sm-8 col-xs-12" style="padding-left: 0">
                                                                <select class="select2-select limits" style="width: 100%;"
                                                                        name="n{{ $i }}_period" id="n{{ $i }}_period">
                                                                    <option></option>
                                                                    <option value="day" @if($card->limits === 'day') selected @endif>@lang('portal/partials/register/section_step_1.daily')</option>
                                                                    <option value="week" @if($card->limits === 'week') selected @endif>@lang('portal/partials/register/section_step_1.weekly')</option>
                                                                    <option value="month" @if($card->limits === 'month') selected @endif>@lang('portal/partials/register/section_step_1.monthly')</option>
                                                                </select>
                                                                <div class="error-message">
                                                                    <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                         alt="">
                                                                    @lang('validation.custom.error_text')
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 col-xs-12" style="padding-right: 0">
                                                                <input type="number" class="form-control input"
                                                                       name="n{{ $i }}_amount" placeholder="0.00" value="{{ number_format($card->amount, 2, '.', ' ') }}" step="0.01">
                                                                <div class="error-message">
                                                                    <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                         alt="">
                                                                    @lang('validation.max.numeric', ['attribute' => 'amount', 'max' => 140])
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content row stack">
                                                        <div class="col-sm-4 col-xs-6">
                                                            <input type="text" class="form-control input"
                                                                   name="n{{ $i }}_name" value="{{ $card->name }}"
                                                                   placeholder="@lang('portal/partials/register/section_step_1.name')">
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-6">
                                                            <input type="text" class="form-control input"
                                                                   name="n{{ $i }}_surname" value="{{ $card->surname }}"
                                                                   placeholder="@lang('portal/partials/register/section_step_1.surname')">
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12">
                                                            <div class="box-container p-code switch-next" data-name="n{{ $i }}_pk">
																@for($ii = 1; $ii <= 11; $ii++) 
																	@php
																		$strValue = 'pk_'.$ii;
																	@endphp
																<div>
                                                                    <input type="text" class="form-control input"
                                                                            name="n{{ $i }}_pk_{{ $ii }}" value="{{ $card->$strValue }}" maxlength="1"
                                                                            data-id="{{ $ii }}" id="n{{ $i }}_pk_{{ $ii }}">
																</div>
																@if($ii == 6)
																<div></div>
																@endif	
																@endfor
                                                            </div>
                                                            <div class="btn-group-note">@lang('portal/partials/register/section_step_1.personal_identifier')</div>
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content row">
                                                        <div class="col-sm-4 col-xs-12">
                                                            <select class="select2-select schools" style="width: 100%;"
                                                                    name="n{{ $i }}_school" id="n{{ $i }}_school">
                                                                @foreach($schools as $number => $school)
                                                                    <option value="{{ $number }}" @if((int) $number === $card->school_id ) selected @endif>{{ $school }}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12">
                                                            <input type="text" class="form-control input"
                                                                   name="n{{ $i }}_class"
                                                                   placeholder="@lang('portal/partials/register/section_step_1.class')" value="{{ $card->class }}">
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-3 col-xs-12">
                                                    <div class="large-padding"
                                                         style="position: relative; padding-top: 50%;">
                                                        <div class="btn-group small-btn-group pale" id="btn-add-new-photo_{{ $i }}" data-id="{{ $i }}">
                                                            <button type="button" class="btn btn-sm btn-blue" data-id="{{ $i }}">
                                                                @lang('portal/partials/register/section_step_1.add_child_photo')
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-siel ico" data-id="{{ $i }}">
                                                                <svg aria-hidden="true" class="icon icon-row-table ">
                                                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        {{--<div>--}}
                                                            {{--<img src="" style="display: none" id="new_card_photo_{{ $i }}" />--}}
                                                        {{--</div>--}}
                                                        <div class="btn-group-note">
                                                            @lang('portal/partials/register/section_step_1.allowed_formats')
                                                            .
                                                            <br/>
                                                            @lang('portal/partials/register/section_step_1.max_image_size')
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @include(
                                    'portal.views.partials.modals.add_child_photo',
                                    ['i' => $i, 'card' => $card, 'id' => $card->id, 'type' => AVATAR_TYPE_CARD]
                                )
                            @endforeach
                            <script>
                                document.addEventListener('DOMContentLoaded', function (e) {
                                    var input = document.getElementById('amt');
                                    var amount = parseInt(input.value);

                                    setTimeout(function () {
                                        for (var i = 1; i <= amount; i++ ) {
                                            e_card_fnc.validate_e_card_item(i);
//                                            var selector = '#modal-add-child-photo_' + i;
                                            $('#btn-add-new-photo_' + i + ' .btn').click(function() {
                                                var i = $(this).attr('data-id');
                                                var selector = '#modal-add-child-photo_' + i;
                                                $(selector).modal('show');
                                            });
                                        }
                                    }, 200);
                                });
                            </script>
                        @elseif(isset($old) && count($old) > 0)
                            <input type="hidden" name="i" value="{{ $old['i'] }}" id="amt">
                            @php
                                /** @var array $cards **/
                                $i = 0;
                            @endphp
                            @for($i = 1; $i <= $old['i']; $i++)
                                @php
                                    $bgClass = ($i & 1) ?  'accent' : 'light-accent';
                                /** @var $cars \App\Models\Card **/
                                @endphp
                                <input type="hidden" name="child_photo_{{ $i }}" id="child_photo_{{ $i }}" value="{{ $old['child_photo_'. $i] }}">
                                <div class="content row {{ $bgClass }}" id="person_{{ $i }}">
                                    <div class="form-group pale">
                                        <div class="col-sm-12">
                                            <div class="content row stack">
                                                <div class="col-sm-9">
                                                    <div class="content row stack">
                                                        <div class="col-sm-4">
                                                            <div class="box-container switch-next" data-name="n{{ $i }}_card_number">
																@for($ii = 1; $ii <= 10; $ii++) 
                                                                <div>
                                                                    <input type="text" class="form-control input"
                                                                            name="n{{ $i }}_card_number_{{ $ii }}" value="{{ $old['n'.$i.'_card_number_'.$ii] }}" maxlength="1"
                                                                            data-id="{{ $ii }}" id="n{{ $i }}_card_number_{{ $ii }}">
                                                                </div>
																@endfor
                                                            </div>
                                                            <div class="btn-group-note">
                                                                @lang('portal/partials/register/section_step_1.e_card_number')
                                                                (<a href="javascript:void 0"
                                                                    class="e-card-help">@lang('portal/partials/register/section_step_1.how_to_find_card_number')</a>)
                                                            </div>
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-xs-12 col-sm-8">
                                                            <div class="col-sm-8 col-xs-12" style="padding-left: 0">
                                                                <select class="select2-select limits" style="width: 100%;"
                                                                        name="n{{ $i }}_period" id="n{{ $i }}_period">
                                                                    <option></option>
                                                                    <option value="day" @if($old['n'.$i.'_period'] === 'day') selected @endif>@lang('portal/partials/register/section_step_1.daily')</option>
                                                                    <option value="week" @if($old['n'.$i.'_period'] === 'week') selected @endif>@lang('portal/partials/register/section_step_1.weekly')</option>
                                                                    <option value="month" @if($old['n'.$i.'_period'] === 'month') selected @endif>@lang('portal/partials/register/section_step_1.monthly')</option>
                                                                </select>
                                                                <div class="error-message">
                                                                    <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                         alt="">
                                                                    @lang('validation.custom.error_text')
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 col-xs-12" style="padding-right: 0">
                                                                <input type="text" class="form-control input"
                                                                       name="n{{ $i }}_amount" placeholder="0.00" value="{{ $old['n'.$i.'_amount'] }}">
                                                                <div class="error-message">
                                                                    <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                         alt="">
                                                                    @lang('validation.max.numeric', ['attribute' => 'amount', 'max' => 140])
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content row stack">
                                                        <div class="col-sm-4 col-xs-6">
                                                            <input type="text" class="form-control input"
                                                                   name="n{{ $i }}_name" value="{{ $old['n'.$i.'_name'] }}"
                                                                   placeholder="@lang('portal/partials/register/section_step_1.name')">
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-6">
                                                            <input type="text" class="form-control input"
                                                                   name="n{{ $i }}_surname" value="{{ $old['n'.$i.'_surname'] }}"
                                                                   placeholder="@lang('portal/partials/register/section_step_1.surname')">
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12">
                                                            <div class="box-container p-code switch-next" data-name="n{{ $i }}_pk">
																@for($ii = 1; $ii <= 11; $ii++) 
                                                                <div>
                                                                    <input type="text" class="form-control input"
                                                                            name="n{{ $i }}_pk_{{ $ii }}" value="{{ $old['n'.$i.'_pk_'.$ii] }}" maxlength="1"
                                                                            data-id="{{ $ii }}" id="n{{ $i }}_pk_{{ $ii }}">
                                                                </div>
																@if($ii == 6)
																<div></div>
																@endif	
																@endfor
                                                            </div>
                                                            <div class="btn-group-note">@lang('portal/partials/register/section_step_1.personal_identifier')</div>
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content row">
                                                        <div class="col-sm-4 col-xs-12">
                                                            <select class="select2-select schools" style="width: 100%;"
                                                                    name="n{{ $i }}_school" id="n{{ $i }}_school">
                                                                @foreach($schools as $number => $school)
                                                                    <option value="{{ $number }}" @if((int) $number === $old['n'.$i.'_school'] ) selected @endif>{{ $school }}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12">
                                                            <input type="text" class="form-control input"
                                                                   name="n{{ $i }}_class"
                                                                   placeholder="@lang('portal/partials/register/section_step_1.class')" value="{{ $old['n'.$i.'_class'] }}">
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-3 col-xs-12">
                                                    <div class="large-padding"
                                                         style="position: relative; padding-top: 50%;">
                                                        <div class="btn-group small-btn-group pale" id="btn-add-new-photo_{{ $i }}" data-id="{{ $i }}">
                                                            <button type="button" class="btn btn-sm btn-blue" data-id="{{ $i }}">
                                                                @lang('portal/partials/register/section_step_1.add_child_photo')
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-siel ico" data-id="{{ $i }}">
                                                                <svg aria-hidden="true" class="icon icon-row-table ">
                                                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        {{--<div>--}}
                                                            {{--<img src="" style="display: none" id="new_card_photo_{{ $i }}" />--}}
                                                        {{--</div>--}}
                                                        <div class="btn-group-note">
                                                            @lang('portal/partials/register/section_step_1.allowed_formats')
                                                            .
                                                            <br/>
                                                            @lang('portal/partials/register/section_step_1.max_image_size')
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @include(
                                    'portal.views.partials.modals.add_child_photo',
                                    ['i' => $old['i'], 'card' => 0, 'id' => 0, 'type' => AVATAR_TYPE_CARD]
                                )
                            @endfor
                            <script>
                                document.addEventListener('DOMContentLoaded', function (e) {
                                    var input = document.getElementById('amt');
                                    var amount = parseInt(input.value);

                                    setTimeout(function () {
                                        for (var i = 1; i <= amount; i++ ) {
                                            e_card_fnc.validate_e_card_item(i);
//                                            var selector = '#modal-add-child-photo_' + i;
                                            $('#btn-add-new-photo_' + i + ' .btn').click(function() {
                                                var i = $(this).attr('data-id');
                                                var selector = '#modal-add-child-photo_' + i;
                                                $(selector).modal('show');
                                            });
                                        }
                                    }, 200);
                                });
                            </script>
                        @else
                            @php
                                $i = 1;
                            @endphp
                            <input type="hidden" name="i" value="0" id="amt">
                            <input type="hidden" name="child_photo_{{ $i }}" id="child_photo_{{ $i }}">
                            <div class="content row accent" id="person_{{ $i }}">
                                <div class="form-group pale">
                                    <div class="col-sm-12">
                                        <div class="content row stack">
                                            <div class="col-sm-9">
                                                <div class="content row stack">
                                                    <div class="col-sm-4">
                                                        <div class="box-container switch-next" data-name="n{{ $i }}_card_number">
															@for($ii = 1; $ii <= 10; $ii++) 
                                                            <div>
																<input type="text" class="form-control input" maxlength="1"
                                                                        name="n{{ $i }}_card_number_{{ $ii }}"
																		data-id="{{ $ii }}" id="n{{ $i }}_card_number_{{ $ii }}">
															</div>
															@endfor
                                                        </div>
                                                        <div class="btn-group-note">
                                                            @lang('portal/partials/register/section_step_1.e_card_number')
                                                            (<a href="javascript:void 0"
                                                                class="e-card-help">@lang('portal/partials/register/section_step_1.how_to_find_card_number')</a>)
                                                        </div>
                                                        <div class="error-message">
                                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                 alt="">
                                                            @lang('validation.custom.error_text')
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-xs-12 col-sm-8">
                                                        <div class="col-sm-8 col-xs-12" style="padding-left: 0">
                                                            <select class="select2-select limits" style="width: 100%;"
                                                                    name="n{{ $i }}_period" id="n{{ $i }}_period">
                                                                <option></option>
                                                                <option value="day">@lang('portal/partials/register/section_step_1.daily')</option>
                                                                <option value="week">@lang('portal/partials/register/section_step_1.weekly')</option>
                                                                <option value="month">@lang('portal/partials/register/section_step_1.monthly')</option>
                                                            </select>
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.custom.error_text')
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12" style="padding-right: 0">
                                                            <input type="number" class="form-control input"
                                                                   name="n{{ $i }}_amount" placeholder="0.00" step="0.01">
                                                            <div class="error-message">
                                                                <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                     alt="">
                                                                @lang('validation.max.numeric', ['attribute' => 'amount', 'max' => 140])
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content row stack">
                                                    <div class="col-sm-4 col-xs-6">
                                                        <input type="text" class="form-control input"
                                                               name="n{{ $i }}_name"
                                                               placeholder="@lang('portal/partials/register/section_step_1.name')">
                                                        <div class="error-message">
                                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                 alt="">
                                                            @lang('validation.custom.error_text')
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-6">
                                                        <input type="text" class="form-control input"
                                                               name="n{{ $i }}_surname"
                                                               placeholder="@lang('portal/partials/register/section_step_1.surname')">
                                                        <div class="error-message">
                                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                 alt="">
                                                            @lang('validation.custom.error_text')
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-12">
                                                        <div class="box-container p-code switch-next" data-name="n{{ $i }}_pk">
															@for($ii = 1; $ii <= 11; $ii++) 
															<div>
																<input type="text" class="form-control input" maxlength="1"
																		name="n{{ $i }}_pk_{{ $ii }}"
																		data-id="{{ $ii }}" id="n{{ $i }}_pk_{{ $ii }}">
															</div>
															@if($ii == 6)
															<div></div>
															@endif	
															@endfor
                                                        </div>
                                                        <div class="btn-group-note">@lang('portal/partials/register/section_step_1.personal_identifier')</div>
                                                        <div class="error-message">
                                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                 alt="">
                                                            @lang('validation.custom.error_text')
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content row">
                                                    <div class="col-sm-4 col-xs-12">
                                                        <select class="select2-select schools" style="width: 100%;"
                                                                name="n{{ $i }}_school" id="n{{ $i }}_school">
                                                            @include('portal.views.partials.register.schools')
                                                        </select>
                                                        <div class="error-message">
                                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                 alt="">
                                                            @lang('validation.custom.error_text')
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-12">
                                                        <input type="text" class="form-control input"
                                                               name="n{{ $i }}_class"
                                                               placeholder="@lang('portal/partials/register/section_step_1.class')">
                                                        <div class="error-message">
                                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                 alt="">
                                                            @lang('validation.custom.error_text')
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-3 col-xs-12">
                                                <div class="large-padding"
                                                     style="position: relative; padding-top: 50%;">
                                                    <div class="btn-group small-btn-group pale" id="btn-add-new-photo_{{ $i }}" data-id="{{ $i }}">
                                                        <button type="button" class="btn btn-sm btn-blue" data-id="{{ $i }}">
                                                            @lang('portal/partials/register/section_step_1.add_child_photo')
                                                        </button>
                                                        <button type="button" class="btn btn-sm btn-siel ico" data-id="{{ $i }}">
                                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    {{--<div>--}}
                                                        {{--<img src="" style="display: none" id="new_card_photo_{{ $i }}" />--}}
                                                    {{--</div>--}}
                                                    <div class="btn-group-note">
                                                        @lang('portal/partials/register/section_step_1.allowed_formats').
                                                        <br/>
                                                        @lang('portal/partials/register/section_step_1.max_image_size')
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    document.addEventListener('DOMContentLoaded', function (e) {
                                        var input = document.getElementById('amt');
                                        var amount = parseInt(input.value);
                                        amount += 1;
                                        input.value = amount;
                                        e_card_fnc.validate_e_card_item(amount);
//                                        var selector = '#modal-add-child-photo_' + amount;
                                        $('#btn-add-new-photo_' + amount + ' .btn').click(function() {
                                            var i = $(this).attr('data-id');
                                            var selector = '#modal-add-child-photo_' + i;
                                            $(selector).modal('show');
                                        });
                                    });
                                </script>
                            </div>
                            @include(
                                'portal.views.partials.modals.add_child_photo',
                                ['i' => $i, 'card' => 0, 'id' => 0, 'type' => AVATAR_TYPE_CARD]
                            )
                        @endif
                    </div>
                </form>


                <div class="content row accent">
                    <div class="form-group pale">
                        <div class="col-sm-12">
                            <div class="content row">
                                <div class="col-sm-3">
                                    <div style="padding-bottom: 20px; padding-top: 10px;">
                                        <div class="btn-group small-btn-group pale" id="add-person">
                                            <button type="button"
                                                    class="btn btn-sm btn-blue">@lang('portal/partials/register/section_step_1.add_more')</button>
                                            <button type="button" class="btn btn-sm btn-siel ico">
                                                <svg aria-hidden="true" class="icon icon-row-table ">
                                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-add') }}"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div style="padding-bottom: 20px; padding-top: 10px;">
                                        @lang('portal/partials/register/section_step_1.approval_color')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content row top stack" style="margin-top: 20px;">
                    <div class="col-sm-6 col-xs-12">
                        <div style="float: right;">
                            <div class="btn-group" id="next-step">
                                <button type="button"
                                        class="btn btn-md btn-blue">@lang('portal/partials/register/section_step_1.next_step')</button>
                                <button type="button" class="btn btn-md btn-green ico">
                                    <svg aria-hidden="true" class="icon icon-row-table ">
                                        <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-next') }}"></use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="btn-group" id="save-cards">
                            <button type="button"
                                    class="btn btn-md btn-blue">@lang('portal/partials/register/section_step_1.complete_later')</button>
                            <button type="button" class="btn btn-md btn-orange ico">
                                <svg aria-hidden="true" class="icon icon-row-table ">
                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-later') }}"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

            </div>


            <div id="topup_ecard" class="tab-pane fade  @if($created) in active @endif">


                <div id="person_1_topup" @if($filled) style="display: none" @endif>

                    <div class="content row top light-accent">
                        <div class="col-sm-12">
                            <ol>
                                <li>@lang('portal/partials/register/section_step_1.desc_2_1')</li>
                                <li>@lang('portal/partials/register/section_step_1.desc_2_2')</li>
                                <li>@lang('portal/partials/register/section_step_1.desc_2_3')</li>
                            </ol>
                        </div>
                    </div>
                    <form id="refill_cards_form" action="{{ route('fill_card') }}" method="post">
                        {{ csrf_field() }}
                        @if((is_array($cards) && count($cards) > 0) || ($cards instanceof \Illuminate\Database\Eloquent\Collection && $cards->count() > 0))
                            @foreach($cards as $i => $card)
                                @php
                                    $i++;
                                    $bgClass = ($i & 1) ?  'accent' : 'light-accent';
                                    $image = \App\Models\Image::where('parent_id', 0)->where('id', $card->child_photo)->first();
                                    /** @var \App\Models\Image $image */
                                    $src = $image ? $image->path.$image->filename : 'assets/img/no_avatar.jpg';
                                /** @var $cars \App\Models\Card **/
                                @endphp
                                <div class="content row {{ $bgClass }}" style="color: #064874;">
                                    <div class="form-group pale">
                                        <div class="col-sm-12">
                                            <div class="content row stack" style="min-height: 120px;">

                                                <div class="col-sm-5 person-line vertical-center col-xs-12 large-padding">
                                                    <p class="person-content"
                                                       style="text-align: right;">@lang('portal/partials/register/section_step_1.replenishment_sum')</p>
                                                </div>
                                                <div class="col-sm-2 person-line vertical-center col-xs-3">
                                                    <div class="person-content">
                                                        <div style="width: 100px; height: 100px; border-radius: 100% 100% 100% 100%; border: solid 2px #87c2e3;">
                                                            <img src="{{ asset($src) }}"
                                                                 class="circle-img" style="width: 100%; height: 100%;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 person-line vertical-center col-xs-5">
                                                    <div class="person-content">
                                                        <h4>{{ $card->name }} &nbsp; {{ $card->surname }}</h4>
                                                        <span style="font-size: 12px;">{{ $card->getCardNumber() }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 person-line vertical-center col-xs-4">
                                                    <div class="person-content">
                                                        <div class="person-content">
                                                            <div>@lang('portal/partials/register/section_step_1.sum')</div>
                                                            <div style="display: table; overflow: hidden;">
                                                                <!--<div style="display: table-cell; vertical-align: middle;">-->
                                                                <input type="text" class="form-control input"
                                                                       value="{{ $card->top_up_sum }}"
                                                                       name="top_up_sum_{{ $i }}"
                                                                       placeholder="0"
                                                                       style="width: 70px; margin-bottom: 0; float: left; margin-right: 10px;">
                                                                <span>€</span>
                                                                <div class="error-message">
                                                                    <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                         alt="">
                                                                    @lang('validation.custom.error_text')
                                                                </div>
                                                                <!--</div>-->
                                                            </div>
                                                            <div>(@lang('portal/partials/register/section_step_1.without_cents'))
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif(isset($old) && count($old) > 0)
                            @foreach($cards as $i => $card)
                                @php
                                    $i++;
                                    $bgClass = ($i & 1) ?  'accent' : 'light-accent';
                                /** @var $cars \App\Models\Card **/
                                @endphp
                                <div class="content row {{ $bgClass }}" style="color: #064874;">
                                    <div class="form-group pale">
                                        <div class="col-sm-12">
                                            <div class="content row stack" style="min-height: 120px;">

                                                <div class="col-sm-5 person-line vertical-center col-xs-12 large-padding">
                                                    <p class="person-content"
                                                       style="text-align: right;">@lang('portal/partials/register/section_step_1.replenishment_sum')</p>
                                                </div>
                                                <div class="col-sm-2 person-line vertical-center col-xs-3">
                                                    <div class="person-content">
                                                        <div style="width: 100px; height: 100px; border-radius: 100% 100% 100% 100%; border: solid 2px #87c2e3;">
                                                            <img src="{{ asset('assets/portal/img/wizzard01.jpg') }}"
                                                                 class="circle-img" style="width: 100%; height: 100%;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 person-line vertical-center col-xs-5">
                                                    <div class="person-content">
                                                        <h4>{{ $card->name }} &nbsp; {{ $card->surname }}</h4>
                                                        <span style="font-size: 12px;">{{ $card->getCardNumber() }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 person-line vertical-center col-xs-4">
                                                    <div class="person-content">
                                                        <div class="person-content">
                                                            <div>@lang('portal/partials/register/section_step_1.sum')</div>
                                                            <div style="display: table; overflow: hidden;">
                                                                <!--<div style="display: table-cell; vertical-align: middle;">-->
                                                                <input type="text" class="form-control input"
                                                                       value="{{ $old['top_up_sum_'.$i] }}"
                                                                       name="top_up_sum_{{ $i }}"
                                                                       placeholder="0"
                                                                       style="width: 70px; margin-bottom: 0; float: left; margin-right: 10px;">
                                                                <span>€</span>
                                                                <div class="error-message">
                                                                    <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                         alt="">
                                                                    @lang('validation.custom.error_text')
                                                                </div>
                                                                <!--</div>-->
                                                            </div>
                                                            <div>(@lang('portal/partials/register/section_step_1.without_cents'))
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <script>
                            document.addEventListener('DOMContentLoaded', function (e) {
                                var input = document.getElementById('amt');
                                var amount = parseInt(input.value);

                                setTimeout(function () {
                                    for (var i = 1; i <= amount; i++ ) {
                                        e_card_fnc.validate_amount_item(i);
                                    }
                                }, 300);
                            });
                        </script>

                        <div class="content row top stack" style="margin-top: 20px;">
                            <div class="col-sm-6 col-xs-12">
                                <div style="float: right;">
                                    <div class="btn-group" id="next-step-approve">
                                        <button type="button"
                                                class="btn btn-md btn-blue">@lang('portal/partials/register/section_step_1.approve')</button>
                                        <button type="submit" class="btn btn-md btn-green ico">
                                            <svg aria-hidden="true" class="icon icon-row-table " id="icones">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="btn-group" id="save-payments">
                                    <button type="button"
                                            class="btn btn-md btn-blue">@lang('portal/partials/register/section_step_1.complete_later')</button>
                                    <button type="button" class="btn btn-md btn-orange ico">
                                        <svg aria-hidden="true" class="icon icon-row-table ">
                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-later') }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="content row top">
                        <div class="col-sm-12" style="text-align: center" id="interrupt_top_up">
                            <a href="javascript:void(0)">@lang('portal/partials/register/section_step_1.run')</a>
                        </div>
                    </div>

                </div>

                <div id="person_1_topup_final" @if(!$filled) style="display: none" @endif>

                    <div class="content row top light-accent">
                        <div class="col-sm-12">
                            <ol>
                                <li>@lang('portal/partials/register/section_step_1.desc_2_1')</li>
                                <li>@lang('portal/partials/register/section_step_1.desc_2_2')</li>
                                <li>@lang('portal/partials/register/section_step_1.desc_2_3')</li>
                            </ol>
                        </div>
                    </div>

                    @if((is_array($cards) && count($cards) > 0) || ($cards instanceof \Illuminate\Database\Eloquent\Collection && $cards->count() > 0))
                        <input type="hidden" name="i" value="{{ count($cards) }}" id="amt">
                        @php
                            /** @var array $cards **/
                        @endphp
                        @foreach($cards as $i => $card)
                            @php
                                $i++;
                                $bgClass = ($i & 1) ?  'accent' : 'light-accent';
                            /** @var $cars \App\Models\Card **/
                            @endphp
                            <div class="content row {{ $bgClass }}" style="color: #064874;">
                                <div class="form-group pale">
                                    <div class="col-sm-12">
                                        <div class="content row"
                                             style="min-height: 120px; padding-bottom: 30px; padding-top: 20px;">

                                            <div class="col-sm-2 person-line"></div>
                                            <div class="col-sm-8 person-line vertical-center">
                                                <div class="center-buttons" style="text-align: center;">
                                                    <h5 style="color: green; font-weight: 600;">
                                                        @lang('portal/partials/register/section_step_1.payment_created_1', ['name' => $card->name, 'surname' => $card->surname])
                                                        &nbsp; ({{ $card->getCardNumber() }})
                                                        &nbsp;@lang('portal/partials/register/section_step_1.payment_created_2')
                                                        &nbsp; {{ $card->amount }} € –
                                                        &nbsp;@lang('portal/partials/register/section_step_1.payment_created_3')
                                                        .
                                                    </h5>
                                                </div>
                                                <div class="content row left">
                                                    <div class="col-sm-6 center-buttons col-xs-12">
                                                        <div class="col-sm-12 no-padding">
                                                            <h5 style="color: #064874; font-weight: 600;">
                                                                @lang('portal/partials/register/section_step_1.payment_requisites')
                                                                :
                                                            </h5>
                                                            <table style="color: #000; font-size: 12px;">
                                                                <tr>
                                                                    <td style="min-width: 80px;">@lang('portal/partials/register/section_step_1.recipient')
                                                                        :
                                                                    </td>
                                                                    <td>{{ $requisites->name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Reg.Nr.:</td>
                                                                    <td>{{ $requisites->reg_nr }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>@lang('portal/partials/register/section_step_1.recipient_address')
                                                                        :
                                                                    </td>
                                                                    <td>{{ $requisites->jur_adrese }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Bank:</td>
                                                                    <td>{{ $requisites->banka }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>@lang('portal/partials/register/section_step_1.recipient_code')
                                                                        :
                                                                    </td>
                                                                    <td>{{ $requisites->bankas_kods }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>@lang('portal/partials/register/section_step_1.recipient_account')
                                                                        :
                                                                    </td>
                                                                    <td>{{ $requisites->konts }}</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 style="color: #064874; font-weight: 600;">@lang('portal/partials/register/section_step_1.show_identifier')
                                                            :</h5>
                                                        <div class="invoice-identifier">{{ $card->payment_code }}</div>
                                                        <h5 style="color: #064874; font-weight: 600;">@lang('portal/partials/register/section_step_1.payment_order')
                                                            :</h5>
                                                        <a href="{{ route('download_payment', ['pdf' => $card->payment_id]) }}"  download class="btn-group small-btn-group pale download-invoice" id="download-invoice-{{ $i }}">
                                                            <button type="button"
                                                                    class="btn btn-sm btn-blue">@lang('portal/partials/register/section_step_1.download')</button>
                                                            <button type="button" class="btn btn-sm btn-siel ico">
                                                                <svg aria-hidden="true" class="icon icon-row-table ">
                                                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-pdf') }}"></use>
                                                                </svg>
                                                            </button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2 person-line"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <div class="content row top" style="margin-top: 20px;">
                        <div class="col-sm-12">
                            <div class="center-buttons">
                                <div class="btn-group" id="next-step-close">
                                    <button type="button"
                                            class="btn btn-md btn-blue">@lang('portal/partials/register/section_step_1.complete')</button>
                                    <button type="button" class="btn btn-md btn-green ico">
                                        <svg aria-hidden="true" class="icon icon-row-table ">
                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section Steps 1 end -->