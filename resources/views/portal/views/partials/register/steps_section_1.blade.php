<!-- Section Steps 1 -->
<section>
    <div class="container">
        <h3 class="title">
            @lang(
            'portal/partials/register/steps_section_1.title',
             ['name' => $data->FirstName, 'surname' => $data->LastName]
             )
        </h3>
        <input type="hidden" name="username" value="{{ $data->FirstName }}">
        <input type="hidden" name="surname" value="{{ $data->LastName }}">
        <input type="hidden" name="pk" value="{{ $data->PersonCode }}">
        <div class="subtitle text-center">@lang('portal/partials/register/steps_section_1.subtitle')</div>
        <div class="content row">
            <div class="form-group pale">

                <input type="hidden" name="email" value="{{ $email }}">
                <div class="col-sm-3">
                    <div class="input-group-icon">
                        <input type="email" class="form-control input" placeholder="{{ $email }}" disabled>
                    <!--                   <input type="text" name="profile-email" id="profile-email" class="input form-control disabled" readonly value="lienite.mezrozite@latepasts.lv" style="width: 240px; margin-bottom: 0px;" /> -->
                        <span class="group-addon noclose svg"><div style="font-size: 18px !important; right: 5px; position: relative;"><svg aria-hidden="true" class="icon icon-row-table green"><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-ok') }}"></use></svg>
                </div></span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <input type="tel" class="form-control input" name="phone"
                           @if(isset($old['phone'])) value="{{ $old['phone'] }}" @endif
                           placeholder="@lang('portal/partials/register/steps_section_1.phone')">
                    <div class="error-message">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.error_phone')
                    </div>
                </div>

                <div class="col-sm-6">
                    <input type="text" class="form-control input" name="address"
                           @if(isset($old['address'])) value="{{ $old['address'] }}" @endif
                           placeholder="@lang('portal/partials/register/steps_section_1.address')">
                    <div class="error-message">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.error_text')
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-sm-2">
                    <select class="select2-select profile-nationality" id="profile_citizenship"  name="citizenship"
                            style="width: 180px;">
                        <option></option>
                        <option value="Latvijas pilsonis" @if((int) $citizenIndex === 1) selected @endif >
                            @lang('portal/partials/profile/section_step_3.citizen')
                        </option>
                        <option value="Latvijas nepilsonis" @if((int) $citizenIndex === 2) selected @endif >
                            @lang('portal/partials/profile/section_step_3.non_citizen')
                        </option>
                        <option value="Uzturēšanas atļauja" @if((int) $citizenIndex === 3) selected @endif >
                            @lang('portal/partials/profile/section_step_3.temporary_permission')
                        </option>
                    </select>
                    <div class="error-message">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.error_text')
                    </div>
                </div>
                <div class="col-sm-2">
                    <select class="select2-select profile-doctype" id="doc_type" name="doc_type" style="width: 180px;">
                        <option></option>
                        <option value="Pase" @if((int) $docTypeIndex === 1) selected @endif >
                            @lang('portal/partials/profile/section_step_3.passport')
                        </option>
                        <option value="eID" @if((int) $docTypeIndex === 2) selected @endif >
                            @lang('portal/partials/profile/section_step_3.id_card')
                        </option>
                        <option value="Cits documents" @if((int) $docTypeIndex === 3) selected @endif >
                            @lang('portal/partials/profile/section_step_3.other_person_document')
                        </option>
                    </select>
                    <div class="btn-group-note">&nbsp;</div>
                    <div class="error-message">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.error_text')
                    </div>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control input" name="doc_number"
                           @if(isset($old['doc_number'])) value="{{ $old['doc_number'] }}" @endif
                           placeholder="@lang('portal/partials/register/steps_section_1.doc_number')">
                    <div class="error-message">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.error_text')
                    </div>
                </div>
                <div class="col-sm-2">
                    <input type="text" name="document_date"
                           @if(isset($old['document_date'])) value="{{ $old['document_date'] }}" @else value="" @endif
                           class="input form-control"  style="/*width: 130px;margin-bottom: 0px; */" />
                    <span class="group-addon noclose svg"><div style="font-size: 18px; right: 15px; position: relative;"><svg aria-hidden="true" class="icon icon-header-row-arrow"><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-calendar') }}"></use></svg></div></span>
                    <div class="error-message">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.error_text')
                    </div>
                    <div class="error-message1" style="display: none">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.document_expired')
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="btn-group small-btn-group">
                        <button type="button" class="btn btn-sm btn-blue register-form" id="doc_image_button">@lang('portal/partials/register/steps_section_1.add_doc_copy')</button>
                        <button type="button" class="btn btn-sm btn-siel ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use></svg></button>
                    </div>
                    <div><input type="file" id="doc_copy" name="doc_copy" style="display: none;" /></div>
                    <div class="btn-group-note">
                        <span id="doc_name"></span>
                        <span id="not_attached" style="color: red;float: right; padding-right: 40px;">@lang('validation.required') &nbsp; *</span>
                        <br />
                        <span id="allowed_formats">
                            @lang('portal/partials/register/steps_section_1.allowed_formats').
                        </span>
                        <br />
                        <span id="max_image_size">
                            @lang('portal/partials/register/steps_section_1.max_image_size')
                        </span>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section><!-- End Section Steps 1 -->