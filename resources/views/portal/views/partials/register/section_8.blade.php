<!-- Section 8 -->
<section class="bg-color" id="section8">
    <div class="container">
        <h3 class="title">@lang('portal/partials/register/section_8.title')</h3>
        <div class="content">
            <form class="form-group" id="feedback_form" method="post" action="{{ route('feedback') }}">
                {{ csrf_field() }}
                <div class="col-md-3 col-xs-6">
                    <div>
                        <input type="text" class="form-control input" id="support_name" name="support_name" placeholder="@lang('portal/partials/register/section_8.name')">
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('portal/partials/register/section_8.error_name')
                        </div>
                    </div>
                    <div>
                        <input type="email" class="form-control input email" id="support_email" name="support_email" placeholder="@lang('portal/partials/register/section_8.email')">
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('portal/partials/register/section_8.error_email')
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="success-message">
                        <img src="{{ asset('assets/portal/img/success.png') }}" alt="">
                        @lang('portal/partials/register/section_8.success')
                    </div>
                </div>
                <div class="col-md-3 col-xs-6">
                    <div>
                        <input type="text" class="form-control input" id="support_surname" name="support_surname" placeholder="@lang('portal/partials/register/section_8.surname')">
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('portal/partials/register/section_8.error_name')
                        </div>
                    </div>
                    <div>
                        <input type="tel" class="form-control input" id="support_phone" name="support_phone" placeholder="@lang('portal/partials/register/section_8.phone')">
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('portal/partials/register/section_8.error_phone')
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <textarea class="form-control textarea" id="support_question" name="support_question" rows="3" placeholder="@lang('portal/partials/register/section_8.questions')"></textarea>
                </div>
                <div class="col-xs-12 text-center">
                    <button class="btn btn-blue" id="feedback_submit_button" type="submit">@lang('portal/partials/register/section_8.send')</button>
                    <div class="col-xs-12 messages">
                        <div class="success-message" style="text-align: left;">
                            <img src="{{ asset('assets/portal/img/success.png') }}" alt=""> @lang('portal/partials/register/section_8.sent')
                        </div>
                        <div class="error-message" style="text-align: left;">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt=""> @lang('portal/partials/register/section_8.fill_all_fields')
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- End Section 8 -->