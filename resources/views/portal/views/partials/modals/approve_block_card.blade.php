<!-- Modal:Approve Delete Card -->
<div class="modal fade modal-warning-block js-next-modal" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal-warning-block">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span id="card_blocked_title">@lang('portal/partials/modals/approve_block_card.title_block')</span>
                <span id="card_unblocked_title" style="display: none">@lang('portal/partials/modals/approve_block_card.title_unblock')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <span id="card_blocked" style="display: none">@lang('portal/partials/modals/approve_block_card.success_block')</span>
                <span id="card_unblocked" style="display: none">@lang('portal/partials/modals/approve_block_card.success_unblock')</span>
                <div  id="block_card_desc">
                    <p class="text-center bold">@lang('portal/partials/modals/approve_block_card.are_you_sure')</p>
                    <p class="text-center" id="block_modal_name"></p>
                </div>
                <div style="display: none;" id="unblock_card_desc">
                    <p class="text-center bold">@lang('portal/partials/modals/approve_block_card.are_you_sure_unblock')</p>
                    <p class="text-center" id="block_modal_name"></p>
                </div>
                <div class="btn-group-centered">
                    <div class="btn-group small-btn-group in-a-row modal-yes" id="block_card_modal" data-i="0" data-id="0">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/approve_block_card.yes')</button>
                        <button type="button" class="btn btn-sm btn-green ico">
                            <svg aria-hidden="true" class="icon icon-row-table ">
                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="btn-group small-btn-group modal-no" id="not_block_card_modal">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/approve_block_card.no')</button>
                        <button type="button" class="btn btn-sm btn-red ico">
                            <svg aria-hidden="true" class="icon icon-row-table ">
                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-cancel') }}"></use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Approve Delete Card -->