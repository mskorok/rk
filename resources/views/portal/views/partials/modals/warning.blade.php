<!-- Modal:Warning -->
<div class="modal fade modal-warning-1 js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/warning.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center bold">@lang('portal/partials/modals/warning.text1')</p>
                <p class="text-center">@lang('portal/partials/modals/warning.text2')</p>
                <div class="btn-group-centered">
                    <div class="btn-group small-btn-group in-a-row">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/warning.yes')</button>
                        <button type="button" class="btn btn-sm btn-green ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-delete') }}"></use></svg></button>
                    </div>
                    <div class="btn-group small-btn-group">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/warning.no')</button>
                        <button type="button" class="btn btn-sm btn-red ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-delete') }}"></use></svg></button>
                    </div>
                </div>
                <!--              <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-6">

                                </div>
                                <div class="col-sm-6">

                                </div>
                              </div>-->
            </div>
        </div>
    </div>
</div>
<!-- End Modal:Warning -->