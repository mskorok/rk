<!-- Modal:Rules -->
<div class="modal fade modal-rules js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/rules.terms')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div style="margin-bottom: 10px;">
                                <strong>@lang('portal/partials/modals/rules.system_manager')</strong>@lang('portal/partials/modals/rules.system_manager_text')
                            </div>
                            <div style="margin-bottom: 10px;">
                                <strong>@lang('portal/partials/modals/rules.system')</strong>@lang('portal/partials/modals/rules.system_text')
                            </div>
                            <div style="margin-bottom: 10px;">
                                <strong>@lang('portal/partials/modals/rules.seller')</strong>@lang('portal/partials/modals/rules.seller_text')
                            </div>
                            <div style="margin-bottom: 10px;">
                                <strong>@lang('portal/partials/modals/rules.user')</strong>@lang('portal/partials/modals/rules.user_text')
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div style="margin-bottom: 10px;">
                                <strong>@lang('portal/partials/modals/rules.account_manager')</strong>@lang('portal/partials/modals/rules.account_manager_text')
                            </div>
                            <div style="margin-bottom: 10px;">
                                <strong>@lang('portal/partials/modals/rules.account')</strong>@lang('portal/partials/modals/rules.account_text')
                            </div>
                            <div style="margin-bottom: 10px;">
                                <strong>@lang('portal/partials/modals/rules.portal')</strong>@lang('portal/partials/modals/rules.portal_text')
                            </div>
                            <div style="margin-bottom: 10px;">
                                <strong>@lang('portal/partials/modals/rules.visitors')</strong>@lang('portal/partials/modals/rules.visitors_text')
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 20px;">
                        <div class="col-sm-12">
                            <ol>
                                <li><h3 class="title">@lang('portal/partials/modals/rules.conditions')</h3></li>
                                <ol class="spaced-list-items">
                                    {!! trans('portal/partials/modals/rules.conditions_text') !!}
                                </ol>
                                </li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal:Rules -->