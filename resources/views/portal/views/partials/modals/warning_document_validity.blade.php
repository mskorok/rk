<!-- Modal:Warning Document Validity -->
<div class="modal fade modal-warning-document js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/warning_document_validity.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center condensed bold" style="margin-bottom: 40px;">@lang('portal/partials/modals/warning_document_validity.text', ['doc' => $docType, 'doc_number' => $docNumber])</p>

                <div class="btn-group-centered">
                    <div class="btn-group small-btn-group in-a-row modal-yes">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/warning_document_validity.yes')</button>
                        <button type="button" class="btn btn-sm btn-green ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use></svg></button>
                    </div>
                    <div class="btn-group small-btn-group modal-no">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/warning_document_validity.no')</button>
                        <button type="button" class="btn btn-sm btn-red ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-cancel') }}"></use></svg></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Warning Document Validity -->