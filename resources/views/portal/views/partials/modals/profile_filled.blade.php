<!-- Profile Removed -->
<div class="modal fade modal-profile-filled js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/profile_filled.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <div class="success">
                    <img src="{{ asset('assets/portal/img/success.png') }}" alt="">
                    <h3>@lang('portal/partials/modals/profile_filled.text')</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Profile Removed -->