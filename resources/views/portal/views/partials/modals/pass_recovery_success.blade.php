<!-- Pass Recovery Success -->
<div class="modal fade pass-recovery-success js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/pass_recovery_success.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <div class="success">
                    <img src="{{ asset('assets/portal/img/success.png') }}" alt="">
                    <p>@lang('portal/partials/modals/pass_recovery_success.sent')</p>
                </div>
            </div>
        </div>
    </div>
</div>