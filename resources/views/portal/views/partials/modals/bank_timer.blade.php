<!-- Modal:Timeout -->
<div class="modal fade modal-bank-timer js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                {{--<span>@lang('portal/partials/modals/timer.add_your_photo')</span>--}}
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center bold">
                    @lang('portal/partials/modals/bank_timer.text1') <span id="time_to_logout_bank"></span> @lang('portal/partials/modals/bank_timer.text2')
                </p>
            </div>
        </div>
    </div>
</div>
<!-- End Modal:Timeout -->