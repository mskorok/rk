<!-- Modal:Card Code -->
<div class="modal fade modal-card-nr-help js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <span>@lang('portal/partials/modals/reg_ecard_help.title')</span>
        <span class="close" data-dismiss="modal"><img src="img/close.png" alt=""></span>
      </div>
      <div class="modal-body">
        <div class="success">
          <img src="{{ asset('assets/portal/img/card-nr-tutor.png') }}" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Card Code -->