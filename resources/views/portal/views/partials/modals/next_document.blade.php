<!-- Modal:Next Document -->
<div class="modal fade modal-next-document js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/next_document.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center condensed bold" style="margin-bottom: 40px;">@lang('portal/partials/modals/next_document.text', ['doc' => $docType, 'number' => $docNumber])</p>

                <div class="btn-group-centered">
                    <div class="btn-group small-btn-group in-a-row modal-yes">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/next_document.add_new')</button>
                        <button type="button" class="btn btn-sm btn-siel ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-document') }}"></use></svg></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Next Document -->