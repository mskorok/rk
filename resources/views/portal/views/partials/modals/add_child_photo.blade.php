<!-- Modal:Add Child Photo -->
@php
    if (!isset($i)) {
        $i = 1;
    }
    if (!isset($type)) {
        $type = AVATAR_TYPE_USER;
    }
    if (!isset($id)) {
        $id = 0;
    }
    if(!isset($url)) {
        $url = 'null';
    }

@endphp
<div class="modal fade modal-add-child-photo js-first-modal" id="modal-add-child-photo_{{ $i }}" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width: auto; max-width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/add_child_photo.add_child_photo')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">

                <div class="row" style="color: #064874;">
                    <div class="col-sm-6">

                        <h4>@lang('portal/partials/modals/add_child_photo.download_child_photo')</h4>
                        <div class="vignette placeholder" id="add_photo_image_{{ $i }}">
                        </div>
                        <div style="width: 300px; height: 1px; position: relative">
                            <div class="vignette button-placeholder" style="position: absolute; top: -40px;">
                                <div class="vignette zoom-out" id="zoom_out_{{ $i }}">-</div>
                                <div class="vignette zoom-in" id="zoom_in_{{ $i }}">+</div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">

                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-justified">
                            <li class="active"><a href="#boys_{{ $i }}" data-toggle="tab">@lang('portal/partials/modals/add_child_photo.boys')</a></li>
                            <li><a href="#girls_{{ $i }}" data-toggle="tab">@lang('portal/partials/modals/add_child_photo.girls')</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content" style="margin-top: 20px;">
                            <div class="tab-pane fade in active avatar-grid" id="boys_{{ $i }}">
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy101.png') }}" id="boys_1_{{ $i }}" class="first-child-image"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy02.png') }}" id="boys_2_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy03.png') }}" id="boys_3_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy04.png') }}" id="boys_4_{{ $i }}"  data-id="{{ $i }}" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy05.png') }}" id="boys_5_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy06.png') }}" id="boys_6_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy07.png') }}" id="boys_7_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy08.png') }}" id="boys_8_{{ $i }}"  data-id="{{ $i }}" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy09.png') }}" id="boys_9_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy10.png') }}" id="boys_10_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy11.png') }}" id="boys_11_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy12.png') }}" id="boys_12_{{ $i }}"  data-id="{{ $i }}" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy13.png') }}" id="boys_13_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy14.png') }}" id="boys_14_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy15.png') }}" id="boys_15_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/boys/boy16.png') }}" id="boys_16_{{ $i }}"  data-id="{{ $i }}" /></div>
                                </div>
                            </div>

                            <div class="tab-pane avatar-grid" id="girls_{{ $i }}">
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl01.png') }}" id="girls_1_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl02.png') }}" id="girls_2_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl03.png') }}" id="girls_3_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl04.png') }}" id="girls_4_{{ $i }}"  data-id="{{ $i }}" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl05.png') }}" id="girls_5_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl06.png') }}" id="girls_6_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl07.png') }}" id="girls_7_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl08.png') }}" id="girls_8_{{ $i }}"  data-id="{{ $i }}" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl09.png') }}" id="girls_9_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl10.png') }}" id="girls_10_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl11.png') }}" id="girls_11_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl12.png') }}" id="girls_12_{{ $i }}"  data-id="{{ $i }}" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl13.png') }}" id="girls_13_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl14.png') }}" id="girls_14_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl15.png') }}" id="girls_15_{{ $i }}"  data-id="{{ $i }}" /></div>
                                    <div class="col-sm-3"><img src="{{ asset('assets/portal/img/avatari/girls/girl16.png') }}" id="girls_16_{{ $i }}"  data-id="{{ $i }}" /></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="modal-footer" style="padding: 15px 25px;">
                <div style="float: left;">
                    <div class="btn-group" id="upload_photo_buttons_{{ $i }}" data-id="{{ $i }}">
                        <button type="button" class="btn btn-sm btn-blue" data-id="{{ $i }}">@lang('portal/partials/modals/add_child_photo.add_image')</button>
                        <button type="button" class="btn btn-sm btn-siel" data-id="{{ $i }}"><img src="{{ asset('assets/portal/img/exit_icon.png') }}" alt=""></button>
                    </div>
                    <div><input type="file" id="modal_upload_{{ $i }}" name="modal_upload_{{ $i }}" style="display: none;"/></div>
                </div>
                <div style="float: left; margin-left: 50px;">
                    <span id="allowed_formats_{{ $i }}">
                    @lang('portal/partials/profile/section_step_3.allowed_formats').
                    </span>
                    <br/>
                    <span id="max_image_size_{{ $i }}">
                    @lang('portal/partials/profile/section_step_3.max_image_size')
                    </span>
                </div>
                <div style="position: relative; display: none;" id="add_photo_errors_{{ $i }}">
                    <div style="float: left; margin-left: 20px; margin-right: 5px; padding: 5px;">
                        <img  src="{{ asset('assets/portal/img/danger.png') }}" style="width: 24px; height: 24px;">
                    </div>
                    <div style="float: left; text-align: left;">
                        <div style="line-height: 16px; color: red;">
                            {!! trans('portal/partials/modals/add_child_photo.error') !!}
                        </div>
                    </div>
                </div>
                <div style="float: right;">
                    <div class="btn-group" id="add_photo_buttons_{{ $i }}" data-id="{{ $i }}">
                        <button type="button" class="btn btn-sm btn-blue" data-id="{{ $i }}">@lang('portal/partials/modals/add_child_photo.save')</button>
                        <button type="button" class="btn btn-sm btn-green" data-id="{{ $i }}"><img src="{{ asset('assets/portal/img/exit_icon.png') }}" alt=""></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    (function () {
        var csrf_token = '{{ csrf_token() }}';
        var avatar_url = '{{ route('create_avatar') }}';
        var type = parseInt('{{ $type }}');
        var i = '{{ $i }}';
        var id = '{{ $id }}';
        var current_route = '{{ Route::current()->getName() }}';
        function enable_edit_button(i){
            if (current_route = 'dashboard') {
                $('#edit_card_' + i + ' .btn').removeAttr('disabled');
            }
        }
        // card or user id, type(card/user),  src from crop
        function create_avatar(id, type, src) {
            var api_token = document.getElementById('api_token');
            var url = avatar_url+'?api_token='+api_token.value;
            var form_data = new FormData();
            form_data.append('type', type);
            form_data.append('id', id);
            form_data.append('src', src);
            form_data.append('_token', csrf_token);
            var response;
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        try {
                            response = JSON.parse(this.response);
                            if (response.success) {
                                console.log('success ajax add photo', i, response);
                                create_avatar_handler(response, i);
                                enable_edit_button(i);
                                $('.modal-add-child-photo').modal('hide');
                            } else if (response.error) {
                                console.log('error ajax add photo', response.error);
                            }
                        } catch (e) {
                            console.log('error', e.message);
                        }
                    } else {
                        console.log('error response', this.response);
                    }
                }
            };
            xhr.open('POST', url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send(form_data);
        }
        function create_avatar_handler(response, i) {
            var selectors = getSelectors();
            var inputs = getInputs();
            if (selectors) {
                [].forEach.call(selectors, function (selector) {
                    if (selector.hasAttribute('src')) {
                        selector.src = response.path;
                        selector.style.display = 'block';
                    }
                })
            }
            if (inputs) {
                [].forEach.call(inputs, function (input) {
                    if (input.tagName === 'INPUT') {
                        input.value = response.id;
                    }
                })
            }
        }
        function getSelectors() {
            var selectors, result = [];
            if (type === parseInt('{{ AVATAR_TYPE_USER }}')) {
                selectors = ['#navbar_avatar', '#add_new_profile_photo', '#edit_profile_photo'];
                [].forEach.call(selectors, function (selector) {
                    var el = document.querySelector(selector);
                    if (el) {
                        result.push(el);
                    }
                });
                return result;

            } else if (type === parseInt('{{ AVATAR_TYPE_CARD }}')) {
                selectors = [
                    '#new_card_photo_'+i,
                    '#edit_card_photo_'+i,
                    '#child_photo_' + i
                ];
                [].forEach.call(selectors, function (selector) {
                    var el = document.querySelector(selector);
                    if (el) {
                        result.push(el);
                    }
                });
                return result;
            } else if (type === parseInt('{{ AVATAR_TYPE_ETICKET }}')) {
                selectors = [
                    '#edit_child_1_' + i,
                    '#edit_child_2_' + i,
                    '#edit_child_3_' + i
                ];
                [].forEach.call(selectors, function (selector) {
                    var el = document.querySelector(selector);
                    if (el) {
                        result.push(el);
                    }
                });
                return result;
            }  else {
                return null;
            }
        }
        function getInputs() {
            var selectors, result = [];
            if (type === parseInt('{{ AVATAR_TYPE_USER }}')) {
                return null;

            } else if (type === parseInt('{{ AVATAR_TYPE_CARD }}')) {
                selectors = ['#child_photo_' + i];
                [].forEach.call(selectors, function (selector) {
                    var el = document.querySelector(selector);
                    if (el) {
                        result.push(el);
                    }
                });
                return result;
            } else if (type === parseInt('{{ AVATAR_TYPE_ETICKET }}')) {
                selectors = ['#edit_child_photo_' + i];
                [].forEach.call(selectors, function (selector) {
                    var el = document.querySelector(selector);
                    if (el) {
                        result.push(el);
                    }
                });
                return result;
            } else {
                return null;
            }
        }
        function removeActive() {
            for (var j=1; j < 17; j++) {
                var boy = document.getElementById('boys_' + j + '_' + i);
                if (boy && boy.classList.contains('active')) {
                    boy.classList.remove('active');
                }
                var girl = document.getElementById('girls_' + j + '_' + i);
                if (girl && girl.classList.contains('active')) {
                    girl.classList.remove('active');
                }
            }
        }
        document.addEventListener('DOMContentLoaded', function () {

            var crop = new Croppie(document.getElementById('add_photo_image_' + i), crop_opts);
//            var first_boy = document.getElementById('boys_' + 1 + '_' + i);
//            var first_url = first_boy.src;
            var outer_url = '{{ $url }}';
            if (outer_url !== 'null') {
                crop.bind({
                    url: outer_url
                });
            } else {
//                    crop.bind({
////                    url: test_url
//                        url: first_url
//                    });
            }
            $('#add_photo_buttons_' + i).click(function (e) {
                var i = $(this).attr('data-id');
                e.preventDefault();
                e.stopPropagation();
                crop.result('rawcanvas').then(function(html) {
                    html.setAttribute('id', 'canvas');
                    html.setAttribute('crossOrigin', 'anonymous');
                    try {
                        var img = html.toDataURL();
//                        console.log('img', img);
                        var src = img.split(',')[1];
                        create_avatar(id, type, src);
                    } catch (e) {
                        console.log('e', e);
                    }
                });
            });
            var up_button = document.getElementById('zoom_in_' +i);
            if (up_button) {
                up_button.addEventListener('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    crop.up();
                })
            }

            var down_button = document.getElementById('zoom_out_' + i);
            if (down_button) {
                down_button.addEventListener('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    crop.down();
                })
            }
            for (var j=1; j < 17; j++) {
                var boy = document.getElementById('boys_' + j + '_' + i);
                if (boy) {
                    boy.addEventListener('click', function () {
                        var i = $(this).attr('data-id');
                        $('#add_photo_buttons_' + i + ' .btn').removeAttr('disabled');
                        var allowed_formats = document.getElementById('allowed_formats_' + i);
                        var max_image_size = document.getElementById('max_image_size_' + i);
                        if (allowed_formats) {
                            allowed_formats.style.color = '#064874';
                        }
                        if (max_image_size) {
                            max_image_size.style.color = '#064874';
                        }
                        removeActive();
                        this.classList.add('active');
                        var url = this.src;
                        crop.bind({
                            url: url
                        })
                    });

                }
                var girl = document.getElementById('girls_' + j + '_' + i);
                if (girl) {
                    girl.addEventListener('click', function () {
                        var i = $(this).attr('data-id');
                        $('#add_photo_buttons_' + i + ' .btn').removeAttr('disabled');
                        var allowed_formats = document.getElementById('allowed_formats_' + i);
                        var max_image_size = document.getElementById('max_image_size_' + i);
                        if (allowed_formats) {
                            allowed_formats.style.color = '#064874';
                        }
                        if (max_image_size) {
                            max_image_size.style.color = '#064874';
                        }
                        removeActive();
                        this.classList.add('active');
                        var url = this.src;
                        crop.bind({
                            url: url
                        })
                    });
                }
            }

            $('.first-child-image_' + i).click();

            document.getElementById('modal_upload_' + i).onchange = function () {
                var extension = this.files[0].name;
                extension = extension.split('.').pop();
                var size = this.files[0].size;
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.addEventListener('load', function(e) {
                        var url = e.target.result;
                        crop.bind({
                            url: url
                        })
                    });
                    reader.readAsDataURL(this.files[0]);
                }
                var upload_errors = document.getElementById('add_photo_errors_' + i);
                removeActive();
                $('#add_photo_buttons_' + i + ' .btn').attr('disabled', true);
                if (upload_errors) {
                    upload_errors.style.display = 'none';
                }
                var allowed_formats = document.getElementById('allowed_formats_' + i);
                var max_image_size = document.getElementById('max_image_size_' + i);
                if (['png', 'jpg'].indexOf(extension.toLowerCase()) !== -1) {
                    if (allowed_formats) {
                        allowed_formats.style.color = '#064874';
                    }
                } else {
                    if (allowed_formats) {
                        allowed_formats.style.color = 'red';
                    }
                }
                if (parseInt(size) < 4000000) {
                    if (max_image_size) {
                        max_image_size.style.color = '#064874';
                    }
                } else {
                    if (max_image_size) {
                        max_image_size.style.color = 'red';
                    }
                }
                if (['png', 'jpg'].indexOf(extension.toLowerCase()) !== -1 && parseInt(size) < 4000000) {
                    $('#add_photo_buttons_' + i + ' .btn').removeAttr('disabled');
                }
            };

            $('#upload_photo_buttons_' + i + ' .btn').click(function () {
                var i = $(this).attr('data-id');
                $('#modal_upload_' + i).click();
            })
        });
    })();
</script>
<!-- End Add Child Photo -->