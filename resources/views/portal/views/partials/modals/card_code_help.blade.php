<!-- Modal:Card Code Help -->
<div class="modal fade modal-card-code-help js-next-modal" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="min-width: 320px;">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/card_code_help.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <div class="success">
                    <img src="{{ asset('assets/portal/img/card-code.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Card Code Help -->