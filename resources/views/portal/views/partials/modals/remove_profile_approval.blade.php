<!-- Modal:Remove Profile Approval -->
<div class="modal fade modal-remove-profile-approve js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/remove_profile_approval.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center condensed bold">
                    @lang('portal/partials/modals/remove_profile_approval.text1', ['name' => $userName, 'surname' => $userSurname])
                </p>

                <p class="text-center condensed">
                <div class="row">
                    <div class="col-sm-8" style="margin-right: 0px;">@lang('portal/partials/modals/remove_profile_approval.text2')</div><div class="col-sm-4"><span class="notification label large red">{{ $positiveBalance }}</span> &euro;</div>
                </div>
                </p>

                <p class="text-center condensed">@lang('portal/partials/modals/remove_profile_approval.text3')<a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a></p>

                <p class="text-center condensed grey script bold">@lang('portal/partials/modals/remove_profile_approval.text4')</p>
                <p class="text-center condensed grey script">@lang('portal/partials/modals/remove_profile_approval.text5', ['name' => $userName, 'surname' => $userSurname, 'sum' => $positiveBalance, 'number' => $cardNumbers, 'bank' => $userBank, 'account' => $userAccount])</p>

                <p class="text-center condensed">@lang('portal/partials/modals/remove_profile_approval.text6')</p>

            </div>
        </div>
    </div>
</div>
<!-- End Modal:Remove Profile Approval -->