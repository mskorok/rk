<!-- Modal:Remove Profile -->
<div class="modal fade modal-remove-profile js-next-modal" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/remove_profile.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center condensed bold">
                    @lang(
                    'portal/partials/modals/remove_profile.text1',
                     ['name' => $userName, 'surname' => $userSurname]
                     )
                </p>

                <p class="text-center condensed">
                <div class="row">
                    <div class="col-sm-8" style="margin-right: 0px;">@lang('portal/partials/modals/remove_profile.text2')</div>
                    <div class="col-sm-4"><span class="notification label large green">{{ $positiveBalance }}</span> &euro;</div>
                </div>
                </p>

                <p class="text-center condensed">@lang('portal/partials/modals/remove_profile.text3')</p>

                <form class="btn-group-centered" id="delete_profile_form">
                    {{ csrf_field() }}
                    <input name="remove_profile_email" type="hidden" value="{{ $email }}">
                    <div class="form-group pale">
                        <select class="select2-select remove-reason" name="remove_reason" id="remove_reason" style="width: 100% !important;">
                            <option></option>
                            <option value="1">@lang('portal/partials/modals/remove_profile.reason_1')</option>
                            <option value="2">@lang('portal/partials/modals/remove_profile.reason_2')</option>
                            <option value="3">@lang('portal/partials/modals/remove_profile.reason_3')</option>
                            <option value="4">@lang('portal/partials/modals/remove_profile.reason_4')</option>
                        </select>
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('validation.custom.error_reason')
                        </div>
                    </div>
                    <div class="form-group register" style="margin-top: 0px;">
                        <div class="input-group-icon register">
                            <input type="password" class="form-control input" style="margin-top: 0px;"
                                   id="remove_password" name="remove_password" placeholder="@lang('portal/partials/modals/remove_profile.password')">
                            <span class="group-addon noclose"></span>
                            <div class="error-message">
                                <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                @lang('validation.custom.error_password')
                            </div>
                        </div>
                    </div>

                </form>

                <div class="btn-group-centered">
                    <div class="btn-group small-btn-group in-a-row modal-yes">
                        <button type="button" class="btn btn-sm btn-blue" id="delete_user_button">
                            @lang('portal/partials/modals/remove_profile.yes')
                        </button>
                        <button type="button" class="btn btn-sm btn-green ico" >
                            <svg aria-hidden="true" class="icon icon-row-table ">
                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="btn-group small-btn-group modal-no">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/remove_profile.no')</button>
                        <button type="button" class="btn btn-sm btn-red ico">
                            <svg aria-hidden="true" class="icon icon-row-table ">
                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-cancel') }}"></use>
                            </svg>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- End Modal:Remove Profile -->