<!-- Modal:Timeout -->
<div class="modal fade modal-timer js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                {{--<span>@lang('portal/partials/modals/timer.add_your_photo')</span>--}}
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center bold">
                    @lang('portal/partials/modals/timer.text1') <span id="time_to_logout"></span> @lang('portal/partials/modals/timer.text2')</p>
                <p class="text-center">@lang('portal/partials/modals/timer.text3')</p>
                <div class="btn-group-centered">
                    <div class="btn-group small-btn-group in-a-row modal-yes" id="timer_yes">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/timer.yes')</button>
                        <button type="button" class="btn btn-sm btn-green ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-delete') }}"></use></svg></button>
                    </div>
                    <div class="btn-group small-btn-group modal-no" id="timer_no">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/timer.no')</button>
                        <button type="button" class="btn btn-sm btn-red ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-delete') }}"></use></svg></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal:Timeout -->