<!-- Modal:Cancel Invoice -->
<div class="modal fade modal-invoice-cancel js-next-modal" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/cancel_invoice.cancel')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center bold"><h4 class="modal-big-header">@lang('portal/partials/modals/cancel_invoice.cancel_invoice')</h4></p>
                <div class="row">
                    <div class="col-sm-6" style="font-weight: 600; text-align: right;">@lang('portal/partials/modals/cancel_invoice.invoice_number')</div>
                    <div class="col-sm-6" id="modal_payment_id"></div>
                </div>
                <div class="row">
                    <div class="col-sm-6" style="font-weight: 600; text-align: right;">@lang('portal/partials/modals/cancel_invoice.created')</div>
                    <div class="col-sm-6" id="modal_payment_date"></div>
                </div>
                <div class="row">
                    <div class="col-sm-6" style="font-weight: 600; text-align: right;">@lang('portal/partials/modals/cancel_invoice.card')</div>
                    <div class="col-sm-6" id="modal_payment_card_number"></div>
                </div>
                <div class="row">
                    <div class="col-sm-6" style="font-weight: 600; text-align: right;">@lang('portal/partials/modals/cancel_invoice.invoice_identifier')</div>
                    <div class="col-sm-6" id="modal_payment_code"></div>
                </div>
                <div class="row">
                    <div class="col-sm-6" style="font-weight: 600; text-align: right;">Summa:</div>
                    <div class="col-sm-6"><span id="modal_payment_sum"></span> EUR</div>
                </div>
                <div class="btn-group-centered" style="margin-top: 20px;">
                    <div class="btn-group small-btn-group in-a-row modal-yes" id="delete_payment_yes">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/cancel_invoice.yes')</button>
                        <button type="button" class="btn btn-sm btn-green ico">
                            <svg aria-hidden="true" class="icon icon-row-table ">
                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="btn-group small-btn-group modal-no" id="delete_payment_no">
                        <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/modals/cancel_invoice.no')</button>
                        <button type="button" class="btn btn-sm btn-red ico">
                            <svg aria-hidden="true" class="icon icon-row-table ">
                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-cancel') }}"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <!--              <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-6">

                                </div>
                                <div class="col-sm-6">

                                </div>
                              </div>-->
            </div>
        </div>
    </div>
</div>
<!-- End Cancel Invoice -->