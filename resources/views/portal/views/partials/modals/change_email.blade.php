<!-- Modal:Change Email -->
<div class="modal fade modal-change-email js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/change_email.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p class="text-center condensed">@lang('portal/partials/modals/change_email.text')</p>
                <form class="btn-group-centered" id="change_email_button">
                    {{ csrf_field() }}
                    <input type="hidden" name="old_email" value="{{ $email }}">
                    <div class="success-message" id="is_registered_successfully">
                        <img src="{{ asset('assets/portal/img/success.png') }}" alt="">
                        @lang('validation.custom.success_is_registered')
                    </div>
                    <div class="form-group">
                        <input type="text" name="email_change" class="input form-control" placeholder="@lang('portal/partials/modals/change_email.email')" value="" />
                    </div>
                    <div class="error-message" id="is_registered_already">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.error_is_registered')
                    </div>

                    <div class="btn-group small-btn-group in-a-row" id="change_email_submit_button">
                        <button type="button" class="btn btn-sm btn-blue" id="change_email_submit_button_1" disabled>@lang('portal/partials/modals/change_email.change')</button>
                        <button type="button" class="btn btn-sm btn-green ico" id="change_email_submit_button_2" disabled><svg aria-hidden="true" class="icon icon-row-table " id="icones_email"><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-edit') }}"></use></svg></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal:Change Email -->