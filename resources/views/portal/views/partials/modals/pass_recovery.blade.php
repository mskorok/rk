<!-- Pass Recovery -->
<div class="modal fade pass-recovery js-first-modal" id="modal_pass_recovery" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/pass_recovery.title')</span>
                <span class="close" id="recovery_form_close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p>@lang('portal/partials/modals/pass_recovery.text')</p>
                <div class="form-group" id="recovery_container">

                    <input type="email" class="form-control input email" id="pass_recovery_email" placeholder="@lang('portal/partials/modals/pass_recovery.email')" autocomplete="off">
                    <div class="error-message" id="recovery_error_message">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        <span>{!! trans('portal/partials/modals/pass_recovery.error') !!}</span>
                    </div>
                    <div class="error-message1" id="recovery_error_message_not_found" style="color: #E44732;margin-bottom: 20px; display:none;">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        <p>{!! trans('errors.data_not_found') !!}</p>
                    </div>
                    <div id="pass_recovery_success" data-toggle="modal" data-target=".pass-recovery-success"></div>
                    <div class="btn-group"  id="pass_recovery_submit">
                        <button type="submit" class="btn btn-md btn-blue">@lang('portal/partials/modals/pass_recovery.recover')</button>
                        <button type="button" class="btn btn-md btn-blue-light ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-cancel') }}"></use></svg></button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Pass Recovery -->