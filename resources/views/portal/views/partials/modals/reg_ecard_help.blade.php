<!-- Registration E-card Help -->
<div class="modal fade modal-reg-ecard-help js-next-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <span>@lang('portal/partials/modals/reg_ecard_help.title')</span>
                <span class="close" data-dismiss="modal"><img src="{{ asset('assets/portal/img/close.png') }}" alt=""></span>
            </div>
            <div class="modal-body">
                <p>@lang('portal/partials/modals/reg_ecard_help.text')</p>
            </div>
        </div>
    </div>
</div>
<!-- End Registration E-card Help -->