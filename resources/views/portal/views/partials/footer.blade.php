<!-- Section Footer -->
<footer id="footer">
    <div class="container-fluid">

        <ul>
            <li><a href="http://www.riga.lv" target="_blank"><img src="{{ asset('assets/portal/css/images/svg_icons/rigas_dome_logo.svg') }}" alt=""></a></li>
            <li><a href="http://www.rigassatiksme.lv" target="_blank"><img src="{{ asset('assets/portal/css/images/svg_icons/rigas_satisksme_logo.svg') }}" alt=""></a></li>
            <li><a href="http://www.rigaskarte.lv" target="_blank"><img src="{{ asset('assets/portal/css/images/svg_icons/rigas_karte_logo.svg') }}" alt=""></a></li>
        </ul>

    </div>
</footer><!-- End Section Footer -->