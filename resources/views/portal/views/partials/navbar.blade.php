<!-- Navbar -->
<div class="navbar navbar-top navbar-fixed-top show-cookie-bar" role="navigation">
    <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand logo" href="@if(Route::current()->getName() !== 'dashboard') {{ route('dashboard') }} @else javascript:void(0) @endif">
                <div class="image"><img src="{{ asset('assets/portal/img/skolena_e_karte.png') }}"></div><div class="heading"><img src="{{ asset('assets/portal/css/images/svg_icons/skolena_e_karte.svg') }}" alt="Skolēna E karte logo"></div>
            </a>
        </div>
        <div class="navbar-collapse collapse menu">
            <ul class="nav navbar-nav navbar-lavalamp" id="lavalamp-nav">
                <li class="active"><a class="js-scroll-to" href="@if(Route::current()->getName() !== 'landing'){{ route('landing') }}@endif#carousel-generic">@lang('portal/partials/navbar.application')</a></li>
                <li><a class="js-scroll-to" href="@if(Route::current()->getName() !== 'landing'){{ route('landing') }}@endif#section1">@lang('portal/partials/navbar.advantages')</a></li>
                <li><a class="js-scroll-to" href="@if(Route::current()->getName() !== 'landing'){{ route('landing') }}@endif#section2">@lang('portal/partials/navbar.how_to_start')</a></li>
                <li><a class="js-scroll-to" href="@if(Route::current()->getName() !== 'landing'){{ route('landing') }}@endif#section6">@lang('portal/partials/navbar.questions')</a></li>
            </ul>
        </div>
        <div class="navbar-right">

            <div class="navbar-toggle">
                <a data-toggle="dropdown" href="#">
                    <div class="menu-rotate closed active"><img src="{{ asset('assets/portal/css/images/svg_icons/top_sandwich.svg') }}"></div>
                    <div class="menu-rotate opened"><img src="{{ asset('assets/portal/css/images/svg_icons/top_sandwich_close.svg') }}"></div>
                </a>
                <div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <div class="lang">
                        <a href="/lv{{ \Request::getRequestUri() }}" @if(App::getLocale() === 'lv')class="active"@endif>lat</a>
                        <a href="/ru{{ \Request::getRequestUri() }}" @if(App::getLocale() === 'ru')class="active"@endif>rus</a>
                        <a href="/en{{ \Request::getRequestUri() }}" @if(App::getLocale() === 'en')class="active"@endif>en</a>
                    </div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a class="js-scroll-to" href="@if(Route::current()->getName() !== 'landing'){{ route('landing') }}@endif#carousel-generic">@lang('portal/partials/navbar.application')</a></li>
                        <li><a class="js-scroll-to" href="@if(Route::current()->getName() !== 'landing'){{ route('landing') }}@endif#section1">@lang('portal/partials/navbar.advantages')</a></li>
                        <li><a class="js-scroll-to" href="@if(Route::current()->getName() !== 'landing'){{ route('landing') }}@endif#section2">@lang('portal/partials/navbar.how_to_start')</a></li>
                        <li><a class="js-scroll-to" href="@if(Route::current()->getName() !== 'landing'){{ route('landing') }}@endif#section6">@lang('portal/partials/navbar.questions')</a></li>
                    </ul>
                </div>
            </div>



            @if(Auth::check())
                @php
                /** @var \App\Models\Upload $image */
                    $image = \App\Models\Upload::where('user_id', \Auth::id())->where('parent_id', 0)->orderBy('id', 'desc')->first();
                    $avatarPath = ($image) ? 'avatars/'.$image->filename : 'avatars/no_avatar.jpg';
                @endphp
                <div class="dropdown avatar logged">
                    <a data-toggle="dropdown" href="#">
                        <span class="avatar-name">{{ Auth::user()->username }}</span>
                        <span class="avatar-img">
                <img src="{{ asset($avatarPath) }}" alt="" class="circle-img" id="navbar_avatar">
              </span>
                    </a>
                    <div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <div class="form-group">
                            <div class="form-group-top">
                                <div class="item title">{{ Auth::user()->username }} &nbsp; {{ Auth::user()->surname }}</div>
                                <div class="item">
                                    <a href="{{ route('profile') }}">
                                        <div style="display: table-cell; vertical-align: middle; padding-right: 5px; font-size: 32px;">
                                            <svg aria-hidden="true" class="icon icon-dropdown-menu">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#submenu-messages') }}"></use>
                                            </svg>
                                        </div>
                                        <div style="display: table-cell; vertical-align: middle;">@lang('portal/partials/navbar.profile')</div>
                                    </a>
                                </div>
                            </div>

                            <div class="form-group-bottom">
                                <a href="{{ route('logout') }}" class="btn-group signout">
                                    <button type="button" class="btn btn-md btn-blue">@lang('portal/partials/navbar.exit')</button>
                                    <button type="button" class="btn btn-md btn-blue-light ico">
                                        <svg aria-hidden="true" class="icon icon-row-table ">
                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-enter') }}"></use>
                                        </svg>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="dropdown avatar">
                <a data-toggle="dropdown" href="#">
                    <span class="avatar-name">@lang('portal/partials/navbar.enter')</span>
                    <span class="avatar-img">
                <img src="{{ asset('assets/portal/img/avatar.png') }}" alt="" class="circle-img">
              </span>
                </a>
                <div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <div class="form-group pale">
                        <form class="form-group-top" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" class="form-control input" id="login" name="email" placeholder="@lang('portal/partials/navbar.email')">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="input-group-icon{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input type="password" class="form-control input" id="password" name="password" placeholder="@lang('portal/partials/navbar.password')">
                                <span class="group-addon noclose"></span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="btn-group signin">
                                <button type="submit" class="btn btn-md btn-blue sign-form">@lang('portal/partials/navbar.enter2')</button>
                                <button type="submit" class="btn btn-md btn-blue-light ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-enter') }}"></use></svg></button>
                            </div>
                            <a href="" data-toggle="modal" data-target=".pass-recovery">@lang('portal/partials/navbar.forget_password')</a>
                        </form>

                        <div class="form-group-bottom">
                            <div class="btn-group register">
                                <button type="button" class="btn btn-md btn-blue sign-form">@lang('portal/partials/navbar.register')</button>
                                <button type="button" class="btn btn-md btn-blue-light ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-add-user') }}"></use></svg></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif





            <div class="lang">
                @if(Route::current()->getName() !== 'registerSteps')
                <a href="/lv{{ \Request::getRequestUri() }}" @if(App::getLocale() === 'lv')class="active"@endif>lat</a>
                <a href="/ru{{ \Request::getRequestUri() }}" @if(App::getLocale() === 'ru')class="active"@endif>rus</a>
{{--                <a href="/en{{ \Request::getRequestUri() }}" @if(App::getLocale() === 'en')class="active"@endif>en</a>--}}
                @endif
            </div>
        </div>
    </div>
    @yield('profile-menu')
</div><!-- End Navbar -->
@section('custom_script')
    <script>
        var reset_url = '{{ route('ajax_reset') }}';
        var feedback_url = '{{ route('feedback') }}';
        var register_url = '{{ route('register') }}';
        var logout_url = '{{ route('logout') }}';
    </script>
@stop
