<!-- Top Slider -->
    <style>
        @media (max-width: 960px) {
            @foreach($topSliders as $i => $slider)
            #carousel-generic .item.slider-{{ $i +1 }} {
                background-image: url('/@if($slider->path_sm){{ $slider->path_sm }}@else{{ $slider->path }}@endif') !important;
            }
            @endforeach
        }
    </style>

<div id="carousel-generic" class="carousel slide" data-interval="7000" data-pause="hover">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        @foreach($topSliders as $i => $slider)
            @php
            $j = $i +1;
            $title = 'portal/partials/top_slider.slider'.$j.'_title';
            $text = 'portal/partials/top_slider.slider'.$j.'_text';
            @endphp
        <div class="item slider-{{ $i + 1 }} @if($i === 0) active @endif" style="background-image: url('/{{ $slider->path }}')">
            <!--<img src="img/slider01.jpg" alt="Skolena e kartes Portals">-->
            <div class="container">
                <div class="carousel-caption @if($i === 2) blue @endif">
                    <div class="carousel-caption-inner">
                        <h2>{!! trans($title) !!}</h2>
                        <p class="m-b-40">{!! trans($text) !!}</p>
                        @if(!Auth::check())
                            <div class="btn-group register">
                                <button type="button" class="btn btn-lg btn-blue">@lang('portal/partials/top_slider.register')</button>
                                <button type="button" class="btn btn-lg btn-siel ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-add-user') }}"></use></svg></button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-generic" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div><!-- End Top Slider -->