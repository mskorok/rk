<!-- Section 4 -->
<section id="section4">
    <div class="container">
        <h3 class="title">@lang('portal/partials/land/section_4.title')</h3>
        <div class="content">
            <p>@lang('portal/partials/land/section_4.content')</p>
            <div class="form-group clearfix">
                <form class="subscribe" id="friends_form">
                    {{ csrf_field() }}
                    <div class="col-xs-8">
                        <input type="text" class="form-control email" id="friends_email" name="email" placeholder="@lang('portal/partials/land/section_4.friends_email')" autocomplete="off">
                    </div>
                    <div class="col-xs-4">
                        <button type="button" class="btn btn-siel js-subscribe" id="friends_submit">@lang('portal/partials/land/section_4.send')</button>
                    </div>
                    <div class="clear"></div>
                    <div class="success-message" id="friends_success">
                        <img src="{{ asset('assets/portal/img/success.png') }}" alt="">
                        @lang('portal/partials/land/section_4.sent')
                    </div>
                    <div class="error-message"  id="friends_error">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('portal/partials/land/section_4.error')
                    </div>
                </form>
            </div>
            {{--<div class="social">--}}
                {{--<a href="" class="ok"><img src="{{ asset('assets/portal/css/images/svg_icons/social_odnoklasniki.svg') }}" alt=""></a>--}}
                {{--<a href="" class="fb"><img src="{{ asset('assets/portal/css/images/svg_icons/social_facebook.svg') }}" alt=""></a>--}}
                {{--<a href="" class="dd"><img src="{{ asset('assets/portal/css/images/svg_icons/social_draugiem.svg') }}" alt=""></a>--}}
                {{--<a href="" class="tw"><img src="{{ asset('assets/portal/css/images/svg_icons/social_twitter.svg') }}" alt=""></a>--}}
            {{--</div>--}}


        </div>
    </div>
</section><!-- End Section 4 -->