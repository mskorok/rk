<!-- Section 5 -->
<section id="section5">
    <div class="container">
        <div class="row">
            <h3 class="title">@lang('portal/partials/land/section_5.title')</h3>
            <div class="content">
                <div class="col-sm-4">
                    <div class="item">
                        <img src="{{ asset('assets/portal/img/article01.jpg') }}" alt="article01">
                        <div class="desc">
                            <h4>@lang('portal/partials/land/section_5.subtitle1')</h4>
                            <p>@lang('portal/partials/land/section_5.text1')</p>
                            <ol>
                                <li>@lang('portal/partials/land/section_5.bus')</li>
                                <li>@lang('portal/partials/land/section_5.tram')</li>
                                <li>@lang('portal/partials/land/section_5.trolleybus')</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <img src="{{ asset('assets/portal/img/article02.jpg') }}" alt="article02">
                        <div class="desc">
                            <h4>@lang('portal/partials/land/section_5.subtitle2')</h4>
                            <p>@lang('portal/partials/land/section_5.text2')</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <img src="{{ asset('assets/portal/img/article03.jpg') }}" alt="article03">
                        <div class="desc">
                            <h4>@lang('portal/partials/land/section_5.subtitle3')</h4>
                            <p>@lang('portal/partials/land/section_5.text3')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End Section 5 -->