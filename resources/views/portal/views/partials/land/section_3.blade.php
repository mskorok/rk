<!-- Section 3 -->
<section class="bg-color" id="section3">
    <div class="container">
        <h3 class="title">@lang('portal/partials/land/section_3.title')</h3>
        <div class="content">
            <div class="owl-carousel js-owl-carousel-review">

                <div class="item">
                    <div class="review">
                        <p>
                            <span class="blockq first"></span>
                            @lang('portal/partials/land/section_3.text1')
                            <span class="blockq last"></span>
                        </p>
                    </div>
                    <div class="review-author">
                        <div class="review-author-img">
                            <img src="{{ asset('assets/portal/img/avatari/mans/man08.png') }}" alt="">
                        </div>
                        <div class="review-author-name">
                            Mārcis Silāns
                            <span>@lang('portal/partials/land/section_3.father')</span>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="review">
                        <p>
                            <span class="blockq first"></span>
                            @lang('portal/partials/land/section_3.text2')
                            <span class="blockq last"></span>
                        </p>
                    </div>
                    <div class="review-author">
                        <div class="review-author-img">
                            <img src="{{ asset('assets/portal/img/avatari/womans/woman13.png') }}" alt="">
                        </div>
                        <div class="review-author-name">
                            Ieva Straupmāne
                            <span>@lang('portal/partials/land/section_3.mother')</span>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="review">
                        <p>
                            <span class="blockq first"></span>
                            @lang('portal/partials/land/section_3.text3')
                            <span class="blockq last"></span>
                        </p>
                    </div>
                    <div class="review-author">
                        <div class="review-author-img">
                            <img src="{{ asset('assets/portal/img/avatari/womans/woman03.png') }}" alt="">
                        </div>
                        <div class="review-author-name">
                            Aleksandra Lipmane
                            <span>@lang('portal/partials/land/section_3.mother')</span>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="review">
                        <p>
                            <span class="blockq first"></span>
                            @lang('portal/partials/land/section_3.text4')
                            <span class="blockq last"></span>
                        </p>
                    </div>
                    <div class="review-author">
                        <div class="review-author-img">
                            <img src="{{ asset('assets/portal/img/avatari/womans/woman15.png') }}" alt="">
                        </div>
                        <div class="review-author-name">
                            Aija Vītola
                            <span>@lang('portal/partials/land/section_3.grandmother')</span>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="review">
                        <p>
                            <span class="blockq first"></span>
                            @lang('portal/partials/land/section_3.text5')
                            <span class="blockq last"></span>
                        </p>
                    </div>
                    <div class="review-author">
                        <div class="review-author-img">
                            <img src="{{ asset('assets/portal/img/avatari/mans/man08.png') }}" alt="">
                        </div>
                        <div class="review-author-name">
                            Mārcis Silāns
                            <span>@lang('portal/partials/land/section_3.grandfather')</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
<!-- End Section 3 -->