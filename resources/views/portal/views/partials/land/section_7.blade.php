<!-- Section 7 -->
<section id="section7">
    <div class="container">
        <h3 class="title">@lang('portal/partials/land/section_7.title')</h3>
        <div class="content">
            <div class="owl-carousel js-owl-carousel-partner">
                @foreach($banners as $banner)
                <div class="item">
                    @if($banner->type === 'image')
                        <a href="{{ $banner->link }}" target="_blank">
                            <img src="{{ env('APP_URL') }}/{{ $banner->path }}" alt="">
                        </a>
                    @else
                        {!! $banner->code !!}
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section><!-- End Section 7 -->