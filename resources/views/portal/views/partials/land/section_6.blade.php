<!-- Section 6 -->
<section class="bg-color" id="section6">
    <div class="container">

        <div class="row">
            <h3 class="title">@lang('portal/partials/land/section_6.title')</h3>
            <div class="content">
                <div class="row">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#registration" data-toggle="tab">@lang('portal/partials/land/section_6.register')</a></li>
                        <li><a href="#usage" data-toggle="tab">@lang('portal/partials/land/section_6.use')</a></li>
                        <li><a href="#security" data-toggle="tab">@lang('portal/partials/land/section_6.security')</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="registration">

                            <div class="col-sm-6">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading js-panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" href="#collapse1">
                                                    <span>@lang('portal/partials/land/section_6.subtitle_1_1')</span>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @lang('portal/partials/land/section_6.text_1_1')
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading js-panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" href="#collapse2">
                                                    <span>@lang('portal/partials/land/section_6.subtitle_1_2')</span>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @lang('portal/partials/land/section_6.text_1_2')
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading js-panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" href="#collapse3">
                                                    <span>@lang('portal/partials/land/section_6.subtitle_1_3')</span>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse3" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @lang('portal/partials/land/section_6.text_1_3')
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="tab-pane" id="usage">
                            <div class="col-sm-6">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading js-panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" href="#collapse7">
                                                    <span>@lang('portal/partials/land/section_6.subtitle_2_1')</span>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse7" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @lang('portal/partials/land/section_6.text_2_1')
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading js-panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" href="#collapse10">
                                                    <span>@lang('portal/partials/land/section_6.subtitle_2_2')</span>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse10" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @lang('portal/partials/land/section_6.text_2_2')
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane" id="security">
                            <div class="col-sm-6">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading js-panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" href="#collapse13">
                                                    <span>@lang('portal/partials/land/section_6.subtitle_3_1')</span>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse13" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @lang('portal/partials/land/section_6.text_3_1')
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="panel-group">
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section><!-- End Section 6 -->