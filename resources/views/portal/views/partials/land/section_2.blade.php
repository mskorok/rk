<!-- Section 2 -->
<section id="section2">
    <div class="container">

        <div class="row">
            <h3 class="title">@lang('portal/partials/land/section_2.title')</h3>
            <div class="content">
                <div class="col-md-3 col-xs-6 one">
                    <div class="wizzard wizzard1">
                        <img src="{{ asset('assets/portal/img/wizzard01.jpg') }}" alt="">
                    </div>
                    <div class="visible-xs visible-sm">
                        <div class="col-md-12 wizzard-mark wizzard-mark1">
                            <div class="wizzard-mark-inner"></div>
                            <span>1</span>@lang('portal/partials/land/section_2.registration')
                        </div>
                        <div class="col-md-12 p-r-15">
                            <h4>{!! trans('portal/partials/land/section_2.register') !!}</h4>
                            <p>@lang('portal/partials/land/section_2.text1')</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-xs-6 two">
                    <div class="wizzard wizzard2">
                        <img src="{{ asset('assets/portal/img/wizzard02.jpg') }}" alt="">
                    </div>
                    <div class="visible-xs visible-sm">
                        <div class="col-md-3 wizzard-mark wizzard-mark2">
                            <div class="wizzard-mark-inner"></div>
                            <span>2</span>@lang('portal/partials/land/section_2.add')
                        </div>
                        <div class="col-md-3 p-l-15">
                            <h4>{!! trans('portal/partials/land/section_2.subtitle2') !!}</h4>
                            <p>@lang('portal/partials/land/section_2.text2')</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6 tree">
                    <div class="wizzard wizzard3">
                        <img src="{{ asset('assets/portal/img/wizzard03.jpg') }}" alt="">
                    </div>
                    <div class="visible-xs visible-sm">
                        <div class="col-md-3 wizzard-mark wizzard-mark3">
                            <div class="wizzard-mark-inner"></div>
                            <span>3</span>@lang('portal/partials/land/section_2.transfer')
                        </div>
                        <div class="col-md-3 p-r-15">
                            <h4>{!! trans('portal/partials/land/section_2.subtitle3') !!}</h4>
                            <p>@lang('portal/partials/land/section_2.text3')</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-xs-6 four">
                    <div class="wizzard wizzard4">
                        <img src="{{ asset('assets/portal/img/wizzard04.jpg') }}" alt="">
                    </div>
                    <div class="visible-xs visible-sm">
                        <div class="col-md-3 wizzard-mark wizzard-mark4">
                            <div class="wizzard-mark-inner"></div>
                            <span>4</span>@lang('portal/partials/land/section_2.use')
                        </div>
                        <div class="col-md-3 p-l-15">
                            <h4>{!! trans('portal/partials/land/section_2.subtitle4') !!}</h4>
                            <p>@lang('portal/partials/land/section_2.text4')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid relative hidden-sm hidden-xs">
        <div class="row">
            <div class="wizzard-mark-cover">
                <div class="wizzard-mark-cover-inner"></div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-3 wizzard-mark wizzard-mark1">
                        <div class="wizzard-mark-inner"></div>
                        <span>1</span>@lang('portal/partials/land/section_2.registration')
                    </div>
                    <div class="col-md-3 wizzard-mark wizzard-mark2">
                        <div class="wizzard-mark-inner"></div>
                        <span>2</span>@lang('portal/partials/land/section_2.add')
                    </div>
                    <div class="col-md-3 wizzard-mark wizzard-mark3">
                        <div class="wizzard-mark-inner"></div>
                        <span>3</span>@lang('portal/partials/land/section_2.transfer')
                    </div>
                    <div class="col-md-3 wizzard-mark wizzard-mark4">
                        <span>4</span>@lang('portal/partials/land/section_2.use')
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h4>{!! trans('portal/partials/land/section_2.register') !!}</h4>
                        <p>@lang('portal/partials/land/section_2.text1')</p>
                    </div>
                    <div class="col-md-3">
                        <h4>{!! trans('portal/partials/land/section_2.subtitle2') !!}</h4>
                        <p>@lang('portal/partials/land/section_2.text2')</p>
                    </div>
                    <div class="col-md-3">
                        <h4>{!! trans('portal/partials/land/section_2.subtitle3') !!}</h4>
                        <p>@lang('portal/partials/land/section_2.text3')</p>
                    </div>
                    <div class="col-md-3">
                        <h4>{!! trans('portal/partials/land/section_2.subtitle4') !!}</h4>
                        <p>@lang('portal/partials/land/section_2.text4')</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section><!-- End Section 2 -->