<!-- Section Kart -->
<div class="kart-mobile">
    <p>@lang('portal/partials/land/section_card.text1')</p>
    <p>@lang('portal/partials/land/section_card.text2')</p>
</div>
<section id="kart"><a href="http://www.eriga.lv" target="_blank">
        <div class="container-fluid relative kart-main">
            <div class="container kart-inner">
                <div class="first">@lang('portal/partials/land/section_card.text1')</div>
                <div class="kart-img"><img src="{{ asset('assets/portal/img/karte.png') }}" alt="Karte"></div>
                <div class="last">@lang('portal/partials/land/section_card.text2')</div>
            </div>
        </div></a>
</section>

<div class="clear"></div>


<!-- End Section Kart -->