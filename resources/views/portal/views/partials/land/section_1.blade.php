<!-- Section 1 -->
<section class="bg-color" id="section1">
    <div class="container">

        <div class="row">
            <h3 class="title">@lang('portal/partials/land/section_1.title')</h3>
            <div class="content">
                <div class="row">
                    <div class="items-inner col-md-3 pull-left p-r-0">
                        <div class="col-xs-6 col-md-12 item m-b-20  p-r-0">
                            <h4><img class="icon icon2" src="{{ asset('assets/portal/css/images/svg_icons/help1.svg') }}" alt="">@lang('portal/partials/land/section_1.subtitle1')</h4>
                            <p>@lang('portal/partials/land/section_1.text1')</p>
                        </div>
                        <div class="col-xs-6 col-md-12 item  p-r-0">
                            <h4><img class="icon icon2" src="{{ asset('assets/portal/css/images/svg_icons/help3.svg') }}" alt="">@lang('portal/partials/land/section_1.subtitle2')</h4>
                            <p>@lang('portal/partials/land/section_1.text2')</p>
                        </div>
                    </div>
                    <div class="items-inner col-md-3 pull-right p-l-0">
                        <div class="col-xs-6 col-md-12 item m-b-0 p-l-0">
                            <h4><img class="icon icon3" src="{{ asset('assets/portal/css/images/svg_icons/help2.svg') }}" alt="">@lang('portal/partials/land/section_1.subtitle3')</h4>
                            <p>@lang('portal/partials/land/section_1.text3')</p>
                        </div>
                        <div class="col-xs-6 col-md-12 item p-l-0">
                            <h4><img class="icon icon4" src="{{ asset('assets/portal/css/images/svg_icons/help4.svg') }}" alt="">@lang('portal/partials/land/section_1.subtitle4')</h4>
                            <p>@lang('portal/partials/land/section_1.text4')</p>
                        </div>
                    </div>
                    <div class="items-inner col-md-6 p-l-0 p-r-0">
                        <img src="{{ asset('assets/portal/img/mac.png') }}" alt="fons ka portals var palidzet">
                    </div>
                </div>
            </div>
        </div>

    </div>
</section><!-- End Section 1 -->