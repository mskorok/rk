<!-- Section Profile 3 -->
<section id="profile-invoices" class="profile-section" style="display: none;">
    <div class="container">
        <h3 class="title">@lang('portal/partials/profile/section_3.payments')</h3>
    </div>
    <div class="container">
        <div class="row stack">
            <div class="col-sm-12">

                <div class="row stack" style="padding: 0; padding-left: 15px; padding-right: 15px;">
                    <form method="post" action="{{ route('ajax_payments') }}" id="payments_form">
                        {{ csrf_field() }}
                        <div class="col-md-6 col-xs-12" style="padding: 0;">

                            <!-- <div style="float: left;"> -->
                            <div style="float: left; margin-right: 10px;">
                                <select name="per_page" class="select2-select per-page" style="width: 65px">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                            <div style="float: left; margin-right: 10px;">
                                <select name="invoice_type" class="select2-select invoice-type" style="width: 165px">
                                    <option></option>
                                    <option value="0">@lang('portal/partials/profile/section_3.all')</option>
                                    <option value="1">@lang('portal/partials/profile/section_3.approved')</option>
                                    <option value="2">@lang('portal/partials/profile/section_3.wait_for_approve')</option>
                                </select>
                            </div>

                            <div style="float: left; margin-right: 10px;">
                                <select name="invoice_ecard" class="select2-select invoice-ecard" style="width: 145px">
                                    <option></option>
                                    <option value="0">@lang('portal/partials/profile/section_3.all')</option>
                                    @foreach($eTickets as $ticket)
                                    <option value="{{ $ticket->nr }}">{{ $ticket->nr }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- </div> -->

                        </div>
                        <div class="col-md-3 col-xs-6" style="padding: 0;">

                            <div style="float: left; margin-right: 10px;" class="form-group">
                                <input type="text" name="invoice_daterange" class="input form-control"
                                       value="05/11/2017 - 06/30/2017" style="width: 194px;"/>
                                <span class="group-addon noclose svg"
                                      style="position: absolute !important; top: 0 !important; left: 167px !important; right: inherit;">
                                    <div style="font-size: 18px;">
                                        <svg aria-hidden="true" class="icon icon-header-row-arrow">
                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-calendar') }}"></use>
                                        </svg>
                                    </div>
                                </span>
                            </div>

                        </div>
                        <div class="col-md-3 col-xs-6" style="padding: 0;">

                            <div style="float: right;">
                                <div class="btn-group small-btn-group pale form-group" id="payments_buttons">
                                    <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/profile/section_2.select')</button>
                                    <button type="button" class="btn btn-sm btn-siel ico">
                                        <svg aria-hidden="true" class="icon icon-row-table ">
                                        <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-filter') }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="payments_container">
                @include('portal.views.partials.profile.table_payments', compact('payments'))
            </div>
        </div>
    </div>
</section>