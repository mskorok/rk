<table id="table_invoices" class="display compact" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th style="text-transform: uppercase">@lang('portal/partials/profile/section_3.code')</th>
        <th style="text-transform: uppercase">@lang('portal/partials/profile/section_3.date')</th>
        <th style="text-transform: uppercase">@lang('portal/partials/profile/section_3.card')</th>
        <th style="text-transform: uppercase">@lang('portal/partials/profile/section_3.sum')&nbsp; &euro;</th>
        <th style="text-transform: uppercase">@lang('portal/partials/profile/section_3.status')</th>
        <th style="text-transform: uppercase">@lang('portal/partials/profile/section_3.document')</th>
        <th style="text-transform: uppercase">@lang('portal/partials/profile/section_3.operations')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($payments as $payment)
        <tr id="payment_{{ $payment->id }}">
            <td id="payment_code_{{ $payment->id }}">{{ $payment->code }}</td>
            <td id="payment_date_{{ $payment->id }}">{{ $payment->date->format('Y-m-d H:i:s') }}</td>
            <td id="payment_card_number_{{ $payment->id }}">{{ $payment->card_number }}</td>
            <td id="payment_sum_{{ $payment->id }}">{{ $payment->sum }}</td>
            <td id="payment_status_{{ $payment->id }}">{{ $payment->status }}</td>
            <td>
                <a href="{{ $payment->document }}">
                    <div style="font-size: 20px; float: left;">
                        <svg aria-hidden="true" class="icon icon-row-table red">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-pdf') }}"></use>
                        </svg>
                    </div>
                    <strong>PDF</strong>
                </a>
            </td>
            <td>
                @if($payment->isDeletable)
                <a href="javascript:void 0" onclick="return false;" class="delete-payment" data-id="{{ $payment->id }}">
                    <div style="font-size: 14px; float: left; margin-right: 5px;">
                        <svg aria-hidden="true" class="icon icon-row-table red">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                        </svg>
                    </div>
                    <span class="cancel">@lang('portal/partials/profile/section_3.cancel')</span>
                </a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
