<!-- Section Profile 2 -->
<section id="profile-notifications" class="profile-section" style="display: none;">
    <div class="container">
        <h3 class="title">@lang('portal/partials/profile/section_2.notifications')</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="row stack" style="padding: 0; padding-left: 15px; padding-right: 15px;">
                    <form id="notifications_form" method="post" action="{{ route('ajax_notifications') }}">
                        {{ csrf_field() }}
                        <div class="col-md-5 col-xs-12" style="padding: 0;">
                            <div style="float: left;">
                                <div style="float: left; margin-right: 10px;">
                                    <select name="per_page" class="select2-select per-page" style="width: 65px">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                                <div style="float: left; margin-right: 10px;">
                                    <select name="notification_type" class="select2-select notification-type"
                                            style="width: 165px">
                                        <option></option>
                                        <option value="0">@lang('portal/partials/profile/section_2.all')</option>
                                        <option value="1">@lang('portal/partials/profile/section_2.finished')</option>
                                        <option value="2">@lang('portal/partials/profile/section_2.waiting')</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-7 col-xs-12 col-md-12" style="padding: 0;">

                            <div style="float: left; margin-right: 10px;" class="form-group">
                                <span id="notification_daterange_container">
                                    <input type="text" name="notification_daterange"
                                           class="input form-control"
                                           id="notification_daterange_input"
                                           value="{{ $weekBefore->format('m-d-Y') }} - {{ $startWeek->format('m-d-Y') }}"/>
                                </span>
                                <span class="group-addon noclose svg"
                                      style="position: absolute !important; top: 0 !important; left: 167px !important; right: inherit;"><div
                                            style="font-size: 18px;"><svg aria-hidden="true"
                                                                          class="icon icon-header-row-arrow"><use
                                                    xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-calendar') }}"></use></svg></div></span>
                            </div>
                            <div style="float: right;">
                                <div class="btn-group small-btn-group pale form-group" id="notifications_buttons">
                                    <button type="button" class="btn btn-sm btn-blue">@lang('portal/partials/profile/section_2.select')</button>
                                    <button type="button" class="btn btn-sm btn-siel ico">
                                        <svg aria-hidden="true" class="icon icon-row-table ">
                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-filter') }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="notifications_container">
                @include('portal.views.partials.profile.table_notifications', compact('notifications'))
            </div>
        </div>
    </div>
</section>