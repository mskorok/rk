<table class="row-table">
    <thead>
    <th></th>
    <th>@lang('portal/partials/profile/section_1.monday')</th>
    <th>@lang('portal/partials/profile/section_1.tuesday')</th>
    <th>@lang('portal/partials/profile/section_1.wednesday')</th>
    <th>@lang('portal/partials/profile/section_1.thursday')</th>
    <th>@lang('portal/partials/profile/section_1.friday')</th>
    <th>@lang('portal/partials/profile/section_1.saturday')</th>
    <th>@lang('portal/partials/profile/section_1.sunday')</th>
    <th>@lang('portal/partials/profile/section_1.total')</th>
    </thead>
    <tbody>
    <tr>
        <td class="heading-column"><span class="text">@lang('portal/partials/profile/section_1.bought') &euro;:</span><span
                    class="short" style="display: none;"><img
                        src="{{ asset('assets/portal/css/images/svg_icons/table_euro.svg') }}"
                        style="height: 20px;"></span></td>
        <td>{{ number_format($payment->monday, 2, '.', ' ') }}</td>
        <td>{{ number_format($payment->tuesday, 2, '.', ' ') }}</td>
        <td>{{ number_format($payment->wednesday, 2, '.', ' ') }}</td>
        <td>{{ number_format($payment->thursday, 2, '.', ' ') }}</td>
        <td>{{ number_format($payment->friday, 2, '.', ' ') }}</td>
        <td>0</td>
        <td>0</td>
        <td>{{ number_format($payment->sum, 2, '.', ' ') }}</td>
    </tr>
    <tr>
        <td class="heading-column"><span class="text">@lang('portal/partials/profile/section_1.free_meals'):</span><span
                    class="short" style="display: none;"><img
                        src="{{ asset('assets/portal/css/images/svg_icons/table_fork.svg') }}"
                        style="height: 20px;"></span></td>
        <td>
            <div style="font-size: 14px;"><a href="javascript:void(0)" onclick='return false;'>
                    @if($payment->monday_free)
                        <svg aria-hidden="true" class="icon icon-row-table green">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-ok') }}"></use>
                        </svg>
                    @else
                        <svg aria-hidden="true" class="icon icon-row-table red">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                        </svg>
                    @endif
                </a></div>
        </td>
        <td>
            <div style="font-size: 14px;"><a href="javascript:void(0)" onclick='return false;'>
                    @if($payment->tuesday_free)
                        <svg aria-hidden="true" class="icon icon-row-table green">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-ok') }}"></use>
                        </svg>
                    @else
                        <svg aria-hidden="true" class="icon icon-row-table red">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                        </svg>
                    @endif
                </a></div>
        </td>
        <td>
            <div style="font-size: 14px"><a href="javascript:void(0)" onclick='return false;'>
                    @if($payment->wednesday_free)
                        <svg aria-hidden="true" class="icon icon-row-table green">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-ok') }}"></use>
                        </svg>
                    @else
                        <svg aria-hidden="true" class="icon icon-row-table red">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                        </svg>
                    @endif
                </a></div>
        </td>
        <td>
            <div style="font-size: 14px"><a href="javascript:void(0)" onclick='return false;'>
                    @if($payment->thursday_free)
                        <svg aria-hidden="true" class="icon icon-row-table green">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-ok') }}"></use>
                        </svg>
                    @else
                        <svg aria-hidden="true" class="icon icon-row-table red">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                        </svg>
                    @endif
                </a></div>
        </td>
        <td>
            <div style="font-size: 14px;"><a href="javascript:void(0)" onclick='return false;'>
                    @if($payment->friday_free)
                        <svg aria-hidden="true" class="icon icon-row-table green">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-ok') }}"></use>
                        </svg>
                    @else
                        <svg aria-hidden="true" class="icon icon-row-table red">
                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                        </svg>
                    @endif
                </a></div>
        </td>
        <td>
            <div style="font-size: 14px;"><a href="javascript:void(0)" onclick='return false;'>
                    <svg aria-hidden="true" class="icon icon-row-table red">
                        <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                    </svg>
                </a></div>
        </td>
        <td>
            <div style="font-size: 14px;"><a href="javascript:void(0)" onclick='return false;'>
                    <svg aria-hidden="true" class="icon icon-row-table red">
                        <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                    </svg>
                </a></div>
        </td>
        <td>{{ $payment->sum_free }}</td>
    </tr>
    </tbody>
</table>