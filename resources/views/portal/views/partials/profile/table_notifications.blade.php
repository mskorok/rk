<table id="table_notifications" class="display compact" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>@lang('portal/partials/profile/section_2.notification')</th>
        <th>@lang('portal/partials/profile/section_2.status')</th>
        <th>@lang('portal/partials/profile/section_2.type')</th>
        <th>@lang('portal/partials/profile/section_2.created')</th>
        <th>@lang('portal/partials/profile/section_2.operations')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($notifications as $notification)
        <tr>
            <td>{{ $notification->text }}</td>
            <td>{{ $notification->status }}</td>
            <td>{{ $notification->operation_type }}</td>
            <td>{{ $notification->date->format('Y-m-d H:i:s') }}</td>
            <td>{{ $notification->operation }}</td>
        </tr>
    @endforeach
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')340@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-19 18:41:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')291@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 18:45:13</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_approved')!--}}
    {{--</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_data_approved')</td>--}}
    {{--<td>2016-10-18 18:41:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')340@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')291@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:55:13</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_approved')!--}}
    {{--</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_data_approved')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')340@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')291@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:55:13</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_approved')!--}}
    {{--</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_data_approved')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')340@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')291@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:55:13</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_approved')!--}}
    {{--</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_data_approved')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')340@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')291@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:55:13</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_approved')!--}}
    {{--</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_data_approved')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')340@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')291@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:55:13</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_approved')!--}}
    {{--</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_data_approved')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')340@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.transfer')291@lang('portal/partials/profile/section_2.for_sum')20 €@lang('portal/partials/profile/section_2.approved').</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.money_transfer')</td>--}}
    {{--<td>2016-10-18 17:55:13</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_approved')!--}}
    {{--</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.finished')</td>--}}
    {{--<td>@lang('portal/partials/profile/section_2.profile_data_approved')</td>--}}
    {{--<td>2016-10-18 17:51:30</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    </tbody>
</table>