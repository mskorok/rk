<!-- Profile Main -->
<div id="profile-main">
    @include('portal.views.partials.profile.section_1')
    @include('portal.views.partials.profile.section_2')
    @include('portal.views.partials.profile.section_3')
</div>
<!-- End Profile Main -->