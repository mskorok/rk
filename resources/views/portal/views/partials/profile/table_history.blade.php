<table id="table_history_{{ $i }}" class="display compact table-history" cellspacing="0" width="100%">
    <thead>
    <tr>
        {{--<th style="text-transform: uppercase;">ID</th>--}}
        <th style="text-transform: uppercase;">@lang('portal/partials/profile/section_1.transactions')</th>
        <th style="text-transform: uppercase;">@lang('portal/partials/profile/section_1.from')</th>
        <th style="text-transform: uppercase;">@lang('portal/partials/profile/section_1.to')</th>
        <th style="text-transform: uppercase;">@lang('portal/partials/profile/section_1.sum') &euro;</th>
        <th>DATUMS</th>
    </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transaction)
        <tr>
{{--            <td>{{ $transaction->id }}</td>--}}
            <td>{{ $transaction->operation }}</td>
            <td>{{ $transaction->from }}</td>
            <td>{{ $transaction->to }}</td>
            <td>{{ $transaction->sum }}</td>
            <td>{{ $transaction->date }}</td>
        </tr>
    @endforeach
    </tbody>
</table>