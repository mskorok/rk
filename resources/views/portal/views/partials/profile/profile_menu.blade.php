<div class="profile-menu">
    <a href="@if(\Request::route()->getName() !== 'dashboard') {{ route('dashboard') }} @else javascript:void(0) @endif">
        <div class="item active" data-id="profile-ecard">
            <div><img src="{{ asset('assets/portal/css/images/svg_icons/submenu_ecard.svg') }}"></div>
            <span class="submenu-title">@lang('portal/partials/profile/profile_menu.card')</span>
        </div>
    </a>
    <a href="@if(\Request::route()->getName() !== 'dashboard') {{ route('dashboard') }} @else javascript:void(0) @endif">
        <div class="item" data-id="profile-notifications">
            <div><img src="{{ asset('assets/portal/css/images/svg_icons/submenu_notifications.svg') }}"></div>
            <span class="submenu-title">@lang('portal/partials/profile/profile_menu.notifications')</span>
            @if(count($unread) > 0)
                <span class="notification label red">{{ count($unread) }}</span>
            @endif
        </div>
    </a>
    <a href="@if(\Request::route()->getName() !== 'dashboard') {{ route('dashboard') }} @else javascript:void(0) @endif">
        <div class="item" data-id="profile-invoices">
            <div><img src="{{ asset('assets/portal/css/images/svg_icons/submenu_invoices.svg') }}"></div>
            <span class="submenu-title">@lang('portal/partials/profile/profile_menu.payments')</span>
        </div>
    </a>
</div>