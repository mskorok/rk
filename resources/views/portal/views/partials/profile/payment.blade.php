<div class="center-buttons" style="text-align: center;">
    <h5 style="color: green; font-weight: 600;">
        @lang('portal/partials/profile/section_1.payment_created_1', ['name' => $eTicket->name ])
        ({{ $eTicket->nr }}) @lang('portal/partials/profile/section_1.payment_created_2')
        <strong>{{ $payment->user_amount/100 }}</strong> € –
        @lang('portal/partials/profile/section_1.payment_created_3').</h5>
</div>
<div class="content row">
    <div class="col-sm-6 center-buttons">
        <div class="col-sm-12">
            <h5 style="color: #064874; font-weight: 600;">@lang('portal/partials/profile/section_1.payment_requisites'):</h5>
            <table style="color: #000; font-size: 12px;">
                <tr>
                    <td style="min-width: 80px;">@lang('portal/partials/profile/section_1.recipient'):</td>
                    <td>{{ $requisites->name }}</td>
                </tr>
                <tr>
                    <td>Reg.Nr.:</td>
                    <td>{{ $requisites->reg_nr }}</td>
                </tr>
                <tr>
                    <td>@lang('portal/partials/profile/section_1.recipient_address'):</td>
                    <td>{{ $requisites->jur_adrese }}</td>
                </tr>
                <tr>
                    <td>Bank:</td>
                    <td>{{ $requisites->banka }}</td>
                </tr>
                <tr>
                    <td>@lang('portal/partials/profile/section_1.recipient_code'):</td>
                    <td>{{ $requisites->bankas_kods }}</td>
                </tr>
                <tr>
                    <td>@lang('portal/partials/profile/section_1.recipient_account'):</td>
                    <td>{{ $requisites->konts }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-sm-6">
        <h5 style="color: #064874; font-weight: 600;">@lang('portal/partials/profile/section_1.show_identifier'):</h5>
        <div class="invoice-identifier">{{ $payment->payment_code }}</div>
        <h5 style="color: #064874; font-weight: 600;">@lang('portal/partials/profile/section_1.payment_order'):</h5>
        <a href="{{ route('download_payment', ['pdf' => $payment->id]) }}" class="btn-group small-btn-group pale form-group" download>
            <button type="button"  class="btn btn-sm btn-blue">@lang('portal/partials/profile/section_1.download')</button>
            <button type="button"  class="btn btn-sm btn-siel ico">
                <svg aria-hidden="true" class="icon icon-row-table ">
                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-pdf') }}"></use>
                </svg>
            </button>
        </a>
    </div>
</div>