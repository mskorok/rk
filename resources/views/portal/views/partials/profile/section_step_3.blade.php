<!-- Section Steps 3 -->
<section id="profile" class="profile-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title"
                    style="margin-bottom: 10px;">
                    @lang(
                    'portal/partials/profile/section_step_3.edit_profile',
                     ['name' => $userName, 'surname' => $userSurname]
                     )
                </h2>
                <input type="hidden" name="username" value="{{ $userName }}">
                <input type="hidden" name="surname" value="{{ $userSurname }}">
                <input type="hidden" name="pk" value="{{ $pk }}">
            </div>
        </div>
    </div>
    @if($errors->any())
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="content row" id="submit_error" style="margin-top: 40px; margin-bottom: 40px;">
                        @foreach($errors->all() as $error)
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div style="float: left; margin-left: 20px; margin-right: 5px; padding-right: 5px; align-self: flex-start;">
                                        <img src="{{ asset('assets/portal/img/danger.png') }}"
                                             style="width: 24px; height: 24px;"></div>
                                    <div style="float: left; text-align: left;">
                                        <div style="line-height: 16px; color: red;">
                                            {{ $error }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="ajax_error"   class="error" style="text-align: center !important; width: 100%;"></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="title">@lang('portal/partials/profile/section_step_3.edit_personal_data')</h3>
            </div>
            @if($editEmail)
                <div class="success-message">
                    <img src="{{ asset('assets/portal/img/success.png') }}" alt="">
                    @lang('portal/mail/change_email.address_approved')
                </div>
            @endif
        </div>
        <div class="row col-md-offset-1">
            <div class="form-group pale"
                 style="position: relative; margin-bottom: 0; min-height: 100px; float: left; margin-right: 20px;">
                <div class="form-group pale"
                     style="position: relative; margin-bottom: 0; float: left; margin-right: 20px;">
                    <div style="float: left;">
                        <div class="input-group-icon">
                            <input type="text" name="email" id="profile_email" class="input form-control @if(!$editEmail) disabled @endif"
                                   @if(!$editEmail) readonly @endif value="{{ $email }}"
                                   style="width: 240px; margin-bottom: 0px;"/>
                            <span class="group-addon noclose svg">
                                <div style="font-size: 18px !important; right: 5px; position: relative;">
                                    <svg aria-hidden="true" class="icon icon-row-table green">
                                        <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-ok') }}"></use>
                                    </svg>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                </div>
                <div class="form-group pale"
                     style="position: relative; margin-bottom: 0; float: left; margin-right: 20px;">
                    <div style="float: left;">
                        <div class="btn-group small-btn-group pale" id="btn-change-email">
                            <button type="button" class="btn btn-sm btn-blue"
                                    style="width: 110px;">@lang('portal/partials/profile/section_step_3.change')</button>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                </div>
                <div class="form-group pale" style="position: relative; margin-bottom: 0; float: left;">
                    <div style="float: left;">
                        <input type="text" class="form-control input" name="phone" id="profile_phone"
                               value="{{ $phone }}"
                               placeholder="@lang('portal/partials/profile/section_step_3.contact_phone')"
                               style="width: 160px; margin-bottom: 0px;">
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('validation.custom.error_phone')
                        </div>
                    </div>
                    <div>&nbsp;</div>
                </div>

                <div class="form-group pale"
                     style="position: relative; margin-bottom: 0; float: left; clear: both; width: 100%; margin-right: 20px;">
                    <div style="float: left; width: 100%;">
                        <input type="text" class="form-control input" name="address" id="profile_address"
                               value="{{ $address }}"
                               placeholder="@lang('portal/partials/profile/section_step_3.contact_address')"
                               style="width: 100%; margin-bottom: 0;">
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('validation.custom.error_text')
                        </div>
                    </div>
                    <div>&nbsp;</div>
                </div>

            </div>
            <div class="form-group pale" style="position: relative; margin-bottom: 0; min-height: 100px; float: left;">

                <div class="center-buttons">
                    <div class="person-block" style="margin-right: 10px;">
                        <div class="avatar btn-add-new-photo">
                            <img src="{{ asset($avatarPath) }}" id="edit_profile_photo">
                        </div>
                    </div>
                    <div style="margin-right: 20px;">
                        <div class="form-group pale" style="position: relative; margin-bottom: 0;">
                            <div class="btn-group small-btn-group pale btn-add-new-photo">
                                <button type="button"
                                        class="btn btn-sm btn-blue">@lang('portal/partials/profile/section_step_3.change_photo')</button>
                                <button type="button" class="btn btn-sm btn-siel ico">
                                    <svg aria-hidden="true" class="icon icon-row-table ">
                                        <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use>
                                    </svg>
                                </button>
                            </div>
                            <div class="btn-group-note italic">
                                @lang('portal/partials/profile/section_step_3.allowed_formats').
                                <br/>
                                @lang('portal/partials/profile/section_step_3.max_image_size')
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="title">@lang('portal/partials/profile/section_step_3.new_documents')</h3>
            </div>
        </div>
        <div class="row col-md-offset-1">
            <div class="form-group pale"
                 style="position: relative; margin-bottom: 0; min-height: 100px; float: left; margin-right: 20px;">
                <select class="select2-select profile-nationality" id="profile_citizenship" name="citizenship"
                        style="width: 180px;">
                    <option></option>
                    <option value="Latvijas pilsonis" @if((int) $citizenIndex === 1) selected @endif >
                        @lang('portal/partials/profile/section_step_3.citizen')
                    </option>
                    <option value="Latvijas nepilsonis" @if((int) $citizenIndex === 2) selected @endif >
                        @lang('portal/partials/profile/section_step_3.non_citizen')
                    </option>
                    <option value="Uzturēšanas atļauja" @if((int) $citizenIndex === 3) selected @endif >
                        @lang('portal/partials/profile/section_step_3.temporary_permission')
                    </option>
                </select>
                <div class="btn-group-note">&nbsp;</div>
                <div class="error-message">
                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                    @lang('validation.custom.error_text')
                </div>
            </div>
            <div class="form-group pale"
                 style="position: relative; margin-bottom: 0; min-height: 100px; float: left; margin-right: 20px;">
                <select class="select2-select profile-doctype" id="doc_type" name="doc_type" style="width: 180px;">
                    <option></option>
                    <option value="Pase" @if((int) $docTypeIndex === 1) selected @endif >
                        @lang('portal/partials/profile/section_step_3.passport')
                    </option>
                    <option value="eID" @if((int) $docTypeIndex === 2) selected @endif >
                        @lang('portal/partials/profile/section_step_3.id_card')
                    </option>
                    <option value="Cits documents" @if((int) $docTypeIndex === 3) selected @endif >
                        @lang('portal/partials/profile/section_step_3.other_person_document')
                    </option>
                </select>
                <div class="btn-group-note">&nbsp;</div>
                <div class="error-message">
                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                    @lang('validation.custom.error_text')
                </div>
            </div>
            <div class="form-group pale"
                 style="position: relative; margin-bottom: 0; min-height: 100px; float: left; margin-right: 20px;">
                <div style="float: left;">
                    <input type="text" class="form-control input" name="doc_number" id="doc_number"
                           value="{{ $docNumber }}"
                           placeholder="@lang('portal/partials/profile/section_step_3.doc_number')"
                           style="width: 200px; margin-bottom: 0;">
                    <div class="error-message">
                        <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                        @lang('validation.custom.error_text')
                    </div>
                </div>
                <div>&nbsp;</div>
            </div>
            <div class="form-group pale"
                 style="position: relative; margin-bottom: 0; min-height: 100px; float: left; margin-right: 20px;">
                <div style="float: left;">
                    <div class="input-group-icon">
                        <input type="text" name="document_date" id="document_date" class="input form-control"
                               value="{{ $documentDate }}"
                               style="width: 130px; margin-bottom: 0px;"/>
                        <span class="group-addon noclose svg"><div style="font-size: 18px;"><svg aria-hidden="true"
                                                                                                 class="icon icon-header-row-arrow"><use
                                            xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-calendar') }}"></use></svg></div></span>
                        <div class="error-message">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('validation.custom.error_text')
                        </div>
                        <div class="error-message1" style="display: none">
                            <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                            @lang('validation.custom.document_expired')
                        </div>
                    </div>
                </div>
                <div>&nbsp;</div>
            </div>
            <div class="form-group pale" style="position: relative; margin-bottom: 0; min-height: 100px; float: left;">
                <div style="float: left">
                    <div class="form-group pale"
                         style="position: relative; /*padding-top: -0.15em;*/ margin-bottom: 0;">
                        <div class="btn-group small-btn-group pale" id="btn-add-new-document">
                            <button type="button"
                                    class="btn btn-sm btn-blue">@lang('portal/partials/profile/section_step_3.new_document_copy')</button>
                            <button type="button" class="btn btn-sm btn-siel ico">
                                <svg aria-hidden="true" class="icon icon-row-table ">
                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use>
                                </svg>
                            </button>
                        </div>
                        <div><input type="file" id="doc_copy" name="doc_copy" style="display: none;"/></div>
                        <div class="btn-group-note">
                            <span id="doc_name"></span>
                            <span id="not_attached" style="color: red;float: right; padding-right: 40px;display: none">@lang('validation.required') &nbsp; *</span>
                            <br/>
                            <span id="allowed_formats">
                                @lang('portal/partials/profile/section_step_3.allowed_formats').
                            </span>
                            <br/>
                            <span id="max_image_size">
                                @lang('portal/partials/profile/section_step_3.max_image_size')
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container register">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="title"
                    style="margin-top: 10px;">@lang('portal/partials/profile/section_step_3.change_password')</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="row col-md-offset-3">
                    <div class="form-group pale"
                         style="position: relative; margin-bottom: 0; font-size: 12px; min-height: 100px; float: left; margin-right: 20px;">
                        <div style="float: left;">
                            <div class="input-group-icon">
                                <input type="password" class="form-control input" name="password_current"
                                       id="password_current"
                                       placeholder="@lang('portal/partials/profile/section_step_3.current_password')"
                                       style="width: 200px; margin-bottom: 0;" autocomplete="off">
                                <span class="group-addon noclose"></span>
                                <div class="error-message">
                                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                    @lang('validation.custom.error_password')
                                </div>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                    </div>
                    <div class="form-group pale"
                         style="position: relative; margin-bottom: 0; font-size: 12px; min-height: 100px; float: left; margin-right: 20px;">
                        <div style="float: left;">
                            <div class="input-group-icon">
                                <input type="password" class="form-control input" name="password_new" id="password_new"
                                       placeholder="@lang('portal/partials/profile/section_step_3.new_password')"
                                       style="width: 200px; margin-bottom: 0;" autocomplete="off">
                                <span class="group-addon noclose"></span>
                                <div class="error-message">
                                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                    @lang('validation.custom.error_password')
                                </div>
                            </div>
                        </div>
                        <div class="btn-group-note italic">@lang('portal/partials/profile/section_step_3.password_terms')</div>
                    </div>
                    <div class="form-group pale"
                         style="position: relative; margin-bottom: 0; font-size: 12px; min-height: 100px; float: left;">
                        <div style="float: left;">
                            <div class="input-group-icon">
                                <input type="password" class="form-control input" name="password_new_confirm"
                                       id="password_new_confirm"
                                       placeholder="@lang('portal/partials/profile/section_step_3.new_password_confirmation')"
                                       style="width: 200px; margin-bottom: 0;" autocomplete="off">
                                <span class="group-addon noclose"></span>
                                <div class="error-message">
                                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                    @lang('validation.custom.error_password')
                                </div>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container footer">
        <div class="row">
            <div class="col-sm-12">
                <div class="center-buttons">
                    <div class="btn-group" id="edit_profile_button">
                        <button type="submit"  class="btn btn-md btn-blue" disabled>
                            @lang('portal/partials/profile/section_step_3.save')
                        </button>
                        <button type="submit" class="btn btn-md btn-green ico" disabled>
                            <svg aria-hidden="true" class="icon icon-row-table " id="icones">
                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content row top" style="margin-top: 30px;">
        <div class="col-sm-12" style="text-align: center;">
            <a href="javascript:void 0" id="btn-remove-profile"
               style="text-decoration: underline;">@lang('portal/partials/profile/section_step_3.delete_profile')</a>
        </div>
    </div>

</section>