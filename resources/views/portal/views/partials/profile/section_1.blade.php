@include('portal.views.partials.modals.card_nr_help')

<!-- Section Profile 1 -->
<section id="profile-ecard" class="profile-section">
    <div class="container">
        <h3 class="title">@lang('portal/partials/profile/section_1.title')</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="title-info">@lang('portal/partials/profile/section_1.last_account_replenishment'): <strong>{{ $lastRefill }}</strong> <span>|</span>
                    @lang('portal/partials/profile/section_1.next_account_replenishment'): <strong>{{ $nextRefill }}</strong></h4>
            </div>
        </div>
    </div>
    <div class="ecard-table">
        <div class="row-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 head-card">
                        @lang('portal/partials/profile/section_1.card')
                    </div>
                    <div class="col-md-1 center head-hide">
                        @lang('portal/partials/profile/section_1.balance')
                    </div>
                    <div class="col-md-1 center head-hide">
                        @lang('portal/partials/profile/section_1.replenishment')
                    </div>
                    <div class="col-md-1 center head-hide">
                        @lang('portal/partials/profile/section_1.history')
                    </div>
                    <div class="col-md-1 center head-hide">
                        @lang('portal/partials/profile/section_1.block')
                    </div>
                    <div class="col-md-1 center head-hide">
                        @lang('portal/partials/profile/section_1.delete')
                    </div>
                    <div class="col-md-4 center head-week" style="padding-left: 0; padding-right: 0">
                        <div style="font-size: 32px; float: left;"><a href="javascript:void 0" onclick="return false;" id="previous_week">
                                <svg aria-hidden="true" class="icon icon-header-row-arrow">
                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#arrow-left') }}"></use>
                                </svg>
                            </a></div>
                        <div class="week">
                            <span id="current_week">{{ $currentWeek }}</span>. @lang('portal/partials/profile/section_1.week'): <span><span id="start_week">{{ $startWeek->format('d.m.Y') }}</span> - <span id="end_week">{{ $endWeek->format('d.m.Y') }}</span></span>
                        </div>
                        <form method="post" action="{{ route('week_expenses') }}" id="week_expenses_form">
                            {{ csrf_field() }}
                            <input type="hidden" value="0" name="week_offset" id="week_offset">
                        </form>
                        <div style="font-size: 32px; float: right;"><a href="javascript:void 0" onclick="return false;" id="next_week">
                                <svg aria-hidden="true" class="icon icon-header-row-arrow">
                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#arrow-right') }}"></use>
                                </svg>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
        @foreach($eTickets as $i => $eTicket)
            @php
                $bgClass = ($i & 1) ?  'row-odd' : 'row-even';
                $disabled = $eTicket->eticket_status === 2;
                $image = \App\Models\Image::where('parent_id', 0)->where('id', $eTicket->thumb_id)->first();
                /** @var \App\Models\Image $image */
                $src = $image ? $image->path.$image->filename : 'assets/img/no_avatar.jpg';
                $srcJs = $image ? $image->path.$image->filename : '';
                $i++;
            @endphp
            <div>
                <div class="{{ $bgClass }}">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 person-block">
                                <div class="row cell-avatar">
                                    <div class="col-md-5 avatar-image" style="padding: 0;">
                                        @if((int) $eTicket->eticket_status === 2)
                                            <div class="avatar action-person disabled">
                                                <a href="javascript:void 0"   onclick='return false;' data-id="{{ $i }}" data-details="person">
                                                    <img src="{{ asset($src) }}" id="edit_child_1_{{ $i }}">
                                                    <div class="avatar-hover">
                                                        <div class="avatar-hover-img">
                                                            <div style="font-size: 48px;">
                                                                <svg aria-hidden="true" class="icon icon-hover-white">
                                                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-lock') }}"></use>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @else
                                            <div class="avatar action-person">
                                                <a href="javascript:void 0"   onclick='return false;' data-id="{{ $i }}" data-details="person">
                                                    <img src="{{ asset($src) }}" id="edit_child_2_{{ $i }}">
                                                    <div class="avatar-hover">
                                                        <div class="avatar-hover-img">
                                                            <div style="font-size: 48px;">
                                                                <svg aria-hidden="true" class="icon icon-hover-white">
                                                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-pencil') }}"></use>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-7 avatar-name">
                                        <div class="person-content action-person hidden-xxs1 row-action simple">
                                            <a href="javascript:void 0"   onclick="return false;" data-id="{{ $i }}" data-details="person">
                                                <h4 id="child_name_{{ $i }}">{{ ucwords($eTicket->name) }}</h4>
                                                <span>{{ $eTicket->nr }}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if((int) $eTicket->eticket_status === 2)
                                <div class="col-md-1 cell-remainder">
                                    <div class="status label orange">
                                        <div class="remainder">
                                        <span>
                                            <div style="font-size: 20px;">
                                                <svg aria-hidden="true"  class="icon icon-hover-white">
                                                    <use  xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-lock') }}"></use>
                                                </svg>
                                            </div>
                                        </span> &euro;
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-1 cell-remainder">
                                    <div class="status label @if($eTicket->balance > 3*$averageCheck[$eTicket->id]) green @else red @endif ">
                                        <div class="remainder"><span>{{ number_format($eTicket->balance/100, 2, '.', ' ') }}</span> &euro;</div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-8">
                                <div class="row cell-options">
                                    <div class="col-md-6 options-buttons" style="padding: 0;">
                                        <div class="row" style="margin-right: 0; margin-left: 0; padding: 0;">
                                            <div class="col-md-3 center">
                                                <div class="row-action action-topup">
                                                    <a href="javascript:void(0)" data-id="{{ $i }}" data-details="topup" onclick='return false;'>
                                                        <div class="menu-row-mark">
                                                            <svg aria-hidden="true" class="icon-menu icon-menu-row">
                                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-add-plus') }}"></use>
                                                            </svg>
                                                        </div>
                                                        <svg aria-hidden="true" class="icon-menu icon-menu-row">
                                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-add') }}"></use>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3 center">
                                                <div class="row-action action-history">
                                                    <a href="javascript:void(0)" data-id="{{ $i }}" data-details="history" onclick='return false;'>
                                                        <svg aria-hidden="true" class="icon-menu icon-menu-row">
                                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-history') }}"></use>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3 center" data-id="{{ $i }}">
                                                <div class="row-action @if((int) $eTicket->eticket_status === 2) action-unblock @else action-block @endif">
                                                    <a href="javascript:void(0)" data-id="{{ $i }}" @if((int) $eTicket->eticket_status === 2) data-details="unblock" @else data-details="block" @endif  onclick='return false;'>
                                                        <svg aria-hidden="true" class="icon-menu icon-menu-row">
                                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-lock') }}"></use>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3 center">
                                                <div class="row-action action-delete">
                                                    <a href="javascript:void(0)" data-id="{{ $i }}" data-details="delete" onclick='return false;'>
                                                        <svg aria-hidden="true" class="icon-menu icon-menu-row">
                                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-delete') }}"></use>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 center options-table" id="table_container_{{ $i }}" style="padding: 0;">
                                        {!! $weekPayments[$i]->html !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-details" id="details_person_{{ $i }}">
                    <div class="container">
                        <h3 class="title">@lang('portal/partials/profile/section_1.edit_card')</h3>
                    </div>
                    <div class="container">
                        <div class="row stack">
                            <div class="col-md-12">
                                <div class="center-buttons">
                                    <form method="post" action="{{ route('edit_card') }}" id="e_card_edit_form_{{$i}}" style="padding: 0; width: 100%;">
                                        {{ csrf_field() }}
                                        <div class="row stack" style="padding: 0; width: 100%;">
                                            <div class="col-md-5 col-xs-12"
                                                 style="padding: 0; display: flex; align-items: center; height: 100px;">

                                                <div class="person-block" style="margin-right: 10px;">
                                                    <div class="avatar btn-add-new-photo">
                                                        <a href="javascript:void(0)" data-id="{{ $i }}" onclick='return false;'>
                                                            <img src="{{ asset($src) }}" id="edit_child_3_{{ $i }}">
                                                        </a>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="child_photo_{{ $i }}" id="edit_child_photo_{{ $i }}" value="{{ $eTicket->thumb_id }}"/>
                                                <input type="hidden" name="eticket_id" id="eticket_id_{{ $i }}" value="{{ $eTicket->id }}"/>
                                                <div style="margin-right: 20px;">
                                                    <div class="form-group pale"
                                                         style="position: relative; /*padding-top: -0.15em;*/ margin-bottom: 0;">
                                                        <div class="btn-group small-btn-group pale" id="btn-add-new-photo_{{ $i }}" data-id="{{ $i }}">
                                                            <button type="button" data-id="{{ $i }}" class="btn btn-sm btn-blue">@lang('portal/partials/profile/section_1.add_child_photo')
                                                            </button>
                                                            <button type="button" data-id="{{ $i }}" class="btn btn-sm btn-siel ico">
                                                                <svg aria-hidden="true" class="icon icon-row-table ">
                                                                    <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="btn-group-note">@lang('portal/partials/profile/section_1.max_image_size')</div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-7 col-xs-12"
                                                 style="padding: 0; height: 100px; display: flex; align-items: center;">

                                                <div class="row"
                                                     style="padding: 0; width: 100%; display: flex; justify-content: center; ">

                                                    <div class="col-xs-5" style="padding: 0; padding-right: 20px;">
                                                        <div class="form-group pale"
                                                             style="position: relative; /*padding-top: -0.15em;*/ margin-bottom: 0;">
                                                            <select
                                                                    class="select2-select limits"
                                                                    style="width: 100%;"
                                                                    name="e{{ $i }}_period"
                                                                    id="e{{ $i }}_period" data-id="{{ $i }}">
                                                                <option></option>
                                                                <option value="day" @if((int) $eTicket->limit_type === 1) selected @endif >@lang('portal/partials/register/section_step_1.daily')</option>
                                                                <option value="week" @if((int) $eTicket->limit_type === 2) selected @endif >@lang('portal/partials/register/section_step_1.weekly')</option>
                                                                <option value="month" @if((int) $eTicket->limit_type === 3) selected @endif >@lang('portal/partials/register/section_step_1.monthly')</option>
                                                            </select>
                                                            {{--<div class="error-message">--}}
                                                                {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                                     {{--alt="">--}}
                                                                {{--@lang('validation.custom.error_text')--}}
                                                            {{--</div>--}}
                                                            <div class="btn-group-note">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2" style="padding: 0;">
                                                        <div class="form-group pale" style="position: relative; /*padding-top: -0.15em;*/ margin-bottom: 0;">
                                                            <input type="number"
                                                                   class="form-control input sum"
                                                                   placeholder="0.00"
                                                                   value="{{ number_format($eTicket->limit/100, 2, '.', ' ') }}"
                                                                   name="e{{ $i }}_amount"
                                                                   id="e{{ $i }}_amount" step="0.01">
                                                            <div class="btn-group-note">&nbsp;</div>
                                                            {{--<div class="error-message">--}}
                                                                {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                                     {{--alt="">--}}
                                                                {{--@lang('validation.custom.error_text')--}}
                                                            {{--</div>--}}
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group" id="edit_card_{{ $i }}" data-id="{{ $i }}">
                                        <button type="button" id="edit_card_button_{{ $i }}" data-id="{{ $i }}" class="btn btn-md btn-blue">@lang('portal/partials/profile/section_1.approve')</button>
                                        <button type="button" id="edit_card_image_{{ $i }}" data-id="{{ $i }}" class="btn btn-md btn-green ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-details" id="details_topup_{{ $i }}">
                    <!-- STEP TOPUP 1 -->
                    <div class="container step-1 step-1-{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.card_replenishment')</h3>
                    </div>
                    <div class="container step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center-buttons">

                                    <div class="row stack" style="padding: 0; width: 100%;">
                                        <div class="col-md-3 col-xs-12 col-md-offset-2"
                                             style="padding: 0; display: flex; align-items: center;">

                                            <div style="margin-right: 40px;">
                                                <div class="person-content form-group pale">
                                                    <h4>{{ $eTicket->name }}</h4>
                                                    <span style="font-size: 12px; color: #444;">{{ $eTicket->nr }}</span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-7 col-xs-12" style="padding: 0;">
                                            <form method="post" action="{{ route('ajax_fill_card') }}" id="top_up_form_{{ $i }}">
                                                {{ csrf_field() }}
                                                <div style="margin-right: 20px;">
                                                    <div class="person-content form-group pale"
                                                         style="position: relative; margin-bottom: 0; font-size: 12px; min-height: 100px; margin-right: 20px; display: inline-block">
                                                        <div>@lang('portal/partials/profile/section_1.sum')</div>
                                                        <div style="display: table; overflow: hidden;">
                                                            <input type="text" name="top_up_sum_{{ $i }}" class="form-control input"
                                                                   placeholder="0" id="top_up_sum_{{ $i }}"
                                                                   style="width: 70px; margin-bottom: 0; float: left; margin-right: 10px;" value="0">
                                                            <span>€</span>
                                                            {{--<div class="error-message">--}}
                                                                {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                                     {{--alt="">--}}
                                                                {{--@lang('validation.custom.error_text')--}}
                                                            {{--</div>--}}
                                                        </div>
                                                        <div>(@lang('portal/partials/profile/section_1.without_cents'))</div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="eticket_id" value="{{ $eTicket->id }}"/>
                                                <div style="margin-right: 20px; display: inline-block;">
                                                    <div class="form-group pale"
                                                         style="position: relative; /*padding-top: -0.15em;*/ margin-bottom: 0; font-size: 12px; min-height: 100px;">
                                                        <div>&nbsp;</div>
                                                        <div style="display: table; overflow: hidden;">
                                                            <select name="payment_type_{{ $i }}" class="select2-select payment-type"
                                                                    style="width: 200px" id="payment_type_{{ $i }}">
                                                                <option></option>
                                                                <option value="1">@lang('portal/partials/profile/section_1.from_account')
                                                                </option>
                                                                {{--<option value="2">@lang('portal/partials/profile/section_1.from_credit_card')</option>--}}
                                                            </select>
                                                            {{--<div class="error-message">--}}
                                                                {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                                     {{--alt="">--}}
                                                                {{--@lang('validation.custom.error_text')--}}
                                                            {{--</div>--}}
                                                        </div>
                                                        <div>
                                                            @lang('portal/partials/profile/section_1.payment_description_1')
                                                            <br/>
                                                            @lang('portal/partials/profile/section_1.payment_description_2')
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container footer step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group next-step" id="topup_{{ $i }}" data-id="{{ $i }}">
                                        <button type="button"  data-id="{{ $i }}" class="btn btn-md btn-blue top-up">
                                            @lang('portal/partials/profile/section_1.approve')
                                        </button>
                                        <button type="button"  data-id="{{ $i }}" class="btn btn-md btn-green ico top-up">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- STEP TOPUP 2 -->
                    <div class="container step-2  step-2-{{ $i }}" data-id="{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.card_replenishment')</h3>
                    </div>
                    <div class="container step-2  step-2-{{ $i }}" data-id="{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center-buttons">
                                    <h4 class="title">@lang('portal/partials/profile/section_1.sum'): <span>
                                            <span id="title_amount_{{ $i }}">25</span> &euro;
                                        </span>
                                    </h4>
                                </div>
                                <input type="hidden" name="topup_amount_{{ $i }}" id="topup_amount_{{ $i }}">
                                <div class="row stack">
                                    <div class="col-md-6 col-xs-12">
                                        <div style="float: right; min-width: 300px;">
                                            <div style="margin-right: 10px; float: left;">
                                                <div class="person-content form-group pale"
                                                     style="position: relative; margin-bottom: 0; font-size: 12px; min-height: 50px;">
                                                    <div style="display: table; overflow: hidden;">
                                                        <input type="text" name="card_holder_{{ $i }}" id="card_holder_{{ $i }}" class="form-control input"
                                                               placeholder="@lang('portal/partials/profile/section_1.card_owner')"
                                                               style="width: 280px; margin-bottom: 0; float: left; margin-right: 10px;">
                                                        {{--<div class="error-message">--}}
                                                            {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                                 {{--alt="">--}}
                                                            {{--@lang('validation.custom.error_text')--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div style="float: left; min-width: 300px;">
                                            <div style="margin-right: 10px;">
                                                <div class="person-content form-group pale"
                                                     style="position: relative; margin-bottom: 0; font-size: 12px; min-height: 50px;">
                                                    <div style="display: table; overflow: hidden;">
                                                        <input type="text" name="card_{{ $i }}" id="card_{{ $i }}" class="form-control input"
                                                               placeholder="@lang('portal/partials/profile/section_1.card_number')"
                                                               style="width: 280px; margin-bottom: 0; float: left; margin-right: 10px;">
                                                        {{--<div class="error-message">--}}
                                                            {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                                 {{--alt="">--}}
                                                            {{--@lang('validation.custom.error_text')--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row stack">
                                    <div class="col-md-6 col-xs-12">
                                        <div style="float: right; min-width: 300px; min-height: 50px;">
                                            <img src="{{ asset('assets/portal/img/card_mastercard.png') }}" style="width: 60px;"> <img
                                                    src="{{ asset('assets/portal/img/card_visa.png') }}" style="width: 60px;">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div style="float: left; min-width: 280px; min-height: 50px;">
                                            <div class="form-group pale"
                                                 style="position: relative; /*padding-top: -0.15em;*/ margin-bottom: 0; font-size: 12px; min-height: 100px; float: left;">
                                                <div style="display: table; overflow: hidden;">
                                                    <div style="float: left; margin-right: 10px;">
                                                        <select name="expiry_month_{{ $i }}" class="select2-select expiry-month"
                                                                style="width: 65px">
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="5">05</option>
                                                            <option value="6">06</option>
                                                            <option value="7">07</option>
                                                            <option value="8">08</option>
                                                            <option value="9">09</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div style="float: left;">
                                                        <select name="expiry_year_{{ $i }}" class="select2-select expiry_year"
                                                                style="width: 65px">
                                                            <option value="2017">17</option>
                                                            <option value="2018">18</option>
                                                            <option value="2019">19</option>
                                                            <option value="2020">20</option>
                                                            <option value="2021">21</option>
                                                            <option value="2022">22</option>
                                                            <option value="2023">23</option>
                                                            <option value="2022">24</option>
                                                            <option value="2023">25</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div>@lang('portal/partials/profile/section_1.expiration_date')</div>
                                            </div>
                                            <div class="form-group pale"
                                                 style="position: relative; /*padding-top: -0.15em;*/ margin-bottom: 0; font-size: 12px; min-height: 100px; float: right;">
                                                <div style="float: right;">
                                                    <input type="text" name="card_code_{{ $i }}" id="card_code_{{ $i }}" class="form-control input"
                                                           placeholder="000" style="width: 70px; margin-bottom: 0;">
                                                    {{--<div class="error-message">--}}
                                                        {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                             {{--alt="">--}}
                                                        {{--@lang('validation.custom.error_text')--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div>
                                                    <a href="javascript:void(0)" style="text-decoration: underline;" class="card-code" onclick='return false;'>
                                                        @lang('portal/partials/profile/section_1.safety_code')
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="container footer step-2 step-2-{{ $i }}" data-id="{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group final-step" data-id="{{ $i }}" id="credit_card_{{ $i }}">
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-blue">@lang('portal/partials/profile/section_1.pay')</button>
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-green ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-add') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- STEP TOPUP 2B -->
                    <div class="container step-2b step-2b-{{ $i }}" >
                        <h3 class="title">@lang('portal/partials/profile/section_1.card_replenishment')</h3>
                    </div>
                    <div class="container step-2b step-2b-{{ $i }}">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8" id="payment_container_{{ $i }}">
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                    <div class="container footer step-2b step-2b-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                            </div>
                        </div>
                    </div>
                    <!-- STEP TOPUP 3 -->
                    <div class="container step-3 step-3-{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center-buttons">
                                    <div style="font-size: 126px;">
                                        <a href="javascript:void(0)" onclick='return false;'>
                                            <svg aria-hidden="true" class="icon icon-row-table green">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-ok') }}"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container step-3 step-3-{{ $i }} no-top">
                        <h3 class="title no-top">@lang('portal/partials/profile/section_1.payment_success')</h3>
                    </div>
                {{--<!--<div class="container footer step-3-{{ $i }}">--}}
                {{--<div class="row">--}}
                {{--<div class="col-sm-12">--}}
                {{--<div class="center-buttons">--}}
                {{--<div class="btn-group close-step">--}}
                {{--<button type="button" class="btn btn-md btn-blue">@lang('portal/partials/profile/section_1.complete')</button>--}}
                {{--<button type="button" class="btn btn-md btn-green ico"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="css/images/svg_icons/icons.svg#button-delete"></use></svg></button>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>	-->--}}
                <!-- STEP TOPUP 4 -->
                    <div class="container step-4 step-4-{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center-buttons">
                                    <div style="font-size: 126px;"><a href="javascript:void(0)" onclick='return false;'>
                                            <svg aria-hidden="true" class="icon icon-row-table red">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#ico-error') }}"></use>
                                            </svg>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container step-4 step-4-{{ $i }} no-top">
                        <h3 class="title no-top">@lang('portal/partials/profile/section_1.error')</h3>
                    </div>
                    <div class="container step-4 footer step-4-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group close-step-{{ $i }}">
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-blue">@lang('portal/partials/profile/section_1.retry')</button>
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-green ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-retry') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END TOPUP STEPS -->
                </div>
                <div class="row-details" id="details_history_{{ $i }}">
                    <!-- STEP HISTORY 1 -->
                    <div class="container step-1 step-1-{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.card_history')</h3>
                    </div>
                    <div class="container step-1  step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="row stack" style="padding: 0; padding-left: 15px; padding-right: 15px;">
                                    <form action="{{ route('ajax_history') }}" method="post" id="history_form_{{ $i }}">
                                        {{ csrf_field() }}
                                        <div class="col-md-5 col-xs-12" style="padding: 0;">
                                            <input type="hidden" name="eticket_id" value="{{ $eTicket->id }}" />
                                            <div style="float: left;">
                                                <div style="float: left; margin-right: 10px;">
                                                    <select name="per_page_{{ $i }}" class="select2-select per-page" style="width: 65px">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </div>
                                                <div style="float: left; margin-right: 10px;">
                                                    <select name="deal_type_{{ $i }}" class="select2-select deal-type"
                                                            style="width: 165px" id="deal_type_{{ $i }}">
                                                        <option value=""></option>
                                                        <option value="0">@lang('portal/partials/profile/section_1.all')</option>
                                                        <option value="1">@lang('portal/partials/profile/section_1.transfer')</option>
                                                        <option value="2">@lang('portal/partials/profile/section_1.purchase')</option>
                                                    </select>
                                                    {{--<div class="error-message">--}}
                                                        {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                             {{--alt="">--}}
                                                        {{--@lang('validation.custom.error_text')--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-7 col-xs-12 col-md-12" style="padding: 0;">

                                            <div style="float: left; margin-right: 10px;" class="form-group">
                                                <div class="input-group-icon">
                                                    <input type="text" name="deal_daterange_{{ $i }}" id="history_date_range_{{ $i }}" class="input form-control"
                                                           value="05/11/2017 - 06/30/2017"/>
                                                    {{--<div class="error-message">--}}
                                                        {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                             {{--alt="">--}}
                                                        {{--@lang('validation.custom.error_text')--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>

                                            <div style="float: right;">
                                                <div class="btn-group small-btn-group pale form-group" id="select_history_{{ $i }}">
                                                    <button type="button" data-id="{{ $i }}" class="btn btn-sm btn-blue">@lang('portal/partials/profile/section_1.select')</button>
                                                    <button type="button" data-id="{{ $i }}" class="btn btn-sm btn-siel ico">
                                                        <svg aria-hidden="true" class="icon icon-row-table ">
                                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-filter') }}"></use>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" id="history_container_{{ $i }}">
                                @include('portal.views.partials.profile.table_history', ['i' => $i, 'transactions'=> $transactions[$eTicket->id]])
                            </div>
                        </div>
                    </div>
                    <div class="container footer step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                            </div>
                        </div>
                    </div>
                    <!-- END HISTORY STEPS -->
                </div>
                <div class="row-details" id="details_block_{{ $i }}">
                    <!-- STEP BLOCK 1 -->
                    <div class="container step-1 step-1-{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.block_card')</h3>
                    </div>
                    <div class="container step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    @lang('portal/partials/profile/section_1.block_card_desc_1') &nbsp;
                                    <span class="accent orange">@lang('portal/partials/profile/section_1.to_block')</span>.<br/>
                                    @lang('portal/partials/profile/section_1.block_card_desc_2').<br/>
                                    @lang('portal/partials/profile/section_1.block_card_desc_3').
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container footer step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group close-step"
                                         id="block_button"
                                         data-i="{{ $i }}"
                                         data-id="{{ $eTicket->id }}"
                                         data-balance="{{ $eTicket->balance }}">
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-blue">@lang('portal/partials/profile/section_1.block')</button>
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-orange ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-lock') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- STEP BLOCK 1B -->
                    <div class="container step-1b step-1b-{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.block_card')</h3>
                    </div>
                    <div class="container step-1b step-1b-{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    @lang('portal/partials/profile/section_1.block_card_desc_1') &nbsp;
                                    <span class="accent orange">@lang('portal/partials/profile/section_1.to_block')</span>.<br/>
                                    @lang('portal/partials/profile/section_1.block_card_desc_2').<br/>
                                    @lang('portal/partials/profile/section_1.block_card_desc_3').
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-md-12">
                                <div class="center-buttons" style="text-align: center;">
                                    <h4 class="title-info" style="font-weight: 600;">@lang('portal/partials/profile/section_1.recover')
                                        <div class="status label red"><span id="blocked_amount_{{ $i }}">{{ number_format($eTicket->balance/100, 2, '.', ' ') }}</span> €</div>
                                        @lang('portal/partials/profile/section_1.recover_to'):
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel-heading js-panel-heading" style="position: absolute; right: 0;">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse_tr1_{{ $i }}">
                                                <span>@lang('portal/partials/profile/section_1.recover_to_your_account')</span>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel-heading js-panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse_tr2_{{ $i }}">
                                                <span>@lang('portal/partials/profile/section_1.recover_to_other_card')</span>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="collapse_tr1_{{ $i }}" class="panel-collapse collapse">
                                        <div class="panel-body" style="text-align: center;">
                                            @lang('portal/partials/profile/section_1.recover_to_account_letter_1') &nbsp;
                                            <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a>
                                            @lang('portal/partials/profile/section_1.recover_to_account_letter_2').
                                            <p class="grey script bold">@lang('portal/partials/profile/section_1.recover_to_account_letter_2')</p>
                                            <p class="grey script">@lang('portal/partials/profile/section_1.recover_to_account_letter_4').</p>
                                        </div>
                                    </div>
                                    <div id="collapse_tr2_{{ $i }}" class="panel-collapse collapse">
                                        <div class="panel-body" style="text-align: center;">
                                            @lang('portal/partials/profile/section_1.recover_to_card_letter_1') &nbsp;
                                            <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a> &nbsp;
                                            @lang('portal/partials/profile/section_1.recover_to_card_letter_2').
                                            <p class="grey script bold">@lang('portal/partials/profile/section_1.recover_to_card_letter_3')</p>
                                            <p class="grey script">@lang('portal/partials/profile/section_1.recover_to_card_letter_4').</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container footer step-1b step-1b-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group close-step block-card" data-id="{{ $i }}" data-eticket="{{ $eTicket->id }}" id="block_card_{{ $i }}">
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-blue">@lang('portal/partials/profile/section_1.block')</button>
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-orange ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-lock') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- STEP BLOCK 2 -->
                    <div class="container step-2 step-2-{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.unblock_card')</h3>
                    </div>
                    <div class="container step-2 step-2-{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    @lang('portal/partials/profile/section_1.unblock_desc_1') &nbsp;
                                    <span class="accent orange">@lang('portal/partials/profile/section_1.unblock_desc_2')</span>,
                                    <br/>@lang('portal/partials/profile/section_1.unblock_desc_3').
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container footer step-2">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group close-step">
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-blue unblock-card" data-eticket="{{ $eTicket->id }}" id="unblock_card_{{ $i }}">@lang('portal/partials/profile/section_1.unblock')</button>
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-orange ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-unlock') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END BLOCK STEPS -->
                </div>
                <div class="row-details" id="details_unblock_{{ $i }}">
                    <!-- STEP UNBLOCK 1 -->
                    <div class="container step-1 step-1-{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.unblock_card')</h3>
                    </div>
                    <div class="container step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    @lang('portal/partials/profile/section_1.unblock_desc_1') &nbsp;
                                    <span class="accent orange">@lang('portal/partials/profile/section_1.unblock_desc_2')</span>,
                                    <br/>@lang('portal/partials/profile/section_1.unblock_desc_3').
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container footer step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group close-step"
                                         id="unblock_button"
                                         data-i="{{ $i }}"
                                         data-id="{{ $eTicket->id }}">
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-blue unblock-card" data-eticket="{{ $eTicket->id }}" id="unblock_card_{{ $i }}">@lang('portal/partials/profile/section_1.unblock')</button>
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-orange ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#menu-unlock') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END UNBLOCK STEPS -->
                </div>
                <div class="row-details" id="details_delete_{{ $i }}">
                    <!-- STEP DELETE 1 -->
                    <div class="container step-1 step-1-{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.delete_card')</h3>
                    </div>
                    <div class="container step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    @lang('portal/partials/profile/section_1.delete_desc_1') &nbsp;
                                    <span class="accent red">@lang('portal/partials/profile/section_1.delete_desc_2')</span>.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container footer step-1 step-1-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group close-step delete-button" id="delete_button_{{ $i }}"
                                         data-i="{{ $i }}"
                                         data-id="{{ $eTicket->id }}"  data-balance="{{ $eTicket->balance }}">
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-blue">@lang('portal/partials/profile/section_1.delete')</button>
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-red ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-delete') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- STEP BLOCK 2 -->
                    <div class="container step-2 step-2-{{ $i }}">
                        <h3 class="title">@lang('portal/partials/profile/section_1.delete_card')</h3>
                    </div>
                    <div class="container step-2">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    @lang('portal/partials/profile/section_1.delete_to_account_letter_1') &nbsp;
                                    <span class="accent red">@lang('portal/partials/profile/section_1.delete')</span>
                                    &nbsp; @lang('portal/partials/profile/section_1.delete_to_account_letter_2').
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-md-12">
                                <div class="center-buttons" style="text-align: center;">
                                    <h4 class="title-info" style="font-weight: 600;">@lang('portal/partials/profile/section_1.recover')
                                        <div class="status label red"><span>{{ number_format($eTicket->balance/100, 2, '.', ' ') }}</span> €</div>
                                        @lang('portal/partials/profile/section_1.recover_to'):
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel-heading js-panel-heading" style="position: absolute; right: 0;">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse_trd1_{{ $i }}">
                                                <span>@lang('portal/partials/profile/section_1.recover_to_your_account')</span>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel-heading js-panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse_trd2_{{ $i }}">
                                                <span>@lang('portal/partials/profile/section_1.recover_to_other_card')</span>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="collapse_trd1_{{ $i }}" class="panel-collapse collapse">
                                        <div class="panel-body" style="text-align: center;">
                                            @lang('portal/partials/profile/section_1.recover_to_account_letter_1') &nbsp;
                                            <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a>
                                            @lang('portal/partials/profile/section_1.recover_to account_letter_2').
                                            <p class="grey script bold">@lang('portal/partials/profile/section_1.recover_to_account_letter_2')</p>
                                            <p class="grey script">@lang('portal/partials/profile/section_1.recover_to_account_letter_4').</p>
                                        </div>
                                    </div>
                                    <div id="collapse_trd2_{{ $i }}" class="panel-collapse collapse">
                                        <div class="panel-body" style="text-align: center;">
                                            @lang('portal/partials/profile/section_1.recover_to_card_letter_1') &nbsp;
                                            <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a> &nbsp;
                                            @lang('portal/partials/profile/section_1.recover_to_card_letter_2').
                                            <p class="grey script bold">@lang('portal/partials/profile/section_1.recover_to_card_letter_3')</p>
                                            <p class="grey script">@lang('portal/partials/profile/section_1.recover_to_card_letter_4').</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container footer step-2 step-2-{{ $i }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-buttons">
                                    <div class="btn-group close-step disabled">
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-blue">@lang('portal/partials/profile/section_1.delete')</button>
                                        <button type="button" data-id="{{ $i }}" class="btn btn-md btn-red ico">
                                            <svg aria-hidden="true" class="icon icon-row-table ">
                                                <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-delete') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END DELETE STEPS -->
                </div>
                @include(
                    'portal.views.partials.modals.add_child_photo',
                    [
                        'i' => $i,
                        'eticket' => $eTicket,
                        'id' => $eTicket->id,
                        'url' => $srcJs,
                        'type' => AVATAR_TYPE_ETICKET
                    ]
                )
            </div>
        @endforeach


        @if($errors->any())
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="content row" id="submit_error" style="margin-top: 40px; margin-bottom: 40px;">
                            @foreach($errors->all() as $error)
                                <div class="col-sm-12">
                                    <div class="center-buttons">
                                        <div style="float: left; margin-left: 20px; margin-right: 5px; padding-right: 5px; align-self: flex-start;">
                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                 style="width: 24px; height: 24px;"></div>
                                        <div style="float: left; text-align: left;">
                                            <div style="line-height: 16px; color: red;">
                                                {{ $error }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div id="ajax_add_card_error" class="error" style="text-align: center !important;width: 100%;"></div>


        <div class="row-even add-person">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 person-block">
                        <div class="avatar add">
                            <div>
                                <a href="#details_addperson_0" id="button_add_person_0">
                                    <svg aria-hidden="true" class="icon icon-add-person">
                                        <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-zoom-in') }}"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div class="person-content"><h4>@lang('portal/partials/profile/section_1.add_new_card')</h4></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-details" id="details_addperson_0">
            <div class="container" style="display: block !important;">
                <div class="content row accent">
                    <form id="ajax_add_card_form" method="post" action="{{ route('ajax_add_card') }}">
                        {{ csrf_field() }}
                        @php
                            $j = 0;
                        @endphp
                        <input type="hidden" name="i" value="{{ count($eTickets) }}" id="amt" >
                        <input type="hidden" name="child_photo_{{ $j }}" id="child_photo_{{ $j }}">
                        <div class="content row accent" id="person_{{ $j }}">
                            <div class="form-group pale">
                                <div class="col-sm-12">
                                    <div class="content row stack">
                                        <div class="col-sm-9">
                                            <div class="content row stack">
                                                <div class="col-sm-4">
                                                    <div class="box-container switch-next" data-name="n{{ $j }}_card_number">
                                                        @for($ii = 1; $ii <= 10; $ii++) 
                                                        <div>
                                                            <input type="text" class="form-control input" maxlength="1"
                                                                    name="n{{ $j }}_card_number_{{ $ii }}" data-id="{{ $ii }}" id="n{{ $j }}_card_number_{{ $ii }}">
                                                        </div>
                                                        @endfor                                                        
                                                    </div>
                                                    <div class="btn-group-note">
                                                        @lang('portal/partials/register/section_step_1.e_card_number')
                                                        (<a href="javascript:void 0"
                                                            class="e-card-help">@lang('portal/partials/register/section_step_1.how_to_find_card_number')</a>)
                                                    </div>
                                                    {{--<div class="error-message">--}}
                                                        {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                             {{--alt="">--}}
                                                        {{--@lang('validation.custom.error_text')--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="col-lg-5 col-xs-12 col-sm-8">
                                                    <div class="col-sm-8 col-xs-12" style="padding-left: 0">
                                                        <select class="select2-select limits" style="width: 100%;"
                                                                name="n{{ $j }}_period" id="n{{ $j }}_period">
                                                            <option></option>
                                                            <option value="day">@lang('portal/partials/register/section_step_1.daily')</option>
                                                            <option value="week">@lang('portal/partials/register/section_step_1.weekly')</option>
                                                            <option value="month">@lang('portal/partials/register/section_step_1.monthly')</option>
                                                        </select>
                                                        <div class="error-message">
                                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                 alt="">
                                                            @lang('validation.custom.error_text')
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-12" style="padding-right: 0">
                                                        <input type="number" class="form-control input"
                                                               name="n{{ $j }}_amount" placeholder="0.00"  step="0.01">
                                                        <div class="error-message">
                                                            <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                                 alt="">
                                                            @lang('validation.max.numeric', ['attribute' => 'amount', 'max' => 140])
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="content row stack">
                                                <div class="col-sm-4 col-xs-6">
                                                    <input type="text" class="form-control input"
                                                           name="n{{ $j }}_name"
                                                           placeholder="@lang('portal/partials/register/section_step_1.name')">
                                                    <div class="error-message">
                                                        <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                             alt="">
                                                        @lang('validation.custom.error_text')
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-xs-6">
                                                    <input type="text" class="form-control input"
                                                           name="n{{ $j }}_surname"
                                                           placeholder="@lang('portal/partials/register/section_step_1.surname')">
                                                    <div class="error-message">
                                                        <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                             alt="">
                                                        @lang('validation.custom.error_text')
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-xs-12">
                                                    <div class="box-container p-code switch-next" data-name="n{{ $j }}_pk">
                                                        @for($ii = 1; $ii <= 11; $ii++) 
                                                        <div>
                                                            <input type="text" class="form-control input" maxlength="1"
                                                                    name="n{{ $j }}_pk_{{ $ii }}" data-id="{{ $ii }}" id="n{{ $j }}_pk_{{ $ii }}">
                                                        </div>
                                                        @if($ii == 6)
                                                        <div></div>
                                                        @endif	
                                                        @endfor                                                        
                                                    </div>
                                                    <div class="btn-group-note">@lang('portal/partials/register/section_step_1.personal_identifier')</div>
                                                    {{--<div class="error-message">--}}
                                                        {{--<img src="{{ asset('assets/portal/img/danger.png') }}"--}}
                                                             {{--alt="">--}}
                                                        {{--@lang('validation.custom.error_text')--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                            <div class="content row">
                                                <div class="col-sm-4 col-xs-12">
                                                    <select class="select2-select schools" style="width: 100%;"
                                                            name="n{{ $j }}_school" id="n{{ $j }}_school">
                                                        @include('portal.views.partials.register.schools')
                                                    </select>
                                                    <div class="error-message">
                                                        <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                             alt="">
                                                        @lang('validation.custom.error_text')
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-xs-12">
                                                    <input type="text" class="form-control input"
                                                           name="n{{ $j }}_class"
                                                           placeholder="@lang('portal/partials/register/section_step_1.class')">
                                                    <div class="error-message">
                                                        <img src="{{ asset('assets/portal/img/danger.png') }}"
                                                             alt="">
                                                        @lang('validation.custom.error_text')
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-3 col-xs-12">
                                            <div class="large-padding"
                                                 style="position: relative; padding-top: 50%;">
                                                <div class="btn-group small-btn-group pale" id="btn-add-new-photo_0">
                                                    <button type="button" class="btn btn-sm btn-blue">
                                                        @lang('portal/partials/register/section_step_1.add_child_photo')
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-siel ico">
                                                        <svg aria-hidden="true" class="icon icon-row-table ">
                                                            <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use>
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="btn-group-note">
                                                    @lang('portal/partials/register/section_step_1.allowed_formats').
                                                    <br/>
                                                    @lang('portal/partials/register/section_step_1.max_image_size')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @include(
                'portal.views.partials.modals.add_child_photo',
                ['i' => $j, 'card' => 0, 'id' => 0, 'type' => AVATAR_TYPE_CARD]
            )
            <div class="container footer">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="center-buttons">
                            <div class="btn-group" id="next-step" data-id="0">
                                <button type="button" class="btn btn-md btn-blue" data-id="0">
                                    @lang('portal/partials/profile/section_1.add')
                                </button>
                                <button type="button" class="btn btn-md btn-green ico" data-id="0">
                                    <svg aria-hidden="true" class="icon icon-row-table ">
                                        <use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-ok') }}"></use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container error"></div>
        </div>

        @if(count($eTickets) === 0)
            <div class="row-even add-person">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 person-block">
                        <div class="person-content"><h4>@lang('portal/partials/profile/section_1.add_first_card')</h4></div>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>

</section>