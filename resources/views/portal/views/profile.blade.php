@extends('portal.layouts.main')
@section('title')
    @lang('portal/profile.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/profile.description')">
    <meta name="author" content="@lang('portal/profile.author')">
    <meta name="keywords" content="@lang('portal/profile.keywords')">
@stop
@section('profile-menu')
    @include('portal.views.partials.profile.profile_menu')
@stop
@section('css')
    @parent
    <!-- Libraries styles -->
    <link href="{{ asset('assets/portal/css/bootstrap-imageupload.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/select2.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/jquery.dataTables.min.css').'?'.time() }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/portal/css/jquery.dataTables.reset.css').'?'.time() }}" rel="stylesheet">

@stop
@section('content')
    <div id="register-steps">
        <form id="register_edit_form" method="post" action="{{ route('profile_edit') }}"  enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="avatar_image_id" id="avatar_image">
            @include('portal.views.partials.profile.section_step_3')
        </form>
    </div>
@stop
@section('modals')
    <!-- Modals
================================================== -->
    @include('portal.views.partials.modals.add_child_photo')
    @include('portal.views.partials.modals.card_code_help')
    @include('portal.views.partials.modals.approve_delete_card')
    @include('portal.views.partials.modals.warning_document_validity', ['docType' => $docType, 'docNumber' => $docNumber])
    @include('portal.views.partials.modals.next_document')
    @include('portal.views.partials.modals.change_email')
    @include('portal.views.partials.modals.remove_profile')
    @include('portal.views.partials.modals.remove_profile_approval')
    @include('portal.views.partials.modals.profile_removed')
    @include('portal.views.partials.modals.cancel_invoice')
    @include('portal.views.partials.modals.timer')

@stop
@section('js')
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    @parent
    <script src="{{ asset('assets/portal/js/bootstrap-imageupload.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/select2.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/jquery.dataTables.min.js').'?'.time() }}"></script>
    <script src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>    
    <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
@stop
@section('script')
    <script type="text/javascript">
        
        moment.locale('{{ App::getLocale() }}');
        var monthNames = moment.months();
        var daysOfWeek = moment.weekdaysShort(); 
        var locale = {
            format: 'YYYY-MM-DD',
            daysOfWeek: daysOfWeek,
            monthNames: monthNames,            
            firstDay: 1
        };
        
        var edit_email = false;
        var doc_expired = '{{ $docExpired }}';
        var positive_balance = '{{ $positiveBalance }}';
        var citizenship_old = '{!! html_entity_decode($citizenship) !!}';
        var doc_type_old = '{!! html_entity_decode($docType) !!}';
        var doc_number_old = '{{ $docNumber }}';
        var delete_url = '{{ route('profile_delete') }}';
        var delete_check_url = '{{ route('profile_delete_check') }}';
        var change_email_url = '{{ route('profile_change_email') }}';
        var is_disable = false;
        var submit_button_selector = '#edit_profile_button .btn';
        positive_balance = parseInt(positive_balance);
        var profile_functions = {
            delete_profile: function () {
                var form = document.getElementById('delete_profile_form') ;
                var form_data = new FormData(form);
                var api_token = document.getElementById('api_token');
                var url = delete_url+'?api_token='+api_token.value;
                var xhr = new XMLHttpRequest();
                var response;
                var errors_container = document.getElementById('ajax_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                xhr.onload = function () {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('delete success', response.success);
                                    profile_functions.delete_success()
                                } else if (response.error) {
                                    console.log('delete error', response.error);
                                    var errors_container = document.getElementById('ajax_error');
                                    var error_container;
                                    for (var k in response.error) {
                                        console.log('k', k, response.error[k][0]);
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error[k][0];
                                        errors_container.appendChild(error_container);
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            delete_success: function () {
                $('.modal-profile-removed').modal('show');
                setTimeout(function () {
                    $('.modal-profile-removed').modal('hide');
                    window.location.assign('/auth/logout');
                }, 5000);
            },
            check_for_delete: function () {
                if (this.reason_not_chosen()) {
                    this.password_not_chosen();
                    return false;
                } else {
                    this.hide_reason_errors();
                    if (this.password_not_chosen()) {
                        return false;
                    }
                    this.hide_password_errors();
                }
                var form = document.getElementById('delete_profile_form');
                var api_token = document.getElementById('api_token');
                var form_data = new FormData(form);
                var self = this;
                var url = delete_check_url+'?api_token='+api_token.value;
                var xhr = new XMLHttpRequest();
                var response;
                xhr.onload = function () {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('check delete', response.success);
                                    self.check_success();
                                } else if (response.error) {
                                    self.check_error();
                                    console.log('check delete error', response.error);
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            check_success: function () {
                if (positive_balance > 0) {
                    $('.modal-remove-profile').modal('hide');
                    $('.modal-remove-profile-approve').modal('show');
                } else {
                    $('.modal-remove-profile').modal('hide');
                    profile_functions.delete_profile();
                }
            },
            check_error: function (bad_password) {
                if (bad_password) {
                    var input = document.getElementById('remove-password');
                    if (input) {
                        custom_functions.show_error(input)
                    }
                }
            },
            reason_not_chosen: function () {
                var reason = document.getElementById('remove_reason');
                if (reason.options[reason.selectedIndex].value == '') {
                    custom_functions.show_error(reason);
                    return true;
                }
                return false;
            },
            hide_reason_errors: function () {
                var reason = document.getElementById('remove_reason');
                custom_functions.hide_error(reason);
            },
            password_not_chosen: function () {
                var pass = document.getElementById('remove_password');
                var value = pass.value;
                if (value === '' || !custom_functions.validatePassword(value)) {
                    custom_functions.show_error(pass);
                }
            },
            hide_password_errors: function () {
                var pass = document.getElementById('remove_password');
                custom_functions.hide_error(pass);
            },
            change_email: function () {
                var form = document.getElementById('change_email_button') ;
                var form_data = new FormData(form);
                var api_token = document.getElementById('api_token');
                var url = change_email_url+'?api_token='+api_token.value;
                var xhr = new XMLHttpRequest();
                var response;
                xhr.onload = function () {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('change email success', response.success);
                                    profile_functions.change_email_success();
                                } else if (response.error) {
                                    profile_functions.change_email_error();
                                    console.log('change email error', response.error);
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            change_email_success: function () {
                $('#is_registered_successfully').show();
                setTimeout(function () {
                    $('.modal-change-email').modal('hide');
                }, 5000);
            },
            change_email_error: function () {
                $('#is_registered_already').show();
            }
        };
        var form_validation = {
            doc_image: true,
            valid_selectors: [
                    'input[name=phone]',
                    'input[name=address]',
                    'select[name=citizenship]',
                    'select[name=doc_type]',
                    'input[name=doc_number]',
                    'input[name=document_date]'

            ],
            form_valid: function () {
                if (form_validation.valid()) {
                    $('#edit_profile_button .btn').removeAttr('disabled');
                } else {
                    $('#edit_profile_button .btn').attr('disabled', true)
                }
            },
            valid: function () {
                var edit_email = (edit_email) ? form_errors.email : true;
                var password = document.querySelector('input[name=password_new]');
                var password_current = document.querySelector('input[name=password_current]');
                var password_confirm = document.querySelector('input[name=password_new_confirm]');
                var citizenship_new = document.querySelector('select[name=citizenship]');
                var doc_type_new = document.querySelector('select[name=doc_type]');
                var doc_number_new = document.querySelector('input[name=doc_number]');
                var doc_copy_error_container = document.querySelector('#not_attached');
                var valid_image = typeof form_errors.doc_image === 'undefined' ? false : form_errors.doc_image;
                var is_doc_copy = (citizenship_new.value !== citizenship_old
                || doc_type_new.value !== doc_type_old
                ||  doc_number_old !== doc_number_new.value) ? valid_image : true;

                if (is_doc_copy) {
                    if (doc_copy_error_container) {
                        doc_copy_error_container.style.display = 'none';
                    }
                } else {
                    if (doc_copy_error_container) {
                        doc_copy_error_container.style.display = 'block';
                    }
                }
                var is_pass = (password.value === '' && password_confirm.value === '' && password_current.value === '');
//                console.log('is_pass', is_pass, form_errors.password_current, form_errors.password, form_errors.password_confirm);
                var check_pass = (is_pass)
                        ? true
                        : (form_errors.password_current  && form_errors.password  && form_errors.password_confirm);
                var result = (
                            this.doc_image
                            && edit_email
                            && form_errors.phone
                            && form_errors.address
                            && form_errors.citizenship
                            && form_errors.doc_type
                            && form_errors.doc_number
                            && form_errors.document_date
                            && form_errors.date_expired
                            && is_doc_copy
                         && check_pass
                        )
                    ;
//                console.log('result', result, form_errors);
                return result;
            },
            show_errors: function () {
                if (!form_errors.phone) {
                    custom_functions.show_error($('input[name=phone]')[0]);
                }
                if (!form_errors.address) {
                    custom_functions.show_error($('input[name=address]')[0]);
                }
                if (!form_errors.citizenship) {
                    custom_functions.show_error($('select[name=citizenship]')[0]);
                }
                if (!form_errors.doc_type) {
                    custom_functions.show_error($('select[name=doc_type]')[0]);
                }
                if (!form_errors.doc_number) {
                    custom_functions.show_error($('input[name=doc_number]')[0]);
                }
                if (!form_errors.document_date) {
                    custom_functions.show_error($('input[name=document_date]')[0]);
                }
                if (!form_errors.date_expired) {
                    custom_functions.show_error($('input[name=document_date]')[0], 1);
                }
                if (typeof form_errors.password_current !== 'undefined' && !form_errors.password_current) {
                    custom_functions.show_error($('input[name=password_current]')[0]);
                }
                if (typeof form_errors.password !== 'undefined' && !form_errors.password) {
                    custom_functions.show_error($('input[name=password_new]')[0]);
                }
                if (typeof form_errors.password_confirm !== 'undefined' && !form_errors.password_confirm) {
                    custom_functions.show_error($('input[name=password_new_confirm]')[0]);
                }
            },
            validate_fields: function () {
                custom_functions.passwordId = 'password_new';
                custom_functions.passwordConfirmId = 'password_new_confirm';
                document.getElementById('doc_copy').onchange = function () {
                    var name = this.files[0].name;
                    var extension = this.files[0].name;
                    extension = extension.split('.').pop();
                    var size = this.files[0].size;
                    var allowed_formats = document.getElementById('allowed_formats');
                    var max_image_size = document.getElementById('max_image_size');
                    if (['png', 'jpg'].indexOf(extension.toLowerCase()) !== -1 ) {
                        if (allowed_formats) {
                            allowed_formats.style.color = '#064874';
                        }
                        form_validation.doc_image = true;
                        form_errors.doc_image = true;
                    } else {
                        if (allowed_formats) {
                            allowed_formats.style.color = 'red';
                        }
                        form_validation.doc_image = false;
                        form_errors.doc_image = false;
                    }
                    if (parseInt(size) < 4000000) {
                        if (max_image_size) {
                            max_image_size.style.color = '#064874';
                        }
                        form_validation.doc_image = form_validation.doc_image === true;
                        form_errors.doc_image = form_validation.doc_image;
                    } else {
                        if (max_image_size) {
                            max_image_size.style.color = 'red';
                        }
                        form_validation.doc_image = false;
                        form_validation.doc_image = false;
                    }
                    if (name) {
                        var doc_name = document.getElementById('doc_name');
                        doc_name.textContent = name;
                        doc_name.style.color = 'green';
                    }
                    if (form_validation.doc_image) {
                        form_validation.form_valid();
                    } else {
                        $('#edit_profile_button .btn').attr('disabled', true)
                    }
                };
                if (edit_email) {
                    custom_functions.validateInput(
                            'input[name=email]',
                            '#edit_profile_button .btn',
                            null,
                            custom_functions.validateEmail,
                            'email'
                    );
                }
                custom_functions.validateInput(
                        'input[name=phone]',
                        '#edit_profile_button .btn',
                        null,
                        custom_functions.validatePhone,
                        'phone'
                );
                custom_functions.validateInput(
                        'input[name=address]',
                        '#edit_profile_button .btn',
                        null,
                        custom_functions.validateString,
                        'address'
                );
                custom_functions.validateSelect(
                        '#profile_citizenship',
                        '#edit_profile_button .btn',
                        'citizenship'
                );
                custom_functions.validateSelect(
                        '#doc_type',
                        '#edit_profile_button .btn',
                        'doc_type'
                );
                custom_functions.validateInput(
                        'input[name=doc_number]',
                        '#edit_profile_button .btn',
                        null,
                        custom_functions.validateString,
                        'doc_number'
                );
                custom_functions.validateInput(
                        'input[name=password_current]',
                        '#edit_profile_button .btn',
                        null,
                        custom_functions.validatePassword
                );
                custom_functions.validateInput(
                        'input[name=password_new]',
                        '#edit_profile_button .btn',
                        null,
                        custom_functions.validatePassword
                );
                custom_functions.validateInput(
                        'input[name=password_new_confirm]',
                        '#edit_profile_button .btn',
                        null,
                        custom_functions.validatePasswordConfirm
                );
                custom_functions.validateInput(
                        'input[name=remove_password]',
                        null,
                        null,
                        custom_functions.validatePassword
                );
                custom_functions.validateSelect(
                        '#remove_reason',
                        null
                );
                custom_functions.validateInput(
                        'input[name=email_change]',
                        ['#change_email_submit_button_1', '#change_email_submit_button_2'],
                        null,
                        custom_functions.validateEmail
                );
                custom_functions.validateForm(form_validation.valid_selectors, form_validation.form_valid);
                form_validation.form_valid();
            },
            validate_date: function (doc_date) {
                var parent = doc_date.parent();
                if (custom_functions.validateDate(doc_date.val())) {
                    $(parent).find('.error-message').hide();
                    doc_date.removeClass('error').addClass('success');
                    if (is_disable) {
                        custom_functions.enable_button(submit_button_selector);
                        form_errors['document_date'] = true;
                    } else {
                        form_errors['document_date'] = true;
                    }
                } else {
                    $(parent).find('.error-message').show();
                    doc_date.addClass('error').removeClass('success');
                    custom_functions.disable_button(submit_button_selector);
                    form_errors['document_date'] = false;
                }
                if (custom_functions.validateDocumentDate(doc_date.val())) {
                    $(parent).find('.error-message1').hide();
                    doc_date.removeClass('error').addClass('success');
                    if (is_disable) {
                        custom_functions.enable_button(submit_button_selector);
                        form_errors['date_expired'] = true;
                    } else {
                        form_errors['date_expired'] = true;
                    }
                } else {
                    $(parent).find('.error-message1').show();
                    doc_date.addClass('error').removeClass('success');
                    custom_functions.disable_button(submit_button_selector);
                    form_errors['date_expired'] = false;
                }
                form_validation.form_valid();
            }
        };
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $( document ).ready(function() {

            var icones = document.getElementById('icones');
            icones.addEventListener('click', function (e) {
                var parent = icones.parentNode;
                if (parent.hasAttribute('disabled')) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            });

            var icones_email = document.getElementById('icones_email');
            icones_email.addEventListener('click', function (e) {
                var parent = icones_email.parentNode;
                if (parent.hasAttribute('disabled')) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            });

            var start = moment().subtract(29, 'days');
            var end = moment();
            //Listeners
            if (doc_expired) {
                $('.modal-warning-document').modal('show');
            }

            $('#btn-change-email').click(function() {
                $('.modal-change-email').modal('show');
            });

            $('#change_email_submit_button').click(function () {
                profile_functions.change_email();
            });


            $('#btn-add-new-document').click(function() {
                $('input[id=doc_copy]').click();
            });

            /******************** MODALS ************************************/
            $('.modal-warning-document .btn-group.modal-yes').click(function() {
                $('.modal-warning-document').modal('hide');
                $('.modal-next-document').modal('show');
            });

            $('.modal-warning-document .btn-group.modal-no').click(function() {
                $('.modal-warning-document').modal('hide');
            });

            $('.modal-next-document .btn-group.modal-yes').click(function() {
                $('.modal-next-document').modal('hide');
                $('input[id=doc_copy]').click();
            });

            $('#btn-remove-profile').click(function() {
                $('.modal-remove-profile').modal('show');
            });

            $('.modal-remove-profile .btn-group.modal-yes').click(function() {
                profile_functions.check_for_delete();
            });


            $('.modal-remove-profile .btn-group.modal-no').click(function() {
                $('.modal-remove-profile').modal('hide');
            });

            $('.btn-add-new-photo .btn').click(function() {
                $('.modal-add-child-photo').modal('show');
            });

    /********************* AND MODALS ************************************************/


     /***************** ADD PERSON ******************************************/



            $('.avatar-grid .row .col-sm-3 img').click(function() {
                $('.avatar-grid .row .col-sm-3 img.active').removeClass('active');
                $(this).addClass('active');
            });

     /************** AND ADDPERSON *******************************************/

            var modal = getUrlParameter('modal');
            if (modal) {
//                console.log(modal);
                if (modal == 'modal-rules') $('.modal-rules').modal('show');
                if (modal == 'modal-warning-1') $('.modal-warning-1').modal('show');
                if (modal == 'modal-add-new-photo') $('.modal-add-new-photo').modal('show');
                if (modal == 'modal-reg-ecard-help') $('.modal-reg-ecard-help').modal('show');
                if (modal == 'modal-add-child-photo') $('.modal-add-child-photo').modal('show');
            }

            $('.select2-select.remove-reason').select2({
                minimumResultsForSearch: Infinity,
                allowClear: true,
                placeholder: "{{$trans['reason']}}"
            });

            $('.select2-select.profile-nationality').select2({
                minimumResultsForSearch: Infinity,
                allowClear: true,
                placeholder: "{{ $trans['citizenship'] }}"
            });
            $('.select2-select.profile-doctype').select2({
                minimumResultsForSearch: Infinity,
                allowClear: true,
                placeholder: "{{ $trans['document'] }}"
            });

            $('.select2-select.profile-nationality').on("select2:selecting", function(e) {
                setTimeout(function () {
                    form_validation.form_valid();
                }, 200)
            });

            $('.select2-select.profile-doctype').on("select2:selecting", function(e) {
                setTimeout(function () {
                    form_validation.form_valid();
                }, 200)
            });

            var doc_date = $('input[name="document_date"]');

            doc_date.daterangepicker(
                {
                    locale: locale,
                    singleDatePicker: true,
                    showDropdowns: true,
                    "opens": "center"
                },
                function(start, end, label) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY'));
                        //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                    setTimeout(function () {
                        form_validation.validate_date(doc_date);
                    }, 200);
                }
            );

            doc_date.attr("placeholder", "@lang('portal/profile.valid_until')");

            form_validation.validate_date(doc_date);
            form_validation.validate_fields();
//            console.log(form_errors);
        });
    </script>
@stop