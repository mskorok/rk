@extends('portal.layouts.main')
@section('title')
    @lang('portal/dashboard.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/dashboard.description')">
    <meta name="author" content="@lang('portal/dashboard.author')">
    <meta name="keywords" content="@lang('portal/dashboard.keywords')">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('profile-menu')
    @include('portal.views.partials.profile.profile_menu')
@stop
@section('css')
    @parent
    <!-- Libraries styles -->
    <link href="{{ asset('assets/portal/css/bootstrap-imageupload.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/select2.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/jquery.dataTables.min.css').'?'.time() }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/portal/css/jquery.dataTables.reset.css').'?'.time() }}" rel="stylesheet">

@stop
@section('content')
    <div id="profile-main">
        @include('portal.views.partials.profile.section_1')
        @include('portal.views.partials.profile.section_2')
        @include('portal.views.partials.profile.section_3')
    </div>
@stop
@section('modals')
    <!-- Modals
================================================== -->

    @include('portal.views.partials.modals.card_code_help')
    @include('portal.views.partials.modals.approve_delete_card')
    @include('portal.views.partials.modals.cancel_invoice')
    @include('portal.views.partials.modals.approve_block_card')
    @include('portal.views.partials.modals.reg_ecard_help')
    @include('portal.views.partials.modals.timer')
    @include('portal.views.partials.modals.limit_changed')

@stop
@section('js')
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    @parent
    <script src="{{ asset('assets/portal/js/bootstrap-imageupload.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/select2.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/jquery.dataTables.min.js').'?'.time() }}"></script>
    <script src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>    
    <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
@stop
@section('script')
    <script type="text/javascript">
        
        moment.locale('{{ App::getLocale() }}');
        var monthNames = moment.months();
        var daysOfWeek = moment.weekdaysShort(); 
        var locale = {
            format: 'YYYY-MM-DD',
            separator: " - ",
            applyLabel: "{{ $trans['applay'] }}",
            cancelLabel: "{{ $trans['cancel'] }}",
            customRangeLabel: "{{ $trans['custom'] }}",
            daysOfWeek: daysOfWeek,
            monthNames: monthNames,            
            firstDay: 1
        };
        var ranges = {
            '{{ $trans['today'] }}': [moment(), moment()],
            '{{ $trans['yesterday'] }}': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '{{ $trans['last7'] }}': [moment().subtract(6, 'days'), moment()],
            '{{ $trans['last30'] }}': [moment().subtract(29, 'days'), moment()],
            '{{ $trans['this_month'] }}': [moment().startOf('month'), moment().endOf('month')],
            '{{ $trans['prev_month'] }}': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        };
        
        var csrf = '{{ csrf_token() }}';
        var refill_ajax_url = '{{ route('ajax_fill_card') }}';
        var edit_e_card_url = '{{ route('edit_card') }}';
        var history_url = '{{ route('ajax_history') }}';
        var block_card_url = '{{ route('ajax_block_card') }}';
        var unblock_card_url = '{{ route('ajax_unblock_card') }}';
        var delete_card_url = '{{ route('ajax_delete_card') }}';
        var week_expenses_url = '{{ route('week_expenses') }}';
        var notifications_url = '{{ route('ajax_notifications') }}';
        var payments_url = '{{ route('ajax_payments') }}';
        var create_card_url = '{{ route('ajax_add_card') }}';
        var delete_payment_url = '{{ route('ajax_delete_payment') }}';
        var site_offset = 0;
        var dashboard_functions = {
            successBlocked: function () {
                var success_unblocked = document.getElementById('card_unblocked');
                var title_unblocked = document.getElementById('card_unblocked_title');
                var success = document.getElementById('card_blocked');
                var title = document.getElementById('card_blocked_title');
                var desc = document.getElementById('block_card_desc');
                var desc_unblocked = document.getElementById('unblock_card_desc');
                if (desc) {
                    desc.style.display = 'none';
                }
                if (desc_unblocked) {
                    desc_unblocked.style.display = 'none';
                }
                if (success) {
                    success.style.display = 'block';
                }
                if (success_unblocked) {
                    success_unblocked.style.display = 'none';
                }
                if (title) {
                    title.style.display = 'block';
                }
                if (title_unblocked) {
                    title_unblocked.style.display = 'none';
                }
            },
            successUnBlocked: function () {
                var success_unblocked = document.getElementById('card_unblocked');
                var title_unblocked = document.getElementById('card_unblocked_title');
                var success = document.getElementById('card_blocked');
                var title = document.getElementById('card_blocked_title');
                var desc = document.getElementById('block_card_desc');
                var desc_unblocked = document.getElementById('unblock_card_desc');
                if (desc) {
                    desc.style.display = 'none';
                }
                if (desc_unblocked) {
                    desc_unblocked.style.display = 'none';
                }
                if (success) {
                    success.style.display = 'none';
                }
                if (success_unblocked) {
                    success_unblocked.style.display = 'block';
                }
                if (title) {
                    title.style.display = 'none';
                }
                if (title_unblocked) {
                    title_unblocked.style.display = 'block';
                }
            },
            setBlock: function () {
                var success_unblocked = document.getElementById('card_unblocked');
                var title_unblocked = document.getElementById('card_unblocked_title');
                var success = document.getElementById('card_blocked');
                var title = document.getElementById('card_blocked_title');
                var desc = document.getElementById('block_card_desc');
                var desc_unblocked = document.getElementById('unblock_card_desc');
                if (desc) {
                    desc.style.display = 'block';
                }
                if (desc_unblocked) {
                    desc_unblocked.style.display = 'none';
                }
                if (success) {
                    success.style.display = 'none';
                }
                if (success_unblocked) {
                    success_unblocked.style.display = 'none';
                }
                if (title) {
                    title.style.display = 'block';
                }
                if (title_unblocked) {
                    title_unblocked.style.display = 'none';
                }
            },
            setUnblock: function () {
                var success_unblocked = document.getElementById('card_unblocked');
                var title_unblocked = document.getElementById('card_unblocked_title');
                var success = document.getElementById('card_blocked');
                var title = document.getElementById('card_blocked_title');
                var desc = document.getElementById('block_card_desc');
                var desc_unblocked = document.getElementById('unblock_card_desc');
                if (desc) {
                    desc.style.display = 'none';
                }
                if (desc_unblocked) {
                    desc_unblocked.style.display = 'block';
                }
                if (success) {
                    success.style.display = 'none';
                }
                if (success_unblocked) {
                    success_unblocked.style.display = 'none';
                }
                if (title) {
                    title.style.display = 'none';
                }
                if (title_unblocked) {
                    title_unblocked.style.display = 'block';
                }
            },
            ajax_delete_payment_listener: function () {
              var links = document.querySelectorAll('.delete-payment');
                if (links) {
                    [].forEach.call(links, function (link) {
                        link.addEventListener('click', function () {
                            var id = link.getAttribute('data-id');
                            var modal_id = document.getElementById('modal_payment_id');
                            modal_id.textContent = id;
                            var code = document.getElementById('payment_code_' + id);
                            if (code) {
                                var modal_code = document.getElementById('modal_payment_code');
                                if (modal_code) {
                                    modal_code.textContent = code.textContent;
                                }
                            }
                            var date = document.getElementById('payment_date_' + id);
                            if (date) {
                                var modal_date = document.getElementById('modal_payment_date');
                                if (modal_date) {
                                    modal_date.textContent = date.textContent;
                                }
                            }
                            var card_number = document.getElementById('payment_card_number_' + id);
                            if (card_number) {
                                var modal_card_number = document.getElementById('modal_payment_card_number');
                                modal_card_number.textContent = card_number.textContent
                            }
                            var sum = document.getElementById('payment_sum_' + id);
                            if (sum) {
                                var modal_sum = document.getElementById('modal_payment_sum');
                                modal_sum.textContent = sum.textContent
                            }

                            $('.modal-invoice-cancel').modal('show');

                        })
                    })
                }
            },
            ajax_modal_delete_payment_listener: function () {
                $('#delete_payment_yes .btn').click(function () {
                    var modal_id = document.getElementById('modal_payment_id');
                    var id = parseInt(modal_id.textContent);
                    $('.modal-invoice-cancel').modal('hide');
                    if (id > 0) {
                        dashboard_functions.ajax_delete_payment(id);
                    }
                });
            },
            ajax_delete_payment: function (id) {
                var api_token = document.getElementById('api_token');
                var url = delete_payment_url+'?api_token='+api_token.value;
                var form_data = new FormData();
                form_data.append('id', id);
                form_data.append('_token', csrf);
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var response;
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('success ajax delete payment', response.success);
                                    window.location.reload();
                                } else if (response.error) {
                                    console.log('error ajax delete payment', response.error);
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_refill_clean_listener: function (i) {
                var input = document.getElementById('top_up_sum_' + i);
                if (input) {
                    input.addEventListener('input', function () {
                        input.value = parseInt(input.value);

                    })
                }

            },
            ajax_add_card_listener: function () {
                var buttons = document.querySelectorAll('#next-step .btn');
                if (buttons) {
                    [].forEach.call(buttons, function (button) {
                        button.addEventListener('click', function () {
                            dashboard_functions.ajax_add_card();
                        });
                    })
                }
            },
            ajax_add_card: function () {
                var api_token = document.getElementById('api_token');
                var url = create_card_url+'?api_token='+api_token.value;
                var form = document.getElementById('ajax_add_card_form');
                var form_data = new FormData(form);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var error_container;
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    window.location.reload();
                                    console.log('success ajax add card', response.success);
                                    errors_container.innerHTML = '';
                                } else if (response.error) {
                                    console.log('error ajax add card', response.error);
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]) ;
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                    $('#button_add_person_0').click();
                                }
                            } catch (e) {
                                error_container = document.createElement('div');
                                error_container.textContent = e.message;
                                errors_container.appendChild(error_container);
                                console.log('error', e.message);
                                $('#button_add_person_0').click();
                            }
                        } else {
                            console.log('error response', this.response);
                            $('#button_add_person_0').click();
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_payments_listener: function () {
                var button = document.getElementById('payments_buttons');
                if (button) {
                    button.addEventListener('click', function () {
                        dashboard_functions.ajax_payments();
                    });
                }
            },
            ajax_payments: function () {
                var api_token = document.getElementById('api_token');
                var url = payments_url+'?api_token='+api_token.value;
                var form = document.getElementById('payments_form');
                var form_data = new FormData(form);
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var error_container;
                var response;
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    var container = document.getElementById('payments_container');
                                    if (container) {
                                        container.innerHTML = response.html;
                                        var per_page = parseInt(response.per_page);
                                        dashboard_functions.ajax_payments_success_handler(per_page);
                                    }
                                    console.log('success ajax payments', response.success);
                                } else if (response.error) {
                                    console.log('error ajax payments', response.error);
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_payments_success_handler: function (per_page) {
                $('#table_invoices').DataTable({
                    "info": false,
                    "searching": false,
                    "lengthChange": false,
                    "pageLength": per_page,
                    "order": [[4, "desc"]],
                    oLanguage: {
                        oPaginate: {
                            sNext: '<i class="fa fa-chevron-right" ></i>',
                            sPrevious: '<i class="fa fa-chevron-left" ></i>'
                        }
                    }
                });
            },
            ajax_notifications_listener: function () {
                var button = document.getElementById('notifications_buttons');
                if (button) {
                    button.addEventListener('click', function () {
                        dashboard_functions.ajax_notifications();
                    });
                }
            },
            ajax_notifications: function () {
                var api_token = document.getElementById('api_token');
                var url = notifications_url+'?api_token='+api_token.value;
                var form = document.getElementById('notifications_form');
                var form_data = new FormData(form);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    var container = document.getElementById('notifications_container');
                                    if (container) {
                                        container.innerHTML = response.html;
                                        var per_page = parseInt(response.per_page);
                                        dashboard_functions.ajax_notification_success_handler(per_page);
                                    }
                                    console.log('success ajax notifications', response.success);
                                } else if (response.error) {
                                    console.log('error ajax notifications', response.error);
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_notification_success_handler: function (per_page) {
                $('#table_notifications').DataTable({
                    "info": false,
                    "searching": false,
                    "lengthChange": false,
                    "pageLength": per_page,
                    "order": [[3, "desc"]],
                    oLanguage: {
                        oPaginate: {
                            sNext: '<i class="fa fa-chevron-right" ></i>',
                            sPrevious: '<i class="fa fa-chevron-left" ></i>'
                        }
                    }
                });
            },
            ajax_week_expenses_listener: function () {
                var next = document.getElementById('next_week');
                if (next) {
                    next.addEventListener('click', function () {
                       dashboard_functions.ajax_week_expenses(1);
                    });
                }
                var previous = document.getElementById('previous_week');
                if (previous) {
                    previous.addEventListener('click', function () {
                        dashboard_functions.ajax_week_expenses(-1);
                    });
                }
            },
            ajax_week_expenses: function (offset) {
                var api_token = document.getElementById('api_token');
                var url = week_expenses_url+'?api_token='+api_token.value;
                var form = document.getElementById('week_expenses_form');
                var form_data = new FormData(form);
                site_offset = site_offset + parseInt(offset);
                form_data.append('offset', site_offset);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    dashboard_functions.ajax_week_expenses_success_handler(response);
                                    console.log('success ajax week expenses', response.success);
                                } else if (response.error) {
                                    console.log('error ajax week expenses', response.error);
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_week_expenses_success_handler: function (response) {
                var i = document.getElementById('amt').value;
                var start = document.getElementById('start_week');
                var end = document.getElementById('end_week');
                var current = document.getElementById('current_week');
                if (start) {
                    start.innerHTML = response.start;
                }
                if (end) {
                    end.innerHTML = response.end;
                }
                if (current) {
                    current.innerHTML = response.current;
                }
                for (var j = 1; j < i; j++) {
                    var container = document.getElementById('table_container_' + j);
                    if (container) {
                        container.innerHTML = response.html[j];
                    }
                }
            },
            ajax_delete_card: function (i, eticket_id) {
                var api_token = document.getElementById('api_token');
                var url = delete_card_url+'?api_token='+api_token.value;
                var form_data = new FormData();
                form_data.append('i', i);
                form_data.append('_token', csrf);
                form_data.append('eticket_id', eticket_id);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('success ajax delete', response.success);
                                    window.location.reload();
                                } else if (response.error) {
                                    $('.modal-warning-delete').modal('hide');
                                    console.log('error ajax delete', response.error);
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                                $('.modal-warning-delete').modal('hide');
                            }
                        } else {
                            console.log('error response', this.response);
                            $('.modal-warning-delete').modal('hide');
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_unblock_card: function (i, eticket_id) {
                var api_token = document.getElementById('api_token');
                var url = unblock_card_url +'?api_token='+api_token.value;
                var form_data = new FormData();
                form_data.append('i', i);
                form_data.append('_token', csrf);
                form_data.append('eticket_id', eticket_id);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    dashboard_functions.ajax_unblock_card_handler();
                                    console.log('success ajax unblock card', response.success);
                                } else if (response.error) {
                                    console.log('error ajax unblock', response.error);
                                    $('.modal-warning-block').modal('hide');
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                $('.modal-warning-block').modal('hide');
                                console.log('error', e.message);
                            }
                        } else {
                            $('.modal-warning-block').modal('hide');
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_unblock_card_handler: function () {
                dashboard_functions.successUnBlocked();
                setTimeout(function () {
                    $('.modal-warning-block').modal('hide');
                }, 2000);
                setTimeout(function () {
                    window.location.reload();
                }, 500);
            },
            ajax_block_card: function (i, eticket_id) {
                var api_token = document.getElementById('api_token');
                var url = block_card_url +'?api_token='+api_token.value;
                var form_data = new FormData();
                form_data.append('i', i);
                form_data.append('_token', csrf);
                form_data.append('eticket_id', eticket_id);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('success ajax block', response.success);
                                    dashboard_functions.ajax_block_card_handler();
                                } else if (response.error) {
                                    console.log('error ajax block', response.error);
                                    $('.modal-warning-block').modal('hide');
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                $('.modal-warning-block').modal('hide');
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                            $('.modal-warning-block').modal('hide');
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_block_card_handler: function () {
                dashboard_functions.successBlocked();
                setTimeout(function () {
                    $('.modal-warning-block').modal('hide');
                }, 2000);
                setTimeout(function () {
                    window.location.reload();
                }, 500);
            },
            ajax_history_listener: function (i) {
                var targets = document.querySelectorAll('#select_history_' + i + ' .btn');
                if (targets) {
                    [].forEach.call(targets, function (target) {
                        target.addEventListener('click', function () {
                            dashboard_functions.ajax_history(i);
                        })
                    })
                }
            },
            ajax_history: function (i) {
                var api_token = document.getElementById('api_token');
                var url = history_url+'?api_token='+api_token.value;
                var form = document.getElementById('history_form_' + i);
                var form_data = new FormData(form);
                form_data.append('i', i);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    var container = document.getElementById('history_container_' + i);
                                    if (container) {
                                        container.innerHTML = response.html;
                                        $('#table_history_' +i).DataTable({
                                            "info": false,
                                            "searching": false,
                                            "lengthChange": false,
                                            "pageLength": response.per_page,
                                            "order": [[4, "asc"]],
                                            oLanguage: {
                                                oPaginate: {
                                                    sNext: '<i class="fa fa-chevron-right" ></i>',
                                                    sPrevious: '<i class="fa fa-chevron-left" ></i>'
                                                }
                                            }
                                        });
                                    }
                                    console.log('success ajax history', response.success);

                                } else if (response.error) {
                                    console.log('error ajax history', response.error);
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_e_card_edit_listener: function (i) {
                var targets = document.querySelectorAll('#edit_card_' + i + ' .btn');
                if (targets) {
                    [].forEach.call(targets, function (target) {
                        target.addEventListener('click', function () {
                            dashboard_functions.ajax_e_card_edit(i);
                        })
                    })
                }
            },
            ajax_e_card_edit: function (i) {
                var api_token = document.getElementById('api_token');
                var url = edit_e_card_url+'?api_token='+api_token.value;
                var form = document.getElementById('e_card_edit_form_' + i);
                var form_data = new FormData(form);
                var details_elem = document.getElementById('details_person_' + i);
                form_data.append('i', i);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('success ajax card edit', response.success);
                                    details_elem.style.display = 'none';
                                    $('#edit_card_' + i + ' .btn').attr('disabled', 'disabled');
                                    $('.modal-limit-changed').modal('show');
                                    setTimeout(function () {
                                        $('.modal-limit-changed').modal('hide');
                                    }, 3000);
//                                    window.location.reload();
                                } else if (response.error) {
                                    console.log('error ajax card edit', response.error);
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            ajax_refill_listener: function (i) {
                var targets = document.querySelectorAll('#topup_' + i + ' .btn');
                if (targets) {
                    [].forEach.call(targets, function (target) {
                        target.addEventListener('click', function () {
                            var select = document.getElementById('payment_type_' + i);
                            if (select && parseInt(select.value) === 1) {
                                dashboard_functions.ajax_refill(i);
                            }
                        })
                    })
                }
            },
            ajax_refill: function (i) {
                var api_token = document.getElementById('api_token');
                var url = refill_ajax_url+'?api_token='+api_token.value;
                var form = document.getElementById('top_up_form_' + i);
                var input = form.elements['top_up_sum_' + i];
                if (input) {
                    if (parseFloat(input.value) < 1) {
                        $('.row-details div.container[class*="step-"]').hide();
                        return false;
                    }
                }
                var form_data = new FormData(form);
                form_data.append('i', i);
                var response;
                var errors_container = document.getElementById('ajax_add_card_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('success ajax refill', response.success);
                                    var container = document.getElementById('payment_container_' + i);
                                    if (container) {
                                        container.innerHTML = response.html;
                                    }

                                } else if (response.error) {
                                    console.log('error ajax refill', response.error);
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]) ;
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            validate_credit_card_item: function (i) {
                setTimeout(function () {
                    custom_functions.validateInput(
                            '#card_' + i,
                            '#credit_card_' + i + ' .btn',
                            null,
                            custom_functions.validatePaymentCard,
                            'card_' + i
                    );
                    custom_functions.validateInput(
                            '#card_holder_' + i,
                            '#credit_card_' + i + ' .btn',
                            null,
                            custom_functions.validateString,
                            'card_holder_' + i
                    );
                    custom_functions.validateInput(
                            '#topup_amount_' + i,
                            '#credit_card_' + i + ' .btn',
                            null,
                            custom_functions.validateNumeric140,
                            'topup_amount_' + i
                    );
                    custom_functions.validateInput(
                            '#card_code_' + i,
                            '#credit_card_' + i + ' .btn',
                            null,
                            custom_functions.validateSecurityCode,
                            'card_code_' + i
                    );
                    var selectors = credit_card_validation.valid_item_selectors(i);
                    custom_functions.validateForm(selectors, credit_card_validation.form_valid, i);
                    credit_card_validation.form_valid(i);
                }, 200);
            },
            validate_history_item: function (i) {
                setTimeout(function () {
                    custom_functions.validateSelect(
                            '#deal_type_' + i,
                            '#select_history_' + i + ' .btn',
                            'deal_type_' + i
                    );
                    var selectors = history_form_validation.valid_item_selectors(i);
                    custom_functions.validateForm(selectors, history_form_validation.form_valid, i);
                    history_form_validation.form_valid(i);
                }, 200);
            },
            validate_top_up_item: function (i) {
                setTimeout(function () {
                    custom_functions.validateInput(
                            '#top_up_sum_'+ i,
                            '#topup_' + i + ' .btn',
                            null,
                            custom_functions.validateNumeric140,
                            'top_up_sum_'+ i
                    );
                    custom_functions.validateSelect(
                            '#payment_type_'+i,
                            '#topup_' + i + ' .btn',
                            'payment_type_' + i
                    );
                    var selectors = amount_form_validation.valid_item_selectors(i);
                    custom_functions.validateForm(selectors, amount_form_validation.form_valid, i);
                    amount_form_validation.form_valid(i);
                }, 200);
            },//top up
            validate_e_card_edit_item: function (i) {
                setTimeout(function () {

                    custom_functions.validateInput(
                            '#e'+ i + '_amount',
                            '#edit_card_' + i + ' .btn',
                            null,
                            custom_functions.validateNumeric140,
                            'e'+ i + '_amount'
                    );
                    custom_functions.validateSelect(
                            '#e'+i+'_period',
                            '#edit_card_' + i + ' .btn',
                            'e' + i+'_period'
                    );
                    var selectors = edit_card_validation.valid_item_selectors(i);
                    custom_functions.validateForm(selectors, edit_card_validation.form_valid, i);
//                    edit_card_validation.form_valid(i);
                }, 200);
            },//edit card
//            validate_amount_item: function (i) {
//                var input = document.querySelector('input[name=top_up_sum_'+i +']');
//                if (input) {
//                    setTimeout(function () {
//
//                        custom_functions.validateInput(
//                                'input[name=top_up_sum_' + i + ']',
//                                '#topup_' + i + ' .btn',
//                                null,
//                                custom_functions.validateNumeric140,
//                                'top_up_sum_' + i
//                        );
//                        custom_functions.validateSelect(
//                                '#payment_type_' + i,
//                                '#topup_' + i + ' .btn',
//                                'payment_type_' + i
//                        );
//                        var selectors = amount_form_validation.valid_item_selectors(i);
//                        custom_functions.validateForm(selectors, amount_form_validation.form_valid, i);
//                        amount_form_validation.form_valid(i);
//                    }, 300);
//                }
//            },//topup
            validate_e_card_item: function (i) {
                for (var j = 1; j < 11; j++) {
                    custom_functions.validateInput(
                            'input[name=n'+i+'_card_number_'+j+']',
                            '#next-step .btn',
                            null,
                            custom_functions.validateNumericWithNull,
                            'n' + i+'_card_number_'+j
                    );
                }
                for (var k = 1; k < 12; k++) {
                    custom_functions.validateInput(
                            'input[name=n'+i+'_pk_'+k+']',
                            '#next-step .btn',
                            null,
                            custom_functions.validateNumericWithNull,
                            'n' + i+'_pk_'+k
                    );
                }
                custom_functions.validateInput(
                        'input[name=n'+i+'_name]',
                        '#next-step .btn',
                        null,
                        custom_functions.validateString,
                        'n' + i+'_name'
                );
                custom_functions.validateInput(
                        'input[name=n'+i+'_surname]',
                        '#next-step .btn',
                        null,
                        custom_functions.validateString,
                        'n' + i+'_surname'
                );
                custom_functions.validateInput(
                        'input[name=n'+i+'_class]',
                        '#next-step .btn',
                        null,
                        custom_functions.validateClass,
                        'n' + i+'_class'
                );
                custom_functions.validateInput(
                        'input[name=n'+i+'_amount]',
                        '#next-step .btn',
                        null,
                        custom_functions.validateNumeric140,
                        'n' + i+'_amount'
                );
                setTimeout(function () {
                    custom_functions.validateSelect(
                            '#n'+i+'_period',
                            '#next-step .btn',
                            'n' + i+'_period'
                    );
                    custom_functions.validateSelect(
                            '#n'+i+'_school',
                            '#next-step .btn',
                            'n' + i+'_school'
                    );
                    var selectors = form_validation.valid_item_selectors(i);
                    custom_functions.validateForm(selectors, form_validation.form_valid, i);
                    form_validation.form_valid(i);
                }, 200);
            }//validate card
        };

        var form_validation = {
            valid_banners: function (i) {
                var result = [];
                for (var j = 1; j < 11; j++) {
                    result.push('n' + i + '_card_number_'+j);
                }
                for (var k = 1; k < 12; k++) {
                    result.push('n' + i + '_pk_'+k);
                }
                result.push('n'+ i + '_period');
                result.push('n'+ i + '_school');
                result.push('n' + i + '_name');
                result.push('n' + i + '_surname');
                result.push('n' + i + '_class');
                result.push('n' + i + '_amount');
                return result;

            },
            valid_selectors: function () {
                var input = document.getElementById('amt');
                if (input) {
                    var amount = parseInt(input.value);
                    if (amount > 0) {
                        var result = [];
                        for (var i =1; i < amount; i++) {
                            for (var j = 1; j < 11; j++) {
                                result.push('input[name=n' + i+'_card_number_'+j + ']');
                            }
                            for (var k = 1; k < 12; k++) {
                                result.push('input[name=n' + i+'_pk_'+k + ']');
                            }
                            result.push('select[name=n' + i+'_period]');
                            result.push('select[name=n' + i+'_school]');
                            result.push('input[name=n' + i+'_name]');
                            result.push('input[name=n' + i+'_surname]');
                            result.push('input[name=n' + i+'_class]');
                            result.push('input[name=n' + i+'_amount]');
                        }
                        return result;
                    }
                }
                return [];
            },
            valid_item_selectors: function (i) {
                var result = [];
                for (var j = 1; j < 11; j++) {
                    result.push('input[name=n' + i+'_card_number_'+j + ']');
                }
                for (var k = 1; k < 12; k++) {
                    result.push('input[name=n' + i+'_pk_'+k + ']');
                }
                result.push('select[name=n' + i+'_period]');
                result.push('select[name=n' + i+'_school]');
                result.push('input[name=n' + i+'_name]');
                result.push('input[name=n' + i+'_surname]');
                result.push('input[name=n' + i+'_class]');
                result.push('input[name=n' + i+'_amount]');
                return result;
            },
            form_valid: function (i) {
                if (typeof i === 'undefined') {
                    i = 0;
                }
                if (form_validation.valid(i)) {
                    custom_functions.enable_button('#next-step .btn');
                } else {
                    custom_functions.disable_button('#next-step .btn');
                }
            },
            valid: function (i) {
                var selectors = form_validation.valid_banners(i);
                var result = true;
                for (var n = 0; n < selectors.length; n++) {
                    var selector = selectors[n];
                    result = result && form_errors[selector];
                }
                return result;
            }
        };

        var amount_form_validation = {
            valid: function (i) {
                var selectors = amount_form_validation.valid_banners(i);
                var result = true;
                for (var n = 0; n < selectors.length; n++) {
                    var selector = selectors[n];
                    result = result && form_errors[selector];
                }
                return result;
            },
            form_valid: function (i) {
                if (amount_form_validation.valid(i)) {
                    custom_functions.enable_button('#topup_' + i + ' .btn');
                } else {
                    custom_functions.disable_button('#topup_' + i + ' .btn');
                }
            },
            valid_banners: function (i) {
                var result = [];
                result.push('top_up_sum_' + i);
//                result.push('payment_type_' + i);
                return result;
            },
            valid_item_selectors: function (i) {
                var result = [];
                result.push('input[name=top_up_sum_' + i + ']');
                result.push('#payment_type_' + i);
                return result;
            }
        };

        var edit_card_validation = {
            valid: function (i) {
                var selectors = edit_card_validation.valid_banners(i);
                var result = true;
                for (var n = 0; n < selectors.length; n++) {
                    var selector = selectors[n];
                    result = result && form_errors[selector];
                }
                return result;
            },
            form_valid: function (i) {
                if (edit_card_validation.valid(i)) {
                    custom_functions.enable_button('#edit_card_' + i + ' .btn');
                } else {
                    custom_functions.disable_button('#edit_card_' + i + ' .btn');
                }
            },
            valid_banners: function (i) {
                var result = [];
                result.push('e'+ i + '_amount');
                result.push('e' + i+'_period');
                return result;
            },
            valid_item_selectors: function (i) {
                var result = [];
                result.push('#e'+ i + '_amount');
                result.push('#e' + i+'_period');
                return result;
            }
        };

        var history_form_validation = {
            valid: function (i) {
                var selectors = history_form_validation.valid_banners(i);
                var result = true;
                for (var n = 0; n < selectors.length; n++) {
                    var selector = selectors[n];
                    result = result && form_errors[selector];
                }
                return result;
            },
            form_valid: function (i) {
                if (history_form_validation.valid(i)) {
                    custom_functions.enable_button('#select_history_' + i + ' .btn');
                } else {
                    custom_functions.disable_button('#select_history_' + i + ' .btn');
                }
            },
            valid_banners: function (i) {
                var result = [];
                result.push('deal_type_' + i);
                return result;
            },
            valid_item_selectors: function (i) {
                var result = [];
                result.push('#deal_type_' + i);
                return result;
            }
        };

        var credit_card_validation = {
            valid: function (i) {
                var selectors = credit_card_validation.valid_banners(i);
                var result = true;
                for (var n = 0; n < selectors.length; n++) {
                    var selector = selectors[n];
                    result = result && form_errors[selector];
                }
                return result;
            },
            form_valid: function (i) {
                if (credit_card_validation.valid(i)) {
                    custom_functions.enable_button('#credit_card_' + i + ' .btn');
                } else {
                    custom_functions.disable_button('#credit_card_' + i + ' .btn');
                }
            },
            valid_banners: function (i) {
                var result = [];
                result.push('topup_amount_' + i);
                result.push('card_holder_' + i);
                result.push('card_' + i);
                result.push('card_code_' + i);
                return result;
            },
            valid_item_selectors: function (i) {
                var result = [];
                result.push('#topup_amount_' + i);
                result.push('#card_holder_' + i);
                result.push('#card_' + i);
                result.push('#card_code_' + i);
                return result;
            }
        };

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $('#add-person .btn').click(function () {
            $('#person_2').show();
        });

        $('#btn-add-new-photo .btn').click(function () {
            $('.modal-add-child-photo').modal('show');
        });



        $(document).ready(function () {

            $('.action-person, .action-topup, .action-history, .action-block, .action-unblock, .action-delete').click(function () {
                var object_id = $(this).find('a').attr('data-id');
                var details_type = $(this).find('a').attr('data-details');
                var details_block = $('#details_' + details_type + '_' + object_id);
                var is_visible = $(details_block).is(":visible");

                $('.row-action.current').removeClass('current');
                if (!$(details_block).is(":visible")) {
                    if ($(this).hasClass('action-person')) {
                        $(this).parent().closest('.person-block').find('.row-action').addClass('current');
                    } else {
                        $(this).addClass('current');
                    }
                }
                $('[id^=details_]').hide();
                $('.row-details div.container[class*="step-"]').hide();
                $('.row-details div.container.step-1-' + object_id).show();
                if ($(details_block).is(":visible") || is_visible === true)
                    $('#details_' + details_type + '_' + object_id).hide();
                else {
                    $('.step-2-' + object_id + ' input[name^=card_holder_]').val('');
                    $('.step-2-' + object_id + ' input[name^=card_]').val('');
                    $('.step-2-' + object_id + ' input[name=card_code]').val('');
                    $('.step-1-' + object_id + ' input[name=amount_]').val('');
                    $('.step-1-' + object_id + ' select[name=payment_type]').val('');

                    $('#details_' + details_type + '_' + object_id).show();
                }
            });

            // --- PERSON


            // --- TOPUP

            $('.row-details[id^=details_topup_] div.container.step-1 .btn').click(function () {
                var i = $(this).attr('data-id');
                var selector = '#details_topup_' + i + ' select[name^=payment_type_] :selected';
                var payment_type = parseInt($(selector).val());
                if (payment_type > 0) {
                    $('.row-details div.container[class*="step-"]').hide();
                }
                if (payment_type == 1) {
                    $('.row-details div.container.step-2b-' +i).show();
                }
                if (payment_type == 2) {
                    var hidden_input = $('#topup_amount_' + i);
                    var value = $('#top_up_sum_' + i).val();
                    hidden_input.val(value);
                    var title = $('#title_amount_' + i);
                    title.text(value);
                    $('.row-details div.container.step-2-'+i).show();
                }
                // else do nothing for now
            });

            $('a.card-code').click(function () {
                $('.modal-card-code-help').modal('show');
            });

            $('.row-details[id^=details_topup_] div.container.step-2 .btn').click(function () {
                var i = $(this).attr('data-id');
                $('.row-details div.container[class*="step-"]').hide();
                if ($('.step-2-' + i + ' input[name=card_holder_' + i + ']').val() == ''
                        || $('.step-2 input[name=card_' + i + ']').val() == ''
                        || $('.step-2 input[name=card_code_' + i + ']').val() == ''
                ) {
                    $('.row-details div.container.step-4-' + i).show();  // failure
                } else {
                    $('.row-details div.container.step-3-' +i).show();  // success
                    $('.step-2-' + i + ' input[name^=card_holder_]').val('');
                    $('.step-2-' + i + ' input[name^=card]').val('');
                    $('.step-2-' + i + ' input[name^=card_code_]').val('');
                    $('.step-2-' + i + ' input[name^=expiry_month]').val('');
                    $('.step-2-' + i + ' input[name^=expiry_year]').val('');
                    $('.step-1-' + i + ' input[name^=amount_]').val('');
                    $('.step-1-' + i + ' select[name^=payment_type_]').val('');
                }
            });

            $('.row-details[id^=details_topup_] div.container.step-4 .btn').click(function () {
                var details_block = $(this).parent().closest('.row-details[id^=details_topup_]');
                $(details_block).find('div.container[class*="step-"]').hide();
                $(details_block).find('div.container.step-2').show();
            });


            $('.select2-select.limits').select2({
                minimumResultsForSearch: Infinity,
                // allowClear: true,
                placeholder: "{{ $trans['limit'] }}"
            });

            $('.select2-select.schools').select2({
                // allowClear: true,
                placeholder: "{{ $trans['select_school'] }}"
            });

            $('.select2-select.payment-type').select2({
                minimumResultsForSearch: Infinity,
                // allowClear: true,
                placeholder: "{{ $trans['select_payment_type'] }}"
            });

            $('.select2-select.expiry-month').select2({
                minimumResultsForSearch: Infinity//,
                // allowClear: true,
                {{--// placeholder: "@lang('portal/dashboard.select')"--}}
            });

            $('.expiry_year').select2({
                minimumResultsForSearch: Infinity//,
                // allowClear: true,
                {{--// placeholder: "@lang('portal/dashboard.select')"--}}
            });

            // --- HISTORY

            $('.table-history').DataTable({
                "info": false,
                "searching": false,
                "lengthChange": false,
                "pageLength": 10,
                "order": [[4, "asc"]],
                oLanguage: {
                    oPaginate: {
                        sNext: '<i class="fa fa-chevron-right" ></i>',
                        sPrevious: '<i class="fa fa-chevron-left" ></i>'
                    }
                }
            });

            $('#table_notifications').DataTable({
                "info": false,
                "searching": false,
                "lengthChange": false,
                "pageLength": 10,
                "order": [[3, "desc"]],
                oLanguage: {
                    oPaginate: {
                        sNext: '<i class="fa fa-chevron-right" ></i>',
                        sPrevious: '<i class="fa fa-chevron-left" ></i>'
                    }
                }
            });

            $('#table_invoices').DataTable({
                "info": false,
                "searching": false,
                "lengthChange": false,
                "pageLength": 10,
                "order": [[0, "desc"]],
                oLanguage: {
                    oPaginate: {
                        sNext: '<i class="fa fa-chevron-right" ></i>',
                        sPrevious: '<i class="fa fa-chevron-left" ></i>'
                    }
                }
            });

            $('.select2-select.deal-type').select2({
                minimumResultsForSearch: Infinity,
                allowClear: true,
                placeholder: "{{ $trans['transaction_type'] }}"
            });

            $('.select2-select.notification-type').select2({
                minimumResultsForSearch: Infinity,
                allowClear: true,
                placeholder: "{{ $trans['status'] }}"
            });

            $('.select2-select.invoice-type').select2({
                minimumResultsForSearch: Infinity,
                allowClear: true,
                placeholder: "{{ $trans['status'] }}"
            });

            $('.select2-select.invoice-ecard').select2({
                minimumResultsForSearch: Infinity,
                allowClear: true,
                placeholder: "{{ $trans['invoice_card'] }}"
            });

            $('.select2-select.per-page').select2({
                minimumResultsForSearch: Infinity
            });

            var start = moment().subtract(1, 'months');
            var end = moment();

            $('input[name^=deal_daterange_]').daterangepicker(
                {
                    locale: locale,
                    "alwaysShowCalendars": true,
                    "startDate": "{{ (new \DateTime('now'))->modify('-1 month')->format('Y-m-d') }}",
                    "endDate": "{{ (new \DateTime('now'))->format('Y-m-d') }}",
                    "opens": "center",
                    ranges: ranges
                },
                function (start, end, label) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                }
            );

            $('input[name=notification_daterange]').daterangepicker(
                {
                    locale: locale,
                    "alwaysShowCalendars": true,
                    "startDate": "{{ (new \DateTime('now'))->modify('-1 month')->format('Y-m-d') }}",
                    "endDate": "{{ (new \DateTime('now'))->format('Y-m-d') }}",
                    "opens": "center",
                    ranges: ranges
                },
                function (start, end, label) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                }
            );

            $('input[name="invoice_daterange"]').daterangepicker(
                {
                    locale: locale,
                    "alwaysShowCalendars": true,
                    "startDate": "{{ (new \DateTime('now'))->modify('-1 month')->format('Y-m-d') }}",
                    "endDate": "{{ (new \DateTime('now'))->format('Y-m-d') }}",
                    "opens": "center",
                    ranges: ranges,
                },
                function (start, end, label) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                }
            );

            // --- BLOCK

            $('.row-details[id^=details_block_] div.container.step-1 #block_button').click(function () {
                var i = $(this).attr('data-i');
                var id = $(this).attr('data-id');
                var balance = $(this).attr('data-balance');
                balance = parseInt(balance);
                var el = document.getElementById('child_name_'+i);
                var target = document.getElementById('block_modal_name');
                if (el) {
                    var name = el.textContent;
                    if (target) {
                        target.textContent = name;
                    }

                }
                $('#block_card_modal').attr('data-i', i).attr('data-id', id).attr('data-type', 'block');
                var row = $(this).parent().closest('.row-details');
                $(row).find('div.container[class*="step-"]').hide();
                if (balance > 0) {
                    $(row).find('div.container.step-1b').show();
                } else {
                    dashboard_functions.setBlock();
                    $('.modal-warning-block').modal('show');
                }
            });

            $('#block_card_modal').click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                var i = $(this).attr('i');
                var eticket_id = $(this).attr('data-id');
                var type = $(this).attr('data-type');
                if (type === 'block') {
                    dashboard_functions.ajax_block_card(i, eticket_id);
                } else if (type === 'unblock') {
                    dashboard_functions.ajax_unblock_card(i, eticket_id);
                } else {
                    $('.modal-warning-block').modal('hide');
                }
            });

            $('#not_block_card_modal').click(function (e) {
                $('.modal-warning-block').modal('hide');
            });

            $('.row-details[id^=details_block_] div.container.step-1b .btn').click(function () {
                var row = $(this).parent().closest('.row-details');
                $(row).find('div.container[class*="step-"]').hide();
            });

            // --- UNBLOCK

            $('.row-details[id^=details_unblock_] div.container.step-1 #unblock_button').click(function () {
                var i = $(this).attr('data-i');
                var id = $(this).attr('data-id');
                var el = document.getElementById('child_name_'+i);
                var target = document.getElementById('block_modal_name');
                if (el) {
                    var name = el.textContent;
                    if (target) {
                        target.textContent = name;
                    }

                }
                $('#block_card_modal').attr('data-i', i).attr('data-id', id).attr('data-type', 'unblock');
                var row = $(this).parent().closest('.row-details');
                $(row).find('div.container[class*="step-"]').hide();
                dashboard_functions.setUnblock();
                $('.modal-warning-block').modal('show');
            });

            // --- DELETE

            $('.row-details[id^=details_delete_] div.container.step-1 .delete-button').click(function () {
                var i = $(this).attr('data-i');
                var id = $(this).attr('data-id');
                var balance = $(this).attr('data-balance');
                balance = parseInt(balance);
                var el = document.getElementById('child_name_'+i);
                var target = document.getElementById('delete_modal_name');
                if (el) {
                    var name = el.textContent;
                    if (target) {
                        target.textContent = name;
                    }

                }
                $('#delete_card_modal').attr('data-i', i).attr('data-id', id);
                var row = $(this).parent().closest('.row-details');
                $(row).find('div.container[class*="step-"]').hide();
                if (balance > 0) {
                    $(row).find('div.container.step-2').show();
                } else {
                    $('.modal-warning-delete').modal('show');
                }
            });

            $('.modal-warning-delete .modal-yes').click(function () {
                var i = $(this).attr('data-i');
                var id = $(this).attr('data-id');
                $('.modal-warning-delete').modal('hide');
                var row = $('.row-details[id^=details_delete_]:visible');
                $(row).find('div.container[class*="step-"]').hide();
                dashboard_functions.ajax_delete_card(i, id);
            });
            $('.modal-warning-delete .modal-no').click(function () {
                $('.modal-warning-delete').modal('hide');
            });

            // --- ADD PERSON

            $('.ecard-table .add-person .avatar.add').click(function () {
                $('.row-details[id^=details_]:visible').hide();
                $('.row-action.current').removeClass('current');
                $('.row-details[id^=details_]:visible').find('div.container[class*="step-"]').hide();
                var details_block = $('#details_addperson_0');
                if ($(details_block).hasClass('visible')) {
                    $(details_block).removeClass('visible');
                    $(details_block).hide();
                }
                else {
                    $(details_block).addClass('visible');
                    $(details_block).show();
                }
            });

            // -- PROFILE MENU

            $('.profile-menu a .item').click(function () {
                $('.profile-menu a .item.active').removeClass('active');
                $(this).addClass('active');
                $('.profile-section').hide();  // hide all sections
                var target_id = $(this).attr('data-id');
                $('section#' + target_id).show();
            });

            // -- PROFILE MENU -- INVOICES
            $('#delete_payment_no .btn').click(function () {
                $('.modal-invoice-cancel').modal('hide');
            });

            ///////////////

            $('.avatar-grid .row .col-sm-3 img').click(function () {
                $('.avatar-grid .row .col-sm-3 img.active').removeClass('active');
                $(this).addClass('active');
            });

            var modal = getUrlParameter('modal');
            if (modal) {
                // console.log(modal);
                if (modal == 'modal-rules') $('.modal-rules').modal('show');
                if (modal == 'modal-warning-1') $('.modal-warning-1').modal('show');
                if (modal == 'modal-add-new-photo') $('.modal-add-new-photo').modal('show');
                if (modal == 'modal-reg-ecard-help') $('.modal-reg-ecard-help').modal('show');
                if (modal == 'modal-add-child-photo') $('.modal-add-child-photo').modal('show');
            }

            $('.box-container .form-control.input').keyup(function (event) {
                //

            }).keydown(function (event) {
                if (event.which == 13 || event.which == 32) {
                    event.preventDefault();
                } else if ($(this).val().length > 0 && (event.which != 8 && event.which != 46 && event.which != 9)) {
                    event.preventDefault();
                }

            });

        });
        document.addEventListener('DOMContentLoaded', function (e) {
            var elements = document.querySelectorAll('input[name^=top_up_sum]');
            [].forEach.call(elements, function (element) {
                element.addEventListener('keyup', function () {
                    var value = element.value;
                    if (isNaN(value)) {
                        element.value = 0;
                    }
                })
            });
            var input = document.getElementById('amt');
            var amount = parseInt(input.value);
            amount += 1;
            input.value = amount;
            dashboard_functions.validate_e_card_item(0);
            $('#btn-add-new-photo_0').click(function () {
                $('#modal-add-child-photo_0').modal('show');
            });
            dashboard_functions.ajax_week_expenses_listener();
            dashboard_functions.ajax_notifications_listener();
            dashboard_functions.ajax_payments_listener();
            dashboard_functions.ajax_add_card_listener();
            dashboard_functions.ajax_delete_payment_listener();
            dashboard_functions.ajax_modal_delete_payment_listener();
            for (var k = 1; k < amount; k++) {
                $('#btn-add-new-photo_' + k + ' .btn').click(function () {
                    var i = $(this).attr('data-id');
                    $('#modal-add-child-photo_' + i).modal('show');
                });
                $('#edit_card_' + k + ' .btn').attr('disabled', 'disabled');
                $(document.body).on('change','#e'+ k + '_period', function(){
                    var n = $(this).attr('data-id');
                    $('#edit_card_' + n + ' .btn').removeAttr('disabled');
                });
                dashboard_functions.validate_credit_card_item(k);
//                dashboard_functions.validate_history_item(k);
                dashboard_functions.validate_top_up_item(k);
                dashboard_functions.validate_e_card_edit_item(k);
//                dashboard_functions.validate_amount_item(k);
                dashboard_functions.ajax_refill_listener(k);
                dashboard_functions.ajax_e_card_edit_listener(k);
                dashboard_functions.ajax_history_listener(k);
                dashboard_functions.ajax_refill_clean_listener(k);
            }
        });
    </script>
@stop