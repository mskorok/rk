@extends('portal.layouts.main')
@section('title')
   @lang('portal/terms.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/terms.description')">
    <meta name="author" content="@lang('portal/terms.author')">
    <meta name="keywords" content="@lang('portal/terms.keywords')">
@stop
@section('css')
    @parent
    <!-- Libraries styles -->
    <link href="{{ asset('assets/portal/css/bootstrap-imageupload.min.css').'?'.time() }}" rel="stylesheet">
@stop
@section('content')
    @include('portal.views.partials.terms_of_use')
@stop
@section('modals')
    <!-- Modals
================================================== -->
    @parent
    @include('portal.views.partials.modals.rules')
    @include('portal.views.partials.modals.warning')
    @include('portal.views.partials.modals.timer')
@stop
@section('js')
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    @parent
    <script src="{{ asset('assets/portal/js/bootstrap-imageupload.min.js').'?'.time() }}"></script>
@stop
@section('script')
    <script type="text/javascript">
        $( document ).ready(function() {
            // $('.modal-rules').modal('show');
            // $('.modal-warning-1').modal('show');
            // $('.modal-add-new-photo').modal('show');

//            $('.btn-group.register').click(function() {
//                window.location.assign('register_require.html');
//            });
//
//            $('.btn-group.signin').click(function() {
//                window.location.assign('dashboard.html');
//            });


        });
    </script>
@stop