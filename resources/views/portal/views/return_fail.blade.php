@extends('portal.layouts.main')
@section('title')
    @lang('portal/dashboard.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/dashboard.description')">
    <meta name="author" content="@lang('portal/dashboard.author')">
    <meta name="keywords" content="@lang('portal/dashboard.keywords')">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('css')
    @parent
    <!-- Libraries styles -->
    <link href="{{ asset('assets/portal/css/bootstrap-imageupload.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/select2.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/jquery.dataTables.min.css').'?'.time() }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/portal/css/jquery.dataTables.reset.css').'?'.time() }}" rel="stylesheet">

@stop
@section('content')
    <?php
    require base_path('card/includes/config.php');
    require base_path('card/includes/Merchant.php');


    $host = $db_host;
    $db = $db_database;
    $user = $db_user;
    $pass = $db_pass;
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $pdo = new PDO($dsn, $user, $pass, $opt);

    $trans_id = $_REQUEST['trans_id'];
    $error_msg = $_REQUEST['error'];
    //$var = ($_POST['var']); //getting additional parameters
            echo '<div style="height: 500px;text-align: center; padding-top: 100px;color: #E44732;font-size: 2rem;font-weight: 600;">';
    echo trans('portal/card.fail').' <br><br>' . $error_msg;
    echo '</div>';

    $stmt = $pdo->prepare('SELECT client_ip_addr FROM `transaction` WHERE `trans_id` = :trans_id');
    $stmt->bindParam(':trans_id', $trans_id);


    if (!$stmt->execute()) {
        $error = $stmt->errorInfo()[2];
        $stmt->closeCursor();
        die('*** Invalid query1: ' . $error);
    } else {
        $row = $stmt->fetch();
        $client_ip_addr = $row[0];
    }
    $stmt->closeCursor();

    $merchant = new Merchant($ecomm_server_url, $cert_url, $cert_pass, 1);

    $resp = $merchant->getTransResult(urlencode($trans_id), $client_ip_addr);
    $resp = htmlentities($resp, ENT_QUOTES);
    $resp = $error_msg . ' + ' . $resp;
    $stmt = $pdo->prepare('INSERT INTO error VALUES (default, now(), "ReturnFailURL", :resp)');
    $stmt->bindParam(':resp', $resp);

    if (!$stmt->execute()) {
        $error = $stmt->errorInfo()[2];
        $stmt->closeCursor();
        die('*** Invalid query2: ' . $error);
    }

    $stmt->closeCursor();
    ?>
@stop
@section('js')
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    @parent
    <script src="{{ asset('assets/portal/js/bootstrap-imageupload.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/select2.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/jquery.dataTables.min.js').'?'.time() }}"></script>
    <script src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>
    <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
@stop