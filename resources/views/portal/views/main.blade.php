@extends('portal.layouts.main')
@section('title')
    @lang('portal/main.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/main.description')">
    <meta name="author" content="@lang('portal/main.author')">
    <meta name="keywords" content="@lang('portal/main.keywords')">
@stop
@section('css')
    @parent
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/portal/css/jquery-ui.min.css').'?'.time() }}" rel="stylesheet">
@stop
@section('modals')
    @parent
    <!-- Modals
================================================== -->
    @include('portal.views.partials.modals.timer')

@stop
@section('content')
    @if($topSlider === 'yes')
        @include('portal.views.partials.top_slider')
    @endif
    @if($section1 === 'yes')
        @include('portal.views.partials.land.section_1')
    @endif
    @if($section2 === 'yes')
        @include('portal.views.partials.land.section_2')
    @endif
    @if($sectionCard === 'yes')
        @include('portal.views.partials.land.section_card')
    @endif
    @if($section3 === 'yes')
        @include('portal.views.partials.land.section_3')
    @endif
    @if($section4 === 'yes')
        @include('portal.views.partials.land.section_4')
    @endif
    @if($section5 === 'yes')
        @include('portal.views.partials.land.section_5')
    @endif
    @if($section6 === 'yes')
        @include('portal.views.partials.land.section_6')
    @endif
    @if($section7 === 'yes')
        @include('portal.views.partials.land.section_7')
    @endif
    @if($section8 === 'yes')
        @include('portal.views.partials.land.section_8')
    @endif

@stop
@section('script')
    <script type="text/javascript">
        var friends_url = '{{ route('friends') }}';
        var landing_fnc = {
            friends_listener: function () {
                $('#friends_submit').click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    landing_fnc.friends_ajax();
                });
            },
            friends_ajax: function () {
                var url = friends_url;
                var form = document.getElementById('friends_form');
                var form_data = new FormData(form);
                var response;
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('friends', response.success);
                                    landing_fnc.friends_success_handler();
                                } else if (response.error) {
                                    console.log('friends', response.error);
                                    landing_fnc.friends_error_handler();
                                }
                            } catch (e) {
                                console.log('error', e.message);
                                landing_fnc.friends_error_handler();
                            }
                        } else {
                            console.log('error response', this.response);
                            landing_fnc.friends_error_handler();
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            friends_success_handler: function () {
                var success = document.getElementById('friends_success');
                if (success) {
                    success.style.display = 'block';
                    setTimeout(function () {
                        success.style.display = 'none';
                    }, 5000);
                }
            },
            friends_error_handler: function () {
                var error = document.getElementById('friends_error');
                if (error) {
                    error.style.display = 'block';
                    setTimeout(function () {
                        error.style.display = 'none';
                    }, 5000);
                }
            },
            friend_validation: function () {
                custom_functions.validateInput(
                        '#friends_email',
                        '#friends_submit',
                        1,
                        custom_functions.validateEmail
                );
            }
        };

        $( document ).ready(function() {

            $('.btn-group.register').click(function() {
                window.location.assign('{{ route('register') }}');
            });

            {{--$('.btn-group.signin').click(function() {--}}
                {{--window.location.assign('{{ route('dashboard') }}');--}}
            {{--});--}}

            $('.btn.btn-support').click(function() {
                if ($('#support_name').val() == ''
                        || $('#support_surname').val() == ''
                        || $('#support_email').val() == ''
                        || $('#support_phone').val() == ''
                ) {
                    $(this).parent().find('.messages .success-message').hide();
                    $(this).parent().find('.messages .error-message').show();
                } else {
                    $(this).parent().find('.messages .success-message').show();
                    $(this).parent().find('.messages .error-message').hide();
                }
            });

            $('#carousel-generic').carousel();

            var email = document.getElementById('login');
            if (email) {
                var errorContainer = email.parentNode;
                if (errorContainer.classList.contains('has-error')) {
                    console.log('has error');
                    $('.avatar-name').click();
                }
            }

            $('.js-owl-carousel-partner').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,

                autoplay: true,
                autoplayTimeout: 7000,
                autoplayHoverPause: true,

                responsiveClass: true,
                responsive: {
                    0: {
                        items: '{{ $size['s'] }}',
                        nav: false
                    },
                    768: {
                        items: '{{ $size['m'] }}',
                        nav: false
                    },
                    990: {
                        items: '{{ $size['l'] }}'
                    }
                }
            });
            landing_fnc.friends_listener();
            landing_fnc.friend_validation();
        });
    </script>
@stop