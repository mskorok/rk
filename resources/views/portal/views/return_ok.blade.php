@extends('portal.layouts.main')
@section('title')
    @lang('portal/dashboard.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/dashboard.description')">
    <meta name="author" content="@lang('portal/dashboard.author')">
    <meta name="keywords" content="@lang('portal/dashboard.keywords')">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('css')
    @parent
    <!-- Libraries styles -->
    <link href="{{ asset('assets/portal/css/bootstrap-imageupload.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/select2.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/jquery.dataTables.min.css').'?'.time() }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/portal/css/jquery.dataTables.reset.css').'?'.time() }}" rel="stylesheet">

@stop
@section('content')
    <?php
    require base_path('card/includes/config.php');
    require base_path('card/includes/Merchant.php');


    $host = $db_host;
    $db = $db_database;
    $user = $db_user;
    $pass = $db_pass;
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $pdo = new PDO($dsn, $user, $pass, $opt);


    $trans_id = $_REQUEST['trans_id'];
    //$var = ($_POST['var']); //getting additional parameters


    $stmt = $pdo->prepare('SELECT client_ip_addr FROM `transaction` WHERE `trans_id` = :trans_id');
    $stmt->bindParam(':trans_id', $trans_id);


    if (!$stmt->execute()) {
        $error = $stmt->errorInfo()[2];
        $stmt->closeCursor();
        die('*** Invalid query1: ' . $error);
    }

    $row = $stmt->fetch();
    $client_ip_addr = $row[0];

    $stmt->closeCursor();

    $merchant = new Merchant($ecomm_server_url, $cert_url, $cert_pass, 1);

    $resp = $merchant->getTransResult(urlencode($trans_id), $client_ip_addr);

    if (strstr($resp, 'RESULT:')) {
        //$resp example RESULT: OK RESULT_CODE: 000 3DSECURE: NOTPARTICIPATED RRN: 915300393049 APPROVAL_CODE: 705368 CARD_NUMBER: 4***********9913


        if (strstr($resp, 'RESULT:')) {
            $result = explode('RESULT: ', $resp);
            $result = preg_split('/\r\n|\r|\n/', $result[1]);
            $result = $result[0];
        } else {
            $result = '';
        }

        if (strstr($resp, 'RESULT_CODE:')) {
            $result_code = explode('RESULT_CODE: ', $resp);
            $result_code = preg_split('/\r\n|\r|\n/', $result_code[1]);
            $result_code = $result_code[0];
        } else {
            $result_code = '';
        }

        if (strstr($resp, '3DSECURE:')) {
            $result_3dsecure = explode('3DSECURE: ', $resp);
            $result_3dsecure = preg_split('/\r\n|\r|\n/', $result_3dsecure[1]);
            $result_3dsecure = $result_3dsecure[0];
        } else {
            $result_3dsecure = '';
        }

        if (strstr($resp, 'CARD_NUMBER:')) {
            $card_number = explode('CARD_NUMBER: ', $resp);
            $card_number = preg_split('/\r\n|\r|\n/', $card_number[1]);
            $card_number = $card_number[0];
        } else {
            $card_number = '';
        }

        $stmt = $pdo->prepare('UPDATE `transaction` SET `result` = :result, `result_code` = :result_code, `result_3dsecure` = :result_3dsecure, `card_number` = :card_number, `response` = :resp WHERE `trans_id` = :trans_id');
        $stmt->bindParam(':result', $result);
        $stmt->bindParam(':result_code', $result_code);
        $stmt->bindParam(':result_3dsecure', $result_3dsecure);
        $stmt->bindParam(':card_number', $card_number);
        $stmt->bindParam(':response', $response);
        $stmt->bindParam(':trans_id', $trans_id);
        if (!$stmt->execute()) {
            $error = $stmt->errorInfo()[2];
            $stmt->closeCursor();
            die('*** Invalid query: ' . $error);
        }
        echo '<div style="height: 500px;text-align: center; padding-top: 100px;color: darkolivegreen;font-size: 2rem;font-weight: 600;">';
        echo $resp;
        echo '</div>';
        $stmt->closeCursor();

    } else {
        echo '<div style="height: 500px;text-align: center; padding-top: 100px;color: darkolivegreen;font-size: 2rem;font-weight: 600;">';
        echo $resp;
        echo '</div>';
        $resp = htmlentities($resp, ENT_QUOTES);
        $stmt = $pdo->prepare("INSERT INTO `error` VALUES (default, now(), 'ReturnOkURL', :resp)");
        $stmt->bindParam(':resp', $resp);


        if (!$stmt->execute()) {
            $error = $stmt->errorInfo()[2];
            $stmt->closeCursor();
            die('*** Invalid query: ' . $error);
        }

        $stmt->closeCursor();

    }
    ?>
@stop
@section('js')
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    @parent
    <script src="{{ asset('assets/portal/js/bootstrap-imageupload.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/select2.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/jquery.dataTables.min.js').'?'.time() }}"></script>
    <script src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>    
    <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
@stop