{!! trans('portal/mail/change_email.before_link') !!}
<br>
<a href="{{ $link = route('edit_profile').'/?token='.$user->confirmation_code.'&date='.urlencode(app('UserService')->getDocumentDate(1)).'&old_email='.urlencode($user->email).'&new_email='.urlencode($newEmail) }}"> {{ $link }} </a>
<br>
{!! trans('portal/mail/change_email.after_link') !!}
<br>
<i>
    {{ $link  }}
</i>
<br>
{!! trans('portal/mail/change_email.after_browser_link', ['new_email' => $newEmail]) !!}
<br>
{!! trans('passwords.disclaimer') !!}