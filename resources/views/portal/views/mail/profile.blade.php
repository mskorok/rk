{!! trans('portal/mail/profile.text', ['name' => $user->username, 'surname' => $user->surname, 'email' => $user->email]) !!}
<br>
{!! trans('passwords.disclaimer') !!}