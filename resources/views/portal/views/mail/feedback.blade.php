@php
    $date = (new DateTime('now'))->format('d-m-Y H:i:s');
@endphp
{!! trans('portal/mail/feedback.text', compact('date', 'name', 'surname', 'email', 'phone', 'text')) !!}