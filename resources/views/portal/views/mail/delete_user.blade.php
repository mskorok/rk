{!! trans('portal/mail/delete_user.text', ['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email]) !!}
<br>
{!! trans('passwords.disclaimer') !!}