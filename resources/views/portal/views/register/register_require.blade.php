@extends('portal.layouts.main')
@section('title')
    @lang('portal/register/require.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/register/require.description')">
    <meta name="author" content="@lang('portal/register/require.author')">
    <meta name="keywords" content="@lang('portal/register/require.keywords')">
@stop
@section('content')
    @include('portal.views.partials.register.section_register_require')
    @include('portal.views.partials.register.section_8')
@stop
@section('modals')
    @parent
    <!-- Modals
================================================== -->
    @include('portal.views.partials.modals.timer')

@stop