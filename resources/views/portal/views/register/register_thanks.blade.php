@extends('portal.layouts.main')
@section('title')
    @lang('portal/register/thanks.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/register/thanks.description')">
    <meta name="author" content="@lang('portal/register/thanks.author')">
    <meta name="keywords" content="@lang('portal/register/thanks.keywords')">
@stop
@section('content')
    @include('portal.views.partials.register.section_thanks')
    @include('portal.views.partials.register.section_8')
@stop
@section('modals')
    @parent
    <!-- Modals
================================================== -->
    @include('portal.views.partials.modals.timer')

@stop