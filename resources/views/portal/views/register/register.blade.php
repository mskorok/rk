@extends('portal.layouts.main')

@section('title')
    @lang('portal/register/register.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/register/register.description')">
    <meta name="author" content="@lang('portal/register/register.author')">
    <meta name="keywords" content="@lang('portal/register/register.keywords')">
@stop
@section('content')
    @include('portal.views.partials.register.section_register')
    @include('portal.views.partials.land.section_8')
@stop
@section('modals')
    @parent
    <!-- Modals
================================================== -->
    @include('portal.views.partials.modals.timer')

@stop
@section('script')
    <script type="text/javascript">

        $( document ).ready(function() {

            var icones = document.getElementById('icones');
            icones.addEventListener('click', function (e) {
                var parent = icones.parentNode;
                if (parent.hasAttribute('disabled')) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            });

            var email_correct = false;
            var password_correct = false;
            $('.email-register1').blur(function() {
                var mail = $(this);
                var parent = mail.parent();
                var button = $('#register_submit_button .btn');
                if (mail.val() != '') {
                    if (mail.val().search(pattern) == 0) {
                        $(parent).find('.error-message1').hide();
                        mail.removeClass('error').addClass('success');
                        button.attr('disabled', true);
                    } else {
                        $(parent).find('.error-message1').show();
                        mail.addClass('error').removeClass('success');
                        button.attr('disabled', true);
                    }
                } else {
                    $(parent).find('.error-message').hide();
                    mail.removeClass('success error');
                    button.attr('disabled', true);
                }
            }).focus(function () {
                var input = $(this);
                var button = $('#register_submit_button .btn');
                var parent = input.parent();
                $(parent).find('.error-message').hide();
                input.removeClass('success error');
                button.attr('disabled', true);
                email_correct = false;
            });

            $('.email-register2').blur(function() {
                var mail = $(this);
                var parent = mail.parent();
                var button = $('#register_submit_button .btn');
                if (mail.val() != '') {
                    if (mail.val().search(pattern) == 0) {
                        var mail1 = $('.email-register1');
                        $(parent).find('.error-message2').hide();
                        if (mail1.val() !== mail.val()) {
                            $(parent).find('.error-message3').show();
                            mail.addClass('error').removeClass('success');
                            button.attr('disabled', true);
                        } else {
                            mail.removeClass('error').addClass('success');
                            button.attr('disabled', true);
                            email_correct = true;
                            if (password_correct) {
                                button.removeAttr('disabled');
                            }
                        }
                    } else {
                        $(parent).find('.error-message2').show();
                        mail.addClass('error').removeClass('success');
                        button.attr('disabled', true);
                    }
                } else {
                    $(parent).find('.error-message').hide();
                    mail.removeClass('success error');
                    button.attr('disabled', true);
                }
            }).focus(function () {
                var input = $(this);
                var button = $('#register_submit_button .btn');
                var parent = input.parent();
                $(parent).find('.error-message').hide();
                input.removeClass('success error');
                button.attr('disabled', true);
            }).on('input', function () {
                var mail = $(this);
                var button = $('#register_submit_button .btn');
                var first_mail = $('.email-register1');
//                console.log(mail.val(), first_mail.val(), password_correct);
                if (mail.val() === first_mail.val()) {
                    email_correct = true;
//                    console.log('email', email_correct);
                    if (password_correct) {
                        button.removeAttr('disabled');
                    }
                } else {
                    button.attr('disabled', true);
                    email_correct = false;
//                    console.log('email', email_correct)
                }
            });

            $('.password-register1').blur(function() {
                var input = $(this);
                var button = $('#register_submit_button .btn');
                var parent = input.parent();
                if (input.val() != '') {
                    if (custom_functions.validatePassword(input.val())) {
                        $(parent).find('.error-message1').hide();
                        input.removeClass('error').addClass('success');
                        button.attr('disabled', true);
                    } else {
                        $(parent).find('.error-message1').show();
                        input.addClass('error').removeClass('success');
                        button.attr('disabled', true);
                    }
                } else {
                    $(parent).find('.error-message').hide();
                    input.removeClass('success error');
                    button.attr('disabled', true);
                }
            }).focus(function () {
                var input = $(this);
                var button = $('#register_submit_button .btn');
                var parent = input.parent();
                $(parent).find('.error-message').hide();
                input.removeClass('success error');
                password_correct = false;
                button.attr('disabled', true);
            });
            $('.password-register2').blur(function() {
                var input = $(this);
                var button = $('#register_submit_button .btn');
                var parent = input.parent();
                if (input.val() != '') {
                    var input1 = $('.password-register1');
                    if (custom_functions.validatePassword(input1.val())) {
                        $(parent).find('.error-message4').hide();
                        if (input1.val() !== input.val()) {
                            $(parent).find('.error-message5').show();
                            input.addClass('error').removeClass('success');
                            button.attr('disabled', true);
                        } else {
                            input.removeClass('error').addClass('success');
                            password_correct = true;
                            if (email_correct) {
                                button.removeAttr('disabled');
                            }
                        }
                    } else {
                        $(parent).find('.error-message4').show();
                        input.addClass('error').removeClass('success');
                        button.attr('disabled', true);
                    }
                } else {
                    $(parent).find('.error-message').hide();
                    input.removeClass('success error');
                    button.attr('disabled', true);
                }
            }).focus(function () {
                var input = $(this);
                var button = $('#register_submit_button .btn');
                var parent = input.parent();
                $(parent).find('.error-message').hide();
                input.removeClass('success error');
                button.attr('disabled', true);
            }).on('input', function () {
                var input = $(this);
                var button = $('#register_submit_button .btn');
                var first_input = $('.password-register1');
                if (input.val() === first_input.val()) {
                    password_correct = true;
                    console.log('pass', password_correct);
                    if (email_correct) {
                        button.removeAttr('disabled');
                    }
                } else {
                    button.attr('disabled', true);
                    password_correct = false;
                }
            });
        });
    </script>
@stop