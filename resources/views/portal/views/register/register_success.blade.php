@extends('portal.layouts.main')
@section('title')
    @lang('portal/register/success.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/register/success.description')">
    <meta name="author" content="@lang('portal/register/success.author')">
    <meta name="keywords" content="@lang('portal/register/success.description')">
@stop
@section('content')
    @include('portal.views.partials.register.section_success')
    @include('portal.views.partials.register.section_8')
@stop
@section('modals')
    @parent
    <!-- Modals
================================================== -->
    @include('portal.views.partials.modals.timer')

@stop