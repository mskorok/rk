@extends('portal.layouts.main')
@section('title')
    @lang('portal/register/ecard.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/register/ecard.description')">
    <meta name="author" content="@lang('portal/register/ecard.author')">
    <meta name="keywords" content="@lang('portal/register/ecard.keywords')">
@stop
@section('css')
    @parent
    <!-- Libraries styles -->
    <link href="{{ asset('assets/portal/css/bootstrap-imageupload.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/select2.min.css').'?'.time() }}" rel="stylesheet">
@stop
@section('content')
    <!-- Register Steps -->
    <div id="register-steps">        
        @include('portal.views.partials.register.section_step_1')
    </div>
    <!-- End Register Steps -->
@stop
@section('modals')
    <!-- Modals
================================================== -->
    @include('portal.views.partials.modals.pass_recovery')
    @include('portal.views.partials.modals.pass_recovery_success')
    @include('portal.views.partials.modals.rules')
    @include('portal.views.partials.modals.warning')
    @include('portal.views.partials.modals.reg_ecard_help')
    @include('portal.views.partials.modals.profile_removed')
    @include('portal.views.partials.modals.timer')
@stop
@section('js')
    @parent
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="{{ asset('assets/portal/js/bootstrap-imageupload.min.js').'?'.time() }}"></script>
    <script src="{{ asset('assets/portal/js/select2.min.js').'?'.time() }}"></script>
@stop
@section('script')
    <script type="text/javascript">
        var save_url = '{{ route('save_card') }}';
        var save_payment_url = '{{ route('save_fill_card') }}';
        var amount_form_validation = {
            valid: function () {
                var selectors = amount_form_validation.valid_banners();
                var result = true;
                for (var n = 0; n < selectors.length; n++) {
                    var selector = selectors[n];
                    result = result && form_errors[selector];
                }
                return result;
            },
            form_valid: function () {
                if (amount_form_validation.valid()) {
                    custom_functions.enable_button('#next-step-approve .btn');
                } else {
                    custom_functions.disable_button('#next-step-approve .btn');
                }
            },
            valid_banners: function () {
                var input = document.getElementById('amt');
                if (input) {
                    var amount = parseInt(input.value);
                    if (amount > 0) {
                        var result = [];
                        for (var i =1; i <= amount; i++) {
                            result.push('top_up_sum_'+i);
                        }
                        return result;
                    }
                }
                return [];
            },
            valid_item_selectors: function (i) {
                var result = [];
                result.push('input[name=top_up_sum_' + i + ']');
                return result;
            }
        };
        var form_validation = {
            valid_banners: function () {
                var input = document.getElementById('amt');
                if (input) {
                    var amount = parseInt(input.value);
                    if (amount > 0) {
                        var result = [];
                        for (var i =1; i <= amount; i++) {
                            for (var j = 1; j < 11; j++) {
                                result.push('n' + i+'_card_number_'+j);
                            }
                            for (var k = 1; k < 12; k++) {
                                result.push('n' + i+'_pk_'+k);
                            }
                            result.push('n' + i+'_period');
                            result.push('n' + i+'_school');
                            result.push('n' + i+'_name');
                            result.push('n' + i+'_surname');
                            result.push('n' + i+'_class');
                            result.push('n' + i+'_amount');
                        }
                        return result;
                    }
                }
                return [];
            },
            valid_selectors: function () {
                var input = document.getElementById('amt');
                if (input) {
                    var amount = parseInt(input.value);
                    if (amount > 0) {
                        var result = [];
                        for (var i =1; i <= amount; i++) {
                            for (var j = 1; j < 11; j++) {
                                result.push('input[name=n' + i+'_card_number_'+j + ']');
                            }
                            for (var k = 1; k < 12; k++) {
                                result.push('input[name=n' + i+'_pk_'+k + ']');
                            }
                            result.push('select[name=n' + i+'_period]');
                            result.push('select[name=n' + i+'_school]');
                            result.push('input[name=n' + i+'_name]');
                            result.push('input[name=n' + i+'_surname]');
                            result.push('input[name=n' + i+'_class]');
                            result.push('input[name=n' + i+'_amount]');
                        }
                        return result;
                    }
                }
                return [];
            },
            valid_item_selectors: function (i) {
                var result = [];
                for (var j = 1; j < 11; j++) {
                    result.push('input[name=n' + i+'_card_number_'+j + ']');
                }
                for (var k = 1; k < 12; k++) {
                    result.push('input[name=n' + i+'_pk_'+k + ']');
                }
                result.push('select[name=n' + i+'_period]');
                result.push('select[name=n' + i+'_school]');
                result.push('input[name=n' + i+'_name]');
                result.push('input[name=n' + i+'_surname]');
                result.push('input[name=n' + i+'_class]');
                result.push('input[name=n' + i+'_amount]');
                return result;
            },
            form_valid: function () {
                if (form_validation.valid()) {
                    custom_functions.enable_button('#next-step .btn');
                } else {
                    custom_functions.disable_button('#next-step .btn');
                }
            },
            valid: function () {
                var selectors = form_validation.valid_banners();
                var result = true;
                for (var n = 0; n < selectors.length; n++) {
                    var selector = selectors[n];
                    result = result && form_errors[selector];
                }
                return result;
            }
        };
        var e_card_fnc = {
            validate_amount_item: function (i) {
                var input = document.querySelector('input[name=top_up_sum_'+i +']');
                if (input) {
                    custom_functions.validateInput(
                            'input[name=top_up_sum_'+i +']',
                            '#next-step-approve .btn',
                            null,
                            custom_functions.validateCard,
                            'top_up_sum_' + i
                    );
                    var selectors = amount_form_validation.valid_item_selectors(i);
                    custom_functions.validateForm(selectors, amount_form_validation.form_valid);
                    amount_form_validation.form_valid();
                }
            },
            validate_e_card_item: function (i) {
                for (var j = 1; j < 11; j++) {
                    custom_functions.validateInput(
                            'input[name=n'+i+'_card_number_'+j+']',
                            '#next-step .btn',
                            null,
                            custom_functions.validateNumericWithNull,
                            'n' + i+'_card_number_'+j
                    );
                }
                for (var k = 1; k < 12; k++) {
                    custom_functions.validateInput(
                            'input[name=n'+i+'_pk_'+k+']',
                            '#next-step .btn',
                            null,
                            custom_functions.validateNumericWithNull,
                            'n' + i+'_pk_'+k
                    );
                }
                custom_functions.validateInput(
                        'input[name=n'+i+'_name]',
                        '#next-step .btn',
                        null,
                        custom_functions.validateString,
                        'n' + i+'_name'
                );
                custom_functions.validateInput(
                        'input[name=n'+i+'_surname]',
                        '#next-step .btn',
                        null,
                        custom_functions.validateString,
                        'n' + i+'_surname'
                );
                custom_functions.validateInput(
                        'input[name=n'+i+'_class]',
                        '#next-step .btn',
                        null,
                        custom_functions.validateClass,
                        'n' + i+'_class'
                );
                custom_functions.validateInput(
                        'input[name=n'+i+'_amount]',
                        '#next-step .btn',
                        null,
                        custom_functions.validateNumeric140,
                        'n' + i+'_amount'
                );
                setTimeout(function () {
                    custom_functions.validateSelect(
                            '#n'+i+'_period',
                            '#next-step .btn',
                            'n' + i+'_period'
                    );
                    custom_functions.validateSelect(
                            '#n'+i+'_school',
                            '#next-step .btn',
                            'n' + i+'_school'
                    );
                    var selectors = form_validation.valid_item_selectors(i);
                    custom_functions.validateForm(selectors, form_validation.form_valid);
                    form_validation.form_valid();
                }, 200);
            },
            e_card_set_template: function () {
                var container = document.getElementById('e_card_container');
                if (!container) {
                    return false;
                }
                var input = document.getElementById('amt');
                var i = parseInt(input.value);
                i += 1;
                var bg_class = (i & 1) ?  'accent' : 'light-accent';
                var schools = '@include('portal.views.partials.register.schools_js')';
                var modal_add_child = '<div class="modal fade modal-add-child-photo js-first-modal" id="modal-add-child-photo_' + i + '" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">' +
                        '<div class="modal-dialog modal-sm" style="width: auto; max-width: 700px;">' +
                        '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<span>' + '{{ $trans['add_child_photo'] }}' +'</span>' +
                        '<span class="close" data-dismiss="modal"><img src="' + '{{ asset('assets/portal/img/close.png') }}' + '" alt=""></span>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        '<div class="row" style="color: #064874;">' +
                        '<div class="col-sm-6">' +
                        '<h4>' + '{{ $trans['download_child_photo'] }}' + '</h4>' +
                        '<div class="vignette placeholder" id="add_photo_image_' + i + '">' +
                        '</div>' +
                        '<div style="width: 300px; height: 1px; position: relative">' +
                        '<div class="vignette button-placeholder" style="position: absolute; top: -40px;">' +
                        '<div class="vignette zoom-out" id="zoom_out_' + i +'">-</div>' +
                        '<div class="vignette zoom-in" id="zoom_in_' + i + '">+</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-6">' +
                        '<ul class="nav nav-pills nav-justified">' +
                        '<li class="active"><a href="#boys_' + i + '" data-toggle="tab">' +
                        '{{ $trans['add_child_photo_boys'] }}' +
                        '</a></li>' +
                        '<li><a href="#girls_' + i + '" data-toggle="tab">' +
                        '{{ $trans['add_child_photo_girls'] }}' +
                        '</a></li>' +
                        '</ul>' +
                        '<div class="tab-content" style="margin-top: 20px;">' +
                        '<div class="tab-pane fade in active avatar-grid" id="boys_' + i + '">' +
                        '<div class="row">' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy101.png') }}' + '" id="boys_1_' + i + '" class="active" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy02.png') }}' + '" id="boys_2_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy03.png') }}' + '" id="boys_3_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy04.png') }}' + '" id="boys_4_' + i + '" /></div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy05.png') }}' + '" id="boys_5_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy06.png') }}' + '" id="boys_6_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy07.png') }}' + '" id="boys_7_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy08.png') }}' + '" id="boys_8_' + i + '" /></div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy09.png') }}' + '" id="boys_9_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy10.png') }}' + '" id="boys_10_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy11.png') }}' + '" id="boys_11_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy12.png') }}' + '" id="boys_12_' + i + '" /></div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy13.png') }}' + '" id="boys_13_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy14.png') }}' + '" id="boys_14_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy15.png') }}' + '" id="boys_15_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/boys/boy16.png') }}' + '" id="boys_16_' + i + '" /></div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="tab-pane avatar-grid" id="girls_' + i + '">' +
                        '<div class="row">' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl01.png') }}' + '" id="girls_1_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl02.png') }}' + '" id="girls_2_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl03.png') }}' + '" id="girls_3_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl04.png') }}' + '" id="girls_4_' + i + '" /></div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl05.png') }}' + '" id="girls_5_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl06.png') }}' + '" id="girls_6_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl07.png') }}' + '" id="girls_7_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl08.png') }}' + '" id="girls_8_' + i + '" /></div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl09.png') }}' + '" id="girls_9_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl10.png') }}' + '" id="girls_10_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl11.png') }}' + '" id="girls_11_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl12.png') }}' + '" id="girls_12_' + i + '" /></div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl13.png') }}' + '" id="girls_13_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl14.png') }}' + '" id="girls_14_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl15.png') }}' + '" id="girls_15_' + i + '" /></div>' +
                        '<div class="col-sm-3"><img src="' + '{{ asset('assets/portal/img/avatari/girls/girl16.png') }}' + '" id="girls_16_' + i + '" /></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="modal-footer" style="padding: 15px 25px;">' +
                        '<div style="float: left;">' +
                        '<div class="btn-group" id="upload_photo_buttons_' + i + '">' +
                        '<button type="button" class="btn btn-sm btn-blue">' +
                        '{{ $trans['add_image'] }}' +
                        '</button>' +
                        '<button type="button" class="btn btn-sm btn-siel"><img src="' + '{{ asset('assets/portal/img/exit_icon.png') }}' + '" alt=""></button>' +
                        '</div>' +
                        '<div><input type="file" id="modal_upload_' + i +'" name="modal_upload_' + i +'" style="display: none;"/></div>' +
                        '</div>' +
                        '<div style="float: left; margin-left: 50px;">' +
                        '<span id="allowed_formats_' + i +'">' +
                        '{{ $trans['allowed_formats'] }}.' +
                        '</span>' +
                        '<br/>' +
                        '<span id="max_image_size_' + i + '">' +
                        '{{ $trans['max_image_size'] }}' +
                        '</span>' +
                        '</div>' +
                        '<div style="position: relative; display: none;" id="add_photo_errors_' + i + '">' +
                        '<div style="float: left; margin-left: 20px; margin-right: 5px; padding: 5px;">' +
                        '<img  src="' + '{{ asset('assets/portal/img/danger.png') }}' + '" style="width: 24px; height: 24px;">' +
                        '</div>' +
                        '<div style="float: left; text-align: left;">' +
                        '<div style="line-height: 16px; color: red;">' +
                        '{!! $trans['add_child_photo_error'] !!}' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div style="float: right;">' +
                        '<div class="btn-group" id="add_photo_buttons_' + i + '">' +
                        '<button type="button" class="btn btn-sm btn-blue">' +
                        '{{ $trans['add_child_photo_save'] }}' +
                        '</button>' +
                        '<button type="button" class="btn btn-sm btn-green">' +
                        '<img src="' + '{{ asset('assets/portal/img/exit_icon.png') }}' + '" alt="">' +
                        '</button>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                var html = '<input type="hidden" name="child_photo_' + i + '" id="child_photo_' + i + '">' +
                        '<div class="content row ' + bg_class + '" id="person_' + i + '">' +
                              '<div class="form-group pale">' +
                                  '<div class="col-sm-12">' +
                                      '<div class="content row stack">' +
                                          '<div class="col-sm-9">' +
                                              '<div class="content row stack">' +
                                                  '<div class="col-sm-4">' +
                                                      '<div class="box-container switch-next" data-name="n' + i + '_card_number">' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_1" data-id="1" id="n' + i + '_card_number_1"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_2" data-id="2" id="n' + i + '_card_number_2"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_3" data-id="3" id="n' + i + '_card_number_3"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_4" data-id="4" id="n' + i + '_card_number_4"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_5" data-id="5" id="n' + i + '_card_number_5"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_6" data-id="6" id="n' + i + '_card_number_6"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_7" data-id="7" id="n' + i + '_card_number_7"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_8" data-id="8" id="n' + i + '_card_number_8"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_9" data-id="9" id="n' + i + '_card_number_9"></div>' +
                                                          '<div><input type="text" class="form-control input" name="n' + i + '_card_number_10" data-id="10" id="n' + i + '_card_number_10"></div>' +
                                                      '</div>' +
                                                      '<div class="btn-group-note">' +
                                                          "{{ $trans['e_card_number'] }}" +
                                                          '(<a href="#" class="e-card-help">' + "{{ $trans['how_to_find'] }}" + '</a>)' +
                                                      '</div>'+
                                                      '<div class="error-message">' +
                                                          '<img src="{{ asset('assets/portal/img/danger.png') }}" alt="">' +
                                                          '{{ $trans['field_required'] }}' +
                                                      '</div>' +
                                                  '</div>' +
                                              '<div class="col-lg-5 col-xs-12 col-sm-8">' +
                                          '<div class="col-sm-8 col-xs-12" style="padding-left: 0">' +
                                              '<select class="select2-select limits" style="width: 100%;" name="n' + i + '_period" id="n' + i + '_period">' +
                                                  '<option></option>' +
                                                  '<option value="day">' + "{{ $trans['daily'] }}</option>" +
                                                  '<option value="week">' + "{{ $trans['weekly'] }}</option>" +
                                                  '<option value="month">' + "{{ $trans['monthly'] }}</option>" +
                                              '</select>' +
                                              '<div class="error-message">' +
                                                  '<img src="{{ asset('assets/portal/img/danger.png') }}" alt="">' +
                                                  '{{ $trans['field_required'] }}' +
                                              '</div>' +
                                          '</div>' +
                                          '<div class="col-sm-4 col-xs-12" style="padding-right: 0">' +
                                              '<input type="number" class="form-control input" name="n' + i +'_amount"  placeholder="0.00" step="0.01">' +
                                              '<div class="error-message">' +
                                                  '<img src="{{ asset('assets/portal/img/danger.png') }}" alt="">' +
                                                  '{{ $trans['max_140'] }}' +
                                                  '</div>' +
                                              '</div>' +
                                      '</div>' +
                                  '</div>' +
                                  '<div class="content row stack">' +
                                      '<div class="col-sm-4 col-xs-6">' +
                                          '<input type="text" class="form-control input" name="n' + i + '_name"  placeholder="' + "{{ $trans['name'] }}" +'">' +
                                          '<div class="error-message">' +
                                              '<img src="{{ asset('assets/portal/img/danger.png') }}" alt="">' +
                                              '{{ $trans['field_required'] }}' +
                                          '</div>' +
                                      '</div>' +
                                      '<div class="col-sm-4 col-xs-6">' +
                                          '<input type="text" class="form-control input" name="n' + i +'_surname"  placeholder="' + "{{ $trans['surname'] }}" + '">' +
                                          '<div class="error-message">' +
                                              '<img src="{{ asset('assets/portal/img/danger.png') }}" alt="">' +
                                              '{{ $trans['field_required'] }}' +
                                          '</div>' +
                                      '</div>' +
                                  '<div class="col-sm-4 col-xs-12">' +
                                      '<div class="box-container p-code switch-next" data-name="n' + i + '_pk">' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_1" data-id="1" id="n' + i + '_pk_1"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_2" data-id="2" id="n' + i + '_pk_2"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_3" data-id="3" id="n' + i + '_pk_3"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_4" data-id="4" id="n' + i + '_pk_4"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_5" data-id="5" id="n' + i + '_pk_5"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_6" data-id="6" id="n' + i + '_pk_6"></div>' +
                                          '<div></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_7" data-id="7" id="n' + i + '_pk_7"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_8" data-id="8" id="n' + i + '_pk_8"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_9" data-id="9" id="n' + i + '_pk_9"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_10" data-id="10" id="n' + i + '_pk_10"></div>' +
                                          '<div><input type="text" class="form-control input" name="n' + i + '_pk_11" data-id="11" id="n' + i + '_pk_11"></div>' +
                                      '</div>' +
                                      '<div class="btn-group-note">' + "{{ $trans['pk'] }}</div>" +
                                      '<div class="error-message">' +
                                          '<img src="{{ asset('assets/portal/img/danger.png') }}" alt="">' +
                                          '{{ $trans['field_required'] }}' +
                                      '</div>' +
                                  '</div>' +
                              '</div>' +
                              '<div class="content row">' +
                                  '<div class="col-sm-4 col-xs-12">' +
                                      '<select class="select2-select schools" style="width: 100%;" name="n' + i + '_school" id="n' + i + '_school">' +
                                      schools +
                                      '</select>' +
                                      '<div class="error-message">' +
                                          '<img src="{{ asset('assets/portal/img/danger.png') }}" alt="">' +
                                          '{{ $trans['field_required'] }}' +
                                      '</div>' +
                                  '</div>' +
                                  '<div class="col-sm-4 col-xs-12">' +
                                      '<input type="text" class="form-control input" name="n' + i + '_class"  placeholder="' + "{{ $trans['class'] }}" + '">' +
                                      '<div class="error-message">' +
                                          '<img src="{{ asset('assets/portal/img/danger.png') }}" alt="">' +
                                          '{{ $trans['field_required'] }}' +
                                      '</div>' +
                                  '</div>' +
                              '</div>' +
                        '</div>' +
                        '<div class="col-sm-3 col-xs-12">' +
                            '<div class="large-padding" style="position: relative; padding-top: 50%;">' +
                                '<div class="btn-group small-btn-group pale" id="btn-add-new-photo_' + i + '" data-id="' + i + '">' +
                                    '<button type="button" class="btn btn-sm btn-blue" data-id="' + i + '">' +
                                        "{{ $trans['add_child_photo_1'] }}" +
                                    '</button>' +
                                    '<button type="button" class="btn btn-sm btn-siel ico" data-id="' + i + '"><svg aria-hidden="true" class="icon icon-row-table "><use xlink:href="{{ asset('assets/portal/css/images/svg_icons/icons.svg#button-upload') }}"></use></svg></button>' +
                                '</div>' +
                                '<div class="btn-group-note">' +
                                    '{{ $trans['allowed_formats'] }}.' +
                                    '<br />' +
                                    '{{ $trans['max_image_size'] }}' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
    '</div>';
                var span = document.createElement('span');
                span.innerHTML = html + modal_add_child;
//                container.insertAdjacentHTML('beforeend', html);
                container.appendChild(span);
                setTimeout(function () {
                    $('#n' + i + '_period').select2({
                        minimumResultsForSearch: Infinity,
                        // allowClear: true,
                        placeholder: "{!! $trans['e_card_limit'] !!}"
                    });

                    $('#n' + i + '_school').select2({
                        // allowClear: true,
                        placeholder: "{{ $trans['select_school'] }}"
                    });

                    input.value = i;
                    e_card_fnc.validate_e_card_item(i);

//                    var selector = '#modal-add-child-photo_' + i;
                    $('#btn-add-new-photo_' + i + ' .btn').click(function() {
                        var i = $(this).attr('data-id');
                        var selector = '#modal-add-child-photo_' + i;
                        $(selector).modal('show');
                    });
                    (function () {
                        var csrf_token = '{{ csrf_token() }}';
                        var avatar_url = '{{ route('create_avatar') }}';
                        var type = parseInt('{{ AVATAR_TYPE_CARD }}');
                        var id = 0;

                        // card or user id, type(card/user),  src from crop
                        function create_avatar(id, type, src) {
                            var api_token = document.getElementById('api_token');
                            var url = avatar_url+'?api_token='+api_token.value;
//                            var url = avatar_url;
                            var form_data = new FormData();
                            form_data.append('type', type);
                            form_data.append('id', id);
                            form_data.append('src', src);
                            form_data.append('_token', csrf_token);
                            var response;
                            var xhr = new XMLHttpRequest();
                            xhr.onload = function() {
                                if (this.readyState === 4) {
                                    if (this.status === 200) {
                                        try {
                                            response = JSON.parse(this.response);
                                            if (response.success) {
                                                console.log('success ajax add photo', i, response);
                                                create_avatar_handler(response);
                                                $('.modal-add-child-photo').modal('hide');
                                            } else if (response.error) {
                                                console.log('error ajax add photo', response.error);
                                            }
                                        } catch (e) {
                                            console.log('error', e.message);
                                        }
                                    } else {
                                        console.log('error response', this.response);
                                    }
                                }
                            };
                            xhr.open('POST', url, true);
                            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                            xhr.send(form_data);
                        }
                        function create_avatar_handler(response) {
                            var selectors = getSelectors();
                            var inputs = getInputs();
                            if (selectors) {
                                [].forEach.call(selectors, function (selector) {
                                    if (selector.hasAttribute('src')) {
                                        selector.src = response.path;
                                        selector.style.display = 'block';
                                    }
                                })
                            }
                            if (inputs) {
                                [].forEach.call(inputs, function (input) {
                                    if (input.tagName === 'INPUT') {
                                        input.value = response.id;
                                    }
                                })
                            }
                        }
                        function getSelectors() {
                            var selectors, result = [];
                            if (type === parseInt('{{ AVATAR_TYPE_USER }}')) {
                                selectors = ['#navbar_avatar', '#add_new_profile_photo', '#edit_profile_photo'];
                                [].forEach.call(selectors, function (selector) {
                                    var el = document.querySelector(selector);
                                    if (el) {
                                        result.push(el);
                                    }
                                });
                                return result;

                            } else if (type === parseInt('{{ AVATAR_TYPE_CARD }}')) {
                                selectors = [
                                    '#new_card_photo_'+i,
                                    '#edit_card_photo_'+i,
                                    '#child_photo_' + i
                                ];
                                [].forEach.call(selectors, function (selector) {
                                    var el = document.querySelector(selector);
                                    if (el) {
                                        result.push(el);
                                    }
                                });
                                return result;
                            } else if (type === parseInt('{{ AVATAR_TYPE_ETICKET }}')) {
                                selectors = [
                                    '#edit_child_1_' + i,
                                    '#edit_child_2_' + i,
                                    '#edit_child_3_' + i
                                ];
                                [].forEach.call(selectors, function (selector) {
                                    var el = document.querySelector(selector);
                                    if (el) {
                                        result.push(el);
                                    }
                                });
                                return result;
                            }  else {
                                return null;
                            }
                        }
                        function getInputs() {
                            var selectors, result = [];
                            if (type === parseInt('{{ AVATAR_TYPE_USER }}')) {
                                return null;

                            } else if (type === parseInt('{{ AVATAR_TYPE_CARD }}')) {
                                selectors = ['#child_photo_' + i];
                                [].forEach.call(selectors, function (selector) {
                                    var el = document.querySelector(selector);
                                    if (el) {
                                        result.push(el);
                                    }
                                });
                                return result;
                            } else if (type === parseInt('{{ AVATAR_TYPE_ETICKET }}')) {
                                selectors = ['#edit_child_photo_' + i];
                                [].forEach.call(selectors, function (selector) {
                                    var el = document.querySelector(selector);
                                    if (el) {
                                        result.push(el);
                                    }
                                });
                                return result;
                            } else {
                                return null;
                            }
                        }
                        function removeActive() {
                            for (var j=1; j < 17; j++) {
                                var boy = document.getElementById('boys_' + j + '_' + i);
                                if (boy && boy.classList.contains('active')) {
                                    boy.classList.remove('active');
                                }
                                var girl = document.getElementById('girls_' + j + '_' + i);
                                if (girl && girl.classList.contains('active')) {
                                    girl.classList.remove('active');
                                }
                            }
                        }

                        var crop = new Croppie(document.getElementById('add_photo_image_' + i), crop_opts);
//                        var first_boy = document.getElementById('boys_' + 1 + '_' + i);
//                        var first_url = first_boy.src;
//                        crop.bind({
//                            url: first_url
//                        });
//                        $('.cr-image').css('opacity', '1');
                        $('#add_photo_buttons_' + i).click(function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            crop.result('rawcanvas').then(function(html) {
                                html.setAttribute('id', 'canvas');
                                html.setAttribute('crossOrigin', 'anonymous');
                                try {
                                    var img = html.toDataURL();
                                    var src = img.split(',')[1];
                                    create_avatar(id, type, src);
                                } catch (e) {
                                    console.log('e', e);
                                }
                            });
                        });
                        var up_button = document.getElementById('zoom_in_' +i);
                        if (up_button) {
                            up_button.addEventListener('click', function (e) {
                                e.preventDefault();
                                e.stopPropagation();
                                crop.up();
                            })
                        }

                        var down_button = document.getElementById('zoom_out_' + i);
                        if (down_button) {
                            down_button.addEventListener('click', function (e) {
                                e.preventDefault();
                                e.stopPropagation();
                                crop.down();
                            })
                        }
                        for (var j=1; j < 17; j++) {
                            var boy = document.getElementById('boys_' + j + '_' + i);
                            if (boy) {
                                boy.addEventListener('click', function () {
                                    $('#add_photo_buttons_' + i + ' .btn').removeAttr('disabled');
                                    var allowed_formats = document.getElementById('allowed_formats_' + i);
                                    var max_image_size = document.getElementById('max_image_size_' + i);
                                    if (allowed_formats) {
                                        allowed_formats.style.color = '#064874';
                                    }
                                    if (max_image_size) {
                                        max_image_size.style.color = '#064874';
                                    }
                                    removeActive();
                                    this.classList.add('active');
                                    var url = this.src;
                                    crop.bind({
                                        url: url
                                    })
                                });

                            }
                            var girl = document.getElementById('girls_' + j + '_' + i);
                            if (girl) {
                                girl.addEventListener('click', function () {
                                    $('#add_photo_buttons_' + i + ' .btn').removeAttr('disabled');
                                    var allowed_formats = document.getElementById('allowed_formats_' + i);
                                    var max_image_size = document.getElementById('max_image_size_' + i);
                                    if (allowed_formats) {
                                        allowed_formats.style.color = '#064874';
                                    }
                                    if (max_image_size) {
                                        max_image_size.style.color = '#064874';
                                    }
                                    removeActive();
                                    this.classList.add('active');
                                    var url = this.src;
                                    crop.bind({
                                        url: url
                                    })
                                });
                            }
                        }
                        document.getElementById('modal_upload_' + i).onchange = function () {
                            var extension = this.files[0].name;
                            extension = extension.split('.').pop();
                            var size = this.files[0].size;
                            if (this.files && this.files[0]) {
                                var reader = new FileReader();
                                reader.addEventListener('load', function(e) {
                                    var url = e.target.result;
                                    crop.bind({
                                        url: url
                                    })
                                });
                                reader.readAsDataURL(this.files[0]);
                            }
                            var upload_errors = document.getElementById('add_photo_errors_' + i);
                            removeActive();
                            $('#add_photo_buttons_' + i + ' .btn').attr('disabled', true);
                            if (upload_errors) {
                                upload_errors.style.display = 'none';
                            }
                            var allowed_formats = document.getElementById('allowed_formats_' + i);
                            var max_image_size = document.getElementById('max_image_size_' + i);
                            if (['png', 'jpg'].indexOf(extension.toLowerCase()) !== -1) {
                                if (allowed_formats) {
                                    allowed_formats.style.color = '#064874';
                                }
                            } else {
                                if (allowed_formats) {
                                    allowed_formats.style.color = 'red';
                                }
                            }
                            if (parseInt(size) < 4000000) {
                                if (max_image_size) {
                                    max_image_size.style.color = '#064874';
                                }
                            } else {
                                if (max_image_size) {
                                    max_image_size.style.color = 'red';
                                }
                            }
                            if (['png', 'jpg'].indexOf(extension.toLowerCase()) !== -1 && parseInt(size) < 4000000) {
                                $('#add_photo_buttons_' + i + ' .btn').removeAttr('disabled');
                            }
                        };

                        $('#upload_photo_buttons_' + i + ' .btn').click(function () {
                            $('#modal_upload_' + i).click();
                        });






//                        document.addEventListener('DOMContentLoaded', function () {
//
//                        });
                    })(i);
                }, 300);
            }
        };
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $('#next-step .btn').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            var form = document.getElementById('ajax_add_card_form');
            if (form) {
                form.submit();
            }
        });




        $(document).ready(function() {


            var icones = document.getElementById('icones');
            icones.addEventListener('click', function (e) {
                var parent = icones.parentNode;
                if (parent.hasAttribute('disabled')) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            });

            $('.modal-add-child-photo').modal('hide');

            $('#add-person .btn').click(function() {
                e_card_fnc.e_card_set_template();
            });

            $('#next-step-approve .btn').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                console.log(123);
                var form = document.getElementById('refill_cards_form');
                if (form) {
                    form.submit();
                }
            });

            $('#save-cards').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                var form = document.getElementById('ajax_add_card_form');
                if (form) {
                    form.setAttribute('action', save_url);
                    form.submit();
                }
            });

            $('#save-payments').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                var form = document.getElementById('refill_cards_form');
                if (form) {
                    form.setAttribute('action', save_payment_url);
                    form.submit();
                }
            });

            $('#interrupt_top_up').click(function (e) {
                window.location.assign('{{ route('interrupt_fill_card') }}');
            });

            $('.select2-select.limits').select2({
                minimumResultsForSearch: Infinity,
                // allowClear: true,
                placeholder: "{!! $trans['e_card_limit'] !!}"
            });

            $('.select2-select.schools').select2({
                // allowClear: true,
                placeholder: "{{ $trans['select_school'] }}"
            });


            $('.avatar-grid .row .col-sm-3 img').click(function() {
                $('.avatar-grid .row .col-sm-3 img.active').removeClass('active');
                $(this).addClass('active');
            });

            var modal = getUrlParameter('modal');
            if (modal) {
                console.log(modal);
                if (modal == 'modal-rules') $('.modal-rules').modal('show');
                if (modal == 'modal-warning-1') $('.modal-warning-1').modal('show');
                if (modal == 'modal-add-new-photo') $('.modal-add-new-photo').modal('show');
                if (modal == 'modal-reg-ecard-help') $('.modal-reg-ecard-help').modal('show');
                if (modal == 'modal-add-child-photo') $('.modal-add-child-photo').modal('show');
                if (modal == 'modal-profile-removed') $('.modal-profile-removed').modal('show');
            }

            $('.btn-group#next-step-close').click(function() {
                window.location.assign('{{ route('filled_card') }}');
            });

            $('a.card-code').click(function() {
                $('.modal-card-code-help').modal('show');
            });

            {{--$('.btn-group.signin').click(function() {--}}
                {{--window.location.assign('{{ route('dashboard') }}');--}}
            {{--});--}}

            $('.box-container .form-control.input').keyup(function(event) {
                //

            }).keydown(function(event) {
                if (event.which == 13 || event.which == 32) {
                    event.preventDefault();
                } else
                if ($(this).val().length > 0 && (event.which != 8 && event.which != 46 && event.which != 9)) {
                    event.preventDefault();
                }

            });
        });
    </script>
@stop