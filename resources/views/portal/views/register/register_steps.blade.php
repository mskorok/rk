@extends('portal.layouts.main')
@section('title')
    @lang('portal/register/steps.title')
@stop
@section('meta')
    <meta name="description" content="@lang('portal/register/steps.description')">
    <meta name="author" content="@lang('portal/register/steps.author')">
    <meta name="keywords" content="@lang('portal/register/steps.keywords')">
@stop
@section('css')
    @parent
    <!-- Libraries styles -->
    <link href="{{ asset('assets/portal/css/bootstrap-imageupload.min.css').'?'.time() }}" rel="stylesheet">
    <link href="{{ asset('assets/portal/css/select2.min.css').'?'.time() }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/portal/css/jquery.dataTables.reset.css').'?'.time() }}" rel="stylesheet">

@stop
@section('content')
    <!-- Register Steps -->
    <div id="register-steps">
        <form id="register_steps_form" method="post" action="{{ route('registration_profile') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('portal.views.partials.register.steps_section_1')
            @include('portal.views.partials.register.steps_section_2')
            @include('portal.views.partials.register.steps_section_3')
        </form>
    </div>
    <!-- End Register Steps -->
    @include('portal.views.partials.register.section_8')
@stop
@section('modals')
    <!-- Modals
================================================== -->
    @parent
    @include('portal.views.partials.modals.rules')
    @include('portal.views.partials.modals.warning')
    @include('portal.views.partials.modals.add_child_photo')
    @include('portal.views.partials.modals.timer')
    @include('portal.views.partials.modals.bank_timer')

@stop
@section('js')
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    @parent
    <script src="{{ asset('assets/portal/js/bootstrap-imageupload.min.js') }}"></script>
    <script src="{{ asset('assets/portal/js/select2.min.js') }}"></script>
    <script src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>    
    <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
@stop
@section('script')
    <script type="text/javascript">
        
        moment.locale('{{ App::getLocale() }}');
        var monthNames = moment.months();
        var daysOfWeek = moment.weekdaysShort(); 
        var locale = {
            format: 'YYYY-MM-DD',
            daysOfWeek: daysOfWeek,
            monthNames: monthNames,            
            firstDay: 1
        };
        
        var save_url = '{{ route('profile_save') }}';
        var redirect_url = '{{ route('landing') }}';
        var is_disable = false;
        var bank_timer = '{{ $bankTimer }}';
        var submit_button_selector = '#register_profile_button .btn';
        var photo_attached = @if($photoAttached) true; @else false; @endif
        var profile_form = {
            doc_image: photo_attached,
            other_funds: true,
            email: true,
            phone: false,
            address: false,
            citizenship: false,
            doc_type: false,
            doc_number: false,
            document_date: false,
            has_status: false,
            social_status: false,
            income_month: false,
            income_year: false,
            outgoing_month: false,
            income_amount: false,
            outgoing_year: false,
            outgoing_amount: false,
            is_manager: true,
            manager_status: false,
            is_owner: true,
            owner_status: false,
            funds_origin: false,
            terms: false,
            valid: function () {
                var valid = this.doc_image
                        && this.other_funds
                        && this.email
                        && form_errors.phone
                        && form_errors.address
                        && form_errors.citizenship
                        && form_errors.doc_type
                        && form_errors.doc_number
                        && form_errors.document_date
                        && (!this.has_status || (this.has_status && form_errors.social_status))
                        && form_errors.income_month
                        && form_errors.income_year
                        && form_errors.income_amount
                        && form_errors.outgoing_month
                        && form_errors.outgoing_year
                        && form_errors.outgoing_amount
                        && form_errors.funds_origin
                        && (this.is_manager || (!this.is_manager && form_errors.manager_status))
                        && (this.is_owner || (!this.is_owner && form_errors.owner_status))
                        && this.terms
                ;
                if (valid === false) {
                    var terms_checkbox = $('#is_accept_terms');
                    terms_checkbox[0].checked = false;
                    this.terms = false;
                }
//                console.log('valid', valid, this.terms);
                return valid;
            },
            form_valid: function () {
                if (profile_form.valid()) {
                    $(submit_button_selector).removeAttr('disabled');
                    $('#ajax_error')[0].textContent = '';
                } else {
                    $(submit_button_selector).attr('disabled', true);
                    $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    var terms_checkbox = $('#is_accept_terms');
                    terms_checkbox[0].checked = false;
                }
            },
            show_errors: function () {
                if (!form_errors.phone) {
                    custom_functions.show_error($('input[name=phone]')[0]);
                }
                if (!form_errors.address) {
                    custom_functions.show_error($('input[name=address]')[0]);
                }
                if (!form_errors.citizenship) {
                    custom_functions.show_error($('select[name=citizenship]')[0]);
                }
                if (!form_errors.doc_type) {
                    custom_functions.show_error($('select[name=doc_type]')[0]);
                }
                if (!form_errors.funds_origin) {
                    custom_functions.show_error($('select[name=funds_origin]')[0]);
                }
                if (!form_errors.doc_number) {
                    custom_functions.show_error($('input[name=doc_number]')[0]);
                }
                if (!form_errors.document_date) {
                    custom_functions.show_error($('input[name=document_date]')[0]);
                }
                if (!form_errors.date_expired) {
                    custom_functions.show_error($('input[name=document_date]')[0], 1);
                }
                if (!form_errors.income_month) {
                    custom_functions.show_error($('input[name=income_month]')[0]);
                }
                if (!form_errors.income_year) {
                    custom_functions.show_error($('input[name=income_year]')[0]);
                }
                if (!form_errors.income_amount) {
                    custom_functions.show_error($('input[name=income_amount]')[0]);
                }
                if (!form_errors.outgoing_month) {
                    custom_functions.show_error($('input[name=outgoing_month]')[0]);
                }
                if (!form_errors.outgoing_year) {
                    custom_functions.show_error($('input[name=outgoing_year]')[0]);
                }
                if (!(!this.has_status || (this.has_status && form_errors.social_status))) {
                    custom_functions.show_error($('input[name=social_status]')[0]);
                }
                if (!(this.is_manager || (!this.is_manager && form_errors.manager_status))) {
                    custom_functions.show_error($('input[name=manager_status]')[0]);
                }
                if (!(this.is_owner || (!this.is_owner && form_errors.owner_status))) {
                    custom_functions.show_error($('input[name=owner_status]')[0]);
                }

            },
            save: function () {
                var api_token = document.getElementById('api_token');
                var url = save_url+'?api_token='+api_token.value;
                var form = document.getElementById('register_steps_form');
                var form_data = new FormData(form);
                var response;
                var errors_container = document.getElementById('ajax_error');
                if (errors_container) {
                    errors_container.textContent = '';
                }
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            try {
                                response = JSON.parse(this.response);
                                if (response.success) {
                                    console.log('save profile', response.success);
                                    window.location.assign(redirect_url);
                                } else if (response.error) {
                                    console.log('save profile', response.error);
                                    var error_container;
                                    if (typeof response.error === 'string' || response.error instanceof String) {
                                        error_container = document.createElement('div');
                                        error_container.textContent = response.error;
                                        errors_container.appendChild(error_container);
                                    } else {
                                        for (var k in response.error) {
                                            console.log('k', k, response.error[k][0]);
                                            error_container = document.createElement('div');
                                            error_container.textContent = response.error[k][0];
                                            errors_container.appendChild(error_container);
                                        }
                                    }
                                }
                            } catch (e) {
                                console.log('error', e.message);
                            }
                        } else {
                            console.log('error response', this.response);
                        }
                    }
                };
                xhr.open('POST', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(form_data);
            },
            validate_date: function (doc_date) {
                var parent = doc_date.parent();
                if (custom_functions.validateDate(doc_date.val())) {
                    $(parent).find('.error-message').hide();
                    doc_date.removeClass('error').addClass('success');
                    if (is_disable) {
                        custom_functions.enable_button(submit_button_selector);
                        form_errors['document_date'] = true;
                    } else {
                        form_errors['document_date'] = true;
                    }
                } else {
                    $(parent).find('.error-message').show();
                    doc_date.addClass('error').removeClass('success');
                    custom_functions.disable_button(submit_button_selector);
                    form_errors['document_date'] = false;
                }
                if (custom_functions.validateDocumentDate(doc_date.val())) {
                    $(parent).find('.error-message1').hide();
                    doc_date.removeClass('error').addClass('success');
                    if (is_disable) {
                        custom_functions.enable_button(submit_button_selector);
                        form_errors['date_expired'] = true;
                    } else {
                        form_errors['date_expired'] = true;
                    }
                } else {
                    $(parent).find('.error-message1').show();
                    doc_date.addClass('error').removeClass('success');
                    custom_functions.disable_button(submit_button_selector);
                    form_errors['date_expired'] = false;
                }
                profile_form.form_valid();
            },
            uncheck_terms: function () {
                $('input[name=phone]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                });
                $('input[name=address]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                });
                $('input[name=doc_number]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                });
                $('input[name=income_month]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                });
                $('input[name=income_year]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                });
                $('input[name=income_amount]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                });
                $('input[name=outgoing_month]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                });
                $('input[name=outgoing_year]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                });
                $('input[name=outgoing_amount]').focus(function () {
                    var checkbox = $('#is_accept_terms');
                    if (checkbox) {
                        checkbox[0].checked = false;
                        $('#ajax_error').text('@lang('portal/partials/register/steps_section_3.fill_form')');
                    }
                })
            }
        };
        $( document ).ready(function() {

            var social_status_required = document.getElementById('social_status_required');
            var social_status = document.getElementById('social_status');
            var manager_status_required = document.getElementById('manager_status_required');
            var manager_status = document.getElementById('manager_status');
            var owner_status_required = document.getElementById('owner_status_required');
            var owner_status = document.getElementById('owner_status');
            var others_funds_required = document.getElementById('others_funds_required');
            var others_funds = document.getElementById('others_funds');

            var error_sign_start = document.getElementById('not_attached');
            if (error_sign_start && photo_attached) {
                error_sign_start.style.display = 'none';
            }

            if (others_funds) {
                others_funds.addEventListener('input', function () {
                    profile_form.other_funds = custom_functions.validateString(others_funds.value);
                    if (profile_form.other_funds) {
                        $(others_funds).removeClass('error').addClass('success');
                    } else {
                        $(others_funds).addClass('error').removeClass('success');
                    }
                    profile_form.form_valid();
                })
            }

            if (social_status) {
                social_status.addEventListener('input', function () {
                    profile_form.social_status = custom_functions.validateString(social_status.value);
                    if (profile_form.social_status) {
                        $(social_status).removeClass('error').addClass('success');
                    } else {
                        $(social_status).addClass('error').removeClass('success');
                    }
                    profile_form.form_valid();
                })
            }

            if (manager_status) {
                manager_status.addEventListener('input', function () {
                    profile_form.manager_status = custom_functions.validateString(manager_status.value);
                    if (profile_form.manager_status) {
                        $(manager_status).removeClass('error').addClass('success');
                    } else {
                        $(manager_status).addClass('error').removeClass('success');
                    }
                    profile_form.form_valid();
                })
            }

            if (owner_status) {
                owner_status.addEventListener('input', function () {
                    profile_form.owner_status = custom_functions.validateString(owner_status.value);
                    if (profile_form.owner_status) {
                        $(owner_status).removeClass('error').addClass('success');
                    } else {
                        $(owner_status).addClass('error').removeClass('success');
                    }
                    profile_form.form_valid();
                })
            }

            var start = moment().subtract(29, 'days');
            var end = moment();

            setTimeout(function () {
                $('.select2-select.profile-nationality').select2({
                    minimumResultsForSearch: Infinity,
                    allowClear: true,
                    placeholder: "{{ $trans['citizenship'] }}"
                });
                $('.select2-select.profile-doctype').select2({
                    minimumResultsForSearch: Infinity,
                    allowClear: true,
                    placeholder: "{{ $trans['document'] }}"
                });
                $('.select2-select.profile-funds-origin').select2({
                    minimumResultsForSearch: Infinity,
                    allowClear: true,
                    placeholder: "{{ $trans['funds_placeholder'] }}"
                });
            }, 200);

            setTimeout(function () {
                $('.select2-select.profile-funds-origin').on("select2:selecting", function(e) {
                    var el = $(this);
                    setTimeout(function () {
                        if (el.val() === 'Cita uz kartes saņemto līdzeklu izcelsme') {
                            others_funds_required.style.display = 'block';
                            profile_form.other_funds = false;
                            profile_form.form_valid();
                            if (others_funds) {
                                others_funds.value = '';
                            }

                        } else {
                            others_funds_required.style.display = 'none';
                            profile_form.other_funds = true;
                            profile_form.form_valid();
                            if (others_funds) {
                                $(others_funds).removeClass('success error');
                            }
                        }
                    }, 200)
                });
            }, 400);

            $(submit_button_selector).attr('disabled', true);

            $('.btn-add-new-photo').click(function() {
                $('.modal-add-child-photo').modal('show');
            });

            $('#get_modal_rules').click(function (e) {
                e.stopPropagation();
                $('.modal-rules').modal('show');
                var checkbox = $('#is_accept_terms');
                checkbox[0].checked = true;
                profile_form.terms = true;
                setTimeout(function () {
                    if(profile_form.valid()) {
//                        $(submit_button_selector).removeAttr('disabled');
                        $('#ajax_error')[0].textContent = '';
                        checkbox[0].checked = true;
                        profile_form.form_valid();
                    } else {
                        profile_form.terms = false;
                        profile_form.form_valid();
                        checkbox[0].checked = false;
//                        $(submit_button_selector).attr('disabled', true);
                        $('#ajax_error')[0].textContent = '@lang('portal/partials/register/steps_section_3.fill_form')';
                    }
                }, 200);
            });
            var terms_checkbox = $('#is_accept_terms');
            terms_checkbox[0].checked = false;
            terms_checkbox.change(function (e) {
                e.stopPropagation();
                if ($(this)[0].checked) {
                    profile_form.terms = true;
                    if (profile_form.valid()) {
                        $('#ajax_error')[0].textContent = '';
                    }
                    profile_form.form_valid();
//                    $(submit_button_selector).removeAttr('disabled');
                } else {
                    profile_form.terms = false;
                    profile_form.form_valid();
//                    $(submit_button_selector).attr('disabled', 'disabled');
                    $('#ajax_error')[0].textContent = '@lang('portal/partials/register/steps_section_3.fill_form')';
                }


//                if ($(this)[0].checked && !profile_form.valid()) {
//                    $(this)[0].checked = false;
//                    $('#submit_error').show();
//                    $(submit_button_selector).attr('disabled', true);
//                    profile_form.show_errors();
//                } else {
//                    $('#submit_error').hide();
//                }
            });


            $('#doc_image_button').click(function() {
                $('input[id=doc_copy]').click();
            });
            document.getElementById('doc_copy').onchange = function () {
                photo_attached = false;
                var name = this.files[0].name;
                var extension = this.files[0].name;
                extension = extension.split('.').pop();
                var size = this.files[0].size;
                var error_message = document.getElementById('doc_copy_error');
                var error_sign = document.getElementById('not_attached');
                var allowed_formats = document.getElementById('allowed_formats');
                var max_image_size = document.getElementById('max_image_size');
                if (['png', 'jpg'].indexOf(extension.toLowerCase()) !== -1 ) {
                    if (allowed_formats) {
                        allowed_formats.style.color = '#064874';
                    }
                    profile_form.doc_image = true;
                    if (error_message) {
                        error_message.style.display = 'none';
                    }
                    if (error_sign) {
                        error_sign.style.display = 'none';
                    }
                } else {
                    if (allowed_formats) {
                        allowed_formats.style.color = 'red';
                    }
                    profile_form.doc_image = false;
                    if (error_message) {
                        error_message.style.display = 'block';
                    }
                    if (error_sign) {
                        error_sign.style.display = 'block';
                    }
                }
                if (parseInt(size) < 4000000) {
                    if (max_image_size) {
                        max_image_size.style.color = '#064874';
                    }
                    profile_form.doc_image = profile_form.doc_image === true;
                    if (error_message) {
                        error_message.style.display = 'none';
                    }
                    if (error_sign) {
                        error_sign.style.display = 'none';
                    }
                } else {
                    if (max_image_size) {
                        max_image_size.style.color = 'red';
                    }
                    profile_form.doc_image = false;
                    if (error_message) {
                        error_message.style.display = 'block';
                    }
                    if (error_sign) {
                        error_sign.style.display = 'block';
                    }
                }
                if (name) {
                    var doc_name = document.getElementById('doc_name');
                    doc_name.textContent = name;
                    doc_name.style.color = 'green';
                }
            };
            custom_functions.validateInput(
                    'input[name=phone]',
                    submit_button_selector,
                    null,
                    custom_functions.validatePhone,
                    'phone'
            );
            custom_functions.validateInput(
                    'input[name=address]',
                    submit_button_selector,
                    null,
                    custom_functions.validateString,
                    'address'
            );
            setTimeout(function () {
                custom_functions.validateSelect(
                        '#profile_citizenship',
                        submit_button_selector,
                        'citizenship'
                );
                custom_functions.validateSelect(
                        '#doc_type',
                        submit_button_selector,
                        'doc_type'
                );
                custom_functions.validateSelect(
                        '#funds_origin',
                        submit_button_selector,
                        'funds_origin'
                );
            }, 200);
            custom_functions.validateInput(
                    'input[name=doc_number]',
                    submit_button_selector,
                    null,
                    custom_functions.validateString,
                    'doc_number'
            );
            custom_functions.validateInput(
                    'input[name=social_status]',
                    submit_button_selector,
                    null,
                    custom_functions.validateString,
                    'social_status'
            );
            $('input[name=has_status]').change(function () {
                var self = this;
                setTimeout(function () {
                    var value = $(self)[0].value;
                    var status = $('input[name=social_status]');
                    var parent = status.parent();
                    if (value === 'yes') {
                        profile_form.has_status = true;
                        form_errors.social_status = false;
                        profile_form.form_valid();
                        if (social_status_required) {
                            social_status_required.style.display = 'block';
                        }
                    } else {
                        profile_form.has_status = false;
                        profile_form.form_valid();
                        $(parent).find('.error-message').hide();
                        status.removeClass('success error');
                        status.val('');
                        if (social_status_required) {
                            social_status_required.style.display = 'none';
                        }
                    }
                }, 100);
            });
            custom_functions.validateInput(
                    'input[name=income_month]',
                    submit_button_selector,
                    null,
                    custom_functions.validateNumeric140,
                    'income_month'
            );
            custom_functions.validateInput(
                    'input[name=income_year]',
                    submit_button_selector,
                    null,
                    custom_functions.validateNumeric2500,
                    'income_year'
            );
            custom_functions.validateInput(
                    'input[name=income_amount]',
                    submit_button_selector,
                    null,
                    custom_functions.validateNumeric2500,
                    'income_amount'
            );
            custom_functions.validateInput(
                    'input[name=outgoing_month]',
                    submit_button_selector,
                    null,
                    custom_functions.validateNumeric140,
                    'outgoing_month'
            );
            custom_functions.validateInput(
                    'input[name=outgoing_year]',
                    submit_button_selector,
                    null,
                    custom_functions.validateNumeric2500,
                    'outgoing_year'
            );
            custom_functions.validateInput(
                    'input[name=outgoing_amount]',
                    submit_button_selector,
                    null,
                    custom_functions.validateNumeric2500,
                    'outgoing_amount'
            );
            custom_functions.validateInput(
                    'input[name=manager_status]',
                    submit_button_selector,
                    null,
                    custom_functions.validateString,
                    'manager_status'
            );
            $('input[name=is_manager]').change(function () {
                var self = this;
                setTimeout(function () {
                    var value = $(self)[0].value;
                    var status = $('input[name=manager_status]');
                    var parent = status.parent();
                    if (value === 'no') {
                        profile_form.is_manager = false;
                        form_errors.manager_status = false;
                        profile_form.form_valid();
                        if (manager_status_required) {
                            manager_status_required.style.display = 'block';
                        }
                    } else {
                        profile_form.is_manager = true;
                        form_errors.manager_status = true;
                        profile_form.form_valid();
                        $(parent).find('.error-message').hide();
                        status.removeClass('success error');
                        status.val('');
                        if (manager_status_required) {
                            manager_status_required.style.display = 'none';
                        }
                    }
                }, 100);
            });
            custom_functions.validateInput(
                    'input[name=owner_status]',
                    submit_button_selector,
                    null,
                    custom_functions.validateString,
                    'owner_status'
            );
            $('input[name=is_owner]').change(function () {
                var self = this;
                setTimeout(function () {
                    var value = $(self)[0].value;
                    var status = $('input[name=owner_status]');
                    var parent = status.parent();
                    if (value === 'no') {
                        profile_form.is_owner = false;
                        form_errors.owner_status = false;
                        profile_form.form_valid();
                        if (owner_status_required) {
                            owner_status_required.style.display = 'block';
                        }
                    } else {
                        profile_form.is_owner = true;
                        form_errors.owner_status = true;
                        profile_form.form_valid();
                        $(parent).find('.error-message').hide();
                        status.removeClass('success error');
                        status.val('');
                        if (owner_status_required) {
                            owner_status_required.style.display = 'none';
                        }
                    }
                }, 100);
            });

            var save_button = document.getElementById('save_profile_button');
            if (save_button) {
                save_button.addEventListener('click', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    profile_form.save();
                    return false;
                })
            }

            var save_button1 = document.getElementById('save_profile_button1');
            if (save_button1) {
                save_button1.addEventListener('click', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    profile_form.save();
                    return false;
                })
            }


            var doc_date = $('input[name="document_date"]');
            doc_date.daterangepicker(
                {
                    locale: locale,
                    singleDatePicker: true,
                    showDropdowns: true,
                    "opens": "center"
                },
                function(start, end, label) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY'));
//                        console.log("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                    setTimeout(function () {
                        profile_form.validate_date(doc_date);
                    }, 200);

                }
            );

            doc_date.attr('placeholder', '{{ trans('portal/register/steps.placeholder') }}');

            profile_form.validate_date(doc_date);
            profile_form.uncheck_terms();
        });
        var show_minutes_bank = 3;
        var timer_time_bank = 0;
        function update_modal_bank() {
            if (timer_time_bank > 360) {

                var el = document.getElementById('time_to_logout_bank');
                el.textContent = show_minutes_bank;
//                console.log('update', show_minutes_bank, el);
                $('.modal-bank-timer').modal('show');
                if (show_minutes_bank === 0) {
                    window.location.assign('https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit');
                }
                show_minutes_bank--;
            }
        }
        $(document).ready(function () {
            if (parseInt(bank_timer) === 1) {
                setInterval('timer_time_bank++;', 1000);
                setInterval('update_modal_bank();', 60000);
            }
            var icones = document.getElementById('icones');
            icones.addEventListener('click', function (e) {
                var parent = icones.parentNode;
                if (parent.hasAttribute('disabled')) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            });
        });
    </script>
@stop