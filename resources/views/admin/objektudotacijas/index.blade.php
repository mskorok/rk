@extends('admin.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span2">{{{ Lang::get('admin/objekti/table.title') }}}</th>
				<th class="span2">Reģ. nr.</th>
				<th class="span2">Dotāciju skaits vakar</th>
				<th class="span2">Dotāciju skaits šodien</th>
				<th class="span2">Attiecība</th>
				<th class="span2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
		<tbody>
		    @foreach($data as $row)
                <tr>
                    <td class="span2">{!! $row->object_name !!}</td>
                    <td class="span2">{!! $row->object_regnr !!}</td>
                    <td class="span2">{!! $v = intval($row->dotationHistoryTwoDays->filter(function ($item) use ($vakardiena) {
                                            return $item->datums == $vakardiena->toDateString();
                                        })->pluck('skaits')->first()) !!}
                    </td>
                    <td class="span2">{!! $s = intval($row->dotationHistoryTwoDays->filter(function ($item) use ($sodiena) {
                                            return $item->datums == $sodiena->toDateString();
                                        })->pluck('skaits')->first()) !!}
                    </td>
                    <td class="span2">
                        <?php
                            if (intval($row->dotationHistoryTwoDays->filter(function ($item) use ($sodiena) {
                                        return $item->datums == $sodiena->toDateString();
                                    })->pluck('regen')->first()) == 1)
                            {
                                echo '<p class="bg-success" style="color:white;text-align:center;">Pārģenerēts</p>';
                            }
                            else {
                                if($v > 0)
                                {
                                    if ($s == 0)
                                        echo '<p class="bg-danger" style="color:white;text-align:center;">0%</p>';
                                    else {
                                        $st = round(($s/$v)*100);
                                        if ($st <= 70)
                                            echo '<p class="bg-danger" style="color:white;text-align:center;">'.$st.'%</p>';
                                        else
                                            echo $st.'%';
                                    }
                                }
                                else {
                                    if ($s > 0)
                                        echo '100%';
                                    else
                                        echo '100%';
                                }
                            }
                        ?>
                    </td>
                    <td class="span2"><a href="/admin/objektudotacijas/regen/{{{ $row->id }}}/" class="btn btn-sm btn-warning">Pārģenerēt dotācijas</a></td>
                </tr>
            @endforeach
		</tbody>
	</table>
@stop


@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
                "bProcessing": false,
                "bServerSide": false,
                "bFilter": true,
                "bSort": true
			});
		});
	</script>
@stop