@extends('admin.layouts.default')

@section('title')
{{{ $title }}} :: @parent
@stop

@section('content')
<div class="page-header">
    <h3>
        {{{ $title }}}

    </h3>
</div>

@include('partials.form.formstart', array('action' => '/admin/check_console_command/'.$command))
    <p></p>
    <div class="form-group">
        <button type="submit" class="btn btn-success">{{ Lang::get('button.execute') }}</button>
    </div>
</form>

<div class="form-group result-message" style="margin-top: 20px;">
    @if(!empty($resultText))
        @if(is_array($resultText))
            @foreach ($resultText as $message)
                <p>{{ $message }}</p>
            @endforeach        
        @else
            {{ $resultText }}
        @endif
    @endif
</div>

</div>
@stop