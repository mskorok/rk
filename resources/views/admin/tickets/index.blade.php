@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span3">{{{ Lang::get('admin/tickets/table.user') }}}</th>
				<th class="span3">{{{ Lang::get('admin/tickets/table.rec') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.own') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.mpos') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.obj') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.eticket') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.status') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.type') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.compl') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.content') }}}</th>
				<th class="span2">{{{ Lang::get('admin/tickets/table.created_at') }}}</th>
				<th class="span2">Darbības</th>
			</tr>
		</thead>
	</table>
@stop


@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
				"sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"aoColumns": [
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable": true },
                    {"bSearchable": true },
                    {"bSearchable": true },
                    {"bSearchable": true },
                    {"bSearchable": true },
                    {"bSearchable": true },
                    {"bSearchable": true },
                    {"bSearchable": true },
					{"bSearchable":false},
					{"bSearchable":false},
				],
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{!! URL::to('admin/tickets/data') !!}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop