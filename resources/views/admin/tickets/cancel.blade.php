@extends('admin.layouts.modal')


@section('content')

    <form class="form-horizontal" method="post" action="" autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{!! $objekts->id !!}" />
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="control-group">
            <div class="controls">
                <element class="btn-cancel close_popup">Atgriezties</element>
                <button type="submit" class="btn btn-danger close_popup">{!! Lang::get('button.cancel') !!}</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop