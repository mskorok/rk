@extends('admin.layouts.modal')


@section('content')

	<form class="form-horizontal" method="post" action="" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">

				<div class="control-group {{{ $errors->has('eticket') ? 'error' : '' }}}">
					<label class="control-label" for="eticket">E-talons</label>
					<div class="controls">
						<input type="text" name="eticket" id="eticket" value="{{{ Input::old('eticket', isset($objekts) ? $objekts->eticket_nr : null) }}}" />
						{!! $errors->first('eticket', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('pk') ? 'error' : '' }}}">
					<label class="control-label" for="pk">Personas kods</label>
					<div class="controls">
						<input type="text" name="pk" id="pk" value="{{{ Input::old('pk', isset($objekts) ? substr($objekts->content,strpos($objekts->content,'PK')+3,12) : null) }}}" />
						{!! $errors->first('pk', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>


			</div>
			<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="control-group">
			<div class="controls">
				<element class="btn close_popup">{!! Lang::get('button.cancel') !!}</element>
				<button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop