@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

			<div class="pull-right">
				<a href="{{{ URL::to('admin/sellers/create') }}}" class="btn btn-small btn-info"><i class="icon-plus-sign icon-white"></i> Izveidot jaunu</a>
			</div>
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span2">{{{ Lang::get('admin/sellers/table.title') }}}</th>
				<th class="span2">Juridiskā adrese</th>
				<th class="span2">Tālrunis</th>
				<th class="span2">Banka</th>
				<th class="span2">Bankas kods</th>
				<th class="span2">Bankas konts</th>
				<th class="span2">Līgums</th>
				<th class="span2">{{{ Lang::get('admin/sellers/table.created_at') }}}</th>
				<th class="span2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
	</table>
@stop


@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
		        "sAjaxSource": "{!! URL::to('admin/sellers/data') !!}",
				"aoColumns": [
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":false},
					{"bSearchable":false},
				],
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop