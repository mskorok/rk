@extends('admin.layouts.default')


@section('content')

<div class="page-header">
    <h3>
        {{{ $title }}}
    </h3>
</div>


    <form class="form-horizontal" method="post" action="" autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{!! $objekts->id !!}" />
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="control-group">
            <div class="controls">
                <a href="{{{ URL::to('admin/sellers/') }}}" class="btn-cancel">{!! Lang::get('button.cancel') !!}</a>
                <button type="submit" name="submit" class="btn btn-danger">{!! Lang::get('button.delete') !!}</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop