@extends('admin.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}
	</h3>
</div>

@if ($mode=='edit')
    {!! Form::model($objekts, ['method' => 'POST', 'url' => URL::to('admin/sellers/' . $objekts->id . '/edit') ,'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
@else
    {!! Form::open(['method' => 'POST' ,'url' => URL::to('admin/sellers/create'), 'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
@endif

        @include('partials.form.text', array('name' => 'seller_name', 'label' => 'Nosaukums'))
        @include('partials.form.text', array('name' => 'jur_adrese', 'label' => 'Juridiskā adrese'))
        @include('partials.form.text', array('name' => 'talrunis', 'label' => 'Tālrunis'))
        @include('partials.form.text', array('name' => 'banka', 'label' => 'Banka'))
        @include('partials.form.text', array('name' => 'bankas_kods', 'label' => 'Bankas kods'))
        @include('partials.form.text', array('name' => 'konts', 'label' => 'Bankas konts'))
        @include('partials.form.text', array('name' => 'reg_nr', 'label' => 'Reģistrācijas nr.'))
        @include('partials.form.text', array('name' => 'contract_nr', 'label' => 'Līguma nr.'))
        @include('partials.form.text', array('name' => 'contract_person', 'label' => 'Paraksttiesīgā persona'))

		<div class="control-group">
			<div class="controls">
				<a href="{{{ URL::to('admin/sellers/') }}}" class="btn-cancel">{!! Lang::get('button.return') !!}</a>
				<button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
			</div>
		</div>

	</form>
@stop