@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

		</h3>
	</div>

    <table class="table table-bordered">
    <thead>
        <tr>
            <th class="span2" style="background-color:#333;color:white;">No e-talona</th>
            <th class="span2" style="background-color:#333;color:white;">Uz MPOS</th>
            <th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
            <th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
            <th class="span2" style="background-color:#333;color:white;">Darbības</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $r)
            <tr>
                <td>{{{ $r->from_eticket_nr }}}</td>
                <td>{{{ Mpos::where('account_id',$r->to_account_id)->value('mpos_name') }}}</td>
                <td>{{{ $r->amount() }}} EUR</td>
                <td>{{{ $r->created_at }}}</td>
                <td><a href="{{{ URL::to('admin/returnmoney/return/'.$r["id"]) }}}">Atgriezt</a><a></td>
            </tr>
        @endforeach
    </table>
@stop