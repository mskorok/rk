@extends('admin.layouts.default')

@section('title')
{{{ $title }}} :: @parent
@stop

@section('content')
<div class="page-header">
    <h3>{{{ $title }}}</h3>
</div>

@include('partials.form.formstart', array('action' => '/admin/transfermoney/transfer'))

@include('partials.form.text', array(
    'name' => 'eticket_from', 
    'label' => 'Pārcelt no e-talona', 
    'options' => [
        'id' => 'eticket_from', 
        'class' => 'eticket-ajax', 
        'autocomplete' => false,
    ]
))
@include('partials.form.text', array(
    'name' => 'eticket_to', 
    'label' => 'Papildināt e-talonu', 
    'options' => [
        'id' => 'eticket_to', 
        'class' => 'eticket-ajax', 
        'autocomplete' => false,
    ]
))
@include('partials.form.text', array('name' => 'summa', 'label' => 'Summa pārcelšanai (EUR)'))

<div class="form-group">
    <button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
</div>

</form>
</div>
@stop

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('.eticket-ajax').typeahead(
            [
            {
                name: 'eticket-ajax',
                remote: '/admin/transfermoney/fetcheticket/%QUERY',
                items: 10,
                minLength: 3
            }
            ]);
    });
</script>
@stop