@extends('admin.layouts.default')

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}

		<div class="pull-right">
			<a href="{{{ URL::to('admin/newtrans') }}}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus-sign"></i> Meklēt e-talonu</a>
		</div>
	</h3>
</div>

<div class="panel panel-primary">
	<form method="post" action="{{{ URL::to('admin/newtrans/create') }}}" autocomplete="off" class="panel-body" role="form">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<input type="hidden" name="eticket_id" value="{{{ $record->id }}}" />

		<div class="form-group">
			<label class="control-label" for="pk">Persona:</label>
			<input type="text" name="pk" id="pk" class="form-control" disabled value="{{{ $record->name }}}" />
		</div>

		<div class="form-group">
			<label class="control-label" for="name">Personas kods:</label>
			<input type="text" name="name" id="name" class="form-control" disabled value="{{{ $record->pk }}}" />
		</div>

        <div class="form-group">
            <label class="control-label" for="datums">Datums:</label>
            <input type="text" name="datums" class="form-control" value="" id="datums" />
        </div>

        <div class="form-group">
            <label class="control-label" for="objekts">Objekts:</label>
            <select name="objekts" class="form-control">
            @foreach($objekti_masivs as $object_id => $row)
                <option value="{{{ $object_id }}}">{{{ $row["name"] }}} ({{{ sprintf("%.2f",$row["price"]/100) }}} EUR)</option>
            @endforeach
            </select>
        </div>

        <div class="form-group">
            <label class="control-label" for="akts">Akta nr.:</label>
            <input type="text" name="akts" id="akts" class="form-control" />
        </div>

		<div class="form-group">
			<button type="submit" name="submit" id="submit" class="btn btn-success btn-sm">Izveidot</button>
		</div>
	</form>
</div>
@stop

@section("scripts")
<script type="text/javascript">
    $(function() {
        $('#datums').datetimepicker({
            pickTime: true,
            language: 'lv',
            format: 'YYYY-MM-DD HH:mm:ss'
        });
    });
</script>
@stop