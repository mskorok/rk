@extends('admin.layouts.default')

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}

		<div class="pull-right">
			<a href="{{{ URL::to('admin/neweticket/create') }}}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus-sign"></i> Izveidot jaunu</a>
		</div>
	</h3>
</div>

<div class="panel panel-primary">
	<form method="post" enctype="multipart/form-data" action="/admin/neweticket/edit" autocomplete="off" class="panel-body" role="form">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<input type="hidden" name="id" value="{{{ $record->id }}}" />

		<div class="form-group">
			<label class="control-label" for="name">Vārds, uzvārds:</label>
			<input type="text" name="name" id="name" class="form-control" value="{{{ Input::old('name', $record->name) }}}" />
		</div>

		@include('partials.form.ipk', array('objekts' => $record ))

		<div class="form-group">
			<label class="control-label" for="nr">E-talons:</label>
			<input type="text" name="nr" id="nr" class="form-control" value="{{{ Input::old('nr', $record->nr) }}}" />
		</div>

		<div class="form-group">
			<label class="control-label" for="grade">Klase:</label>
			<input type="text" name="grade" id="grade" class="form-control" value="{{{ Input::old('grade', $record->grade) }}}" />
		</div>

		<div class="form-group">
			<label class="control-label" for="free_meals_from">Brīvpusdienas no:</label>
			<input type="text" name="free_meals_from" id="free_meals_from" class="form-control" value="{{{ Input::old('free_meals_from', $record->free_meals_from) }}}" />
		</div>

        <div class="form-group">
            <label class="control-label" for="free_meals_until">Brīvpusdienas līdz:</label>
            <input type="text" name="free_meals_until" id="free_meals_until" class="form-control" value="{{{ Input::old('free_meals_until', $record->free_meals_until) }}}" />
        </div>

		<div class="form-group">
			<label class="control-label" for="objekti">Objekti:</label>
			@foreach($objekti as $o)
                <p>{{{ $o }}}</p>
            @endforeach
		</div>

		<div class="form-group">
			<button type="submit" name="submit" id="submit" class="btn btn-success btn-sm">{!! Lang::get('button.edit') !!}</button>
		</div>
	</form>
</div>
@stop