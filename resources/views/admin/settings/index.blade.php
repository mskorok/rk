@extends('admin.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span1">{{{ Lang::get('admin/settings/table.lang') }}}</th>
				<th class="span2">{{{ Lang::get('admin/settings/table.bann') }}}</th>
				<th class="span2">{{{ Lang::get('admin/settings/table.wrongp') }}}</th>
				<th class="span3">{{{ Lang::get('admin/settings/table.path') }}}</th>
				<th class="span2">E-talona apstiprināšanas veids</th>
				<th class="span4">Sistēmas rekvizīti</th>
				<th class="span4">Document text</th>
				<th class="span1">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
	</table>
@stop

@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
		        "sAjaxSource": "{!! URL::to('admin/settings/data') !!}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop