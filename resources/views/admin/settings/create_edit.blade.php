@extends('admin.layouts.default')

@section('content')

<div class="page-header">
    <h3>
        {{{ $title }}}
    </h3>
</div>

	<form class="form-horizontal" method="post" action="@if (isset($objekts)){!! URL::to('admin/settings/' . $objekts->id . '/edit') !!}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">

				<div class="control-group {{{ $errors->has('lang') ? 'error' : '' }}}">
					<label class="control-label" for="lang">Valoda</label>
					<div class="controls">
						<input class="form-control" type="text" name="lang" size="2" maxlength="2" id="lang" value="{{{ Input::old('lang', isset($objekts) ? $objekts->lang : null) }}}" />
						{!! $errors->first('lang', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('banned_ips') ? 'error' : '' }}}">
					<label class="control-label" for="banned_ips">Bani IP adresēm</label>
					<div class="controls">
						<input class="form-control" type="text" name="banned_ips" id="banned_ips" value="{{{ Input::old('banned_ips', isset($objekts) ? $objekts->banned_ips : null) }}}" />
						{!! $errors->first('banned_ips', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('wrong_password') ? 'error' : '' }}}">
					<label class="control-label" for="wrong_password">Nepareizu paroļu limits</label>
					<div class="controls">
						<input class="form-control" type="text" name="wrong_password" size="2" maxlength="2" id="wrong_password" value="{{{ Input::old('wrong_password', isset($objekts) ? $objekts->wrong_password : null) }}}" />
						{!! $errors->first('wrong_password', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('import_path') ? 'error' : '' }}}">
					<label class="control-label" for="import_path">Importa fails</label>
					<div class="controls">
						<input class="form-control" type="text" name="import_path" id="import_path" value="{{{ Input::old('import_path', isset($objekts) ? $objekts->import_path : null) }}}" />
						{!! $errors->first('import_path', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group">
					<label class="control-label"><b>Sistēmas rekvizīti</b></label>
					<div class="controls"></div>
				</div>

				<div class="control-group {{{ $errors->has('name') ? 'error' : '' }}}">
					<label class="control-label" for="name">Nosaukums</label>
					<div class="controls">
						<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', isset($objekts) ? $objekts->name : null) }}}" />
						{!! $errors->first('name', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('jur_adrese') ? 'error' : '' }}}">
					<label class="control-label" for="jur_adrese">Juridiskā adrese</label>
					<div class="controls">
						<input class="form-control" type="text" name="jur_adrese" id="jur_adrese" value="{{{ Input::old('jur_adrese', isset($objekts) ? $objekts->jur_adrese : null) }}}" />
						{!! $errors->first('jur_adrese', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('talrunis') ? 'error' : '' }}}">
					<label class="control-label" for="talrunis">Tālrunis</label>
					<div class="controls">
						<input class="form-control" type="text" name="talrunis" id="talrunis" value="{{{ Input::old('talrunis', isset($objekts) ? $objekts->talrunis : null) }}}" />
						{!! $errors->first('talrunis', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('banka') ? 'error' : '' }}}">
					<label class="control-label" for="banka">Banka</label>
					<div class="controls">
						<input class="form-control" type="text" name="banka" id="banka" value="{{{ Input::old('banka', isset($objekts) ? $objekts->banka : null) }}}" />
						{!! $errors->first('banka', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('bankas_kods') ? 'error' : '' }}}">
					<label class="control-label" for="bankas_kods">Bankas kods</label>
					<div class="controls">
						<input class="form-control" type="text" name="bankas_kods" id="bankas_kods" value="{{{ Input::old('bankas_kods', isset($objekts) ? $objekts->bankas_kods : null) }}}" />
						{!! $errors->first('bankas_kods', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('konts') ? 'error' : '' }}}">
					<label class="control-label" for="konts">Bankas konts</label>
					<div class="controls">
						<input class="form-control" type="text" name="konts" id="konts" value="{{{ Input::old('konts', isset($objekts) ? $objekts->konts : null) }}}" />
						{!! $errors->first('konts', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('reg_nr') ? 'error' : '' }}}">
					<label class="control-label" for="reg_nr">Reģistrācijas nr.</label>
					<div class="controls">
						<input class="form-control" type="text" name="reg_nr" id="reg_nr" value="{{{ Input::old('reg_nr', isset($objekts) ? $objekts->reg_nr : null) }}}" />
						{!! $errors->first('reg_nr', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('payment_form_message') ? 'error' : '' }}}">
					<label class="control-label" for="payment_form_message">Manuāla maksājuma forma - ievades teksts</label>
					<div class="controls">
						<input class="form-control" type="text" name="payment_form_message" id="payment_form_message" value="{{{ Input::old('payment_form_message', isset($objekts) ? $objekts->payment_form_message : null) }}}" />
						{!! $errors->first('payment_form_message', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

                <div class="control-group {{{ $errors->has('payment_form_details') ? 'error' : '' }}}">
                    <label class="control-label" for="payment_form_details">Manuāla maksājuma forma - rekvizīti (rindas atdalīt ar \n)</label>
                    <div class="controls">
                        <input class="form-control" type="text" name="payment_form_details" id="payment_form_details" value="{{{ Input::old('payment_form_details', isset($objekts) ? $objekts->payment_form_details : null) }}}" />
                        {!! $errors->first('payment_form_details', '<span class="help-inline">:message</span>') !!}
                    </div>
                </div>

				<div class="control-group {{{ $errors->has('dokuments_text') ? 'error' : '' }}}">
					<label class="control-label" for="dokuments_text">Paskaidrojošais teksts dokumenta pievienošanas formā</label>
					<div class="controls">
						<input class="form-control" type="text" name="dokuments_text" id="dokuments_text" value="{{{ Input::old('dokuments_text', isset($objekts) ? $objekts->dokuments_text : null) }}}" />
						{!! $errors->first('dokuments_text', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('yearly_text') ? 'error' : '' }}}">
					<label class="control-label" for="yearly_text">Paskaidrojošais teksts ikgadējai informācijas aktualizēšanai</label>
					<div class="controls">
						<input class="form-control" type="text" name="yearly_text" id="yearly_text" value="{{{ Input::old('yearly_text', isset($objekts) ? $objekts->yearly_text : null) }}}" />
						{!! $errors->first('yearly_text', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

                <div class="control-group {{{ $errors->has('yearly_date') ? 'error' : '' }}}">
                    <label class="control-label" for="yearly_date">Datums ikgadējai informācijas aktualizēšanai</label>
                    <div class="controls">
                        <input class="form-control" type="text" name="yearly_date" id="datetimepicker1" value="{{{ Input::old('yearly_date', isset($objekts) ? $objekts->yearly_date : "") }}}" />
                        {!! $errors->first('yearly_date', '<span class="help-inline">:message</span>') !!}
                    </div>
                </div>

			</div>
			<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="control-group">
			<div class="controls">
				<a href="{{{ URL::to('admin/settings/') }}}" class="btn-cancel">{!! Lang::get('button.return') !!}</a>
				<button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop