<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>
            @section('title')
            Administration skolas.rigaskarte.lv @if (App::environment() === 'dev') DEV @elseif (App::environment() == 'local') LOCAL @endif
            @show
        </title>

        <meta name="keywords" content="@yield('keywords')" />
        <meta name="author" content="@yield('author')" />

        <meta name="description" content="skolas.rigaskarte.lv admin" />

        <!--  Mobile Viewport Fix -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">

        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" type="text/css" href="/assets/compiled/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="/assets/compiled/css/other.css" />
        @yield('css')
        <style>
            @section('styles')
            @show
        </style>

        <style>
            body {
                padding-top: 140px;
            }
        </style>
    </head>

    <body>
        <div class="container">

            <!-- Navbar -->
            <div class="navbar navbar-default navbar-fixed-top">

                <div class="container">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Izvēlne</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <a class="navbar-brand" href="/admin">SKOLAS.RIGASKARTE.LV admin</a>
                    </div>

                    <div class="collapse navbar-collapse visible-lg" id="navbar-collapse-1">
                        <ul class="nav navbar-nav pull-left">
                            <li{!! (Request::is('admin') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin') }}}"><i class="icon-home icon-white"></i> Pārskats</a></li>
                            <li class="dropdown{!! (Request::is('admin/users*|admin/roles*') ? ' active' : '') !!}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{{ URL::to('admin/users') }}}">
                                    <i class="icon-user icon-white"></i> Kodifikatori <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{!! (Request::is('admin/users*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/users') }}}"><span class="glyphicon glyphicon-user"></span> Lietotāji</a></li>
                                    <li{!! (Request::is('admin/mpos*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/mpos') }}}"><span class="glyphicon glyphicon-cog"></span> MPOS</a></li>
                                    <li{!! (Request::is('admin/objekti*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/objekti') }}}"><span class="glyphicon glyphicon-book"></span> Objekti</a></li>
                                    <li{!! (Request::is('admin/sellers*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/sellers') }}}"><span class="glyphicon glyphicon-briefcase"></span> Tirgotāji</a></li>
                                    <li{!! (Request::is('admin/auths*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/auths') }}}"><span class="glyphicon glyphicon-globe"></span> Autorizatori</a></li>
                                    <li{!! (Request::is('admin/etickets*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/etickets') }}}"><span class="glyphicon glyphicon-tag"></span> E-taloni</a></li>
                                    <li{!! (Request::is('admin/dotations*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/dotations') }}}"><span class="glyphicon glyphicon-glass"></span> Dotāciju komplekti</a></li>
                                    <li{!! (Request::is('admin/tickets*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/tickets') }}}"><span class="glyphicon glyphicon-exclamation-sign"></span> Paziņojumi</a></li>
                                    <li{!! (Request::is('admin/loguser*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/loguser') }}}"><span class="glyphicon glyphicon-eye-open"></span> Lietotāju darbības</a></li>
                                    <li{!! (Request::is('admin/logadmin*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/logadmin') }}}"><span class="glyphicon glyphicon-eye-open"></span> Administratoru darbības</a></li>
                                    <li{!! (Request::is('admin/objgroup*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/objgroup') }}}"><span class="glyphicon glyphicon-book"></span> Objektu grupēšana</a></li>
                                    <li{!! (Request::is('admin/objektudotacijas') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/objektudotacijas') }}}"><span class="glyphicon glyphicon-book"></span> Objektu dotācijas</a></li>
                                </ul>
                            </li>
                            <li{!! (Request::is('admin/settings*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/settings') }}}"> Uzstādījumi</a></li>
                            <li{!! (Request::is('admin/banners*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/banners') }}}"> Banners</a></li>
                            <li{!! (Request::is('admin/top_slider*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/top_slider') }}}"> Top Slider</a></li>

                            <li{!! (Request::is('admin/translations') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/translations') }}}"> Translations</a></li>
                            <li class="dropdown{!! (Request::is('admin/import*|admin/converttoeuro*') ? ' active' : '') !!}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{{ URL::to('admin/users') }}}">
                                    <i class="icon-user icon-white"></i> Operācijas <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{!! (Request::is('admin/newbase*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/newbase') }}}"><span class="glyphicon glyphicon-tag"></span> E-talonu bāzes dati</a></li>
                                    <li{!! (Request::is('admin/neweticket*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/neweticket') }}}"><span class="glyphicon glyphicon-tag"></span> E-talonu pievienošana</a></li>
                                    <li{!! (Request::is('admin/newtrans*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/newtrans') }}}"><span class="glyphicon glyphicon-tag"></span> Manuāla brīvpusdienu transakcija</a></li>
                                    <li{!! (Request::is('admin/changeuser*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/changeuser') }}}"><span class="glyphicon glyphicon-user"></span> Atvērt sistēmu kā lietotājam</a></li>
                                    <li{!! (Request::is('accountant/*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('accountant/index') }}}"><span class="glyphicon glyphicon-tasks"></span> Grāmatveža skats</a></li>
                                    <li{!! (Request::is('rk/*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('rk/index') }}}"><span class="glyphicon glyphicon-tasks"></span> RK skats</a></li>
                                    <li{!! (Request::is('admin/returnmoney/*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/returnmoney') }}}"><span class="glyphicon glyphicon-tasks"></span> Naudas atgriešana</a></li>
                                    <li{!! (Request::is('admin/transfermoney/*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/transfermoney') }}}"><span class="glyphicon glyphicon-tasks"></span> Atlikuma pārcelšana</a></li>
                                </ul>
                            </li>
                            <li class="dropdown{!! (Request::is('admin/reports*') ? ' active' : '') !!}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="icon-user icon-white"></i> Pārskati <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{!! (Request::is('admin/reports/global*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/reports/global') }}}"><span class="glyphicon glyphicon-euro"></span> Ienākošo un izejošo maksājumu pārskats</a></li>
                                    <li{!! (Request::is('reports/objetickets*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/objetickets') }}}"><span class="glyphicon glyphicon-euro"></span> Objekta e-talonu atskaite</a></li>
                                    <li{!! (Request::is('reports/objseller*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/objseller') }}}"><span class="glyphicon glyphicon-euro"></span> Objekta tirgotāju atskaite</a></li>
                                    <li{!! (Request::is('reports/objekts*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/objekts') }}}"><span class="glyphicon glyphicon-euro"></span> Objekta atskaite</a></li>
                                    <li{!! (Request::is('reports/seller*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/seller') }}}"><span class="glyphicon glyphicon-euro"></span> Tirgotāja atskaite</a></li>
                                    <li{!! (Request::is('reports/account*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/account') }}}"><span class="glyphicon glyphicon-euro"></span> Lietotāja atskaite</a></li>
                                    <li{!! (Request::is('reports/entrance*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/entrance') }}}"><span class="glyphicon glyphicon-euro"></span> Pārvietošanās atskaite</a></li>
                                    <li{!! (Request::is('reports/akts*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/akts') }}}"><span class="glyphicon glyphicon-euro"></span> Salīdzināšanas akts</a></li>
                                    <li{!! (Request::is('reports/selltotals*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/selltotals') }}}"><span class="glyphicon glyphicon-euro"></span> Tirgotāja kopējā atskaite</a></li>
                                    <li{!! (Request::is('reports/money*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/money') }}}"><span class="glyphicon glyphicon-euro"></span> Atskaite par elektronisko naudu sistēmā</a></li>
                                </ul>
                            </li>
                            <li{!! (Request::is('logviewer*') ? ' class="active"' : '') !!}><a target="_blank" href="{{{ URL::to('admin/logviewer') }}}"> Log faili</a></li>
                            <?php /*
                            @if (Auth::user()->id == 1)
                            <li class="dropdown{!! (Request::is('admin/check_console_command*') ? ' active' : '') !!}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    Run console command <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{!! (Request::is('admin/check_console_command/1') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/1') }}}"> Ģenerēt tirgotāju izmaksas 3x mēnesī</a></li>
                                    <li{!! (Request::is('admin/check_console_command/2') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/2') }}}"> Atjaunot informāciju no API monitoringa</a></li>
                                    <li{!! (Request::is('admin/check_console_command/3') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/3') }}}"> Dzēst neapstiprinātās lietotāju reģistrācijas pēc 5 dienām</a></li>
                                    <li{!! (Request::is('admin/check_console_command/4') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/4') }}}"> Deaktivizēt trešo statusu no CSV improtētajiem, kas nav atnākuši no ZZ</a></li>
                                    <li{!! (Request::is('admin/check_console_command/5') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/5') }}}"> Ģenerēt trūkstošās tirgotāju izmaksas</a></li>
                                    <li{!! (Request::is('admin/check_console_command/6') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/6') }}}"> Ģenerēt tirgotāju dienas beigu bilances</a></li>
                                    <li{!! (Request::is('admin/check_console_command/7') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/7') }}}"> Ģenerēt tirgotāju brīvpusdienu datus par pēdējo mēnesi</a></li>
                                    <li{!! (Request::is('admin/check_console_command/8') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/8') }}}"> Ģenerēt tirgotāju izmaksas 3x mēnesī</a></li>
                                    <li{!! (Request::is('admin/check_console_command/9') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/9') }}}"> Atjaunot informāciju no API par personām ar brīvpusdienām</a></li>
                                    <li{!! (Request::is('admin/check_console_command/10') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/10') }}}"> Atjaunot skolu informāciju no API</a></li>
                                    <li{!! (Request::is('admin/check_console_command/11') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/11') }}}"> Importēt info no CSV faila un sanemt karsu datus no API</a></li>
                                    <li{!! (Request::is('admin/check_console_command/12') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/12') }}}"> Importēt info no CSV faila, bez RK</a></li>
                                    <li{!! (Request::is('admin/check_console_command/13') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/13') }}}"> Pārģenerācija</a></li>
                                    <li{!! (Request::is('admin/check_console_command/14') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/14') }}}"> DIENAS ATSKAITE DARĪJUMI LIETOTĀJI - visas personas, visi etaloni</a></li>
                                    <li{!! (Request::is('admin/check_console_command/15') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/15') }}}"> Ikdienas atskaite nr.7 - visi tirgotāji pa skolām</a></li>
                                    <li{!! (Request::is('admin/check_console_command/16') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/16') }}}"> Mēneša atskaite nr.8 - visi tirgotāji pa skolām</a></li>
                                    <li{!! (Request::is('admin/check_console_command/17') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/17') }}}"> Neatrastie kopgalda darījumi par vakardienu</a></li>
                                    <li{!! (Request::is('admin/check_console_command/18') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/18') }}}"> Nosūtīt brīvpusdienu datus par vakardienu</a></li>
                                    <li{!! (Request::is('admin/check_console_command/19') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/19') }}}"> Atjaunot informāciju no RK API par visiem aktīvajiem e-taloniem</a></li>
                                    <li{!! (Request::is('admin/check_console_command/20') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/20') }}}"> Atjaunot informāciju autorizatoram par personām ar brīvpusdienām</a></li>
                                    <li{!! (Request::is('admin/check_console_command/21') ? ' class="active"' : '') !!}><a href="{{{ URL::to('admin/check_console_command/21') }}}"> Papildināt vakardienas transakcijas ar tirgotāju reģistrācijas numuriem un brīvpusdienu tipiem</a></li>
                                </ul>
                            </li>
                            @endif
                             * 
                             */?>
                        </ul>
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="glyphicon glyphicon-user"></span> {{{ Auth::user()->username }}} {{{ Auth::user()->surname }}}	<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{{ URL::to('/') }}}"><i class="icon-wrench"></i> Sākumlapa</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{{ URL::to('auth/logout') }}}"><i class="icon-share"></i> Iziet</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- ./ nav-collapse -->
                </div>
                <!-- ./ container -->
            </div><!-- ./ navbar -->


            <!-- Notifications -->
            @include('notifications')
            <!-- ./ notifications -->

            <!-- Content -->
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div>
            </div>
            <!-- ./ content -->

            <!-- Footer -->
            <footer class="clearfix">
                @yield('footer')
            </footer>
            <!-- ./ Footer -->

        </div>
        <!-- Javascripts
        ================================================== -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script>

        <script src="/assets/compiled/js/all.js"></script>

        {{--<script type="text/javascript">--}}
{{--$(document).ready(function ()--}}
{{--{--}}

    {{--var r = $('.slider').slider({--}}
        {{--value: 0,--}}
        {{--step: 0.01,--}}
        {{--tooltip: 'hide'--}}
    {{--})--}}
            {{--.on('slide', function (ev) {--}}
                {{--$('#sumv').val(r.getValue());--}}

                {{--var tmp = parseFloat(r.getValue());--}}

                {{--betalons = Math.round((0 - min + tmp) * 100) / 100;--}}
                {{--bkonts = Math.round((max - tmp) * 100) / 100;--}}

                {{--$('#bkonts').val(bkonts);--}}
                {{--$('#betalons').val(betalons);--}}
            {{--})--}}
            {{--.data('slider');--}}

    {{--if (r != null)--}}
    {{--{--}}
        {{--var min = r.getMin();--}}
        {{--var max = r.getMax();--}}

        {{--$('#bkonts').val(max);--}}
        {{--$('#betalons').val(0 - min);--}}
        {{--var bkonts = max;--}}
        {{--var betalons = 0 - min;--}}
    {{--}--}}

    {{--// ar roku ierakstita vertiba--}}
    {{--$('#sumv').change(function ()--}}
    {{--{--}}
        {{--if ($(this).val() > max)--}}
        {{--{--}}
            {{--$(this).val(max);--}}
            {{--r.setValue(max);--}}

            {{--bkonts = 0;--}}
            {{--betalons = 0 - min + max;--}}

        {{--} else if ($(this).val() < min)--}}
        {{--{--}}
            {{--$(this).val(min);--}}
            {{--r.setValue(min);--}}

            {{--betalons = 0;--}}
            {{--bkonts = bkonts + (0 - min);--}}
        {{--} else {--}}
            {{--r.setValue($(this).val());--}}
            {{--var tmp = parseFloat($(this).val());--}}

            {{--betalons = Math.round((0 - min + tmp) * 100) / 100;--}}
            {{--bkonts = Math.round((max - tmp) * 100) / 100;--}}
        {{--}--}}

        {{--$('#bkonts').val(bkonts);--}}
        {{--$('#betalons').val(betalons);--}}

        {{--$('#sum').val(tmp);--}}
    {{--});--}}

    {{--$('element.close_popup, .btn-inverse').click(function () {--}}
        {{--parent.oTable.fnReloadAjax();--}}
        {{--parent.$.colorbox.close();--}}
        {{--return false;--}}
    {{--});--}}

    {{--$('button[type="submit"].close_popup').click(function (e) {--}}

        {{--if (self == top)--}}
        {{--{--}}
            {{--return true;--}}
        {{--} else {--}}
            {{--e.preventDefault();--}}
            {{--$.post(window.location, $("form:first").serialize());--}}

            {{--setTimeout(function () {--}}
                {{--parent.oTable.fnReloadAjax();--}}
                {{--parent.$.colorbox.close();--}}

            {{--}, 400);--}}

            {{--return false;--}}
        {{--}--}}

    {{--});--}}

    {{--$('#datetimepicker1').datetimepicker({--}}
        {{--pickTime: false,--}}
        {{--language: 'lv',--}}
        {{--format: 'YYYY-MM-DD'--}}
    {{--});--}}
    {{--$('#datetimepicker2').datetimepicker({--}}
        {{--pickTime: false,--}}
        {{--language: 'lv',--}}
        {{--format: 'YYYY-MM-DD'--}}
    {{--});--}}
{{--});--}}
        {{--</script>--}}

        @yield('scripts')

    </body>

</html>