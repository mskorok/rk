@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

		</h3>
	</div>

	@include('partials.form.formstart', array('action' => '/admin/changeuser/change'))

		<div class="form-group">
			<label class="control-label" for="users">E-pasts</label>
			<input class="form-control" type="text" name="users" id="users" autocomplete="off" />
		</div>

		<div class="form-group">
			<label class="control-label" for="pass">Jūsu parole</label>
			<input class="form-control" type="password" name="pass" id="pass" autocomplete="off" />
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
		</div>

			<p>Pēc pārslēgšanās, beidzot darbu, nepieciešams izlogoties no sistēmas! Pārslēgšanas atpakaļ uz administratoru nav iespējama!</p>
		</form>
	</div>
@stop


@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#users').typeahead([
			  {
			    name: 'system-users',
			    remote: '/admin/changeuser/fetch/%QUERY',
			    items: 10,
			    minLength : 2
			  }
			]);
		});
	</script>
@stop