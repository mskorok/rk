@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

			<div class="pull-right">
				<a href="{{{ URL::to('admin/objgroup/create') }}}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-plus"></span> Izveidot jaunu</a>
			</div>
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span3">Grupa</th>
				<th class="span3">Objekti</th>
				<th class="span2">{{{ Lang::get('table.created_at') }}}</th>
				<th class="span2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
			@foreach ($data as $single)
			<tr>
				<td>{!! $single->ogroup_name !!}</td>
				<td>{!! $single->objekti !!}</td>
				<td>{!! $single->created_at !!}</td>
				<td><a href="{{{ URL::to('admin/objgroup/' . $single->id . '/edit' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get('button.edit') }}}</a>
                <a href="{{{ URL::to('admin/objgroup/' . $single->id . '/delete' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get('button.delete') }}}</a></td>
			</tr>
			@endforeach
		</thead>
	</table>
@stop