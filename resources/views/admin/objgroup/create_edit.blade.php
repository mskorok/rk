@extends('admin.layouts.default')

@section('content')

<div class="page-header">
    <h3>
        {{{ $title }}}
    </h3>
</div>

<form class="form-horizontal" method="post" action="@if (isset($objekts)){!! URL::to('admin/objgroup/' . $objekts->id . '/edit') !!}@endif" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <!-- ./ csrf token -->

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">

            <div class="control-group {{{ $errors->has('ogroup_name') ? 'error' : '' }}}">
                <label class="control-label" for="ogroup_name">Nosaukums</label>
                <div class="controls">
                    <input type="text" name="ogroup_name" id="ogroup_name" value="{{{ Input::old('ogroup_name', isset($objekts) ? $objekts->ogroup_name : null) }}}" />
                    {!! $errors->first('ogroup_name', '<span class="help-inline">:message</span>') !!}
                </div>
            </div>

            <div class="control-group {{{ $errors->has('object_id') ? 'error' : '' }}}">
                <label class="control-label" for="object_id">Objekts</label>
                <div class="controls">
                    <select name="object_id[]" id="object_id" multiple size="10">
                        @foreach ($data as $single)
                        @if ($mode == 'create')
                        <option value="{{{ $single->id }}}">{{{ $single->object_name }}}</option>
                        @else
                        <option value="{{{ $single->id }}}" @if (in_array($single->id,$oo)) selected="selected" @endif;>{{{ $single->object_name }}}</option>
                        @endif
                        @endforeach
                    </select>

                    <span class="help-block">
                        Sasaistīt objektus
                    </span>
                </div>
            </div>

        </div>
        <!-- ./ general tab -->

    </div>
    <!-- ./ tabs content -->

    <!-- Form Actions -->
    <div class="control-group">
        <div class="controls">
            <a href="{{{ URL::to('admin/objgroup/') }}}" class="btn-cancel">{!! Lang::get('button.cancel') !!}</a>
            <button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>
@stop