@extends('admin.layouts.default')

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}
	</h3>
</div>

<div class="panel panel-primary">
	<form method="post" action="{{{ URL::to('admin/neweticket/create2') }}}" autocomplete="off" class="panel-body" role="form">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

		<div class="form-group">
			<label class="control-label" for="eticket">Personas kods - {{{ $results["r2"]["pk"] }}}</label>
		</div>

		<div class="form-group">
			<label class="control-label" for="etickets">E-taloni (atdalīti ar komatu):</label>
			<input type="text" name="etickets" id="etickets" class="form-control" value="{{{ Input::old('etickets', '') }}}" />
		</div>

		<div class="form-group">
			<button type="submit" name="submit" id="submit" class="btn btn-success btn-sm">Saglabāt</button>
		</div>
	</form>
</div>
@stop