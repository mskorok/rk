@extends('admin.layouts.default')

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}

		<div class="pull-right">
			<a href="{{{ URL::to('admin/neweticket') }}}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus-sign"></i> Meklēt e-talonu</a>
		</div>
	</h3>
</div>

<div class="panel panel-primary">
	<form method="post" action="{{{ URL::to('admin/neweticket/create') }}}" autocomplete="off" class="panel-body" role="form">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

		<div class="form-group">
			<label class="control-label" for="pk">Personas kods:</label>
			<input type="text" name="pk" id="pk" class="form-control" value="{{{ Input::old('pk', '') }}}" />
		</div>

		<div class="form-group">
			<button type="submit" name="submit" id="submit" class="btn btn-success btn-sm">Nosūtīt pieprasījumu uz ārējiem API</button>
		</div>
	</form>
</div>
@stop