@extends('admin.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}
	</h3>
</div>

	<!-- Tabs -->
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab-general" data-toggle="tab">Objekta informācija</a></li>
        <li><a href="#tab-allow" data-toggle="tab">Pārvietošanās dati</a></li>
	</ul>
	<!-- ./ tabs -->

	<form class="form-horizontal" method="post" action="@if (isset($objekts)){!! URL::to('admin/objekti/' . $objekts->id . '/edit') !!}@endif" autocomplete="off">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">

				<div class="control-group {{{ $errors->has('object_type') ? 'error' : '' }}}">
	                <label class="control-label" for="object_type">Objekta veids</label>
	                <div class="controls">
		                <select name="object_type" id="object_type">
		                        <option value="1">Skola</option>
						</select>
	            	</div>
	            	{!! $errors->first('object_type', '<span class="help-inline">:message</span>') !!}
				</div>

				<div class="control-group {{{ $errors->has('object_name') ? 'error' : '' }}}">
					<label class="control-label" for="object_name">Nosaukums</label>
					<div class="controls">
						<input type="text" name="object_name" id="object_name" value="{{{ Input::old('object_name', isset($objekts) ? $objekts->object_name : null) }}}" />
						{!! $errors->first('object_name', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('object_regnr') ? 'error' : '' }}}">
					<label class="control-label" for="kods">Reģistrācijas nr.</label>
					<div class="controls">
						<input type="text" name="object_regnr" id="object_regnr" value="{{{ Input::old('object_regnr', isset($objekts) ? $objekts->object_regnr : null) }}}" />
						{!! $errors->first('object_regnr', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('object_country') ? 'error' : '' }}}">
					<label class="control-label" for="object_country">Valsts</label>
					<div class="controls">
						<input type="text" name="object_country" id="object_country" value="{{{ Input::old('object_country', isset($objekts) ? $objekts->object_country : null) }}}" />
						{!! $errors->first('object_country', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('object_city') ? 'error' : '' }}}">
					<label class="control-label" for="object_city">Pilsēta</label>
					<div class="controls">
						<input type="text" name="object_city" id="object_city" value="{{{ Input::old('object_city', isset($objekts) ? $objekts->object_city : null) }}}" />
						{!! $errors->first('object_city', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('object_address') ? 'error' : '' }}}">
					<label class="control-label" for="object_address">Adrese</label>
					<div class="controls">
						<input type="text" name="object_address" id="object_address" value="{{{ Input::old('object_address', isset($objekts) ? $objekts->object_address : null) }}}" />
						{!! $errors->first('object_address', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('object_ip') ? 'error' : '' }}}">
					<label class="control-label" for="object_ip">IP adrese</label>
					<div class="controls">
						<input type="text" name="object_ip" id="object_ip" value="{{{ Input::old('object_ip', isset($objekts) ? $objekts->object_ip : null) }}}" />
						{!! $errors->first('object_ip', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('kods') ? 'error' : '' }}}">
					<label class="control-label" for="kods">Kods</label>
					<div class="controls">
						<input type="text" name="kods" id="kods" value="{{{ Input::old('kods', isset($objekts) ? $objekts->kods : null) }}}" />
						{!! $errors->first('kods', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				@if ($mode=='create')
				<div class="control-group {{{ $errors->has('school_id') ? 'error' : '' }}}">
					<label class="control-label" for="school_id">Skola no kodifikatora</label>
					<div class="controls">
						<select name="school_id" id="school_id">
                            @foreach($schools as $school)
                                <option value="{{{ $school->id }}}" {{{ isset($objekts) ? (($objekts->school_id == $school->id) ? 'selected="selected"' : '') : '' }}}>{{{ $school->heading }}}</option>
                            @endforeach
						</select>
					</div>
					{!! $errors->first('school_id', '<span class="help-inline">:message</span>') !!}
				</div>
                @endif

			</div>
			<!-- ./ general tab -->

			<div class="tab-pane" id="tab-allow">

				<div class="control-group {{{ $errors->has('entrance_ip') ? 'error' : '' }}}">
					<label class="control-label" for="entrance_ip">IP adrese, datu saņemšanai no objekta</label>
					<div class="controls">
						<input type="text" name="entrance_ip" id="entrance_ip" value="{{{ Input::old('entrance_ip', isset($objekts) ? $objekts->entrance_ip : null) }}}" />
						{!! $errors->first('entrance_ip', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('sync_path') ? 'error' : '' }}}">
					<label class="control-label" for="sync_path">Adrese, datu sinhronizācijas skriptam</label>
					<div class="controls">
						<input type="text" name="sync_path" id="sync_path" value="{{{ Input::old('sync_path', isset($objekts) ? $objekts->sync_path : null) }}}" />
						{!! $errors->first('sync_path', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('sync_enable') ? 'error' : '' }}}">
	                <label class="control-label" for="sync_enable">Tālāka sinhronizācija</label>
	                <div class="controls">
		                <select name="sync_enable" id="sync_enable">
		                        <option value="0" {{{ isset($objekts) ? (($objekts->sync_enable==0) ? 'selected="selected"' : '') : '' }}}>Nē</option>
		                        <option value="1" {{{ isset($objekts) ? (($objekts->sync_enable==1) ? 'selected="selected"' : '') : '' }}}>Jā</option>
						</select>
	            	</div>
	            	{!! $errors->first('sync_enable', '<span class="help-inline">:message</span>') !!}
				</div>

			</div>

		</div>
		<!-- ./ tabs content -->

		<div class="control-group">
			<div class="controls">
				<element class="btn-cancel">{!! Lang::get('button.cancel') !!}</element>
				<button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
			</div>
		</div>
	</form>
@stop