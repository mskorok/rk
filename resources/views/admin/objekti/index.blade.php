@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

			<div class="pull-right">
				<a href="{{{ URL::to('admin/objekti/create') }}}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-plus"></span> {{{ Lang::get('button.create') }}}</a>
			</div>
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span2">{{{ Lang::get('admin/objekti/table.title') }}}</th>
				<th class="span1">Reģ. nr.</th>
				<th class="span1">{{{ Lang::get('admin/objekti/table.type') }}}</th>
				<th class="span1">{{{ Lang::get('admin/objekti/table.country') }}}</th>
				<th class="span1">{{{ Lang::get('admin/objekti/table.city') }}}</th>
				<th class="span1">{{{ Lang::get('admin/objekti/table.addr') }}}</th>
				<th class="span1">{{{ Lang::get('admin/objekti/table.ip') }}}</th>
				<th class="span1">Kods</th>
				<th class="span1">{{{ Lang::get('admin/objekti/table.created_at') }}}</th>
				<th class="span2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
	</table>
@stop


@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
		        "sAjaxSource": "{!! URL::to('admin/objekti/data') !!}",
				"aoColumns": [
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":false},
					{"bSearchable":false},
				],
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop