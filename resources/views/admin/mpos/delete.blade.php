@extends('admin.layouts.default')

@section('title')
    {{{ $title }}} :: @parent
@stop

@section('content')

<div class="page-header">
    <h3>
        {{{ $title }}}
    </h3>
</div>

    <form class="form-horizontal" method="post" action="" autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{!! $mpos->id !!}" />
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="control-group">
            <div class="controls">
                <a href="{{{ URL::to('admin/mpos/') }}}" class="btn-cancel">Atcelt</a>
                <button type="submit" class="btn btn-danger close_popup">Dzēst</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop