@extends('admin.layouts.modal')


@section('content')

	<form class="form-horizontal" enctype="multipart/form-data" method="post" action="" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">

				<div class="control-group {{{ $errors->has('fw_version') ? 'error' : '' }}}">
	                <label class="control-label" for="fw_version">FW versija</label>
	                <div class="controls">
		                <input type="text" name="fw_version" id="fw_version" value="{{{ Input::old('fw_version') }}}" />
	            	</div>
	            	{!! $errors->first('fw_version', '<span class="help-inline">:message</span>') !!}
				</div>

				<div class="control-group {{{ $errors->has('fails') ? 'error' : '' }}}">
					<label class="control-label" for="fails">FW fails</label>
					<div class="controls">
						<input type="file" name="fails" id="fails" />
						{!! $errors->first('fails', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

			</div>
			<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="control-group">
			<div class="controls">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop