@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

			<div class="pull-right">
				<a href="{{{ URL::to('admin/mpos/create') }}}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-plus"></span> Izveidot jaunu</a>
			</div>
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span3">{{{ Lang::get('admin/mpos/table.title') }}}</th>
				<th class="span3">{{{ Lang::get('admin/mpos/table.type') }}}</th>
				<th class="span3">{{{ Lang::get('admin/mpos/table.object') }}}</th>
				<th class="span3">{{{ Lang::get('admin/mpos/table.owner') }}}</th>
				<th class="span2">{{{ Lang::get('admin/mpos/table.lastonline') }}}</th>
				<th class="span2">{{{ Lang::get('admin/mpos/table.fw') }}}</th>
				<th class="span2">Serial nr.</th>
				<th class="span2">Secret key</th>
				<th class="span2">{{{ Lang::get('admin/mpos/table.created_at') }}}</th>
				<th class="span2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
	</table>
@stop


@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
				"sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"aoColumns": [
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable":true},
					{"bSearchable":false},
					{"bSearchable":false},
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":false},
					{"bSearchable":false},
				],
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{!! URL::to('admin/mpos/data') !!}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop