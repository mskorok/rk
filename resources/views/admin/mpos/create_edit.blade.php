@extends('admin.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}
	</h3>
</div>


	<form class="form-horizontal" method="post" action="@if (isset($mpos)){!! URL::to('admin/mpos/' . $mpos->id . '/edit') !!}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">

				<div class="control-group">
					<label class="control-label" for="abc">ID</label>
					<div class="controls">
						<input type="text" name="abc" id="abc" disabled="disabled" value="{{{ isset($mpos) ? $mpos->id : '' }}}" />
					</div>
				</div>

				<div class="control-group {{{ $errors->has('mpos_type') ? 'error' : '' }}}">
	                <label class="control-label" for="mpos_type">Iekārtas tips</label>
	                <div class="controls">
		                <select name="mpos_type" id="mpos_type">
		                        <option value="1">Standarta</option>
						</select>
	            	</div>
	            	{!! $errors->first('mpos_type', '<span class="help-inline">:message</span>') !!}
				</div>

				<div class="control-group {{{ $errors->has('mpos_name') ? 'error' : '' }}}">
					<label class="control-label" for="mpos_name">Nosaukums</label>
					<div class="controls">
						<input type="text" name="mpos_name" id="mpos_name" value="{{{ Input::old('mpos_name', isset($mpos) ? $mpos->mpos_name : null) }}}" />
						{!! $errors->first('mpos_name', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('firmware_version') ? 'error' : '' }}}">
					<label class="control-label" for="firmware_version">Firmware versija</label>
					<div class="controls">
						<input type="text" name="firmware_version" id="firmware_version" value="{{{ Input::old('firmware_version', isset($mpos) ? $mpos->firmware_version : null) }}}" />
						{!! $errors->first('firmware_version', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('operation_id') ? 'error' : '' }}}">
					<label class="control-label" for="operation_id">Operācijas ID</label>
					<div class="controls">
						<input type="text" name="operation_id" id="operation_id" value="{{{ Input::old('operation_id', isset($mpos) ? $mpos->operation_id : null) }}}" />
						{!! $errors->first('operation_id', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('mpos_serial') ? 'error' : '' }}}">
					<label class="control-label" for="mpos_serial">Serial nr.</label>
					<div class="controls">
						<input type="text" name="mpos_serial" id="mpos_serial" value="{{{ Input::old('mpos_serial', isset($mpos) ? $mpos->mpos_serial : null) }}}" />
						{{ $errors->first('mpos_serial', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('secret_key') ? 'error' : '' }}}">
					<label class="control-label" for="secret_key">Secret key</label>
					<div class="controls">
						<input type="text" name="secret_key" id="secret_key" value="{{{ Input::old('secret_key', isset($mpos) ? $mpos->secret_key : null) }}}" />
						{{ $errors->first('secret_key', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				@if ($t==0)

					<div class="control-group {{{ $errors->has('object_id') ? 'error' : '' }}}">
		                <label class="control-label" for="object_id">Objekts</label>
		                <div class="controls">
			                <select name="object_id" id="object_id">
		                        	<option value="0"> --- </option>
			                        @foreach ($objekti as $single)
										@if ($mode == 'create')
			                        		<option value="{{{ $single->id }}}">{{{ $single->object_name }}}</option>
			                        	@else
											<option value="{{{ $single->id }}}" @if ($single->id==$mpos->object_id) selected="selected" @endif;>{{{ $single->object_name }}}</option>
										@endif
			                        @endforeach
							</select>

							<span class="help-block">
								Piesaistīt pie objekta.
							</span>
		            	</div>
					</div>

					<div class="control-group {{{ $errors->has('owner_id') ? 'error' : '' }}}">
		                <label class="control-label" for="owner_id">Pārdevējs</label>
		                <div class="controls">
			                <select name="owner_id" id="owner_id">
		                        	<option value="0"> --- </option>
			                        @foreach ($sellers as $single)
										@if ($mode == 'create')
			                        		<option value="{{{ $single->id }}}">{{{ $single->owner_name }}}</option>
			                        	@else
											<option value="{{{ $single->id }}}" @if ($single->id==$mpos->owner_id) selected="selected" @endif;>{{{ $single->owner_name }}}</option>
										@endif
			                        @endforeach
							</select>

							<span class="help-block">
								Piesaistīt pie pārdevēja.
							</span>
		            	</div>
					</div>

				@else

					<span class="help-block">
						Ar šo iekārtu jau veiktas transakcijas, pārdevēju un objektu vairs nevar mainīt!
						<br />
						<br />
					</span>

				@endif

			</div>
			<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="control-group">
			<div class="controls">
				<a href="{{{ URL::to('admin/mpos/') }}}" class="btn-cancel">{!! Lang::get('button.cancel') !!}</a>
				<button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop