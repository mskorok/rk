@extends('admin.layouts.default')

@section('title')
{{{ $title }}} :: @parent
@stop

@section('content')
<div class="page-header">
    <h3>
        {{{ $title }}}
    </h3>
</div>

<table id="comments" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>{{{ Lang::get('admin/etickets/table.title') }}}</th>
            <th>Īpašnieks, bērns</th>
            <th>{{{ Lang::get('admin/etickets/table.status') }}}</th>
            <th>{{{ Lang::get('admin/etickets/table.pin') }}}</th>
            <th>{{{ Lang::get('admin/etickets/table.balance') }}}</th>
            <th>{{{ Lang::get('admin/etickets/table.limit_type') }}}</th>
            <th>{{{ Lang::get('admin/etickets/table.limit') }}}</th>
            <th>Brīvpusdienas</th>
            <th>Objekti (skolas)</th>
            <th>{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
        </tr>
    </thead>
</table>
@stop

@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function () {
        oTable = $('#comments').dataTable({
            "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            },
            "aoColumns": [
                {"bSearchable": true},
                {"bSearchable": true},
                {"bSearchable": true},
                {"bSearchable": true},
                {"bSearchable": true},
                {"bSearchable": true},
                {"bSearchable": true},
                {"bSearchable": false},
                {"bSearchable": false},
                {"bSearchable": false}
            ],
            "bProcessing": true,
            "bServerSide": true,
            "columnDefs": [{//this prevents errors if the data is null
                    "targets": "_all",
                    "defaultContent": ""
                }],
            "sAjaxSource": "{!! URL::to('admin/etickets/data') !!}",
        });
    });
</script>
@stop