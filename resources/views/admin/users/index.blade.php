@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

			<div class="pull-right">
				<a href="{{{ URL::to('admin/users/create') }}}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-plus"></span> Izveidot jaunu</a>
			</div>
		</h3>
	</div>

	<table id="users" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span2">{{{ Lang::get('admin/users/table.username') }}}</th>
				<th class="span2">{{{ Lang::get('admin/users/table.surname') }}}</th>
				<th class="span3">{{{ Lang::get('admin/users/table.email') }}}</th>
				<th class="span3">{{{ Lang::get('admin/users/table.phone') }}}</th>
				<th class="span3">{{{ Lang::get('admin/users/table.roles') }}}</th>
				<th class="span2">{{{ Lang::get('admin/users/table.activated') }}}</th>
				<th class="span2">{{{ Lang::get('admin/users/table.created_at') }}}</th>
				<th class="span2">{{{ Lang::get('admin/users/table.last_login') }}}</th>
				<th class="span2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop


@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#users').dataTable( {
		        "sAjaxSource": "{!! URL::to('admin/users/data') !!}",
				"aoColumns": [
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":true},
					{"bSearchable":false},
					{"bSearchable":false},
					{"bSearchable":false},
				],
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop