@extends('admin.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}
	</h3>
</div>

<div class="container">

    @if ($mode=='edit')
    {!! Form::model($user, ['method' => 'POST', 'url' => URL::to('admin/users/' . $user->id . '/edit'),'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
    @else
    {!! Form::open(['method' => 'POST', 'url' => URL::to('admin/users/create'),'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
    @endif

        @include('partials.form.textv', array('name' => 'username', 'label' => 'Vārds', 'value' => isset($user->username) ? $user->username : ''))
        @include('partials.form.textv', array('name' => 'surname', 'label' => 'Uzvārds', 'value' => isset($user->surname) ? $user->surname : ''))
        @include('partials.form.textv', array('name' => 'email', 'label' => 'E-pasts', 'value' => isset($user->email) ? $user->email : ''))
        @include('partials.form.textv', array('name' => 'phone', 'label' => 'Telefons', 'value' => isset($user->phone) ? $user->phone : ''))
        @include('partials.form.password', array('name' => 'password', 'label' => 'Parole'))
        @include('partials.form.password', array('name' => 'password_confirmation', 'label' => 'Atkārtota parole'))

				<!-- Activation Status -->
				<div class="form-group {{{ $errors->has('activated') || $errors->has('confirm') ? 'error' : '' }}}">
					<label class="control-label" for="confirm">Aktīvs lietotājs?</label>
					<div class="controls">
						@if ($mode == 'create')
							<select name="confirm" id="confirm" class="form-control">
								<option value="1"{{{ (Input::old('confirm', 0) === 1 ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
								<option value="0"{{{ (Input::old('confirm', 0) === 0 ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
							</select>
						@else
							<select{{{ ($user->id === Auth::user()->id ? ' disabled="disabled"' : '') }}} name="confirm" id="confirm" class="form-control">
								<option value="1"{{{ ($user->confirmed ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
								<option value="0"{{{ ( ! $user->confirmed ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
							</select>
						@endif
					</div>
				</div>
				<!-- ./ activation status -->

				<!-- Groups -->
				<div class="form-group {{{ $errors->has('role') ? 'error' : '' }}}">
	                <label class="control-label" for="role">Veids</label>
	                <div class="controls">
		                <select name="role" id="role"  class="form-control">
							<option value="0"> --- </option>
		                        @foreach ($roles as $role)
									@if ($mode == 'create')
		                        		<option value="{{{ $role->id }}}"{{{ ( in_array($role->id, $selectedRoles) ? ' selected="selected"' : '') }}}>{{{ $role->name }}}</option>
		                        	@else
										<option value="{{{ $role->id }}}"{{{ ( array_search($role->id, $user->currentRoleIds()) !== false && array_search($role->id, $user->currentRoleIds()) >= 0 ? ' selected="selected"' : '') }}}>{{{ $role->name }}}</option>
									@endif
		                        @endforeach
						</select>
	            	</div>
				</div>
				<!-- ./ groups -->

    @include('partials.form.formend', array('cancel' => 'admin/users/'))
@stop