@extends('admin.layouts.default')

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}

		<div class="pull-right">
			<a href="{{{ URL::to('admin/newbase/create') }}}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus-sign"></i> Izveidot jaunu</a>
		</div>
	</h3>
</div>

<div class="panel panel-primary">
	<form method="post" enctype="multipart/form-data" action="/admin/newbase/edit" autocomplete="off" class="panel-body" role="form">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<input type="hidden" name="id" value="{{{ $record->id }}}" />

		<div class="form-group">
			<label class="control-label" for="name">Vārds:</label>
			<input type="text" name="name" id="name" class="form-control" value="{{{ Input::old('name', $record->name) }}}" />
		</div>

		<div class="form-group">
			<label class="control-label" for="surname">Uzvārds:</label>
			<input type="text" name="surname" id="surname" class="form-control" value="{{{ Input::old('surname', $record->surname) }}}" />
		</div>

		@include('partials.form.ipk', array('objekts' => $record ))

		<div class="form-group">
			<label class="control-label" for="etickets">E-talons:</label>
			<input type="text" name="etickets" id="etickets" class="form-control" value="{{{ Input::old('etickets', $record->etickets) }}}" />
		</div>

		<div class="form-group">
			<label class="control-label" for="grade">Klase:</label>
			<input type="text" name="grade" id="grade" class="form-control" value="{{{ Input::old('grade', $record->grade) }}}" />
		</div>

		<div class="form-group">
			<label class="control-label" for="skola">Skola:</label>
			{!! Form::select('skola', $skolas, Input::old('skola', $record->school_id), ["class" => "form-control", "id" => "skola"]) !!}
		</div>

		<div class="form-group">
			<button type="submit" name="submit" id="submit" class="btn btn-success btn-sm">{!! Lang::get('button.edit') !!}</button>
		</div>
	</form>
</div>
@stop