@extends('admin.layouts.default')


@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}

		<div class="pull-right">
			<a href="{{{ URL::to('admin/newbase/create') }}}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-plus-sign"></span> Izveidot jaunu</a>
		</div>
	</h3>
</div>

<div class="panel panel-primary">
	<form method="post" enctype="multipart/form-data" action="" autocomplete="off" class="panel-body" role="form">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<div class="form-group">
			<label class="control-label" for="pk">Personas kods:</label>
			<input type="text" name="pk" id="pk" class="form-control" value="{{{ Input::old('pk', '') }}}" />
		</div>

		<div class="form-group">
			<label class="control-label" for="eticket">E-talona nr.:</label>
			<input type="text" name="eticket" id="eticket" class="form-control" value="{{{ Input::old('eticket', '') }}}" />
		</div>

		<!-- Form Actions -->
		<div class="form-group">
			<button type="submit" name="submit" id="submit" class="btn btn-success btn-sm">{!! Lang::get('button.search') !!}</button>
		</div>
		<!-- ./ form actions -->
	</form>
</div>
@stop