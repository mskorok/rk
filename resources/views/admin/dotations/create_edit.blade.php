@extends('admin.layouts.default')

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}
	</h3>
</div>

	<form class="form-horizontal" method="post" action="@if (isset($objekts)){!! URL::to('admin/dotations/' . $objekts->id . '/edit') !!}@endif" autocomplete="off">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

		<div class="row">
			<div class="col-md-6">

                <div class="form-group {{{ $errors->has('heading') ? 'error' : '' }}}">
                    <label class="control-label" for="heading">Nosaukums</label>
                    <input type="text" class="form-control" name="heading" value="{{{ Input::old('heading', isset($objekts) ? $objekts->heading : null) }}}" />
                    {!! $errors->first('heading', '<span class="help-inline">:message</span>') !!}
                </div>

            </div>
        </div>

		<div class="control-group">
			<div class="controls">
				<a href="{{{ URL::to('admin/dotations/') }}}" class="btn-cancel">{!! Lang::get('button.cancel') !!}</a>
				<button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
			</div>
		</div>
	</form>
@stop