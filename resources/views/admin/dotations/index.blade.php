@extends('admin.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span3">Skola</th>
				<th class="span3">{{{ Lang::get('admin/dotations/table.name') }}}</th>
				<th class="span1">Cena</th>
				<th class="span2">Pēdējās izmaiņas</th>
				<th class="span2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
	</table>
@stop

@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
		        "sAjaxSource": "{!! URL::to('admin/dotations/data') !!}",
				"aoColumns": [
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable":false},
					{"bSearchable":false},
				],
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop