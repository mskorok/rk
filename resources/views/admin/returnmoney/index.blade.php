@extends('admin.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="page-header">
		<h3>{{{ $title }}}</h3>
	</div>

	@include('partials.form.formstart', array('action' => '/admin/returnmoney/search'))

        @include('partials.form.text', array('name' => 'eticket', 'label' => 'E-talons'))

        <div class="form-group">
            <label class="control-label" for="datums">Datums no:</label>
            <div class="input-group date" id="datetimepicker1">
                <input class="form-control" type="text" name="datums" value="@if (isset($datums)) {!!$datums!!} @endif" />
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>

		<div class="form-group">
			<button type="submit" class="btn btn-success">{!! Lang::get('button.search') !!}</button>
		</div>

		</form>
	</div>
@stop