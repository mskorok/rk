@extends('admin.layouts.default')

@section('content')

<div class="page-header">
	<h3>
		{{{ $title }}}
	</h3>
</div>


	<form class="form-horizontal" method="post" action="@if (isset($objekts)){!! URL::to('admin/auths/' . $objekts->id . '/edit') !!}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">

				<div class="control-group">
					<label class="control-label" for="abc">ID</label>
					<div class="controls">
						<input type="text" name="abc" id="abc" disabled="disabled" value="{{{ isset($objekts) ? $objekts->id : '' }}}" />
					</div>
				</div>

				<div class="control-group {{{ $errors->has('authorisator_name') ? 'error' : '' }}}">
					<label class="control-label" for="authorisator_name">Nosaukums</label>
					<div class="controls">
						<input type="text" name="authorisator_name" id="authorisator_name" value="{{{ Input::old('authorisator_name', isset($objekts) ? $objekts->authorisator_name : null) }}}" />
						{!! $errors->first('authorisator_name', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('secret_key') ? 'error' : '' }}}">
					<label class="control-label" for="secret_key">Secret key</label>
					<div class="controls">
						<input type="text" name="secret_key" id="secret_key" value="{{{ Input::old('secret_key', isset($objekts) ? $objekts->secret_key : null) }}}" />
						{{ $errors->first('secret_key', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<div class="control-group {{{ $errors->has('object_id') ? 'error' : '' }}}">
	                <label class="control-label" for="object_id">Objekts</label>
	                <div class="controls">
		                <select name="object_id" id="object_id">
	                        	<option value="0"> --- </option>
		                        @foreach ($objekti as $single)
									@if ($mode == 'create')
		                        		<option value="{{{ $single->id }}}">{{{ $single->object_name }}}</option>
		                        	@else
										<option value="{{{ $single->id }}}" @if ($single->id==$objekts->object_id) selected="selected" @endif;>{{{ $single->object_name }}}</option>
									@endif
		                        @endforeach
						</select>

						<span class="help-block">
							Piesaistīt pie objekta.
						</span>
	            	</div>
				</div>

			</div>
			<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="control-group">
			<div class="controls">
				<a href="{{{ URL::to('admin/auths/') }}}" class="btn-cancel">{!! Lang::get('button.cancel') !!}</a>
				<button type="submit" class="btn btn-success">{!! Lang::get('button.confirm') !!}</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop