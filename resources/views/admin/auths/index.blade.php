@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="page-header">
		<h3>
			{{{ $title }}}

			<div class="pull-right">
				<a href="{{{ URL::to('admin/auths/create') }}}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-plus"></span> Izveidot jaunu</a>
			</div>
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span3">{{{ Lang::get('admin/auths/table.object') }}}</th>
				<th class="span3">{{{ Lang::get('admin/auths/table.title') }}}</th>
                <th class="span2">Secret key</th>
				<th class="span2">{{{ Lang::get('admin/auths/table.created_at') }}}</th>
				<th class="span2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
	</table>
@stop


@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
		        "sAjaxSource": "{!! URL::to('admin/auths/data') !!}",
				"aoColumns": [
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable": true },
					{"bSearchable":false},
					{"bSearchable":false},
				],
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop