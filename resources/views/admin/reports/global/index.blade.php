@extends('admin.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	
	<div class="page-header">
		<h3>
			{{{ $title }}}
		</h3>
	</div>

	<div class="container">
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			{!! $dateintervalssubmit !!}
		</form>
	</div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">No</th>
				<th class="span2" style="background-color:#333;color:white;">Uz</th>
				<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
			</tr>
		</thead>
		<tbody>
			@if (count($data)>0)
				@foreach ($data as $single)
						<tr>
							<td>@if ($single->from_account_id==0) 
									Bankas pārskaitījums
								@elseif ($single->from_account!==NULL)
									@if ($single->from_account->account_type==3) 
										Tirgotāja konts: {!! $single->from_account->owner->owner_name !!}
									@endif
								@else
									{!! $single->from_account_id !!}
								@endif
							</td>
							<td>
								@if ($single->to_account_id==0)
									Nav definēts konts
								@elseif ($single->to_account!==NULL)
									@if ($single->to_account->account_type==1) 
										Lietotāja konts: {!! $single->to_account->owner->owner_name !!}
									@endif
								@else
									@if ($single->to_account_id == 999999)
										Bankas pārskaitījums
									@else
										{!! $single->to_account_id !!}
									@endif
								@endif
							</td>
							<td>
								{!!sprintf("%.2f",$single->amount/100)!!} EUR
							</td>
							<td>{!! $single->created_at !!}</td>
							</td>
						</tr>
				@endforeach
			@endif
		</tbody>
	</table>

	@if (count($data)>0) 
	<div style="margin-top:20px;" class="pull-right">
		{!! $data->appends(array('no' => $no, 'lidz' => $lidz))->render() !!}
	</div>
	@endif

	@if ($totals!==0)
	<div style="margin-top:20px;" class="pull-left">
		<p style="font-weight:bold;">Apgrozījums par periodu:</p>
		<p>Ienākošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["in"]/100)!!} EUR
		</span></p>
		<p>Izejošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["out"]/100)!!} EUR
		</span></p>
		<p>Atlikums sistēmā: <span style="font-weight:bold;">
			{!!sprintf("%.2f",($totals["in"]-$totals["out"])/100)!!} EUR
		</span></p>
	</div>
	@endif
@stop