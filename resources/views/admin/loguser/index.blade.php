@extends('admin.layouts.default')


@section('title')
{{{ $title }}} :: @parent
@stop


@section('content')
<div class="page-header">
    <h3>
        {{{ $title }}}

    </h3>
</div>

<table id="comments" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>{{{ Lang::get('admin/loguser/table.user') }}}</th>
            <th>{{{ Lang::get('admin/loguser/table.op') }}}</th>
            <th>{{{ Lang::get('admin/loguser/table.ip') }}}</th>
            <th>{{{ Lang::get('admin/loguser/table.date') }}}</th>
            <th>{{{ Lang::get('admin/loguser/table.data') }}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
        <tr id="{{ $row["id"] }}" class="context-menu">
            <td>{{{ $row["username"] }}}</td>
            <td>{{{ $row["op"] }}}</td>
            <td>{{{ $row["ip"] }}}</td>
            <td style="min-width: 100px; text-align: center;">{{{ $row["created_at"] }}}</td>
            <td style="word-break: break-word;">{{{ $row["value"] }}}</td>
        </tr>
        @endforeach
    </tbody>    
</table>
@stop


@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function () {
        oTable = $('#comments').dataTable({
            serverSide: false,
            processing: true,            
            //ajax: "{!! URL::to('admin/loguser/data') !!}",
            language: {
                "emptyTable": "Nav datu"
            },
            columnDefs: [
                { "searchable": false, "targets": 4 }
            ],            
        });
    });
</script>
@stop