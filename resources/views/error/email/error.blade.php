@extends('error.email.layouts.common')

@section('title')
    @include('error.email.subject')
@stop

@section('emailbody')
    <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
        <tr>
            <td valign="top">
                {!! $emailbody !!}

                {!! $trace !!}

                <h2>PHP environment:</h2>
                <pre style="max-width:600px;">
$_REQUEST = {!! print_r($_REQUEST, true) !!}
                </pre>
            </td>
        </tr>
    </table>
@stop