		<div class="form-group">
			<a href="{!! URL::to($cancel) !!}" class="btn btn-cancel">{{{ Lang::get('button.cancel') }}}</a>
			<button type="submit" name="submit" class="btn btn-danger">{{{ Lang::get('button.delete') }}}</button>
		</div>

	{!! Form::close() !!}
</div>