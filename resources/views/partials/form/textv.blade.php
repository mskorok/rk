<div class="form-group {{{ $errors->has($name) ? 'has-error' : '' }}}">
	<label class="control-label" for="{!! $name !!}">{!! $label !!}</label>
	{!! Form::text($name, isset($value) ? $value : '', array('class' => 'form-control')) !!}
</div>