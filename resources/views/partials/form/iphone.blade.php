<div class="form-group {{{ $errors->has('phone') ? 'has-error' : '' }}}">
	<label class="control-label" for="phone">@if (isset($fieldname)) {!! $fieldname !!} @else Telefons @endif</label>
	<input class="form-control" type="text" name="phone" id="phone" value="{{{ Input::old('phone') }}}" />
	{!! $errors->first('phone', '<span class="help-inline">:message</span>') !!}
</div>