<div class="form-group {{{ $errors->has($name) ? 'has-error' : '' }}}">
	<label class="control-label" for="{!!$name!!}">{!!$label!!}</label>
	{!! Form::select($name, $all_data, $selected_data, array('class' => 'form-control')) !!}
</div>