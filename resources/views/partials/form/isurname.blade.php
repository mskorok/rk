<div class="form-group {{{ $errors->has('surname') ? 'has-error' : '' }}}">
	<label class="control-label" for="surname">Uzvārds</label>
		<input class="form-control" type="text" name="surname" id="surname" value="{{{ Input::old('surname', isset($objekts->surname) ? $objekts->surname : null) }}}" />
		{!! $errors->first('surname', '<span class="help-inline">:message</span>') !!}
</div>