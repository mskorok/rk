<div class="form-group {{{ $errors->has('pk') ? 'has-error' : '' }}}">
	<label class="control-label" for="pk">Personas kods</label>
		<input class="form-control" type="text" name="pk" id="pk" maxlength="12" value="{{{ Input::old('pk', isset($objekts->pk) ? $objekts->pk : null) }}}" />
		{!!  $errors->first('pk', '<span class="help-inline">:message</span>') !!}
</div>