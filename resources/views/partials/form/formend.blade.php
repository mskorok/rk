		<div class="form-group">
			<a href="{!! URL::to($cancel) !!}" class="btn btn-cancel">{{{ Lang::get('button.cancel') }}}</a>
			<button type="submit" class="btn btn-success">{{{ Lang::get('button.confirm') }}}</button>
		</div>

	{!! Form::close() !!}
</div>