<?php
    $options = array_merge_recursive(['class' => 'form-control'], (!empty($options) ? (array)$options : []));
    if(is_array($options['class'])){
        $options['class'] = implode(' ', $options['class']);
    }
?>
<div class="form-group {{{ $errors->has($name) ? 'has-error' : '' }}}">
	<label class="control-label" for="{!! $name !!}">{!! $label !!}</label>
	{!! Form::text($name, null, $options) !!}
</div>