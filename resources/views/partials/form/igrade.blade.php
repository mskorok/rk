<div class="form-group {{{ $errors->has('grade') ? 'has-error' : '' }}}">
	<label class="control-label" for="grade">Klase (1A-12Z)</label>
	<input class="form-control" type="text" name="grade" id="grade" maxlength="2" value="{{{ Input::old('grade', isset($objekts->grade) ? $objekts->grade : '1') }}}" />
	{!! $errors->first('grade', '<span class="help-inline">:message</span>') !!}
</div>