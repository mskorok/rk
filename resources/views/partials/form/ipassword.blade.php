<div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
	<label class="control-label" for="password">Parole</label>
	<input class="form-control" type="password" name="password" id="password" value="{{{ Input::old('password', isset($objekts->password) ? $objekts->password : null) }}}" />
	{!! $errors->first('password', '<span class="help-inline">:message</span>') !!}
</div>