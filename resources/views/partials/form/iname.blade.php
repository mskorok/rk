<div class="form-group {{{ $errors->has('name') ? 'has-error' : '' }}}">
	<label class="control-label" for="name">Vārds</label>
		<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', isset($objekts->name) ? $objekts->name : null) }}}" />
		{!! $errors->first('name', '<span class="help-inline">:message</span>') !!}
</div>