<div class="form-group {{{ $errors->has('etickets') ? 'has-error' : '' }}}">
	<label class="control-label" for="etickets">E-taloni</label>
		<input class="form-control" type="text" name="etickets" id="etickets" value="{{{ Input::old('etickets', isset($objekts->etickets) ? $objekts->etickets : null) }}}" />
		{!! $errors->first('etickets', '<span class="help-inline">:message</span>') !!}
</div>