<div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
	<label class="control-label" for="phone">@if (isset($fieldname)) {!! $fieldname !!} @else E-pasts @endif</label>
	<input class="form-control" type="text" name="email" id="email" value="{!! Input::old('email', isset($objekts->email) ? $objekts->email : null) !!}" />
	{!! $errors->first('email', '<span class="help-inline">:message</span>') !!}
</div>