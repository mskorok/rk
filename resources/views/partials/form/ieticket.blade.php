<div class="form-group {{{ $errors->has('eticket') ? 'has-error' : '' }}}">
    <label class="control-label" for="eticket">E-talons</label>
    <select class="form-control" name="eticket" id="eticket">
            @foreach ($etaloni as $single)
            	<option value="{{{ $single->nr }}}">{{{ $single->nr }}}</option>
            @endforeach
	</select>
	{!! $errors->first('eticket', '<span class="help-inline">:message</span>') !!}
</div>