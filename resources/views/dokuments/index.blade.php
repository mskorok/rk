@extends('site.layouts.default')

@section('content')

@include('partials.form.formstartfile', array('action' => $action))
		<div class="row">
			<div class="col-md-12">

                <p>{{{ $dokuments_text }}}</p>

				<div class="form-group">
                     <div style="position:relative;">
                        <a class='btn btn-primary' style="width:400px;height:42px;" href='javascript:;'>
                            Izvēlēties dokumentu (JPG, PNG, GIF, PDF līdz 2MB)
                            <input type="file" style='position:absolute;z-index:2;top:0;left:0;width:400px;height:42px;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                        </a>
                        &nbsp;
                        <span class='label label-info' id="upload-file-info"></span>
                    </div>
                </div>
			</div>
		</div>
		<div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <a class="btn-cancel" href="/etickets">Atcelt</a>
                    <button type="submit" class="btn btn-success">Saglabāt</button>
                </div>
			</div>

		</div>
	</form>
</div>
@stop