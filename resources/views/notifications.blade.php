@if (count($errors->all()) > 0)
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Kļūda!</h4>
    @if ($errors->has('recaptcha_response_field'))
    Nepareizs drošības kods!
    @else
    Skatiet zemāk esošo formu.
    @if (App::environment() == 'local')
        @if ( ! $errors instanceof \Illuminate\Support\ViewErrorBag)
            {!! $errors !!}
        @endif
    @endif
    @endif
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissable">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
    @if(is_array($message))
        @foreach ($message as $m)
            {!! $m !!}
        @endforeach
    @else
        {!! $message !!}
    @endif
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4>Kļūda!</h4>
    @if(is_array($message))
        @foreach ($message as $m)
            @if(is_array($m))
                {!! $m[0] !!}
            @else
                {!! $m !!}
            @endif
        @endforeach
    @else
<?php
    $m = json_decode($message);
    $mm = (json_last_error() === JSON_ERROR_NONE) ? $m : $message;
?>
    {!! $mm !!}
    @endif
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissable">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4>Brīdinājums</h4>
    @if(is_array($message))
    @foreach ($message as $m)
    {!! $m !!}
    @endforeach
    @else
    {!! $message !!}
    @endif
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-dismissable">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4>Uzmanību</h4>
    @if(is_array($message))
    @foreach ($message as $m)
    {!! $m !!}
    @endforeach
    @else
    {!! $message !!}
    @endif
</div>
@endif
