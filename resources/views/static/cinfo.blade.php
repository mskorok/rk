@extends('site.layouts.modal')

@section('title')
    {{{ $title }}} :: @parent
@stop

@section('content')
    <div style="text-align:center;margin-top:50px;">
        <h2>SIA "RĪGAS KARTE"<br />
            Reģ.Nr.: 40003979933<br />
            Adrese: Vīlandes iela 6-3, Rīga, LV-1010<br />
            Kontakttālrunis: <a href="tel:67326300">67326300</a><br />
            E-pasts: <a href="mailto:support@skolas.rigaskarte.lv">support@skolas.rigaskarte.lv</a>
        </h2>
    </div>
@stop