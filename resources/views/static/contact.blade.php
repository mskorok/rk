@extends('site.layouts.modal')

@section('title')
    {{{ $title }}} :: @parent
@stop


@section('content')

<div class="container">
	<form class="form-horizontal" method="post" action="" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<div class="form-group {{{ $errors->has('epasts') ? 'has-error' : '' }}}">
			<label class="control-label" for="epasts">E-pasts</label>
			<input class="form-control" type="text" name="epasts" id="epasts" @if (isset(Auth::user()->id)) disabled @endif 
			value="{{{ Input::old('epasts', isset(Auth::user()->email) ? Auth::user()->email : '' ) }}}" />
		</div>

		<div class="form-group {{{ $errors->has('talrunis') ? 'has-error' : '' }}}">
			<label class="control-label" for="talrunis">Tālrunis</label>
			<input class="form-control" type="text" name="talrunis" id="talrunis" @if (isset(Auth::user()->id)) disabled @endif 
			value="{{{ Input::old('talrunis', isset(Auth::user()->phone) ? Auth::user()->phone : '') }}}" />
		</div>

		<div class="form-group {{{ $errors->has('message') ? 'has-error' : '' }}}">
			<label class="control-label" for="message">Ziņa</label>
			<textarea class="form-control" name="message" id="message">{{{ Input::old('message', '') }}}</textarea>
		</div>

		<div class="form-group">
			{!! Recaptcha::render() !!}
		</div>

		<!-- Form Actions -->
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-sm">{!! Lang::get('button.confirm') !!}</button>
		</div>
		<!-- ./ form actions -->
	</form>
</div>
@stop