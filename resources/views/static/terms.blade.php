@extends('site.layouts.modal')

@section('title')
{{{ $title }}} :: @parent
@stop

@section('content')

<style type="text/css">
    @page { margin-left: 1.18in; margin-right: 0.59in; margin-top: 0.79in; margin-bottom: 0.79in }
    p { margin-bottom: 0.1in; direction: ltr; line-height: 120%; text-align: justify; orphans: 2; widows: 2 }
</style>

<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i><b>Sistēmas
                        pārvaldnieks</b></i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
– SIA „RĪGAS KARTE”, reģistrācijas numurs 40003979933,
juridiskā adrese Vīlandes iela 6-3, Rīga, LV-1010.</span></font></font></p>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i><b>Sistēma</b></i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
– </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">norēķinu
sistēma maksājumu veikšanai par Tirgotāju sniegtajiem
pakalpojumiem ar personalizēto viedkarti vai Skolēna E karti
(turpmāk tekstā abas – Skolēna E karte), kuru nodrošina
Sistēmas pārvaldnieks.</span></font></font></p>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i><b>Tirgotājs</b></i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
– </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">preču
pārdevējs vai pakalpojumu sniedzējs, kas noslēdzis līgumu ar</span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
</span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
pārvaldnieku</span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
</span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">par
Sistēmas izmantošanu un ir apņēmies pieņemt Lietotāju Skolēna
E kartes kā norēķinu līdzekli.</span></font></font></p>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i><b>Lietotājs</b></i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
– </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Skolēna
E kartes turētājs, kuru Konta pārvaldnieks ir piesaistījis savam
Kontam.</span></font></font></p>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i><b>Konta
                        pārvaldnieks</b></i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i>
                    – </i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">persona,
kas ir reģistrējusies Sistēmā, lai Sistēmā veiktu atļautas
darbības ar savu Kontu Lietotāja interesēs.</span></font></font></p>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i><b>Konts</b></i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
– </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
sastāvdaļa, kas ietver informāciju par Konta pārvaldnieku, Kontam
piesaistītajiem Lietotājiem, kontā un Lietotāju Skolēna E kartēs
esošajiem naudas līdzekļiem, ar tiem veiktajiem darījumiem un
kura ir pieejama Konta pārvaldniekam Pašapkalpošanās portālā.</span></font></font></p>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i><b>Pašapkalpošanās
                        portāls</b></i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
– interneta </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">tīmekļa
vietne </span></font></font><a href="https://skolas.rigaskarte.lv/"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">https://skolas.rigaskarte.lv</span></font></font></a><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">.</span></font></font></p>
<p align="justify" style="margin-bottom: 0.19in; line-height: 100%">
    <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><i><b>Pašapkalpošanās
                        portāla viesis</b></i></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
– jebkura persona, kas ir atvērusi jebkuru no Pašapkalpošanās
portāla lapām, izmantojot tīmekļa pārlūkprogrammu.</span></font></font></p>
<ol>
    <li/>
    <p align="justify" style="margin-bottom: 0; line-height: 100%">
        <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><b>Vispārējie
                        noteikumi:</b></span></font></font></p>
    <ol>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Pašapkalpošanās
		portāla un Skolēna E kartes lietošanas noteikumi (turpmāk
		tekstā – Noteikumi) nosaka Sistēmas izmantošanas kārtību.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldniekam ir tiesības jebkurā laikā mainīt Noteikumus, ja
		tam ir pamatots iemesls, un tie ir spēkā no brīža, kad tiek
		publicēti Pašapkalpošanās portālā. Konta pārvaldnieks tiek
		automātiski informēts par šādām izmaiņām un viņam ir
		pienākums piekrist šīm izmaiņām, lai lietotu Pašapkalpošanās
		portālu.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Noteikumi
		ir saistoši Konta pārvaldniekam ar brīdi, kad Konta pārvaldnieks
		Noteikumos noteiktajā kārtībā ir aktivizējis Kontu, un ir
		uzskatāmi par juridiski nozīmīgu vienošanos starp Sistēmas
		pārvaldnieku un Konta pārvaldnieku. Šī vienošanās ir spēkā
		uz nenoteiktu laiku un var tikt izbeigta Noteikumos paredzētajā
		kārtībā.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks ir tiesīgs ierakstīt un apstrādāt visu
		informāciju, kas tam ir nodota, lietojot Sistēmu, un reģistrēt
		Konta pārvaldnieka un Lietotāja veiktās darbības, izmantojot
		Sistēmu, un, ja nepieciešams, izmantot šo informāciju veikto
		darījumu pamatošanai un pierādīšanai. Šāda informācija tiek
		glabāta pie Sistēmas pārvaldnieka 5 (piecus) gadus kopš tās
		radīšanas brīža. </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Šī
		punkta izpildei Sistēmas pārvaldniekam ir tiesības sadarboties
		ar fizisko personu datu operatoriem.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks, vienpusēji bez iepriekšējās paziņošanas Konta
		pārvaldniekam, ir tiesīgs bloķēt viņa Kontu un darbības
		Sistēmā, ja Konta pārvaldnieks ir pārkāpis Noteikumus vai
		radies pamatots iemesls aizdomām, ka Sistēma tiek lietota
		nelegālām vai prettiesiskām darbībām, vai krāpnieciskos
		nolūkos, vai, lai legalizētu noziedzīgā ceļā iegūtus naudas
		līdzekļus. Konta darbība tiek bloķēta līdz brīdim, kamēr
		Sistēmas pārvaldnieks ir pilnībā</span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
		</span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">pārliecinājies
		par pretējo. Ja iepriekš minētā informācija apstiprinās,
		Sistēmas pārvaldnieks ir tiesīgs vienpusēji, par to iepriekš
		nebrīdinot Konta pārvaldnieku, pieņemt lēmumu par Konta
		pārvaldnieka Konta slēgšanu vai citām darbībām, ja to
		pieprasa kompetentas valsts pārvaldes institūcijas, izziņas
		iestādes vai tiesa.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks nodrošina, ka Konta pārvaldnieks izmanto Sistēmu
		bez maksas, izņemot gadījumus, kad, veicot pārskaitījumu uz
		Sistēmas pārvaldnieka bankas kontu, maksājuma uzdevuma
		izpildītājs (piemēram, banka, kurā Konta pārvaldniekam ir
		atvērts bankas konts) ietur komisiju saskaņā ar konkrētā
		maksājuma izpildītāja spēkā esošo un apstiprināto cenrādi.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks ir atbrīvots no atbildības par pilnīgu vai daļēju
		savu saistību neizpildi, ja tāda neizpilde ir radusies
		nepārvaramas varas apstākļu rezultātā.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Jautājumiem,
		kas nav atrunāti Noteikumos, piemērojami Latvijas Republikas
		normatīvie akti.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Gadījumā,
		ja kāds no Noteikumu nosacījumiem zaudē spēku kāda iemesla
		dēļ, tai skaitā normatīvo aktu grozījumu rezultātā, tad šis
		nosacījums (ciktāl tas ir spēkā neesošs) nav piemērojams un
		tiek uzskatīts par neiekļautu Noteikumu tekstā, neietekmējot
		pārējos Noteikumu nosacījumus.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Visas
		sūdzības, pretenzijas un iesniegumi (izņemot iesniegumus par
		naudas līdzekļu atgriešanu un konta dzēšanu, kurus var
		iesniegt rakstveidā, nosūtot </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">uz
		Sistēmas pārvaldnieka juridisko adresi</span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">)
		Sistēmas pārvaldniekam iesniedzami Pašapkalpošanās portāla
		sadaļā „Palīdzība”, kas tai skaitā ir pieejama arī
		Pašapkalpošanās portāla viesiem.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Ar
		Noteikumu izpildi saistītie strīdi un domstarpības , ja tie nav
		atrisināmi sarunu ceļā, ir izskatāmi Latvijas Republikas tiesā.</span></font></font></p>
    </ol>
</ol>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <br/>

</p>
<ol start="2">
    <li/>
    <p align="justify" style="margin-bottom: 0; line-height: 100%">
        <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><b>Konta
                        aktivizācija:</b></span></font></font></p>
    <ol>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Pašapkalpošanās
		portāla viesis var aktivizēt Kontu Pašapkalpošanās portālā,
		autentificējoties, izmantojot Rīgas pašvaldības SIA “Rīgas
		satiksme” Elektronisko pieteikumu sistēmas autentifikācijas
		rīkus ar internetbankas starpniecību un tiešsaistē, aizpildot
		un nosūtot Sistēmas pārvaldniekam reģistrācijas formu. Pēc
		reģistrācijas formas apstrādes uz Pašapkalpošanās portāla
		viesa norādīto e-pasts adresi tiks nosūtīta reģistrācijas
		apstiprinājuma vēstule. Lai pabeigtu reģistrāciju un kļūtu
		par Konta pārvaldnieku, Pašapkalpošanās portāla viesim
		jāapstiprina Konta aktivizācija, izmantojot vēstulē norādīto
		saiti.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Aizpildot
		reģistrācijas formu, Pašapkalpošanās portāla viesim ir
		pienākums iepazīties ar Noteikumiem un piekrist tiem, izdarot
		atzīmi Pašapkalpošanās portāla norādītajā vietā, pretējā
		gadījumā Konta aktivizācija tiks atteikta un Pašapkalpošanās
		portāla viesis nekļūs par Konta pārvaldnieku.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		lietošana, t.sk. lietošanas uzsākšana, vai citas Noteikumos vai
		normatīvajos aktos paredzētās Pašapkalpošanās portāla viesa
		darbības, ir uzskatāma par Pašapkalpošanās portāla viesa
		piekrišanu Noteikumiem un nodibina līgumisku saistību starp
		Konta pārvaldnieku un Sistēmas pārvaldnieku.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Aktivizējot
		kontu, Konta pārvaldnieks apliecina, ka tam nav tiesisku šķēršļu
		uzņemties tiesības un pienākumus, kas izriet no Noteikumiem.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Konta
		pārvaldnieks apņemas rūpīgi glabāt un aizsargāt sava profila
		piekļuves paroli. Konta pārvaldniekam ir jāpieliek visas pūles,
		lai novērstu paroles nonākšanu trešo personu rīcībā, kā
		rezultātā ir iespējams veikt nesankcionētas darbības ar Kontu.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Konta
		pārvaldnieks ir atbildīgs Sistēmas pārvaldniekam par radītajiem
		zaudējumiem, uzņemtajām saistībām vai citām darbībām, kuras
		ir prettiesiski veiktas ar Kontu.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Ievērojot
		Latvijas Republikas normatīvo aktu prasības, Sistēmas
		pārvaldnieks apņemas apstrādāt Pašapkalpošanās portāla
		viesa vai Konta pārvaldnieka iegūtos personas datus tikai
		Noteikumos paredzētajiem mērķiem.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Ja
		Konta pārvaldnieks 12 (divpadsmit) mēnešu laikā Kontā nav
		veicis nevienu darījumu un Kontā nav naudas līdzekļu, Sistēmas
		pārvaldniekam ir tiesības dzēst Kontu.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Konta
		pārvaldnieks var pieprasīt Sistēmas pārvaldniekam dzēst savu
		Kontu, </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">nosūtot
		rakstveida iesniegumu uz Sistēmas pārvaldnieka juridisko adresi:
		Vīlandes iela 6-3, Rīga, LV-1010, vai nosūtot iesniegumu
		tiešsaistē, izmantojot Pašapkalpošanās portālu.</span></font></font></p>
    </ol>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%"><br/>
</p>
<ol start="3">
    <li/>
    <p align="justify" style="margin-bottom: 0; line-height: 100%; page-break-before: always">
        <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><b>Skolēna
                        E kartes piesaistīšana Kontam un papildināšana:</b></span></font></font></p>
    <ol>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Konta
		pārvaldnieks var piesaistīt savam Kontam Skolēna E kartes un
		papildināt tās uzreiz pēc Konta aktivizācijas, ielogojoties
		Pašapkalpošanās portālā.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Piesaistot
		Skolēna E karti savam Kontam, Konta pārvaldnieks apliecina, ka
		rīkojas Skolēna E kartes Lietotāja interesēs, ir tiesīgs
		apstrādāt Lietotāja personas datus, uzņemas atbildību par
		Lietotāja personas datu nodošanu Sistēmas pārvaldniekam un,
		ievērojot </span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Latvijas
		Republikas</span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">
		normatīvo aktu prasības, piekrīt to turpmākai apstrādei
		Noteikumos paredzētajiem mērķiem.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Konta
		pārvaldnieks ir atbildīgs par Sistēmas pārvaldniekam nodoto
		Lietotāja personas datu pareizību, zaudējumiem un/vai
		kaitējumiem, kas radušies sakarā ar kļūdaino vai nepilnīgo
		datu nodošanu Sistēmas pārvaldniekam.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Lai
		papildinātu Kontam piesaistīto Skolēna E karti, Konta
		pārvaldniekam Sistēmā jānorāda papildināšanas summa, par
		kuru Sistēma automātiski sagatavo maksājuma uzdevumu.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Konta
		papildināšana notiek, veicot pārskaitījumu uz maksājumu
		uzdevumā norādīto Sistēmas pārvaldnieka bankas kontu. Veicot
		pārskaitījumu, Sistēmas pārvaldniekam ir obligāti jānorāda
		maksājuma mērķis, proti, darījuma identifikācijas numurs un
		Skolēna E kartes numurs.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Naudas
		pārskaitījuma izpildes laiks uz Sistēmas pārvaldnieka bankas
		kontu ir noteikts konkrētā maksājuma izpildītāja spēkā
		esošajos noteikumos.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks atbild šajos Noteikumos noteiktajā kārtībā par
		Kontā un Skolēna E kartēs esošajiem naudas līdzekļiem, to
		saglabāšanu un par kļūdaini izpildītiem darījumiem, ja tas
		saistīts ar naudas līdzekļu pazušanu vai kļūdaini izpildītais
		darījums ir radies Sistēmas trūkumu vai nepareizas darbības
		rezultātā, izņemot, ja attiecīgo trūkumu vai nepareizo darbību
		izraisījis pats Konta pārvaldnieks un/vai Lietotājs, vai tas
		radies trešās personas apzinātas rīcības rezultātā. Sistēmas
		pārvaldnieks nav atbildīgs par Konta pārvaldniekam un/vai
		Lietotājam radītajiem netiešajiem zaudējumiem un negūto peļņu.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieka atbildība pret Konta pārvaldnieku un Lietotāju
		tiek noteikta saskaņā ar Noteikumiem, nepārsniedzot naudas
		līdzekļu apmēru, ko Sistēmas pārvaldnieka vainas dēļ
		zaudējis Konta pārvaldnieks un/vai Lietotājs.</span></font></font></p>
    </ol>
</ol>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <br/>

</p>
<ol start="4">
    <li/>
    <p align="justify" style="margin-bottom: 0; line-height: 100%">
        <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><b>Norēķini
                        par Tirgotāja sniegtajiem pakalpojumiem, izmantojot Skolēna E
                        karti:</b></span></font></font></p>
    <ol>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Lietotājs
		Skolēna E kartē ieskaitītos līdzekļus drīkst izmantot tikai
		norēķiniem ar Sistēmā norādītajiem Tirgotājiem.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Lietotājam,
		norēķinoties ar Tirgotāju par saņemtajām precēm vai
		pakalpojumiem, izmantojot Skolēna E karti, tiek piemērots
		Tirgotāja noteiktais preču vai pakalpojumu spēkā esošais un
		apstiprinātais cenrādis.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Pārskati
		ir pieejami Pašapkalpošanās portālā, Konta pārvaldniekam
		ielogojoties. Pēc Konta pārvaldnieka pieprasījuma, Sistēmas
		pārvaldnieks nodrošina Konta pārvaldnieku ar Kontā veikto
		darījumu pārskatu pēdējo 5 (piecu) gadu laikā.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Ja
		Konta atlikums nav pietiekams vai, ja maksājums ar Sistēmas
		palīdzību nav piemērots veikšanai citu iemeslu dēļ, Sistēmas
		pārvaldniekam ir tiesības neizpildīt maksājumu. Sistēmas
		pārvaldnieks nav atbildīgs par zaudējumiem vai kaitējumu, kas
		šajā sakarā var rasties Konta pārvaldniekam un/vai Lietotājam.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks nav atbildīgs par Tirgotāja preču vai pakalpojumu
		kvalitāti, trūkumiem, drošumu un atbilstību saskaņā ar spēkā
		esošajiem Latvijas Republikas normatīviem aktiem. Visas prasības
		un pretenzijas saistībā ar iepriekš minēto ir iesniedzamas
		Tirgotājam vai citām personām, kas atbild par preču un
		pakalpojumu kvalitāti, drošumu, trūkumiem vai to atbilstību,
		saskaņā ar spēkā esošajiem Latvijas Republikas normatīvajiem
		aktiem.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks nav atbildīgs par Tirgotāja darbībām vai
		bezdarbību, kā arī nesniedz Konta pārvaldniekam un Lietotajam
		nekādas garantijas un apliecinājumus par Tirgotāju un tā
		pārdotajām precēm vai sniegtajiem pakalpojumiem. Konta
		pārvaldnieks un Lietotājs uzņemas visu darījumu risku ar
		Tirgotāju.</span></font></font></p>
    </ol>
</ol>
<p align="justify" style="margin-bottom: 0; line-height: 100%">
    <br/>

</p>
<ol start="5">
    <li/>
    <p align="justify" style="margin-bottom: 0; line-height: 100%">
        <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV"><b>Kontā
                        vai Skolēna E kartē ieskaitīto līdzekļu atgriešana:</b></span></font></font></p>
    <ol>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Kontā
		vai Lietotāju Skolēna E kartēs esošo naudas līdzekļu
		atgriešana notiek, pamatojoties uz Sistēmas pārvaldniekam
		iesniegto rakstveida iesniegumu, nosūtot to uz Sistēmas
		pārvaldnieka juridisko adresi: Vīlandes iela 6-3, Rīga, LV-1010,
		vai nosūtot iesniegumu tiešsaistē, izmantojot Pašapkalpošanās
		portālu. Iesniegumā nepieciešams minēt Konta vai Skolēna E
		kartes numuru, Kontā vai Skolēna E kartē esošo naudas līdzekļu
		atlikumu, kā arī rekvizītus, uz kuriem Sistēmas pārvaldniekam
		pārskaitīt naudas līdzekļus.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks ir tiesīgs pieprasīt no iesnieguma iesniedzēja
		nepieciešamo informāciju, lai identificētu viņa tiesības
		rīkoties ar Kontu vai Skolēna e kartē esošajiem līdzekļiem.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Sistēmas
		pārvaldnieks Kontā vai Skolēna E kartēs esošos naudas
		līdzekļus atmaksā, pārskaitot tos uz iesniegumā norādīto
		Konta pārvaldnieka bankas kontu ne vēlāk kā 10 (desmit) darba
		dienu laikā no iesnieguma saņemšanas brīža.</span></font></font></p>
        <li/>
        <p align="justify" style="margin-bottom: 0; line-height: 100%">
            <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="lv-LV">Par
		Kontā vai Skolēna E kartēs esošo naudas līdzekļu atmaksu tiek
		ieturēta komisijas maksa 10% (desmit procentu) apmērā no
		iesnieguma iesniedzējam izmaksājamās summas.</span></font></font></p>
    </ol>
</ol>

@stop