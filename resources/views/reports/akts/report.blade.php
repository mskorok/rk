<div style="text-align:center;">
    <h3>Darbu izpildes akts Nr. {{{ $akts_nr.$nrt }}}</h3>
    <p>(pie līguma nr. {{{ $seller->contract_nr }}})</p>
</div>

<table width="100%" border="0">
    <tr>
        <td colspan="2" style="height:120px;">
            Datums: {!! date("Y").'. gada '.date("d").'. '.$months[intval(date("m"))] !!}
        </td>
    </tr>
    <tr>
        <td width="50%">
            <b>PASŪTĪTĀJS: </b><br />
            {{{ $settings->name }}}<br />
            {{{ $settings->jur_adrese }}}    <br />
            PVN reģ. Nr. {{{ $settings->reg_nr }}}    <br />
        </td>
        <td width="50%" valign="top">
            <b>TIRGOTĀJS: </b><br />
            {{{ $seller->seller_name }}}<br />
            Jurid.adrese: {{{ $seller->jur_adrese }}}<br />
            PVN reģ. Nr. {{{ $seller->reg_nr }}}<br />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" style="height:120px;">
            Darbu izpildes periods: {!! $no !!} - {!! $lidz !!}
        </td>
    </tr>
    <tr>
        <td colspan="2">
            Ar šo aktu PUSES apstiprina sekojošo:
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right" width="70%">
            <table width="100%" border="0">
                <tr>
                    <td align="right">Atlikums uz {!! $no !!}:</td>
                    <td style="font-weight:bold;">{!! sprintf("%.2f",($start_balance/100)) !!}</td>
                    <td>EUR</td>
                </tr>
                <tr>
                    <td align="right">Ieskaitīts kontā:</td>
                    <td style="font-weight:bold;">{!! sprintf("%.2f",($total_in/100)) !!}</td>
                    <td>EUR</td>
                </tr>
                <tr>
                    <td align="right">Pārskaitīts uz TIRGOTĀJA norēķinu kontu:</td>
                    <td style="font-weight:bold;">{!! sprintf("%.2f",($outgoing_paid/100)) !!}</td>
                    <td>EUR</td>
                </tr>
                <tr>
                    <td align="right">Atlikums uz {!! $lidz !!} </td>
                    <td style="font-weight:bold;">{!! sprintf("%.2f",($end_balance/100)) !!}</td>
                    <td>EUR</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="height:120px;">
            Saskaņā ar līguma 4.1. p., TIRGOTĀJS par veiktajiem darījumiem maksā PASŪTĪTĀJAM komisiju no kopējas summas. Komisijas summa ar PVN ir ieturēta uzreiz no pārskaitāmās summas.
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="50%" border="0">
                <tr>
                    <td align="right">Komisijas procents:</td>
                    <td>0%</td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right">Summa bez PVN:</td>
                    <td style="font-weight:bold;">{!! sprintf("%.2f",(round($outgoing_comision/1.21,2)/100)) !!}</td>
                    <td>EUR</td>
                </tr>
                <tr>
                    <td align="right">PVN:</td>
                    <td style="font-weight:bold;">{!! sprintf("%.2f",(round($outgoing_comision-($outgoing_comision/1.21),2)/100)) !!}</td>
                    <td>EUR</td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold;">Kopā ar PVN:</td>
                    <td style="font-weight:bold;">{!! sprintf("%.2f",($outgoing_comision/100)) !!}</td>
                    <td>EUR</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="50%" style="height:120px;">
            <b>PASŪTĪTĀJS: </b><br />
            {{{ $settings->name }}}<br />
            <br />
            _____________________   <br />
        </td>
        <td width="50%" style="height:120px;">
            <b>IZPILDĪTĀJS: </b><br />
            {{{ $seller->seller_name }}}<br />
            <br />
            {{{ $seller->contract_person }}}<br />
        </td>
    </tr>
</table>