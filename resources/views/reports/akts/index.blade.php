@extends('site.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="container">
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			<div class="row">
				<div class="form-group col-md-4">
                    <label class="control-label" for="et">Tirgotājs: </label><br />
					<select class="form-control input-sm" name="seller">
						<option value="0"> -Izvēlieties tirgotāju- </option>
						@foreach ($all_sellers as $s)
							<option value="{!! $s->id !!}">{!! $s->seller_name !!}</option>
						@endforeach
					</select>
				</div>

				{!! $dateintervalssubmit !!}
			</div>
		</form>
	</div>
@stop