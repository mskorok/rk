@extends('site.layouts.default')

@section('title')
    {{{ $title }}} :: @parent
@stop

@section('content')
    <div class="container">
        <form action="" method="post" class="form-inline">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <div class="row">
                <div class="form-group col-md-4">
                    <label class="control-label" for="et">Tirgotājs: </label><br />
                    <select class="form-control input-sm" name="seller">
                        <option value="0"> -Izvēlieties tirgotāju- </option>
                        @foreach ($all_sellers as $s)
                            @if ($s->id==$seller_id)
                                <option selected="selected" value="{!! $s->id !!}">{!! $s->seller_name !!}</option>
                            @else
                                <option value="{!! $s->id !!}">{!! $s->seller_name !!}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                {!! $dateintervalssubmit !!}
            </div>
        </form>
    </div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="span2" style="background-color:#333;color:white;">Datums</th>
            <th class="span2" style="background-color:#333;color:white;">MPOS</th>
            <th class="span2" style="background-color:#333;color:white;">Darījumu skaits</th>
            <th class="span2" style="background-color:#333;color:white;">Darījumu summa</th>
            <th class="span2" style="background-color:#333;color:white;">Atgriezto maksājumu summa</th>
            <th class="span2" style="background-color:#333;color:white;">Izmaksu summa</th>
            <th class="span2" style="background-color:#333;color:white;">Dienas bilance</th>
        </tr>
        </thead>
        <tbody>
        @if (count($data)>0)
            @foreach ($data as $d)
                <tr>
                    <td>{!! $d->datums !!}</td>
                    <td>@if (isset($mpos[$d->mpos_id])) {!! $mpos[$d->mpos_id] !!}  @else Atcelta maksājuma atgriešana @endif</td>
                    <td>{!! $d->sk !!}</td>
                    <td>{!! sprintf("%.2f",$d->total/100) !!} EUR</td>
                    <td>{!! sprintf("%.2f",$d->returned/100) !!} EUR</td>
                    <td>{!! sprintf("%.2f",$d->outgoing/100) !!} EUR</td>
                    <td>{!! sprintf("%.2f",$d->daytotal/100) !!} EUR</td>
                </tr>
            @endforeach
            <tr style="font-weight:bold;">
                <td>KOPĀ</td>
                <td></td>
                <td>{!! $data->sum("sk") !!}</td>
                <td>{!! sprintf("%.2f",$data->sum("total")/100) !!} EUR</td>
                <td>{!! sprintf("%.2f",$data->sum("returned")/100) !!} EUR</td>
                <td>{!! sprintf("%.2f",$data->sum("outgoing")/100) !!} EUR</td>
                <td></td>
            </tr>
        @endif
        </tbody>
    </table>
@stop