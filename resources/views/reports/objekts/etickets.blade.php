@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="container">
		<form action="/reports/objetickets" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <div class="row">
                <div class="form-group col-md-4">
                    <label class="control-label" for="account">Izvēlieties skolu: </label><br />
                    <select class="form-control input-sm" name="account" onchange="this.form.submit()">
                        <option value="0"> -Izvēlieties skolu- </option>
                        @foreach ($all_acc as $et)
                            @if ($et->id==$account_id)
                                <option selected="selected" value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
                            @else
                                <option value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-3">
                    <label class="control-label" for="eticket">E-talons:</label>
                    <input class="form-control input-sm" name="eticket" id="eticketajax" class="typeahead" value="{!! $eticket_data !!}" autocomplete="off" />
                </div>

                <div class="form-group col-md-2">
                    <label class="control-label" for="lidz">Tikai ar PIN kodu:</label>
                    <select class="form-control input-sm" name="pin" style="width:100px;">
                        <option value="0"> --- </option>
                        <option value="yes" @if ($pin=='yes') selected="selected" @endif> Jā </option>
                        <option value="no" @if ($pin=='no') selected="selected" @endif> Nē </option>
                    </select>
                </div>

                <div class="form-group col-md-2">
                    <label class="control-label" for="blocked">Tikai bloķētie:</label>
                    <select class="form-control input-sm" name="blocked" style="width:100px;">
                        <option value="0"> --- </option>
                        <option value="yes" @if ($blocked=='yes') selected="selected" @endif> Jā </option>
                        <option value="no" @if ($blocked=='no') selected="selected" @endif> Nē </option>
                    </select>
                </div>

                <div class="form-group col-md-2">
                    <button type="submit" class="btn btn-primary btn-sm">Atlasīt</button>
                </div>

			</div>
		</form>
	</div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">E-talons</th>
				<th class="span2" style="background-color:#333;color:white;">PIN</th>
				<th class="span2" style="background-color:#333;color:white;">Statuss</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
			</tr>
		</thead>
		<tbody>
			@if (count($data)>0)
				@foreach ($data as $single)
						<tr>
							<td>
								{!! $single->nr !!} {!! $single->name !!}
							</td>
							<td>
								@if ($single->pin=='0000') Nav @else Ir @endif
							</td>
							<td>
								@if ($single->eticket_status==2) Bloķēts @else Aktīvs @endif
							</td>
							<td>{!! $single->created_at !!}</td>
							</td>
						</tr>
				@endforeach
			@endif
		</tbody>
	</table>

	@if (count($data)>0) {!! $data->appends(array('account' => $account_id, 'pin' => $pin, 'blocked' => $blocked))->render() !!} @endif

	@if (isset($totals))
	<div style="margin-top:20px;" class="pull-left">
		<p style="font-weight:bold;">Kopā e-taloni: <b>{!! $totals !!}</b></p>
	</div>
	@endif
@stop

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#eticketajax').typeahead([
			  {
			    name: 'object-etickets',
			    remote: {
			    	url: '/reports/eticket/fetch/%QUERY',
			    },
			    items: 10,
			    minLength : 1,
			  }
			]);
		});
	</script>
@stop