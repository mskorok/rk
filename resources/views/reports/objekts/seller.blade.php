@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div>
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <div class="row">
                <div class="form-group col-md-4">
                    <label class="control-label" for="account">Izvēlieties skolu: </label><br />
                    <select class="form-control input-sm" name="account" onchange="this.form.submit()">
                        <option value="0"> -Izvēlieties skolu- </option>
                        @foreach ($all_acc as $et)
                            @if ($et->id==$account_id)
                                <option selected="selected" value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
                            @else
                                <option value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-2">
                    <label class="control-label" for="seller">Izvēlieties: </label>
                    <select class="form-control input-sm" name="seller">
                        <option value="0"> -visi tirgotāji- </option>
                        @foreach ($all_sellers as $et)
                            @if ($et['id']==$seller)
                                <option selected="selected" value="{!! $et['id'] !!}">Tirgotājs: {!! $et['seller_name'] !!}</option>
                            @else
                                <option value="{!! $et['id'] !!}">Tirgotājs: {!! $et['seller_name'] !!}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                {!! $dateintervalssubmit !!}
			</div>
		</form>
	</div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">Tirgotājs</th>
				<th class="span2" style="background-color:#333;color:white;">Apgrozījums</th>
			</tr>
		</thead>
		<tbody>
			@if (count($data)>0)
				@foreach ($data as $single)
						<tr>
							<td>
								{!! $single['name'] !!}
							</td>
							<td>
								{!!sprintf("%.2f",$single['summa']/100)!!} EUR
							</td>
						</tr>
				@endforeach
			@endif
		</tbody>
	</table>

	@if ($totals!==0)
	<div style="margin-top:20px;" class="pull-left">
		<p style="font-weight:bold;">Apgrozījums par periodu:</p>
		<p>Ienākošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["in"]/100)!!} EUR
		</span></p>
	</div>
	@endif
@stop