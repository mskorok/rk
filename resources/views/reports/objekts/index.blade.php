@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="container">
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <div class="row">
                <div class="form-group col-md-4">
                    <label class="control-label" for="account">Izvēlieties skolu: </label><br />
                    <select class="form-control input-sm" name="account" onchange="this.form.submit()">
                        <option value="0"> -Izvēlieties skolu- </option>
                        @foreach ($all_acc as $et)
                            @if ($et->id==$account_id)
                                <option selected="selected" value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
                            @else
                                <option value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                {!! $dateintervalssubmit !!}
            </div>
        </form>
    </div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">No</th>
				<th class="span2" style="background-color:#333;color:white;">Uz</th>
				<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
			</tr>
		</thead>
		<tbody>
			@if (count($data)>0)
				@foreach ($data as $single)
						<tr>
							<td>@if ($single->from_account_id==0) 
									Bankas pārskaitījums
                                @elseif ($single->from_account_id==2)
                                    Brīvpusdienas
								@elseif ($single->from_account_id==999997)
									Atgriezts maksājums ({!! $all_mpos[$single->mpos_id] !!})
								@elseif ($single->from_account->account_type==2)
									E-talons {!! $single->from_eticket_nr !!}
								@elseif ($single->from_account->account_type==5)
									E-talonu grupa
								@else
									{!! $single->from_account_id !!}
								@endif
							</td>
							<td>
								@if ($single->to_account_id==0)
									Nav definēts konts
                                @elseif ($single->to_account_id==999997)
                                    Atgriezts maksājums
								@else
									@if ($single->to_account->account_type==1) 
										Pārskaitījums uz lietotāja kontu: {!! $single->to_account->owner->owner_name !!}
									@elseif ($single->to_account->account_type==2)
										E-talons {!! $single->to_eticket_nr !!}
									@elseif ($single->to_account->account_type==6) 
										MPOS ({!! $all_mpos[$single->to_account->owner_id] !!})
                                    @elseif ($single->to_account->account_type==7)
                                        Tirgotājs {!! $single->to_account->owner->owner_name !!}
									@else
										{!! $single->to_account_id !!}
									@endif
								@endif
							</td>
							<td>
								{!!sprintf("%.2f",$single->amount/100)!!} EUR
							</td>
							<td>{!! $single->created_at !!}</td>
							</td>
						</tr>
				@endforeach
			@endif
		</tbody>
	</table>

	@if (count($data)>0) 
	<div style="margin-top:20px;" class="pull-right">
		{!! $data->appends(array('account' => $account_id, 'no' => $no, 'lidz' => $lidz))->render() !!}
	</div>
	@endif

	@if ($totals!==0)
	<div style="margin-top:20px;" class="pull-left">
		<p style="font-weight:bold;">Apgrozījums par periodu:</p>
		<p>Ienākošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["in"]/100)!!} EUR
		</span></p>
        <p>Izejošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["out"]/100)!!} EUR
		</span></p>
	</div>
	@endif
@stop