@extends('site.layouts.default')

@section('title')
    {{{ $title }}} :: @parent
@stop

@section('content')
    <div class="container">
        <form action="" method="post" class="form-inline">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <div class="row">
                {!! $dateintervalssubmit !!}
            </div>
        </form>
    </div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="span2" style="background-color:#333;color:white;">Datums</th>
            <th class="span2" style="background-color:#333;color:white;">Apgrozībā esoša E-nauda dienas sākumā</th>
            <th class="span2" style="background-color:#333;color:white;">Emitēta E-nauda</th>
            <th class="span2" style="background-color:#333;color:white;">Iztērēta E-nauda</th>
            <th class="span2" style="background-color:#333;color:white;">Apgrozībā esoša E-nauda dienas beigās</th>
        </tr>
        </thead>
        <tbody>
        @if (count($data)>0)
            @foreach ($data as $d)
                <tr>
                    <td>{!! $d->datums !!}</td>
                    <td>{!! sprintf("%.2f",$d->start_balance/100) !!} EUR</td>
                    <td>{!! sprintf("%.2f",$d->incoming/100) !!} EUR</td>
                    <td>{!! sprintf("%.2f",$d->outgoing/100) !!} EUR</td>
                    <td>{!! sprintf("%.2f",$d->end_balance/100) !!} EUR</td>
                </tr>
            @endforeach
            <tr style="font-weight:bold;">
                <td>KOPĀ</td>
                <td>{!! sprintf("%.2f",$totals["first"]/100) !!} EUR</td>
                <td>{!! sprintf("%.2f",$totals["in"]/100) !!} EUR</td>
                <td>{!! sprintf("%.2f",$totals["out"]/100) !!} EUR</td>
                <td>{!! sprintf("%.2f",$totals["last"]/100) !!} EUR</td>
            </tr>
        @endif
        </tbody>
    </table>

    @if (count($data)>0)
        <a class="btn btn-large btn-info" target="_blank" href="/reports/moneyxls/{!! $no2 !!}/{!! $lidz2 !!}/">Lejuplādēt kā XLS failu</a>
    @endif
@stop