@extends('site.layouts.default')


@section('title')
	{{{ $title }}} :: @parent
@stop


@section('content')
	<div class="container">
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			<div class="form-group col-md-3">
				<label class="control-label" for="et">E-talons: </label>
				<select class="form-control input-sm" name="et" onchange="this.form.submit()">
					<option value="0"> -Izvēlieties e-talonu- </option>
					@foreach ($all_et as $et)
						@if ($et->id==$eticket_id)
							<option selected="selected" value="{!! $et->id !!}">E-talons nr.: {!! $et->nr !!} {!! $et->name !!} {!! $et->surname !!}</option>
						@else
							<option value="{!! $et->id !!}">E-talons nr.: {!! $et->nr !!} {!! $et->name !!} {!! $et->surname !!}</option>
						@endif
					@endforeach
				</select>
			</div>

			{!! $datesubmit !!}
		</form>
	</div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">Attēls</th>
				<th class="span3" style="background-color:#333;color:white;">Vārds, uzvārds</th>
				<th class="span3" style="background-color:#333;color:white;">Datums, laiks</th>
				<th class="span2" style="background-color:#333;color:white;">Virziens</th>
				<th class="span2" style="background-color:#333;color:white;">E-talona nr.</th>
			</tr>
		</thead>
		<tbody>
			@if (count($data)>0)
				@foreach ($data as $single)
						<tr>
							<td>{!! $thumbnail !!}</td>
							<td>{!! $single->eticket->name !!} {!! $single->eticket->surname !!}</td>
							<td>{!! $single->laiks !!}</td>
							<td>@if ($single->virziens==1) Ienāca @else Izgāja @endif</td>
							<td>{!! $single->eticket->nr !!}</td>
						</tr>
				@endforeach
			@endif
		</tbody>
	</table>

	@if (count($data)>0)
	<div style="margin-top:20px;" class="pull-right">
		{!! $data->appends(array('et' => $et, 'datums' => $datums))->render() !!}
	</div>
	@endif

	@if (isset($totals))
	<div style="margin-top:20px;" class="pull-left">
		<p style="font-weight:bold;">Kopējais skaits: <b>
			{!! $totals !!}
		</b></p>
	</div>
	@endif

@stop