@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="container">
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			<div class="form-group col-md-4">
				<label class="control-label" for="account">Tirgotājs:</label><br />
				<select class="form-control input-sm" name="account">
					<option value="0"> -Izvēlieties tirgotāju- </option>
					@foreach ($all_acc as $et)
						@if ($et->id==$account_id)
							<option selected="selected" value="{!! $et->id !!}">Tirgotājs: {!! $et->Owner->owner_name !!}</option>
						@else
							<option value="{!! $et->id !!}">Tirgotājs: {!! $et->Owner->owner_name !!}</option>
						@endif
					@endforeach
				</select>
			</div>

			@if ($account_id>0)
			<div class="form-group col-md-4">
				<label class="control-label" for="object">Objekts:</label><br />
				<select class="form-control input-sm" name="object">
					<option value="0"> -Visi objekti- </option>
					@foreach ($seller_objects as $et)
						@if ($et->id==$object_id)
							<option selected="selected" value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
						@else
							<option value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
						@endif
					@endforeach
				</select>
			</div>

			<div class="form-group col-md-4">
				<label class="control-label" for="mpos">MPOS:</label><br />
				<select class="form-control input-sm" name="mpos">
					<option value="0"> -Izvēlieties MPOS- </option>
					@foreach ($mpos_accounts as $et)
						@if ($et->id==$mpos_id)
							<option selected="selected" value="{!! $et->id !!}">MPOS: {!! $et->mpos_name !!}</option>
						@else
							<option value="{!! $et->id !!}">MPOS: {!! $et->mpos_name !!}</option>
						@endif
					@endforeach
				</select>
			</div>

			<div class="form-group col-md-2">
				<label class="control-label" for="veids">Veids:</label>
				<select class="form-control input-sm" name="veids">
					<option value="0"> Visas transakcijas </option>
					<option value="1" @if($veids == 1) selected="selected" @endif> E-nauda </option>
					<option value="2" @if($veids == 2) selected="selected" @endif> Brīvpusdienas </option>
				</select>
			</div>

			<div class="form-group col-md-2">
				<label class="control-label" for="grupet">Grupēt pa objektiem:</label>
				<select class="form-control input-sm" name="grupet">
					<option value="0"> Nē </option>
					<option value="1" @if($grupet == 1) selected="selected" @endif> Jā </option>
				</select>
			</div>
			@endif

			{!! $dateintervalssubmit !!}
		</form>
	</div>

	<?php
	$subtotal["in"] = 0;
	$subtotal["out"] = 0;
	$subtotal["free"] = 0;
	?>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">No</th>
				<th class="span2" style="background-color:#333;color:white;">Uz</th>
				<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
			</tr>
		</thead>
		<tbody>
			@if (count($data)>0)
				@foreach ($data as $single)
					@if ($grupet == 1)
						@if ( ! isset($last_object_id))
							<?php
							$last_object_id =  $single->object_id;
							$subtotal["in"] = 0;
							$subtotal["out"] = 0;
							$subtotal["free"] = 0;
							?>
						@elseif($last_object_id != $single->object_id)
							</table>

							<div style="margin-top:10px;" class="pull-left">
								<p>Ienākošais: <span style="font-weight:bold;">
									{!!sprintf("%.2f",$subtotal["in"]/100)!!} EUR
								</span></p>
								<p>Izejošais: <span style="font-weight:bold;">
									{!!sprintf("%.2f",$subtotal["out"]/100)!!} EUR
								</span></p>
								@if ($subtotal["free"] > 0)
									<p>Izsniegtas brīvpusdienas: <span style="font-weight:bold;">
										{!! $subtotal["free"] !!}
									</span></p>
								@endif
							</div>

							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="span3" style="background-color:#333;color:white;">No</th>
									<th class="span2" style="background-color:#333;color:white;">Uz</th>
									<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
									<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
								</tr>
								</thead>
								<tbody>

							<?php
							$last_object_id =  $single->object_id;
							$subtotal["in"] = 0;
							$subtotal["out"] = 0;
							$subtotal["free"] = 0;
							?>
						@endif

						<?php
						if ($single->object_id > 0 && $single->from_account_id > 2)
							$subtotal["in"]+= $single->amount;
						if ($single->object_id == 0 && $single->to_account_id != $seller_free_meals_account->id)
							$subtotal["out"]+= $single->amount;
						if ($single->from_account_id == 2)
							$subtotal["free"]++;
						?>
					@endif

						<tr>
							<td>@if ($single->from_account_id==0) 
									Bankas pārskaitījums
                                @elseif ($single->from_account_id==999997)
                                    Naudas atgriešanas atcelšana
								@elseif ($single->from_account_id==2)
                                    E-talons @if ($admin_user) {!! $single->from_eticket_nr !!} @else {!! "*******".substr($single->from_eticket_nr,-3,3) !!} @endif brīvpusdienas
								@elseif ($single->from_account->account_type==6) 
									Pārskaitījums no MPOS ({!! $all_mpos[$single->from_account->owner_id] !!})
								@elseif ($single->from_account->account_type==2)
									E-talons @if ($admin_user) {!! $single->from_eticket_nr !!} @else {!! "*******".substr($single->from_eticket_nr,-3,3) !!} @endif
								@elseif ($single->from_account->account_type==5)
									E-talonu grupa
								@elseif ($single->from_account->account_type==3)
									Tirgotāja konts: {!! $single->from_account->owner->owner_name !!}
								@else
									{!! $single->from_account_id !!}
								@endif

                                @if ($admin_user == 1 && $single->comments == 'Manual Serial')
                                    &nbsp;(manual)
                                @endif
							</td>
							<td>
								@if ($single->to_account_id==0)
									Nav definēts konts
								@elseif ($single->to_account!==NULL)
									@if ($single->to_account->account_type==3) 
										Tirgotāja konts: {!! $single->to_account->owner->owner_name !!}
									@elseif ($single->to_account->account_type==6) 
										MPOS ({!! $all_mpos[$single->to_account->owner_id] !!})
									@endif
								@else
									@if ($single->to_account_id == 999999)
										Naudas izmaksa
									@elseif ($single->to_account_id == 999998)
										Naudas izmaksas komisija
                                    @elseif ($single->to_account_id == 999997)
                                        Naudas atgriešana
									@else
										{!! $single->to_account_id !!}
									@endif
								@endif
							</td>
							<td>
                                @if ($single->from_account_id==2 || $single->to_account_id == $seller_free_meals_account->id)
                                    Brīvpusdienas
                                @else
                                    {!!sprintf("%.2f",$single->amount/100)!!} EUR
                                @endif
							</td>
							<td>{!! $single->created_at !!}</td>
							</td>
						</tr>
				@endforeach
			@endif
		</tbody>
	</table>

	@if ($grupet == 1)
		<div style="margin-top:10px;width:100%;" class="pull-left">
			<p>Ienākošais: <span style="font-weight:bold;">
								{!!sprintf("%.2f",$subtotal["in"]/100)!!} EUR
							</span></p>
			<p>Izejošais: <span style="font-weight:bold;">
								{!!sprintf("%.2f",$subtotal["out"]/100)!!} EUR
							</span></p>
			@if ($subtotal["free"] > 0)
				<p>Izsniegtas brīvpusdienas: <span style="font-weight:bold;">
										{!! $subtotal["free"] !!}
									</span></p>
			@endif
		</div>
	@endif

	@if (count($data)>0 && $grupet == 0)
        <div style="margin-top:20px;width:100%;" class="pull-right">
            {!! $data->appends(array('account' => $account_id, 'object' => $object_id, 'mpos' => $mpos_id, 'no' => $no, 'lidz' => $lidz, 'veids' => $veids, 'grupet' => $grupet))->render() !!}
        </div>
	@endif

	@if ($totals!==0)
	<div style="margin-top:20px;" class="pull-left">
		<p style="font-weight:bold;">Kopā par periodu:</p>
		<p>Ienākošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["in"]/100)!!} EUR
		</span></p>
		<p>Izejošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["out"]/100)!!} EUR
		</span></p>
		@if ($veids != 1)
            <p> <span style="font-weight:bold;">Izsniegtas brīvpusdienas, kopā: {!! $totals["free"] !!}, tai skaitā:</span></p>
            @if ($freemeals[0] > 0)
                <p>Izsniegtas brīvpusdienas, bez atšifrējuma: <span>{!! $freemeals[0] !!}</span></p>
            @endif
            @if ($freemeals[1] > 0)
                <p>Brīvpusdienas – pabalsts: <span>{!! $freemeals[1] !!}</span></p>
            @endif
            @if ($freemeals[2] > 0)
                <p>Valsts brīvpusdienas no neizmantotajiem līdzekļiem: <span>{!! $freemeals[2] !!}</span></p>
            @endif
            @if ($freemeals[3] > 0)
                <p>Pašvaldības brīvpusdienas: <span>{!! $freemeals[3] !!}</span></p>
            @endif
			@if ($freemeals[4] > 0)
				<p>Kopgalds: <span>{!! $freemeals[4] !!}</span>, no tām valsts: <span>{!! $freemeals[41] !!}</span>, pašvaldības: <span>{!! $freemeals[42] !!}</span></p>
			@endif
		@endif
	</div>
	@endif

	@if (count($data)>0 && $account_id > 0)
        <?php
                $link_no = ($no != '') ? $no : '2016-01-01';
                $link_lidz = ($lidz != '') ? $lidz : date("Y-m-d");
                $link = base64_encode($link_no.'|'.$link_lidz.'|'.$account_id.'|'.$object_id.'|'.$mpos_id.'|'.$grupet.'|'.$veids);
        ?>
		<div style="margin-top:20px;width:100%;" class="pull-left">
		    <a class="btn btn-large btn-info" target="_blank" href="/reports/sellerxls/{!! $link !!}/">Lejuplādēt kā XLS failu</a>
        </div>
	@endif
@stop