@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="container">
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			<div class="row">
				<div class="form-group col-md-2">
                    <label class="control-label" for="seller_id">Tirgotājs: </label>
                    {!! Form::select('seller_id', $all_sellers, Input::old('seller_id',$seller_id), ["class" => "form-control input-sm"]) !!}
				</div>

				{!! $dateintervalssubmit !!}
			</div>
		</form>
	</div>

	<table class="table table-bordered">
		<thead>
		<tr>
			<th class="span2" style="background-color:#333;color:white;">E-talons</th>
			<th class="span2" style="background-color:#333;color:white;">MPOS iekārta</th>
			<th class="span2" style="background-color:#333;color:white;">Skola</th>
			<th class="span2" style="background-color:#333;color:white;">Summa</th>
			<th class="span2" style="background-color:#333;color:white;">Apmaksāts</th>
			<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
		</tr>
		</thead>
		<tbody>
		@if (count($data)>0)
			@foreach ($data as $single)
				<tr>
					<td>{!! $single->from_eticket_nr !!}</td>
					<td>{!! $single->mpos->mpos_name !!}</td>
					<td>{!! $single->object->object_name !!}</td>
					<td>{!!sprintf("%.2f",$single->amount/100)!!} EUR</td>
					<td>@if (strpos($single->created_at,date("Y-m"))!==false) Nē @else Jā @endif</td>
					<td>{!! $single->created_at !!}</td>
				</tr>
			@endforeach
		@endif
		</tbody>
	</table>

	@if (count($data)>0)
		<div style="margin-top:20px;" class="pull-right">
			{!! $data->appends(array('seller' => $seller_id, 'no' => $no, 'lidz' => $lidz))->render() !!}
		</div>
	@endif

	@if ($totals!==0)
		<div style="margin-top:20px;" class="pull-left">
			<p style="font-weight:bold;">Brīvpusdienas par šo periodu:</p>
			<p>Kopsumma: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["sum"]/100)!!} EUR
		</span></p>
			<p>Skaits: <span style="font-weight:bold;">
			{!! $totals["count"] !!}
		</span></p>
		</div>
	@endif
@stop