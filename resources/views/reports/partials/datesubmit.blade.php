<div class="form-group col-md-2">
	<label class="control-label" for="no">Datums no:</label>
	<div class="input-group input-group-sm date" id="datetimepicker1">
	    <input class="form-control" type="text" name="no" value="@if(isset($no)){!!$no!!}@endif" />
	    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	</div>
</div>

<div class="form-group col-md-2">
	<label class="control-label" for="lidz">Datums līdz:</label>
	<div class="input-group input-group-sm date" id="datetimepicker2">
	    <input class="form-control" type="text" name="lidz" value="@if(isset($lidz)){!!$lidz!!}@endif" />
	    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	</div>
</div>

<div class="form-group col-md-1" style="height:60px;position:relative;">
	<button type="submit" class="btn btn-primary btn-sm" style="position: absolute;right:0;bottom:0;">Atlasīt</button>
</div>