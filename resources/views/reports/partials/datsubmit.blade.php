<div class="form-group col-md-2">
	<label class="control-label" for="datums">Datums no:</label>
	<div class="input-group input-group-sm date" id="datetimepicker1">
	    <input class="form-control" data-format="GGGG-MM-DD" type="text" name="datums" value="@if (isset($datums)) {!!$datums!!} @endif" />
	    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	</div>
</div>

<div class="form-group col-md-1" style="height:58px;position:relative;">
	<button type="submit" class="btn btn-primary btn-sm" style="position: absolute;right:0;bottom:0;">Atlasīt</button>
</div>