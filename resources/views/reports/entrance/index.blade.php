@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="container">
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			<div class="row">
                <div class="form-group col-md-3">
                    <label class="control-label" for="account">Izvēlieties skolu: </label>
                    <select class="form-control input-sm" name="account" onchange="this.form.submit()">
                        <option value="0"> -Izvēlieties skolu- </option>
                        @foreach ($all_acc as $et)
                            @if ($et->id==$account_id)
                                <option selected="selected" value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
                            @else
                                <option value="{!! $et->id !!}">Objekts: {!! $et->object_name !!}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                {!! $datesubmit !!}
            </div>
		</form>
	</div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">Datums</th>
				<th class="span2" style="background-color:#333;color:white;">Personas kods (E-talona nr.)</th>
				<th class="span2" style="background-color:#333;color:white;">Vārti</th>
				<th class="span2" style="background-color:#333;color:white;">Virziens</th>
				<th class="span2" style="background-color:#333;color:white;">Laiks</th>
			</tr>
		</thead>
		<tbody>
			@if (count($data)>0)
				@foreach ($data as $single)
						<tr>
							<td>{!! $single->datums !!}</td>
							<td>{!! $single->pk !!} {!! $single->eticket_nr !!}</td>
							<td>{!! $single->turnicet_id !!}</td>
							<td>{!! $single->virziens !!}</td>
							<td>{!! $single->laiks !!}</td>
						</tr>
				@endforeach
			@endif
		</tbody>
	</table>

	@if (count($data)>0)
	<div style="margin-top:20px;" class="pull-right">
		{!! $data->appends(array('account' => $account_id, 'datums' => $datums))->render() !!}
	</div>
	@endif

	@if (isset($totals))
	<div style="margin-top:20px;" class="pull-left">
		<p style="font-weight:bold;">Kopējais skaits: <b>
			{!! $totals !!}
		</b></p>
	</div>
	@endif
@stop