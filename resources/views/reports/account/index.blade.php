@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="container">
		<form action="" method="post" class="form-inline">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			<div class="row" style="row-height:60px;">
				<div class="form-group col-md-4">
					<label class="control-label" for="account">Lietotājs: </label><br />
					@if ($admin_user==0)
					<select class="form-control" name="account" onchange="this.form.submit()">
						@foreach ($all_acc as $acc)
							@if ($acc->id==$account_id)
								<option selected="selected" value="{!! $acc->id !!}">Lietotāja konts: {!! $acc->owner->owner_name !!}</option>
							@else
								<option value="{!! $acc->id !!}">Lietotāja konts: {!! $acc->owner->owner_name !!}</option>
							@endif
						@endforeach
					</select>
					@else
						<input name="account" id="accountajax" class="form-control input-sm twitter-typeahead" value="{!! $username !!}" style="width:100%;" autocomplete="off" />
					@endif
				</div>

				{!! $dateintervalssubmit !!}
			</div>
		</form>
	</div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">No</th>
				<th class="span2" style="background-color:#333;color:white;">Uz</th>
				<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
			</tr>
		</thead>
		<tbody>
			@if (count($data)>0)
				@foreach ($data as $single)
						<tr>
							<td>@if ($single->from_account_id==0) 
									Bankas pārskaitījums 
								@elseif ($single->from_account_id==$account_id) 
									Lietotāja konts ({!! $username !!})
								@elseif ($single->from_account->account_type==2)
									E-talons {!! $single->from_eticket_nr !!} @if (isset($single->fromEt->name)) ({!! $single->fromEt->name !!}) @endif
								@elseif ($single->from_account->account_type==1)
									Pārskaitījums no lietotāja:
								@else
									{!! $single->from_account_id !!}
								@endif
							</td>
							<td>@if ($single->to_account_id==$account_id) 
									Lietotāja konts ({!! $username !!})
								@elseif ($single->to_account->account_type==2)
									E-talons {!! $single->to_eticket_nr !!} @if (isset($single->toEt->name)) ({!! $single->toEt->name !!}) @endif
								@elseif ($single->to_account->account_type==1)
									Pārskaitījums lietotājam: {!! $single->to_account->owner->owner_name !!}
								@else
									{!! $single->to_account_id !!}
								@endif
							</td>
							<td>
								{!!sprintf("%.2f",$single->amount/100)!!} EUR
							</td>
							<td>{!! $single->created_at !!}</td>
						</tr>
				@endforeach
			@endif
		</tbody>
	</table>

	@if (count($data)>0) 
	<div style="margin-top:20px;" class="pull-right">
		{!! $data->appends(array('account' => $username,'no' => $no, 'lidz' => $lidz))->render() !!}
	</div>
	@endif

	<div style="margin-top:20px;" class="pull-left">
		<p style="font-weight:bold;">Apgrozījums par periodu:</p>
		<p>Ienākošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["in"]/100)!!} EUR
		</span></p>
		<p>Izejošais: <span style="font-weight:bold;">
			{!!sprintf("%.2f",$totals["out"]/100)!!} EUR
		</span></p>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#accountajax').typeahead([
			  {
			    name: 'accounts',
			    remote: {
			    	url: '/reports/account/fetch/%QUERY',
			    },
			    items: 10,
			    minLength : 3,
			  }
			]);
		});
	</script>
@stop