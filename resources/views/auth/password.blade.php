{!! trans('passwords.reset_body_before_link') !!}
<a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
{!! trans('passwords.reset_body_after_link') !!}
<br>
<i>
    {{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}
</i>
<br>
{!! trans('passwords.reset_body_after_non_active_link') !!}
<br>
{!! trans('passwords.disclaimer') !!}
