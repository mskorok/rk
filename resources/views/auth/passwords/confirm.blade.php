{!! trans('passwords.register_body_before_link') !!}
<br>
<a href="{{ $link = url('portal/registration/confirm/'.$user->confirmation_code).'?email='.urlencode($user->email) }}"> {{ $link }} </a>
<br>
{!! trans('passwords.register_body_after_link') !!}
<br>
<i>
    {{ $link = url('portal/registration/confirm/'.$user->confirmation_code).'?email='.urlencode($user->email) }}
</i>
<br>
{!! trans('passwords.register_body_after_non_active_link') !!}
<br>
{!! trans('passwords.disclaimer') !!}