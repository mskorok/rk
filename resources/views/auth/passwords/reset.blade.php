@extends('portal.layouts.main')
@section('title')
    @lang('passwords.recovery_title')
@stop
@section('content')
<div class="container" id="register" style="margin-top: 160px; margin-bottom: 30px;">
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('passwords.reset_password')</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('passwords.email_address')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control email-register1" name="email" value="{{ $email or old('email') }}">
                                <div class="error-message error-message1">
                                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                    @lang('portal/partials/register/section_register.invalid_email')
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">@lang('passwords.new_password')</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control password-register1" name="password">
                                <div class="error-message error-message1">
                                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                    @lang('passwords.invalid_password')
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <div class="text-center input-group-note form-text">@lang('portal/partials/register/section_register.password_terms')</div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">@lang('passwords.confirm_password')</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control password-register2" name="password_confirmation">
                                <div class="error-message error-message4">
                                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                    @lang('passwords.invalid_password')
                                </div>
                                <div class="error-message error-message5">
                                    <img src="{{ asset('assets/portal/img/danger.png') }}" alt="">
                                    @lang('passwords.invalid_password_confirmation')
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i> Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        var reset_url = '{{ route('ajax_reset') }}';
        $(document).find('.error-message').hide();
        $('.password-register1').blur(function() {
            var input = $(this);
            if (input.val() != '') {
                var parent = input.parent();
                if (custom_functions.validatePassword(input.val())) {
                    $(parent).find('.error-message1').hide();
                    input.removeClass('error').addClass('success');
                } else {
                    $(parent).find('.error-message1').show();
                    input.addClass('error').removeClass('success');
                }
            } else {
                $(parent).find('.error-message').hide();
                input.removeClass('success error');
            }
        }).focus(function () {
            var input = $(this);
            var parent = input.parent();
            $(parent).find('.error-message').hide();
            input.removeClass('success error');
        });
        $('.password-register2').blur(function() {
            var input = $(this);
            if (input.val() != '') {
                var parent = input.parent();
                var input1 = $('.password-register1');
                if (custom_functions.validatePassword(input1.val())) {
                    $(parent).find('.error-message4').hide();
                    if (input1.val() !== input.val()) {
                        $(parent).find('.error-message5').show();
                        input.addClass('error').removeClass('success');
                    } else {
                        input.removeClass('error').addClass('success');
                    }
                } else {
                    $(parent).find('.error-message4').show();
                    input.addClass('error').removeClass('success');
                }
            } else {
                $(parent).find('.error-message').hide();
                input.removeClass('success error');
            }
        }).focus(function () {
            var input = $(this);
            var parent = input.parent();
            $(parent).find('.error-message').hide();
            input.removeClass('success error');
        });
        $('.email-register1').blur(function() {
            var mail = $(this);
            if (mail.val() != '') {
                var parent = mail.parent();
                if (mail.val().search(pattern) == 0) {
                    $(parent).find('.error-message1').hide();
                    mail.removeClass('error').addClass('success');
                } else {
                    $(parent).find('.error-message1').show();
                    mail.addClass('error').removeClass('success');
                }
            } else {
                $(parent).find('.error-message').hide();
                mail.removeClass('success error');
            }
        }).focus(function () {
            var input = $(this);
            var parent = input.parent();
            $(parent).find('.error-message').hide();
            input.removeClass('success error');
        });
    </script>
@stop
