@extends('site.layouts.default')

@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

@section('content')
<div style="width:450px;margin:0 auto;">
<div class="page-header">
	<h1>Ienākt sistēmā</h1>
</div>
<form method="POST" action="{!! URL::to('auth/login') !!}" accept-charset="UTF-8" autocomplete=”off”>
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">

    <div class="form-group">
        <label for="email">{!! Lang::get('confide/confide.e_mail') !!}</label>
        <input class="form-control" tabindex="1" placeholder="{!! Lang::get('confide/confide.e_mail') !!}" type="text" autocomplete=”off” name="email" id="email" value="{!! Input::old('email') !!}">
    </div>

    <div class="form-group">
        <label for="password">
            {!! Lang::get('confide/confide.password') !!}
        </label>
        <input class="form-control" tabindex="2" placeholder="{!! Lang::get('confide/confide.password') !!}" autocomplete=”off” type="password" name="password" id="password">
    </div>

    @if ( Session::get('notice') )
    <div class="alert alert-warning alert-dismissable">{!! Session::get('notice') !!}</div>
    @endif

    <button tabindex="3" name="submit" type="submit" class="btn btn-primary">{!! Lang::get('confide/confide.login.submit') !!}</button>

    <a class="btn btn-primary" style="margin-left:35px;" href="https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit">{{{ Lang::get('user/user.register') }}}</a>

    <div class="btn-group" style="margin-left:35px;">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Lietošanas pamācība <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a target="_blank" href="/pamaciba_lv.pdf">Lietošanas pamācība (latviešu valodā)</a></li>
            <li><a target="_blank" href="/pamaciba_ru.pdf">Lietošanas pamācība (krievu valodā)</a></li>
        </ul>
    </div>

</form>
   
</div>

@stop