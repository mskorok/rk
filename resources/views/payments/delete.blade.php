@extends('site.layouts.default')

@section('content')

    <form class="form-horizontal" method="post" action="" autocomplete="off">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

        <h4>Atcelt maksājuma uzdevumu</h4>

        <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Maksājuma nr.</label>
                    <pre>{!! $data->id !!}</pre>
                </div>

                <div class="form-group">
                    <label class="control-label">Izveidots</label>
                    <pre>{!! $data->created_date() !!}</pre>
                </div>

                <div class="form-group">
                    <label class="control-label">E-talons</label>
                    <pre>{!! $eticket_nr !!}</pre>
                </div>

                <div class="form-group">
                    <label class="control-label">Maksājuma identifikācijas kods</label>
                    <pre>{!! $data->payment_code !!}</pre>
                </div>

                <div class="form-group">
                    <label class="control-label" for="summa">Summa</label>
                    <pre>{!! $data->humanAmount() !!}</pre>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <a class="btn-cancel" href="/reports/payments">Atpakaļ</a>
                        <button type="submit" class="btn btn-danger">{!! Lang::get('button.delete') !!}</button>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </form>
@stop