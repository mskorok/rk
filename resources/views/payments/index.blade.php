@extends('site.layouts.default')

@section('content')
	<div class="page-header">
		<h3>
			Maksājuma uzdevumi
		</h3>
	</div>

	<div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="span2" style="background-color:#333;color:white;">{!! Lang::get('admin/etickets/table.created_at') !!}</th>
					<th class="span2" style="background-color:#333;color:white;">E-talons</th>
					<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
					<th class="span3" style="background-color:#333;color:white;">Statuss</th>
					<th class="span2" style="background-color:#333;color:white;">Dokuments</th>
					<th class="span2" style="background-color:#333;color:white;">Darbības</th>
				</tr>
			</thead>
			<tbody>
				@if (count($data)>0)
					@foreach ($data as $single)
							<tr>		
								<td>{!! $single->created_at !!}</td>
								<td>{!! $single->eticket->nr !!}</td>
								<td>{!! $single->humanUserAmount() !!}</td>
								<td>@if ($single->confirmed==0) Gaida apstiprināšanu @else Apstiprināts @endif</td>
								<td><a class="btn btn-sm btn-primary" target="_blank" href="{!! URL::to('reports/payments/pdf/'.$single->id) !!}">PDF</a></td>
								<td>@if ($single->confirmed==0) 
									<a class="btn btn-sm btn-danger" href="{!! URL::to('reports/payments/delete/'.$single->id) !!}">Atcelt</a>
									@endif</td>
							</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
@stop