<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="skolēna e karte portāls">
    <meta name="author" content="rigas karte">
    <link rel="shortcut icon" href="/favicon.ico">
    <title>SKOLĒNA E KARTE</title>
    <link href="/under_construction/css/bootstrap.min.css" rel="stylesheet">
    <link href="/under_construction/css/jquery-ui.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="/under_construction/css/font-awesome.min.css" rel="stylesheet">
    <!-- Libraries styles -->
    <!-- <link href="css/owl.carousel.min.css" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="/under_construction/css/icons.css" rel="stylesheet">
    <link href="/under_construction/css/style.css" rel="stylesheet">
    <link href="/under_construction/css/adaptive.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="margin-top: auto;">

    <div class="navbar navbar-top" role="navigation" style="">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand logo" href="{{ route('blocked_ip') }}">
            <div class="image"><img src="/under_construction/img/skolena_e_karte.png"></div><div class="heading"><img src="/under_construction/css/images/svg_icons/skolena_e_karte.svg" alt="Skolēna E karte logo"></div>
          </a>
        </div>

        <div class="navbar-right">
          <div class="lang">
            <a href="/lv{{ \Request::getRequestUri() }}" @if(App::getLocale() === 'lv')class="active"@endif >lat</a>
            <a href="/ru{{ \Request::getRequestUri() }}" @if(App::getLocale() === 'ru')class="active"@endif >rus</a>
          </div>
        </div>
      </div>
    </div>

    <section id="register_thanks" style="padding: 0;">
      <div class="container">
        <h1 class="title" style="font-weight: 700; letter-spacing: 0.05em; margin-bottom: 20px;">SKOLĒNA E KARTES PORTĀLA JAUNĀS VERSIJAS<br/>ATKLĀŠANA – 15. SEPTEMBRĪ!</h1>
        <div class="content">
          <div class="thanks-img text-center">
            <img src="/under_construction/img/karte_laptop.png" alt="e karte un laptop dators" style="width: 340px; height: auto;">
          </div>
        </div>
      </div>
    </section>

    <section class="bg-color" id="section1" style="padding: 10px 0;">
      <div class="container">
        
        <div class="row">
          <h3 class="title" style="letter-spacing: 0.05em; font-weight: 600; margin-bottom: 30px; font-size: 24px;">KĀ PORTĀLS VAR PALĪDZĒT VECĀKIEM</h3>
          <div class="content">
            <div class="row">
              <div class="items-inner col-md-3 pull-left p-r-0">
                <div class="col-xs-6 col-md-12 item m-b-20  p-r-0">
                  <h4><img class="icon icon2" src="/under_construction/css/images/svg_icons/help1.svg" alt="">Nav jāuztraucas</h4>
                  <p>Jūs variet pārliecināties par to, kā bērns tērē naudu, jo ar Skolēna e kartes naudu var norēķināties tikai skolā.</p>
                </div>
                <div class="col-xs-6 col-md-12 item  p-r-0">
                  <h4><img class="icon icon2" src="/under_construction/css/images/svg_icons/help3.svg" alt="">Var iemācīt</h4>
                  <p>Bērns mācās plānot budžetu. Bērnam ir iespēja viegli apgūt bezskaidras naudas norēķinus.</p>
                </div>
              </div>
              <div class="items-inner col-md-3 pull-right p-l-0">
                <div class="col-xs-6 col-md-12 item m-b-0 p-l-0">
                  <h4><img class="icon icon3" src="/under_construction/css/images/svg_icons/help2.svg" alt="">Var aizsargāt</h4>
                  <p>Ja Jūsu bērnam nav skaidras naudas, to nevar atņemt, pazaudēt, kā arī iztērēt ārpus skolas. Ja Skolēna e karte tiek pazaudēta, to viegli var nobloķēt.</p>
                </div>
                <div class="col-xs-6 col-md-12 item p-l-0">
                  <h4><img class="icon icon4" src="/under_construction/css/images/svg_icons/help4.svg" alt="">Rūpes</h4>
                  <p>Jūs vienmēr varēsiet pārliecināties, vai Jūsu bērns ir paēdis pusdienas. Skolēna e karti var papildināt uz nedēļu vai mēnesi uz priekšu.</p>
                </div>
              </div>
              <div class="items-inner col-md-6 p-l-0 p-r-0">
                <img src="/under_construction/img/thanks.png" class="img-thanks" alt="fons ka portals var palidzet" style="width: 60%; display: block; margin: auto;">
              </div>
            </div>
          </div>
        </div>

      </div>
    </section>

    <section id="footer" style="bottom: 0; min-height: 120px;">
      <div class="container-fluid">
        <ul>
          <li><a href="#"><img src="/under_construction/css/images/svg_icons/rigas_dome_logo.svg" alt=""></a></li>
          <li><a href="#"><img src="/under_construction/css/images/svg_icons/rigas_satisksme_logo.svg" alt=""></a></li>
          <li><a href="#"><img src="/under_construction/css/images/svg_icons/rigas_karte_logo.svg" alt=""></a></li>
        </ul>
      </div>
    </section>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/under_construction/js/jquery.min.js"></script>
    <script src="/under_construction/js/jquery-ui.min.js"></script>
    <script src="/under_construction/js/bootstrap.min.js"></script>
    <!-- <script src="js/owl.carousel.min.js"></script> -->
    <!-- <script src="js/jquery.validate.min.js"></script> -->
    <!-- <script src="js/main.js"></script> -->

  </body>
</html>