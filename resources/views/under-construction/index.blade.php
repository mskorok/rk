<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            @section('title')
            SKOLAS PORTALS @if (App::environment() == 'dev') DEV @elseif (App::environment() == 'local') LOCAL @endif
            @show
        </title>
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="description" content="" />
        <meta name="csrf-token" content="<?= csrf_token() ?>">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            .outer {
                display: table;
                position: absolute;
                height: 99%;
                width: 99%;
            }

            .middle {
                display: table-cell;
                vertical-align: middle;
            }

            .inner {
                margin-left: auto;
                margin-right: auto; 
                width: 50%;
                max-width: 800px;
                border: solid 1px silver;
                padding: 20px;
            }
            
            .inner img {
               width: 100%;
            }            

            h1 {
                margin-top: 0;
                padding: 15px;
                text-align: center;
                font-family: verdana;
                color: #F3BA19;
                text-shadow: 1px 1px 3px #585857;
            }
            
            h1 span {
                text-shadow: 1px 1px 3px #585857, 0 0 1em #F3BA19;
            }
        </style>
        <link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">
    </head>

    <body bgcolor="#FFFFFF">
        <div class="outer">
            <div class="middle">
                <div class="inner">
                    <h1>{{{ $title_1 }}}</h1>
                    <h1 style="font-size: 300%;"><span>{{{ $title_2 }}}</span></h1>
                    <img src="{{{ asset('assets/img/under-construction.gif') }}}" align="bottom" border="0">
                </div>
            </div>
        </div>               
    </body>
</html>
