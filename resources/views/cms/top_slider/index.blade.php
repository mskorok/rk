@extends('cms.layouts.default')

@section('title')
	{{ $title }} :: @parent
@stop

@section('content')
	<div class="page-header">
		<h3>
			{{ $title }}
		</h3>
	</div>

	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span1">@lang('admin/top_slider/table.name')</th>
                <th class="span1">@lang('admin/top_slider/table.sort')</th>
                <th class="span1">@lang('admin/top_slider/table.active')</th>
                <th class="span2">@lang('admin/top_slider/table.from')</th>
                <th class="span2">@lang('admin/top_slider/table.to')</th>
                <th class="span1">@lang('table.actions')</th>
			</tr>
		</thead>
	</table>
	<div>
        <a class="btn btn-info" href="{{  route('cms_slider_create_get') }}">@lang('button.create')</a>
    </div>
    <br>
    <br>
@stop

@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
		        "sAjaxSource": "{!! URL::to('cms/top_slider/data') !!}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop