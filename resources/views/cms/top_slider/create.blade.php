@extends('cms.layouts.default')

@section('content')
	<div class="page-header">
		<h3>
			{{ $title }}
		</h3>
	</div>

	<form class="form-horizontal" method="post" action="{{ $action }}" autocomplete="off" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="tab-content">
			<div class="tab-pane active" id="tab-general">

				<div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
					<label class="control-label" for="name">@lang('admin/top_slider/messages.name')</label>
					<div class="controls">
						<input class="form-control" type="text" name="name"  id="name"  />
						{!! $errors->first('name', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{ $errors->has('sort') ? 'error' : '' }}">
					<label class="control-label" for="sort">@lang('admin/top_slider/messages.sort')</label>
					<div class="controls">
						<input class="form-control" type="text" name="sort" id="sort" value="1" />
						{!! $errors->first('sort', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="checkbox {{ $errors->has('active') ? 'error' : '' }}">
					<label>
						<input type="checkbox" name="active" id="active" >
						@lang('admin/top_slider/messages.active')
					</label>
					{!! $errors->first('active', '<span class="help-inline">:message</span>') !!}
				</div>

				<div class="control-group {{ $errors->has('path') ? 'error' : '' }}">
					<label class="control-label" for="path">@lang('admin/top_slider/messages.path')</label>
					<div class="controls">
						<input class="form-control" type="file" name="path" id="path"  />
						{!! $errors->first('path', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{ $errors->has('exposition_time_to') ? 'error' : '' }}">
					<label class="control-label" for="exposition_time_to">@lang('admin/top_slider/messages.exposition_time_to')</label>
					<div class="controls">
						<input class="form-control" type="text" name="exposition_time_to" id="exposition_time_to"  />
						{!! $errors->first('exposition_time_to', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="control-group {{ $errors->has('exposition_time_from') ? 'error' : '' }}">
					<label class="control-label" for="exposition_time_from">@lang('admin/top_slider/messages.exposition_time_from')</label>
					<div class="controls">
						<input class="form-control" type="text" name="exposition_time_from" id="exposition_time_from"  />
						{!! $errors->first('exposition_time_from', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="control-group">
			<div class="controls">
				<a href="{{ url('cms/top_slider/') }}" class="btn-cancel">@lang('button.return')</a>
				<button type="submit" class="btn btn-success">@lang('button.confirm')</button>
			</div>
		</div>
		<br>
		<br>
	</form>
@stop
@section('scripts')
	<script>
		$(document).ready(function(){
			$('#exposition_time_to').datetimepicker({
				pickTime: false,
				language: 'lv',
				format: 'YYYY-MM-DD'
			});
			$('#exposition_time_from').datetimepicker({
				pickTime: false,
				language: 'lv',
				format: 'YYYY-MM-DD'
			});
		});
	</script>
@stop