@extends('cms.layouts.default')

@section('title')
	{{ $title }} :: @parent
@stop

@section('content')
	<div class="page-header">
		<h3>
			{{ $title }}
		</h3>
	</div>

	<table id="banners" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span1">@lang('admin/banners/table.name')</th>
				<th class="span1">@lang('admin/banners/table.type')</th>
				<th class="span1">@lang('admin/banners/table.sort')</th>
				<th class="span1">@lang('admin/banners/table.active')</th>
				<th class="span1">@lang('admin/banners/table.size')</th>
				<th class="span2">@lang('admin/banners/table.from')</th>
				<th class="span2">@lang('admin/banners/table.to')</th>
				<th class="span1">@lang('table.actions')</th>
			</tr>
		</thead>
	</table>
	<div>
	    <a class="btn btn-info" href="{{  route('cms_banner_create_get') }}">@lang('button.create')</a>
	</div>
	<br>
    <br>
@stop

@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#banners').dataTable( {
		        "sAjaxSource": "{!! URL::to('cms/banners/data') !!}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop