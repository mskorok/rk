@extends('cms.layouts.default')

@section('content')

<div class="page-header">
    <h3>
        {{ $title }}
    </h3>
</div>

	<form class="form-horizontal" method="post" action="{{ $action }}" autocomplete="off" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="tab-content">
			<div class="tab-pane active" id="tab-general">

				<div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
					<label class="control-label" for="name">@lang('admin/banners/messages.name')</label>
					<div class="controls">
						<input class="form-control" type="text" name="name"  id="name" value="{{  $old ? $old['name'] : $banner->name }}" />
						{!! $errors->first('name', '<span class="help-inline">:message</span>') !!}
					</div>
				</div>

				<div class="form-group {{ $errors->has('type') ? 'error' : '' }}">
					<label class="control-label" for="type">@lang('admin/banners/messages.type')</label>
					<div class="controls">
						<select name="type" id="type" class="form-control">
							<option value="image" {{ ($old ? $old['type'] : $banner->type) === 'image' ? ' selected="selected"' : '' }}>
								@lang('admin/banners/messages.image')
							</option>
							<option value="flash" {{ ($old ? $old['type'] : $banner->type) === 'flash' ? ' selected="selected"' : '' }}>
								@lang('admin/banners/messages.flash')
							</option>
							<option value="html" {{ ($old ? $old['type'] : $banner->type) === 'html' ? ' selected="selected"' : '' }}>
								@lang('admin/banners/messages.html')
							</option>
							<option value="ajax_html" {{ ($old ? $old['type'] : $banner->type) === 'ajax_html' ? ' selected="selected"' : '' }}>
								@lang('admin/banners/messages.ajax_html')
							</option>
							<option value="xml" {{ ($old ? $old['type'] : $banner->type) === 'xml' ? ' selected="selected"' : '' }}>
								@lang('admin/banners/messages.xml')
							</option>
						</select>
					</div>
				</div>

                <div class="control-group {{ $errors->has('sort') ? 'error' : '' }}">
                    <label class="control-label" for="sort">@lang('admin/banners/messages.sort')</label>
                    <div class="controls">
                        <input class="form-control" type="text" name="sort" id="sort" value="{{  $old ? $old['sort'] : $banner->sort }}" />
                        {!! $errors->first('sort', '<span class="help-inline">:message</span>') !!}
                    </div>
                </div>

                <div class="checkbox {{ $errors->has('active') ? 'error' : '' }}">
                    <label>
                        <input type="checkbox" name="active" value="1" id="active" @if($old ? $old['active'] : $banner->active) checked @endif>
                        @lang('admin/banners/messages.active')
                    </label>
                    {!! $errors->first('active', '<span class="help-inline">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('size') ? 'error' : '' }}">
                    <label class="control-label" for="size">@lang('admin/banners/messages.type')</label>
                    <div class="controls">
                        <select name="size" id="size" class="form-control">
                            <option value="1140 х 250" {{ ($old ? $old['size'] : $banner->size) === '1140 х 250' ? ' selected="selected"' : '' }}>
                                @lang('admin/banners/messages.1140')
                            </option>
                            <option value="555 х 160" {{ ($old ? $old['size'] : $banner->size) === '555 х 160' ? ' selected="selected"' : '' }}>
                                @lang('admin/banners/messages.555')
                            </option>
                            <option value="360 х 160" {{ ($old ? $old['size'] : $banner->size) === '360 х 160' ? ' selected="selected"' : '' }}>
                                @lang('admin/banners/messages.360')
                            </option>
                            <option value="262 х 160" {{ ($old ? $old['size'] : $banner->size) === '262 х 160' ? ' selected="selected"' : '' }}>
                                @lang('admin/banners/messages.262')
                            </option>
                        </select>
                    </div>
                </div>

                <div class="control-group {{ $errors->has('path') ? 'error' : '' }}">
                    <label class="control-label" for="path">@lang('admin/banners/messages.path')</label>
                    <div class="controls">
                        <input class="form-control" type="file" name="path" id="path" />
                        {!! $errors->first('path', '<span class="help-inline">:message</span>') !!}
                    </div>
                </div>

                <div class="control-group {{ $errors->has('code') ? 'error' : '' }}">
                    <label class="control-label" for="code">@lang('admin/banners/messages.code')</label>
                    <div class="controls">
                        <textarea class="form-control"  name="code" id="code" >{{  $old ? $old['code'] : $banner->code }}</textarea>
                        {!! $errors->first('code', '<span class="help-inline">:message</span>') !!}
                    </div>
                </div>

                <div class="control-group {{ $errors->has('exposition_time_to') ? 'error' : '' }}">
                    <label class="control-label" for="exposition_time_to">@lang('admin/banners/messages.exposition_time_to')</label>
                    <div class="controls">
                        <input class="form-control" type="text" name="exposition_time_to" id="exposition_time_to" value="{{  $old ? $old['exposition_time_to'] : $banner->exposition_time_to }}" />
                        {!! $errors->first('exposition_time_to', '<span class="help-inline">:message</span>') !!}
                    </div>
                </div>

                <div class="control-group {{ $errors->has('exposition_time_from') ? 'error' : '' }}">
                    <label class="control-label" for="exposition_time_from">@lang('admin/banners/messages.exposition_time_from')</label>
                    <div class="controls">
                        <input class="form-control" type="text" name="exposition_time_from" id="exposition_time_from" value="{{  $old ? $old['exposition_time_from'] : $banner->exposition_time_from }}" />
                        {!! $errors->first('exposition_time_from', '<span class="help-inline">:message</span>') !!}
                    </div>
                </div>
			</div>
		</div>
        <br>
        <br>
		<div class="control-group">
			<div class="controls">
				<a href="{{ url('cms/banners/') }}" class="btn-cancel">@lang('button.return')</a>
				<button type="submit" class="btn btn-success">@lang('button.confirm')</button>
			</div>
		</div>
        <br>
        <br>
	</form>
@stop
@section('scripts')
    <script>
        $(document).ready(function(){
            $('#exposition_time_to').datetimepicker({
                pickTime: false,
                language: 'lv',
                format: 'YYYY-MM-DD'
            });
            $('#exposition_time_from').datetimepicker({
                pickTime: false,
                language: 'lv',
                format: 'YYYY-MM-DD'
            });
        });
    </script>
@stop