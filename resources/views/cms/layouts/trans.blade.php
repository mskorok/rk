<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>
            @section('title')
            CMS skolas.rigaskarte.lv @if (App::environment() === 'dev') DEV @elseif (App::environment() == 'local') LOCAL @endif
            @show
        </title>

        <meta name="keywords" content="@yield('keywords')" />
        <meta name="author" content="@yield('author')" />

        <meta name="description" content="skolas.rigaskarte.lv admin" />

        <!--  Mobile Viewport Fix -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <link rel="shortcut icon" href="{{ asset('assets/ico/favicon.png') }}">

        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" type="text/css" href="/assets/compiled/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="/assets/compiled/css/other.css" />
        @yield('css')
        <style>
            @section('styles')
            @show
        </style>

        <style>
            body {
                padding-top: 140px;
            }
        </style>
    </head>

    <body>
        <div class="container">

            <!-- Navbar -->
            <div class="navbar navbar-default navbar-fixed-top">

                <div class="container">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Izvēlne</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <a class="navbar-brand" href="/cms">SKOLAS.RIGASKARTE.LV CMS</a>
                    </div>

                    <div class="collapse navbar-collapse visible-lg" id="navbar-collapse-1">
                        <ul class="nav navbar-nav pull-left">
                            <li{!! (Request::is('cms') ? ' class="active"' : '') !!}><a href="{{ URL::to('cms') }}"><i class="icon-home icon-white"></i> Pārskats</a></li>
                            <li{!! (Request::is('cms/banners*') ? ' class="active"' : '') !!}><a href="{{ URL::to('cms/banners') }}"> Banners</a></li>
                            <li{!! (Request::is('cms/top_slider*') ? ' class="active"' : '') !!}><a href="{{ URL::to('cms/top_slider') }}"> Top Slider</a></li>

                            <li{!! (Request::is('cms/translations') ? ' class="active"' : '') !!}><a href="{{ URL::to('cms/translations') }}"> Translations</a></li>

                        </ul>
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="glyphicon glyphicon-user"></span> {{ Auth::user()->username }} {{ Auth::user()->surname }}	<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to('/') }}"><i class="icon-wrench"></i> Sākumlapa</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::to('auth/logout') }}"><i class="icon-share"></i> Iziet</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- ./ nav-collapse -->
                </div>
                <!-- ./ container -->
            </div><!-- ./ navbar -->


            <!-- Notifications -->
            @include('notifications')
            <!-- ./ notifications -->

            <!-- Content -->
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div>
            </div>
            <!-- ./ content -->

            <!-- Footer -->
            <footer class="clearfix">
                @yield('footer')
            </footer>
            <!-- ./ Footer -->

        </div>
        <!-- Javascripts
        ================================================== -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script>

        <script src="/assets/compiled/js/all.js"></script>

        {{--<script type="text/javascript">--}}
{{--$(document).ready(function ()--}}
{{--{--}}

    {{--var r = $('.slider').slider({--}}
        {{--value: 0,--}}
        {{--step: 0.01,--}}
        {{--tooltip: 'hide'--}}
    {{--})--}}
            {{--.on('slide', function (ev) {--}}
                {{--$('#sumv').val(r.getValue());--}}

                {{--var tmp = parseFloat(r.getValue());--}}

                {{--betalons = Math.round((0 - min + tmp) * 100) / 100;--}}
                {{--bkonts = Math.round((max - tmp) * 100) / 100;--}}

                {{--$('#bkonts').val(bkonts);--}}
                {{--$('#betalons').val(betalons);--}}
            {{--})--}}
            {{--.data('slider');--}}

    {{--if (r != null)--}}
    {{--{--}}
        {{--var min = r.getMin();--}}
        {{--var max = r.getMax();--}}

        {{--$('#bkonts').val(max);--}}
        {{--$('#betalons').val(0 - min);--}}
        {{--var bkonts = max;--}}
        {{--var betalons = 0 - min;--}}
    {{--}--}}

    {{--// ar roku ierakstita vertiba--}}
    {{--$('#sumv').change(function ()--}}
    {{--{--}}
        {{--if ($(this).val() > max)--}}
        {{--{--}}
            {{--$(this).val(max);--}}
            {{--r.setValue(max);--}}

            {{--bkonts = 0;--}}
            {{--betalons = 0 - min + max;--}}

        {{--} else if ($(this).val() < min)--}}
        {{--{--}}
            {{--$(this).val(min);--}}
            {{--r.setValue(min);--}}

            {{--betalons = 0;--}}
            {{--bkonts = bkonts + (0 - min);--}}
        {{--} else {--}}
            {{--r.setValue($(this).val());--}}
            {{--var tmp = parseFloat($(this).val());--}}

            {{--betalons = Math.round((0 - min + tmp) * 100) / 100;--}}
            {{--bkonts = Math.round((max - tmp) * 100) / 100;--}}
        {{--}--}}

        {{--$('#bkonts').val(bkonts);--}}
        {{--$('#betalons').val(betalons);--}}

        {{--$('#sum').val(tmp);--}}
    {{--});--}}

    {{--$('element.close_popup, .btn-inverse').click(function () {--}}
        {{--parent.oTable.fnReloadAjax();--}}
        {{--parent.$.colorbox.close();--}}
        {{--return false;--}}
    {{--});--}}

    {{--$('button[type="submit"].close_popup').click(function (e) {--}}

        {{--if (self == top)--}}
        {{--{--}}
            {{--return true;--}}
        {{--} else {--}}
            {{--e.preventDefault();--}}
            {{--$.post(window.location, $("form:first").serialize());--}}

            {{--setTimeout(function () {--}}
                {{--parent.oTable.fnReloadAjax();--}}
                {{--parent.$.colorbox.close();--}}

            {{--}, 400);--}}

            {{--return false;--}}
        {{--}--}}

    {{--});--}}

    {{--$('#datetimepicker1').datetimepicker({--}}
        {{--pickTime: false,--}}
        {{--language: 'lv',--}}
        {{--format: 'YYYY-MM-DD'--}}
    {{--});--}}
    {{--$('#datetimepicker2').datetimepicker({--}}
        {{--pickTime: false,--}}
        {{--language: 'lv',--}}
        {{--format: 'YYYY-MM-DD'--}}
    {{--});--}}
{{--});--}}
        {{--</script>--}}

        @yield('scripts')

    </body>

</html>