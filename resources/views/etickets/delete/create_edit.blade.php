@extends('site.layouts.default')

@section('content')

	@include('partials.form.formstart', array('action' => ''))

			@include('partials.form.ipassword')

			<span class="help-block">
				Jūsu konta atlikums ir {{{ $balance }}} EUR.<br />
				Profila dzēšana ir neatgriezeniska un Jūsu konta atlikums Jums netiks atgriezts!
			</span>

		<div class="form-group">
			<a href="/etickets" class="btn btn-cancel">Atcelt</a>
			<button type="submit" class="btn btn-danger">Dzēst profilu!</button>
		</div>

	</form>
</div>	
@stop