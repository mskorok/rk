@extends('site.layouts.default')

@section('content')





	@include('partials.form.formstart', array('action' => '/etickets/mtransfer/create'))

		<div class="form-group {{{ $errors->has('eticket') ? 'has-error' : '' }}}">
			<label class="control-label" for="eticket">E-talons</label>
			<div class="controls">
				<select class="form-control" name="eticket" id="eticket">
					@foreach ($all_etickets as $single)
						<option value="{{{ $single->id }}}">{{{ $single->nr }}} {{{ $single->name }}}</option>
					@endforeach
				</select>
				{!! $errors->first('eticket', '<span class="help-inline">:message</span>') !!}
			</div>
		</div>

        <div class="form-group {{{ $errors->has('summa') ? 'error' : '' }}}">
            <label class="control-label" for="summa">Papildināt e-talona bilanci par EUR</label>
            <input class="form-control" type="text" name="summa" id="summa" maxlength="8" value="{{{ Input::old('summa', null) }}}" />
            {!! $errors->first('summa', '<span class="help-inline">:message</span>') !!}
        </div>

		<div class="form-group">
			<label class="control-label">{!! $info !!}</label>
		</div>

	@include('partials.form.formend', array('cancel' => 'etickets'))
@stop