<h1>Maksājuma uzdevums</h1>
<table cellspacing="5" cellpadding="2">
    <tr>
        <td>Dokumenta nr:</td>
        <td style="border:1px solid #444;">{!! $manual_payment->id !!}</td>
    </tr>
    <tr>
        <td>Saņēmējs:</td>
        <td style="border:1px solid #444;">{!! str_replace("\\n","<br />",$rekviziti) !!}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Informācija saņēmējam:</td>
        <td style="border:1px solid #444;font-size:12px;">Maksājums no skolas.rigaskarte.lv ar identifikatoru {!! $manual_payment->payment_code !!} par skolēna e karti nr. {!! $manual_payment->eticket->nr !!}</td>
    </tr>
        <tr>
        <td>Izveidošanas datums:</td>
        <td style="border:1px solid #444;">{!! $manual_payment->created_date() !!}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Summa apmaksai:</td>
        <td style="border:1px solid #444;">{!! $manual_payment->humanUserAmount() !!}</td>
    </tr>
</table>