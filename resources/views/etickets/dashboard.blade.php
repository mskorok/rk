@extends('site.layouts.default')

@section('content')
	<div class="container">
		<div class="row col-xs-12">
			<div class="pull-left"><h4>Šībrīža bilance : {!! $account->humanBalance() !!} EUR</h4></div>
		</div>
	</div>
@stop