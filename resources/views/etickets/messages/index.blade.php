@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<table id="comments" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="span3" style="background-color:#333;color:white;">{{{ Lang::get('etickets/messages/table.title') }}}</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('etickets/messages/table.status') }}}</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('etickets/messages/table.type') }}}</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('etickets/messages/table.created_at') }}}</th>
				<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
		<tbody>

            @if (count($data)>0)
            @foreach ($data as $single)
                @if ($single->ticket_type == 4)
                <tr>
                    <td>Naudas atgriešana uz e-talonu: {!!$single->eticket_nr!!}, {!! intval($single->content)/100 !!} EUR</td>
                    <td>
                        @if ($single->ticket_status == 0 && Auth::user()->id == $single->user_id)
                        Gaida uz apstiprinājumu
                        @elseif ($single->ticket_status == 1)
                        Izpildīts
                        @elseif ($single->ticket_status == 2)
                        Atcelts
                        @else
                        Nepieciešams apstiprināt
                        @endif
                    </td>
                    <td>@if (isset($single->type->name)) {!! $single->type->name  !!} @endif</td>
                    <td>{!! $single->created_at !!}</td>
                    <td>
                        @if ($single->ticket_status == 0 && Auth::user()->id == $single->recipient_id)
                            <a href="{{{ URL::to('etickets/messages/' . $single->id . '/mconfirm' ) }}}" class="btn btn-sm btn-primary">{{{ Lang::get('button.confirm') }}}</a>
                            <a href="{{{ URL::to('etickets/messages/' . $single->id . '/mcancel' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get('button.cancel') }}}</a>
                        @endif
                    </td>
                </tr>
            	@else
					<tr>
						<td>{!! $single->content !!}</td>
						<td>
							@if ($single->ticket_status == 2)
								Atcelts
							@elseif ($single->ticket_status == 1)
								Izpildīts
							@else
								Gaida uz izskatīšanu 
							@endif
						</td>
						<td>{!! $single->type->name !!}</td>
						<td>{!! $single->created_at !!}</td>
						<td>
							@if ($single->ticket_status == 0 && Auth::user()->id == $single->user_id)
								<a href="{{{ URL::to('etickets/messages/' . $single->id . '/cancel' ) }}}" class="btn btn-sm btn-danger">{{{ Lang::get('button.cancel') }}}</a>
							@endif
						</td>
					</tr>
				@endif
            @endforeach
            @endif
		</tbody>
	</table>
@stop