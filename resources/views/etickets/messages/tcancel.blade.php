@extends('site.layouts.default')


@section('content')

    <form class="form-horizontal" method="post" action="" autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{!! $objekts->id !!}" />
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="control-group">
            <div class="controls">
                <element class="/etickets/messages">{!! Lang::get('button.cancel') !!}</element>
                <button type="submit" class="btn btn-danger">{!! Lang::get('button.confirm') !!}</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop