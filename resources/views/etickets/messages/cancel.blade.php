@extends('site.layouts.default')


@section('content')

    <form class="form-horizontal" method="post" action="" autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{!! $objekts->id !!}" />
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="control-group">
            <div class="controls">
                <a href="{!! URL::to('etickets/messages') !!}" class="btn btn-cancel">{{{ Lang::get('button.return') }}}</a>
                <button type="submit" class="btn btn-danger">{!! Lang::get('button.cancel') !!}</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop