@extends('site.layouts.default')

@section('title')
	{{{ $title }}} :: @parent
@stop

@section('content')
	<div class="page-header">
		<h3>
			<div class="pull-right">
                @if (Auth::user()->terms == 1)
				<a 
				@if (App::environment() != 'testing')
						href="{{{ URL::to('etickets/etickets/create') }}}"
					class="btn btn-sm btn-info iframe"><span class="glyphicon glyphicon-plus"></span> Pievienot jaunu e-talonu</a>
				@endif
                @endif
				<a href="{{{ URL::to('etickets/delete') }}}" class="btn btn-sm btn-danger"><i class=""></i> Dzēst profilu</a>
			</div>
		</h3>
	</div>

	<table id="comments" class="table table-bordered">
		<thead>
			<tr>
				<th class="col-md-1" style="background-color:#333;color:white;">Avatars</th>
				<th class="col-md-2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.title') }}}</th>
				<th class="col-md-1" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.status') }}}</th>
				<th class="col-md-1" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.pin') }}}</th>
				<th class="col-md-1" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.balance') }}}</th>
				<th class="col-md-1" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.limit_type') }}}&nbsp;</th>
				<th class="col-md-1" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.limit') }}}</th>
				<th class="col-md-1" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
				<th class="col-md-2" style="background-color:#333;color:white;">{{{ Lang::get('table.actions') }}}</th>
				<th class="col-md-1" style="background-color:#333;color:white;">{{{ Lang::get('button.delete') }}}</th>
			</tr>
		</thead>
	</table>
@stop

<style type="text/css">
.blocked {
	background-color:#FFBFBF;
}
</style>

@section('scripts')
	<script type="text/javascript">
		var oTable;
		var oTable2;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
				"sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"bProcessing": true,
		        "bServerSide": true,
		        "bInfo": false,
		        "bPaginate": false,
		        "bFilter": false,
		        "bSort": false,
		        "aaSorting": [[ 7, "asc" ]],
		        "fnRowCallback": function( nRow, aaData, iDisplayIndex ) {
	                if ( aaData[2] == "Bloķēts" )
	                    {
	                        $('td:eq(0)', nRow).addClass( 'blocked' );
	                        $('td:eq(1)', nRow).addClass( 'blocked' );
	                        $('td:eq(2)', nRow).addClass( 'blocked' );
	                        $('td:eq(3)', nRow).addClass( 'blocked' );
	                        $('td:eq(4)', nRow).addClass( 'blocked' );
	                        $('td:eq(5)', nRow).addClass( 'blocked' );
	                        $('td:eq(6)', nRow).addClass( 'blocked' );
	                        $('td:eq(7)', nRow).addClass( 'blocked' );
	                        $('td:eq(8)', nRow).addClass( 'blocked' );
	                        $('td:eq(9)', nRow).addClass( 'blocked' );
	                    }
	                    return nRow;
	                },
		        "sAjaxSource": "{!! URL::to('etickets/etickets/data') !!}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop