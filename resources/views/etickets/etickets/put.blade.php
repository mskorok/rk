@extends('site.layouts.default')


@section('content')

    @include('partials.form.formstart', array('action' => ''))

        <input type="hidden" name="id" value="{!! Hashids::encode($objekts->id) !!}" />

        <div class="form-group" style="height:55px;">
            <label class="control-label col-md-2" for="bkonts">Pieejams konta atlikums</label>
            <input style="display:inline-block;width:100px;" class="form-control" type="text" id="bkonts" disabled="disabled" style="margin-left:50px;font-weight:bold;width:50px;" value="0" />  <b>EUR</b>
        </div>

        <div class="form-group" style="height:55px;">
            <label class="control-label col-md-2" for="betalons">E-talona balanss</label>
            <input style="display:inline-block;width:100px;" class="form-control" type="text" id="betalons" disabled="disabled" style="margin-left:50px;font-weight:bold;width:50px;" value="0" />  <b>EUR</b>
        </div>

        <div class="form-group {{{ $errors->has('sum') ? 'has-error' : '' }}}">
            <label class="control-label" style="text-align:left;" for="sum">Papildināt par summu</label>
                
            <input type="text" name="sum" id="sum" class="slider" value="0" data-slider-min="{{{ 0-$group_balance }}}" data-slider-max="{{{ $user_balance }}}" data-slider-value="0" data-slider-selection="after">
            <input class="form-control" style="display:inline-block;width:100px;" type="text" id="sumv" style="margin-left:50px;font-weight:bold;width:50px;" value="0" />  <b>EUR</b>

            {!! $errors->first('sum', '<span class="help-inline">:message</span>') !!}
        </div>

    @include('partials.form.formend', array('cancel' => 'etickets/etickets'))
@stop