@extends('site.layouts.default')

@section('content')

    @if (intval($objekts->humanBalance()) > 0)
        <p>Nav iespējams dzēst, jo bilance ir {{{ $objekts->humanBalance() }}} EUR. Šis atlikums NETIKS automātiski pārskaitīts uz Jūsu kontu!</p>
        <a href="{!! URL::to('etickets/etickets') !!}" class="btn btn-cancel">{{{ Lang::get('button.cancel') }}}</a>
    @else
        <form class="form-horizontal" method="post" action="" autocomplete="off">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <input type="hidden" name="id" value="{!! $objekts->id !!}" />

            <div class="control-group">
                <div class="controls">
                    <a href="{!! URL::to('etickets/etickets') !!}" class="btn btn-cancel">{{{ Lang::get('button.cancel') }}}</a>
                    <button type="submit" class="btn btn-danger close_popup">{!! Lang::get('button.delete') !!}</button>
                </div>
            </div>
        </form>
    @endif

@stop