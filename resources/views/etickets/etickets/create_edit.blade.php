@extends('site.layouts.modal')

@section('content')

<?php $action=''; ?>
<?php 
if (isset($objekts)) 
{
	$action = URL::to('etickets/etickets/' . Hashids::encode($objekts->id) . '/edit');
}
else $objekts=null;
?>
@include('partials.form.formstartfile', array('action' => $action))

		<div class="row">
			<div class="col-md-8">

				@if (!isset($objekts))
					<div class="form-group {{{ $errors->has('nr') ? 'has-error' : '' }}}">
						<label class="control-label" for="nr">Nr (10 cipari)</label>
							<input class="form-control" type="text" name="nr" id="nr" maxlength="10" value="{{{ Input::old('nr', isset($objekts) ? $objekts->nr : null) }}}" />
							{!! $errors->first('nr', '<span class="help-inline">:message</span>') !!}
					</div>

					@include('partials.form.ipk', array('objekts' => $objekts ))
				@endif

				@if ($mode!='edit')
				<div class="form-group {{{ $errors->has('name') ? 'has-error' : '' }}}">
					<label class="control-label" for="name">Vārds, uzvārds</label>
						<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', isset($objekts) ? $objekts->name : null) }}}" />
						{!! $errors->first('name', '<span class="help-inline">:message</span>') !!}
				</div>
				@endif

				<div class="form-group {{{ $errors->has('pin') ? 'has-error' : '' }}}">
					<label class="control-label" for="pin">Pin kods</label>
						<input class="form-control" type="text" name="pin" id="pin" maxlength="4" value="{{{ Input::old('pin', isset($objekts) ? $objekts->pin : '0000') }}}" />
						{!! $errors->first('pin', '<span class="help-inline">:message</span>') !!}
				</div>

				@if ($mode=='edit')
					@if ($objekts->group_id==0)
					<div class="form-group {{{ $errors->has('limit_type') ? 'has-error' : '' }}}">
		                <label class="control-label" for="limit_type">Limita veids</label>
			                <select class="form-control" name="limit_type" id="limit_type">
		                        	<option value="0"> --- </option>
			                        @foreach ($limiti as $single)
										@if ($mode == 'create')
			                        		<option value="{{{ $single->id }}}">{{{ $single->name }}}</option>
			                        	@else
											<option value="{{{ $single->id }}}" @if ($single->id==$objekts->limit_type) selected="selected" @endif;>{{{ $single->name }}}</option>
										@endif
			                        @endforeach
							</select>
							{!! $errors->first('limit_type', '<span class="help-inline">:message</span>') !!}
					</div>

					<div class="form-group {{{ $errors->has('limit') ? 'has-error' : '' }}}">
						<label class="control-label" for="limit">Limits EUR</label>
							<input class="form-control" type="text" name="limit" id="limit" value="{{{ Input::old('limit', isset($objekts) ? $objekts->humanLimit() : null) }}}" />
							{!! $errors->first('limit', '<span class="help-inline">:message</span>') !!}
					</div>
					@endif
				@endif

				@if ($mode!='edit')
                <div class="form-group {{{ $errors->has('skola') ? 'error' : '' }}}">
                    <label class="control-label" for="skola">Skola</label>
					{!! Form::select('skola', $skolas, Input::old('skola'), ["class" => "form-control", "id" => "skola"]) !!}
                    {!! $errors->first('skola', '<span class="help-inline">:message</span>') !!}
                </div>

				<div class="form-group {{{ $errors->has('grade') ? 'error' : '' }}}">
					<label class="control-label" for="grade">Klase (1a-12z)</label>
						<input class="form-control" placeholder="klase ar burtu, piem: 1a" type="text" name="grade" id="grade" maxlength="3" value="{{{ Input::old('grade', isset($objekts) ? $objekts->grade : '') }}}" />
						{!! $errors->first('grade', '<span class="help-inline">:message</span>') !!}
				</div>
				@endif

			</div>
			<div class="col-md-4">
				<div style="width:150px;height:150px;">
					@if ($thumb_name!='')
						<img width="150" style="border:1px solid grey;" src="/{!! $thumb_name !!}" />
					@else
						<img src="/assets/img/no_avatar.jpg" />
					@endif
				</div>
				<div class="new_Btn btn btn-sm btn-success">Izvēlēties attēlu</div><br />
				<input id="html_btn" style="display:none;" name="file" type="file" />
				<div id="fakepath"></div>
			</div>

		</div>
		<div class="row">

			<div class="form-group">
				<div class="controls">
					<element class="btn btn-sm btn-warning close_popup">Atcelt</element>
					<button type="submit" class="btn btn-sm btn-success">Saglabāt</button>
				</div>
			</div>

		</div>
	</form>
</div>
@stop