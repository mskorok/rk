<h3>Konta pārvaldniekam {!! $payment_user->fullname() !!} jāapstiprina pirmais maksājums!</h3>

<p>Summa - {!! sprintf("%.2f",$summa) !!} EUR</p>
<p>Konta numurs {!! $konts !!}</p>
<p>Maksājuma nr. - {!! $payment->id !!}</p>
<p>E-talona nr. - {!! App\Models\Eticket::where('id',$payment->eticket_id)->first()->nr !!}</p>
<p>Maksājuma identifikācijas kods - {!! $payment->payment_code !!}</p>