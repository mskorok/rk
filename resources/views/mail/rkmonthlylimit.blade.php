<h3>Personai šomēnes ir sasniegts 140.00 EUR maksājumu limits!</h3>

<p>Persona - {!! $eticket->name !!} ({!! $eticket->pk !!}), e-talonu nr. {!! $all_eticket_nrs !!}</p>
<p>Šībrīža mēneša maksājumu kopsumma - {!! sprintf("%.2f",$monthly_sum/100) !!} EUR</p>
<p>Pēdējais apstiprinātais maksājums Nr. {!! $payment->id !!} par {!! sprintf("%.2f",$summa) !!} EUR</p>
<p>Konta pārvaldnieks - {!! $owner !!}</p>