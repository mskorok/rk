<p>Sveicināti, Cien./God. Konta pārvaldnieks {!! $user->username !!} {!! $user->surname !!}!
Informējam Jūs, ka esat veiksmīgi pievienojis personu apliecinoša dokumenta kopiju.
Sistēmas pārvaldnieks, saskaņā ar Latvijas Republikas normatīvo aktu prasībām, veiks padziļināto izpēti un informēs Jūs par tās rezultātiem ne vēlāk kā 1 (vienas) darba dienas laikā.</p>