@extends('site.layouts.default')

@section('content')

	<div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
					<th class="span2" style="background-color:#333;color:white;">Veids</th>
					<th class="span2" style="background-color:#333;color:white;">Statuss</th>
					<th class="span2" style="background-color:#333;color:white;">Fails</th>
				</tr>
			</thead>
			<tbody>
				@if (count($data)>0)
					@foreach ($data as $single)
							<tr>		
								<td>{!! $single->created_at !!}</td>
								<td>@if ($single->to_account_id == 999996) Brīvpusdienu kopsavilkums par mēnesi @else Naudas izmaksa @endif</td>
								<td>@if ($single->confirmed==1) Izpildīts @else Gaida apstiprināšanu @endif</td>
                                @if ($single->to_account_id == 999996)
                                    <td><a target="_blank" href="{!! URL::to('outgoing/freepdf/'.$single->id) !!}">PDF</a></td>
                                @else
                                    <td><a target="_blank" href="{!! URL::to('outgoing/pdf/'.$single->id) !!}">PDF</a></td>
                                @endif
							</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
@stop