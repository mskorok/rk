<!DOCTYPE html>
<html lang="en">
<head id="Skolas.rigaskarte.lv">

	<meta charset="UTF-8">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>
		@section('title')
			{{{ $title }}} :: Modal
		@show
	</title>

	<meta name="keywords" content="@yield('keywords')" />
	<meta name="author" content="@yield('author')" />

	<meta name="description" content="@yield('description')" />

	<!--  Mobile Viewport Fix -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- CSS
    ================================================== -->
	<link rel="stylesheet" type="text/css" href="/assets/compiled/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="/assets/compiled/css/other.css" />

	<style>
	body {
		padding: 60px 0;
	}
	</style>

	@yield('styles')

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body>
	<!-- Container -->
	<div class="container">

		<!-- Notifications -->
		@include('notifications')
		<!-- ./ notifications -->

		<div class="page-header">
			<h3>
				{!! $title !!}
			</h3>
		</div>

		<!-- Content -->
		@yield('content')
		<!-- ./ content -->

		<!-- Footer -->
		<footer class="clearfix">
			@yield('footer')
		</footer>
		<!-- ./ Footer -->

	</div>
	<!-- ./ container -->

	<!-- Javascripts -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script>

	<script src="/assets/compiled/js/all.js"></script>

    <script type="text/javascript">
    	$(document).ready(function(){
			$('.close_popup').click(function(){
				parent.oTable.fnReloadAjax();
				parent.$.colorbox.close();
				return false;
			});

			$('.new_Btn').bind("click" , function () {
			    $('#html_btn').click();
			});

			$('#html_btn').change(function() {
				var v  = $("#html_btn").val();
				$("#fakepath").html(v.substring(12));
			});
		});
       	$(prettyPrint)
    </script>

    @yield('scripts')

</body>

</html>