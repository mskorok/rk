<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
		@section('title')
			skolas.rigaskarte.lv @if (App::environment() == 'dev') DEV @elseif (App::environment() == 'local') LOCAL @endif
		@show
	</title>
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="csrf-token" content="<?= csrf_token() ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/assets/compiled/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/compiled/css/other.css" />

    <style>
        @section('styles')
		@show
    </style>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">
	<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">
</head>

<body>
@include('ec_directive_disabled')
@include('portal.views.partials.cookie')
<!-- To make sticky footer need to wrap in a div -->
<div id="wrap">
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">

			<div class="navbar-header">
				<a class="navbar-brand" href="/">skolas.rigaskarte.lv</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				<ul class="nav navbar-nav pull-left">
					@if (App::environment() != 'production' && 1==2)
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								@if ( Session::has('locale') )
									@if (Session::get('locale')=='en')
										<i class="cus-gb"></i> Englixsh <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="{{{ URL::to('/lv') }}}"><i class="cus-lv"></i> Latviski</a></li>
								@else
									<i class="cus-lv"></i> Latviski <span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a href="{{{ URL::to('/en') }}}"><i class="cus-gb"></i> English</a></li>
										@endif
										@else
											<i class="cus-lv"></i> Latviski <span class="caret"></span>
											</a>
											<ul class="dropdown-menu">
												<li><a href="{{{ URL::to('/en') }}}"><i class="cus-gb"></i> English</a></li>
												@endif
											</ul>
						</li>
					@endif
					<li><a href="/contact" class="iframe">Palīdzība</a></li>
					<li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Lietošanas pamācība <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a target="_blank" href="/pamaciba_lv.pdf">Lietošanas pamācība (latviešu valodā)</a></li>
                            <li><a target="_blank" href="/pamaciba_ru.pdf">Lietošanas pamācība (krievu valodā)</a></li>
                        </ul>
					</li>
				</ul>


				@if (Auth::check())
					<ul class="nav navbar-nav pull-right">
						<li><a href="{{{ URL::to('auth/logout') }}}">Iziet</a></li>
					</ul>
				@endif

			</div>
		</div>
	</div>
	<!-- ./ navbar -->

	<!-- Container -->
	<div class="container">
		@if (Auth::check())
			<div class="row" style="margin-bottom:20px;">
				<div class="col-md-6">
					<h3>Sveiki!</h3>
					<h2>{{{ $u->username.' '.$u->surname }}}</h2>
				</div>
				<div class="col-md-6" style="text-align:right;">
					@if (count($roleids)==0 || array_search(1, $roleids)!== false)
						<h4>Aktīvi E-taloni: @if (isset($active_etickets)) {!! $active_etickets !!} @endif</h4>
						<h4 style="display:inline">Konta atlikums:</h4> <h3 style="display:inline">@if (isset($account)) @if ($account instanceof App\Models\Account) {!! $account->humanBalance() !!} @else 0.00 @endif @endif EUR</h3>
					@elseif (array_search(3, $roleids)!== false)
                        <table border="0" style="width:100%;">
                            <tr>
                                <td style="text-align:right;">
                                    <h4 style="display:inline;">Tirgotāja konta atlikums:</h4>
                                    <h3 style="display:inline;">@if (isset($seller_balance)) {!! $seller_balance !!} @endif EUR</h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;">
                                    <h4 style="display:inline;">Šomēnes izsniegtās brīvpusdienas:</h4>
                                    <h3 style="display:inline;">@if (isset($seller_free_meal_count)) {!! $seller_free_meal_count !!} @endif</h3>
                                </td>
                            </tr>
                        </table>
					@elseif (array_search(2, $roleids)!== false)
						<h4>E-taloni objektā: @if (isset($object_eticket_count)) {!! $object_eticket_count !!} @endif</h4>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<nav class="navbar navbar-default" role="navigation">
						<div class="container">
							<ul class="nav navbar-nav navbar-left">
								@if ((array_search(3, $roleids)=== false) && (array_search(2, $roleids)=== false))
									@if (count($roleids)==0 || array_search(1, $roleids)!== false)
										<li class="dropdown">
											<a class="dropdown-toggle" role="button" data-toggle="dropdown"  data-target="{{{ URL::to('etickets') }}}" href="{{{ URL::to('etickets') }}}">Mans konts <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="{{{ URL::to('etickets/etickets') }}}">Mani E-taloni</a></li>
											</ul>
										</li>
									@elseif (count($granted)>0)
										<li><a href="{{{ URL::to('etickets/etickets') }}}">E-taloni</a></li>
									@endif
								@endif
								@if ((array_search(4, $roleids)=== false) AND (array_search(5, $roleids)=== false))
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#">Vēsture <span class="caret"></span></a>
										<ul class="dropdown-menu">
											@if (count($roleids)==0 || array_search(1, $roleids)!== false)
												<li><a href="{{{ URL::to('reports/account') }}}">Konta vēsture</a></li>
												@if (isset($active_etickets))
													@if ($active_etickets>0)
														<li><a href="{{{ URL::to('reports/eticket') }}}">E-talonu vēsture</a></li>
													@endif
												@endif
												<li><a href="{{{ URL::to('reports/payments') }}}">Maksājuma uzdevumi</a></li>
											@endif
											@if (array_search(3, $roleids)!== false)
												<li><a href="{{{ URL::to('reports/seller') }}}">Tirgotāja atskaite</a></li>
												<li><a href="{{{ URL::to('outgoing/history') }}}">Izmaksu vēsture</a></li>
											@endif
											@if (array_search(2, $roleids)!== false)
												<li><a href="{{{ URL::to('reports/objekts') }}}">Objekta atskaite</a></li>
												<li><a href="{{{ URL::to('reports/objetickets') }}}">Objekta e-talonu atskaite</a></li>
												<li><a href="{{{ URL::to('reports/objseller') }}}">Objekta tirgotāju atskaite</a></li>
											@endif
										</ul>
									</li>
								@endif
								@if (count($roleids)==0 || array_search(1, $roleids)!== false)
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            Mans profils <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{{ URL::to('/') }}}">Labot profila pamatinformāciju</a></li>
                                            @if ($u->terms == 0 || $u->terms == 3)<li><a href="{{{ URL::to('user/editform') }}}">Labot reģistrācijas anketu</a></li>@endif
                                            @if ($u->terms == 0 || $u->terms == 3)<li><a href="{{{ URL::to('dokuments') }}}">Augšuplādēt personu apliecinošu dokumentu</a></li>@endif
                                        </ul>
                                    </li>
									@if (App::environment() != 'testing')
										<li><a href="{{{ URL::to('etickets/messages') }}}">Paziņojumi @if ($new_notifs>0) <span class="badge">{!!$new_notifs!!}</span> @endif</a></li>
									@endif

								@elseif ((array_search(3, $roleids)!== false) || (array_search(2, $roleids)!== false))
									<li><a href="{{{ URL::to('/') }}}">Mans profils</a></li>
									@if (array_search(3, $roleids)!== false)
										@if (App::environment() != 'testing')
											<li><a href="{{{ URL::to('etickets/messages') }}}">Paziņojumi @if ($new_notifs>0) <span class="badge">{!!$new_notifs!!}</span> @endif</a></li>
										@endif
									@endif
								@endif
								@if (array_search(3, $roleids)!== false)
										<li>
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">
												<i class="icon-user icon-white"></i> Instrukcijas <span class="caret"></span>
											</a>
											<ul class="dropdown-menu">
                                                <li><a href="/isa_instrukcija">Īsa instrukcija</a></li>
                                                <li><a href="/aprikojuma_instrukcija">Aprīkojuma lietošanas instrukcija</a></li>
                                                <li><a href="/aprikojuma_instrukcijaru">Aprīkojuma lietošanas instrukcija krievu valodā</a></li>
                                                <li><a href="/kludu_pazinojumi">Kļūdu paziņojumi</a></li>
											</ul>
										</li>
								@endif
								@if (array_search(1, $roleids)!== false)
									<li><a href="{{{ URL::to('admin') }}}">Administrācija</a></li>
								@endif
								@if (array_search(4, $roleids)!== false)
									<li><a href="/">Profils</a><li>
									<li><a href="/accountant/mtransfer">Naudas iemaksa sistēmā</a><li>
                                    <li>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="icon-user icon-white"></i> Iemaksu imports <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="/accountant/mimport">Fidavista</a><li>
                                            <li><a href="/accountant/mimport2">CSV</a><li>
                                        </ul>
                                    </li>
									<li><a href="/accountant/index">Naudas izmaksa tirgotājiem</a><li>
									<li>
										<a class="dropdown-toggle" data-toggle="dropdown" href="#">
											<i class="icon-user icon-white"></i> Pārskati <span class="caret"></span>
										</a>
										<ul class="dropdown-menu">
											<li{!! (Request::is('reports/akts*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/akts') }}}"><span class="glyphicon glyphicon-euro"></span> Salīdzināšanas akts</a></li>
											<li{!! (Request::is('reports/grfreemeals*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/grfreemeals') }}}"><span class="glyphicon glyphicon-euro"></span> Brīvpusdienu atskaite</a></li>
											<li{!! (Request::is('reports/seller*') ? ' class="active"' : '') !!}><a href="{{{ URL::to('reports/seller') }}}"><span class="glyphicon glyphicon-euro"></span> Tirgotāja atskaite</a></li>
										</ul>
									</li>
								@endif
								@if (array_search(5, $roleids)!== false)
									<li><a href="/">Profils</a><li>
									<li><a href="/rk/index">Lietotāju apstiprināšana</a><li>
								@endif
							</ul>

							<div class="navbar-right" style="margin-right:30px;">
								@if (count($roleids)==0 || array_search(1, $roleids)!== false)
									@if (Auth::user()->terms == 1)
									    <a class="btn navbar-btn btn-default" href="{{{ URL::to('etickets/mtransfer') }}}"><span class="glyphicon glyphicon-plus-sign"></span> Papildināt skolēna e karti</a>
                                    @endif
								@endif
							</div>
						</div><!-- /.container -->
					</nav>
				</div>
			</div>
			@endif

			@include('notifications')

			<div>
				<h3>
					@if (isset($title)) {!! $title !!} @endif
				</h3>
			</div>

			@yield('content')
	</div>
	<!-- ./ container -->

	<!-- the following div is needed to make a sticky footer -->
	<div id="push"></div>
</div>
<!-- ./wrap -->

<div id="footer">
	@if (Request::is('auth/login'))
		<div class="navbar navbar-fixed-bottom" style="background-color:#333;">
			<div class="container" style="width:90%;margin:0 auto;background-color:#333;text-align:center;padding-top:10px;">
				<a style="margin-left:10px;" class="btn btn-info btn-sm" href="http://www.rigassatiksme.lv">Rīgas satiksme</a>
				<a style="margin-left:10px;" class="btn btn-info btn-sm iframe" href="{{{ URL::to('terms') }}}">Noteikumi</a>
				<a style="margin-left:10px;" class="btn btn-info btn-sm iframe" href="{{{ URL::to('contact') }}}">Ieteikumi un atsauksmes</a>
				<a style="margin-left:10px;" class="btn btn-info btn-sm iframe" href="{{{ URL::to('cinfo') }}}">Kontaktinformācija</a>

			</div>
		</div>
	@endif
</div>

<!-- Javascripts
================================================== -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script>

<script src="/assets/compiled/js/all.js"></script>
<script src="{{ asset('assets/js/jquery.cookie.js') }}"></script>

<script type="text/javascript">
	$(function() {

		$(".iframe").colorbox({iframe:true, width:"90%", height:"90%"});

		jQuery('ul.nav li.dropdown').hover(function() {
			jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn();
		}, function() {
			jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut();
		});

		var r = $('.slider').slider({
			value: 0,
			step: 0.01,
			tooltip: 'hide'
		})
				.on('slide', function(ev){
					$('#sumv').val(r.getValue());

					var tmp = parseFloat(r.getValue());

					betalons = Math.round((0-min+tmp) * 100) / 100;
					bkonts = Math.round((max-tmp) * 100) / 100;

					$('#bkonts').val(bkonts);
					$('#betalons').val(betalons);
				})
				.data('slider');

		if (r!=null)
		{
			var min = r.getMin();
			var max = r.getMax();

			$('#bkonts').val(max);
			$('#betalons').val(0-min);
			var bkonts = max;
			var betalons = 0-min;
		}

		// ar roku ierakstita vertiba
		$('#sumv').change( function()
		{
			if ($(this).val()>max)
			{
				$(this).val(max);
				r.setValue(max);

				bkonts = 0;
				betalons = 0-min+max;

			}
			else if ($(this).val()<min)
			{
				$(this).val(min);
				r.setValue(min);

				betalons = 0;
				bkonts = bkonts+(0-min);
			}
			else {
				r.setValue($(this).val());
				var tmp = parseFloat($(this).val());

				betalons = Math.round((0-min+tmp) * 100) / 100;
				bkonts = Math.round((max-tmp) * 100) / 100;
			}

			$('#bkonts').val(bkonts);
			$('#betalons').val(betalons);

			$('#sum').val(tmp);
		});

		$('#datetimepicker1').datetimepicker({
			pickTime: false,
			language: 'lv',
			format: 'YYYY-MM-DD'
		});
		$('#datetimepicker2').datetimepicker({
			pickTime: false,
			language: 'lv',
			format: 'YYYY-MM-DD'
		});
	});
</script>

@yield('scripts')
</body>
</html>
