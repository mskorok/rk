@extends('site.layouts.default')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-11">

                <form class="form-horizontal" method="post" enctype="multipart/form-data" action="" autocomplete="off">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>

                    <div class="form-group">
                        <label for="address">Deklarētā adrese</label>
                        <input class="form-control" type="text" name="address" id="address" autocomplete="off"
                               value="{{{ (Input::old('address')) ? Input::old('address') : $newuser->address }}}">
                        <span style="color:red;">{{{ $errors->first('address') }}}</span>
                    </div>

                    <div class="form-group {{{ $errors->has('pilsoniba') ? 'has-error' : '' }}}">
                        <label for="address">Pilsonība</label>
                        <select name="pilsoniba" class="form-control">
                            <option @if (trim($citi->pilsoniba)=='Latvijas pilsonis') selected
                                    @endif value="Latvijas pilsonis">Latvijas pilsonis
                            </option>
                            <option @if (trim($citi->pilsoniba)=='Nepilsonis') selected @endif value="Nepilsonis">
                                Nepilsonis
                            </option>
                            <option @if (trim($citi->pilsoniba)=='Cits') selected @endif value="Cits">Cits</option>
                        </select>
                        <span style="color:red;">{{{ $errors->first('pilsoniba') }}}</span>
                    </div>

                    <div class="form-group {{{ $errors->has('info') ? 'has-error' : '' }}}">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="info1">Personas dokuments</label>
                                <select name="info1" class="form-control">
                                    <option @if (trim($info[0])=='Pase') selected @endif value="Pase">Pase</option>
                                    <option @if (trim($info[0])=='ID karte') selected @endif value="ID karte">ID karte
                                    </option>
                                    <option @if (trim($info[0])=='Uzturešanās atļauja vai ceļošanas dokuments') selected
                                            @endif value="Uzturešanās atļauja vai ceļošanas dokuments">Uzturešanās
                                        atļauja vai ceļošanas dokuments
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="info">Personas dokumenta numurs</label>
                                <input class="form-control" placeholder="Dokumenta numurs" type="text" name="info"
                                       id="info" autocomplete="off"
                                       value="{{{ (Input::old('info')) ? Input::old('info') : $info[1] }}}">
                            </div>
                            <div class="col-md-4">
                                <label for="info2">Dokuments derīgs līdz</label>
                                <div class='input-group date' id='datepicker1'>
                                    <input class="form-control" placeholder="Derīguma termiņš" type="text" name="info2"
                                           id="info2" autocomplete="off"
                                           value="{{{ (Input::old('info2')) ? Input::old('info2') : $info[2] }}}">
                                    <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                                </div>

                            </div>
                        </div>

                        <span style="color:red;">{{{ $errors->first('info') }}}</span>
                    </div>

                    <div class="form-group">
                        <label for="pazime">Lūdzu, zemāk norādiet, vai atbilstat kādai no šādām pazīmēm</label>
                        <ul class="list-group">
                            <li class="list-group-item small">
                                a. Ārpus Latvijas Republikas ieņemu kādu no šādiem amatiem: valsts vadītājs, parlamenta
                                deputāts, valdības vadītājs, ministrs, ministra vietnieks vai ministra vietnieka
                                vietnieks, valsts sekretārs, augstākās tiesas tiesnesis, konstitucionālās tiesas
                                tiesnesis, augstākās revīzijas (audita) iestādes padomes vai valdes loceklis, centrālās
                                bankas padomes vai valdes loceklis, vēstnieks, pilnvarotais lietvedis, bruņoto spēku
                                augstākais virsnieks, valsts kapitālsabiedrības padomes vai valdes loceklis, vai arī
                                attiecīgo amatu atstāju viena gada laikā;
                            </li>
                            <li class="list-group-item small">
                                b. esmu iepriekšminēto personu vecāks, laulātais vai viņam pielīdzināma persona, bērns,
                                viņa laulātais vai laulātajam pielīdzināma persona. Persona par laulātajam pielīdzināmu
                                uzskatāma tikai tad, ja attiecīgās valsts likumi tai nosaka šādu statusu;
                            </li>
                            <li class="list-group-item small">
                                c. esmu persona, par kuru ir publiski zināms, ka tai ir darījuma attiecības ar kādu no
                                šī dokumenta 1. punktā, vai kurai ar šo personu kopīgi pieder pamatkapitāls
                                komercsabiedrībā, kā arī fiziskā persona, kura ir vienīgā tāda juridiska veidojuma
                                īpašnieks, par kuru ir zināms, ka tas faktiski izveidots šī dokumenta 1. punktā minētās
                                personas labā;
                            </li>
                            <li class="list-group-item {{{ $errors->has('pazime') ? 'has-error' : '' }}}">
                                Nē <input type="radio" value="2"
                                          name="pazime" <?php if (intval($newuser->pazime) == 2) {
                                    echo 'checked="checked"';
                                } ?> />&nbsp;
                                Jā (lūdzu, norādiet valsti un amatu) <input type="radio" value="1"
                                                                            name="pazime" <?php if (intval($newuser->pazime) == 1) {
                                    echo 'checked="checked"';
                                } ?> />
                            </li>
                            <li class="list-group-item {{{ $errors->has('pazime_text') ? 'has-error' : '' }}}">
                                <input class="form-control" type="text" name="pazime_text" id="pazime_text"
                                       value="{{{ $newuser->pazime_text }}}" <?php if (intval($newuser->pazime) != 1) {
                                    echo 'style="display:none;"';
                                } ?> />
                            </li>
                        </ul>
                    </div>

                    <div class="form-group">
                        <label for="pazime">Ziņas par e-kartes kontā veicamajiem darījumiem un klienta līdzekļu
                            izcelsmi</label>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="maks1">Plānotie ienākošie/izejošie maksājumi viena kalendārā mēneša laikā
                                    (maksimāli 140 EUR)</label>
                            </div>
                            <div class="col-md-3 {{{ $errors->has('maks1') ? 'has-error' : '' }}}">
                                <input class="form-control" placeholder="Ienākošie, EUR" type="text" name="maks1"
                                       id="maks1" autocomplete="off"
                                       value="{{{ (Input::old('maks1')) ? Input::old('maks1') : $citi->maks1 }}}">
                            </div>
                            <div class="col-md-3 {{{ $errors->has('maks2') ? 'has-error' : '' }}}">
                                <input class="form-control" placeholder="Izejošie, EUR" type="text" name="maks2"
                                       id="maks2" autocomplete="off"
                                       value="{{{ (Input::old('maks2')) ? Input::old('maks2') : $citi->maks2 }}}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="maks1">Plānotie ienākošie/izejošie maksājumi viena viena kalendārā gada
                                    laikā (maksimāli 2500 EUR)</label>
                            </div>
                            <div class="col-md-3 {{{ $errors->has('maks3') ? 'has-error' : '' }}}">
                                <input class="form-control" placeholder="Ienākošie, EUR" type="text" name="maks3"
                                       id="maks3" autocomplete="off"
                                       value="{{{ (Input::old('maks3')) ? Input::old('maks3') : $citi->maks3 }}}">
                            </div>
                            <div class="col-md-3 {{{ $errors->has('maks4') ? 'has-error' : '' }}}">
                                <input class="form-control" placeholder="Izejošie, EUR" type="text" name="maks4"
                                       id="maks4" autocomplete="off"
                                       value="{{{ (Input::old('maks4')) ? Input::old('maks4') : $citi->maks4 }}}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="maks1">Plānotais viena pirkuma darījuma vidējais un maksimāli pieļaujamais
                                    apjoms) (EUR)</label>
                            </div>
                            <div class="col-md-3 {{{ $errors->has('maks5') ? 'has-error' : '' }}}">
                                <input class="form-control" placeholder="Vidējais apjoms, EUR" type="text" name="maks5"
                                       id="maks5" autocomplete="off"
                                       value="{{{ (Input::old('maks5')) ? Input::old('maks5') : $citi->maks5 }}}">
                            </div>
                            <div class="col-md-3 {{{ $errors->has('maks6') ? 'has-error' : '' }}}">
                                <input class="form-control" placeholder="Maksimālais apjoms, EUR" type="text"
                                       name="maks6" id="maks6" autocomplete="off"
                                       value="{{{ (Input::old('maks6')) ? Input::old('maks6') : $citi->maks6 }}}">
                            </div>
                        </div>

                        <span style="color:red;">{{{ $errors->first('maks') }}}</span>
                    </div>

                    <div class="form-group {{{ $errors->has('izcelsme') ? 'has-error' : '' }}}">
                        <label for="izcelsme1">Uz kartes saņemto līdzekļu izcelsme</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="izcelsme1" id="izcelsme1" class="form-control">
                                    <option value="Darba alga" <?php if (((Input::old('izcelsme1')) ? Input::old('izcelsme1') : trim($citi->izcelsme1)) == "Darba alga") {
                                        echo 'selected="selected"';
                                    } ?>>Darba alga
                                    </option>
                                    <option value="Pensija" <?php if (((Input::old('izcelsme1')) ? Input::old('izcelsme1') : trim($citi->izcelsme1)) == "Pensija") {
                                        echo 'selected="selected"';
                                    } ?>>Pensija
                                    </option>
                                    <option value="Stipendija" <?php if (((Input::old('izcelsme1')) ? Input::old('izcelsme1') : trim($citi->izcelsme1)) == "Stipendija") {
                                        echo 'selected="selected"';
                                    } ?>>Stipendija
                                    </option>
                                    <option value="Pabalsts" <?php if (((Input::old('izcelsme1')) ? Input::old('izcelsme1') : trim($citi->izcelsme1)) == "Pabalsts") {
                                        echo 'selected="selected"';
                                    } ?>>Pabalsts
                                    </option>
                                    <option value="Cita uz kartes saņemto līdzekļu izcelsme" <?php if (((Input::old('izcelsme1')) ? Input::old('izcelsme1') : trim($citi->izcelsme1)) == "Cita uz kartes saņemto līdzekļu izcelsme") {
                                        echo 'selected="selected"';
                                    } ?>>Cita uz kartes saņemto līdzekļu izcelsme
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" placeholder="norādiet" type="text" name="izcelsme2"
                                       id="izcelsme2" autocomplete="off"
                                       value="{{{ (Input::old('izcelsme2')) ? Input::old('izcelsme2') : $citi->izcelsme2 }}}" <?php if ($citi->izcelsme1 != "Nē" && Input::old('izcelsme1') != "Cita uz kartes saņemto līdzekļu izcelsme") {
                                    echo 'style="display:none;"';
                                } ?>>
                            </div>
                        </div>

                        <span style="color:red;">{{{ $errors->first('izcelsme') }}}</span>
                    </div>

                    <div class="form-group {{{ $errors->has('darijumi') ? 'has-error' : '' }}}">
                        <label for="darijumi1">Visi darījumi ar E-kartes kontā esošajiem līdzekļiem tiks veikti Konta
                            pārvaldnieka un E-kartes lietotāja interesēs</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="darijumi1" id="darijumi1" class="form-control">
                                    <option value="Jā" <?php if (((Input::old('darijumi1')) ? Input::old('darijumi1') : $citi->darijumi1) == "Jā") {
                                        echo 'selected="selected"';
                                    } ?>>Jā
                                    </option>
                                    <option value="Nē" <?php if (((Input::old('darijumi1')) ? Input::old('darijumi1') : $citi->darijumi1) == "Nē") {
                                        echo 'selected="selected"';
                                    } ?>>Nē
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" placeholder="paskaidrojiet" type="text" name="darijumi2"
                                       id="darijumi2" autocomplete="off"
                                       value="{{{ (Input::old('darijumi2')) ? Input::old('darijumi2') : $citi->darijumi2 }}}" <?php if ($citi->darijumi1 != "Nē" && Input::old('darijumi1') != "Nē") {
                                    echo 'style="display:none;"';
                                } ?>>
                            </div>
                        </div>

                        <span style="color:red;">{{{ $errors->first('darijumi') }}}</span>
                    </div>

                    <div class="form-group {{{ $errors->has('ipasnieks') ? 'has-error' : '' }}}">
                        <label for="darijumi1">Esmu E-kartes kontā esošo līdzekļu patiesais īpašnieks</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select name="ipasnieks1" id="ipasnieks1" class="form-control">
                                    <option value="Jā" <?php if (((Input::old('ipasnieks1')) ? Input::old('ipasnieks1') : $citi->ipasnieks1) == "Jā") {
                                        echo 'selected="selected"';
                                    } ?>>Jā
                                    </option>
                                    <option value="Nē" <?php if (((Input::old('ipasnieks1')) ? Input::old('ipasnieks1') : $citi->ipasnieks1) == "Nē") {
                                        echo 'selected="selected"';
                                    } ?>>Nē
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" placeholder="paskaidrojiet" type="text" name="ipasnieks2"
                                       id="ipasnieks2" autocomplete="off"
                                       value="{{{ (Input::old('ipasnieks2')) ? Input::old('ipasnieks2') : $citi->ipasnieks2 }}}" <?php if ($citi->ipasnieks1 != "Nē" && Input::old('ipasnieks1') != "Nē") {
                                    echo 'style="display:none;"';
                                } ?>>
                            </div>
                        </div>

                        <span style="color:red;">{{{ $errors->first('ipasnieks') }}}</span>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <a href="/" class="btn-cancel">Atcelt</a>
                            <button type="submit" class="btn btn-success">Apstiprināt</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('input[type=radio][name=pazime]').change(function () {
                if (this.value == '1') {
                    $('#pazime_text').show();
                }
                else $('#pazime_text').hide();
            });

            $('#ipasnieks1').change(function () {
                if (this.value == 'Nē') {
                    $('#ipasnieks2').show();
                }
                else $('#ipasnieks2').hide();
            });

            $('#darijumi1').change(function () {
                if (this.value == 'Nē') {
                    $('#darijumi2').show();
                }
                else $('#darijumi2').hide();
            });

            $('#izcelsme1').change(function () {
                if (this.value == 'Cita uz kartes saņemto līdzekļu izcelsme') {
                    $('#izcelsme2').show();
                }
                else $('#izcelsme2').hide();
            });

            $('#datepicker1').datetimepicker({
                pickTime: false,
                language: 'lv',
                format: 'YYYY-MM-DD'
            });
        });
    </script>
@stop