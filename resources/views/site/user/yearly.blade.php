@extends('site.layouts.default')

@section('title')
Ikgadējā informācijas atjaunošana ::
@parent
@stop

@section('content')
<div class="row">
<div class="col-md-8 col-md-offset-2">
	<div class="page-header">
		<h3>{!! $yearly_text !!}</h3>
	</div>

	<div class="form-group" id="pogas">
	    <a class="btn btn-large btn-primary" href="#" id="yes">JĀ</a>
	    <a  class="btn btn-large btn-info" href="/user/yearly/no">NĒ</a>
	</div>

	<div class="row" id="forma" style="display:none">
	<div class="col-md-12">
        <form method="POST" action="{{{ URL::action('UserController@postYearly')  }}}" accept-charset="UTF-8">
            <input type="hidden" name="_token" id="csrf_token" value="{{{ csrf_token() }}}" />

                <div class="form-group {{{ $errors->has('izcelsme') ? 'has-error' : '' }}}">
                    <label for="izcelsme1">Uz kartes saņemto līdzekļu izcelsme</label>
                    <div class="row">
                        <div class="col-md-4">
                            <select name="izcelsme1" id="izcelsme1" class="form-control">
                                <option value="Algots darbinieks" <?php if(Input::old('izcelsme1')== "Algots darbinieks") { echo 'selected="selected"'; } ?>>Algots darbinieks</option>
                                <option value="Skolēns" <?php if(Input::old('izcelsme1')== "Skolēns") { echo 'selected="selected"'; } ?>>Skolēns</option>
                                <option value="Pensionārs" <?php if(Input::old('izcelsme1')== "Pensionārs") { echo 'selected="selected"'; } ?>>Pensionārs</option>
                                <option value="Bezdarbnieks" <?php if(Input::old('izcelsme1')== "Bezdarbnieks") { echo 'selected="selected"'; } ?>>Bezdarbnieks</option>
                                <option value="Cita uz kartes saņemto līdzeklu izcelsme" <?php if(Input::old('izcelsme1')== "Cita uz kartes saņemto līdzeklu izcelsme") { echo 'selected="selected"'; } ?>>Cita uz kartes saņemto līdzeklu izcelsme</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" placeholder="norādiet" type="text" name="izcelsme2" id="izcelsme2" autocomplete="off" value="{{{ Input::old('izcelsme2') }}}" <?php if(Input::old('izcelsme1')!= "Cita uz kartes saņemto līdzeklu izcelsme") { echo 'style="display:none;"'; } ?>>
                        </div>
                    </div>

                    <span style="color:red;">{{{ $errors->first('izcelsme') }}}</span>
                </div>

                <div class="form-group {{{ $errors->has('darijumi') ? 'has-error' : '' }}}">
                    <label for="darijumi1">Visi darījumi ar E-kartes kontā esošajiem līdzekļiem tiks veikti Konta pārvaldnieka un E-kartes lietotāja interesēs</label>
                    <div class="row">
                        <div class="col-md-4">
                            <select name="darijumi1" id="darijumi1" class="form-control">
                                <option value="Jā" <?php if(Input::old('darijumi1')== "Jā") { echo 'selected="selected"'; } ?>>Jā</option>
                                <option value="Nē" <?php if(Input::old('darijumi1')== "Nē") { echo 'selected="selected"'; } ?>>Nē</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" placeholder="paskaidrojiet" type="text" name="darijumi2" id="darijumi2" autocomplete="off" value="{{{ Input::old('darijumi2') }}}" <?php if(Input::old('darijumi1')!= "Nē") { echo 'style="display:none;"'; } ?>>
                        </div>
                    </div>

                    <span style="color:red;">{{{ $errors->first('darijumi') }}}</span>
                </div>

                <div class="form-group {{{ $errors->has('ipasnieks') ? 'has-error' : '' }}}">
                    <label for="darijumi1">Esmu E-kartes kontā esošo līdzekļu patiesais īpašnieks</label>
                    <div class="row">
                        <div class="col-md-4">
                            <select name="ipasnieks1" id="ipasnieks1" class="form-control">
                                <option value="Jā" <?php if(Input::old('ipasnieks1')== "Jā") { echo 'selected="selected"'; } ?>>Jā</option>
                                <option value="Nē" <?php if(Input::old('ipasnieks1')== "Nē") { echo 'selected="selected"'; } ?>>Nē</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" placeholder="paskaidrojiet" type="text" name="ipasnieks2" id="ipasnieks2" autocomplete="off" value="{{{ Input::old('ipasnieks2') }}}" <?php if(Input::old('ipasnieks1')!= "Nē") { echo 'style="display:none;"'; } ?>>
                        </div>
                    </div>

                    <span style="color:red;">{{{ $errors->first('ipasnieks') }}}</span>
                </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-primary">{{{ Lang::get('confide/confide.signup.submit') }}}</button>
            </div>
        </form>
    </div>
    </div>

	<br />
	<br />

</div>
</div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function() {

            $('#yes').click(function() {
                $('#pogas').hide();
                $('#forma').show();
            });

            $('#ipasnieks1').change(function() {
                if (this.value == 'Nē')
                {
                    $('#ipasnieks2').show();
                }
                else $('#ipasnieks2').hide();
            });

            $('#darijumi1').change(function() {
                if (this.value == 'Nē')
                {
                    $('#darijumi2').show();
                }
                else $('#darijumi2').hide();
            });

            $('#izcelsme1').change(function() {
                if (this.value == 'Cita uz kartes saņemto līdzeklu izcelsme')
                {
                    $('#izcelsme2').show();
                }
                else $('#izcelsme2').hide();
            });

        });
    </script>
@stop
