@extends('site.layouts.default')

@section('title')
{{{ Lang::get('user/user.settings') }}} ::
@parent
@stop

@section('content')
<div class="page-header">
	<h3>Labot profilu</h3>
</div>

<div class="panel panel-primary">
<form method="post" action="@if (isset($user)){!! URL::to('auth/user') !!}@endif" autocomplete="off" class="form-horizontal panel-body" role="form">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" id="csrf_token" value="{{{ csrf_token() }}}" />
	<!-- ./ csrf token -->

	<div class="form-group">
		<label for="acc" class="col-sm-2 control-label">Bankas konts</label>
		<div class="col-sm-4"><input type="text" name="acc" class="form-control" disabled="disabled" value="{{{ $user->konts }}}" /></div>
	</div>

	<div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
		<label for="username" class="col-sm-2 control-label">Vārds</label>
		<div class="col-sm-4"><input type="text" name="username" id="username" class="form-control" value="{{{ Input::old('username', $user->username) }}}" /></div>
		{{{ $errors->first('username') }}}
	</div>

	<div class="form-group {{{ $errors->has('surname') ? 'has-error' : '' }}}">
		<label class="col-sm-2 control-label" for="surname">Uzvārds</label>
		<div class="col-sm-4">
			<input type="text" name="surname" id="surname" class="form-control" value="{{{ Input::old('surname', $user->surname) }}}" />
			{{{ $errors->first('surname') }}}
		</div>
	</div>

	<div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
		<label class="col-sm-2 control-label" for="email">E-pasts</label>
		<div class="col-sm-4">
			<input type="text" name="email" id="email" class="form-control" value="{{{ Input::old('email', $user->email) }}}" />
			{{{ $errors->first('email') }}}
		</div>
	</div>

	<div class="form-group {{{ $errors->has('phone') ? 'has-error' : '' }}}">
		<label class="col-sm-2 control-label" for="phone">Telefons</label>
		<div class="col-sm-4">
			<input type="text" name="phone" id="phone" class="form-control" autocomplete="off" value="{{{ Input::old('phone', $user->phone) }}}" />
			{{{ $errors->first('phone') }}}
		</div>
	</div>

	<div class="form-group {{{ $errors->has('cpassword') ? 'has-error' : '' }}}">
		<label class="col-sm-2 control-label" for="cpassword">Pašreizējā parole</label>
		<div class="col-sm-4">
			<input type="password" name="cpassword" class="form-control" id="cpassword" value="" />
			{{{ $errors->first('cpassword') }}}
		</div>
	</div>

	<!-- Password -->
	<div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
		<label class="col-sm-2 control-label" for="password">Parole (vismaz 8 simboli, 1 burts un 1 cipars)</label>
		<div class="col-sm-4">
			<input type="password" name="password" class="form-control" id="password" value="" />
			{{{ $errors->first('password') }}}
		</div>
	</div>
	<!-- ./ password -->
	
	<div class="form-group {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">
		<label class="col-sm-2 control-label" for="password_confirmation">Atkārtota parole</label>
		<div class="col-sm-4">
			<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" value="" />
			{{{ $errors->first('password_confirmation') }}}
		</div>
	</div>

	<!-- Update button -->
	<div class="form-group">
		<div class="col-sm-4">
			<button type="submit" class="btn btn-primary">Labot</button>
		</div>
	</div>
	<!-- ./ update button -->
</form>
</div>
@stop
