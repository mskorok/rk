@extends('site.layouts.default')

@section('title')
Nepieciešams pabeigt skolas.rigaskarte.lv reģistrāciju ::
@parent
@stop

@section('content')
<div class="row">
<div class="col-md-6 col-md-offset-3">
	<div class="page-header">
		<h1>Nepieciešams pabeigt skolas.rigaskarte.lv reģistrāciju</h1>
	</div>

	<div class="form-group">
	    Lai veiktu autorizāciju ar internet-banku - <a href="https://pieteikums.rigassatiksme.lv/Authentication/RKSubmit">spiediet šeit</a>
	</div>

	<br />
	<br />

</div>
</div>
@stop
