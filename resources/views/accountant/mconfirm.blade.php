@extends('site.layouts.default')

@section('content')

    <form class="form-horizontal" method="post" action="" autocomplete="off">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{!! $data->id !!}" />

        <h4>Apstiprināt ienākošo maksājumu</h4>

        <div class="form-group">
            <label class="control-label">Maksājuma nr.</label>
            <pre>{!! $data->id !!}</pre>
        </div>

        <div class="form-group">
            <label class="control-label">Maksājuma datums</label>
            <pre>{!! $data->created_date() !!}</pre>
        </div>

        <div class="form-group">
            <label class="control-label">Lietotājs</label>
            <pre>{!! $data->user->fullname() !!}</pre>
        </div>

        <div class="form-group">
            <label class="control-label">E-talons</label>
            <pre>{!! $eticket_nr !!}</pre>
        </div>

        <div class="form-group">
            <label class="control-label">Maksājuma identifikācijas kods</label>
            <pre>{!! $data->payment_code !!}</pre>
        </div>

        @if ($data->user->rk_confirmed_user_id > 0)
            <div class="form-group">
                <label class="control-label">Konta pārvaldnieka bankas konts</label>
                <pre>{!! $data->user->konts !!}</pre>
            </div>
        @else
            <div class="form-group {{{ $errors->has('konts') ? 'error' : '' }}}">
                <label class="control-label" for="konts">Konta pārvaldnieka bankas konts</label>
                <input class="form-control" type="text" name="konts" id="konts" maxlength="34" value="{{{ Input::old('konts', $data->user->konts) }}}" />
                {!! $errors->first('konts', '<span class="help-inline">:message</span>') !!}
            </div>
        @endif

        <div class="form-group {{{ $errors->has('summa') ? 'error' : '' }}}">
            <label class="control-label" for="summa">Papildināt e-talona bilanci par EUR</label>
            <input class="form-control" type="text" name="summa" id="summa" maxlength="8" value="{{{ Input::old('summa', round($data->user_amount/100,2)) }}}" />
            {!! $errors->first('summa', '<span class="help-inline">:message</span>') !!}
        </div>

        <div class="control-group">
            <div class="controls">
                <a class="btn-cancel" href="/accountant/mtransfer">Atcelt</a>
                <button type="submit" class="btn btn-confirm">{!! Lang::get('button.confirm') !!}</button>
            </div>
        </div>
    </form>
@stop