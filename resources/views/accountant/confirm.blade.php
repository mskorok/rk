@extends('site.layouts.default')

@section('content')

    <form class="form-horizontal" method="post" action="" autocomplete="off">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{!! $data->id !!}" />

        <h4>Apstiprināt {!! round($data->amount/100,2) !!} EUR maksājumu tirgotājam {!! $seller->seller_name !!} ?</h4>
        <br />

        <div class="control-group">
            <div class="controls">
                <a class="btn-cancel" href="/accountant/index">Atcelt</a>
                <button type="submit" class="btn btn-confirm">{!! Lang::get('button.confirm') !!}</button>
            </div>
        </div>
    </form>
@stop