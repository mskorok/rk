<h1>Maksājuma uzdevums SKOLAS.RIGASKARTE.LV</h1>
<table cellspacing="5" cellpadding="2">
    <tr>
        <td>Dokumenta nr:</td>
        <td style="border:1px solid #444;">{!! $payment_id !!}</td>
    </tr>
    <tr>
        <td style="border-bottom:1px solid #333;">Maksātājs</td>
        <td></td>
    </tr>
    <tr>
        <td>Nosaukums:</td>
        <td style="border:1px solid #444;">{!! $settings->name !!}</td>
    </tr>
    <tr>
        <td>Banka:</td>
        <td style="border:1px solid #444;">{!! $settings->banka !!} {!! $settings->bankas_kods !!}</td>
    </tr>
    <tr>
        <td>Konts:</td>
        <td style="border:1px solid #444;">{!! $settings->konts !!}</td>
    </tr>
    <tr>
        <td>Reģ nr.:</td>
        <td style="border:1px solid #444;">{!! $settings->reg_nr !!}</td>
    </tr>
    <tr>
        <td>Adrese:</td>
        <td style="border:1px solid #444;">{!! $settings->jur_adrese !!}</td>
    </tr>
    <tr>
        <td>Tālrunis:</td>
        <td style="border:1px solid #444;">{!! $settings->talrunis !!}</td>
    </tr>
    <tr>
        <td style="border-bottom:1px solid #333;">Saņēmējs</td>
        <td></td>
    </tr>
    <tr>
        <td>Nosaukums:</td>
        <td style="border:1px solid #444;">{!! $seller->seller_name !!}</td>
    </tr>
    <tr>
        <td>Banka:</td>
        <td style="border:1px solid #444;">{!! $seller->banka !!} {!! $seller->bankas_kods !!}</td>
    </tr>
    <tr>
        <td>Konts:</td>
        <td style="border:1px solid #444;">{!! $seller->konts !!}</td>
    </tr>
    <tr>
        <td>Reģ nr.:</td>
        <td style="border:1px solid #444;">{!! $seller->reg_nr !!}</td>
    </tr>
    <tr>
        <td>Adrese:</td>
        <td style="border:1px solid #444;">{!! $seller->jur_adrese !!}</td>
    </tr>
    <tr>
        <td>Tālrunis:</td>
        <td style="border:1px solid #444;">{!! $seller->talrunis !!}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Informācija saņēmējam:</td>
        <td style="border:1px solid #444;">Maksājums no SKOLAS.RIGASKARTE.LV sistēmas par periodu {!! substr($datums_no,0,10) !!} lidz {!! substr($datums_lidz,0,10) !!}.</td>
    </tr>
    <tr>
        <td>Izveidošanas datums:</td>
        <td style="border:1px solid #444;">{!! $data->created_date() !!}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Summa:</td>
        <td style="border:1px solid #444;">{!! sprintf("%.2f",round(($data->amount)/100,2)) !!} EUR</td>
    </tr>
    <tr>
        <td style="font-weight:bold;">Summa apmaksai:</td>
        <td style="border:1px solid #444;font-weight:bold;">{!! $data->humanAmount() !!}</td>
    </tr>
</table>