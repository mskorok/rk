@extends('site.layouts.default')

@section('content')
	<div class="page-header">
		<h3>
			Aktīvie maksājumi
			<div class="pull-right">
				<a href="{{{ URL::to('accountant/history') }}}" class="btn btn-sm btn-info"> Maksājumu vēsture</a>
			</div>
		</h3>
	</div>

	<div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="span2" style="background-color:#333;color:white;">{!! Lang::get('admin/etickets/table.created_at') !!}</th>
					<th class="span3" style="background-color:#333;color:white;">Tirgotājs</th>
					<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
					<th class="span2" style="background-color:#333;color:white;">Statuss</th>
					<th class="span2" style="background-color:#333;color:white;">Fails</th>
					<th class="span2" style="background-color:#333;color:white;">Darbības</th>
				</tr>
			</thead>
			<tbody>
				@if (count($data)>0)
					@foreach ($data as $single)
							<tr>		
								<td>{!! $single->created_at !!}</td>
								<td>{!! $single->seller_name !!}</td>
								<td>{!! round($single->amount/100,2) !!} EUR</td>
								<td>@if ($single->confirmed==0) Gaida apstiprināšanu @endif</td>
								<td><a class="btn btn-sm btn-primary" target="_blank" href="{!! URL::to('accountant/pdf/'.$single->id) !!}">PDF</a></td>
								<td>@if ($single->confirmed==0) 
									<a class="btn btn-sm btn-primary" href="/accountant/confirm/{!! $single->id !!}">Apstiprināt</a>
									@endif</td>
							</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
@stop