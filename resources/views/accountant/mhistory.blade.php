@extends('site.layouts.default')

@section('content')
	<div class="page-header">
		<h3>
			Ienākošo pārskaitījumu vēsture
		</h3>
	</div>

	<div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
					<th class="span3" style="background-color:#333;color:white;">Lietotājs</th>
					<th class="span3" style="background-color:#333;color:white;">E-talons</th>
					<th class="span2" style="background-color:#333;color:white;">Lietotāja summa EUR</th>
					<th class="span2" style="background-color:#333;color:white;">Apstiprinātā summa EUR</th>
					<th class="span2" style="background-color:#333;color:white;">Apstiprināšanas datums</th>
					<th class="span2" style="background-color:#333;color:white;">Apstiprinātājs</th>
				</tr>
			</thead>
			<tbody>
				@if (count($data)>0)
					@foreach ($data as $single)
							<tr>		
								<td>{!! $single->created_date() !!}</td>
								<td>
									<?php
									$user = App\Models\User::where('id',$single->user_id)->first();
									$username = (isset($user->id)) ? $user->fullname() : 'Dzēsts vai neeksistējošs lietotājs';
									?>
									{!! $username !!}
								</td>
                                <td>
                                    <?php
                                    $et = App\Models\Eticket::where('id',$single->eticket_id)->first();
                                    $username = (isset($et->nr)) ? $et->nr : 'Dzēsts vai neeksistējošs e-talons';
                                    ?>
                                    {!! $username !!}
                                </td>
								<td>{!! $single->humanUserAmount() !!}</td>
								<td>{!! $single->humanAmount() !!}</td>
								<td>{!! $single->confirmed_date !!}</td>
                                <td>
                                    <?php
                                    $user = App\Models\User::where('id',$single->accountant_id)->first();
                                    $username = (isset($user->id)) ? $user->fullname() : 'Dzēsts vai neeksistējošs lietotājs';
                                    ?>
                                    {!! $username !!}
                                </td>
							</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
@stop