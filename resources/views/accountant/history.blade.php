@extends('site.layouts.default')


@section('content')
	<div class="page-header">
		<h3>
			Maksājumu vēsture
		</h3>
	</div>

	<div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="span2" style="background-color:#333;color:white;">{!! Lang::get('admin/etickets/table.created_at') !!}</th>
					<th class="span3" style="background-color:#333;color:white;">Tirgotājs</th>
					<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
					<th class="span2" style="background-color:#333;color:white;">Statuss</th>
					<th class="span2" style="background-color:#333;color:white;">Fails</th>
				</tr>
			</thead>
			<tbody>
				@if (count($data)>0)
					@foreach ($data as $single)
							<tr>		
								<td>{!! $single->created_at !!}</td>
								<td>{!! $single->seller_name !!}</td>
								<td>{!! round($single->amount/100,2) !!} EUR</td>
								<td>@if ($single->confirmed==1) Izpildīts @endif</td>
								<td><a class="btn btn-sm btn-primary" target="_blank" href="{!! URL::to('accountant/pdf/'.$single->id) !!}">PDF</a></td>
							</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
@stop