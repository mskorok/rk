@extends('site.layouts.default')

@section('content')

	<div class="panel panel-primary">
		<form method="post" enctype="multipart/form-data" action="" autocomplete="off" class="panel-body" role="form">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			<div class="form-group {{{ $errors->has('d') ? 'error' : '' }}}">
				<label class="control-label" for="amount">Fidavista XML fails no bankas</label>
				{!! Form::file('file') !!}
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-success btn-sm">{!! Lang::get('button.confirm') !!}</button>
			</div>
		</form>
	</div>
@stop