@extends('site.layouts.default')

@section('content')
	<div class="page-header">
		<h3>
			Ienākošie pārskaitījumi, kas gaida uz apstiprināšanu

			<div class="pull-right">
				<a href="{{{ URL::to('accountant/mhistory') }}}" class="btn btn-sm btn-info"> Ienākušo pārskaitījumu vēsture</a>
			</div>
		</h3>
	</div>

	<div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="span2" style="background-color:#333;color:white;">{{{ Lang::get('admin/etickets/table.created_at') }}}</th>
					<th class="span3" style="background-color:#333;color:white;">Lietotājs</th>
					<th class="span3" style="background-color:#333;color:white;">E-talons</th>
					<th class="span2" style="background-color:#333;color:white;">Summa EUR</th>
					<th class="span2" style="background-color:#333;color:white;">Kods</th>
					<th class="span2" style="background-color:#333;color:white;">Statuss</th>
					<th class="span1" style="background-color:#333;color:white;">Jauns</th>
					<th class="span2" style="background-color:#333;color:white;">Darbības</th>
				</tr>
			</thead>
			<tbody>
				@if (count($data)>0)
					@foreach ($data as $single)
							<tr @if($single->first_payment() == 'JĀ') style="background-color:#3498db;" @endif>
								<td>{!! $single->created_at !!}</td>
								<td>
                                    <?php
                                        $user = App\Models\User::where('id',$single->user_id)->first();
                                        $username = (isset($user->id)) ? $user->fullname() : 'Dzēsts vai neeksistējošs lietotājs';
                                    ?>
                                    {!! $username !!}
								</td>
								<td>
									<?php
									$et = App\Models\Eticket::where('id',$single->eticket_id)->first();
									$username = (isset($et->nr)) ? $et->nr : 'Dzēsts vai neeksistējošs e-talons';
									?>
									{!! $username !!}
								</td>
								<td>{!! $single->humanUserAmount() !!}</td>
								<td>{!! $single->payment_code !!}</td>
								<td>@if ($single->confirmed==0) Gaida apstiprināšanu @endif</td>
								<td>{!! $single->first_payment() !!}</td>
								<td>@if ($single->confirmed==0 && $username != 'Dzēsts vai neeksistējošs lietotājs')
									<a class="btn btn-sm btn-primary" href="/accountant/mconfirm/{!! $single->id !!}">Apstiprināt</a>
									@endif</td>
							</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
@stop