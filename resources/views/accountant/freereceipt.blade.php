<h1>Brīvpusdienu kopsavilkums SKOLAS.RIGASKARTE.LV</h1>
<table cellspacing="5" cellpadding="2">
    <tr>
        <td>Dokumenta nr:</td>
        <td style="border:1px solid #444;">{!! $payment_id !!}</td>
    </tr>
    <tr>
        <td style="border-bottom:1px solid #333;">Saņēmējs</td>
        <td></td>
    </tr>
    <tr>
        <td>Nosaukums:</td>
        <td style="border:1px solid #444;">{!! $seller->seller_name !!}</td>
    </tr>
    <tr>
        <td>Banka:</td>
        <td style="border:1px solid #444;">{!! $seller->banka !!} {!! $seller->bankas_kods !!}</td>
    </tr>
    <tr>
        <td>Konts:</td>
        <td style="border:1px solid #444;">{!! $seller->konts !!}</td>
    </tr>
    <tr>
        <td>Reģ nr.:</td>
        <td style="border:1px solid #444;">{!! $seller->reg_nr !!}</td>
    </tr>
    <tr>
        <td>Adrese:</td>
        <td style="border:1px solid #444;">{!! $seller->jur_adrese !!}</td>
    </tr>
    <tr>
        <td>Tālrunis:</td>
        <td style="border:1px solid #444;">{!! $seller->talrunis !!}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Informācija saņēmējam:</td>
        <td style="border:1px solid #444;">Informācija par veiktajiem brīvpusdienu darījumiem no SKOLAS.RIGASKARTE.LV sistēmas par {!! substr($periods,0,4) !!}. gada {!! intval(substr($periods,5,2)) !!} mēnesi.</td>
    </tr>
    <tr>
        <td>Izveidošanas datums:</td>
        <td style="border:1px solid #444;">{!! $data->created_date() !!}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Darījumu kopskaits:</td>
        <td style="border:1px solid #444;">{!! $count  !!}</td>
    </tr>
</table>