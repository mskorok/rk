<?php

return array (
  'already_exists' => 'Lietotājs jau eksistē!',
  'does_not_exist' => 'Lietotājs neeksistē.',
  'login_required' => 'The login field is required',
  'password_required' => 'Jāaizpilda paroles lauks.',
  'password_does_not_match' => 'Paroles neskrīt.',
  'create' => 
  array (
    'error' => 'Neizdevās izveidot lietotāju.',
    'success' => 'Lietotājs veiksmīgi izveidots.',
  ),
  'edit' => 
  array (
    'impossible' => 'Nevar labot šo lietotāju.',
    'error' => 'Neizdevās viekt labojumus šim lietotājam.',
    'success' => 'Leitotāja dati laboti veiksmīgi.',
  ),
  'delete' => 
  array (
    'impossible' => 'Nevar izdzēst šo lietotāju.',
    'error' => 'Neizdevās dzēst šo lietotāju.',
    'success' => 'Lietotājs veiksmīgi dzēsts.',
  ),
);
