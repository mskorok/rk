<?php

return array (
  'first_name' => 'Vārds',
  'surname' => 'Uzvārds',
  'user_id' => 'User Id',
  'username' => 'Vārds',
  'email' => 'E-pasts',
  'phone' => 'Telefons',
  'groups' => 'Grupas',
  'roles' => 'Tiesības',
  'activated' => 'Aktīvs lietotājs',
  'created_at' => 'Izveidots',
  'last_login' => 'Pēdējoreiz',
);
