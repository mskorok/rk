<?php

return array (
  'does_not_exist' => 'Šāds ieraksts nav atrasts!',
  'create' => 
  array (
    'success' => 'Ieraksts izveidots veiksmīgi.',
  ),
  'edit' => 
  array (
    'success' => 'Ieraksts veiksmīgi labots.',
  ),
  'delete' => 
  array (
    'error' => 'Neizdevās izdzēst ierakstu!',
    'success' => 'Ieraksts dzēsts.',
  ),
);
