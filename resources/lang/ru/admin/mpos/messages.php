<?php

return array (
  'does_not_exist' => 'Šāds ieraksts nav atrasts!',
  'create' => 
  array (
    'success' => 'Ieraksts izveidots veiksmīgi.',
  ),
  'edit' => 
  array (
    'success' => 'Ieraksts veiksmīgi labots.',
  ),
  'delete' => 
  array (
    'error' => 'Neizdevās izdzēst eirakstu!',
    'success' => 'Ieraksts dzēsts.',
  ),
  'restart' => 
  array (
    'error' => 'Neizdevās pārstartēt MPOS iekārtu!',
    'success' => 'Pārstartēšanas komanda veiksksmīgi nosūtīta.',
  ),
  'conf' => 
  array (
    'error' => 'Neizdevās saglabāt MPOS iekārtas konfigurāciju!',
    'success' => 'Konfigurācija veiksmīgi saglabāta.',
  ),
  'fw' => 
  array (
    'error' => 'Neizdevās saglabāt MPOS firmware!',
    'success' => 'Firmware veiksmīgi saglabāta.',
  ),
);
