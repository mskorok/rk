<?php

return array (
  'subject' => 'New User has been created',
  'text' => 'Created new user with email :email, personal code :pk, id :id',
);
