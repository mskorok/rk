<?php

return array (
  'subject' => 'Вопрос от пользователя сайта :name :surname',
  'text' => 'Дата и время отправки сообщения на портале :date <br>Данные отправителя: :name :surname <br>Номер телефона :phone <br>Электронный адрес :email <br>Текст сообщения: :text',
);
