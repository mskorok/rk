<?php

return array (
  'title' => 'ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ',
  'use' => 'ИСПОЛЬЗОВАНИЕ',
  'register' => 'РЕГИСТРАЦИЯ',
  'security' => 'БЕЗОПАСНОСТЬ',
  'subtitle1' => 'Что делать, если у моего ребёнка нет э-карты школьника или она уже недействительна?',
  'text1' => 'Более подробная информация о том как оформить или новую  э-карту школьника доступна здесь.',
  'subtitle2' => 'Как оформить э-карту школьника?',
  'subtitle3' => 'Как зарегистрироваться на сайте?',
  'subtitle_1_1' => 'Kas  jādara, ja manam bērnam nav Skolēna e kartes, vai tās derīguma termiņš ir beidzies?',
  'text_1_1' => 'Plašāka informācija par skolēna kartes noformēšanu vai atkārtotu izsniegšanu ir pieejama <a href="https://www.rigassatiksme.lv/lv/biletes/e-talonu-veidi/skolena-e-karte/ ">šeit</a>.',
  'subtitle_1_2' => 'Kā noformēt Skolēna e karti?',
  'text_1_2' => 'Plašāna informācija par skolēna kartes noformēšanu vai atkārtotu izsniegšanu ir pieejama <a href="https://www.rigassatiksme.lv/lv/biletes/e-talonu-veidi/skolena-e-karte/">šeit</a>.',
  'subtitle_1_3' => 'Kā reģistrēties portālā?',
  'text_1_3' => 'Jums būs nepieciešama Jūsu pases vai ID skenēta kopija vai fotogrāfija, kā arī piekļuve internetbankai. Reģistrācijas uzsākšana spiediet <a href="https://skolas.rigaskarte.lv/portal/landing "> šeit</a>.',
  'subtitle_2_1' => 'Kur es varu izmantot Skolēna e-karti?  ',
  'text_2_1' => 'Skolēna e-karte ir līdzeklis  brīvpusdienu saņemšanai un pirkšanai skolas ēdnīcā. Vecākiem ir iespēja pārskaitīt savam bērnam naudu, sākot ar 1 EUR, un būt drošiem, ka bērns neizmantos šo naudu neveselīgas pārtikas iegādei un varēs tērēt naudu tikai skolas ēdnīcā .',
  'subtitle_2_2' => 'Kā ir jālieto skolēna e-karte ēdnīcā? ',
  'text_2_2' => 'Pirms norēķināšanas vienkārši pieliec to pie viena no ēdnīcas termināliem ar norādi reģistrē brīvpusdienas šeit vai norēķinies ar e-naudu šeit. ',
  'subtitle_3_1' => 'Vai ar nozaudēto karti var norēķināties cita persona? ',
  'text_3_1' => 'Jūsu bērna e-karte ir personalizēta, līdz ar to citam cilvēkam būs grūti no izmantot. Ar mūsu portāla palīdzību Jūs varat uzreiz nobloķēt nozaudēto karti ar e-naudu. ',
);
