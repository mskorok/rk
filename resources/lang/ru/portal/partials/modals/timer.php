<?php

return array (
  'add_your_photo' => 'PIEVIENOT JŪSU ATTĒLU',
  'text1' => 'Ваша сессия закончится через ',
  'text2' => ' минут',
  'text3' => 'Вы хотите продолжить сеанс?',
  'yes' => 'Да',
  'no' => 'Нет',
);
