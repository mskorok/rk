<?php

return array (
  'cancel' => 'ОТМЕНИТЬ',
  'cancel_invoice' => 'Отменить платёж',
  'invoice_number' => 'Номер платежа:',
  'created' => 'Создан:',
  'card' => 'Э-карта:',
  'invoice_identifier' => 'Идентификационный код платежа:',
  'sum' => 'Сумма:',
  'yes' => 'ДА',
  'no' => 'НЕТ',
);
