<?php

return array (
  'title' => 'ПОЖАЛУЙСТА, ВВЕДИТЕ СВОЙ E-MAIL И ПАРОЛЬ',
  'password_terms' => 'Не менее 8 символов, 1 буквы и 1 цифры',
  'invalid_email' => 'Неверный e-mail. Пожалуйста, проверьте правильности написания адреса электронной почты.',
  'invalid_email_confirmation' => 'Ошибка! Адреса e-mail не совпадают.',
  'register' => 'ЗАРЕГИСТРИРОВАТЬСЯ',
  'password_confirm' => 'Повторите пароль',
  'password' => 'Пароль',
  'email' => 'E-mail',
  'email_confirm' => 'Повторите e-mail',
);
