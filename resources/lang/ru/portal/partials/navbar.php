<?php

return array (
  'application' => 'ПРИМЕНЕНИЕ',
  'advantages' => 'ПРЕИМУЩЕСТВА',
  'how_to_start' => 'КАК НАЧАТЬ',
  'questions' => 'ВОПРОСЫ',
  'enter' => 'ВХОД',
  'enter2' => 'ВОЙТИ',
  'forget_password' => 'Забыли пароль?',
  'register' => 'ЗАРЕГИСТРИРОВАТЬСЯ',
  'email' => 'E-mail',
  'password' => 'Пароль',
  'profile' => 'ПРОФИЛЬ',
  'exit' => 'ВЫЙТИ',
);
