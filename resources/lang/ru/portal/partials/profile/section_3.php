<?php

return array (
  'payments' => 'ПЛАТЁЖНЫЕ ПОРУЧЕНИЯ',
  'all' => 'Все',
  'finished' => 'Выполнено',
  'approved' => 'Подтверждено',
  'wait_for_approve' => 'Ждёт подтверждения',
  'cancel' => 'Отменить',
  'date' => 'ЧИСЛО',
  'card' => 'Э-КАРТА',
  'sum' => 'СУММА',
  'status' => 'СТАТУС',
  'document' => 'ДОКУМЕНТ',
  'operations' => 'ДЕЙСТВИЯ',
  'code' => 'КОД',
);
