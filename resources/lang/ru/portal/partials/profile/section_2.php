<?php

return array (
  'notifications' => 'СООБЩЕНИЯ',
  'notification' => 'СООБЩЕНИЕ',
  'all' => 'Все',
  'finished' => 'Выполнено',
  'waiting' => 'Ожидают выполнения',
  'select' => 'ВЫБРАТЬ',
  'status' => 'СТАТУС',
  'type' => 'ВИД',
  'created' => 'СОЗДАН',
  'operations' => 'ДЕЙСТВИЯ',
  'transfer' => 'перевод №',
  'for_sum' => 'не сумму',
  'approved' => 'получен и одобрен',
  'money_transfer' => 'Перевод',
  'profile_approved' => 'Профиль пользователя одобрен в системе, можете начать его использование',
  'profile_data_approved' => 'подтверждение данных',
);
