<?php

return array (
    'description' => 'описание',
    'author' => 'автор',
    'keywords' => 'ключевые слова',
    'limit' => 'Выбрать лимит \\u20AC',
    'select_school' => 'Выбрать школу',
    'select_payment_type' => 'Выбрать способ оплаты',
    'select' => 'Выбрать',
    'transaction_type' => 'Вид операции',
    'status' => 'Статус',
    'card' => 'Э-карта',
    'today' => 'Сегодня',
    'yesterday' => 'Вчера ',
    'last_7_days' => 'За последние 7 дней',
    'last_30_days' => 'За последние 30 дней',
    'this_month' => 'За этот месяц',
    'previous_month' => 'За предыдущий месяц',
    'title' => 'САЙТ ШКОЛЫ',
    'custom' => 'Настраиваемый',
    'applay' => 'Применить',
    'cancel' => 'Отказаться',   
);
