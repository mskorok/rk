<?php

return array (
  'register' => 'Зарегистрироваться',
  'login' => 'войти',
  'login_first' => 'Сначала необходимо войти',
  'account' => 'Профиль',
  'forgot_password' => 'не помню пароля',
  'settings' => 'Настройки',
  'profile' => 'Профиль',
  'user_account_is_not_confirmed' => 'Пользователь не подтвержден',
  'user_account_updated' => 'Изменения сохранены',
  'user_account_created' => 'Пользователь созданю Необходимо подтвердить регистрацию. Проверьте свою почту. ',
);
