<?php

return array (
  'accepted' => 'условие :attribute должно быть подтверждено.',
  'active_url' => 'The :attribute ссылка недействительна',
  'after' => 'The :attribute дата должна быть позже after :date.',
  'alpha' => 'The :attribute может содержать только буквы',
  'alpha_dash' => 'The :attribute может содержать только буквы, числа и черточки.',
  'alpha_num' => 'The :attribute может содержать только буквы и числа.',
  'before' => 'The :attribute дата должна быть до :date.',
  'between' => 
  array (
    'numeric' => 'The :attribute необходимое значение должно быть между :min - :max.',
    'file' => 'The :attribute размер файла должен быть :min - :max kilobytes.',
    'string' => 'The :attribute количество символов :min - :max characters.',
  ),
  'confirmed' => ' :attribute подтверждение не совпадает.',
  'date' => 'The :attribute дата недействительна.',
  'date_format' => 'The :attribute формат даты недейсвителен :format.',
  'different' => 'значение :attribute и значение :other должны отличаться.',
  'digits' => 'The :attribute digits только цифры.',
  'digits_between' => ' значение :attribute должно быть между :min и :max.',
  'email' => ':attribute формат недействителен.',
  'exists' => 'выбранный :attribute введен неверно.',
  'image' => ':attribute только избражение.',
  'in' => 'выбранный :attribute введен неверно.',
  'integer' => 'введенное значение :attribute должно быть числом.',
  'ip' => ':attribute недействительный IP адрес.',
  'max' => 
  array (
    'numeric' => ':attribute не может быть больше :max.',
    'file' => ':attribute не может быть больше :max килобайтов.',
    'string' => ':attribute не может быть больше :max символов.',
  ),
  'mimes' => ':attribute должен быть файлом формата :values.',
  'min' => 
  array (
    'numeric' => ':attribute должен быть как минимум :min.',
    'file' => ':attribute должен быть как минимум :min килобайтов.',
    'string' => ':attribute должен быть как минимум :min символов.',
  ),
  'not_in' => 'выбранный :attribute недействителен.',
  'numeric' => ':attribute должно быть число.',
  'regex' => 'неверный формат заполнения.',
  'required' => 'это поле обязательно к заполнению.',
  'required_if' => ' :attribute поле обязательно если :other :value.',
  'required_with' => 'значение :attribute в поле должно быть заполнено, когда присутствует значение :values.',
  'required_without' => 'значение :attribute в поле должно быть заполнено, когда отсутствует значение:values.',
  'same' => ':attribute и :other должны совпадать.',
  'size' => 
  array (
    'numeric' => ':attribute должен быть :size.',
    'file' => ':attribute должен быть :size килобайтов.',
    'string' => ':attribute должна быть :size символов.',
  ),
  'unique' => ':attribute уже используется.',
  'url' => ':attribute недействительный формат.',
  'custom' => 
  array (
    'error_email' => 'Неверно указан e-mail. Пожалуйста, проверьте ещё раз',
    'error_name' => 'Неверно указаны имя или фамилия получателя. Пожалуйста, проверьте ещё раз!',
    'error_phone' => 'Неверно указан номер телефона получателя. Пожалуйста, проверьте номер ещё раз.',
    'error_text' => 'Поле обязательно для заполнения',
    'error_text1' => 'Поле обязательно для заполнения, максимальное значение :max',
    'error_password' => 'Ваш пароль не подходит. Пожалуйста, введите другой вариант.',
    'error_is_registered' => 'Указанный адрес на портале уже зарегистрирован!',
    'error_reason' => 'Указание причин удаления своего профиля обязательно!',
    'success_is_registered' => 'На указанный адрес отправлена ссылка для активации.',
    'required' => 'Поле обязательное для заполнения.',
    'email' => 'Введите корректный e-mail.',
    'phone' => 'Введите корректный номер телефона без кода страны.',
    'size' => 'Некорректная длинна поля (8 цифр).',
    'integer' => 'Можно вводить только числа.',
    'unique' => 'Такие данные уже зарегистрированы в системе.',
    'document_expired' => 'Закончился срок действия документа.',
  ),
  'recaptcha' => 'Неверный защитный код',
);
