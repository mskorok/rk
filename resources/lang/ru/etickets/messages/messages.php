<?php

return array (
  'cancel' => 
  array (
    'error' => 'Neizdevās atcelt ierakstu.',
    'success' => 'Ieraksts atcelts.',
  ),
  'confirm' => 
  array (
    'error' => 'Neizdevās apstiprināt ierakstu.',
    'success' => 'Ieraksts veiksmīgi apstiprināts.',
  ),
  'tconfirm' => 
  array (
    'error' => 'Neizdevās pabeigt pārskaitījumu.',
    'success' => 'Pārskaitījums veiksmīgi apstiprināts un pabeigts.',
  ),
);
