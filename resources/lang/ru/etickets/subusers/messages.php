<?php

return array (
  'cancel' => 
  array (
    'error' => 'Neizdevās atcelt.',
    'success' => 'Veiksmīgi atcelts.',
  ),
  'create' => 
  array (
    'success' => 'Ieraksts veiksmīgi izveidots.',
  ),
);
