<?php

return array (
  'does_not_exist' => 'Šāds ieraksts nav atrasts!',
  'create' => 
  array (
    'success' => 'Ieraksts izveidots veiksmīgi.',
    'success2' => 'E-talons piesaistīts veiksmīgi. E-talonu varēsiet sākt izmantot kad tas būs apstiprināts!',
  ),
  'edit' => 
  array (
    'success' => 'Ieraksts veiksmīgi labots.',
  ),
  'delete' => 
  array (
    'error' => 'Neizdevās izdzēst ierakstu!',
    'success' => 'Ieraksts dzēsts.',
  ),
  'disable' => 
  array (
    'error' => 'Neizdevās bloķēt e-talonu!',
    'success' => 'E-talons bloķēts.',
  ),
  'enable' => 
  array (
    'error' => 'Neizdevās atbloķēt e-talonu!',
    'success' => 'E-talons atbloķēts.',
  ),
  'put' => 
  array (
    'error' => 'Neizdevās veikt pārskaitījumu!',
    'success' => 'Pārskaitījums veiksmīgs.',
  ),
);
