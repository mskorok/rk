<?php

return array (
  'mpos_management' => 'MPOS iekārtas',
  'mpos_edit' => 'Labot iekārtu',
  'mpos_delete' => 'Dzēst iekārtu',
  'mpos_firmware' => 'Atjaunot firmware',
  'create_a_new_mpos' => 'Izveidot jaunu iekārtu',
);
