<?php

return array (
  'title' => 'Iekārta',
  'type' => 'Veids',
  'object' => 'Objekts, kurā atrodas',
  'owner' => 'Pārdevējs',
  'lastonline' => 'Pēdējoreiz online',
  'fw' => 'Firmware versija',
  'created_at' => 'Izveidots',
);
