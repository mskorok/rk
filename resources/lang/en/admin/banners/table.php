<?php

return array (
  'name' => 'Name',
  'type' => 'Type',
  'sort' => 'Sort',
  'active' => 'Active',
  'size' => 'Size',
  'from' => 'Exposition time from',
  'to' => 'Exposition time to',
);
