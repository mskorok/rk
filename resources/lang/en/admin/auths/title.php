<?php

return array (
  'management' => 'Autorizatori',
  'obj_edit' => 'Labot autorizatoru',
  'obj_delete' => 'Dzēst autorizatoru',
  'create_a_new_obj' => 'Izveidot jaunu autorizatoru',
);
