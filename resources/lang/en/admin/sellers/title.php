<?php

return array (
  'management' => 'Tirgotāji',
  'obj_edit' => 'Labot tirgotāju',
  'obj_delete' => 'Dzēst tirgotāju',
  'create_a_new_obj' => 'Izveidot jaunu tirgotāju',
);
