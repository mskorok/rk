<?php

return array (
  'name' => 'Nosaukums',
  'title' => 'Nr',
  'status' => 'Statuss',
  'pin' => 'PIN kods',
  'balance' => 'Bilance',
  'limit_type' => 'Limita veids',
  'limit' => 'Limits',
  'turnover' => 'Apgrozījums',
  'created_at' => 'Izveidots',
);
