<?php

return array (
  'management' => 'Objektu apvienošana grupās',
  'obj_edit' => 'Labot grupu',
  'obj_delete' => 'Dzēst grupu',
  'create_a_new_obj' => 'Izveidot jaunu grupu',
);
