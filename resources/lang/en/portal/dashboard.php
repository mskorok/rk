<?php

return array (
  'description' => 'description',
  'author' => 'author',
  'keywords' => 'keywords',
  'limit' => 'Izvēlēties limitu \\u20AC',
  'select_school' => 'Izvēlēties skolu',
  'select_payment_type' => 'Izvēlēties maksājuma veidu',
  'select' => 'Izvēlēties',
  'transaction_type' => 'Darījuma veids',
  'status' => 'Statuss',
  'card' => 'E karte',
  'today' => 'Šodien',
  'yesterday' => 'Vakar',
  'last_7_days' => 'Pēdējās 7 dienās',
  'last_30_days' => 'Pēdējās 30 dienās',
  'this_month' => 'Šomēnes',
  'previous_month' => 'Iepr.mēnesī',
  'title' => 'SKOLAS PORTALS',
);
