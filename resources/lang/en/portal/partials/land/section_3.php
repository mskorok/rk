<?php

return array (
  'title' => 'VECĀKU ATSAUKSMES',
  'text1' => 'Dēls saņem skolas naudu ēdināšanai  tieši Skolēna e kartē. Un es detalizēti redzu visu, kas notiek.',
  'text2' => 'Tāds nemiers – minēt vai bērns skolā ir paēdis, vai nē. Tagad viss ir kā uz delnas!',
  'text3' => 'Abi mani bērni izmanto e naudu Skolēna e kartē un man nav jāuztraucas par kaitīgiem pirkumiem veikalā, kas atrodas blakus skolai.',
  'text4' => 'Izmantot ērti un mazbērna Skolēna e kartes papildināšana ir diezgan vienkārša.  Pat es, savā vecumā sapratu, kā un ko darīt!',
  'text5' => 'Dēls saņem skolas naudu ēdināšanai  tieši Skolēna e kartē. Un es detalizēti redzu visu, kas notiek.',
  'grandmother' => 'vecmamma',
  'grandfather' => 'vectēvs',
  'mother' => 'māte',
  'father' => 'tēvs',
);
