<?php

return array (
  'title' => 'KĀ UZSĀKT LIETOT SKOLĒNA E KARTI',
  'registration' => 'REĢISTRĒJIETIES',
  'add' => 'PIEVIENOJIET',
  'transfer' => 'PĀRSKAITIET',
  'use' => 'LIETOJIET',
  'register' => 'Reģistrējieties <br /> portālā',
  'text1' => 'Jums būs nepieciešama Jūsu pases vai ID skenēta kopija vai fotogrāfija, kā arī piekļuve internetbankai.',
  'subtitle2' => 'Pievienojiet bērna <br /> Skolēna e karti',
  'text2' => 'Skolēna e kartei jābūt derīgai.',
  'subtitle3' => 'Pārskaitiet līdzekļus uz <br /> Skolēna e karti',
  'text3' => 'Apmaksājiet maksājuma uzdevumu internetbankā, bankas filiālē, jebkurā citā maksājumu iestādē (bezmaksas) vai norēķinieties ar maksājuma karti (maksas pakalpojums).',
  'subtitle4' => 'Skolēna e karte gatava <br /> lietošanai ',
  'text4' => 'Tagad Jūsu bērns var ērti iepirkties skolas ēdnīcā un Jūsu profilā ir iespējams redzēt ar skolēna e karti veiktos pirkumus.',
);
