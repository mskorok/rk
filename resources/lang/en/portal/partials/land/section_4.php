<?php

return array (
  'title' => 'IESAKIET ŠO PORTĀLU DRAUGIEM',
  'content' => 'Ja ziniet, kam no jums pazīstamiem vecākiem var palīdzēt mūsu portāls, lūdzu, nosūtiet viņiem šo uzaicinājumu',
  'friends_email' => 'Drauga e-pasts',
  'send' => 'NOSŪTĪT',
  'sent' => 'Jūsu ieteikums veiksmīgi nosūtīts. Paldies!',
  'error' => 'Kļūdaina saņēmēja e-pasts adrese. Lūdzu, pārbaudiet e-pasts pareizrakstību.',
);
