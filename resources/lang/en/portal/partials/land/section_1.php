<?php

return array (
  'title' => 'KĀ PORTĀLS VAR PALĪDZĒT VECĀKIEM',
  'text1' => 'Jūs variet pārliecināties par to, kā bērns tērē naudu, jo ar Skolēna e kartes naudu var norēķināties tikai skolā.',
  'text2' => 'Bērns mācās plānot budžetu. Bērnam ir iespēja viegli apgūt bezskaidras naudas norēķinus.',
  'text3' => 'Ja Оūsu bērnam nav skaidras naudas, to nevar atņemt, pazaudēt, kā arī iztērēt ārpus skolas. Ja Skolēna e karte tiek pazaudēta, to viegli var nobloķēt.',
  'text4' => 'Jūs vienmēr varēsiet pārliecināties, vai Jūsu bērns ir paēdis pusdienas Skolēna e karti var papildināt uz nedēļu vai mēnesi uz priekšu.',
  'subtitle1' => 'Nav jāuztraucas',
  'subtitle2' => 'Var iemācīt',
  'subtitle3' => 'Var aizsargāt',
  'subtitle4' => 'Rūpes',
  'user_account' => 'Lietotāja konts',
  'purchase' => 'Pirkums',
  'refill' => 'Pārskaitijums',
  'return_to_account' => 'Return to account',
);
