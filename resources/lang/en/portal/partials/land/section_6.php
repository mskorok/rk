<?php

return array (
  'title' => 'BIEŽĀK UZDOTIE JAUTĀJUMI',
  'use' => 'LIETOŠANA',
  'register' => 'REĢISTRĀCIJA',
  'security' => 'DROŠĪBA',
  'subtitle1' => 'Kas  jādara, ja manam bērnam nav Skolēna e kartes, vai tās derīguma termiņš ir beidzies?',
  'text1' => 'Plašāka informācija par skolēna kartes noformēšanu vai atkārtotu izsniegšanu ir pieejama šeit.',
  'subtitle2' => 'Kā noformēt Skolēna e karti?',
  'subtitle3' => 'Kā reģistrēties portālā?',
);
