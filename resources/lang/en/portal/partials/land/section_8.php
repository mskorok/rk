<?php

return array (
  'title' => 'PIEPRASĪT MŪSU SPECIĀLISTA ATBALSTU',
  'success' => 'Jūsu pieprasījums veiksmīgi nosūtīts. Paldies!',
  'error_email' => 'Kļūdaina e-pasts adrese. Lūdzu, pārbaudiet e-pasts pareizrakstību.',
  'error_name' => 'Saņēmēja Vārds vai Uzvārds nav derīgs. Lūdzu, pārbaudiet pareizrakstību.',
  'error_phone' => 'Saņēmēja tālruņa numurs nav derīgs. Lūdzu, pārbaudiet numura pareizrakstību.',
  'sent' => 'Jūsu pieprasījums veiksmīgi nosūtīts. Paldies!',
  'fill_all_fields' => 'Lūdzu, aizpildiet visus laukus.',
  'name' => 'Vārds',
  'surname' => 'Uzvārds',
  'email' => 'E-pasts',
  'phone' => 'Tālrunis',
  'message' => 'Jūsu jautājums',
  'send' => 'NOSŪTĪT',
);
