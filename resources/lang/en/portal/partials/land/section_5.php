<?php

return array (
  'title' => 'IESAKIET ŠO PORTĀLU DRAUGIEM',
  'trolleybus' => 'trolejbusos',
  'tram' => 'tramvajos',
  'bus' => 'autobusos',
  'subtitle1' => 'Skolēni var braukt bez maksas',
  'text1' => 'Skolēni var braukt bez maksas Rīgas sabiedriskajā transportā:',
  'subtitle2' => 'Brīvpusdienu saņemšanai un pirkšanai skolas ēdnīcā',
  'text2' => 'Vecākiem ir iespēja pārskaitīt savam bērnam naudu sākot ar 1 EUR un būt drošiem, ka bērns neizmantos šo naudu neveselīgas pārtikas iegādei un varēs tērēt naudu tikai skolas ēdnīcā.',
  'subtitle3' => 'Skolas apmeklēšanas statistika un kontrole',
  'text3' => 'Skolās, kuras ir aprīkotas ar ieejas kontroles sistēmu, ienākot skolā un izejot no tās ir nepieciešams reģistrēt savu Skolēna e karti. Tādā veidā būs iespējams kontrolēt skolas apmeklējumu – cikos bērns atnāca uz skolu un cikos aizgāja.',
);
