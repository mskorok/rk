<?php

return array (
  'notifications' => 'PAZIŅOJUMI',
  'notification' => 'PAZIŅOJUMS',
  'all' => 'Visi',
  'finished' => 'Izpildīts',
  'waiting' => 'Gaida izpildi',
  'select' => 'ATLASĪT',
  'status' => 'STATUSS',
  'type' => 'VEIDS',
  'created' => 'IZVEIDOTS',
  'operations' => 'DARBĪBAS',
  'transfer' => 'Pārskaitījums nr. ',
  'for_sum' => ' par summu ',
  'approved' => ' ir saņemts un apstiprināts',
  'money_transfer' => 'Naudas pārskaitījums',
  'profile_approved' => 'Jūsu lietotāja profils sistēmā ir apstiprināts, varat sākt to izmantot pilnībā',
  'profile_data_approved' => 'Profila datu apstiprinājums',
);
