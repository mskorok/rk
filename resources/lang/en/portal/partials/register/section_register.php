<?php

return array (
  'title' => 'LŪDZU, IEVADIET SAVU E-PASTU UN PAROLI',
  'password_terms' => 'Vismaz 8 simboli, 1 burts un 1 cipars',
  'invalid_email' => 'Kļūdaina e-pasts adrese. Lūdzu, pārbaudiet e-pasts pareizrakstību.',
  'invalid_email_confirmation' => 'Kļūda! E-pasts adreses nesakrīt.',
  'register' => 'REĢISTRĒTIES',
  'password_confirm' => 'Parole atkārtoti',
  'password' => 'Parole',
  'email' => 'E-pasts',
  'email_confirm' => 'E-pasts atkārtoti',
);
