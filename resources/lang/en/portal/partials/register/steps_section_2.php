<?php

return array (
  'title' => 'LŪDZU, ZEMĀK NORĀDIET, VAI ATBILSTAT KĀDAI NO ŠĀDĀM PAZĪMĒM',
  'a' => ' Ārpus Latvijas Republikas ieņemu kādu no šādiem amatiem: valsts vadītājs, parlamenta deputāts, valdības vadītājs, ministrs, ministra vietnieks vai ministra vietnieka vietnieks, valsts sekretārs, augstākās tiesas tiesnesis, konstitucionālās tiesas tiesnesis, augstākās revīzijas (audita) iestādes padomes vai valdes loceklis, centrālās bankas padomes vai valdes loceklis, vēstnieks, pilnvarotais lietvedis, bruņoto spēku augstākais virsnieks, valsts kapitālsabiedrības padomes vai valdes loceklis, vai arī attiecīgo amatu atstāju viena gada laikā;',
  'b' => ' Esmu iepriekšminēto personu vecāks, laulātais vai viņam pielīdzināma persona, bērns, viņa laulātais vai laulātajam pielīdzināma persona. Persona par laulātajam pielīdzināmu uzskatāma tikai tad, ja attiecīgās valsts likumi tai nosaka šādu statusu;',
  'c' => ' Esmu persona, par kuru ir publiski zināms, ka tai ir darījuma attiecības ar kādu no šī dokumenta 1. punktā, vai kurai ar šo personu kopīgi pieder pamatkapitāls komercsabiedrībā, kā arī fiziskā persona, kura ir vienīgā tāda juridiska veidojuma īpašnieks, par kuru ir zināms, ka tas faktiski izveidots šī dokumenta 1. punktā minētās personas labā;',
  'yes' => 'Jā',
  'no' => 'Nē',
  'position' => 'Lūdzu, norādiet valsti un amatu',
);
