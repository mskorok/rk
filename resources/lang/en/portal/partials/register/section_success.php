<?php

return array (
  'title' => 'JŪSU KONTS VEIKSMĪGI REĢISTRĒTS',
  'message1' => 'Lai pārliecinātos, ka norādītais e-pasts: ',
  'message2' => ' pieder Jums, uz to tiks nosūīta vēstule ar apstiprinājuma saiti. Lūdzu, nospiediet to, lai apstiprinātu Jūsu e-pastu un turpinātu reģistrāciju.',
);
