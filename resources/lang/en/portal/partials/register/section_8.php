<?php

return array (
  'support_title' => 'PIEPRASĪT MŪSU SPECIĀLISTA ATBALSTU',
  'sent' => 'Jūsu pieprasījums veiksmīgi nosūtīts. Paldies!',
  'invalid_email' => 'Kļūdaina e-pasts adrese. Lūdzu, pārbaudiet e-pasts pareizrakstību.',
  'send' => 'NOSŪTĪT',
  'questions' => 'Jūsu jautājums',
  'name' => 'Vārds',
  'surname' => 'Uzvārds',
  'phone' => 'Tālrunis',
  'email' => 'E-pasts',
  'title' => 'PIEPRASĪT MŪSU SPECIĀLISTA ATBALSTU',
  'success' => 'Jūsu pieprasījums veiksmīgi nosūtīts. Paldies!',
  'error_email' => 'Kļūdaina e-pasts adrese. Lūdzu, pārbaudiet e-pasts pareizrakstību.',
  'error_name' => 'Saņēmēja Vārds vai Uzvārds nav derīgs. Lūdzu, pārbaudiet pareizrakstību.',
  'error_phone' => 'Saņēmēja tālruņa numurs nav derīgs. Lūdzu, pārbaudiet numura pareizrakstību.',
  'fill_all_fields' => 'Lūdzu, aizpildiet visus laukus.',
  'message' => 'Jūsu jautājums',
);
