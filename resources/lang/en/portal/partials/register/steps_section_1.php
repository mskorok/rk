<?php

return array (
  'title' => 'SVEICINĀTI, :name :surname',
  'subtitle' => 'LAI REĢISTRĒTOS SKOLĒNA E KARTES PORTĀLĀ, AIZPILDIET FORMU',
  'add_doc_copy' => 'PIEVIENOT DOK. KOPIJU',
  'max_image_size' => 'Maks. attēla lielums līdz 4 Mb',
  'allowed_formats' => 'Atļauts formāts: JPG, PNG',
  'doc_number' => 'Personas dokumenta numurs',
  'doc_type' => 'Personas dokum.',
  'citizenship' => 'Pilsonība',
  'address' => 'Deklarētā adrese',
  'phone' => 'Tālrunis',
);
