<?php

return array (
  'title' => 'ZIŅAS PAR SKOLĒNA E KARTES KONTĀ VEICAMAJIEM DARĪJUMIEM UN KLIENTA LĪDZEKĻU IZCELSMI',
  'text1' => 'Plānotie ienākošie/izejošie maksājumi viena kalendārā mēneša laikā (maksimāli 140 €).',
  'text2' => 'Plānotie ienākošie/izejošie maksājumi viena viena kal - endārā gada laikā (maksimāli 2500 €).',
  'text3' => 'Plānotais viena pirkuma darī - juma vidējais un maksimāli pieļaujamais apjoms',
  'incoming' => 'Ienākošie €',
  'outgoing' => 'Izejošie €',
  'amount' => 'Vidējais apjoms €',
  'max_amount' => 'Maksimālais apjoms €',
  'desc1' => 'Visi darījumi ar E-kartes kontā esošajiem līdzekļiem tiks veikti Konta pārvaldnieka un E-kartes lietotāja interesēs',
  'desc2' => 'Esmu E-kartes kontā esošo līdzekļu patiesais īpašnieks',
  'add_image' => 'PIEVIENOT JŪSU ATTĒLU',
  'max_image_size' => 'Maks. attēla lielums līdz 4 Mb',
  'allowed_formats' => 'Atļauts formāts: JPG, PNG',
  'error' => 'ВНИМАНИЕ: регистрационная форма заполнена с ошибками или не полностью!
',
  'agree' => 'Piekrītu noteikumiem',
  'register' => 'REĢISTRĒTIES',
  'save' => 'SAGLABĀT UN PABEIGT VĒLĀK',
  'yes' => 'Jā',
  'no' => 'Nē',
  'position' => 'Lūdzu, norādiet valsti un amatu',
);
