<?php

return array (
  'application' => 'PIELIETOJUMS',
  'advantages' => 'PRIEKŠROCĪBAS',
  'how_to_start' => 'KĀ UZSĀKT',
  'questions' => 'JAUTĀJUMI',
  'enter' => 'IEEJA',
  'enter2' => 'IENĀKT',
  'forget_password' => 'Aizmirsat paroli?',
  'register' => 'REĢISTRĒTIES',
  'email' => 'Email',
  'password' => 'Password',
  'profile' => 'JŪSU PROFILS',
  'exit' => 'IZIET',
);
