<?php

return array (
  'title' => 'JAUNS E-PASTS',
  'text' => 'Lai pārliecinātos, ka noradīts e-pasts pieder jums, mēs nosūtīsim vēstuli ar apstiprinājuma saiti. Lūdzu, nospiediet to, lai apstiprinātu ka tā ir Jūsu e-pasts. Kamēr e-pasts netiks apstiprināts, tas nebūs mainīts.',
  'email' => 'E-pasts',
  'change' => 'NOMAINĪT E-PASTU',
);
