<?php

return array (
  'add_child_photo' => 'PIEVIENOT SKOLĒNA ATTĒLU',
  'download_child_photo' => 'LEJUPIELĀDĒT SKOLĒNA ATTĒLU:',
  'boys' => 'ZĒNI',
  'girls' => 'MEITENES',
  'add_image' => 'PIEVIENOT ATTĒLU',
  'error' => 'KĻŪDA:<br/>Attēla nepareizs izmērs vai formāts.',
  'save' => 'SAGLABĀT',
);
