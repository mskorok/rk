<?php

return array (
  'title' => 'PAROLES ATGŪŠANA',
  'text' => 'Lūdzu, ievadiet savu e-pasts adresi, ko esat reģistrējis profilā, un mēs Jums nosūtīsim saiti uz lapu, kurā varēsiet izvēlēties jaunu paroli.',
  'email' => 'E-pasts',
  'recover' => 'ATGŪT PAROLI',
  'error' => 'KĻUDA: <br />Kļūdaina e-pasts adrese.',
);
