<?php

return array (
  'title' => 'BRĪDINĀJUMS',
  'text' => ' Vai Jūsu “DOKUMENTS”, “DOK. NUMURS” joprojām derīgs?',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
