<?php

return array (
  'title' => 'DZĒST PROFILU',
  'text1' => 'Vai tiešam dzēst profilu ;name :surname ?',
  'text2' => 'Jūsu Skolēna e kartes atlikums ir',
  'text3' => 'Lai izdzēstu profilu, lūdzu nokopējiet un aizpildiet iesniegumu līdzekļu atgriešanai un nosūtiet to uz e-pastu ',
  'text4' => 'Iesniegums līdzekļu atgriešanai',
  'text5' => 'Es, (:name :surname), lūdzu atgriezt (:sum) EUR no Skolēna e kartes (:number), pārskaitot to uz (:bank) bankas kontu (:account) un dzēst manu pro lu skolas.rigaskarte. lv portālā.',
  'text6' => 'Jūsu iesniegums tiks izskatīts 5 darba dienu laikā, par tās rezultātiem informēsim Jūs, nosūtot e-pastu.',
);
