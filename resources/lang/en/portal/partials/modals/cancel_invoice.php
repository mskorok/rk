<?php

return array (
  'cancel' => 'ATCELT',
  'cancel_invoice' => 'Atcelt maksājuma uzdevumu',
  'invoice_number' => 'Maksājuma nr.:',
  'created' => 'Izveidots:',
  'card' => 'E karte:',
  'invoice_identifier' => 'Maks. ident. kods:',
  'sum' => 'Summa:',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
