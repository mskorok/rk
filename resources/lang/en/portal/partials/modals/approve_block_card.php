<?php

return array (
  'title_block' => 'BLOĶĒT KARTI',
  'title_unblock' => 'ATBLOĶĒT KARTI',
  'are_you_sure' => 'Vai tiešam nobloķēt Skolēna e karti',
  'are_you_sure_unblock' => 'Vai tiešam atbloķēt Skolēna e karti',
  'success_block' => 'Skolēna karte ir bloket',
  'success_unblock' => 'Skolēna karte ir ir atbloķēta',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
