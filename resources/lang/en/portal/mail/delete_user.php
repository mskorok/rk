<?php

return array (
  'subject' => 'Удалена ваша учетная запись на портале SKOLĒNA E KARTE',
  'text' => 'Подтверждение удаления вашей учетной записи на портале skolas.rigaskarte.lv.<br>Пользователь: :name :surname <br>Электронный адрес: :email <br>Данное письмо не требует ответа.<br>Всегда рады вашему возвращению на портал!<br>При возникновении вопросов свяжитесь с нами по адресу: <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a><br>С уважением,<br>ПОРТАЛ SKOLĒNA E KARTI',
);
