<?php

return array (
  'subject' => ' Ваш друг рекомендует Вам посетить портал SKOLĒNA E KARTE',
  'text' => 'Добрый день!<br>Ваш хороший друг отправил вам приглашение познакомиться с порталом SKOLĒNA E KARTE. Перейдите по ссылке <a href="https://skolas.rigaskarte.lv">skolas.rigaskarte.lv</a> и узнайте все возможности!<br>Портал позволяет полностью управлять Skolēna e karte вашего ребенка. Skolēna e karte Jūsu<br> sirdsmieram. Pateicoties portālam, Jūs variet pilnībā kontrolēt Skolēna e karti:<br><ul type="circle"><li>отметка о бесплатном обеде</li><li>papildināt to</li><li>apskatīt pirkumu vēsturi</li><li>aizsargāt to ar pin kodu</li><li>nozaudēšanas gadījumā nobloķēt to</li><br>Pusdienas ar Skolēna e karti Jūs vienmēr zināsiet, vai Jūsu bērns ir paēdis. Bezskaidras naudas<br>norēķini ar Skolēna e karti Rīgas skolās. Tagad vecāki varēs būt droši, ka viņu bērniem piešķirtā nauda<br> tiks izmantota tieši paredzētajam mērķim – veselīgām pusdienām.<br>С уважением,<br>ПОРТАЛ SKOLĒNA E KARTE<br>',
);
