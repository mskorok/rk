<?php

return array (
  'subject' => 'Обновлён ваш емайл адрес на портале SKOLĒNA E KARTE',
  'text' => 'Вами был изменен емайл адрес в своей учетной записи на портале skolas.rigaskarte.lv.<br>Старый электронный адрес: :old_email <br>Новый электронный адрес: :email <br>Данное письмо не требует ответа.<br>При возникновении вопросов свяжитесь с нами по адресу: <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a><br>С уважением,<br>ПОРТАЛ SKOLĒNA E KARTI',
);
