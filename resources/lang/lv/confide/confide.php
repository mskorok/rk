<?php

return array (
  'username' => 'Vārds',
  'surname' => 'Uzvārds',
  'password' => 'Parole',
  'password_confirmation' => 'Parole atkārtoti',
  'e_mail' => 'E-pasts',
  'phone' => 'Telefons',
  'username_e_mail' => 'Lietotājvārds vai e-pasts',
  'signup' => 
  array (
    'title' => 'Ienākt',
    'desc' => 'Pierakstīšanās jaunam kontam',
    'confirmation_required' => 'Būs nepieciešams apstiprināt',
    'submit' => 'Nosūtīt',
  ),
  'login' => 
  array (
    'title' => 'Ienākt',
    'desc' => 'Ievadiet savus datus',
    'forgot_password' => 'Aizmirsu paroli',
    'remember' => 'Atcerēties',
    'submit' => 'Ienākt',
  ),
  'forgot' => 
  array (
    'title' => 'Atjaunot paroli',
    'submit' => 'Turpināt',
  ),
  'alerts' => 
  array (
    'account_created' => 'Lietotāja profils veiksmīgi izveidots. Lūdzu, sekojiet instrukcijai e-pastā, lai apstiprinātu Jūsu profilu!',
    'too_many_attempts' => 'Pārāk daudz nesekmīgu mēģinājumu, uzgaidiet dažas minūtes.',
    'wrong_credentials' => 'Nepareizs lietotājs vai parole',
    'not_confirmed' => 'Jūsu konts nav apstiprināts, pārbaudiet savu e-pastu!',
    'confirmation' => 'Jūsu reģistrācija ir apstiprināta! Varat ielogoties sistēmā.',
    'wrong_confirmation' => 'Nepareizs apstiprinājuma kods.',
    'password_forgot' => 'Informācija paroles atjaunošanai ir nosūtīta uz Jūsu e-pastu.',
    'wrong_password_forgot' => 'Šāds lietotājs nav atrasts.',
    'password_reset' => 'Parole veiksmīgi nomainīta.',
    'wrong_password_reset' => 'Nepareiza parole! Mēģiniet vēlreiz.',
    'wrong_token' => 'Paroles atjaunošanas kods nav korekts.',
    'duplicated_credentials' => 'Šādi dati sistēmā jau ir reģistrēti!',
  ),
  'email' => 
  array (
    'account_confirmation' => 
    array (
      'subject' => 'Reģistrācijas apstiprinājums',
      'greetings' => 'Sveicināti, Cien./God. Konta pārvaldnieks :name',
      'body' => 'Informējam, ka saskaņā ar Noziedzīgi iegūtu līdzekļu legalizācijas un terorisma finansēšanas novēršanas likuma 12.panta pirmo daļu, Sistēmas pārvaldnieks Konta pārvaldnieku identificē, pārbaudot tā identitāti pēc personu apliecinoša dokumenta. Lūdzam Jūs, aktivizējot šo saiti, ielādēt personu apliecinoša dokumenta kopiju.',
      'farewell' => '',
    ),
    'password_reset' => 
    array (
      'subject' => 'Atjaunot paroli',
      'greetings' => 'Labdien, :name',
      'body' => 'Uzklikšķiniet uz saites, lai nomainītu paroli',
      'farewell' => '',
    ),
  ),
);
