<?php

return array (
  'does_not_exist' => 'Šāds ieraksts nav atrasts!',
  'create' => 
  array (
    'success' => 'Ieraksts veiksmīgi izveidots.',
  ),
  'edit' => 
  array (
    'success' => 'Ieraksts veiksmīgi labots.',
  ),
  'delete' => 
  array (
    'error' => 'Neizdevās dzēst ierakstu!',
    'success' => 'Ieraksts dzēsts.',
  ),
);
