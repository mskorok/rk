<?php

return array (
  'user' => 'Lietotājs',
  'rec' => 'Saņēmējs',
  'own' => 'Īpašnieks',
  'mpos' => 'MPOS',
  'obj' => 'Objekts',
  'eticket' => 'E-talons',
  'status' => 'Statuss',
  'type' => 'Tips',
  'compl' => 'Apstiprināts',
  'content' => 'Saturs',
  'created_at' => 'Izveidots',
);
