<?php

return array (
  'title' => 'Iekārta',
  'type' => 'Veids',
  'object' => 'Objekts, kurā atrodas',
  'owner' => 'Pārdevējs',
  'lastonline' => 'Pēdējā pieslēgšanās',
  'fw' => 'Programmaparatūras versija',
  'created_at' => 'Izveidots',
);
