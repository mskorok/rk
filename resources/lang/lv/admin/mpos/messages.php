<?php

return array (
  'does_not_exist' => 'Šāds ieraksts nav atrasts!',
  'create' => 
  array (
    'success' => 'Ieraksts veiksmīgi izveidots.',
  ),
  'edit' => 
  array (
    'success' => 'Ieraksts veiksmīgi labots.',
  ),
  'delete' => 
  array (
    'error' => 'Neizdevās dzēst ierakstu!',
    'success' => 'Ieraksts dzēsts.',
  ),
  'restart' => 
  array (
    'error' => 'Neizdevās pārstartēt MPOS iekārtu!',
    'success' => 'Pārstartēšanas komanda veiksmīgi nosūtīta.',
  ),
  'conf' => 
  array (
    'error' => 'Neizdevās saglabāt MPOS iekārtas konfigurāciju!',
    'success' => 'Konfigurācija veiksmīgi saglabāta.',
  ),
  'fw' => 
  array (
    'error' => 'Neizdevās saglabāt MPOS programmaparatūru!',
    'success' => 'Programmaparatūra veiksmīgi saglabāta.',
  ),
);
