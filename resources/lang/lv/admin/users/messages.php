<?php

return array (
  'already_exists' => 'Lietotājs jau eksistē!',
  'does_not_exist' => 'Lietotājs neeksistē.',
  'login_required' => 'Norādiet lietotāja vārdu!',
  'password_required' => 'Jāaizpilda paroles lauks.',
  'password_does_not_match' => 'Paroles nesakrīt.',
  'create' => 
  array (
    'error' => 'Neizdevās izveidot lietotāju.',
    'success' => 'Lietotājs veiksmīgi izveidots.',
  ),
  'edit' => 
  array (
    'impossible' => 'Nevar labot šo lietotāju.',
    'error' => 'Neizdevās veikt labojumus šim lietotājam.',
    'success' => 'Lietotāja dati veiksmīgi laboti.',
  ),
  'delete' => 
  array (
    'impossible' => 'Nevar izdzēst šo lietotāju.',
    'error' => 'Neizdevās dzēst šo lietotāju.',
    'success' => 'Lietotājs veiksmīgi dzēsts.',
  ),
);
