<?php

return array (
  'user_management' => 'Lietotāji',
  'user_update' => 'Labot lietotāja datus',
  'user_delete' => 'Dzēst lietotāju',
  'create_a_new_user' => 'Izveidot jaunu lietotāju',
);
