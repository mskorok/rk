<?php

return array (
  'first_name' => 'Vārds',
  'surname' => 'Uzvārds',
  'user_id' => 'Lietotāja ID',
  'username' => 'Lietotājvārds',
  'email' => 'E-pasts',
  'phone' => 'Telefons',
  'groups' => 'Grupas',
  'roles' => 'Tiesības',
  'activated' => 'Aktīvs lietotājs',
  'created_at' => 'Izveidots',
  'last_login' => 'Pēdējā pieslēgšanās',
);
