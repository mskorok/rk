<?php

return array (
  'name' => 'Nosaukums',
  'sort' => 'Veids',
  'path' => 'Ceļš',
  'exposition_time_to' => 'Rādīt līdz',
  'exposition_time_from' => 'Rādīt no',
  'image' => 'Attēls',
  'flash' => 'Flash',
  'active' => 'Aktīvs',
  'html' => 'Html',
  'ajax_html' => 'Ajax html',
  'xml' => 'Xml',
  0 => '1140 х 250',
  1 => '555 х 160',
  2 => '360 х 160',
  3 => '262 х 160',
  'does_not_exist' => 'Šāds ieraksts nav atrasts!',
  'edit' => 
  array (
    'success' => 'Ieraksts veiksmīgi labots.',
  ),
  'create' => 
  array (
    'success' => 'Izveidots jauns objekts',
  ),
);
