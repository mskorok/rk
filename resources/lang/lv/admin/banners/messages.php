<?php

return array (
  'name' => 'Nosaukums',
  'type' => 'Tips',
  'sort' => 'Veids',
  'size' => 'Izmērs',
  'path' => 'Ceļš',
  'code' => 'kods',
  'exposition_time_to' => 'Rādīt līdz',
  'exposition_time_from' => 'Rādīt no',
  'image' => 'Attēls',
  'flash' => 'Flash',
  'active' => 'aktīvs',
  'html' => 'Html',
  'ajax_html' => 'Ajax html',
  'xml' => 'Xml',
  0 => '1140 x 250',
  1 => '555 x 160',
  2 => '360 x 160',
  3 => '262 x 160',
  'does_not_exist' => 'Šāds ieraksts nav atrasts!',
  'edit' => 
  array (
    'success' => 'Ieraksts veiksmīgi labots.',
  ),
  'create' => 
  array (
    'success' => 'Izveidots jauns objekts',
  ),
);
