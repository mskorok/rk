<?php

return array (
  'name' => 'Nosaukums',
  'type' => 'Tips',
  'sort' => 'Veids',
  'active' => 'Aktīvs',
  'size' => 'Izmērs',
  'from' => 'Rādīt no',
  'to' => 'Rādīt līdz',
);
