<?php

return array (
  'password' => 'Parolē jābūt vismaz 6 rakstzīmēm un jāatbilst apstiprinājumam.',
  'user' => 'Lietotājs ar šādu e-pastu nepastāv.',
  'token' => 'Paroles atiestatīšanas marķieris nav derīgs.',
);
