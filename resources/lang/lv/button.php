<?php

return array (
  'save' => 'Saglabāt',
  'edit' => 'Labot',
  'delete' => 'Dzēst',
  'disable' => 'Bloķēt',
  'enable' => 'Atbloķēt',
  'put' => 'Papildināt',
  'cancel' => 'Atcelt',
  'return' => 'Atpakaļ',
  'confirm' => 'Apstiprināt',
  'create' => 'Izveidot jaunu',
  'restart' => 'Restartēt',
  'search' => 'Meklēt',
  'upload_firmware' => 'Atjaunot programmaparatūru',
  'upload_conf' => 'Atjaunot konfigurāciju',
  'execute' => 'Izpildīt',
);
