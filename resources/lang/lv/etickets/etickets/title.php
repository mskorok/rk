<?php

return array (
  'management' => 'E-taloni',
  'obj_edit' => 'Labot e-talonu',
  'obj_delete' => 'Dzēst e-talonu',
  'obj_disable' => 'Bloķēt e-talonu',
  'obj_enable' => 'Atbloķēt e-talonu',
  'obj_put' => 'Papildināt e-talonu',
  'obj_putgroup' => 'Papildināt e-talonu grupu',
  'obj_give' => 'Nodot e-talonu citam lietotājam',
  'obj_addgroup' => 'Izveidot e-talonu grupu',
  'obj_delgroup' => 'Dzēst e-talonu grupu',
  'create_a_new_obj' => 'Pievienot jaunu e-talonu',
);
