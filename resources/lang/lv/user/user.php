<?php

return array (
  'register' => 'Reģistrēties',
  'login' => 'Ienākt',
  'login_first' => 'Nepieciešams ielogoties',
  'account' => 'Konts',
  'forgot_password' => 'Aizmirsu paroli',
  'settings' => 'Uzstādījumi',
  'profile' => 'Profils',
  'user_account_is_not_confirmed' => 'Lietotājs nav apstiprināts.',
  'user_account_updated' => 'Labotā informācija ir saglabāta.',
  'user_account_created' => 'Lietotājs izveidots. Nepieciešams apstiprināt reģistrāciju, pārbaudiet savu e-pastu.',
);
