<?php

return array (
  'add_your_photo' => 'PIEVIENOT JŪSU FOTO',
  'text1' => 'Jūsu sesija beigsies pēc',
  'text2' => 'minūtēm',
  'text3' => 'Vai vēlaties turpināt sesiju?',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
