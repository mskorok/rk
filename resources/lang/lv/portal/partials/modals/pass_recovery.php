<?php

return array (
  'title' => 'PAROLES ATGŪŠANA',
  'text' => 'Lūdzu, ievadiet savu e-pasta adresi, uz kuru nosūtīt saiti uz lapu, kurā varēsiet izveidot jaunu paroli.',
  'email' => 'E-pasts',
  'recover' => 'ATGŪT PAROLI',
  'error' => 'KĻŪDA: <br />Kļūdaina e-pasta adrese.',
);
