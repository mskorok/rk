<?php

return array (
  'add_your_photo' => 'PIEVIENOJIET FOTO',
  'text1' => 'Jūsu bankas sesija beigsies pēc',
  'text2' => 'minūtēm',
  'text3' => 'Vai vēlaties turpināt sesiju?',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
