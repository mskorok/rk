<?php

return array (
  'title' => 'JAUNS E-PASTS',
  'text' => 'Uz norādīto e-pastu tiks nosūtīta apstiprinājuma saite. Uzklišķiniet uz saites, lai apstiprinātu Jūsu e-pastu. Izmaiņas tiks veiktas pēc e-pasta apstiprināšanas.',
  'email' => 'E-pasts',
  'change' => 'NOMAINĪT E-PASTU',
);
