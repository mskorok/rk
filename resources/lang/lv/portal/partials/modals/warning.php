<?php

return array (
  'title' => 'BRĪDINĀJUMS',
  'text1' => 'Jūsu sesija beigsies pēc 5 minūtēm',
  'text2' => 'Vai vēlaties turpināt sesiju?',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
