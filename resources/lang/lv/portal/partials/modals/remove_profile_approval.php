<?php

return array (
  'title' => 'DZĒST PROFILU',
  'text1' => 'Vai tiešām dzēst profilu :name :surname ?',
  'text2' => 'Jūsu Skolēna e-kartes atlikums ir',
  'text3' => 'Lai dzēstu profilu, lūdzu, nokopējiet un aizpildiet iesniegumu līdzekļu atgūšanai, un nosūtiet to uz e-pastu.',
  'text4' => 'Iesniegums līdzekļu atgūšanai',
  'text5' => 'Es, (:name :surname), lūdzu atmaksāt (:sum) EUR no Skolēna e-kartes (:number), pārskaitot to uz (:bank) bankas kontu (:account), un dzēst manu profilu no portāla skolas.rigaskarte.lv.',
  'text6' => 'Jūsu iesniegums tiks izskatīts 5 (piecu) darba dienu laikā, atbildi nosūtot uz Jūsu e-pastu.',
);
