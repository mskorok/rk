<?php

return array (
  'title' => 'DZĒST PROFILU',
  'text1' => 'Vai tiešām dzēst profilu :name :surname?',
  'text2' => 'Jūsu Skolēna e-kartes atlikums ir',
  'text3' => 'Profila dzēšana ir neatgriezeniska.',
  'reason_1' => 'Bērns ir pabeidzis skolu, un Skolēna e-karte vairs nav derīga.',
  'reason_2' => 'Bērns ir mainījis skolu. Jaunajā skolā Skolēna e-karte netiek izmantota.',
  'reason_3' => 'Bērns neizmanto Skolēna e-karti pirkumu apmaksai.',
  'reason_4' => 'Cits iemesls',
  'password' => 'Parole',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
