<?php

return array (
  'title_block' => 'BLOĶĒT KARTI',
  'title_unblock' => 'ATBLOĶĒT KARTI',
  'are_you_sure' => 'Vai tiešām bloķēt Skolēna e-karti?',
  'are_you_sure_unblock' => 'Vai tiešām atbloķēt Skolēna e-karti?',
  'success_block' => 'Skolēna e-karte ir bloķēta.',
  'success_unblock' => 'Skolēna e-karte ir atbloķēta.',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
