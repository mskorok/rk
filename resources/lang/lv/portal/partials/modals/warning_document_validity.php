<?php

return array (
  'title' => 'BRĪDINĀJUMS',
  'text' => 'Vai Jūsu :doc , :doc_number vēl ir derīgs?',
  'yes' => 'JĀ',
  'no' => 'NĒ',
);
