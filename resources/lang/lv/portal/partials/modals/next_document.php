<?php

return array (
  'title' => 'BRĪDINĀJUMS',
  'text' => 'Jūsu :doc, :number beidzies derīguma termiņš. Lūdzu, pievienojiet jaunu personu apliecinošu dokumentu.',
  'add_new' => 'PIEVIENOT JAUNU DOKUMENTU',
);
