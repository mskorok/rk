<?php

return array (
  'register' => 'REĢISTRĒTIES',
  'slider1_title' => 'Bezskaidras naudas <br/> norēķini ar Skolēna e-karti <br/> Rīgas skolās',
  'slider1_text' => 'Vecāki var būt droši, <br/> ka bērniem piešķirtā nauda <br/> tiks izmantota paredzētajam mērķim - <br/> veselīgām pusdienām.',
  'slider2_title' => 'Veselīga pārtika skolā <br/> ar Skolēna e-karti',
  'slider2_text' => 'Tagad ikviens skolēns var ātri <br/> un viegli veikt pirkumus skolas ēdnīcā, <br/> norēķinoties ar e-naudu.',
  'slider3_title' => 'Skolēna e-karte <br/> Jūsu sirdsmieram',
  'slider3_text' => 'Pateicoties portālam, Jūs varat pilnībā <br/> kontrolēt Skolēna e-karti.',
  'slider3_select_1' => 'papildināt to',
  'slider3_select_2' => 'apskatīt pirkumu vēsturi',
  'slider3_select_3' => 'aizsargāt to ar pin kodu',
  'slider3_select_4' => 'nozaudēšanas gadījumā nobloķēt to',
);
