<?php

return array (
  'payments' => 'MAKSĀJUMA UZDEVUMI',
  'all' => 'Visi',
  'finished' => 'Izpildīts',
  'approved' => 'Apstiprināts',
  'wait_for_approve' => 'Gaida apstiprināšanu',
  'cancel' => 'Atcelt',
  'date' => 'DATUMS',
  'card' => 'E-KARTE',
  'sum' => 'SUMMA',
  'status' => 'STATUSS',
  'document' => 'DOKUMENTS',
  'operations' => 'DARBĪBAS',
  'code' => 'KODS',
);
