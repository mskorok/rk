<?php

return array (
  'title' => 'LŪDZU, IEVADIET SAVU E-PASTU UN PAROLI',
  'password_terms' => 'Vismaz 8 simboli, tai skaitā vismaz 1 lielais burts un 1 cipars',
  'invalid_email' => 'Kļūdaina e-pasta adrese. Lūdzu, pārbaudiet to vēlreiz!',
  'invalid_email_confirmation' => 'Kļūda! E-pasta adreses nesakrīt.',
  'register' => 'REĢISTRĒTIES',
  'password_confirm' => 'Parole atkārtoti',
  'password' => 'Parole',
  'email' => 'E-pasts',
  'email_confirm' => 'E-pasts atkārtoti',
);
