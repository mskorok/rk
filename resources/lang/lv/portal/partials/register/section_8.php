<?php

return array (
  'support_title' => 'JAUTĀJUMS MŪSU SPECIĀLISTAM',
  'sent' => 'Jūsu pieprasījums veiksmīgi nosūtīts. Paldies!',
  'invalid_email' => 'Kļūdaina e-pasta adrese. Lūdzu, pārbaudiet to vēlreiz!',
  'send' => 'NOSŪTĪT',
  'questions' => 'Jūsu jautājums',
  'name' => 'Vārds',
  'surname' => 'Uzvārds',
  'phone' => 'Tālrunis',
  'email' => 'E-pasts',
  'title' => 'JAUTĀJUMS MŪSU SPECIĀLISTAM',
  'success' => 'Jūsu pieprasījums veiksmīgi nosūtīts. Paldies!',
  'error_email' => 'Kļūdaina e-pasta adrese. Lūdzu, pārbaudiet to vēlreiz!',
  'error_name' => 'Saņēmēja Vārds vai Uzvārds nav derīgs. Lūdzu, pārbaudiet to vēlreiz!',
  'error_phone' => 'Saņēmēja tālruņa numurs nav derīgs. Lūdzu, pārbaudiet to vēlreiz!',
  'fill_all_fields' => 'Lūdzu, aizpildiet visus laukus.',
  'message' => 'Jūsu jautājums',
);
