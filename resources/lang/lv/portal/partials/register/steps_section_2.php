<?php

return array (
  'title' => 'LŪDZU, ZEMĀK NORĀDIET, VAI UZ JUMS ATTIECAS KĀDS NO APGALVOJUMIEM',
  'a' => ' Ārpus Latvijas Republikas ieņemu vai pēdējā gada laikā esmu atstājis kādu no šādiem amatiem: valsts vadītājs, parlamenta deputāts, valdības vadītājs, ministrs, ministra vietnieks vai ministra vietnieka vietnieks, valsts sekretārs, augstākās tiesas tiesnesis, konstitucionālās tiesas tiesnesis, augstākās revīzijas (audita) iestādes padomes vai valdes loceklis, centrālās bankas padomes vai valdes loceklis, vēstnieks, pilnvarotais lietvedis, bruņoto spēku augstākais virsnieks, valsts kapitālsabiedrības padomes vai valdes loceklis;',
  'b' => ' Esmu iepriekšminēto personu vecāks, laulātais vai viņam pielīdzināma persona, bērns, viņa laulātais vai laulātajam pielīdzināma persona. Persona par laulātajam pielīdzināmu uzskatāma tikai tad, ja attiecīgās valsts likumi tai nosaka šādu statusu;',
  'c' => 'Esmu persona, par kuru ir publiski zināms, ka tai ar kādu no šī dokumenta 1. punkta ir darījuma attiecības vai kopīgi pieder pamatkapitāls komercsabiedrībā, vai arī fiziska persona, kura ir vienīgais īpašnieks tādam juridiskam veidojumam, par kuru ir zināms, ka tas faktiski izveidots šī dokumenta 1. punktā minētās personas labā;',
  'yes' => 'Jā',
  'no' => 'Nē',
  'position' => 'Lūdzu, norādiet valsti un amatu',
);
