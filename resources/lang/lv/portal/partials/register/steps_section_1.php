<?php

return array (
  'title' => 'SVEICINĀTI, :name :surname',
  'subtitle' => 'LAI REĢISTRĒTOS SKOLĒNA E-KARTES PORTĀLĀ, AIZPILDIET FORMU',
  'add_doc_copy' => 'PIEVIENOT DOKUMENTA KOPIJU',
  'max_image_size' => 'Maks. attēla lielums 4 Mb',
  'allowed_formats' => 'Atļautais formāts: JPG, PNG',
  'doc_number' => 'Personas dokumenta numurs',
  'doc_type' => 'Personas dokuments',
  'citizenship' => 'Pilsonība',
  'address' => 'Deklarētā adrese',
  'phone' => 'Tālrunis',
);
