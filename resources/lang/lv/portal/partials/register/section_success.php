<?php

return array (
  'title' => 'JŪSU KONTS VEIKSMĪGI REĢISTRĒTS',
  'message1' => 'Uz norādīto e-pastu:',
  'message2' => 'tiks nosūtīta apstiprinājuma saite. Uzklikšķiniet uz saites, lai apstiprinātu Jūsu e-pastu un turpinātu reģistrāciju.',
);
