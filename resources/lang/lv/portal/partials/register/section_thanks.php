<?php

return array (
  'title' => 'PALDIES PAR REĢISTRĀCIJU',
  'text1' => 'Jūsu konts tiks apstiprināts 1 (vienas) darba dienas laikā. Pēc apstiprināšanas varēsiet lietot portālu bez ierobežojumiem.',
  'text2' => 'Saskaņā ar Latvijas Republikas normatīvo aktu prasībām Sistēmas pārvaldnieks veiks Jūsu identitātes pārbaudi neklātienē un informēs Jūs par tās rezultātiem, nosūtot e-pastu ne vēlāk kā 1 (vienas) darba dienas laikā.',
);
