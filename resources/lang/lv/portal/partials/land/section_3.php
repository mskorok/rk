<?php

return array (
  'title' => 'VECĀKU ATSAUKSMES',
  'text1' => 'Dēls saņem skolas naudu ēdināšanai  tieši Skolēna e-kartē, un es detalizēti redzu visu, kas notiek.',
  'text2' => 'Tāds nemiers – minēt, vai bērns skolā ir paēdis. Tagad viss ir kā uz delnas!',
  'text3' => 'Abi mani bērni izmanto e-naudu Skolēna e-kartē, un man nav jāuztraucas par kaitīgiem pirkumiem veikalā, kas atrodas blakus skolai.',
  'text4' => 'Ērti izmantot, un mazbērna Skolēna e-kartes papildināšana ir diezgan vienkārša.  Pat es savā vecumā sapratu, kā un ko darīt!',
  'text5' => 'Pirmdienu rītos vairs nav jādomā par sīknaudu, ko dot mazmeitai līdzi uz skolu. Papildinu Skolēna e-karti, un lieta darīta!',
  'grandmother' => 'vecmamma',
  'grandfather' => 'vectēvs',
  'mother' => 'māte',
  'father' => 'tēvs',
);
