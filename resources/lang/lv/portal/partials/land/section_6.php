<?php

return array (
  'title' => 'BIEŽĀK UZDOTIE JAUTĀJUMI',
  'use' => 'LIETOŠANA',
  'register' => 'REĢISTRĀCIJA',
  'security' => 'DROŠĪBA',
  'subtitle1' => 'Kas  jādara, ja manam bērnam nav Skolēna e-kartes vai tās derīguma termiņš ir beidzies?',
  'text1' => 'Plašāka informācija par skolēna e-kartes noformēšanu vai atkārtotu izsniegšanu ir pieejama šeit.',
  'subtitle2' => 'Kā noformēt Skolēna e-karti?',
  'subtitle3' => 'Kā reģistrēties portālā?',
  'subtitle_1_1' => 'Kas  jādara, ja manam bērnam nav Skolēna e-kartes vai tās derīguma termiņš ir beidzies?',
  'text_1_1' => 'Plašāka informācija par skolēna e-kartes noformēšanu vai atkārtotu izsniegšanu ir pieejama <a href="https://www.rigassatiksme.lv/lv/biletes/e-talonu-veidi/skolena-e-karte/ ">šeit</a>.',
  'subtitle_1_2' => 'Kā noformēt Skolēna e-karti?',
  'text_1_2' => 'Plašāka informācija par skolēna e-kartes noformēšanu vai atkārtotu izsniegšanu ir pieejama <a href="https://www.rigassatiksme.lv/lv/biletes/e-talonu-veidi/skolena-e-karte/">šeit</a>.',
  'subtitle_1_3' => 'Kā reģistrēties portālā?',
  'text_1_3' => 'Jums būs nepieciešama Jūsu pases vai ID skenēta kopija vai fotogrāfija, kā arī piekļuve internetbankai. Lai reģistrētos, spiediet <a href="https://skolas.rigaskarte.lv/portal/landing "> šeit</a>.',
  'subtitle_2_1' => 'Kur es varu izmantot Skolēna e-karti?  ',
  'text_2_1' => 'Skolēna e-karte ir līdzeklis  brīvpusdienu saņemšanai un pirkumiem skolas ēdnīcā. Vecākiem ir iespēja pārskaitīt savam bērnam naudu, sākot ar 1 EUR, un būt drošiem, ka bērns neizmantos šo naudu neveselīgas pārtikas iegādei un varēs tērēt naudu tikai skolas ēdnīcā .',
  'subtitle_2_2' => 'Kā lietot skolēna e-karti ēdnīcā?',
  'text_2_2' => 'Pirms norēķināšanās vienkārši pieliec to pie viena no ēdnīcas termināliem ar norādi "Reģistrē brīvpusdienas šeit" vai "Norēķinies ar e-naudu šeit".',
  'subtitle_3_1' => 'Vai ar nozaudēto karti var norēķināties cita persona? ',
  'text_3_1' => 'Jūsu bērna e-karte ir personalizēta, līdz ar to citam cilvēkam būs grūti to izmantot. Nozaudēto karti ar e-naudu Jūs varat nobloķēt mūsu portālā.',
);
