<?php

return array (
  'title' => 'KĀ PORTĀLS VAR PALĪDZĒT VECĀKIEM',
  'text1' => 'Jūs varat sekot līdzi bērna tēriņiem, jo ar Skolēna e-kartes naudu var norēķināties tikai skolā.',
  'text2' => 'Bērns mācās plānot budžetu un viegli apgūst bezskaidras naudas norēķinus.',
  'text3' => 'Ja Jūsu bērnam nav skaidras naudas, to nevar atņemt, pazaudēt vai iztērēt ārpus skolas. Ja Skolēna e-karte tiek pazaudēta, to viegli var nobloķēt.',
  'text4' => 'Jūs vienmēr varēsiet pārliecināties, vai bērns ir paēdis pusdienas. Skolēna e-karti var papildināt nedēļu vai mēnesi uz priekšu.',
  'subtitle1' => 'Nav jāuztraucas',
  'subtitle2' => 'Var iemācīt',
  'subtitle3' => 'Var aizsargāt',
  'subtitle4' => 'Rūpes',
  'user_account' => 'Lietotāja konts',
  'purchase' => 'Pirkums',
  'refill' => 'Pārskaitījums',
  'return_to_account' => 'Atgriezties Lietotāja kontā',
);
