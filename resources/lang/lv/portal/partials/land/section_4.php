<?php

return array (
  'title' => 'IESAKIET PORTĀLU DRAUGIEM',
  'content' => 'Ja zināt kādu, kam mūsu portāls var noderēt, lūdzu, nosūtiet viņam šo uzaicinājumu!',
  'friends_email' => 'Drauga e-pasts',
  'send' => 'NOSŪTĪT',
  'sent' => 'Jūsu ieteikums veiksmīgi nosūtīts. Paldies!',
  'error' => 'Kļūdaina saņēmēja e-pasta adrese. Lūdzu, pārbaudiet to vēlreiz!',
);
