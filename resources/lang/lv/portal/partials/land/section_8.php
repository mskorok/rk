<?php

return array (
  'title' => 'JAUTĀJUMS MŪSU SPECIĀLISTAM',
  'success' => 'Jūsu jautājums veiksmīgi nosūtīts. Paldies!',
  'error_email' => 'Kļūdaina e-pasta adrese. Lūdzu, pārbaudiet to vēlreiz!',
  'error_name' => 'Saņēmēja Vārds vai Uzvārds nav derīgs. Lūdzu, pārbaudiet to vēlreiz!',
  'error_phone' => 'Saņēmēja tālruņa numurs nav derīgs. Lūdzu, pārbaudiet to vēlreiz!',
  'sent' => 'Jūsu jautājums veiksmīgi nosūtīts. Paldies!',
  'fill_all_fields' => 'Lūdzu, aizpildiet visus laukus!',
  'name' => 'Vārds',
  'surname' => 'Uzvārds',
  'email' => 'E-pasts',
  'phone' => 'Tālrunis',
  'message' => 'Jūsu jautājums',
  'send' => 'NOSŪTĪT',
);
