<?php

return array (
  'application' => 'PIELIETOJUMS',
  'advantages' => 'PRIEKŠROCĪBAS',
  'how_to_start' => 'KĀ UZSĀKT',
  'questions' => 'JAUTĀJUMI',
  'enter' => 'IEEJA',
  'enter2' => 'IENĀKT',
  'forget_password' => 'Aizmirsāt paroli?',
  'register' => 'REĢISTRĒTIES',
  'email' => 'E-pasts',
  'password' => 'Parole',
  'profile' => 'JŪSU PROFILS',
  'exit' => 'IZIET',
);
