<?php

return array (
    'description' => 'apraksts',
    'author' => 'autors',
    'keywords' => 'atslēgas vārdi',
    'limit' => 'Izvēlēties limitu \\u20AC',
    'select_school' => 'Izvēlēties skolu',
    'select_payment_type' => 'Izvēlēties maksājuma veidu',
    'select' => 'Izvēlēties',
    'transaction_type' => 'Darījuma veids',
    'status' => 'Statuss',
    'card' => 'E-karte',
    'today' => 'Šodien',
    'yesterday' => 'Vakar',
    'last_7_days' => 'Pēdējās 7 dienās',
    'last_30_days' => 'Pēdējās 30 dienās',
    'this_month' => 'Šomēnes',
    'previous_month' => 'Iepriekšējā mēnesī',
    'title' => 'SKOLAS PORTĀLS',
    'custom' => 'Pielāgojams',
    'applay' => 'Pielietot',
    'cancel' => 'Atcelt',
);
