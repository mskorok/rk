<?php

return array (
  'subject' => 'Jaunas portāla lietotāja e-pasta adreses apstiprinājums',
  'before_link' => 'Cienījamais lietotāj, <br>lai aktivizētu kontā e-pasta adreses maiņu, uzklikšķiniet uz zemāk esošās saites',
  'after_link' => 'Aktivizējiet saiti 24 stundu laikā.<br>Ja saite nedarbojas, lūdzu, iekopējiet to jūsu pārlūkprogrammas adrešu joslā:<br>',
  'after_browser_link' => 'Noklikšķinot uz saites, tiks<br> apstiprināta e-pasta adreses maiņa :new_email, lai ielogotos <br> portālā skolas.rigaskarte.lv.<br> Ja Jums ir kādi jautājumi, sazinieties ar mums: <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a><br>Ar cieņu,<br>PORTĀLS SKOLĒNA E-KARTE',
  'address_approved' => 'Jūsu e-pasta adrese apstiprināta!',
);
