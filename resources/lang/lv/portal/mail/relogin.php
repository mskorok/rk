<?php

return array (
  'subject' => 'Lietotājs ir pieslēdzies ar citu e-pasta adresi',
  'text' => 'Jūs esat iegājis savā kontā, izmantojot e-pastu :new - jūsu pašreizējā konta e-pasta adrese - :old',
);
