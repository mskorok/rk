<?php

return array (
  'subject' => 'Jūsu konts portālā ir dzēsts',
  'text' => 'Jūsu konta dzēšanas apstiprinājums.<br>Lietotājs: :name :surname <br>E-pasts: :email <br>Uz šo vēstuli nav jāatbild.<br>Priecāsimies, ja atgriezīsieties portālā!<br>Ja Jums ir kādi jautājumi, sazinieties ar mums: <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a><br>Ar cieņu,<br>PORTĀLS SKOLĒNA E-KARTE',
);
