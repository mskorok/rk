<?php

return array (
  'subject' => 'jautājums no portāla lietotāja :name :surname',
  'text' => 'Ziņas nosūtīšanas datums un laiks :date <br>Sūtītāja dati: :name :surname <br>Tālruņa numurs :phone <br>E-pasts :email <br>Ziņojums: :text',
);
