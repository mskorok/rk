<?php

return array (
  'subject' => 'Jūsu e-pasta adrese portālā ir atjaunināta',
  'text' => 'Jūsu e-pasta adrese portālā ir nomainīta. Iepriekšējā e-pasta adrese: :old_email <br>Jaunā e-pasta adrese: :email <br>Uz šo vēstuli nav jāatbild.<br>Ja Jums ir kādi jautājumi, sazinieties ar mums: <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a><br>Ar cieņu,<br>PORTĀLS SKOLĒNA E-KARTE',
);
