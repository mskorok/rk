<?php

return array (
  'subject' => 'Jūsu draugs iesaka apmeklēt portālu SKOLĒNA E-KARTE' . "\0" . '',
  'text' => 'Labdien!<br>Jūsu draugs Jums ir nosūtījis uzaicinājumu iepazīties ar portālu SKOLĒNA E-KARTE. Apmeklējiet saiti<a href="https://skolas.rigaskarte.lv">skolas.rigaskarte.lv</a> un uzziniet par iespējām!<br>Skolēna e-karte Jūsu<br> sirdsmieram. Pateicoties portālam, Jūs varat pilnībā kontrolēt Skolēna e-karti:<br><ul type="circle"><li>atzīme par bezmaksas pusdienām</li><li>papildināt to</li><li>apskatīt pirkumu vēsturi</li><li>aizsargāt to ar pin kodu</li><li>nozaudēšanas gadījumā nobloķēt</li><br>Jūs vienmēr zināsiet, vai bērns ir ēdis pusdienas. <br>Vecāki var būt droši, ka bērniem piešķirtā nauda<br> tiek izmantota paredzētajam mērķim – veselīgam uzturam.<br>Ar cieņu,<br>PORTĀLS SKOLĒNA E-KARTE<br>',
);
