<?php

return array (
  'subject' => 'PORTĀLĀ skolas.rigaskarte.lv JAUNA LIETOTĀJA REĢISTRĀCIJA ',
  'text' => 'Sveicināti, :name :surname! <br>Laipni lūgti portālā SKOLĒNA E-KARTE!<br>Apmeklējiet saiti skolas.rigaskarte.lv un uzziniet par iespējām!<br>Skolēna e-karte Jūsu sirdsmieram. Portāls ļauj pilnībā pārvaldīt Jūsu bērna Skolēna e-karti.:<ul type="circle"><li>atzīme par bezmaksas pusdienām</li><li>papildināt to</li><li>apskatīt pirkumu vēsturi</li><li>aizsargāt to ar pin kodu</li><li>nozaudēšanas gadījumā nobloķēt</li></ul><br>Jūs vienmēr zināsiet, vai bērns ir ēdis pusdienas <br>Vecāki var būt droši, ka bērniem piešķirtā nauda tiek izmantota paredzētajam mērķim – veselīgam uzturam.<br>Ja Jums ir kādi jautājumi, sazinieties ar mums: <a href="mailto:skolas@rigaskarte.lv">skolas@rigaskarte.lv</a><br>Ar cieņu,<br>PORTĀLS SKOLĒNA E-KARTE<br>',
  0 => 'atruna',
  'disclaimer' => 'Šis ziņojums attiecas tikai uz norādīto adresātu un var saturēt konfidenciālu, patentētu vai citādi privātu informāciju. Ja neesat norādītais ziņas adresāts, lūdzu, nekavējoties informējiet sūtītāju un izdzēsiet ziņu un visas tās kopijas.',
);
