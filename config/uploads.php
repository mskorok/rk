<?php

// Project config
define('APP_PHOTO_DEFAULT', 1);
define('APP_PHOTO_SMALL', 2);
define('APP_PHOTO_MEDIUM', 3);
define('APP_PHOTO_LARGE', 4);
define('AVATAR_TYPE_CARD', 1);
define('AVATAR_TYPE_USER', 2);
define('AVATAR_TYPE_ETICKET', 3);

return [
    'models_files_path' => 'uploads',
    'avatar' => '/avatar',
    'doc_copy' => '',
    'no_avatar_image_path' => '/assets/portal/img/no_photo.png',
    'img_version_folder' => [
        APP_PHOTO_SMALL => 'small',
        APP_PHOTO_MEDIUM => 'medium',
        APP_PHOTO_LARGE => 'large',
        APP_PHOTO_DEFAULT => 'default'
    ]
];
