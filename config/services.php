<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mandrill' => array(
		'secret' => '0pPkrEc5VO8rbgBPGenL-w',
	),

	'rs' => array(
		'zzurl' => env('ZZ_URL', 'localhost'),
		'zzurl2' => env('ZZ_URL2', 'localhost'),
		'rkurl' => 'https://91.231.68.12/AtlasCatsWebServiceProd/WS_addon.asmx',
		'key' => 'QXNzRnB6MTE5KzA4I3Z2cG9zMjA3N2Y0d2U9MTExZzU=',
		'iv' => 'MTA0OTI1eHhvYXBzMTMjY3MhNEZqazAwN21heDVAOHk=',
		'certkey' => env('ZZ_PASS', ''),
		'rk' => 'TTSprod:xdrRGNnji',
	),

	'mailgun' => array(
		'domain' => 'skolas.rigaskarte.lv',
		'secret' => 'key-9438051b7d1a6d5bc139622615261b72',
	),

	'curl' => array(
		'timeout' => env('CURL_TIMEOUT', 60),
	),

];
